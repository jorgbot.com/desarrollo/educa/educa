<?php
/***********************************************************************************************
* @file        : notas_observa.php                                                             *
* @brief       : planilla de observaciones por alumno y periodo                                *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$numero = isset($_GET['numero']) ? $_GET['numero'] : null ;
$alumtot = isset($_GET['alumtot']) ? $_GET['alumtot'] : null ;
$id_asignatura = $_GET["id_asignatura"];
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_director = $_GET["id_director"];
$pertipo = $_GET["pertipo"];
$periodo = $_GET["periodo"];
$estado = $_GET["estado"];
$op = isset($_GET['op']) ? $_GET['op'] : null ;
connect();
//datos asignatura
$sql="select ano, grado, area, asignatura from asignaturas where id_asignatura = '$id_asignatura' limit 1";
$datasig = viewsql($sql);
foreach($datasig as $regasig)
{
	$ano = $regasig->ano;
	$grado = $regasig->grado;
	$asignatura = $regasig->asignatura;
}
//nivel del grado
$sql="select nivel from grados where grado = '$grado' limit 1";
$datasig = viewsql($sql);
foreach($datasig as $regasig)
{$nivel = $regasig->nivel;}
//traer id_alumno del curso
if ($numero > 0)
{
	$cursos = "y".$ano."cursos";
	$sql="SELECT id_alumno, apellidos, nombres, tipoedu, fchretiro FROM $cursos WHERE grado = '$grado' and curso = '$curso' and numero = '$numero' limit 1";
	$datcurs=viewsql($sql);
	foreach($datcurs as $regcurs)
	{
		$id_alumno = $regcurs->id_alumno;
		$alumno = $regcurs->apellidos." ".$regcurs->nombres;
		$tipoedu = $regcurs->tipoedu;
		$fchretiro = $regcurs->fchretiro;
	}
	$numero = intval($numero);
	$alumtot = intval($alumtot);
	$numeroant = $numero - 1;
	$numerosig = $numero + 1;
	if ($numeroant <= 0)
		$numeroant = 1;
	if ($numerosig > $alumtot)
		$numerosig = $alumtot;
}
else
{
	$sql="SELECT apellidos, nombres, tipoedu, fchretiro FROM alumnos WHERE id_alumno = '$id_alumno' limit 1";
	$datalum=viewsql($sql);
	foreach($datalum as $regalum)
	{
		$alumno = $regalum->apellidos." ".$regalum->nombres;
		$tipoedu = $regalum->tipoedu;
		$fchretiro = $regalum->fchretiro;
	}
}
//datos alumno
if ($tipoedu == "E")
	$alumno = $alumno." (ESPECIAL)";
//borrar observacion periodo de notas
if ($op == "borrarobservacion")
{
	$file = "y".$ano."observa";
	$sql = "DELETE FROM $file WHERE id_obseasig='".$_GET["id_obseasig"]."' limit 1";
	execsql($sql);
}
//asignatura pendiente o inactivar
$notatot = 0;
$inactivar = 0;
$pendiente = 0;
$chkpen = "";
$chkina = "";
$file = "y".$ano."notas";
$sql="select nota, pendiente, inactivar from $file where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = '$pertipo' and periodo = '$periodo' limit 1";
$dattot=viewsql($sql);
foreach($dattot as $regtot)
{
	$notatot = $regtot->nota;
	$inactivar = $regtot->inactivar;
	$pendiente = $regtot->pendiente;
	if ($regtot->pendiente > 0)
		$chkpen = "checked";
	if ($regtot->inactivar > 0)
		$chkina = "checked";
}
//lista de observaciones por nivel
$sql="SELECT observaciones.*, categorias.categoria FROM observaciones, categorias where observaciones.id_categoria = categorias.id_categoria and observaciones.niveles like '% $nivel %' ";
if ($id_director <= 0) //si usuario no es el director de grupo
	$sql.="and observaciones.director = '0' ";
$sql.="order by categorias.categoria, observaciones.observacion";
$datlis = viewsql($sql);
$readonly="readonly";
$action="";
if (($_SESSION["nivnot"] >= 2 and $estado == "ABIERTA") or $_SESSION["nivnot"] >= 4)
{
	$readonly="";
	$action="database/grabar_planillas.php";
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='javascript' type='text/JavaScript'>
function validar()
{
	var idalumno = eval ('document.form1.id_alumno.value');
	if(document.form1.notatot.value <= 0)
	{
		if (document.form1.chkina.checked == true)
		{
			eval ("opener.document.form1.inac"+idalumno+".value='"+1+"'");
			eval ("opener.document.form1.cali"+idalumno+".value='INAC'");
		}
		else
		{
			eval ("opener.document.form1.inac"+idalumno+".value='"+0+"'");
			eval ("opener.document.form1.cali"+idalumno+".value=''");
			if (document.form1.chkpen.checked == true)
			{
				eval ("opener.document.form1.pend"+idalumno+".value='"+1+"'");
				eval ("opener.document.form1.cali"+idalumno+".value='PEND'");
			}
			else
			{
				eval ("opener.document.form1.pend"+idalumno+".value='"+0+"'");
				eval ("opener.document.form1.cali"+idalumno+".value=''");
			}
		}
	}
	return true;
}
</script>
</head>
<body>
<form action="<?php echo $action;?>" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='planilla_observaciones'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='curso' value='<?php echo $curso;?>'>
<input type="hidden" name="pertipo" value="<?php echo $pertipo;?>">
<input type="hidden" name="periodo" value="<?php echo $periodo;?>">
<input type="hidden" name="estado" value="<?php echo $estado;?>">
<input type='hidden' name='numero' value='<?php echo $numero;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type='hidden' name='id_director' value='<?php echo $id_director;?>'>
<input type='hidden' name='alumno' value='<?php echo $alumno;?>'>
<input type='hidden' name='notatot' value='<?php echo $notatot;?>'>
<input type='hidden' name='oldpendiente' value='<?php echo $pendiente;?>'>
<input type='hidden' name='oldinactivar' value='<?php echo $inactivar;?>'>
<input type='hidden' name='alumtot' value='<?php echo $alumtot;?>'>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src="images/observa.jpg" width="40" height="40" border="0" align="absmiddle"> PERIODO <?php echo $periodo;?> - OBSERVACIONES <?php echo $asignatura;?></td>
		<td class="subtitulo" align="right">GRADO <?php echo $grado;?> A�O <?php echo $ano;?></td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td colspan="2">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tabla_color">
			<tr height="30">
				<td>
					&nbsp;&nbsp;
					<?php if ($numero > 0) { ?>
					<a href="notas_observa.php?numero=<?php echo $numeroant;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&pertipo=<?php echo $pertipo;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>"><img src="images/arrback.png" border="0" width="24" height="24" hspace="2" title="Alumno Anterior..." align="absmiddle"></a>
					<?php } ?>
					<img src="images/alumnosm.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"> <b><?php echo $alumno;?></b>
					<?php if ($numero > 0) { ?>
					<a href="notas_observa.php?numero=<?php echo $numerosig;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&pertipo=<?php echo $pertipo;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>"><img src="images/arrforwardt.png" border="0" width="24" height="24" hspace="2" title="Alumno Siguiente..." align="absmiddle"></a>
					<?php } ?>
				</td>
				<td align="right">
					<b>
					Asignatura Nota Pendiente <input type="checkbox" name="chkpen" <?php echo $chkpen;?> <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
					&nbsp;&nbsp;
					<?php if ($_SESSION["nivnot"] >= 4) { ?>
					Inactivar Nota en Promedios <input type="checkbox" name="chkina" <?php echo $chkina;?> <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
					<?php } else { ?>
					Inactivar Nota en Promedios <input type="checkbox" name="chkina" <?php echo $chkina;?> readonly="readonly" onclick="javascript: return false;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
					<?php } ?>
					</b>
					&nbsp;&nbsp;
				</td>
			</tr>
		</table>
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="20">
		<th width="80" background="images/backsilver.gif">Asignatura</th>
		<th background="images/backsilver.gif">Observaci�n</th>
		<th width="20" background="images/backsilver.gif"></th>
	</tr>
	<?php
	$id_libre = 0;
	$file = "y".$ano."observa";
	$sql="select $file.*, asignaturas.asignatura from $file, asignaturas where $file.id_alumno = '$id_alumno' and $file.pertipo = '$pertipo' and $file.periodo = '$periodo' and $file.id_asignatura = asignaturas.id_asignatura order by asignaturas.asignatura";
	$datobse = viewsql($sql);
	foreach($datobse as $regobse)
	{
	$id_obseasig = $regobse->id_obseasig;
	if ($regobse->id_observacion == 0) //existe observacion libre
		$id_libre = $regobse->id_obseasig;
	$swmod = "readonly";
	if ($regobse->id_asignatura == $id_asignatura and $action!="")
		$swmod = "";	
	?>		
	<tr height="25" bgcolor="#FFFFFF">
		<td width="80" title="<?php echo $regobse->asignatura;?>"><?php echo substr($regobse->asignatura,0,15);?></td>
		<td>
			<?php if ($regobse->id_observacion > 0) { ?>
			<?php echo $regobse->observacion;?>
			<?php } else { ?>
			<textarea name="observacion" rows="2" <?php echo $swmod;?> style="width: 700px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $regobse->observacion;?></textarea>
			<?php } ?>
		</td>
		<td width="20" align="center">
			<?php if ($action!="") { ?>
			<a href="notas_observa.php?op=borrarobservacion&id_obseasig=<?php echo $id_obseasig;?>&id_alumno=<?php echo $id_alumno;?>&id_asignatura=<?php echo $id_asignatura;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumno=<?php echo $alumno;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><img src="images/b_drop.png" width="16" height="16" border="0" align="absmiddle" title="Eliminar"></a>
			<?php } ?>
		</td>
	</tr>
	<?php } ?>
	<tr bgcolor="#FFFFFF">
		<td><?php echo substr($asignatura,0,15);?></td>
		<td>
		<fieldset>
		<legend class="titulo_campos">Nueva Observaci�n de Lista (Bolet�n)</legend>
		<select name="id_observacion" style="width: 700px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
		<option value="" selected>Seleccionar...</option>
		<?php
		$id_categoria = "";
		foreach($datlis as $reglis)
		{
			if ($reglis->id_categoria != $id_categoria)
			{
				echo "<option value='' style='color: #FFFFFF; background-color: #006C2E;'>$reglis->categoria</option>";
				$id_categoria = $reglis->id_categoria;
			}
			$title = trim($reglis->observacion);
			if (strlen($title) > 110)
				$observacion = substr($title,0,110)."...";
			else
				$observacion = $title;
			echo "<option value='$reglis->id_observacion' title='$title'>$observacion</option>";
		}
		?>																
		</select>
		<input type='hidden' name='id_libre' value='<?php echo $id_libre;?>'>
		</fieldset>
		</td>
		<td></td>
	</tr>
	<?php if ($id_libre == 0) { ?>
	<tr bgcolor="#FFFFFF">
		<td><?php echo substr($asignatura,0,15);?></td>
		<td>
		<fieldset>
		<legend class="titulo_campos">Nueva Observaci�n Libre (Interna)</legend>
		<textarea name="observacion" rows="2" <?php echo $readonly;?> style="width: 700px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></textarea>
		</fieldset>
		</td>
		<td></td>
	</tr>
	<?php } ?>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($action!="") { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
</form>
</body>
</html>
<?php disconnect();?>
