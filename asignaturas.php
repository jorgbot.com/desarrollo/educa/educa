<?php
/***********************************************************************************************
* @file        : asignaturas.php                                                               *
* @brief       : Muestra la lista general de areas, materias y asignatutas por a�o             *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 20-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 20-feb-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivasi"] <= 0)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</script>";}
include("database/database.php");
include("libs/fechas.php");
connect();
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$op = isset($_POST['op']) ? $_POST['op'] : null ;
$id_profesor = isset($_GET['id_profesor']) ? $_GET['id_profesor'] : null ;
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
$usuario = $_SESSION["usutip"];
//lista de grados
$sql="select id_grado, grado, gradonom, gradocolor from grados order by id_grado";
$datgrad=viewsql($sql);
if ($_SESSION["usutip"]!="DOCENTE" and $id_profesor <= 0 and $grado == "") //no ha seleccionado profesor
{
	foreach($datgrad as $reggrad)
	{
		$grado = $reggrad->grado;
		break;
	}
}
if ($_SESSION["usutip"]=="DOCENTE" and $id_profesor <= 0)
{
	$id_profesor = $_SESSION["usunro"];
	$grado = "";
}
//lista de asignaturas
if ($id_profesor > 0)
{
	//asignaturas del docente
	$file = "y".$ano."profasig";
	$sql="SELECT $file.id_profasig, asignaturas.* FROM $file, asignaturas WHERE $file.id_profesor = '$id_profesor' and $file.id_asignatura = asignaturas.id_asignatura AND asignaturas.ano = '$ano' ";
	if ($grado != "")
		$sql.="AND asignaturas.grado = '$grado' ";
	$sql.= "GROUP BY asignaturas.id_asignatura ORDER BY sign(asignaturas.grado), cast(SUBSTRING_INDEX(asignaturas.grado, '-', 1) as decimal), asignaturas.area, asignaturas.asignatura";
}
else
{
	//todas las asignaturas
	$sql="SELECT * FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' ";
	$sql.= "ORDER BY area, asignatura";
}
$datasig=viewsql($sql);
//echo "$sql<br>";
?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>IDIC</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="robots" content="noindex"/>
<meta name="googlebot" content="noindex"/>
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	var usutip = document.form0.usuario.value;
	var ano = document.form0.ano.value;
	var grado = document.form0.grado.value;
	var id_profesor = document.form0.id_profesor.value;
	var old_profesor = document.form0.old_profesor.value;
	if (old_profesor <= 0 && id_profesor > 0 && usutip=='OPERADOR')
		grado = '';
	var theURL = 'asignaturas.php?ano='+ano+'&grado='+grado+'&id_profesor='+id_profesor;
	window.location.href = theURL;
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#FFFF66">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="150" bgcolor="#006600">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr height="40" bgcolor="#006600">
		<td background="fondos/top_verde.jpg"></td>
		<td background="fondos/top_verde.jpg" class="titulo_color">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_top.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/top_verde.jpg"></td>
	</tr>
	<tr height="3" bgcolor="#FFFF66">
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_top.jpg"></td></tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="150">
					<?php include("includes/menu.php");?>
				</td>
				<td width="10">
				</td>
				<td align="center">
					<div class="div_marco">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr height="40">
							<td width="50"><img src='images/asignaturas.png' width="40" height="40" border="0" align="absmiddle"></td>
							<td class="titulo">Asignaturas</td>
							<td align="right">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>&nbsp;&nbsp;<a class="link_black" href="horarios.php?ano=<?php echo $ano;?>&grado=<?php echo $grado;?>" onclick="newWindow(this.href, '', 1024, 550, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Horarios..."><img src="images/horario.gif" width="28" height="28" border="0" hspace="0" align="absmiddle"> Horarios</a>&nbsp;&nbsp;</td>
										<td width="1" bgcolor="#FFCC00"></td>
										<td>&nbsp;&nbsp;<a class="link_black" href="asignaturas_observa.php" onclick="newWindow(this.href, '', 850, 550, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Tabla Observaciones..."><img src="images/observa.png" width="28" height="28" border="0" hspace="0" align="absmiddle"> Observaciones</a>&nbsp;&nbsp;</td>
										<td width="1" bgcolor="#FFCC00"></td>
										<td>&nbsp;&nbsp;<a class="link_black" href="asignaturas_areas.php?grado=<?php echo $grado;?>" onclick="newWindow(this.href, '', 600, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Tabla de Areas..."><img src="images/areas.png" width="28" height="28" border="0" hspace="0" align="absmiddle"> Areas</a>&nbsp;&nbsp;</td>
										<td width="1" bgcolor="#FFCC00"></td>
										<td>&nbsp;&nbsp;<a class="link_black" href="asignaturas_grados.php" onclick="newWindow(this.href, '', 550, 500, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Tabla de Grados..."><img src="images/grados.png" width="28" height="28" border="0" hspace="0" align="absmiddle"> Grados</a>&nbsp;&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					</div>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<form action="" method="post" name="form0">
					<input type="hidden" name="op" value="buscar">
					<input type="hidden" name="usuario" value="<?php echo $_SESSION["usutip"];?>">
					<input type="hidden" name="old_profesor" value="<?php echo $id_profesor;?>">
						<tr><td colspan="3" height="15"></td></tr>
						<tr height="32">
							<td width="6" background="images/backsilver401.gif"></td>
							<td align="right" background="images/backsilver402.gif">
								<?php if ($_SESSION["usutip"]=="OPERADOR") { ?>
								<select name="id_profesor" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
								<option value="" selected>Docentes...</option>
								<?php
								$file = "y".$ano."profasig";
								$sql="SELECT id_profesor, profesor, grado FROM $file group by id_profesor order by profesor";
								$datprof=viewsql($sql);
								foreach($datprof as $regprof)
								{
									if ($regprof->id_profesor==$id_profesor)
									echo "<option value='$regprof->id_profesor' selected>$regprof->profesor</option>";
									else
									echo "<option value='$regprof->id_profesor'>$regprof->profesor</option>";
								}
								?>																
								</select>
								&nbsp;&nbsp;
								<?php } else { ?>
								<input type="hidden" name="id_profesor" value="<?php echo $id_profesor;?>">
								<?php } ?>
								<select name="grado" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
								<option value="" selected>Grados...</option>
								<?php
								if ($_SESSION["usutip"]=="DOCENTE")
									echo "<option value='' selected>GRADOS...</option>";	
								foreach($datgrad as $reggrad)
								{
									if ($grado == $reggrad->grado)
										echo "<option value='$reggrad->grado' selected>GRADO $reggrad->gradonom</option>";
									else
										echo "<option value='$reggrad->grado'>GRADO $reggrad->gradonom</option>";
								}
								?>																
								</select>
								&nbsp;
								<select name="ano" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
								<?php
								$sql="SELECT ano FROM anoslect ORDER BY ano desc";
								$datanol=viewsql($sql);
								foreach($datanol as $reganol)
								{
									$selano = "";
									if ($reganol->ano==$ano)
										$selano = "selected";
									echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
								}
								?>
								</select>
								&nbsp;
								<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
								<?php if ($ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 4) { ?>
								&nbsp;
								<a href="asignaturas_edit.php?grado=<?php echo $grado;?>" onclick="newWindow(this.href, '', 750, 430, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro Asignatura"></a>
								<?php } ?>
								&nbsp;
								<a href="reports/rep_asignaturas.php?ano=<?php echo $ano;?>&grado=<?php echo $grado;?>" title="Reporte..." onclick="newWindow(this.href, '', 1024, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_imprimir.png' border="0" width="70" height="24" alt="Imprimir" align="absmiddle" title="Imprimir Asignaturas"></a>
							</td>
							<td width="6" background="images/backsilver403.gif"></td>
						</tr>
					</form>
					</table>
					<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="100%">
						<tr height="20">
							<th width="20"></th>
							<th width="230">Area</th>
							<th>Asignatura</th>
							<th width="40">C�digo</th>
							<th width="30">Horas</th>
							<th width="35">%</th>
							<th width="60"></th>
							<th width="70"></th>
						</tr>
						<?php
						foreach($datasig as $regasig)
						{
						$id_asignatura = $regasig->id_asignatura;
						$asignatura = $regasig->asignatura;
						$seccion="seccion".$id_asignatura;
						$codigo = $regasig->codigo;
						?>		
						<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
							<td align="center" bgcolor="<?php echo $regasig->gradocolor;?>" class="titulo_campos"><font color="<?php echo $regasig->gradofont;?>"><?php echo $regasig->grado;?>&deg;</font></td>
							<td title="ID_area <?php echo $regasig->id_area;?>"><?php echo substr($regasig->area,0,35);?></td>
							<td><?php echo $regasig->asignatura;?></td>
							<td><a class="link_list" href="asignaturas_edit.php?id_asignatura=<?php echo $id_asignatura;?>&grado=<?php echo $regasig->grado;?>" onclick="newWindow(this.href, '', 750, 430, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="CODIGO"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $codigo;?></a></td>
							<td align="center"><?php echo $regasig->intensidad;?></td>
							<td align="center">% <?php echo $regasig->porcentaje;?></td>
							<td><a class="link_list" href="asignaturas_logros.php?id_asignatura=<?php echo $id_asignatura;?>" onclick="newWindow(this.href, '', 980, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Logros"><img src="images/logrosm.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"> Logros</a></td>
							<td><a class="link_list" href="asignaturas_profe.php?id_asignatura=<?php echo $id_asignatura;?>" onclick="newWindow(this.href, '', 700, 400, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Docentes"><img src="images/profesorsm.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"> Docentes</a></td>
						</tr>
						<?php } ?>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_bot.jpg"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#D3A919"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FFDB4B"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FCF463"></td></tr>
	<tr>
		<td colspan="3" valign="top">
		<div id="fondo_pie">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</div>
		</td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>