<?php
/************************************************************************************************
* @file        : database.php                                                                   *
* @brief       : Funciones para base de datos con Mysqli                                        *
* @version     : 1.0                                                                            *
* @company     : CAT SYSTEM                                                                          *
* @author      : Nora Rodriguez 15-feb-2012                                                     *
* @Modificado  : Nora Rodriguez 03-dic-2018                                                     *
************************************************************************************************/
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />   
</head>

<body>

<?php

	$bd="educa001";
	$hostbd="localhost";
	$userbd="root";
	$password="root";

$link = mysqli_connect($hostbd , $userbd , $password);
        if (mysqli_connect_errno()) 
        {
            printf("Fall� la conexi�n: %s\n", mysqli_connect_error());
        } else {
echo "ESTE ES EL NUMERO $link";
            
        }


mysqli_select_db($link, $bd);

$tildes = $link->query("SET NAMES 'utf8'"); //Para que se muestren las tildes

$result = mysqli_query($link, "SELECT * FROM niveles");

mysqli_data_seek ($result, 0);

$extraido= mysqli_fetch_array($result);

echo "- Id: ".$extraido['id_nivel']."<br/>";

echo "- nivel: ".$extraido['nivel']."<br/>";

echo "- nombre: ".$extraido['nombre']."<br/>";

mysqli_free_result($result);

mysqli_close($link);

?>

</body>
</html>
