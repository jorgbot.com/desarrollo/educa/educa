<?php
/***********************************************************************************************
* @file        : setup_usuariosedit.php                                                        *
* @brief       : Edita usuario de aplicativo                                                   *
* @author      : Nora Rodriguez 12-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 12-abr-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesión de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_setup.php");
include("libs/fechas.php");
include("libs/image_resize.php");
$id_usuario = isset($_GET['id_usuario']) ? $_GET['id_usuario'] : null ;
$origen = isset($_GET['origen']) ? $_GET['origen'] : null ;
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : null ;
$id_profesor = isset($_POST['id_profesor']) ? $_POST['id_profesor'] : null ;
connect();
$titulo="NUEVO USUARIO";
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/validInput.js'></script>
<script language="javascript">
function validarsel()
{
	if (document.form1.tipo[0].checked==true && document.form1.id_profesor.value <= 0) 
	{alert('Falta seleccionar el Docente...'); return false;}
	return true;
}
function validar()
{
	if(document.form1.origen.value == '')
	{
		if(document.form1.login.value == "")
		{alert("Falta asignar login...");document.form1.login.focus(); return false;}
		if(document.form1.apellidos.value == '')
		{alert('Falta asignar Apellidos...');document.form1.apellidos.focus(); return false;}
		if(document.form1.nombres.value == '')
		{alert('Falta asignar Nombres...');document.form1.nombres.focus(); return false;}
		if(document.form1.docunum.value <= 0)
		{alert('Falta asignar Identificación...');document.form1.docunum.focus(); return false;}
		if(document.form1.correo.value == '')
		{alert("Falta dirección de correo...");document.form1.correo.focus(); return false;}
		if(document.form1.tipo.value == '')
		{alert("Falta tipo de perfil...");document.form1.tipo.focus(); return false;}
		if(document.form1.login.value.substr(0,1) > 0 || document.form1.login.value.substr(0,1) == '0')
		{alert("Login debe comenzar por una letra...");document.form1.login.focus(); return false;}
	}
	if(document.form1.psw.value=="")
	{alert("Favor digitar Contraseña");document.form1.psw.focus();return false;}
	else
	{
		if(document.form1.psw2.value=="")
		{alert("Favor confrimar Contraseña");document.form1.psw2.focus();return false;}
		psw1=document.form1.psw.value;
		psw2=document.form1.psw2.value;
		if(psw1!=psw2)
		{alert("Las contraseñas son diferentes Favor Confirmar");document.form1.psw2.focus(); return false;}
	}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar este usuario?");
	if(ok)
	{
		document.form1.op.value='borrarusuario';
		document.form1.submit();
	}
}
</script>
</head>
<body>
<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
	<?php if ($opcion == "SELECCIONAR") { ?>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr height="50">
					<td class="titulo"><img src="images/key.png" border="0" width="16" height="16" align="absmiddle"> <?php echo $titulo;?></td>
					<td class="titulo" align="right"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EEF0F0" class="tabla_marco">
			<form action="setup_usuariosedit.php" method="post" name="form1" onSubmit="return validarsel();">
			<input type="hidden" name="opcion" value="editar">
				<tr height="30"><td width="30"></td><td></td><td width="30"></td></tr>
				<tr>
					<td></td>
					<td align="center">
						<table border="0" cellpadding="0" cellspacing="0" width="400">
							<tr height="30">
								<td><input type="radio" name="tipo" value="DOCENTE" checked> DOCENTE</td>
								<td>
									<input type="hidden" name="id_profesor">
									<input type="text" name="profesor" value="Seleccionar..." style="width: 230px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<a href="profesores_select.php" onclick="newWindow(this.href, '', 800, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_lupa.png' border="0" width="24" height="24" alt="Buscar" align="absmiddle" title="Seleccionar Profesor..."></a>
								</td>
							</tr>
							<tr height="30">
								<td><input type="radio" name="tipo" value="OPERADOR"> OPERADOR</td>
								<td></td>
							</tr>
						</table>
					</td>
					<td></td>
				</tr>
				<tr height="30"><td colspan="3"></td></tr>
				<tr height="36" valign="middle">
					<td colspan="7" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
						<input type="image" class="botones_img" name="continuar" src="images/btn_continuar.png" width="75" height="24" border="0" alt="Grabar" align="absmiddle" hspace="20">
						<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="20" align="absmiddle"></a>
					</td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
	<?php }
	else //editar usuario
	{
	if ($id_profesor > 0) //nuevo profesor
	{
		$sql="select id_usuario from usuarios where tipo = 'DOCENTE' and id_tipo = '$id_profesor' limit 1";
		$datusua = viewsql($sql);
		foreach($datusua as $regusua)
		{$id_usuario = $regusua->id_usuario;}
	}
	$datusuario = datos_usuario($id_usuario);
	if ($id_usuario <= 0 and $id_profesor > 0)
	{
		$datusuario["id_tipo"] = $id_profesor;
		$datusuario["tipo"] = "DOCENTE";
		$sql="select * from usuarios where tipo = 'DOCENTE' LIMIT 1";
		$datusua = viewsql($sql);
		foreach($datusua as $regusua)
		{
			$datusuario["nivalu"] = $regusua->nivalu;
			$datusuario["nivmat"] = $regusua->nivmat;
			$datusuario["nivpro"] = $regusua->nivpro;
			$datusuario["nivasi"] = $regusua->nivasi;
			$datusuario["nivnot"] = $regusua->nivnot;
			$datusuario["nivfal"] = $regusua->nivfal;
			$datusuario["nivusu"] = $regusua->nivusu;
			$datusuario["nivpar"] = $regusua->nivpar;
		}
	}
	if ($id_usuario > 0)
		$titulo = "EDITAR PERFIL DE USUARIO";
	if ($datusuario["tipo"] == "DOCENTE")
	{
		$sql="select apellidos, nombres, docunum, correo, foto from profesores where id_profesor = '".$datusuario["id_tipo"]."' limit 1";
		$datprof = viewsql($sql);
		foreach($datprof as $reg)
		{
			$datusuario["apellidos"] = $reg->apellidos;
			$datusuario["nombres"] = $reg->nombres;
			$datusuario["docunum"] = $reg->docunum;
			$datusuario["correo"] = $reg->correo;
			$datusuario["foto"] = $reg->foto;
		}
	}
	?>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr height="50">
					<td class="titulo"><img src="images/key.png" border="0" width="16" height="16" align="absmiddle"> <?php echo $titulo;?></td>
					<td class="titulo" align="right"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EEF0F0" class="tabla_marco">
			<form action="database/grabar_setup.php" method="post" name="form1" onSubmit="return validar();">
			<input type="hidden" name="op" value="grabarusuario">
			<input type="hidden" name="id_usuario" value="<?php echo $id_usuario;?>">
			<input type="hidden" name="id_tipo" value="<?php echo $datusuario["id_tipo"];?>">
			<input type="hidden" name="tipo" value="<?php echo $datusuario["tipo"];?>">
			<input type="hidden" name="foto" value="<?php echo $datusuario["foto"];?>">
			<input type="hidden" name="oldpass" value="<?php echo $datusuario["password"];?>">
			<input type="hidden" name="origen" value="<?php echo $origen;?>">
				<tr height="30">
					<th colspan="7" background="images/backsilver.gif" align="right">
						<?php if ($datusuario["inactivar"] <= 0) { ?>
						ACTIVO <input type="radio" name="inactivar" value="0" checked style="border: 1px solid #006600;">
						&nbsp;&nbsp;&nbsp;
						INACTIVO <input type="radio" name="inactivar" value="1" style="border: 1px solid #006666;">
						<?php } else { ?>
						ACTIVO <input type="radio" name="inactivar" value="0" style="border: 1px solid #006600;">
						&nbsp;&nbsp;&nbsp;
						INACTIVO <input type="radio" name="inactivar" value="1" checked style="border: 1px solid #006666;">
						<?php } ?>
					</th>
				</tr>
				<tr height="10"><td width="15"></td><td></td><td></td><td></td><td width="15"></td><td></td><td width="15"></td></tr>
				<tr valign="top">
					<td></td>
					<td width="160" height="160" bgcolor="#E0E7E9" align="center" valign="middle">
						<?php if ($datusuario["foto"] == "" or !file_exists($datusuario["foto"])) { ?>
						<img src="images/foto.gif" width="80" height="80" border="0" align="absmiddle">
						<?php } else {  $height = 150; $width = resize_width($datusuario["foto"],150); ?>
						<img src="<?php echo $datusuario["foto"];?>" width="<?php echo $width;?>" height="<?php echo $height;?>" border="0" align="absmiddle">
						<?php } ?>
					</td>
					<td width="10"></td>
					<td>
						<fieldset>
						<legend class="titulo_campos">PERFIL <?php echo $datusuario["tipo"];?></legend>
						<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<tr height="5"><td colspan="2"></td></tr>
							<tr>
								<td>Login </td>
								<td>
									<input type="text" name="login" style="width: 180px; text-transform:uppercase;" maxlength="15" value="<?php echo $datusuario["login"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr>
								<td>Apellidos </td>
								<td><input type="text" name="apellidos" onKeyUp="javascript:checkText(form1.apellidos);" value="<?php echo $datusuario["apellidos"];?>" style="width: 180px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							<tr>
							<tr>
								<td>Nombres </td>				
								<td><input type="text" name="nombres" onKeyUp="javascript:checkText(form1.nombres);" value="<?php echo $datusuario["nombres"];?>" style="width: 180px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							<tr>
							<tr>
								<td>Cédula </td>				
								<td><input type="text" name="docunum"  style="width: 180px;" maxlength="15" value="<?php echo $datusuario["docunum"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							<tr>
							<tr>
								<td>Correo </td>
								<td>
									<input type="text" name="correo" style="width: 180px;" value="<?php echo $datusuario["correo"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr height="5"><td colspan="2"></td></tr>
						</table>
						</fieldset>
						<br>
						<fieldset>
						<legend class="titulo_campos">Contraseña</legend>
						<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<?php if ($origen == "") { ?>
							<tr>
								<td>Contraseña <font color="red">*</font></td>
								<td><input type="password" name="psw" style="width: 180px;" value="<?php echo $datusuario["password"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>               
							<tr>
								<td>Confirmación <font color="red">*</font></td>
								<td><input type="password" name="psw2" style="width: 180px;" value="<?php echo $datusuario["password"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							<?php } else { ?>
							<tr>
								<td>Contraseña <font color="red">*</font></td>
								<td><input type="password" name="psw" style="width: 180px;" value="" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>               
							<tr>
								<td>Confirmación <font color="red">*</font></td>
								<td><input type="password" name="psw2" style="width: 180px;" value="" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							<?php } ?>
						</table>
						</fieldset>
					</td>
					<td></td>
					<td>
						<fieldset>
						<legend class="titulo_campos">Permisos Tablas</legend>
						<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<tr>
								<td>Alumnos</td>
								<td>
									<select name="nivalu" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									$sql="select * from listas where listip='NIVELES' order by lisval";
									$lista=viewsql($sql);
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivalu"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Matriculas</td>
								<td>
									<select name="nivmat" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivmat"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Docentes</td>
								<td>
									<select name="nivpro" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivpro"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Asignaturas</td>
								<td>
									<select name="nivasi" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivasi"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Notas</td>
								<td>
									<select name="nivnot" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivnot"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Fallas</td>
								<td>
									<select name="nivfal" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivfal"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Pensiones</td>
								<td>
									<select name="nivpag" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivpag"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Transporte</td>
								<td>
									<select name="nivtra" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivtra"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Usuarios</td>
								<td>
									<select name="nivusu" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivusu"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Parámetros</td>
								<td>
									<select name="nivpar" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($lista as $registros)
									{
										if (intval($datusuario["nivpar"])==intval($registros->lisval))
											echo "<option value='$registros->lisval' selected>$registros->lisnom</option>";
										else
											echo "<option value='$registros->lisval'>$registros->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
					<td></td>
				</tr>
				<tr><td colspan="7" height="10"></td></tr>
				<tr height="35" valign="middle">
					<td colspan="7" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
						<?php if ($_SESSION["nivusu"] >= 2 or $origen == "USUARIO") { ?>
						<input type="image" class="botones_img" name="grabar" src="images/btn_grabar.png" width="75" height="24" border="0" alt="Grabar" align="absmiddle" hspace="20">
						<?php } if ($id_usuario!="" and $origen=="" and $_SESSION["nivusu"] >= 3) { ?>
						<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="20" align="absmiddle"></a>
						<?php } ?>
						<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="20" align="absmiddle"></a>
					</td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
	<?php } ?>
</table>
</body>
</html>
<?php disconnect();?>