<?php
/***********************************************************************************************
* @file        : notas.php                                                                     *
* @brief       : Lista de asignaturas por docente, grado y curso para selecci�n periodo notas  *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 28-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 28-feb-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivnot"] <= 0)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
connect();
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_profesor = isset($_GET['id_profesor']) ? $_GET['id_profesor'] : null ;
if ($id_profesor <= 0 and $_SESSION["usutip"]=="DOCENTE")
	$id_profesor = $_SESSION["usunro"];
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
//traer profesores asignados a cursos
$file = "y".$ano."profasig";
$sql="SELECT id_profesor, profesor, grado FROM $file group by id_profesor order by profesor";
$datprof=viewsql($sql);
//lista de cursos
$director = 0;
$sql="SELECT cursos.grado, cursos.curso, cursos.id_director, grados.gradonom FROM cursos, grados where cursos.ano = '$ano' and cursos.grado = grados.grado order by grados.id_grado, cursos.curso";
$datcurs=viewsql($sql);
foreach($datcurs as $regcurs)
{
	if ($grado == "" and $id_profesor <= 0)
	{
		$grado = $regcurs->grado;
		$curso = $regcurs->curso;
	}
	if ($regcurs->grado == $grado and $regcurs->curso == $curso and $regcurs->id_director == $id_profesor)
		$director = 1;
}
//traer periodos
$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
$datperi=viewsql($sql);
$numperi = count($datperi);
//1. traer asignaturas que requieren notas de planilla
$profasig = "y".$ano."profasig";
$planillas = "y".$ano."planillas";
$sql = "SELECT asignaturas.id_asignatura, asignaturas.grado, asignaturas.gradocolor, asignaturas.gradofont, asignaturas.asignatura, $profasig.* FROM asignaturas, $profasig ".
"WHERE asignaturas.ano = '$ano' and (asignaturas.notas = '1' or asignaturas.observacion = '1') ";
if ($id_profesor > 0)
	$sql.="AND $profasig.id_profesor = '$id_profesor' ";
if ($grado != "")
	$sql.="AND $profasig.grado = '$grado' and $profasig.curso = '$curso' ";
$sql.="AND asignaturas.id_asignatura = $profasig.id_asignatura ".
"order by $profasig.profesor, sign($profasig.grado), cast(SUBSTRING_INDEX($profasig.grado, '-', 1) as decimal), $profasig.curso, asignaturas.asignatura";
$datasig=viewsql($sql);
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex" />
<meta name="googlebot" content="noindex" />
<title>Educa</title>
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" href="estilos.css" />
<link rel="stylesheet" href="estilosmenu.css" />
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	var ano = document.form0.ano.value;
	var id_profesor = document.form0.id_profesor.value;
	var pos = document.form0.gradocurso.value.indexOf('*');
	var grado = document.form0.gradocurso.value.substr(0,pos);
	var curso = document.form0.gradocurso.value.substr(pos+1);
	var theURL = 'notas.php?ano='+ano+'&id_profesor='+id_profesor+'&grado='+grado+'&curso='+curso;
	window.location.href = theURL;
}
</script>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="1">
        <td></td>
        <td width="1000"></td>
        <td></td>
    </tr>
    <tr height="120">
        <td background="fondos/fondolat1.jpg">&nbsp;</td>
	<td width="1000" align="center"><?php include("includes/encabezado.php");?></td>
	<td background="fondos/fondolat2.jpg">&nbsp;</td>
    </tr>
    <tr height="12">
        <td background="fondos/marco_top.gif"></td>
        <td width="1000" background="fondos/marco_top.gif"></td>
        <td  background="fondos/marco_top.gif"></td>
    </tr>
    <tr>
        <td></td>
	<td align = "center" bgcolor="#FFFFFF"><?php include("includes/menu_principal.php");?></td>
	<td></td>
    </tr>
    <tr height="535">
        <td></td>
	<td valign="top"  bgcolor="#FFFFFF">
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="80">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10">
				</td>
				<td>
					<div class="div_marco">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr height="50">
							<td width="50" align="center"><img src='images/notas70.png' width="50" height="50" border="0" align="absmiddle"></td>
							<td class="titulo">Planillas Notas</td>
							<td align="right">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>&nbsp;&nbsp;<a class="link_black" href="notas_periodos.php?ano=<?php echo $ano;?>" onclick="newWindow(this.href, '', 930, 500, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Tabla de Peri�dos..."><img src="images/periodos48.png" width="48" height="48" border="0" hspace="0" align="absmiddle">Peri&oacute;dos</a>&nbsp;&nbsp;</td>
										<td width="1" bgcolor="#FFCC00"></td>
										<td>&nbsp;&nbsp;<a class="link_black" href="alumnos_cursos.php?ano=<?php echo $ano;?>&grado=<?php echo $grado;?>" onclick="newWindow(this.href, '', 930, 500, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Cursos..."><img src="images/cursos48.png" width="48" height="48" border="0" hspace="0" align="absmiddle">Cursos</a>&nbsp;&nbsp;</td>
										<td width="1" bgcolor="#FFCC00"></td>
										<td>&nbsp;&nbsp;<a class="link_black" href="horarios.php?ano=<?php echo $ano;?>&grado=<?php echo $grado;?>" onclick="newWindow(this.href, '', 1024, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Horarios..."><img src="images/horarios48.png" width="48" height="48" border="0" hspace="0" align="absmiddle">Horarios</a>&nbsp;&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					</div>
                                        <br>
					<div class="menu_tablas">
                                        <table border="0" cellpadding="0" cellspacing="0" width="100%">
					<form action="" method="post" name="form0">
						<tr height="32">
							<td align="right">
								<select name="id_profesor" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
								<option value="" selected>Docente...</option>
								<?php
								foreach($datprof as $regprof)
								{
									if ($regprof->id_profesor==$id_profesor)
									echo "<option value='$regprof->id_profesor' selected>$regprof->profesor</option>";
									else
									{
									if ($_SESSION["usutip"]!="DOCENTE" or $director==1)
										echo "<option value='$regprof->id_profesor'>$regprof->profesor</option>";
									}
								}
								?>																
								</select>
								&nbsp;&nbsp;
								<select name="gradocurso" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
								<option value="" selected>Grado y Curso...</option>
								<?php
								foreach($datcurs as $regcurs)
								{
									if ($regcurs->grado==$grado and $regcurs->curso==$curso)
									echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $regcurs->gradonom - CURSO $regcurs->curso</option>";
									else
									echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $regcurs->gradonom - CURSO $regcurs->curso</option>";
								}
								?>																
								</select>
								&nbsp;
								<select name="ano" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
								<?php
								$sql="SELECT ano FROM anoslect ORDER BY ano desc";
								$datanol=viewsql($sql);
								foreach($datanol as $reganol)
								{
									$selano = "";
									if ($reganol->ano==$ano)
										$selano = "selected";
									echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
								}
								?>
								</select>
								&nbsp;
								<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
							</td>
						</tr>
					</form>
					</table>
                                        </div>
					<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="100%">
						<tr height="20">
							<th rowspan="2" width="20"></th>
							<th rowspan="2">Docente</th>
							<th rowspan="2">Curso</th>
							<th rowspan="2" width="150">Asignatura</th>
							<th colspan="<?php echo $numperi;?>" align="left"> &nbsp;Planillas de Notas</th>
						</tr>
						<tr>
							<?php
							foreach($datperi as $regperi)
							{echo "<th width='52' title='$regperi->pernomb'>$regperi->perplan</th>";}					
							?>
						</tr>
						<?php
						//declarar arreglo estados de periodos de la asignatura
						$asigper = array();
						//listar asignaturas
						foreach($datasig as $regasig)
						{
						$id_asignatura = $regasig->id_asignatura;
						$id_profasig = $regasig->id_profasig;
						$grado = $regasig->grado;
						$curso = $regasig->curso;
						$id_profesor = $regasig->id_profesor;
						$profesor = $regasig->profesor;
						$seccion = "seccion".$id_profasig;
						//limpiar arreglo de estados
						for($i=0; $i<=$numperi ;$i++)
						{$asigper[$i] = "";}
						$asigper[0] = "CERRADA";
						//traer estados planillas
						$sql = "select * from $planillas where grado='$grado' and curso='$curso' and id_asignatura = '$id_asignatura' order by id_planilla";
						$datplan=viewsql($sql);
						foreach($datplan as $regplan)
						{
							if ($regplan->pertipo == "P") //periodo acad�mico
							{
								if ($regplan->periodo == 1)
									$asigper[1] = $regplan->estado;
								if ($regplan->periodo == 2)
									$asigper[2] = $regplan->estado;
								if ($regplan->periodo == 3)
									$asigper[4] = $regplan->estado;
								if ($regplan->periodo == 4)
									$asigper[5] = $regplan->estado;
							}
							else //examenes
							{
								if ($regplan->periodo == 1)
									$asigper[3] = $regplan->estado;
								if ($regplan->periodo == 2)
									$asigper[6] = $regplan->estado;
								if ($regplan->periodo == 3)
									$asigper[7] = $regplan->estado;
							}
						}						
						?>		
						<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
							<td align="center" bgcolor="<?php echo $regasig->gradocolor;?>" class="titulo_campos"><font color="<?php echo $regasig->gradofont;?>"><?php echo $regasig->grado;?>&deg;</font></td>
							<td><?php echo substr($profesor,0,28);?></td>
							<td align="center"><b><?php echo $regasig->curso;?></b></td>
							<td title="<?php echo $regasig->asignatura;?>"><?php echo substr($regasig->asignatura,0,25);?></td>
							<?php
							foreach($datperi as $regperi)
							{
								$notanum = $regperi->notanum;
								$pertipo = $regperi->pertipo;
								$periodo = $regperi->periodo;
								$perplan = $regperi->perplan;
								$estado = $asigper[$notanum]; //estado planilla
								if (trim($estado)=="" and $regperi->estado=="ABIERTO")
									$estado = "ABIERTA";
								$color = "#000000";
								if ($estado!="CERRADA")
									$color = estado_color("PLANILLAS",$estado);
								?>
								<td><a class="link_small" name="<?php echo $seccion;?>" href="notas_planilla.php?id_profasig=<?php echo $id_profasig;?>&id_asignatura=<?php echo $id_asignatura;?>&periodo=<?php echo $periodo;?>&pertipo=<?php echo $pertipo;?>"><font color="<?php echo $color;?>"><?php echo $estado;?></font></a></td>
								<?php
							}				
							?>
						</tr>
						<?php } ?>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" valign="top">
            <div id="fondo_pie">
            <?php include("includes/menu_bottom.php");?>
            </div>
        </td>
    </tr>
</table>
</body>
</html>
<?php disconnect();?>