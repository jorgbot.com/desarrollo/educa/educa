<?php
/***********************************************************************************************
* @file        : setup_anoslectedit.php                                                        *
* @brief       : Edita a�o lectivo                                                             *
* @author      : Nora Rodriguez 12-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 06-nov-2018                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_setup.php");
include("libs/fechas.php");
include("libs/image_resize.php");
$op = isset($_GET['op']) ? $_GET['op'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$fecha_hora = date('Y-m-d') . " " . date('H:i:s');
if (intval($ano) > 0)
	$nuevo = 0;
else
	$nuevo = 1;
connect();
$titulo = "NUEVO A�O LECTIVO";
if ($ano > 0)
	$titulo = "EDITAR A�O LECTIVO";
$datanolect = datos_anolect($ano);
//esta opcion ajustar tablas y  examenes finales para manejo de notas parciales con examenes
if ($op=="ajustar" and $ano > 0)
{
	@set_time_limit( 0 );
	ini_set('max_execution_time', 0); //600 seconds = 10 minutes
	include("database/grabar_notas.php");
	$notas = "y".$ano."notas";
	$notasfa = "y".$ano."notasfa";
	$cursos = "y".$ano."cursos";
	//borrar campo no necesario (nota definitiva)
	$sql = "ALTER TABLE $notas DROP `notaDef`";
	execsql($sql);
	//a�adir nuevos campos
	$notasp1 = "y".$ano."notasp1";
	$sql = "ALTER TABLE $notasp1 ADD `numEx` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'numero examen ajuste' AFTER notaEx";
	execsql($sql);
	$notasp2 = "y".$ano."notasp2";
	$sql = "ALTER TABLE $notasp2 ADD `numEx` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'numero examen ajuste' AFTER notaEx";
	execsql($sql);
	$notasp3 = "y".$ano."notasp3";
	$sql = "ALTER TABLE $notasp3 ADD `numEx` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'numero examen ajuste' AFTER notaEx";
	execsql($sql);
	$notasp4 = "y".$ano."notasp4";
	$sql = "ALTER TABLE $notasp4 ADD `numEx` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'numero examen ajuste' AFTER notaEx";
	execsql($sql);
	$sql = "ALTER TABLE $notas ADD `notaExF` decimal( 5, 1 ) NOT NULL DEFAULT '0.0' COMMENT 'nota asignatura ajustada examen final' AFTER notaEx";
	execsql($sql);
	$sql = "ALTER TABLE $notasfa ADD `notaExF` decimal( 5, 1 ) NOT NULL DEFAULT '0.0' COMMENT 'nota area ajustada examen final' AFTER notaEx";
	execsql($sql);
	//ajustar campos examen superacion
	$sql = "UPDATE $notas SET pertipo='E', periodo='4' WHERE pertipo='S'";
	execsql($sql);
	$sql = "UPDATE $notas SET notaEx='0'";
	execsql($sql);
	$sql = "UPDATE $notasfa SET notaEx='0'";
	execsql($sql);
	//nota limite a�o lectivo
	$notalim = 65;
	$sql = "select notalim from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{$notalim = intval($reganol->notalim);}
	//traer asignaturas y alumnos por grados
	$sql = "SELECT grado FROM grados order by id_grado";
	$datgrad = viewsql($sql);
	foreach($datgrad as $reggrad)
	{
		$grado = $reggrad->grado;
		//asignaturas del grado que requieren notas
		$sql="SELECT id_asignatura, id_area FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' ORDER BY id_area, id_asignatura";
		$datasig=viewsql($sql);
		//alumnos del grado
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' ORDER BY id_alumno";
		$datalum=viewsql($sql);
		foreach($datalum as $regalum)
		{
			$id_alumno = $regalum->id_alumno;
			$tipoedu = $regalum->tipoedu;
			//ajustar logros y promediar asignaturas alumno
			foreach($datasig as $regasig)
			{
				$id_asignatura = $regasig->id_asignatura;
				//traer nota examen del periodo
				$sql = "select periodo, nota from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = 'E' ORDER BY periodo";
				$datexa = viewsql($sql);
				foreach($datexa as $regexa)
				{
					$periodo = $regexa->periodo;
					$examen = $regexa->nota;
					ajustar_logros($ano, $periodo, $id_asignatura, $id_alumno, $tipoedu, $examen, $notalim);
				}
				//promediar asignatura 4 periodos
				for($i=1; $i<=4 ;$i++) //periodos de notas
				{
					$notanw = promediar_asignatura($ano, $id_alumno, $id_asignatura, $i, $tipoedu, $notalim);
				}
			}
			//promediar areas del alumno
			$id_area = 0;
			foreach($datasig as $regasig)
			{
				if ($regasig->id_area != $id_area)
				{
					$id_area = $regasig->id_area;
					grabar_notaarea($ano, $grado, $id_area, $id_alumno, 1);
					grabar_notaarea($ano, $grado, $id_area, $id_alumno, 2);
					grabar_notaarea($ano, $grado, $id_area, $id_alumno, 3);
					grabar_notaarea($ano, $grado, $id_area, $id_alumno, 4);
				}
			}
		} //fin alumnos
	} //fin grados
	echo"<script>alert('PROCESO REPARACION NOTAS FINALIZADO, A�O $ano');window.close();</script>";
}
//esta opcion ajustar tablas para manejo de evidencias en notas
if ($op=="ajuste_evidencias" and $ano > 0)
{
	@set_time_limit( 0 );
	ini_set('max_execution_time', 0); //600 seconds = 10 minutes
        include("database/iniciar_anolectivo.php");
	$logros = "y".$ano."logros";
	$sql = "ALTER TABLE $logros ADD `evidemin` int( 2 ) NOT NULL COMMENT 'cantidad evidencias minimas' AFTER tipoedu";
	execsql($sql);
	$sql = "ALTER TABLE $logros ADD `evidepor` varchar( 50 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'porcentajes evidencias' AFTER evidemin";
	execsql($sql);
        $activ = "y".$ano."activ";
	$sql = "ALTER TABLE $activ ADD `id_evidencia` int( 6 ) NOT NULL COMMENT 'id evidencia asociada' AFTER id_logro";
	execsql($sql);
	$notas = "y".$ano."notasp1";
	$sql = "ALTER TABLE $notas ADD `id_evidencia` int( 6 ) NOT NULL COMMENT 'id evidencia asociada' AFTER id_logro";
	execsql($sql);
	$notas = "y".$ano."notasp2";
	$sql = "ALTER TABLE $notas ADD `id_evidencia` int( 6 ) NOT NULL COMMENT 'id evidencia asociada' AFTER id_logro";
	execsql($sql);
	$notas = "y".$ano."notasp3";
	$sql = "ALTER TABLE $notas ADD `id_evidencia` int( 6 ) NOT NULL COMMENT 'id evidencia asociada' AFTER id_logro";
	execsql($sql);
	$notas = "y".$ano."notasp4";
	$sql = "ALTER TABLE $notas ADD `id_evidencia` int( 6 ) NOT NULL COMMENT 'id evidencia asociada' AFTER id_logro";
	execsql($sql);
        crear_evidencias($ano);
}
if ($op=="repetidas" and $ano > 0)
{
	$sql="select grado from grados order by id_grado";
	$datgrado = viewsql($sql);
	foreach($datgrado as $reggrado)
	{
		$grado = $reggrado->grado;
		$sql="SELECT id_alumno FROM $cursos WHERE grado = '$grado' ORDER BY id_alumno";
		$datalum=viewsql($sql);
		$sql="SELECT id_asignatura FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' ORDER BY id_area, id_asignatura";
		$datasig=viewsql($sql);
		foreach($datasig as $regasig)
		{
			$id_asignatura = $regasig->id_asignatura;
			foreach($datalum as $regalum)
			{
				$id_alumno = $regalum->id_alumno;
				//borrar notas repetidas de un periodo y asignatura
				$periodo = "";
				$sql="select id_nota, periodo from $notas where id_alumno='$id_alumno' and id_asignatura='$id_asignatura' order by id_nota desc";
				$datnot=viewsql($sql);
				foreach($datnot as $regnot)
				{
					if ($regnot->periodo == $periodo)
					{
						$id_nota = $regnot->id_nota;
						$sql = "delete from $notas where id_nota = '$id_nota' limit 1";
						execsql($sql);
					}
					else
						$periodo = $regnot->periodo;
				}
			}
		}
	}
	echo"<script>alert('PROCESO ELIMINACION NOTAS REPETIDAS FINALIZADO, A�O $ano');window.close();</script>";
}
//asignar cursos nuevo a�o
if ($op=="cursos" and $ano > 0)
{
	include("database/iniciar_anolectivo.php");
	$sql="SELECT grado FROM grados ORDER BY id_grado";
	$datgrado=viewsql($sql);
	foreach($datgrado as $reggrado)
	{
		$grado = $reggrado->grado;
		asignar_cursos($ano, $grado);
	}
	echo"<script>alert('Alumnos asignados en cursos a�o $ano...');window.close();</script>";
}
//asignar fotos en lote
if ($op=="cargar_fotos" and $ano > 0)
{
    $sql="SELECT id_alumno, foto FROM alumnos ORDER BY id_alumno";
    $datalum=viewsql($sql);
    foreach($datalum as $regalum)
    {
        $id_alumno = $regalum->id_alumno;
        $oldfoto = $regalum->foto;
        //borrar archivo foto anterior
        //$oldfoto = ROOT_DIR . "/" .$oldfoto;
        //if (file_exists($oldfoto))
        //{@unlink($oldfoto);}
        //cargar nueva foto con id alumno y fecha hora minutos segundos
        $nwfecha = str_replace ( ":" , "" , $fecha_hora );
        $nwfecha = str_replace ( "-" , "" , $nwfecha );
        $nwfecha = str_replace ( " " , "" , $nwfecha );
        $foto = "fotoalu" . $id_alumno."-".$nwfecha;
        $foto = "fotos/" . $foto . ".JPG";
        $nwfoto = ROOT_DIR . "/" . $foto;
        $oldfoto = ROOT_DIR."/fotos" .$ano."/".$id_alumno.".JPG";
        if (rename($oldfoto, $nwfoto)) {
            resize_image($rutafoto,150,150); 
        } else {
                $foto = "";
        }
        $sql = "update alumnos set foto='$foto' where id_alumno='$id_alumno' limit 1";
        execsql($sql);
    }
    echo"<script>alert('Fotos de Alumnos cargadas a�o $ano...');window.close();</script>";
}

//periodos del a�o
$estadoper="";
$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
$datperi = viewsql($sql);
foreach($datperi as $regperi)
{
	if ($regperi->pertipo == "P" and $regperi->periodo == 1)
		$estadoper=$regperi->estado;
}
$chkevi = "";
if ($datanolect["notaevi"] > 0)
	$chkevi = "checked";
$chkprintevi = "";
if ($datanolect["printevi"] > 0)
	$chkprintevi = "checked";

?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/validInput.js'></script>
<script language="javascript">
function validar()
{
	if(document.form1.fchmatpre.value == '')
	{alert('Falta fecha matriculas preescolar...');document.form1.fchmatpre.focus(); return false;}
	if(document.form1.fchmatpri.value == '')
	{alert('Falta fecha matriculas primaria...');document.form1.fchmatpri.focus(); return false;}
	if(document.form1.fchmatbac.value == '')
	{alert('Falta fecha matriculas bachillerato...');document.form1.fchmatbac.focus(); return false;}
	if(document.form1.protocolo.value == '')
	{alert('Falta texto Protocolizaci�n...');document.form1.protocolo.focus(); return false;}
	if(document.form1.nuevo.value > 0)
	{
		ok=confirm("Esta opci�n crea los archivos del a�o e inicia los par�metros generales,  Esta seguro de Continuar?");
		if(ok)
			document.form1.submit();
		else
			return false;
	}
	else
		return true;
}
function activarconf()
{
	ok=confirm("Desea activar el a�o en su sesi�n de usuario?");
	if(ok)
	{
		document.form1.op.value='activaranolect';
		document.form1.submit();
	}
}
function periodos()
{
	ok=confirm('Esta opci�n crea los registros de los periodos acad�micos, desea continuar?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		window.location.href='database/grabar_setup.php?op=periodos&ano='+ano;
	}
}
function cursos()
{
	ok=confirm('Esta opci�n crea y asigna los alumnos en los Cursos por Grado, desea continuar?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		window.location.href='setup_anoslectedit.php?op=cursos&ano='+ano;
	}
}
function matriculas()
{
	ok=confirm('Esta opci�n actualiza o crea los registros de Matr�culas, desea continuar?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		window.location.href='database/grabar_setup.php?op=matriculas&ano='+ano;
	}
}
function contratos()
{
	ok=confirm('Esta opci�n actualiza o crea los contratos de Docentes, desea continuar?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		window.location.href='database/grabar_setup.php?op=contratos&ano='+ano;
	}
}
function foliar()
{
	ok=confirm('Esta opci�n ejecuta la foliaci�n de Matr�culas, desea continuar?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		window.location.href='database/grabar_setup.php?op=foliar&ano='+ano;
	}
}
function pensiones()
{
	ok=confirm('Esta opci�n modifica y crea tablas para manejo de pensiones y pagos, desea continuar?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		window.location.href='database/grabar_ajustes.php?op=pensiones&ano='+ano;
	}
}
function importarTransp()
{
	ok=confirm('Esta opci�n importa transporte anual de alumnos, desea continuar?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		window.location.href='libs/impo_transp.php?impano='+ano;
	}
}
function importarPagos()
{
	ok=confirm('Esta opci�n importa los pagos de pensiones y matriculas, desea continuar?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		window.location.href='libs/impo_pagos.php?impano='+ano;
	}
}
function evidencias()
{
	ok=confirm('Esta opci�n crea y ajusta tablas para manejo de EVIDENCIAS, desea continuar?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		window.location.href='setup_anoslectedit.php?op=ajuste_evidencias&ano='+ano;
	}
}
function cargarFotos()
{
       var ano = document.form1.ano.value;
	ok=confirm('Esta opci�n carga las fotos de alumnos desde la carpeta (fotos'+ano+') , desea continuar?');
	if(ok)
	{
		window.location.href='setup_anoslectedit.php?op=cargar_fotos&ano='+ano;
	}
}
function cargarAjustes()
{
       var ano = document.form1.ano.value;
	ok=confirm('Esta opci�n carga los ajustes de pensiones en la tabla Ajustes , desea continuar?');
	if(ok)
	{
		window.location.href='database/grabar_setup.php?op=Ajustes_Pension&ano='+ano;
	}
}
</script>
</head>
<body>
<table width="90%" border="0" cellpadding="0" cellspacing="0" align="center">
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
				<tr height="50">
					<td class="titulo"><img src="images/idicestrella.png" border="0" width="24" height="24" align="absmiddle" hspace="1"> <?php echo $titulo;?></td>
					<td class="titulo" align="right"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td>
			<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#EEF0F0" class="tabla_marco">
			<form action="database/grabar_setup.php" method="post" name="form1" onSubmit="return validar();">
			<input type="hidden" name="op" value="grabaranolect">
			<input type="hidden" name="ano" value="<?php echo $datanolect["ano"];?>">
			<input type="hidden" name="nuevo" value="<?php echo $nuevo;?>">
				<tr height="30">
					<th width="10" background="images/backsilver.gif"></th>
					<th background="images/backsilver.gif" class="titulo" align="left">A�O <?php echo $datanolect["ano"];?></th>
					<th width="10" background="images/backsilver.gif"></th>
					<th width="160" background="images/backsilver.gif" align="right">
						<?php if ($datanolect["estado"] == "ABIERTO") { ?>
						ABIERTO <input type="radio" name="estado" value="ABIERTO" checked style="border: 1px solid #006600;">
						&nbsp;
						CERRADO <input type="radio" name="estado" value="CERRADO" style="border: 1px solid #006666;">
						<?php } else { ?>
						ABIERTO <input type="radio" name="estado" value="ABIERTO" style="border: 1px solid #006600;">
						&nbsp;
						CERRADO <input type="radio" name="estado" value="CERRADO" checked style="border: 1px solid #006666;">
						<?php } ?>
						&nbsp;
					</th>
					<th width="10" background="images/backsilver.gif"></th>
				</tr>
				<tr><td colspan="5" height="15"></td></tr>
				<tr valign="top">
					<td></td>
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<fieldset>
									<legend class="titulo_campos">Notas</legend>
									<table border="0" cellpadding="1" cellspacing="1" width="100%">
										<tr height="10"><td colspan="2"></td></tr>
										<tr>
											<td>Nota M�nima </td>
											<td><input type="text" name="notamin" value="<?php echo $datanolect["notamin"];?>" style="width: 30px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
										<tr>
										<tr>
											<td>Nota M�xima </td>
											<td><input type="text" name="notamax" value="<?php echo $datanolect["notamax"];?>" style="width: 30px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
										<tr>
										<tr>
											<td>Nota L�mite </td>
											<td><input type="text" name="notalim" value="<?php echo $datanolect["notalim"];?>" style="width: 30px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
										<tr>
										<tr height="10"><td colspan="2"></td></tr>
									</table>
									</fieldset>
								</td>
								<td width="10"></td>
								<td>
									<fieldset>
									<legend class="titulo_campos">P�rdida</legend>
									<table border="0" cellpadding="1" cellspacing="1" width="100%">
										<tr height="10"><td colspan="2"></td></tr>
										<tr title="Cantidad de �reas perdidas para p�rdida de a�o">
											<td>Areas p�rdida de a�o</td>
											<td><input type="text" name="areasper" value="<?php echo $datanolect["areasper"];?>" style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
										<tr>
										<tr title="Perdida de asignaturas por fallas seg�n el porcentaje">
											<td>Asignatura p�rdida fallas %</td>				
											<td><input type="text" name="fallasper" value="<?php echo $datanolect["fallasper"];?>" style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
										<tr>
										<tr title="Porcentaje de Inter�s por mora en pensiones">
											<td>Inter�s mora pensi�n %</td>				
											<td><input type="text" name="interes" value="<?php echo $datanolect["interes"];?>" style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
										<tr>
										<tr height="10"><td colspan="2"></td></tr>
									</table>
									</fieldset>
								</td>
								<td width="10"></td>
								<td>
									<fieldset>
									<legend class="titulo_campos">Matr�culas</legend>
									<table border="0" cellpadding="1" cellspacing="1" width="100%">
										<tr height="10"><td colspan="2"></td></tr>
										<tr>
											<td>Preescolar </td>
											<td><input type="text" name="fchmatpre" id="fchmatpre" value="<?php echo $datanolect["fchmatpre"];?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchmatpre, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
										</tr>
										<tr>
											<td>Primaria</td>
											<td><input type="text" name="fchmatpri" id="fchmatpri" value="<?php echo $datanolect["fchmatpri"];?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchmatpri, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
										</tr>
										<tr>
											<td>Bachillerato</td>
											<td><input type="text" name="fchmatbac" id="fchmatbac" value="<?php echo $datanolect["fchmatbac"];?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchmatbac, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
										</tr>
										<tr height="10"><td colspan="2"></td></tr>
									</table>
									</fieldset>
								</td>
							</tr>
							<tr>
								<td colspan="5">
									<fieldset>
									<legend class="titulo_campos">Evidencias</legend>
									<table border="0" cellpadding="1" cellspacing="1" width="100%">
										<tr height="1"><td></td></tr>
                                                                                <tr><td>Manejo de Evidencias en Logros <input type="checkbox" name="chkevi" <?php echo $chkevi;?>> &nbsp; Imprimir Evidencias en Boletin <input type="checkbox" name="chkprintevi" <?php echo $chkprintevi;?>></td><tr>
										<tr height="1"><td></td></tr>
									</table>
									</fieldset>
                                                                        <fieldset>
									<legend class="titulo_campos">Protocolizaci�n</legend>
									<table border="0" cellpadding="1" cellspacing="1" width="100%">
										<tr height="1"><td></td></tr>
										<tr><td><input type="text" name="protocolo" value="<?php echo $datanolect["protocolo"];?>" style="width: 440px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td><tr>
										<tr height="1"><td></td></tr>
									</table>
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
					<td></td>
					<td width="170">
						<?php if(strtoupper($_SESSION["nivpar"])>=4 and $nuevo==0) { ?>
						<fieldset>
						<legend class="titulo">PROCESOS</legend>
						<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<tr height="5"><td></td></tr>
							<?php if ($estadoper!="CERRADO") { ?>
							<tr height="25">
								<td title="Cargar las matr�culas del a�o seg�n resultados acad�micos del a�o anterior."><a class="link_black" href="javascript:;" onClick="matriculas();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Iniciar Matr�culas </a></td>
							<tr>
							<tr height="25">
								<td title="Cargar contratos del a�o anterior para docentes activos."><a class="link_black" href="javascript:;" onClick="contratos();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Iniciar Docentes </a></td>
							<tr>
							<tr height="25">
								<td title="Cargar los alumnos a la tabla cursos asign�ndolos aleatoriamente por g�nero y nombre para cada grado, seg�n las matr�culas cerradas."><a class="link_black" href="javascript:;" onClick="cursos();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Asignar Cursos</a></td>
							<tr>
							<?php } ?>
							<tr height="25">
								<td title="Asignar n�mero de Folio a las matr�culas por consecutivo autom�tico para cada nivel acad�mico."><a class="link_black" href="javascript:;" onClick="foliar();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Foliar Matr�culas</a></td>
							<tr>
							<?php if ($ano <= 2015) { ?>
							<tr height="25">
								<td title="Crear tablas para pensiones, transportes y pagos"><a class="link_black" href="javascript:;" onClick="pensiones();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Crear Tablas Pensiones</a></td>
							<tr>
							<tr height="25">
								<td title="Importar Transporte Anual de Alumnos"><a class="link_black" href="javascript:;" onClick="importarTransp();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Importar Transporte</a></td>
							<tr>
							<tr height="25">
								<td title="Importar pagos Pensiones, Matriculas, Transporte"><a class="link_black" href="javascript:;" onClick="importarPagos();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Importar Pagos</a></td>
							<tr>
							<?php } ?>
							<?php if ($ano <= 2018) { ?>
							<tr height="25">
								<td title="Crear tablas para evidencias"><a class="link_black" href="javascript:;" onClick="evidencias();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Crear Evidencias</a></td>
							<tr>
							<tr height="25">
								<td title="Cargar ajustes pensiones en lote en tabla de Ajustes"><a class="link_black" href="javascript:;" onClick="cargarAjustes();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Ajustes en Pensiones</a></td>
							<tr>
							<?php } ?>
							<tr height="25">
								<td title="Cargar fotos en lote en tabla de Alumnos"><a class="link_black" href="javascript:;" onClick="cargarFotos();"><img src="images/sumar.gif" border="0" width="20" height="20" hspace="2" align="absmiddle"> Cargar Fotos</a></td>
							<tr>
                                                        <tr height="5"><td></td></tr>
						</table>
						</fieldset>
						<?php } ?>
					</td>
					<td></td>
				</tr>
				<tr><td colspan="5" height="15"></td></tr>
				<tr height="35" valign="middle">
					<td colspan="5" bgcolor="#CCCCCC" background="images/backsilver.gif" align="right">
						<?php if ($_SESSION["nivpar"] >= 2) { ?>
						<input type="image" class="botones_img" name="grabar" src="images/btn_grabar.png" width="70" height="24" border="0" alt="Grabar" align="absmiddle" hspace="20">
						<?php } if ($_SESSION["nivpar"] >= 2 and $datanolect["estado"] == "ABIERTO" and $nuevo==0 and $ano != $_SESSION["anoaca"]) { ?>
						<a href="javascript:;" onClick="activarconf();" title="Activar A�o en Sesi�n"><img src="images/btn_activar.png" border="0" width="75" height="24" alt="Activar A�o en Sesi�n" hspace="20" align="absmiddle"></a>
						<?php } ?>
						<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="20" align="absmiddle"></a>
					</td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
	<?php if ($nuevo==1) { ?>
	<tr height="30"><td class="titulo_error" align="center">*** Nuevo a�o crea las tablas anuales e inicia los par�metros generales ***</td></tr>
	<?php } ?>
</table>
</body>
</html>
<?php disconnect();?>