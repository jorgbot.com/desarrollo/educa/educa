<?php
/***********************************************************************************************
* @file        : alumnos_select.php                                                            *
* @brief       : Lista alumnos para selecci�n  (debe permitir de todos los a�os)               *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
connect();
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$buscar = isset($_POST['buscar']) ? $_POST['buscar'] : null ;
$ano = $_SESSION["anoaca"];
//traer alumnos
$numalum = 0;
if ($buscar!="" or $grado!="")
{
	$sql="SELECT alumnos.id_alumno, alumnos.apellidos, alumnos.nombres, alumnos.docunum, alumnos.estado, alumnos.fchnace, MAX(matriculas.ano) AS ano, matriculas.grado, matriculas.curso FROM alumnos, matriculas ".
	"WHERE alumnos.id_alumno = matriculas.id_alumno ";
	if ($buscar != "")
	{
		if ($buscar > 0)
			$sql.="AND alumnos.id_alumno = '$buscar' ";
		else
			$sql.="AND (alumnos.apellidos like '$buscar%' or alumnos.nombres like '$buscar%') ";
	}
	else
	{
		if ($grado != "")
			$sql.="AND matriculas.grado = '$grado' ";
		if ($curso != "")
			$sql.="AND matriculas.curso = '$curso' ";
	}
	$sql.= "GROUP BY matriculas.id_alumno ORDER BY alumnos.apellidos, alumnos.nombres, matriculas.ano DESC";
	$datalum=viewsql($sql);
	$numalum = count($datalum);
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	var pos = document.form0.gradocurso.value.indexOf('*');
	var grado = document.form0.gradocurso.value.substr(0,pos);
	var curso = document.form0.gradocurso.value.substr(pos+1);
	var theURL = 'alumnos_select.php?grado='+grado+'&curso='+curso;
	window.location.href = theURL;
}
function devuelve(campo1,campo2)
{
	eval ("opener.document.form1.id_alumno.value='" +  campo1 + "'")
	eval ("opener.document.form1.alumno.value='" +  campo2 + "'")
	window.close()
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="40">
		<td class="titulo"><img src='images/alumno.jpg' width="40" height="40" border="0" align="absmiddle"> Seleccionar Alumno</td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
<form action="" method="post" name="form0">
<input type="hidden" name="op" value="buscar">
	<tr><td colspan="3" height="15"></td></tr>
	<tr height="32">
		<td width="6" background="images/backsilver401.gif"></td>
		<td align="right" background="images/backsilver402.gif">
			BUSCAR...
			<input type="text" name="buscar" value="<?php echo $buscar;?>" size="20" style="text-transform:uppercase;">
			&nbsp;
			<select name="gradocurso" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
			<option value="" selected>Grado y Curso...</option>
			<?php
			$sql = "SELECT grado, gradonom FROM grados order by id_grado";
			$datgrad = viewsql($sql);
			foreach($datgrad as $reggrad)
			{
				$lgrado = $reggrad->grado;
				$lcurso = "";
				if ($lgrado==$grado and $lcurso==$curso)
					echo "<option value='$lgrado*$lcurso' selected>GRADO $reggrad->gradonom</option>";
				else
					echo "<option value='$lgrado*$lcurso'>GRADO $reggrad->gradonom</option>";
				$sql = "SELECT grado, curso FROM cursos WHERE ano = '$ano' AND grado = '$lgrado' order by curso";
				$datcurs = viewsql($sql);
				foreach($datcurs as $regcurs)
				{
					if ($regcurs->grado==$grado and $regcurs->curso==$curso)
					echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
					else
					echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
				}
			}
			?>																
			</select>
			&nbsp;
			<select name="ano" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
			<?php
			for($i=2011; $i<=$ano+1 ;$i++)
			{
				$selano = "";
				if ($i==$ano)
					$selano="selected";
				echo "<option value='$i' $selano>$i</option>";
			}
			?>
			</select>
			&nbsp;
			<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
			&nbsp;
		</td>
		<td width="6" background="images/backsilver403.gif"></td>
	</tr>
</form>
</table>
<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="95%" align="center">
	<tr height="20">
		<th>Apellidos y Nombres</th>
		<th width="30">IdAlum</th>
		<th width="80">Estado</th>
		<th width="30">Edad</th>
		<th width="30">A�o</th>
		<th width="30">Grado</th>
		<th width="30">Curso</th>
	</tr>
	<?php
	if ($numalum > 0)
	{
	foreach($datalum as $regalum)
	{
	$id_alumno = $regalum->id_alumno;
	$alumno = trim($regalum->apellidos)." ".trim($regalum->nombres);
	$edad = calculo_edad($regalum->fchnace);
	?>		
	<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
		<td><a href="javascript:;" onClick="devuelve('<?php echo $id_alumno;?>','<?php echo $alumno;?>');"><img src="images/ok.png" border="0" width="16" height="16" alt="Seleccionar" title="" hspace="0" align="absmiddle"><?php echo $regalum->apellidos." ".$regalum->nombres;?></a></td>
		<td><?php echo $regalum->id_alumno;?></td>
		<td><?php echo $regalum->estado;?></td>
		<td align="center"><?php echo $edad;?></td>
		<td align="center"><?php echo $regalum->ano;?></td>
		<td align="center"><b><?php echo $regalum->grado;?></b></td>
		<td align="center"><b><?php echo $regalum->curso;?></b></td>
	</tr>
	<?php } } else { ?>
	<tr><td colspan="7" bgcolor="#FFFFFF" height="100" class="subtitulo" align="center">Asigne una condici�n de b�squeda...</td></tr>
	<?php } ?>
	<tr height="36">
		<td colspan="7" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
</body>
</html>
<?php disconnect();?>
