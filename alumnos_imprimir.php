<?php
/***********************************************************************************************
* @file        : alumnos_imprimir.php                                                          *
* @brief       : Opciones de impresi�n de un alumno                                            *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 26-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 26-Mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("libs/fechas.php");
$id_alumno = $_GET["id_alumno"];
$ano = $_SESSION["anoaca"];
$opcion = "imprimir";
connect();
include("includes/menu_titulo.php");
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript'>
function validar()
{
	if(document.form1.concepto.value == '' && (document.form1.formato[7].checked==true || document.form1.formato[8].checked==true))
	{alert('Debe especificar el concepto...'); document.form1.concepto.focus(); return false;}
	return true;
}

function imprimir()
{
	var pos = document.form1.anogrado.value.indexOf('-');
	var ano = document.form1.anogrado.value.substr(0,pos);
	var grado = document.form1.anogrado.value.substr(pos+1);
	var curso = grado;
	pos = grado.indexOf('*');
	grado = grado.substr(0,pos);
	curso = curso.substr(pos+1);
	var id_alumno = document.form1.id_alumno.value;
	var tipoedu = document.form1.tipoedu.value;
	var id_plantilla = document.form1.id_plantilla.value;
	var consecutivo = document.form1.consecutivo.value;
	var concepto = document.form1.concepto.value;
	var implogo = 'S';
	var periodo = 1;
	var papel = 'LEGAL';
	var theURL= 'reports/rep_boletin.php?';
	if (document.form1.papel[1].checked==true)
		papel = 'LETTER';
	if (document.form1.chklogo.checked == false)
		implogo = '';
	if (document.form1.formato[1].checked==true)
		periodo = 2;
	if (document.form1.formato[2].checked==true)
		periodo = 3;
	if (document.form1.formato[3].checked==true)
		periodo = 4;
	if (document.form1.formato[4].checked==true)
		theURL= 'reports/rep_boletincon.php?formato=FINAL&';
	if (document.form1.formato[5].checked==true)
		theURL= 'reports/rep_boletincon.php?consecutivo='+consecutivo+'&';
	if (document.form1.formato[6].checked==true)
		theURL= 'reports/certificados_notas.php?consecutivo='+consecutivo+'&';
	if (document.form1.formato[7].checked==true)
		theURL= 'reports/diploma.php?concepto='+concepto+'&';
	if (document.form1.formato[8].checked==true)
		theURL= 'reports/diploma_men.php?concepto='+concepto+'&';
	if (document.form1.formato[9].checked==true)
		theURL= 'reports/carnet.php?';
	if (id_plantilla > 0)
		theURL= 'reports/certificados_alum.php?id_plantilla='+id_plantilla+'&id_alumno='+id_alumno+'&ano='+ano+'&consecutivo='+consecutivo+'&implogo='+implogo;
	else
		theURL = theURL+'id_alumno='+id_alumno+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&periodo='+periodo+'&tipoedu='+tipoedu+'&papel='+papel+'&implogo='+implogo;
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
</script>
</head>
<body>
<?php include("includes/menu_alumnos.php");?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="javascript:imprimir();" method="post" name="form1" onSubmit="return validar();">
<input type="hidden" name="id_alumno" value="<?php echo $id_alumno;?>">
<input type="hidden" name="grado" value="<?php echo $grado;?>">
<input type="hidden" name="tipoedu" value="<?php echo $tipoedu;?>">
	<tr><td colspan="3" height="30"></td></tr>
	<tr height="320" valign="top">
		<td width="30"></td>
		<td align="center">
			<table border="0">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<legend class="titulo_campos"><img src="images/planilla.png" border="0" width="28" height="28" align="absmiddle">Formatos</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Bolet�n Periodo I</td></tr>
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Bolet�n Periodo II</td></tr>
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Bolet�n Periodo III</td></tr>
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Bolet�n Periodo IV</td></tr>
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Bolet�n Final de A�o</td></tr>
									</table>
								</td>
								<td width="20"></td>
								<td>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="5"> Consolidado Notas</td></tr>
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="6"> Certificado de Notas</td></tr>
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="7"> Diploma de Honor</td></tr>
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="8"> Menci�n de Honor</td></tr>
										<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="formato" value="9"> Carnet</td></tr>
									</table>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
					<td width="20"></td>
					<td align="center">
						<fieldset>
						<legend class="titulo_campos">Opciones</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr><td height="10"></td></tr>
							<tr>
								<td height="30">&nbsp;&nbsp;
								<select name="anogrado">
								<?php
								$sql="SELECT ano, grado, curso FROM matriculas WHERE id_alumno = '$id_alumno' order by ano desc";
								$datmatr=viewsql($sql);
								foreach($datmatr as $regmatr)
								{
								if ($regmatr->ano == $ano)
								echo "<option value='$regmatr->ano-$regmatr->grado*$regmatr->curso' selected>A&ntilde;o $regmatr->ano Grado $regmatr->grado</option>";
								else
								echo "<option value='$regmatr->ano-$regmatr->grado*$regmatr->curso'>A&ntilde;o $regmatr->ano Grado $regmatr->grado</option>";
								}
								?>
								</select>
								</td>
							</tr>
							<tr><td height="30">&nbsp;&nbsp;<input type="radio" name="papel" value="LEGAL" checked> Papel Oficio &nbsp;&nbsp; <input type="radio" name="papel" value="LETTER"> Papel Carta</td></tr>
							<tr><td height="30">&nbsp;&nbsp;<input type="checkbox" name="chklogo"> Imprimir Logo IDIC</td></tr>
							<tr><td height="10"></td></tr>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td colspan="3">
						<fieldset>
						<legend class="titulo_campos"><img src="images/certificado.png" border="0" width="19" height="28" align="absmiddle">Constancias</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td height="30">
									<select name="id_plantilla" style="width: 360px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<option value="" selected>Seleccionar Formato...</option>
									<?php
									$sql="select id_plantilla, plantilla from plantillas where categoria = 'ALUMNOS' order by plantilla";
									$datplan=viewsql($sql);
									foreach($datplan as $regplan)
									{
										echo "<option value='$regplan->id_plantilla'>$regplan->plantilla</option>";
									}
									?>																
									</select>
									Consecutivo N�
									<input type="text" name="consecutivo" value="" style="width: 80px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
						</table>
						</fieldset>
						<fieldset>
						<legend class="titulo_campos">Concepto Diploma / Menci�n</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td width="30">POR:</td>
								<td><input type="text" name="concepto" style="width: 500px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
			</table>
		</td>
		<td width="30"></td>
	</tr>
	<tr><td colspan="3" height="30"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<input type="image" class="botones_img" src="images/btn_imprimir.png" border="0" name="imprimir"  width="70" height="24" hspace="10" alt="Imprimir">
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
