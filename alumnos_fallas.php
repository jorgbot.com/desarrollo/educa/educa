<?php
/***********************************************************************************************
* @file        : alumnos_fallas.php                                                        *
* @brief       : Lista y edita fallas de un alumno                                             *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 10-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 10-Mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("libs/fechas.php");
$page = isset($_POST['page']) ? $_POST['page'] : null ;
if ($page == "")
	$page = isset($_GET['page']) ? $_GET['page'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
if ($id_alumno == "")
	$id_alumno = isset($_POST['id_alumno']) ? $_POST['id_alumno'] : null ;
$fecha = isset($_GET['fecha']) ? $_GET['fecha'] : null ;
if ($fecha == "")
	$fecha = isset($_POST['fecha']) ? $_POST['fecha'] : null ;
$op = isset($_GET['op']) ? $_GET['op'] : "" ;
if ($op == "")
	$op = isset($_POST['op']) ? $_POST['op'] : null ;
$id_falla = isset($_GET['id_falla']) ? $_GET['id_falla'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] : 0;
if ($ano <= 0) {
    $ano = $_SESSION["anoaca"];
}
if ($op=="") {
    $op = "listar";
}
$opcion = "fallas";
$fallas = "y".$ano."fallas";
$cursos = "y".$ano."cursos";
connect();
include("includes/menu_titulo.php");
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.fecha.value == '' || document.form1.fecha.value.substr(0,2) == '00')
	{alert('Debe seleccionar fecha...');document.form1.fecha.focus(); return false;}
	if(document.form1.fecha.value.substr(6,4) != document.form1.ano.value)
	{alert('Fecha incorrecta, no corresponde al a�o '+document.form1.ano.value+' !');document.form1.fecha.focus(); return false;}
	if(document.form1.id_alumno.value <= 0)
	{alert('Debe seleccionar el Alumno...'); return false;}
	return true;
}
function validarfalla()
{
	if(document.form1.fecha.value == '' || document.form1.fecha.value.substr(0,2) == '00')
	{alert('Debe seleccionar fecha...');document.form1.fecha.focus(); return false;}
	if(document.form1.fecha.value.substr(6,4) != document.form1.ano.value)
	{alert('Fecha incorrecta, no corresponde al a�o '+document.form1.ano.value+' !');document.form1.fecha.focus(); return false;}
	if(document.form1.tipo.value == "P" && document.form1.concepto.value == "")
	{alert('Debe especificar el concepto de permiso...'); document.form1.concepto.focus(); return false;}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar esta Falla?");
	if(ok)
	{
		document.form1.op.value='borrarfalla';
		document.form1.submit();
	}
}
function seleccionar()
{
	var ano = document.form0.ano.value;
	var id_alumno = document.form0.id_alumno.value;
	var theURL = 'alumnos_fallas.php?id_alumno='+id_alumno+'&ano='+ano;
	window.location.href = theURL;
}
</script>
</head>
<body>
<?php
if (substr($page,0,6) == "fallas")
{ ?>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="60"><td class="titulo"><img src='images/alumno.jpg' width="40" height="40" border="0" align="absmiddle"> <?php echo $titulo;?></td></tr>
</table>
<?php
}
else
	include("includes/menu_alumnos.php");
if ($op == "listar")
{
?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="alumnos_fallas.php" method="post" name="form1" onSubmit="return validar();">
<input type="hidden" name="op" value="editar">
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type="hidden" name="tipo" value="I">
	<tr height="420" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="30">
					<th width="110">Fecha</th>
                                        <th width="30">Tipo</th>
					<th width="100">Justificada</th>
					<th width="230">Concepto</th>
					<th valign="top">
						<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr height="30">
								<th align="right">
									<?php if ($_SESSION["nivfal"] >= 3 and $ano==$_SESSION["anoaca"]) { ?>
									Fecha de Novedad <font color="red">*</font>&nbsp;
									<input type="text" name="fecha" id="fecha" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fecha, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									&nbsp;
									<input type="image" class="botones_img" src="images/btn_nuevo.png" border="0" name="nuevo"  width="70" height="24" hspace="10" alt="Nuevo" align="absmiddle" title="Crear Falla">
									<?php } ?>
								</th>
							</tr>
						</table>
					</th>
				</tr>
				<?php
				//traer encabezado de fallas
				$sql="SELECT $fallas.id_falla, $fallas.fecha, $fallas.tipo, $fallas.fchjust, $fallas.detalles, categorias.categoria FROM $fallas left JOIN categorias ON $fallas.id_categoria = categorias.id_categoria WHERE $fallas.id_alumno = '$id_alumno' and $fallas.id_asignatura='0' and ($fallas.tipo='I' or $fallas.tipo='R' or $fallas.tipo='P') order by $fallas.fecha desc";
                                $datfall=viewsql($sql);
				foreach($datfall as $regfall)
				{
				$id_falla = $regfall->id_falla;
				$fecha = $regfall->fecha;
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><a class="link_list" href="alumnos_fallas.php?op=editar&ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>&id_alumno=<?php echo $id_alumno;?>"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo fecha_larga($regfall->fecha);?></a></td>
                                        <td align="center"><?php echo $regfall->tipo;?></td>
					<td><?php echo fecha_larga($regfall->fchjust);?></td>
					<td><?php echo $regfall->categoria;?></td>
					<td><?php echo $regfall->detalles;?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<th align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</th>
	</tr>
</form>
</table>
<?php
}
else //editar
{
if ($fecha!="")
	$fecha=fec_amd($fecha);
if ($id_falla <= 0) //nueva falla
{
	//verificar registro principal de falla en la fecha y alumno
	$sql="select id_falla from $fallas where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura = '0' limit 1";
	$datfall = viewsql($sql);
	foreach($datfall as $regfall)
	{$id_falla = $regfall->id_falla;}
}
$datfalla = datos_falla($ano,$id_falla);
if ($id_falla > 0) {
	$fecha = $datfalla["fecha"];
        $tipo = $datfalla["tipo"];
}
//traer estado periodo
$estado = "";
$pernomb = "";
$periodo = 0;
$sql="SELECT periodo, pernomb, estado FROM periodos WHERE ano = '$ano' AND pertipo = 'P' AND '$fecha' BETWEEN date_format(fchfallasi,'%Y-%m-%d') AND date_format(fchfallasf,'%Y-%m-%d') LIMIT 1";
$datperi = viewsql($sql);
foreach($datperi as $regperi)
{
	$estado = $regperi->estado;
	$pernomb = $regperi->pernomb;
	$periodo = $regperi->periodo;
}
if ($estado == "")
	$estado = "ABIERTO";
if ($ano!=$_SESSION["anoaca"])
{
	$estado = "CERRADO";
	$sw = 0;
}
$titulo = fecha_completa($fecha);
$numdia = date("w",mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2)), intval(substr($fecha,0,4))));
$titdia = nombre_dia($numdia);
//traer curso del alumno
$nivel = "";
$grado = "";
$curso = "";
$sql = "select $cursos.grado, $cursos.curso, grados.gradonom, grados.nivel from $cursos, grados where $cursos.id_alumno = '$id_alumno' and $cursos.grado = grados.grado limit 1";
$datcurs = viewsql($sql);
foreach($datcurs as $regcurs)
{
	$nivel = $regcurs->nivel;
	$grado = $regcurs->grado;
	$gradonom = $regcurs->gradonom;
	$curso = $regcurs->curso;
}
if ($curso != "")
{
    $numdia = date("w",mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2)), intval(substr($fecha,0,4))));
    $fallas = "y".$ano."fallas";
    //traer asignaturas del grado
    $sql="SELECT id_asignatura, asignatura FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' ORDER BY asignatura";
    $datasig=viewsql($sql);
    //horario asignado con intervalos del nivel
    $sql = "SELECT horariosint.intervalo, horariosint.horas, horarios.id_asignatura FROM horariosint, horarios WHERE horariosint.nivel = '$nivel' AND horarios.ano='$ano' and horarios.dia='$numdia' and horarios.grado='$grado' and horarios.curso='$curso' and horarios.intervalo = horariosint.intervalo order by abs(horariosint.intervalo)";
    $dathora = viewsql($sql);
    //traer fallas del dia
    $sql="SELECT id_asignatura, intervalos FROM $fallas WHERE id_alumno = '$id_alumno' and id_asignatura > '0' and fecha = '$fecha' order by id_asignatura";
    $datfall=viewsql($sql);
}
//traer asignaturas de la fecha por el horario
$tittipo = "INASISTENCIA";
if ($datfalla["tipo"] == "R")
    $tittipo = "RETARDO";
if ($datfalla["tipo"] == "P")
    $tittipo = "PERMISO";
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_fallas.php" method="post" name="form1" onSubmit="return validarfalla();">
<input type='hidden' name='op' value='grabarfalla'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='grado' value='<?php echo $grado;?>'>
<input type='hidden' name='curso' value='<?php echo $curso;?>'>
<input type='hidden' name='periodo' value='<?php echo $periodo;?>'>
<input type='hidden' name='numdia' value='<?php echo $numdia;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type='hidden' name='id_falla' value='<?php echo $id_falla;?>'>
<input type='hidden' name='tipo' value='<?php echo $tipo;?>'>
<input type='hidden' name='page' value='<?php echo $page;?>'>
	<tr height="40">
		<td width="20"></td>
		<td class="titulo"><img src='images/fallas.png' width="28" height="28" border="0" align="absmiddle"> <?php echo $tittipo;?></td>
		<td width="15"></td>
		<td width="370"></td>
		<td width="20"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr height="40">
                                    <td colspan="2" class="titulo_campos">
                                        <?php echo $titulo; ?>
                                        <input type='hidden' name='fecha' value='<?php echo fec_dma($fecha);?>'>
                                        <input type='hidden' name='hcita' value=''>
                                    </td>
                                    <td></td>
                                    <td></td>
                                </tr>
				<tr height="30">
					<td width="60">Excusa</td>
					<td>
						<select name="id_categoria" style="width: 240px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                                <option value='0' selected></option>
						<?php
						$sql="select id_categoria, categoria from categorias where tip_categoria = 'INASISTENCIAS' order by categoria";
						$datcateg=viewsql($sql);
						foreach($datcateg as $regcateg)
						{
							if (intval($datfalla["id_categoria"]) == intval($regcateg->id_categoria))
								echo "<option value='$regcateg->id_categoria' selected>$regcateg->categoria</option>";
							else
								echo "<option value='$regcateg->id_categoria'>$regcateg->categoria</option>";
						}
						?>																
						</select>
                                        </td>
                                        <td>Fecha Excusa &nbsp;</td>
					<td><input type="text" name="fchjust" id="fchjust" value="<?php echo fec_dma($datfalla["fchjust"]);?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchjust, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>	
				<tr height="40">
                                        <?php if ($tipo=="P") { ?>
					<td>Permiso</td>
                                        <td><input type="text" name="concepto" value="<?php echo $datfalla["concepto"];?>" style="width: 240px; text-transform:uppercase;"></td>
                                        <?php } else { ?>
					<td></td>
                                        <td><input type='hidden' name='concepto' value=''></td>
                                        <?php } ?>
                                        <td>Vigente hasta&nbsp;</td>
					<td><input type="text" name="fchfin" id="fchfin" value="<?php echo fec_dma($datfalla["fchfin"]);?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchfin, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>				
				<tr valign="top">
					<td>Detalles</td>
                                        <td colspan="3">
					<textarea name='detalles' rows="7" cols="60" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datfalla["detalles"];?></textarea>
                                        <?php echo $pernomb;?> <?php echo $estado;?>
					</td>
				</tr>
				<tr height="10"><td colspan="4"></td></tr>
			</table>
		</td>
		<td></td>
		<td>
                        <?php
 			//si curso asignado y horario configurado completamente
			if ($curso != "" and count($dathora) > 0)
			{
				$sw = 1;
				if (strtotime($fecha) > strtotime(date('Y-m-d'))) //no admite fechas posteriores a hoy
					$sw = 0;
				?>
				<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="100%">
				<tr>
                                    <th colspan="4" class="titulo_error">FALLAS EN ASIGNATURAS</th>
				</tr>
				<?php
				$num = 0;
				foreach($dathora as $reghora)
				{
					$num = $num + 1;
					$intervalo = $reghora->intervalo;
					$id_asignatura = $reghora->id_asignatura;
					$nameA = "A".$intervalo;
					$nameF = "F".$intervalo;
					$chk = "";
					$campo  = " ".$intervalo." ";
					foreach($datfall as $regfall)
					{
						$buscaren = "*".$regfall->intervalos;
						if (strpos($buscaren,$campo) > 0)
						{
							$id_asignatura = $regfall->id_asignatura;
							$chk = "checked";
							break;
						}
					}
					?>
					<tr bgcolor="#FFFFFF" height="25">
						<td width="15" align="center"><b><?php echo $num;?></b></td>
						<td width="75"><?php echo $reghora->horas;?></td>
						<td>
						<select name="<?php echo $nameA;?>" style="width: 220px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						foreach($datasig as $regasig)
						{
							if (intval($id_asignatura) == intval($regasig->id_asignatura))
								echo "<option value='$regasig->id_asignatura' selected>$regasig->asignatura</option>";
							else
							{
								if ($_SESSION["nivfal"] >= 4)
									echo "<option value='$regasig->id_asignatura'>$regasig->asignatura</option>";
							}
						}
						?>																
						</select>
						</td>
						<td width="25" align="center"><input type="checkbox" name="<?php echo $nameF;?>" <?php echo $chk;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					</tr>
					<?php
				} ?>
				</table>
				<?php
			}
			else
			{
				$sw = 0;
				echo "(FECHA NO VALIDA / PENDIENTE CURSO / HORARIO SIN DEFINIR)";
			} ?>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr height="36">
		<td colspan="5" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if (($_SESSION["nivfal"] >= 2 and $estado != "CERRADO" and $sw==1) or ($_SESSION["nivfal"] >= 4 and $sw==1)) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_falla > 0 and $_SESSION["nivfal"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="70" height="24" alt="Eliminar" hspace="10"></a>
			<?php } ?>
                        <?php if ($tipo != "P" and $id_falla > 0 and $datfalla["id_categoria"] > 0) {?><a name="imprimir" class="link_list" href="reports/rep_fallaspap.php?ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>" onclick="newWindow(this.href, '', 980, 500, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Imprimir"><img src="images/btn_imprimir.png" border="0" width="70" height="24" alt="Cerrar" hspace="10"></a><?php }?>
                        <?php if ($tipo == "P" and $id_falla > 0 and $datfalla["id_categoria"] > 0) {?><a name="imprimir" class="link_list" href="reports/rep_fallaspapper.php?ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>" onclick="newWindow(this.href, '', 980, 500, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Imprimir"><img src="images/btn_imprimir.png" border="0" width="70" height="24" alt="Cerrar" hspace="10"></a><?php }?>
                        <?php if ($page == "") { ?>
			<a href="alumnos_fallas.php?id_alumno=<?php echo $id_alumno;?>&id_falla=<?php echo $id_falla;?>"><img src="images/btn_regresar.png" border="0" width="70" height="24" alt="Regresar" hspace="10"></a>
			<?php }?>			
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
