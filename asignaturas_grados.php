<?php
/***********************************************************************************************
* @file        : asignaturas_grados.php                                                        *
* @brief       : Lista y edita grados acad�micos                                               *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 12-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 12-abr-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : "listar" ;
$op = isset($_GET['op']) ? $_GET['op'] : null ;
$id_grado = isset($_GET['id_grado']) ? $_GET['id_grado'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
connect();
//promediar notas �ltimo periodo abierto
if ($op=="promediar")
{
	include_once("database/grabar_notas.php");
	include_once("database/datos_notas.php");
	$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
	$periodo = isset($_GET['periodo']) ? $_GET['periodo'] : null ;
	$pertipo = substr($periodo,0,1);
	$periodo = substr($periodo,1,1);
	$titulo = "PERIODO";
	$curso = "";
	$id_alumno = 0;
	if ($pertipo == "P") //periodo de notas
	{
		if ($periodo <= 4)
			promediar_notas($ano, $periodo, $grado, $curso, $id_alumno);
		else
		{
			promediar_finales($ano, $grado, $curso, $id_alumno);
			promediar_anolect($ano, $grado, $curso, $id_alumno);
		}
	}
	else //examenes
	{
		$titulo = "EXAMEN";
		promediar_examen($ano, $periodo, $grado, $curso, $id_alumno);
	}
	echo"<script>alert('GRADO $grado A�O $ano $titulo $periodo, Retotalizado.'); window.location.href='asignaturas_grados.php';</script>";
}
//totalizar puestos
if ($op=="puestos")
{
	include("database/grabar_notas.php");
	$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
	$periodo = isset($_GET['periodo']) ? $_GET['periodo'] : null ;
	$pertipo = substr($periodo,0,1);
	$periodo = substr($periodo,1,1);
	$id_alumno = 0;
	if ($pertipo=="P")
	{
		$sql = "SELECT curso FROM cursos WHERE ano = '$ano' AND grado = '$grado' ORDER BY curso";
		$datcurs = viewsql($sql);
		foreach($datcurs as $regcurs)
		{
			$curso = $regcurs->curso;
			puestos($ano, $periodo, $grado, $curso);
		}
		echo"<script>alert('Grado $grado Puestos Retotalizados...'); window.location.href='asignaturas_grados.php';</script>";
	}
	else
	{echo"<script>alert('Opci�n inactiva para ex�menes...'); window.location.href='asignaturas_grados.php';</script>";}
}
//asignar cursos nuevo a�o
if ($op=="cursos")
{
	include("database/iniciar_anolectivo.php");
	$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
	asignar_cursos($ano, $grado);
	echo"<script>alert('Cursos asignados Alumnos Grado $grado...'); window.location.href='asignaturas_grados.php';</script>";
}
if ($op=="reparar")
{
	include("database/grabar_notas.php");
	$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
	$ano = $_SESSION["anoaca"];
	$cursos = "y".$ano."cursos";
	$notas = "y".$ano."notas";
	//periodos
	$sql="SELECT notanum, pertipo, periodo FROM periodos WHERE ano = '$ano' and pertipo = 'P' order by notanum";
	$datperi = viewsql($sql);
	//alumnos
	$sql="SELECT id_alumno FROM $cursos WHERE grado = '$grado' ORDER BY id_alumno";
	$datalum=viewsql($sql);
	//asignaturas
	$sql="SELECT id_asignatura FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' ORDER BY id_area, id_asignatura";
	$datasig=viewsql($sql);
	//calcular por alumno definitivas por periodo y asignatura
	foreach($datalum as $regalum)
	{
		$id_alumno = $regalum->id_alumno;
		foreach($datasig as $regasig)
		{
			$id_asignatura = $regasig->id_asignatura;
			foreach($datperi as $regperi)
			{
				$i = $regperi->periodo;
				$notaDef = promediar_definitiva($ano, $id_alumno, $id_asignatura, $i);
				$sql = "UPDATE $notas SET notaDef = '$notaDef' WHERE id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo='P' and periodo = '$i' LIMIT 1";
				execsql($sql);
			} //periodos
		} //asignaturas
	} //alumnos
}
if ($op=="nuevo")
{
	$sql="SELECT grado FROM grados ORDER BY id_grado";
	$datgrado=viewsql($sql);
	foreach($datgrado as $reggrado)
	{
		$grado = $reggrado->grado;
		asignar_cursos($ano, $grado);
		$sql= "INSERT INTO gradosval (ano, grado, matricula, pension, transporte) VALUES ('$ano','$grado','0','0','0')";
		execsql($sql);
	}
}
$op1_back1="images/backsilver1.gif";
$op1_back2="images/backsilver2.gif";
$op1_back3="images/backsilver3.gif";
$op2_back1="images/backsilver1.gif";
$op2_back2="images/backsilver2.gif";
$op2_back3="images/backsilver3.gif";
if ($opcion=="valores")
{
	$op2_back1="images/backgreen1.gif";
	$op2_back2="images/backgreen2.gif";
	$op2_back3="images/backgreen3.gif";
}
else
{
	$op1_back1="images/backgreen1.gif";
	$op1_back2="images/backgreen2.gif";
	$op1_back3="images/backgreen3.gif";
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.grado.value == '')
	{alert('Falta asignar grado...');document.form1.grado.focus(); return false;}
	if(document.form1.gradonom.value == '')
	{alert('Falta asignar nombre del grado...');document.form1.gradonom.focus(); return false;}
	if(document.form1.gradocolor.value == '')
	{alert('Falta seleccionar color de fondo...');document.form1.gradocolor.focus(); return false;}
	if(document.form1.gradofont.value == '')
	{alert('Falta seleccionar color de letra...');document.form1.gradofont.focus(); return false;}
	return true;
}
function seleccionar()
{
	var ano = document.form1.ano.value;
	var theURL = 'asignaturas_grados.php?opcion=valores&ano='+ano;
	window.location.href = theURL;
}
function totalizar()
{
	var grado = document.form1.grado.value;
	var periodo = document.form1.periodo.value;
	var opcion = document.form1.opcion.value;
	if ((opcion=='CURSOS' && periodo=='') || (opcion!='' && periodo!=''))
	{
		ok=confirm('Esta opci�n puede demorar unos minutos, desea continuar?');
		if(ok)
		{
			if (opcion=='PROMEDIOS')
				window.location.href='asignaturas_grados.php?op=promediar&grado='+grado+'&periodo='+periodo;
			if (opcion=='PUESTOS')
				window.location.href='asignaturas_grados.php?op=puestos&grado='+grado+'&periodo='+periodo;
			if (opcion=='CURSOS')
				window.location.href='asignaturas_grados.php?op=cursos&grado='+grado+'&periodo='+periodo;
			if (opcion=='REPARAR')
				window.location.href='asignaturas_grados.php?op=reparar&grado='+grado;
		}
	}
	else
	{
		alert('Falta seleccionar periodo u opci�n a procesar...');
	}
}
function ChangeColor(obj)
{
	var nwcolor = obj.value;
	var obname = obj.name;
	if (obname=='gradocolor')
		ColorBack.style.backgroundColor=nwcolor;
	else
		ColorFont.style.backgroundColor=nwcolor;
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/grados.jpg' width="40" height="40" border="0" align="absmiddle"> Grados Acad�micos</td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr>
		<td align="right" valign="bottom">
			<table cellpadding="0" cellspacing="0" border="0">
				<tr height="25" bgcolor="#FFFFFF">
					<td width="6" background="<?php echo $op1_back1;?>"></td>
					<td background="<?php echo $op1_back2;?>" align="center">&nbsp;&nbsp;<a href="asignaturas_grados.php" class="link_menu">Configurar Grados</a>&nbsp;&nbsp;</td>
					<td width="6" background="<?php echo $op1_back3;?>"></td>
					<td width="6" background="<?php echo $op2_back1;?>"></td>
					<td background="<?php echo $op2_back2;?>" align="center">&nbsp;&nbsp;<a href="asignaturas_grados.php?opcion=valores" class="link_menu">Valores Anuales</a>&nbsp;&nbsp;</td>
					<td width="6" background="<?php echo $op2_back3;?>"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td height="2" bgcolor="#BBE040"></td></tr>
	<tr><td height="1" bgcolor="#A7CD2C"></td></tr>
</table>
<?php if ($opcion == "listar") {?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="20"></th>
					<th>Descripci�n</th>
					<th width="80">Nivel</th>
				</tr>
				<?php
				$sql="SELECT grados.*, niveles.nombre FROM grados, niveles WHERE grados.nivel = niveles.nivel order by grados.id_grado";
				$datgrado=viewsql($sql);
				foreach($datgrado as $reggrado)
				{
				$id_grado = $reggrado->id_grado;
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center" bgcolor="<?php echo $reggrado->gradocolor;?>" class="titulo_campos"><font color="<?php echo $reggrado->gradofont;?>"><?php echo $reggrado->grado;?>&deg;</font></td>
					<td>&nbsp;<a class="link_black" href="asignaturas_grados.php?opcion=editar&id_grado=<?php echo $id_grado;?>" title="Grado"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="3" align="absmiddle"> <?php echo $reggrado->gradonom;?></a></td>
					<td><?php echo $reggrado->nombre;?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 4) { ?>
			<a href="asignaturas_grados.php?opcion=editar"><img src="images/btn_nuevo.png" border="0" width="75" height="24" alt="Nuevo Registro" hspace="10"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
<br><br>
<?php } if ($opcion == "valores") {?>
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabar_valores'>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="30">
					<th colspan="2" class="titulo" align="left">
						<select name="ano" style="font-size: 15px; font-weight: bold;" onChange="javascript:seleccionar();">
						<?php
						$sql="SELECT ano FROM anoslect ORDER BY ano desc";
						$datanol=viewsql($sql);
						foreach($datanol as $reganol)
						{
							$selano = "";
							if ($reganol->ano==$ano)
								$selano = "selected";
							echo "<option value='$reganol->ano' $selano>A�O $reganol->ano</option>";
						}
						?>
						</select>
					</th>
					<th width="80">$ Transporte</th>
					<th width="80">$ Matr�cula</th>
					<th width="80">$ Pensi�n</th>
                                        <th width="80">$ Pensi�n</br>16 al 30</th>
					<th width="80">$ Pensi�n</br>M�s del 30</th>
				</tr>
				<?php
				$sql="SELECT gradosval.matricula, gradosval.pension, gradosval.pensionfin, gradosval.pensionret, gradosval.transporte, grados.* FROM gradosval, grados WHERE gradosval.ano = '$ano' and gradosval.grado = grados.grado order by grados.id_grado";
				$datgrado=viewsql($sql);
				foreach($datgrado as $reggrado)
				{
				$id_grado = $reggrado->id_grado;
				$valtra = "valtra".$id_grado;
				$valmat = "valmat".$id_grado;
				$valpen = "valpen".$id_grado;
				$valfin = "valfin".$id_grado;
				$valret = "valret".$id_grado;
				$transporte = $reggrado->transporte;
				$matricula = $reggrado->matricula;
				$pension = $reggrado->pension;
				$pensionfin = $reggrado->pensionfin;
				$pensionret = $reggrado->pensionret;
				if ($matricula <= 0)
					$matricula = "";
				if ($pension <= 0)
					$pension = "";
				if ($transporte <= 0)
					$transporte = "";
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td width="30" align="center" bgcolor="<?php echo $reggrado->gradocolor;?>" class="titulo_campos"><font color="<?php echo $reggrado->gradofont;?>"><?php echo $reggrado->grado;?>&deg;</font></td>
					<td>&nbsp;&nbsp;&nbsp;<?php echo $reggrado->gradonom;?></td>
					<td align="center"><input type="text" name="<?php echo $valtra;?>" style="width: 50px; text-align: right; font-size: 12px;" value="<?php echo $transporte;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center"><input type="text" name="<?php echo $valmat;?>" style="width: 50px; text-align: right; font-size: 12px;" value="<?php echo $matricula;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center"><input type="text" name="<?php echo $valpen;?>" style="width: 50px; text-align: right; font-size: 12px;" value="<?php echo $pension;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center"><input type="text" name="<?php echo $valfin;?>" style="width: 50px; text-align: right; font-size: 12px;" value="<?php echo $pensionfin;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center"><input type="text" name="<?php echo $valret;?>" style="width: 50px; text-align: right; font-size: 12px;" value="<?php echo $pensionret;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivmat"] >= 4 and count($datgrado) > 0) { ?> 
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar" align="absmiddle">
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</table>
</form>
<br><br>
<?php }
if ($opcion == "editar") {
$datgrado = datos_grado($id_grado);
$titulo = "Nuevo Grado";
if ($id_grado > 0)
	$titulo = "Editar Grado";
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabargrado'>
<input type='hidden' name='id_grado' value='<?php echo $id_grado;?>'>
<input type='hidden' name='oldgradocolor' value='<?php echo $datgrado["gradocolor"];?>'>
<input type='hidden' name='oldgradofont' value='<?php echo $datgrado["gradofont"];?>'>
<input type='hidden' name='oldgradosig' value='<?php echo $datgrado["gradosig"];?>'>
	<tr height="10">
		<td width="30"></td>
		<td></td>
		<td width="30"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos"><?php echo $titulo;?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td height="30"></td></tr>
				<tr height="30">
					<td>Grado</td>
					<td>
					<input type="text" name="grado" value="<?php echo $datgrado["grado"];?>" maxlength="2" style="width: 25px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					<input type="text" name="gradonom" size="29" value="<?php echo $datgrado["gradonom"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr height="30">
					<td>Nivel</td>
					<td>
						<select name="nivel" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php
						$sql= "select nivel, nombre from niveles order by id_nivel";
						$datnive=viewsql($sql);
						foreach($datnive as $regnive)
						{
							if ($datgrado["nivel"] == $regnive->nivel)
								echo "<option value='$regnive->nivel' selected>$regnive->nombre</option>";
							else
								echo "<option value='$regnive->nivel'>$regnive->nombre</option>";
						}
						?>																
						</select>
						Promueve a
						<select name="gradosig" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php
						if ($id_grado <= 0)
							echo "<option value='' selected>...</option>";	
						$sql="select grado from grados order by id_grado";
						$datgrad=viewsql($sql);
						foreach($datgrad as $reggrad)
						{
							if ($datgrado["gradosig"] == $reggrad->grado)
								echo "<option value='$reggrad->grado' selected>$reggrad->grado&deg;</option>";
							else
								echo "<option value='$reggrad->grado'>$reggrad->grado&deg;</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr height="30">
					<td>Educaci�n</td>
					<td>
					<input type="text" name="educacion" size="13" value="<?php echo $datgrado["educacion"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr height="30">
					<td>Perdida A�o</td>
					<td>
					<?php if ($datgrado["perdida"] > 0) { ?>
					SI <input type="radio" name="perdida" value="1" checked> &nbsp;&nbsp;&nbsp; NO <input type="radio" name="perdida" value="0">
					<?php } else { ?>
					SI <input type="radio" name="perdida" value="1"> &nbsp;&nbsp;&nbsp; NO <input type="radio" name="perdida" value="0" checked>
					<?php } ?>
					</td>
				</tr>
				<tr height="30">
					<td>Ex.Superaci�n</td>
					<td>
					<?php if ($datgrado["superacion"] > 0) { ?>
					SI <input type="radio" name="superacion" value="1" checked> &nbsp;&nbsp;&nbsp; NO <input type="radio" name="superacion" value="0">
					<?php } else { ?>
					SI <input type="radio" name="superacion" value="1"> &nbsp;&nbsp;&nbsp; NO <input type="radio" name="superacion" value="0" checked>
					<?php } ?>
					</td>
				</tr>
				<tr height="30">
					<td>Fondo</td>
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><DIV ID="ColorBack" STYLE="width: 30px; height: 18px; border: 1px solid #7F9DB9; background-color: <?php echo $datgrado["gradocolor"];?>;"></DIV></td>
							<td width="10"></td>
							<td>
								<input type="text" name="gradocolor" id="gradocolor" value="<?php echo $datgrado["gradocolor"];?>" readonly style="width: 50px;" onFocus="javascript:ChangeColor(this)" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<a href="javascript:;" onClick="window.open('libs/colores.php?campo=gradocolor','','top=100,left=100,scrollbars=yes,width=430,height=300')" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><img src="images/ico_paleta.gif" border="0" width="20" height="20" align="absmiddle" hspace="1" title="Paleta..."></a>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr height="30">
					<td>Letra</td>
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<td><DIV ID="ColorFont" STYLE="width: 30px; height: 18px; border: 1px solid #7F9DB9; background-color: <?php echo $datgrado["gradofont"];?>;"></DIV></td>
							<td width="10"></td>
							<td>
								<input type="text" name="gradofont" id="gradofont" value="<?php echo $datgrado["gradofont"];?>" readonly style="width: 50px;" onFocus="javascript:ChangeColor(this)" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<a href="javascript:;" onClick="window.open('libs/colores.php?campo=gradofont','','top=100,left=100,scrollbars=yes,width=430,height=300')" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><img src="images/ico_paleta.gif" border="0" width="20" height="20" align="absmiddle" hspace="1" title="Paleta..."></a>
							</td>
						</tr>
						</table>
					</td>
				</tr>
				<tr height="30">
					<td>Estado</td>
					<td>
						<?php if ($datgrado["inactivar"]==0) { ?>
						Activo <input type="radio" name="inactivar" value="0" checked> &nbsp;&nbsp;&nbsp; Inactivo <input type="radio" name="inactivar" value="1">
						<?php } else { ?>
						Activo <input type="radio" name="inactivar" value="0"> &nbsp;&nbsp;&nbsp; Inactivo <input type="radio" name="inactivar" value="1" checked>
						<?php } ?>
					</td>
				</tr>
				<tr><td height="30"></td></tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 4) { ?>
			<select name="periodo" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			<option value="" selected>Per�odo...</option>
			<?php
			$cierre = "";
			$sql="SELECT notanum, pertipo, periodo, perplan, pernomb, estado FROM periodos WHERE ano = '$ano' order by notanum";
			$datperi=viewsql($sql);
			foreach($datperi as $regperi)
			{
				if ($regperi->pertipo=="P") //estado del ultimo periodo
					$cierre=$regperi->estado;
				if ($regperi->estado=="CERRADO")
					echo "<option value='$regperi->pertipo$regperi->periodo'>$regperi->pernomb</option>";
			}
			if ($superacion > 0) {?>
			<option value="E4">EXAMEN SUPERACION</option>
			<?php
			}
			if ($cierre=="CERRADO")
			{
			?>
			<option value="P5">DEFINITIVAS Y CIERRE</option>
			<?php } ?>
			</select>
			<select name="opcion" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<option value="" selected>Opci�n...</option>
				<option value="PROMEDIOS">Calcular Promedios</option>
				<option value="PUESTOS">Asignar Puestos</option>
				<option value="CURSOS">Asignar Cursos</option>
				<?php if(strtoupper($_SESSION["usulog"])=="ADMIN") { ?>
				<option value="REPARAR">Reparar Definitivas</option>
				<?php } ?>
			</select>
			<a href="javascript:;" onClick="totalizar();" title="Totalizar..."><img src="images/btn_procesar.png" border="0" width="75" height="24" alt="Totalizar Notas" hspace="1" align="absmiddle"></a>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="1" alt="Grabar" align="absmiddle">
			<?php } ?>
			<a href="asignaturas_grados.php"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="1" align="absmiddle"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
