<?php
//IMPORTAR NOTAS DESDE ARCHIVO TXT - DELIMITADO POR TABULACIONES
//SE ABRE LA TABLA FOXBASE CON EXCEL Y SE GRABA COMO TXT DELIMITADA
//AFECTA TABLAS Y2012FALLAS

$impano = "2012";
$ano = "2012";
$periodo = 1;
$fecha_hora = date('Y-m-d')." ".date('H:i:s');

include("../database/database.php");
include("../database/grabar_notas.php");
connect();
//nombres de tablas
$fallas = "y".$impano."fallas";
$cursos = "y".$impano."cursos";

//desocupar tabla temporal
$archivo = "C:/xampp/htdocs/democol/temporales/notasidic/fallas.txt";
$file = "importar";
execsql("delete from ".$file);
//cargar datos tabla temporal	
$sqlc="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sqlc);
$sql="select * from $file";
$data=viewsql($sql);
$ini_fecha = "";
$ini_alumno = "";
foreach($data as $registros)
{
	$mes = "0".trim($registros->campo1);
	$dia = intval(trim($registros->campo2));
	if ($dia < 10)
		$dia = "0".$dia;
	$fecha = $ano."-".$mes."-".$dia;
	$id_alumno = intval(trim($registros->campo3));
	$grado = intval(trim($registros->campo4));
	if ($grado == 12) //prejardin
		$grado = -2;
	if ($grado == 13) //jardin
		$grado = -1;
	if ($grado == 14) //transicion
		$grado = 0;
	$codigo = trim($registros->campo5).trim($registros->campo6);
	$horas = intval(trim($registros->campo7));
	$id_categoria = 10;
	//buscar asignatura
	$id_asignatura = 0;
	$sql="select id_asignatura from asignaturas where ano = '$ano' and grado = '$grado' and codigo = '$codigo' limit 1";
	$datasig = viewsql($sql);
	foreach($datasig as $regasig)
	{$id_asignatura = $regasig->id_asignatura;}
	//traer curso del alumno
	$curso = "";
	$sql = "select curso from $cursos where id_alumno = '$id_alumno' limit 1";
	$datcurs = viewsql($sql);
	foreach($datcurs as $regcurs)
	{$curso = $regcurs->curso;}
	//horario asignado con intervalos del nivel
	$numdia = date("w",mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2)), intval(substr($fecha,0,4))));
	$intervalos = "";
	$num = 0;
	$sql = "SELECT intervalo FROM horarios WHERE ano='$ano' and dia='$numdia' and grado='$grado' and curso='$curso' and id_asignatura='$id_asignatura' order by abs(intervalo)";
	$dathora = viewsql($sql);
	foreach($dathora as $reghora)
	{
		$num = $num + 1;
		$intervalos = $intervalos.$reghora->intervalo." ";
		if ($num >= $horas)
			break;
	}
	if ($intervalos != "")
		$intervalos = " ".$intervalos;
	//crear falla principal
	if ($ini_fecha != $fecha or $ini_alumno != $id_alumno)
	{
		if ($ini_fecha != $fecha)
			$ini_fecha = $fecha;
		if ($ini_alumno != $id_alumno)
			$ini_alumno = $id_alumno;
		$sql = "INSERT INTO $fallas (id_alumno, id_categoria, fecha, periodo, fchcrea, usucrea) VALUES ('$id_alumno', '$id_categoria', '$fecha', '1', '$fecha_hora', 'IMPORTACION') ";
		execsql($sql);
	}
	//crear falla por asignatura
	$sql= "INSERT INTO $fallas (id_alumno, id_asignatura, id_categoria, fecha, periodo, horas, intervalos) VALUES ('$id_alumno', '$id_asignatura', '$id_categoria', '$fecha', '1', '$horas', '$intervalos') ";
	execsql($sql);
} //fin registros
echo "IMPORTAR FALLAS OK...<BR>";
disconnect();
?>
<a href="../notas.php">Regresar a NOTAS...</a>
