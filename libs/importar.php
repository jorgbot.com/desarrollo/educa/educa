<?php
/***********************************************************************************************
* @file        : alumnos.php                                                                   *
* @brief       : Muestra la lista general de alumnos para selecci�n o creaci�n                 *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 15-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 15-feb-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("database/database.php");
include("libs/fechas.php");
?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>IDIC</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="robots" content="noindex"/>
<meta name="googlebot" content="noindex"/>
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#FFFF66">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="150" bgcolor="#006600">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr height="40" bgcolor="#006600">
		<td background="fondos/top_verde.jpg"></td>
		<td background="fondos/top_verde.jpg" class="titulo_color">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_top.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/top_verde.jpg"></td>
	</tr>
	<tr height="3" bgcolor="#FFFF66">
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_top.jpg"></td></tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1024">
			<tr><td colspan="3" height="40"></td></tr>
			<tr valign="top">
				<td width="200">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10"></td>
				<td>
					<table>
					<tr>
						<td>
							<a href="libs/impo_alumnos.php">Importar Alumnos...</a>
							<br><br>
							<a href="libs/impo_asignaturas.php">Importar Asignaturas...</a>
							<br><br>
							<a href="libs/impo_logros.php">Importar Logros...</a>
							<br><br>
							<a href="libs/impo_profesores.php">Importar Profesores...</a>
							<br><br>
							<a href="libs/impo_notas.php">Importar Notas...</a>
							<br><br>
							<a href="libs/impo_fallas.php">Importar Fallas...</a>
						</td>
					</tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="20"></td></tr>			
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_bot.jpg"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#D3A919"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FFDB4B"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FCF463"></td></tr>
	<tr>
		<td colspan="3" valign="top">
		<div id="fondo_pie">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</div>
		</td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
