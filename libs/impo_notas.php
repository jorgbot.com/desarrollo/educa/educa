<?php
//IMPORTAR NOTAS DESDE ARCHIVO TXT - DELIMITADO POR TABULACIONES
//SE ABRE LA TABLA FOXBASE CON EXCEL Y SE GRABA COMO TXT DELIMITADA
//AFECTA TABLAS Y2012NOTAS, Y2012NOTASP1, Y2012OBSERVA, Y2012PLANILLA DEL PERIODO CORRESPONDIENTE

$impano = "2012";
$ano = "2012";
$periodo = 1;
$fecha_hora=date('Y-m-d')." ".date('H:i:s');

include("../database/database.php");
include("../database/grabar_notas.php");
connect();
//nombres de tablas
$notas = "y".$impano."notas";
$notasp = "y".$impano."notasp1";
$logros = "y".$impano."logros";
$activ = "y".$impano."activ";
$observa = "y".$impano."observa";
$cursos = "y".$impano."cursos";
$planillas = "y".$impano."planillas";

//PASO 1. Imporat notas
//desocupar tabla temporal
$archivo = "C:/xampp/htdocs/democol/temporales/notasidic/notas6a11.txt";
$file = "importar";
execsql("delete from ".$file);
//cargar datos tabla temporal	
$sqlc="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sqlc);
$fecha_hora=date('Y-m-d') . " ". date('H:i:s'); //Fecha del sistema caracter
$sql="select * from $file";
$data=viewsql($sql);
foreach($data as $registros)
{
	$id_alumno = intval(trim($registros->campo2));
	$grado = intval(trim($registros->campo3));
	if ($grado == 12) //prejardin
		$grado = -2;
	if ($grado == 13) //jardin
		$grado = -1;
	if ($grado == 14) //transicion
		$grado = 0;
	$codigo = trim($registros->campo4).trim($registros->campo5);	
	$logrocod = trim($registros->campo6);
	$act1 = intval(trim($registros->campo7));
	$act2 = intval(trim($registros->campo8));
	$act3 = intval(trim($registros->campo9));
	$pend = trim($registros->campo10);
	if ($pend != "NA")
		$pend = "";
	//buscar asignatura
	$id_asignatura = 0;
	$activmin = 3;
	$sql="select id_asignatura, activmin from asignaturas where ano = '$impano' and grado = '$grado' and codigo = '$codigo' limit 1";
	$datasig = viewsql($sql);
	foreach($datasig as $regasig)
	{
		$id_asignatura = $regasig->id_asignatura;
		$activmin = $regasig->activmin;
	}
	//buscar logro
	if ($logrocod <= 99)
	{
		$id_logro = 0;
		$sql="select id_logro from $logros where id_asignatura = '$id_asignatura' and logrocod = '$logrocod' limit 1";
		$datlogr = viewsql($sql);
		foreach($datlogr as $reglogr)
		{$id_logro = $reglogr->id_logro;}
		if ($pend != "NA")
		{
			if ($activmin > 0) //carga nota a las actividades del logro
			{
				//validar 3 actividades para el logro
				$sql = "SELECT id_actividad, numero FROM $activ WHERE id_logro = '$id_logro' AND periodo = '$periodo' ORDER BY numero";
				$datacti = viewsql($sql);
				$num = count($datacti);
				if ($num < $activmin and $activmin > 1)
				{
					$num = $num+1;
					for($i=$num; $i<=$activmin ;$i++)
					{
						$actividad = "ACT".$i;
						$sql= "INSERT INTO $activ (id_asignatura, id_logro, periodo, actividad, codigo, numero) ".
						"VALUES ('$id_asignatura', '$id_logro', '$periodo', '$actividad', '$actividad', '$i')";
						execsql($sql);
					}  
					$sql = "SELECT id_actividad, numero FROM $activ WHERE id_logro = '$id_logro' AND periodo = '$periodo' ORDER BY numero";
					$datacti = viewsql($sql);
				}
				$num = 0;
				foreach($datacti as $regacti)
				{
					$num = $num +1;
					$id_actividad = $regacti->id_actividad;
					if ($num == 1)
						$nota = $act1;
					if ($num == 2)
						$nota = $act2;
					if ($num == 3)
						$nota = $act3;
					$sql= "INSERT INTO $notasp (id_alumno, id_asignatura, id_logro, id_actividad, nota) ".
						"VALUES ('$id_alumno', '$id_asignatura', '$id_logro', '$id_actividad', '$nota')";
					execsql($sql);
					if ($num >= 3 or ($num==1 and $activmin == 1))
						break;
				}
			}
			else //carga primera nota a la asignatura
			{
				$sql= "INSERT INTO $notas (id_alumno, id_asignatura, pertipo, periodo, nota, pendiente, inactivar) ".
						"VALUES ('$id_alumno', '$id_asignatura', 'P', '$periodo', '$act1', '0', '0')";
				execsql($sql);
			}
		}
		else //carga logro sin nota - pendiente
		{
			$sql= "INSERT INTO $notasp (id_alumno, id_asignatura, id_logro, id_actividad, nota, pendiente, inactivar) ".
				"VALUES ('$id_alumno', '$id_asignatura', '$id_logro', '0', '0', '1', '0')";
			execsql($sql);
		}
	}
	else
	{
		$id_observacion = 0;
		$observacion = "";
		$sql="select id_observacion, observacion from observaciones where codigo = '$logrocod' limit 1";
		$datobse = viewsql($sql);
		foreach($datobse as $regobse)
		{
			$id_observacion = $regobse->id_observacion;
			$observacion = $regobse->observacion;
		}
		$sql= "INSERT INTO $observa (id_alumno, id_asignatura, pertipo, periodo, id_observacion, observacion) ".
			"VALUES ('$id_alumno', '$id_asignatura', 'P', '$periodo', '$id_observacion', '$observacion')";
		execsql($sql);
	}
} //fin registros
echo "IMPORTAR NOTAS OK...<BR>";

//PASO 2. Totalizar
for($i=6; $i<=11; $i++)
{
	$grado = $i;
	$curso = "";
	$id_alumno = 0;
	//promediar grado
	promediar_grado($ano, $periodo, $grado, $curso, $id_alumno);
	//grabar y cerrar planillas de asignaturas por curso
	$sql="SELECT * FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' and notas = '1' ORDER BY id_area, id_asignatura ";
	$datasigna=viewsql($sql);	
	$sql = "SELECT curso FROM cursos WHERE ano = '$ano' AND grado = '$grado' ORDER BY curso";
	$datcurs = viewsql($sql);
	foreach($datcurs as $regcurs)
	{
		$curso = $regcurs->curso;
		foreach($datasigna as $regasigna)
		{
			$id_asignatura = $regasigna->id_asignatura;
			$id_planilla = 0;
			$sql = "select id_planilla from $planillas where id_asignatura = '$id_asignatura' and grado='$grado' and curso='$curso' and pertipo = 'P' and periodo = '$periodo' limit 1";
			$datreg=viewsql($sql);
			foreach($datreg as $reg)
			{$id_planilla = intval($reg->id_planilla);}
			if ($id_planilla <= 0)
				$sql= "INSERT INTO $planillas (grado, curso, id_asignatura, pertipo, periodo, estado, fchcierre) VALUES ('$grado', '$curso', '$id_asignatura', 'P', '$periodo', 'CERRADA', '$fecha_hora')";
			else
				$sql = "UPDATE $planillas SET estado = 'CERRADA', fchcierre = '$fecha_hora' WHERE id_planilla = '$id_planilla' LIMIT 1";
			execsql($sql);
		}
		//grabar y cerrar planilla general del curso
		$id_planilla = 0;
		$sql = "select id_planilla from $planillas where id_asignatura = '0' and grado='$grado' and curso='$curso' and pertipo = 'P' and periodo = '$periodo' limit 1";
		$datreg=viewsql($sql);
		foreach($datreg as $reg)
		{$id_planilla = intval($reg->id_planilla);}
		if ($id_planilla > 0)
			$sql = "UPDATE $planillas SET estado = 'CERRADO', fchcierre = '$fecha_hora' WHERE id_planilla = '$id_planilla' LIMIT 1";
		else
			$sql= "INSERT INTO $planillas (grado, curso, id_asignatura, pertipo, periodo, estado, fchcierre) VALUES ('$grado', '$curso', '0', 'P', '$periodo', 'CERRADO', '$fecha_hora')";
		execsql($sql);
	}
}  
disconnect();
echo "TOTALIZAR NOTAS OK...<BR>";
?>
<a href="../notas.php">Regresar a Notas...</a>
