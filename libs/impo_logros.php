<?php
//IMPORTAR ASIGNATURAS DESDE ARCHIVO TXT - DELIMITADO POR TABULACIONES
//SE ABRE LA TABLA FOXBASE CON EXCEL Y SE GRABA COMO TXT DELIMITADA
//TABLAS LOGROS, ACTIVIDADES Y OBSERVACIONES

include("database/database.php");
connect();
$impano = 2011;
//desocupar tabla temporal
$archivo = "C:/xampp/htdocs/idic/recursos/nologrob".$impano.".txt";
$file = "importar";
execsql("delete from ".$file);
//cargar datos tabla temporal	
$sqlc="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sqlc);
$fecha_hora=date('Y-m-d') . " ". date('H:i:s'); //Fecha del sistema caracter
//1.crear logros principales
$logros = "y".$impano."logros";
$actividades = "y".$impano."activ";
$sql="select * from $file where campo1 < '90' and campo5 = '2' order by campo1, campo2, campo3, campo4";
$data=viewsql($sql);
foreach($data as $registros)
{
	$grado = intval(trim($registros->campo1));
	if ($grado == 12)
		$grado = "J";
	if ($grado == 13)
		$grado = "PJ";
	if ($grado == 14)
		$grado = "T";
	$areacod = intval(trim($registros->campo2));
	$materiacod = intval(trim($registros->campo3));
	$logrocod = intval(trim($registros->campo4));
	$nombre = trim($registros->campo6);
	$nombre2 = trim($registros->campo7);
	$nombre3 = trim($registros->campo8);
	$nombre4 = trim($registros->campo9);
	$nombre5 = trim($registros->campo10);
	if (substr($nombre,strlen($nombre)-1,1)=="-")
		$nombre = str_replace('-','',$nombre);
	else
		$nombre = $nombre." ";
	if ($nombre2!="")
		$nombre = $nombre.$nombre2;	
	if (substr($nombre,strlen($nombre)-1,1)=="-")
		$nombre = str_replace('-','',$nombre);
	else
		$nombre = $nombre." ";
	if ($nombre3!="")
		$nombre = $nombre.$nombre3;	
	if (substr($nombre,strlen($nombre)-1,1)=="-")
		$nombre = str_replace('-','',$nombre);
	else
		$nombre = $nombre." ";
	if ($nombre4!="")
		$nombre = $nombre.$nombre4;	
	if (substr($nombre,strlen($nombre)-1,1)=="-")
		$nombre = str_replace('-','',$nombre);
	else
		$nombre = $nombre." ";
	if ($nombre5!="")
		$nombre = $nombre.$nombre5;
	$nombre = trim($nombre);	
	$nombre = str_replace('?','�',$nombre);
	if ($nombre != "")
	{		
		$id_asignatura = 0;	
		$id_area = 0;
		$id_materia = 0;
		$sql = "select id_asignatura, id_area, id_materia from asignaturas where ano='$impano' and grado='$grado' and areacod='$areacod' and materiacod='$materiacod' limit 1";
		$datasig = viewsql($sql);
		foreach($datasig as $regasig)
		{
			$id_asignatura = $regasig->id_asignatura;
			$id_area = $regasig->id_area;
			$id_materia = $regasig->id_materia;
		}
		$periodo = 1;
		if ($logrocod >= 11 and $logrocod <= 20)
			$periodo = 2;
		if ($logrocod >= 21 and $logrocod <= 30)
			$periodo = 3;
		if ($logrocod >= 31)
			$periodo = 4;
		$periodos = " ".$periodo." ";
		$activmin = 3;
		$activpor = "33 33 34 ";
		if ($grado >= 4 and $grado <= 11)
			$activpor = "20 35 45 ";
		$sql= "INSERT INTO $logros (grado, id_asignatura,id_area,id_materia,logro,areacod,materiacod,logrocod,periodos,tipoedu,activmin,activpor) VALUES ('$grado','$id_asignatura','$id_area','$id_materia','$nombre','$areacod','$materiacod','$logrocod','$periodos','F','$activmin','$activpor')";
		execsql($sql);
		//CREAR ACTIVIDADES
		//if ($periodo == 1)
		//{
			$sql = "select id_logro from $logros where grado='$grado' and areacod='$areacod' and materiacod='$materiacod' and logrocod='$logrocod' limit 1";
			$datlogr = viewsql($sql);
			foreach($datlogr as $reglogr)
			{$id_logro = $reglogr->id_logro;}
			for($i=1; $i <= 3 ; $i++)
			{
				$actividad = "ACTIVIDAD ".$i;
				$codigo = "ACT".$i;
				$porcentaje = 0;
				if ($grado >= 4 and $grado <= 11)
				{
					if ($i == 1)
						$porcentaje = 20;
					if ($i == 2)
						$porcentaje = 35;
					if ($i == 3)
						$porcentaje = 45;
				}
				$sql= "INSERT INTO $actividades (id_asignatura,id_logro,periodo,actividad,porcentaje,codigo,numero) VALUES ('$id_asignatura','$id_logro','$periodo','$actividad','$porcentaje','$codigo','$i')";
				execsql($sql);
			}
		//}
	}//if no en blanco
}
//2.logros secundarios
$sql="select * from $file where campo1 < '90' and campo5 != '2' order by campo1, campo2, campo3, campo4";
$data=viewsql($sql);
foreach($data as $registros)
{
	$grado = intval(trim($registros->campo1));
	if ($grado == 12)
		$grado = "J";
	if ($grado == 13)
		$grado = "PJ";
	if ($grado == 14)
		$grado = "T";
	$areacod = intval(trim($registros->campo2));
	$materiacod = intval(trim($registros->campo3));
	$logrocod = intval(trim($registros->campo4));
	$logrotip = intval(trim($registros->campo5));
	$nombre = trim($registros->campo6);
	$nombre2 = trim($registros->campo7);
	$nombre3 = trim($registros->campo8);
	$nombre4 = trim($registros->campo9);
	$nombre5 = trim($registros->campo10);
	if (substr($nombre,strlen($nombre)-1,1)=="-")
		$nombre = str_replace('-','',$nombre);
	else
		$nombre = $nombre." ";
	if ($nombre2!="")
		$nombre = $nombre.$nombre2;	
	if (substr($nombre,strlen($nombre)-1,1)=="-")
		$nombre = str_replace('-','',$nombre);
	else
		$nombre = $nombre." ";
	if ($nombre3!="")
		$nombre = $nombre.$nombre3;	
	if (substr($nombre,strlen($nombre)-1,1)=="-")
		$nombre = str_replace('-','',$nombre);
	else
		$nombre = $nombre." ";
	if ($nombre4!="")
		$nombre = $nombre.$nombre4;	
	if (substr($nombre,strlen($nombre)-1,1)=="-")
		$nombre = str_replace('-','',$nombre);
	else
		$nombre = $nombre." ";
	if ($nombre5!="")
		$nombre = $nombre.$nombre5;
	$nombre = trim($nombre);	
	$nombre = str_replace('?','�',$nombre);		
	if ($logrotip == 1)	
		$sql="UPDATE $logros set logrosi = '$nombre' where grado='$grado' and areacod='$areacod' and materiacod='$materiacod' and logrocod='$logrocod' limit 1";
	else
		$sql="UPDATE $logros set logrono = '$nombre' where grado='$grado' and areacod='$areacod' and materiacod='$materiacod' and logrocod='$logrocod' limit 1";
	execsql($sql);
}
//3.logros observaciones
/*
if ($impano == 2012)
{
	$sql="select * from $file where campo1 > '90' order by campo4";
	$data=viewsql($sql);
	foreach($data as $registros)
	{
		$logrocod = intval(trim($registros->campo4));
		$nombre = trim($registros->campo6);
		$nombre2 = trim($registros->campo7);
		$nombre3 = trim($registros->campo8);
		$nombre4 = trim($registros->campo9);
		$nombre5 = trim($registros->campo10);
		if (substr($nombre,strlen($nombre)-1,1)=="-")
			$nombre = str_replace('-','',$nombre);
		else
			$nombre = $nombre." ";
		if ($nombre2!="")
			$nombre = $nombre.$nombre2;	
		if (substr($nombre,strlen($nombre)-1,1)=="-")
			$nombre = str_replace('-','',$nombre);
		else
			$nombre = $nombre." ";
		if ($nombre3!="")
			$nombre = $nombre.$nombre3;	
		if (substr($nombre,strlen($nombre)-1,1)=="-")
			$nombre = str_replace('-','',$nombre);
		else
			$nombre = $nombre." ";
		if ($nombre4!="")
			$nombre = $nombre.$nombre4;	
		if (substr($nombre,strlen($nombre)-1,1)=="-")
			$nombre = str_replace('-','',$nombre);
		else
			$nombre = $nombre." ";
		if ($nombre5!="")
			$nombre = $nombre.$nombre5;
		$nombre = trim($nombre);	
		$nombre = str_replace('?','�',$nombre);
		if ($nombre!="")
		{
			$niveles = " K P S ";	
			$sql= "INSERT INTO observaciones (observacion,observacod,niveles) VALUES ('$nombre','$logrocod','$niveles')";
			execsql($sql);
		}
	}
}
*/
disconnect();
echo"<script> alert('Logros Grabados.'); window.location.href='asignaturas.php';</script>";
?>
