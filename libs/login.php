<?php
/***********************************************************************************************
* @file        : login.php                                                                   *
* @brief       : Validar login del usuario e iniciar sesi�n y variables del usuario            *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 15-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 03-dic-2018                                                    *
***********************************************************************************************/

include("../database/database_master.php");
$login = strtoupper($_POST["login"]);
$pass = $_POST["pass"];
$clave = md5($pass);
$msgerror = "";
$instid = 0;
$usuid = 0;
$usulog = "";
$usunom = "";
$usucor = "";
$usutip = "";
$usunro = "";
$nivalu = "";
$nivmat = "";
$nivpro = "";
$nivasi = "";
$nivnot = "";
$nivfal = "";
$nivtra = "";
$nivpag = "";
$nivusu = "";
$nivpar = "";
connect();
$sql="select * from usuarios where login='$login' and password='$clave' and inactivar='0' limit 1";
$resultado=viewsql($sql);
foreach($resultado as $registro)
{
	$instid = intval($registro->id_institucion);
	$usuid = intval($registro->id_usuario);
	$usulog = $registro->login;
	$usunom = $registro->nombres." ".$registro->apellidos;
	$usucor = $registro->correo;
	$usutip = $registro->tipo;
	$usunro = $registro->id_tipo;
	$nivalu = $registro->nivalu;
	$nivmat = $registro->nivmat;
	$nivpro = $registro->nivpro;
	$nivasi = $registro->nivasi;
	$nivnot = $registro->nivnot;
	$nivfal = $registro->nivfal;
	$nivtra = $registro->nivtra;
	$nivpag = $registro->nivpag;
	$nivusu = $registro->nivusu;
	$nivpar = $registro->nivpar;
}
if ($usuid > 0 and $instid > 0)
{
	$instinom = "";
	$insticol = "";
	$instibot = "";
	$sql="select institucion, sigla, color_titulos, color_botones from instituciones WHERE id_institucion = '$instid' limit 1";
	$datinst=viewsql($sql);
	foreach($datinst as $reginst)
	{
		$instinom = $reginst->institucion." ".$reginst->sigla;
		$insticol = $reginst->color_titulos;
		$instibot = $reginst->color_botones;
	}
	//iniciar variables de sesi�n
        if ($instid < 10)
            $instid = "00".$instid;
        if ($instid > 9 and $instid < 100)
            $instid = "0".$instid;
	$anoaca = substr(date('Y-m-d'),0,4);
	session_start();
	$_SESSION["anoaca"] = $anoaca; //a�o academico
	$_SESSION["usuid"] = $usuid; //id usuario
	$_SESSION["usulog"] = $usulog; //login usuario
	$_SESSION["usunom"] = $usunom; //usuario nombre
	$_SESSION["usucor"] = $usucor; //usuario correo
	$_SESSION["usutip"] = $usutip; //usuario tipo
	$_SESSION["usunro"] = $usunro; //tipo id profesor o alumno
	$_SESSION["nivalu"] = $nivalu; //alumnos
	$_SESSION["nivmat"] = $nivmat; //matriculas
	$_SESSION["nivpro"] = $nivpro; //profesores
	$_SESSION["nivasi"] = $nivasi; //asignaturas
	$_SESSION["nivnot"] = $nivnot; //notas
	$_SESSION["nivfal"] = $nivfal; //fallas
	$_SESSION["nivtra"] = $nivtra; //transporte
	$_SESSION["nivpag"] = $nivpag; //pagos
	$_SESSION["nivusu"] = $nivusu; //usuarios
	$_SESSION["nivpar"] = $nivpar; //parametros
	$_SESSION["instid"] = $instid; //ID instituci�n
        $_SESSION["instinom"] = $instinom; //instituci�n
	$_SESSION["insticol"] = $insticol; //color titulos
	$_SESSION["instibot"] = $instibot; //color botones
	//header("Location: ../inicio.php");
	disconnect();
	echo "<META http-equiv=\"refresh\" content=\"0.1; url=../inicio.php\">";
}
else
{
	disconnect();
	$msgerror = "Login o Clave inv�lida!";
	echo "<META http-equiv=\"refresh\" content=\"0.1; url=../index.php?msgerror=$msgerror\">";
}
?>