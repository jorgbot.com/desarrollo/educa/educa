<?php
//IMPORTAR PROFESORES DESDE ARCHIVO TXT - DELIMITADO POR TABULACIONES
//SE ABRE LA TABLA FOXBASE CON EXCEL Y SE GRABA COMO TXT DELIMITADA
//TABLAS PROFESORES, PROFESORESCONT, Y2012PROFASIG

include("database/database.php");
connect();
$impano = "2012";
//desocupar tabla temporal
$archivo = "C:/xampp/htdocs/idic/recursos/noprofe".$impano.".txt";
$file = "importar";
execsql("delete from ".$file);
//cargar datos tabla temporal	
$sqlc="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sqlc);
$fecha_hora=date('Y-m-d') . " ". date('H:i:s'); //Fecha del sistema caracter
//1.crear profesor y contrato
$sql="select * from $file order by campo5";
$data=viewsql($sql);
foreach($data as $registros)
{
	$profesor = trim($registros->campo5);
	$profesor = str_replace('?','�',$profesor);
	$pos=strpos($profesor," ");
	$nom1 = trim(substr($profesor,0,$pos));
	$nom2 = trim(substr($profesor,$pos+1));
	$pos=strpos($nom2," ");
	$ape1 = trim(substr($nom2,$pos+1));
	$nom2 = trim(substr($nom2,0,$pos));
	$pos=strpos($ape1," ");
	$ape2 = "";
	if ($pos > 0)
	{
		$ape2 = trim(substr($ape1,$pos+1));
		$ape1 = trim(substr($ape1,0,$pos));
	}
	$nombres = $nom1." ".$nom2;
	$apellidos = $ape1." ".$ape2;
	$id_profesor = 0;
	$sql = "select id_profesor from profesores where profesor='$profesor' limit 1";
	$datprof = viewsql($sql);
	foreach($datprof as $regprof)
	{$id_profesor = $regprof->id_profesor;}
	if ($id_profesor == 0)
	{
		$sql= "INSERT INTO profesores (nombres, apellidos, profesor, tipo, estado) VALUES ('$nombres','$apellidos','$profesor','DOCENTE','ACTIVO')";
		execsql($sql);
		$sql = "select id_profesor from profesores where profesor='$profesor' limit 1";
		$datprof = viewsql($sql);
		foreach($datprof as $regprof)
		{$id_profesor = $regprof->id_profesor;}
		$sql= "INSERT INTO profesorescont (id_profesor, ano, estado, fchinicio, fchfinal) VALUES ('$id_profesor','$impano','ACTIVO','2012-02-01','2012-11-30')";
		execsql($sql);
	}
}
//2.crear asignaturas y grados al profesor
$profasig = "y".$impano."profasig";
$sql="select * from $file order by campo1, campo2, campo3, campo4";
$data=viewsql($sql);
foreach($data as $registros)
{
	$grado = intval(trim($registros->campo1));
	$areacod = intval(trim($registros->campo2));
	$materiacod = intval(trim($registros->campo3));
	$curso = intval(trim($registros->campo4));
	$profesor = trim($registros->campo5);
	$profesor = str_replace('?','�',$profesor);
	if ($grado == 12)
		$grado = "J";
	if ($grado == 13)
		$grado = "PJ";
	if ($grado == 14)
		$grado = "T";
	//actualizar grados profesor
	$id_profesor = 0;
	$sql = "select id_profesor from profesores where profesor='$profesor' limit 1";
	$datarea = viewsql($sql);
	foreach($datarea as $regarea)
	{$id_profesor = $regarea->id_profesor;}
	$grados = "";
	$sql = "select grados from profesorescont where id_profesor='$id_profesor' limit 1";
	$datarea = viewsql($sql);
	foreach($datarea as $regarea)
	{$grados = $regarea->grados;}
	$seekgrado = $grado." ";
	$pos = strpos($grados,$seekgrado);
	if ($pos <= 0) //no lo encuentra
	{
		if (strlen($grados) <= 0)
			$grados = " ".$grado." ";
		else
			$grados = $grados.$grado." ";
	}
	$sql = "UPDATE profesorescont SET grados = '$grados' WHERE id_profesor = '$id_profesor' LIMIT 1";
	execsql($sql);
	//actualizar asignatura profesor
	$id_asignatura = 0;
	$sql = "select id_asignatura from asignaturas where ano='$impano' and grado='$grado' and areacod='$areacod' and materiacod='$materiacod' limit 1";
	$datasig = viewsql($sql);
	foreach($datasig as $regasig)
	{$id_asignatura = $regasig->id_asignatura;}
	$sql= "INSERT INTO $profasig (id_profesor, profesor, id_asignatura, grado, curso) VALUES ('$id_profesor','$profesor','$id_asignatura','$grado','$curso')";
	execsql($sql);
}
disconnect();
echo"<script> alert('Profesores Grabados.'); window.location.href='profesores.php';</script>";
?>
