<?php
//IMPORTAR ASIGNATURAS DESDE ARCHIVO TXT - DELIMITADO POR TABULACIONES
//SE ABRE LA TABLA FOXBASE CON EXCEL Y SE GRABA COMO TXT DELIMITADA
//TABLAS AREAS, MATERIAS, ASIGNATURAS

$impano = "2012";
include("database/database.php");
connect();
//desocupar tabla temporal
$archivo = "C:/xampp/htdocs/democol/temporales/asig".$impano.".txt";
$file = "importar";
execsql("delete from ".$file);
//cargar datos tabla temporal	
$sqlc="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sqlc);
$fecha_hora=date('Y-m-d') . " ". date('H:i:s'); //Fecha del sistema caracter
//1.crear areas
$sql="select * from $file where campo3 <= '0' order by campo1, campo2, campo3";
$data=viewsql($sql);
foreach($data as $registros)
{
	$grado = intval(trim($registros->campo1));
	$areacod = intval(trim($registros->campo2));
	$nombre = trim($registros->campo4);
	if (trim($registros->campo5)!="")
		$nombre = $nombre." ".trim($registros->campo5);
	$nombre = str_replace(':','',$nombre);	
	$id_area = 0;
	$sql= "INSERT INTO areas (grado, area, areacod) VALUES ('$grado', '$nombre', '$areacod')";
	execsql($sql);
}
//2.crear materias
$sql="select * from $file where campo3 > '0' order by campo1, campo2, campo3";
$data=viewsql($sql);
foreach($data as $registros)
{
	$grado = intval(trim($registros->campo1));
	$areacod = intval(trim($registros->campo2));
	$materiacod = intval(trim($registros->campo3));
	$nombre = trim($registros->campo4);
	if (trim($registros->campo5)!="")
		$nombre = $nombre." ".trim($registros->campo5);
	$nombre = str_replace(':','',$nombre);	
	$id_materia = 0;
	$id_area = 0;
	$area = "";
	$sql = "select id_materia, materia from materias where grado = '$grado' and materia = '$nombre' limit 1";
	$datmate = viewsql($sql);
	foreach($datmate as $regmate)
	{
		$id_materia = $regmate->id_materia;
		$materia = $regmate->materia;
	}
	if ($id_materia <= 0)
	{
		$sql = "select id_area, area from areas where grado = '$grado' and areacod = '$areacod' limit 1";
		$datarea = viewsql($sql);
		foreach($datarea as $regarea)
		{
			$id_area = $regarea->id_area;
			$area = $regarea->area;
		}
		$sql= "INSERT INTO materias (id_area, area, materia, grado) VALUES ('$id_area', '$area', '$nombre', '$grado')";
		execsql($sql);
	}
}
//2.crear asignaturas con intensidad horaria
/*
$sql="select * from $file where campo6 > '0' order by campo1, campo2, campo3";
$data=viewsql($sql);
foreach($data as $registros)
{
	$grado = intval(trim($registros->campo1));
	$areacod = intval(trim($registros->campo2));
	$materiacod = intval(trim($registros->campo3));
	$nombre = trim($registros->campo4);
	if (trim($registros->campo5)!="")
		$nombre = $nombre." ".trim($registros->campo5);
	$nombre = str_replace(':','',$nombre);	
	$intensidad = intval(trim($registros->campo6));
	$codigo = $areacod.$materiacod;
	$id_materia = 0;
	$materia = "";
	$id_area = 0;
	$area = "";
	$sql = "select id_area, area from areas where grado = '$grado' and areacod = '$areacod' limit 1";
	$datarea = viewsql($sql);
	foreach($datarea as $regarea)
	{
		$id_area = $regarea->id_area;
		$area = $regarea->area;
	}
	if ($materiacod > 0)
	{
		$sql = "select id_materia, materia from materias where materia = '$nombre' limit 1";
		$datmate = viewsql($sql);
		foreach($datmate as $regmate)
		{
			$id_materia = $regmate->id_materia;
			$materia = $regmate->materia;
		}
	}
	if ($grado == 12)
		$grado = "-2";
	if ($grado == 13)
		$grado = "-1";
	if ($grado == 14)
		$grado = "0";
	$color = "";
	$sql = "select gradocolor, gradofont from grados where grado = '$grado' limit 1";
	$datgrad = viewsql($sql);
	foreach($datgrad as $reggrad)
	{$color = $reggrad->gradocolor;}
	$activmin = 3;
	$activpor = "33 33 34 ";
	if ($grado >= 4 and $grado <= 11)
		$activpor = "20 35 45 ";
	$sql= "INSERT INTO asignaturas (ano, grado, id_area, area, id_materia, materia, codigo, areacod, materiacod, intensidad, activmin, activpor, boletin, notas, perdida, gradocolor) VALUES ('$impano', '$grado', '$id_area', '$area', '$id_materia', '$materia', '$codigo', '$areacod', '$materiacod', '$intensidad', '$activmin', '$activpor', '1', '1', '1', '$color')";
	execsql($sql);
} //fin registros
*/
$sql = "update areas set grado = '-2' where grado = '12'";
execsql($sql);
$sql = "update areas set grado = '-1' where grado = '13'";
execsql($sql);
$sql = "update areas set grado = '0' where grado = '14'";
execsql($sql);
$sql = "update materias set grado = '-2' where grado = '12'";
execsql($sql);
$sql = "update materias set grado = '-1' where grado = '13'";
execsql($sql);
$sql = "update materias set grado = '0' where grado = '14'";
execsql($sql);
disconnect();
echo"<script> alert('Asignaturas Grabadas.'); window.location.href='alumnos.php';</script>";
?>
