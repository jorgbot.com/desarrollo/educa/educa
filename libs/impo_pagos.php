<?php
//IMPORTAR PAGOS PENSIONES, MATRICULAS Y TRANSPORTE DESDE ARCHIVO TXT - DELIMITADO POR TABULACIONES
//SE ABRE LA TABLA FOXBASE CON EXCEL Y SE GRABA COMO TXT DELIMITADA CON TABULACIONES
//TABLAS QUE CARGAMOS: Y9999PAGOSENT, Y9999PAGOSTRA, Y9999PAGOS, Y9999PAGOSDET

include_once("../database/database.php");
include_once("../database/datos_pagos.php");
include_once("../webparam.php");
$impano = isset($_GET['impano']) ? $_GET['impano'] : null ;
$ano = $impano;
$fecha_hora=date('Y-m-d') . " ". date('H:i:s');
connect();
//Desocupar tabla entradas pensiones
$fileent = "y".$ano."pagosent";
execsql("TRUNCATE TABLE ".$fileent);
//Desocupar tabla entradas transporte
$filetra = "y".$ano."pagostra";
execsql("TRUNCATE TABLE ".$filetra);
//Desocupar tabla pagos
$filepag = "y".$ano."pagos";
execsql("TRUNCATE TABLE ".$filepag);
//Desocupar tabla pagos detalle mes
$filedet = "y".$ano."pagosdet";
execsql("TRUNCATE TABLE ".$filedet);
//HOJAS DE ENTRADA PENSION
$file = "importar";
execsql("TRUNCATE TABLE ".$file);
$archivo = ROOT_DIR."/temporales/MANUDIA".$impano.".txt";
$sql="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sql);
$sql="select campo1, campo2, campo3, campo4 from $file order by CAST(campo1 AS SIGNED)";
$data=viewsql($sql);
$nument = 0;
foreach($data as $registros)
{
	$nument = $nument + 1;
	$id_entrada = intval(trim($registros->campo1));
	$anoent = intval(trim($registros->campo2));
	$mesent = intval(trim($registros->campo3));
	$diaent = intval(trim($registros->campo4));
	if ($diaent < 9)
		$diaent="0".$diaent;
	if ($mesent < 9)
		$mesent="0".$mesent;
	$fchentrada = "20".$anoent."-".$mesent."-".$diaent;
	if ($id_entrada > $nument)
	{
		$dif = $id_entrada - $nument;
		for($i=1; $i<=$dif ;$i++)
		{
			$sql= "INSERT INTO $fileent (estado) VALUES ('CERRADA')";
			execsql($sql);
			$nument = $nument + 1;
		}
	}
	$sql= "INSERT INTO $fileent (fchentrada,estado) VALUES ('$fchentrada','CERRADA')";
	execsql($sql);
}
//PAGOS PENSION
$porcentaje = 0;
$sql="SELECT interes FROM anoslect WHERE ano = '$ano' LIMIT 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcentaje = $reganol->interes;}
$file = "importar";
execsql("TRUNCATE TABLE ".$file);
$archivo = ROOT_DIR."/temporales/MAHOJA".$impano.".txt";
$sql="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sql);
$sql="select campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8, campo9, campo10, campo11, campo12 from $file";
$data=viewsql($sql);
foreach($data as $registros)
{
	$id_alumno = intval(trim($registros->campo1));
	$id_entrada = intval(trim($registros->campo2));
	$apellidos = trim($registros->campo3)." ".trim($registros->campo3);	
	$nombres = trim($registros->campo5);
	$mes = intval(trim($registros->campo7));
	$valor = intval(trim($registros->campo8));
	$banco = intval(trim($registros->campo9));
	$id_banco = $banco;
	if ($banco==1)
		$id_banco = 2;
	if ($banco==2)
		$id_banco = 1;
	$diapag = intval(trim($registros->campo10));
	$mespag = intval(trim($registros->campo11));
	$anopag = intval(trim($registros->campo12));
	if ($diapag < 9)
		$diapag="0".$diapag;
	if ($mespag < 9)
		$mespag="0".$mespag;
	$fchpago = "20".$anopag."-".$mespag."-".$diapag;
	if ($id_entrada < 2000)
	{
		//traer fecha de entrada
		$fchentrada = "";
		$sql = "select fchentrada from $fileent where id_entrada='$id_entrada' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{$fchentrada = $reg->fchentrada;}
		//traer datos matricula
		$id_matricula = 0;
		$beca = 0;
		$grado = "";
		$curso = "";
		$sql = "select id_matricula, grado, curso, beca from matriculas where id_alumno='$id_alumno' and ano = '$ano' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$id_matricula = $reg->id_matricula;
			$grado = $reg->grado;
			$curso = $reg->curso;
			$beca = $reg->beca;
		}
		//crear registro pago total
		if ($id_matricula > 0)
		{
			$gradovalmat = 0;
			$gradovalpen = 0;
			$sql="SELECT matricula, pension FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
			$dat=viewsql($sql);
			foreach($dat as $reg)
			{
				$gradovalmat = $reg->matricula;
				$gradovalpen = $reg->pension;
			}
			if ($beca > 0)
			{
				$gradovalpen = round($gradovalpen*($beca/100),0);
				if ($beca==100)
					$gradovalpen = 0;
			}
			$sql="UPDATE matriculas SET beca='$beca',matricula='$gradovalmat',pension02='$gradovalpen',pension03='$gradovalpen',pension04='$gradovalpen',pension05='$gradovalpen',pension06='$gradovalpen',pension07='$gradovalpen',pension08='$gradovalpen',pension09='$gradovalpen',pension10='$gradovalpen',pension11='$gradovalpen' WHERE id_matricula = '$id_matricula' limit 1";
			execsql($sql);
			$sql = "INSERT INTO $filepag (id_alumno,id_banco,id_entrada,fchentrada,tipo,destino,ano,grado,curso,fchpago,valor,valor_con,neto,porcentaje) VALUES ('$id_alumno','$id_banco','$id_entrada','$fchentrada','P','1','$ano','$grado','$curso','$fchpago','$valor','$valor','$valor','$porcentaje')";
			execsql($sql);
		}
	}
} //fin alumnos
//crear registros pagos pension por mes
$sql = "select id_alumno from matriculas where ano = '$ano' order by id_alumno";
$dat = viewsql($sql);
foreach($dat as $reg)
{
	$id_alumno = $reg->id_alumno;
	$tipo = "P";
	reliquidar_saldos($ano,$id_alumno,$tipo);
}

//HOJAS DE ENTRADA TRANSPORTE
$file = "importar";
execsql("TRUNCATE TABLE ".$file);
$archivo = ROOT_DIR."/temporales/TRHOJA".$impano.".txt";
$sql="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sql);
$sql="select campo2, campo7 from $file order by CAST(campo2 AS SIGNED)";
$data=viewsql($sql);
foreach($data as $registros)
{
	$id_entrada = intval(trim($registros->campo2));
	$mes = intval(trim($registros->campo7));
	$tmes = $mes;
	if ($mes < 10)
		$tmes = "0".$mes;
	$fchentrada = $ano."-".$tmes."-01";
	$id = 0;
	$sql = "select id_entrada from $filetra where id_entrada='$id_entrada' limit 1";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{$id = $reg->id_entrada;}
	if ($id <= 0 and $id_entrada < 2000)
	{
		$sql= "INSERT INTO $filetra (fchentrada,estado) VALUES ('$fchentrada','CERRADA')";
		execsql($sql);
	}
}

//PAGOS TRANSPORTE
$sql="select campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8, campo9, campo10, campo11, campo12, campo13 from $file order by CAST(campo2 AS SIGNED)";
$data=viewsql($sql);
foreach($data as $registros)
{
	$id_alumno = intval(trim($registros->campo1));
	$id_entrada = intval(trim($registros->campo2));
	$mes = intval(trim($registros->campo7));
	$valor = intval(trim($registros->campo8));
	$banco = intval(trim($registros->campo10));
	$id_banco = $banco;
	if ($banco==1)
		$id_banco = 2;
	if ($banco==2)
		$id_banco = 1;
	$diapag = intval(trim($registros->campo11));
	$mespag = intval(trim($registros->campo12));
	$anopag = intval(trim($registros->campo13));
	if ($diapag < 9)
		$diapag="0".$diapag;
	if ($mespag < 9)
		$mespag="0".$mespag;
	$fchpago = "20".$anopag."-".$mespag."-".$diapag;
	//evitar codigos de ajustes 9999
	if ($id_entrada < 2000)
	{
		//traer fecha de entrada transportes
		$fchentrada = "";
		$sql = "select fchentrada from $filetra where id_entrada='$id_entrada' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{$fchentrada = $reg->fchentrada;}
		//traer datos transporte
		$id_transporte = 0;
		$grado = "";
		$curso = "";
		$sql = "select id_transporte, grado, curso from transporte where id_alumno='$id_alumno' and ano = '$ano' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$id_transporte = $reg->id_transporte;
			$grado = $reg->grado;
			$curso = $reg->curso;
		}
		//crear registro pago total
		if ($id_transporte > 0)
		{
			$sql = "INSERT INTO $filepag (id_alumno,id_banco,id_entrada,fchentrada,tipo,ano,grado,curso,fchpago,valor,valor_con,neto) VALUES ('$id_alumno','$id_banco','$id_entrada','$fchentrada','T','$ano','$grado','$curso','$fchpago','$valor','$valor','$valor')";
			execsql($sql);
		}
	}
} //fin alumnos
//crear registros pagos transporte por mes
$sql = "select id_alumno from transporte where ano = '$ano' order by id_alumno";
$dat = viewsql($sql);
foreach($dat as $reg)
{
	$id_alumno = $reg->id_alumno;
	$tipo = "T";
	reliquidar_saldos($ano,$id_alumno,$tipo);
}
disconnect();
echo"<script> alert('Importación de Pagos Finalizada.'); window.close();</script>";
?>
