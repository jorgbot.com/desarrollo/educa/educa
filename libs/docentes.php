<?php
/*** librerias funciones de docentes ***/

/***********************************************/
/* funcion que recibe dos fechas dia-mes-a�o   */
/* entrega tiempo trabajado meses de 30 dias   */ 
/***********************************************/
function tiempo_trabajado($fchinicio,$fchfinal)
{
    $tiempo = "";
    $anos = intval(substr($fchfinal,0,4))-intval(substr($fchinicio,0,4));
    $meses = 0;
    $dias = 0;
    $diaini = intval(substr($fchinicio,8,2));
    $diafin = intval(substr($fchfinal,8,2));
    if ($diaini >= 30)
        $diaini = 30;
    if ($diafin >= 30)
        $diafin = 30;
    if ($anos <= 0)
    {
	$meses = intval(substr($fchfinal,5,2)) - intval(substr($fchinicio,5,2)) - 1;
	$dias = (30-$diaini) + $diafin;
    }
    else {
	$anos = $anos - 1;
	$meses = $anos * 12;
	$meses = $meses + (12 - intval(substr($fchinicio,5,2))); 
	$meses = $meses + intval(substr($fchfinal,5,2)) - 1; 
	$dias = (30-$diaini) + $diafin;
    }
    if ($dias >= 28)
    {
	$dias = $dias - 28;
	$meses = $meses + 1;
	if ($dias >= 30){
        	$dias =0;
		$meses = $meses + 1;
	}
	else {
		$dias = $dias -2;
	}
    }
    $tiempo = $meses."M ";
    if ($dias > 0)
        $tiempo = $tiempo.$dias."D";
    return($tiempo);
}

/***********************************************/
/* funcion que calcula carga academica del docente   */
/* entrega horas trabajadas carga academica          */ 
/***********************************************/
function tiempo_carga($ano,$id_profesor)
{
    $horas = 0;
    $profasig = "y".$ano."profasig";
    $sql="SELECT SUM(asignaturas.intensidad) AS suma, $profasig.id_profesor FROM asignaturas, $profasig WHERE $profasig.id_profesor = '$id_profesor' AND $profasig.id_asignatura = asignaturas.id_asignatura";
    $datasig = viewsql($sql);
    foreach($datasig as $regasig)
    {$horas = $regasig->suma;}
    $horas = intval($horas);
    return($horas);
}

?>