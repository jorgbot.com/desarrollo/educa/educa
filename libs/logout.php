<?php
/***********************************************************************************************
* @file        : logout.php                                                                    *
* @brief       : Borra variables de sesi�n y destruye la sesi�n                                *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 15-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 15-feb-2012                                                    *
***********************************************************************************************/

session_start();
unset($_SESSION["anoaca"]);
unset($_SESSION["usuid"]);
unset($_SESSION["usulog"]);
unset($_SESSION["usunom"]);
unset($_SESSION["usucor"]);
unset($_SESSION["usutip"]);
unset($_SESSION["usunro"]);
unset($_SESSION["nivalu"]);
unset($_SESSION["nivmat"]);
unset($_SESSION["nivpro"]);
unset($_SESSION["nivasi"]);
unset($_SESSION["nivnot"]);
unset($_SESSION["nivfal"]);
unset($_SESSION["nivtra"]);
unset($_SESSION["nivpag"]);
unset($_SESSION["nivusu"]);
unset($_SESSION["nivpar"]);
unset($_SESSION["instid"]);
unset($_SESSION["instinom"]);
unset($_SESSION["insticol"]);
unset($_SESSION["instibot"]);
session_destroy();
header("Location: ../index.php");
?>