<?php 
/***********************************************************************************************
* @file        : image_resize.php                                                              *
* @brief       : Ajustar tama�o de imagenes cargadas                                           *
*              : Verificar en php.ini que la linea extension=php_gd2.dll est� descomentareada  *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 15-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 15-feb-2012                                                    *
***********************************************************************************************/

function resize_image($upfile,$limitw,$limith)
{
//	$upfile '/path/to/file.jpg';
//	Header("Content-type: image/jpeg");
	$size = getimagesize($upfile); // Read the size
	$width = $size[0];
	$height = $size[1];
	$tipo = $size[2];
	$nw_width = $size[0];
	$nw_height = $size[1];
	if ($height > $limith)
		{$nw_width=round(($width*$limith)/$height); $nw_height=$limith;}
	if ($nw_width > $limitw)
		{$nw_height=round(($nw_height*$limitw)/$nw_width); $nw_width=$limitw;}
	// Increase memory limit to support larger files
	ini_set('memory_limit', '200M');
	// Create the new image GIF
	if ($tipo==1 and ($nw_width != $width or $nw_height != $height))
	{
		$src = imagecreatefromgif($upfile);
		$dst = imagecreatetruecolor($nw_width, $nw_height);
		imagecopyresized($dst, $src, 0, 0, 0, 0, $nw_width, $nw_height, $width, $height);
		imagegif($dst,$upfile);
	}
	// Create the new image JPEG
	if ($tipo==2 and ($nw_width != $width or $nw_height != $height))
	{
		$src = imagecreatefromjpeg($upfile);
		$dst = imagecreatetruecolor($nw_width, $nw_height);
		imagecopyresized($dst, $src, 0, 0, 0, 0, $nw_width, $nw_height, $width, $height);
		imagejpeg($dst,$upfile);
	}
	// Create the new image PNG
	if ($tipo==3 and ($nw_width != $width or $nw_height != $height))
	{
		$src = imagecreatefrompng($upfile);
		$dst = imagecreatetruecolor($nw_width, $nw_height);
		imagecopyresized($dst, $src, 0, 0, 0, 0, $nw_width, $nw_height, $width, $height);
		imagepng($dst,$upfile);
	}
	// Destroy the images
	if ($tipo <= 3 and ($nw_width != $width or $nw_height != $height))
	{
		imagedestroy($src);
		imagedestroy($dst);
	}	
}
/****** recibe nombre y ruta del archivo de imagen *****/
/****** recibe largo a ajustar en pixeles *****/
/****** retorna numeros de nuevo ancho y largo de la imagen *****/
function resize_width($imagen,$largo)
{
	$size = getimagesize($imagen);
	$pixw=$size[0]; // ancho en pixels
	$pixh=$size[1]; // largo en pixels
	if ($pixh > $largo)
		{$nw_ancho=round(($pixw*$largo)/$pixh);}
	else
		{$nw_ancho=$pixw;}
	return($nw_ancho);
}
function resize_height($imagen,$ancho)
{
	$size = getimagesize($imagen);
	$pixw=$size[0]; // ancho en pixels
	$pixh=$size[1]; // largo en pixels
	if ($pixw > $ancho)
		{$nw_largo=round(($pixh*$ancho)/$pixw);}
	else
		{$nw_largo=$pixh;}
	return($nw_largo);
}
?>
