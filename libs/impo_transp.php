<?php
//IMPORTAR PAGOS TRANSPORTES ALUMNOS, RUTAS Y CONDUCTORES DESDE ARCHIVO TXT - DELIMITADO POR TABULACIONES
//SE ABRE LA TABLA FOXBASE CON EXCEL Y SE GRABA COMO TXT DELIMITADA
//TABLAS TRANSPORTE, RUTAS, CONDUCTORES

include_once("../database/database.php");
$impano = isset($_GET['impano']) ? $_GET['impano'] : null ;
$ano = $impano;
$fecha_hora=date('Y-m-d') . " ". date('H:i:s');
connect();
//CARGAR RUTAS
$file = "importar";
execsql("TRUNCATE TABLE ".$file);
$archivo = "C:/xampp/htdocs/democol/temporales/TRCONDU".$impano.".txt";
$sql="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sql);
$sql="select campo1, campo2, campo3, campo4, campo5, campo6, campo7, campo8, campo9, campo10, campo11, campo12, campo13, campo14, campo15, campo16 from $file order by CAST(campo1 AS SIGNED)";
$data=viewsql($sql);
$nument = 0;
foreach($data as $registros)
{
	$id_ruta = intval(trim($registros->campo1));
	$recorrido = trim($registros->campo16);
	//verificar que no exista ruta
	$nruta = 0;
	$sql="SELECT id_ruta FROM rutas WHERE ano = '$ano' AND ruta = '$id_ruta' LIMIT 1";
	$dat=viewsql($sql);
	foreach($dat as $reg)
	{$nruta = $reg->id_ruta;}
	//crear ruta
	if ($nruta <= 0)
	{
		$sql= "INSERT INTO rutas (ruta,ano,recorrido,id_conductor) VALUES ('$id_ruta','$ano','$recorrido','$id_ruta')";
		execsql($sql);
	}
	//crear conductor
	$apellidos = trim($registros->campo2)." ".trim($registros->campo3);	
	$apellidos = str_replace('?','�',$apellidos);
	$nombres = trim($registros->campo4);	
	$nombres = str_replace('?','�',$nombres);
	$docunum = intval(trim($registros->campo5));
	$docutip = "CC";
	$direccion = trim($registros->campo6);	
	$barrio = trim($registros->campo7);	
	$telefono = trim($registros->campo8);	
	$celular = trim($registros->campo9);	
	$color = trim($registros->campo10);	
	$placa = trim($registros->campo11);	
	$modelo = trim($registros->campo12);	
	$marca = trim($registros->campo13);	
	$fchingreso = trim($registros->campo14)."-02-01";
	$capacidad = trim($registros->campo15);
	if ($modelo <= 15)
	{
		if ($modelo < 10)
			$modelo = "200".$modelo;
		else
			$modelo = "20".$modelo;
	}
	else
		$modelo = "19".$modelo;
	//verificar que no exista conductor
	$id_conductor = 0;
	$sql="SELECT id_conductor FROM conductores WHERE docunum = '$docunum' LIMIT 1";
	$dat=viewsql($sql);
	foreach($dat as $reg)
	{$id_conductor = $reg->id_conductor;}
	//crear conductor
	if ($id_conductor <= 0)
	{
		$sql = "INSERT INTO conductores (apellidos, nombres, docunum, docutip, docuexp, direccion, barrio, ciudad, pais,telefono, celular, placa, marca, color, modelo, capacidad, fchingreso, estado) ".
		"VALUES ('$apellidos', '$nombres', '$docunum', '$docutip', 'BOGOTA', '$direccion', '$barrio', 'BOGOTA', 'COLOMBIA', '$telefono', '$celular', '$placa', '$marca', '$color', '$modelo', '$capacidad', '$fchingreso', 'ACTIVO')";
		execsql($sql);
	}
}
//CARGAR SERVICIO DE TRANSPORTE
$valtra = 0;
$sql="SELECT transporte FROM gradosval WHERE ano = '$ano' LIMIT 1";
$dat=viewsql($sql);
foreach($dat as $reg)
{
	$valtra = $reg->transporte;
}
$sql = "DELETE FROM transporte WHERE ano = '$ano' ";
execsql($sql);
$file = "importar";
execsql("TRUNCATE TABLE ".$file);
$archivo = "C:/xampp/htdocs/democol/temporales/TRANS".$impano.".txt";
$sql="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sql);
$sql="select * from $file";
$data=viewsql($sql);
foreach($data as $registros)
{
	$id_alumno = intval(trim($registros->campo31));
	$id_ruta = intval(trim($registros->campo1));
	$codigo = intval(trim($registros->campo2));
	$direccion = trim($registros->campo7);
	$barrio = trim($registros->campo8);
	$telefono = trim($registros->campo9);
	$telefofi = trim($registros->campo10);
	$familiar = trim($registros->campo11);
	$detalles = trim($registros->campo12);
	$jornada = intval(trim($registros->campo24));
	$celular = trim($registros->campo32); 
	//Familiar ruta
	$id_familiar = 0;
	$sql = "select id_familiar, apellidos, nombres from familiares where id_alumno='$id_alumno' ";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		$nom1 = trim($reg->nombres)." ".trim($reg->apellidos);
		$nom2 = trim($reg->apellidos)." ".trim($reg->nombres);
		if ($nom1 == $familiar or $nom2 == $familiar)
		{
			$id_familiar = $reg->id_familiar;
			$sql = "UPDATE familiares SET barrio='$barrio',direccion='$direccion',telefono='$telefono',telefofi='$telefofi',celular='$celular' WHERE id_familiar='$id_familiar' LIMIT 1";
			execsql($sql);
		}
	}
	//traer datos matricula
	$id_matricula = 0;
	$sql = "select id_matricula, grado, curso, fecha, fchretiro from matriculas where id_alumno='$id_alumno' and ano = '$ano' limit 1";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		$id_matricula = $reg->id_matricula;
		$grado = $reg->grado;
		$curso = $reg->curso;
		$fchingreso = $reg->fecha;
		$fchretiro = $reg->fchretiro;
		$estado = "ACTIVO";
		if (substr($fchretiro,0,4)!="0000")
			$estado = "RETIRADO";
	}
	//crear registro pago total
	if ($id_matricula > 0)
	{
		$sql= "INSERT INTO transporte (id_alumno,ano,grado,curso,codigo,estado,fchingreso,fchretiro,id_ruta,id_familiar,jornada,transp02,transp03,transp04,transp05,transp06,transp07,transp08,transp09,transp10,transp11,detalles) ".
		"VALUES ('$id_alumno','$ano','$grado','$curso','$codigo','$estado','$fchingreso','$fchretiro','$id_ruta','$id_familiar','$jornada','$valtra','$valtra','$valtra','$valtra','$valtra','$valtra','$valtra','$valtra','$valtra','$valtra','$detalles')";
		execsql($sql);
	}
} //fin alumnos
disconnect();
echo"<script> alert('Importaci�n Transporte Finalizada.'); window.close();</script>";
?>
