<?php
/*** librerias para manejo y seleccion de fechas ***/

/******************************************/
/* funcion que recibe fecha dia-mes-a�o   */
/* entrega variable fecha a�o-mes-dia      */ 
/*****************************************/
function fec_amd($fecdma)
{
	if (substr($fecdma,4,1)=="-")
                return($fecdma);
	$rfec = "";
	if (substr($fecdma,0,2)!="00" and $fecdma!="")
		$rfec=substr($fecdma,6,4)."-".substr($fecdma,3,3).substr($fecdma,0,2); //convertir a A�O-MES-DIA
	return($rfec);
}
/******************************************/
/* funcion que recibe fecha a�o-mes-dia   */
/* entrega variable fecha dia-mes-a�o     */ 
/*****************************************/
function fec_dma($fecamd)
{
	if (substr($fecamd,2,1)=="-")
                return($fecamd);
	$rfec = "";
	if (substr($fecamd,0,2)!="00" and $fecamd!="")
		$rfec=substr($fecamd,8,2)."-".substr($fecamd,5,3).substr($fecamd,0,4); //convertir a DIA-MES-A�O
	return($rfec);
}
/******************************************/
/* funcion que recibe fecha a�o-mes-dia   */
/* entrega variable fecha mes-dia-a�o     */ 
/*****************************************/
function fec_mda($fecamd)
{
	$rfec = "";
	if (substr($fecamd,0,2)!="00" and $fecamd!="")
		$rfec=substr($fecamd,5,3).substr($fecamd,8,2)."-".substr($fecamd,0,4); //convertir a MES-DIA-A�O
	return($rfec);
}
/******************************************/
/* funcion que captura el dia en combo   */
/* recibe fecha texto a�o-mes-dia        */ 
/* envia  variable  $nomcampo            */ 
/*****************************************/
function select_dia($fecini,$nomcampo)
{
	$dini=strval(substr($fecini,8,2));
	echo "<select name='$nomcampo' style='font-size: 10px; color: #000000; width: 50;' onKeyDown='if(event.keyCode==13) event.keyCode=9;'>\n";
	echo "<option value=''>D�a</option>\n";
	for($i=1; $i<32 ;$i++)
	{
		if ($i < 10)
			{$dia="0".$i;}
		else
			{$dia=$i;}
		$sel="";
		if($i==$dini)	
			{$sel="selected";}
		echo "<option value='$dia' $sel>$dia</option>\n";
	}  
	echo "</select>\n";
}
/******************************************/
/* funcion que captura el mes en combo   */
/* recibe fecha texto a�o-mes-dia        */ 
/* envia  variable  $selmes             */ 
/*****************************************/
function select_mes($fecini,$nomcampo)
{
	// OJO ARREGLOS EN PHP INICIAN EN POSICION 0 
	$mini=strval(substr($fecini,5,2));
	$mes[ ]="Mes";
	$valor[ ]="";
	$mes[ ]="ENE";
	$valor[ ]="01";
	$mes[ ]="FEB";
	$valor[ ]="02";
	$mes[ ]="MAR";
	$valor[ ]="03";
	$mes[ ]="ABR";
	$valor[ ]="04";
	$mes[ ]="MAY";
	$valor[ ]="05";
	$mes[ ]="JUN";
	$valor[ ]="06";
	$mes[ ]="JUL";
	$valor[ ]="07";
	$mes[ ]="AGO";
	$valor[ ]="08";
	$mes[ ]="SEP";
	$valor[ ]="09";
	$mes[ ]="OCT";
	$valor[ ]="10";
	$mes[ ]="NOV";
	$valor[ ]="11";
	$mes[ ]="DIC";
	$valor[ ]="12";
	echo "<select name='$nomcampo' id='mes' style='font-size: 10px; color: #000000;' onKeyDown='if(event.keyCode==13) event.keyCode=9;'>\n";
	for($i=0; $i<13 ;$i++)
	{
		$sel="";
		if($i==$mini)	
			{$sel="selected";}
		echo "<option value='$valor[$i]' $sel>$mes[$i]</option>\n";
	}
	echo "</select>\n";
}
/******************************************/
/* funcion que captura el a�o en combo    */
/* recibe fecha texto a�o-mes-dia        */ 
/* envia variable  $selano              */ 
/*****************************************/
function select_ano($fecvar,$nomcampo)
{
	$afec = intval(substr($fecvar,0,4)); //a�o de la variable
	$aini = 1930;
	$afin = intval(substr(date('Y-m-d'),0,4))+1;
	echo "<select name='$nomcampo' style='font-size: 10px; color: #000000;' onKeyDown='if(event.keyCode==13) event.keyCode=9;'>\n";
	echo "<option value='' selected>A�o</option>\n";
	for($i=$afin; $i >= $aini ; $i--)
	{
		$nano = $i;
		$sel="";
		if($nano==$afec and $afec > 0)	
			{$sel="selected";}
		echo "<option value='$nano' $sel>$nano</option>\n";
	}
	echo "</select>\n";
}
/**************************************************/
/* funcion que convierte numero dia a texto       */
/* recibe variables $dia = numero del dia         */
/**************************************************/
function nombre_dia($dia)
{
$tdia="";
switch($dia)
 {
	case "0";
		$tdia="Domingo";
	break;
	case "1";
		$tdia="Lunes";
	break;
	case "2";
		$tdia="Martes";
	break;
	case "3";
		$tdia="Miercoles";
	break;
	case "4";
		$tdia="Jueves";
	break;
	case "5";
		$tdia="Viernes";
	break;
	case "6";
		$tdia="Sabado";
	break;
 }
return($tdia);
}
/**************************************************/
/* funcion que convierte numero mes a texto       */
/* recibe variables $mes = numero del mes         */
/**************************************************/
function nombre_mes($mes)
{
$tmes="";
switch($mes)
 {
    case "01";
	$tmes="Enero";
	break;
    case "02";
	$tmes="Febrero";
	break;
    case "03";
	$tmes="Marzo";
	break;
    case "04";
	$tmes="Abril";
	break;
    case "05";
	$tmes="Mayo";
	break;
    case "06";
	$tmes="Junio";
	break;
    case "07";
	$tmes="Julio";
	break;
    case "08";
	$tmes="Agosto";
	break;
    case "09";
	$tmes="Septiembre";
	break;
    case "10";
	$tmes="Octubre";
	break;
    case "11";
	$tmes="Noviembre";
	break;
    case "12";
	$tmes="Diciembre";
	break;
 }
return($tmes);
}
/**************************************************/
/* funcion que convierte nombre mes a numero      */
/* recibe variables $mes = nombre o numero mes    */
/**************************************************/
function numero_mes($mes)
{
$nmes="";
if (intval($mes) > 0)
{
	$nmes=$mes;
	if (strlen($mes)<2)
		$nmes="0".$mes;
}
else
{
	$mes=strtoupper($mes);
	switch($mes)
	 {
		case "ENE";
		$nmes="01";
		break;
		case "FEB";
		$nmes="02";
		break;
		case "MAR";
		$nmes="03";
		break;
		case "ABR";
		$nmes="04";
		break;
		case "MAY";
		$nmes="05";
		break;
		case "JUN";
		$nmes="06";
		break;
		case "JUL";
		$nmes="07";
		break;
		case "AGO";
		$nmes="08";
		break;
		case "SEP";
		$nmes="09";
		break;
		case "OCT";
		$nmes="10";
		break;
		case "NOV";
		$nmes="11";
		break;
		case "DIC";
		$nmes="12";
		break;
	 }
}
return($nmes);
}
/**************************************************/
/* fecha con nombre largo del DIA Y  corto MES    */
/* recibe variables $fecha NUMERICA O CARACTER    */
/* retorna ejemplo VIE 16/MAR/2007                */
/**************************************************/
function fecha_larga($fecha)
{
$tfecha="";
if ($fecha=="" or substr($fecha,0,2)=="00")
{return($tfecha);}
if (gettype($fecha)=="string")//convertir fecha caracter a numerica
{
	$nwf=$fecha;
	if (substr($fecha,6,4) > 1900) // convierte formato DIA-MES-A�O a A�O-MES-DIA
	{$nwf=substr($fecha,6,4)."-".substr($fecha,3,2)."-".substr($fecha,0,2);}
	$fecha=strtotime($nwf);
}
$nano=date("Y",$fecha);
$nmes=date("m",$fecha);
$ndia=date("d",$fecha);
$num=date("w",$fecha); 
if ($nano <= 0)
{return($tfecha);}
$tdia=nombre_dia($num);
$tdia=strtoupper($tdia);
$tmes=nombre_mes($nmes);
$tmes=strtoupper($tmes);
$tfecha=substr($tdia,0,3) . " " . $ndia .  "/" . substr($tmes,0,3) . "/" . $nano;
return($tfecha);
}
/**************************************************/
/* fecha con nombre largo del DIA Y  corto MES    */
/* recibe variables $fecha NUMERICA O CARACTER    */
/* retorna ejemplo V 16/MAR/2007                */
/**************************************************/
function fecha_dia($fecha)
{
$tfecha="";
if ($fecha=="" or substr($fecha,0,2)=="00")
{return($tfecha);}
if (gettype($fecha)=="string")//convertir fecha caracter a numerica
{
	$nwf=$fecha;
	if (substr($fecha,6,4) > 1900) // convierte formato DIA-MES-A�O a A�O-MES-DIA
	{$nwf=substr($fecha,6,4)."-".substr($fecha,3,2)."-".substr($fecha,0,2);}
	$fecha=strtotime($nwf);
}
$nano=date("Y",$fecha);
$nmes=date("m",$fecha);
$ndia=date("d",$fecha);
$num=date("w",$fecha); 
$tdia=nombre_dia($num);
$tmes=nombre_mes($nmes);
$tmes=strtoupper($tmes);
$tfecha=substr($tdia,0,1) . " " . $ndia .  "/" . $nmes . "/" . $nano;
return($tfecha);
}
/**************************************************/
/* fecha con nombre largo del DIA Y  corto MES    */
/* recibe variables $fecha NUMERICA O CARACTER    */
/* retorna ejemplo VIERNES 16/MAR/2007            */
/**************************************************/
function fecha_completa($fecha)
{
$tfecha="";
if ($fecha=="" or substr($fecha,0,2)=="00")
{return($tfecha);}
if (gettype($fecha)=="string")//convertir fecha caracter a numerica
{
	$nwf=$fecha;
	if (substr($fecha,6,4) > 1900) // convierte formato DIA-MES-A�O a A�O-MES-DIA
	{$nwf=substr($fecha,6,4)."-".substr($fecha,3,2)."-".substr($fecha,0,2);}
	$fecha=strtotime($nwf);
}
$nano=date("Y",$fecha);
$nmes=date("m",$fecha);
$ndia=date("d",$fecha);
$num=date("w",$fecha); 
$tdia=nombre_dia($num);
$tdia=strtoupper($tdia);
$tmes=nombre_mes($nmes);
$tmes=strtoupper($tmes);
$tfecha=$tdia . " " . $ndia .  "/" . substr($tmes,0,3) . "/" . $nano;
return($tfecha);
}
/**************************************************/
/* Convierte fecha con nombre corto del mes       */
/* recibe variables fecha caracter o numerica     */
/* retorna ejemplo 16/MAR/2007                    */
/**************************************************/
function fecha_texto($fecha)
{
$tfecha="";
if ($fecha=="" or substr($fecha,0,2)=="00")
{return($tfecha);}
if (is_numeric($fecha)=="TRUE")//convertir fecha numerica a caracter d-m-Y
{$fecha=date('d-m-Y',$fecha);}
if (substr($fecha,2,1) == "-") // convertir fecha en formato DIA-MES-A�O a formato A�O-MES-DIA
{$fecha=substr($fecha,6,4)."-".substr($fecha,3,2)."-".substr($fecha,0,2);}
$mes=substr($fecha,5,2);
$tmes=nombre_mes($mes);
$tmes=substr(strtoupper($tmes),0,3);
$tfecha=substr($fecha,8,2). "/" . substr($tmes,0,3)  . "/" .  substr($fecha,0,4);
return($tfecha);
}
/**************************************************/
/* fecha con nombre largo del DIA Y  corto MES    */
/* recibe variables $fecha NUMERICA O CARACTER    */
/* retorna ejemplo Viernes 16 de marzo de 2007    */
/**************************************************/
function fecha_carta($fecha)
{
$tfecha="";
if ($fecha=="" or substr($fecha,0,2)=="00")
{return($tfecha);}
if (gettype($fecha)=="string")//convertir fecha caracter a numerica
{
	$nwf=$fecha;
	if (substr($fecha,6,4) > 1900) // convierte formato DIA-MES-A�O a A�O-MES-DIA
	{$nwf=substr($fecha,6,4)."-".substr($fecha,3,2)."-".substr($fecha,0,2);}
	$fecha=strtotime($nwf);
}
$nano=date("Y",$fecha);
$nmes=date("m",$fecha);
$ndia=date("d",$fecha);
$num=date("w",$fecha); 
$tdia=nombre_dia($num);
$tmes=nombre_mes($nmes);
$tfecha=$tdia . " " . $ndia .  " de " . $tmes . " de " . $nano;
return($tfecha);
}
/****************************************************/
/* Calcula edad de una persona                      */
/* recibe variables fecha caracter nacimiento Y-M-D */
/* retorna numero de a�os de la edad                */
/****************************************************/
function calculo_edad($fecha)
{
$edad=0;
if ($fecha!="" and substr($fecha,0,2)!="00")
{
	$hoy=date('Y-m-d');
	list ($hoyano, $hoymes, $hoydia) = explode ('-', $hoy);
	list ($ano, $mes, $dia) = explode ('-', $fecha);
	$edad=intval($hoyano - $ano);
	if ($mes > $hoymes or ($mes == $hoymes and $dia > $hoydia))
	{$edad=$edad-1;}
}
return($edad);
}

function semanas_academicas($ano)
{
$semanas = 0;
$sql="SELECT fchinicio, fchfinal FROM periodos WHERE ano = '$ano' and pertipo='P' order by notanum";
$datperi=viewsql($sql);
foreach($datperi as $regperi)
{
	$f1 = $regperi->fchinicio;
	$f2 = $regperi->fchfinal;
	$s1 = mktime(0,0,0,intval(substr($f1,5,2)) ,intval(substr($f1,8,2)), intval(substr($f1,0,4)));
	$s2 = mktime(0,0,0,intval(substr($f2,5,2)) ,intval(substr($f2,8,2)), intval(substr($f2,0,4)));
	$dif = $s2-$s1;
	$semanas = $semanas + $dif/(7 * 24 * 60 * 60);
}
$semanas = round($semanas,0);
return($semanas);
}

/**************************************************/
/* Busca color del documento en tabla listas      */
/* recibe variables documento y estado            */
/* retorna el color del estado ejemplo #E50000    */
/**************************************************/
function estado_color($doctip,$docest)
{
	$rcolor="#000000";
	$sqlcol="select liscol from listas where listip='$doctip' and lisval='$docest' limit 1";
	$datcol=viewsql($sqlcol);
	foreach($datcol as $regcol)
	{$rcolor=$regcol->liscol;}
	return($rcolor);
}

?>