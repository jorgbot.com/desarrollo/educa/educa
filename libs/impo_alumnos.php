<?php
//IMPORTAR ALUMNOS DESDE ARCHIVO TXT - DELIMITADO POR TABULACIONES
//SE ABRE LA TABLA FOXBASE CON EXCEL Y SE GRABA COMO TXT DELIMITADA
//TABLAS ALUMNOS, MATRICULAS, FAMILIARES, CURSOS, Y2012CURSOS
//OJO, DESOCUPAR TODAS MENOS CURSOS

//inicializar registros de alumnos y matriculas
$impano = "2012";
$iniciar = 1;
include("database/database.php");
connect();
if ($iniciar == 1)
{
	for($i=1; $i <= 2226 ; $i++)
	{
		$sql= "INSERT INTO alumnos (apellidos) VALUES ('')";
		execsql($sql);
		$sql= "INSERT INTO matriculas (folio) VALUES ('')";
		execsql($sql);
	}
}
//desocupar tabla temporal
$archivo = "C:/xampp/htdocs/democol/temporales/matri".$impano.".txt";
$file = "importar";
execsql("delete from ".$file);
//cargar datos tabla temporal	
$sqlc="LOAD DATA INFILE '$archivo' INTO TABLE $file FIELDS OPTIONALLY ENCLOSED BY '\"' LINES TERMINATED BY '\r\n' IGNORE 1 LINES";
execsql($sqlc);
$fecha_hora=date('Y-m-d') . " ". date('H:i:s'); //Fecha del sistema caracter
$sql="select * from $file";
$data=viewsql($sql);
foreach($data as $registros)
{
	$id_alumno = intval(trim($registros->campo1));
	$id_matricula = intval(trim($registros->campo1));
	$ano = intval(trim($registros->campo2))+2000;
	$mes = intval(trim($registros->campo3));
	$dia = intval(trim($registros->campo4));
	if ($mes < 10)
		$mes = "0".$mes;
	if ($dia < 10)
		$dia = "0".$dia;
	$fecha = $ano."-".$mes."-".$dia;
	$apellidos = trim($registros->campo5)." ".trim($registros->campo6);	
	$apellidos = str_replace('?','�',$apellidos);
	$nombres = trim($registros->campo7);	
	$nombres = str_replace('?','�',$nombres);
	$docu = intval(trim($registros->campo8));
	$docutip = "";
	if ($docu == 1)
		$docutip = "TI";
	if ($docu == 2)
		$docutip = "CC";
	if ($docu == 3)
		$docutip = "NUIP";
	if ($docu == 4)
		$docutip = "RC";
	$docunum = trim($registros->campo9);
	$docuexp = trim($registros->campo10);
	$docudep = trim($registros->campo11);
	$docunum = str_replace('.','',$docunum);
	$docunum = str_replace(',','',$docunum);
	$docunum = str_replace('-','',$docunum);
	$genero = intval(trim($registros->campo12));
	if ($genero == 1)
		$genero = "F";
	else
		$genero = "M";
	$lugnace = trim($registros->campo13);	
	$depnace = trim($registros->campo14);	
	$ano = intval(trim($registros->campo15));
	$mes = intval(trim($registros->campo16));
	$dia = intval(trim($registros->campo17));
	$fchnace ="0000-00-00";
	if ($mes > 0 and $dia > 0)
	{
		if ($ano <= 40)
			$ano = $ano + 2000;
		else
			$ano = $ano + 1900;
		if ($mes < 10)
			$mes = "0".$mes;
		if ($dia < 10)
			$dia = "0".$dia;
		$fchnace = $ano."-".$mes."-".$dia;
	}
	$grado = intval(trim($registros->campo19));
	if ($grado == 12) //prejardin
		$grado = "-2";
	if ($grado == 13) //jardin
		$grado = "-1";
	if ($grado == 14) //transicion
		$grado = "0";
	$procegrado = trim($registros->campo20);
	if ($procegrado == 12)
		$procegrado = "-2";
	if ($procegrado == 13)
		$procegrado = "-1";
	if ($grado == 14)
		$procegrado = "0";
	$proceinsti = trim($registros->campo21);
	$tbeca = trim($registros->campo43);
	$beca = 0;
	if ($tbeca == 1)
		$beca = 50;
	if ($tbeca == 2)
		$beca = 75;
	if ($tbeca == 3)
		$beca = 100;
	$curso = trim($registros->campo56);
	$ano = intval(trim($registros->campo72));
	$mes = intval(trim($registros->campo71));
	$dia = intval(trim($registros->campo70));
	$fchretiro ="0000-00-00";
	if ($mes > 0 and $dia > 0)
	{
		if ($ano <= 40)
			$ano = $ano + 2000;
		else
			$ano = $ano + 1900;
		if ($mes < 10)
			$mes = "0".$mes;
		if ($dia < 10)
			$dia = "0".$dia;
		$fchretiro = $ano."-".$mes."-".$dia;
	}
	$estado = "ACTIVO";
	$estadomat = "CERRADA";
	$activo = intval(trim($registros->campo55));
	if ($activo == 1)
		$estadomat = "PENDIENTE";
	if ($activo == 2)
	{
		$estado = "RETIRADO";
		$estadomat = "CANCELADA";
	}
	if ($fchretiro!="0000-00-00")
		$estado = "RETIRADO";
	$vive = intval(trim($registros->campo74));
	$id_saludenti = intval(trim($registros->campo82));
	if ($id_saludenti > 0)
		$id_saludenti = 1;
	//actualizar alumno
	$sql="UPDATE alumnos SET apellidos='$apellidos', nombres='$nombres', docutip='$docutip', docunum='$docunum', docuexp='$docuexp', docudep='$docudep', ".
	"genero='$genero', estado='$estado', tipoedu='F', fchnace='$fchnace', lugnace='$lugnace',depnace='$depnace', ".
	"procegrado='$procegrado', proceinsti='$proceinsti', id_matricula='$id_alumno', id_saludenti='$id_saludenti', fchretiro='$fchretiro' ".
	"WHERE id_alumno = '$id_alumno' limit 1";
	execsql($sql);
	//actualizar matriula
	$sql="UPDATE matriculas SET id_alumno='$id_alumno', ano = '$impano', grado='$grado', curso='$curso', fecha='$fecha', estado='$estadomat', beca='$beca' ".
	"WHERE id_matricula = '$id_alumno' limit 1";
	execsql($sql);
	//datos del padre
	$padre = trim($registros->campo22);	
	$padre = str_replace('?','�',$padre);
	$pos=strpos($padre," ");
	$nom1 = trim(substr($padre,0,$pos));
	$nom2 = trim(substr($padre,$pos+1));
	$pos=strpos($nom2," ");
	$ape1 = trim(substr($nom2,$pos+1));
	$nom2 = trim(substr($nom2,0,$pos));
	$pos=strpos($ape1," ");
	$ape2 = "";
	if ($pos > 0)
	{
		$ape2 = trim(substr($ape1,$pos+1));
		$ape1 = trim(substr($ape1,0,$pos));
	}
	$nombres = $nom1." ".$nom2;
	$apellidos = $ape1." ".$ape2;
	$direccion = trim($registros->campo24);
	$telefono = trim($registros->campo26)." ".trim($registros->campo29);
	$telefofi = trim($registros->campo28);
	$celular = trim($registros->campo30);
	$profesion = trim($registros->campo40);
	$docutip = "CC";
	$pdocunum = trim($registros->campo75);
	$pdocunum = str_replace('.','',$pdocunum);
	$pdocunum = str_replace(',','',$pdocunum);
	$docuexp = trim($registros->campo78);
	$estrato = intval(trim($registros->campo81));
	if ($padre != "")
	{
		$sql= "INSERT INTO familiares (id_alumno, tipo, apellidos, nombres, docunum, docutip, docuexp, direccion, telefono, telefofi, celular, profesion, estrato) ".
		"VALUES ('$id_alumno', 'PADRE','$apellidos', '$nombres', '$pdocunum', '$docutip', '$docuexp', '$direccion', '$telefono', '$telefofi', '$celular', '$profesion', '$estrato')";
		execsql($sql);
	}
	//datos de la madre
	$madre = trim($registros->campo23);	
	$madre = str_replace('?','�',$madre);
	$pos=strpos($madre," ");
	$nom1 = trim(substr($madre,0,$pos));
	$nom2 = trim(substr($madre,$pos+1));
	$pos=strpos($nom2," ");
	$ape1 = trim(substr($nom2,$pos+1));
	$nom2 = trim(substr($nom2,0,$pos));
	$pos=strpos($ape1," ");
	$ape2 = "";
	if ($pos > 0)
	{
		$ape2 = trim(substr($ape1,$pos+1));
		$ape1 = trim(substr($ape1,0,$pos));
	}
	$nombres = $nom1." ".$nom2;
	$apellidos = $ape1." ".$ape2;
	$direccion = trim($registros->campo25);
	$telefono = trim($registros->campo27)." ".trim($registros->campo32);
	$telefofi = trim($registros->campo31);
	$celular = trim($registros->campo33);
	$profesion = trim($registros->campo41);
	$docutip = "CC";
	$mdocunum = trim($registros->campo76);
	$mdocunum = str_replace('.','',$mdocunum);
	$mdocunum = str_replace(',','',$mdocunum);
	$docuexp = trim($registros->campo79);
	$estrato = intval(trim($registros->campo81));
	if ($madre != "")
	{
		$sql= "INSERT INTO familiares (id_alumno, tipo, apellidos, nombres, docunum, docutip, docuexp, direccion, telefono, telefofi, celular, profesion, estrato) ".
		"VALUES ('$id_alumno', 'MADRE','$apellidos', '$nombres', '$mdocunum', '$docutip', '$docuexp', '$direccion', '$telefono', '$telefofi', '$celular', '$profesion', '$estrato')";
		execsql($sql);
	}
	//datos del acudiente
	$acudiente = trim($registros->campo34);	
	$acudiente = str_replace('?','�',$acudiente);
	$pos=strpos($acudiente," ");
	$nom1 = trim(substr($acudiente,0,$pos));
	$nom2 = trim(substr($acudiente,$pos+1));
	$pos=strpos($nom2," ");
	$ape1 = trim(substr($nom2,$pos+1));
	$nom2 = trim(substr($nom2,0,$pos));
	$pos=strpos($ape1," ");
	$ape2 = "";
	if ($pos > 0)
	{
		$ape2 = trim(substr($ape1,$pos+1));
		$ape1 = trim(substr($ape1,0,$pos));
	}
	$nombres = $nom1." ".$nom2;
	$apellidos = $ape1." ".$ape2;
	$adocunum = trim($registros->campo77);
	$adocunum = str_replace('.','',$adocunum);
	$adocunum = str_replace(',','',$adocunum);
	$resp = "";
	if ($adocunum == $pdocunum or $acudiente == $padre)
		$resp = "PADRE";
	if ($adocunum == $mdocunum or $acudiente == $madre)
		$resp = "MADRE";
	if ($resp == "")
	{
		$direccion = trim($registros->campo35);
		$telefono = trim($registros->campo36)." ".trim($registros->campo38);
		$telefofi = trim($registros->campo37);
		$celular = trim($registros->campo39);
		$profesion = trim($registros->campo42);
		$docutip = "CC";
		$docuexp = trim($registros->campo80);
		$estrato = intval(trim($registros->campo81));
		if ($nombres != "")
		{
			$sql= "INSERT INTO familiares (id_alumno, tipo, apellidos, nombres, docunum, docutip, docuexp, direccion, telefono, telefofi, celular, profesion, estrato, resppagos, respmatri, respnotas) ".
			"VALUES ('$id_alumno', 'ACUDIENTE','$apellidos', '$nombres', '$adocunum', '$docutip', '$docuexp', '$direccion', '$telefono', '$telefofi', '$celular', '$profesion', '$estrato', '1', '1', '1')";
			execsql($sql);
		}
	}
	else
	{
		//asigna responsable
		$sql="UPDATE familiares SET resppagos = '1', respmatri = '1', respnotas = '1' ".
		"WHERE id_alumno = '$id_alumno' and tipo = '$resp' limit 1";
		execsql($sql);
	}
	$id_familiar = 0;
	$tipo = "MADRE";
	if ($vive == 1 and $padre!="")
		$tipo = "PADRE";
	if ($vive == 2 and $acudiente!="")
		$tipo = "ACUDIENTE";
	$sql = "select id_familiar from familiares where id_alumno='$id_alumno' and tipo = '$tipo' limit 1";
	$datfam = viewsql($sql);
	foreach($datfam as $regfam)
	{$id_familiar = $regfam->id_familiar;}
	if ($id_familiar == 0)
	{
		$sql = "select id_familiar from familiares where id_alumno='$id_alumno' limit 1";
		$datfam = viewsql($sql);
		foreach($datfam as $regfam)
		{$id_familiar = $regfam->id_familiar;}
	}
	//actualiza con quien vive
	$sql="UPDATE alumnos SET id_familiar = '$id_familiar' WHERE id_alumno = '$id_alumno' limit 1";
	execsql($sql);
} //fin registros
echo "ALUMNOS OK...<BR>";
//crear cursos
$ycursos = "y".$impano."cursos";	
$sql = "select id_alumno, grado, curso from matriculas where ano='$impano' and id_alumno > '0' and estado='CERRADA' and curso !='' order by grado, curso, id_alumno";
$datmatr = viewsql($sql);
foreach($datmatr as $regmatr)
{
	$id_alumno = $regmatr->id_alumno;
	$grado = $regmatr->grado;
	$curso = $regmatr->curso;
	if ($curso > 0)
	{
		$sql= "INSERT INTO $ycursos (grado, curso, id_alumno, tipoedu) VALUES ('$grado','$curso','$id_alumno','F')";
		execsql($sql);
		$id_curso = 0;
		$sql = "select id_curso from cursos where ano='$impano' and grado='$grado' and curso='$curso' limit 1";
		$datcurs = viewsql($sql);
		if (count($datcurs) <= 0)
		{
			$sql= "INSERT INTO cursos (grado, curso, ano) VALUES ('$grado','$curso','$impano')";
			execsql($sql);
		}
	}
}
echo "CURSOS OK...<BR>";

//totalizar cursos
$sql="SELECT * FROM cursos order by grado, curso";
$datcurs=viewsql($sql);
foreach($datcurs as $regcurs)
{
	$id_curso = $regcurs->id_curso;
	$grado = $regcurs->grado;
	$curso = $regcurs->curso;
	$alumtot = 0;
	$sql = "select count(*) as numreg from $ycursos where grado='$grado' and curso='$curso'";
	$datnume = viewsql($sql);
	foreach($datnume as $regnume)
	{$alumtot = $regnume->numreg;}
	$sql="UPDATE cursos SET alumtot = '$alumtot' WHERE id_curso = '$id_curso' limit 1";
	execsql($sql);
}
disconnect();
echo "TOTALIZAR OK...<BR>";
/*
echo"<script> alert('Alumnos Grabados.'); window.location.href='alumnos.php';</script>";
*/
?>
