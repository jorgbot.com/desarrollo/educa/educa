<?php
/***********************************************************************************************
* @file        : setup_backup.php                                                              *
* @brief       : Ejecutar backup de base de datos, tablas maestras o movimiento                *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 15-May-2012                                                    *
* @Modificado  : Nora Rodriguez 15-May-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivpar"] < 4)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</script>";}
include("database/database.php");
include("libs/fechas.php");
$op = isset($_GET['op']) ? $_GET['op'] : null ;
$ano = $_SESSION["anoaca"];
connect();
?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>IDIC</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="estilos.css">
<link href="estilosfot.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript">
function backup()
{
	var tipo = '';
	if (document.form1.tipo[1].checked==true)
		tipo = 'NOTAS';
	var ano = document.form1.ano.value;
	var theURL = 'backup/backup.php?tipo='+tipo+'&ano='+ano;
	newWindow(theURL, '', 600, 400, 1, 1, 0, 0, 0, 1, 0);
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#FFFF66">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="150" bgcolor="#006600">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr height="40" bgcolor="#006600">
		<td background="fondos/top_verde.jpg"></td>
		<td background="fondos/top_verde.jpg" class="titulo_color">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_top.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/top_verde.jpg"></td>
	</tr>
	<tr height="3" bgcolor="#FFFF66">
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_top.jpg"></td></tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="200">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10"></td>
				<td>
					<?php include("includes/menu_setup.php");?>
					<br/>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr height="32">
							<td width="6" background="images/backsilver401.gif"></td>
							<td align="left" background="images/backsilver402.gif" class="titulo">
							<img src="images/backup.png" width="28" height="28" border="0" align="absmiddle"> 
							Backup
							</td>
							<td width="6" background="images/backsilver403.gif"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#C0C0C0" class="tabla_marco">
					<form action="javascript:backup();" method="post" name="form1">
					<tr height="30"><td width="40"></td><td></td><td width="40"></td></tr>
					<tr>
						<td></td>
						<td>
							<fieldset><legend class="titulo_campos">Exportar en formato SQL comprimido la Base de Datos</legend>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr height="40">
									<td>
									<input type='radio' name='tipo' value='maestras' checked onKeyDown="if(event.keyCode==13) event.keyCode=9;"> Tablas Maestras (Grados, Areas, Alumnos, Docentes...)
									</td>
								</tr>
								<tr height="40">
									<td>
									<input type='radio' name='tipo' value='notas' onKeyDown="if(event.keyCode==13) event.keyCode=9;"> Tablas de Notas A�o 
									<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									$sql="SELECT ano FROM anoslect ORDER BY ano desc";
									$datanol=viewsql($sql);
									foreach($datanol as $reganol)
									{
										$selano = "";
										if ($reganol->ano==$ano)
											$selano = "selected";
										echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
									}
									?>
									</select>
									</td>
								</tr>
							</table>
							</fieldset>
						</td>
						<td></td>
					</tr>
					<tr><td colspan="3" height="30"></td></tr>
					<tr height="36">
						<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
							<?php if ($_SESSION["nivpar"] >= 4) { ?>
							<input type="image" class="botones_img" src="images/btn_procesar.png" border="0" name="procesar"  width="70" height="24" hspace="10" alt="Grabar">
							<?php } ?>
						</td>
					</tr>
					</form>					
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_bot.jpg"></td></tr>
	<tr height="3" bgcolor="#FFFF66">
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
	</tr>
	<tr height="100" bgcolor="#FFFF66">
		<td background="fondos/pie_amarillo.jpg"></td>
		<td background="fondos/pie_amarillo.jpg" valign="top">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/pie_amarillo.jpg"></td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>