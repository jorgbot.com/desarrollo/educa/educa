<?php
/***********************************************************************************************
* @file        : notas_planilla.php                                                            *
* @brief       : planilla para captura de notas general por periodo o examen                   *
* @version     : 2.0                                                                           *
* @author      : Nora Rodriguez 10-mar-2012                                                    *
* @Modificado  : Nora Rodriguez 24-jul-2019  
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("database/database.php");
include("database/datos_notas.php");
include("libs/fechas.php");
connect();
$id_asignatura = $_GET["id_asignatura"];
$id_profasig = $_GET["id_profasig"];
$pertipo = $_GET["pertipo"];
$periodo = $_GET["periodo"];
$tipoedu = isset($_GET['tipoedu']) ? $_GET['tipoedu'] : "F" ;
$general = ""; //lista todos los alumnos
$seccion = "seccion".$id_profasig;
include("database/datos_planilla.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8" />
<meta name="robots" content="noindex" />
<meta name="googlebot" content="noindex" />
<title>Educa</title>
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" href="estilos.css" />
<link rel="stylesheet" href="estilosmenu.css" />
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript'>
function checkNota(obj,numalum)
{
	var valobj = obj.value;
	var total = 0;
	var totalQ = '';
	var notamin = '<?php echo $notamin;?>';
	var notamax = '<?php echo $notamax;?>';
	var numpunt = '<?php echo $numpunt;?>';
	var numlogro = '<?php echo $numlogr;?>';
	numpunt = parseInt(numpunt,10);
	numlogro = parseInt(numlogro,10);
	if (valobj=='' || valobj=='NaN')
		var intNota = 0;
	else
		var intNota = parseInt(valobj,10);
	if (intNota > 0)
	{
		//validar minimo y maximo de la nota
		if (intNota < notamin)
		{
			obj.value = '';
			obj.focus();
			alert('Nota inv�lida, m�nima '+notamin);
			return false;
		}
		if (intNota > notamax)
		{
			obj.value = '';
			obj.focus();
			alert('Nota inv�lida, m�xima '+notamax);
			return false;
		}
		//calcular promedio nota de logros
		total = intNota;
		if (numlogro > 0)
		{
			var notalogro = 0;
			var totlogros = 0;
			for(var i=1;i<=numlogro;i++){
				notalogro = eval ('document.form1.alu'+numalum+'log'+i+'.value');
				if (notalogro=='' || notalogro=='NaN')
					notalogro = 0;
				else
					notalogro = parseInt(notalogro,10);
				totlogros = totlogros + notalogro;
			}
			total = parseInt((totlogros/numlogro),10);
		}
		//ajustar topes
		if (total < notamin && total > 0)
			total = notamin; 
		if (total > notamax)
			total = notamax;
		//retornar valor numerico de la nota
		eval ("document.form1.nota"+numalum+".value='"+total+"'");
		//nota cualitativa
		var puntaje = '';
		var max1 = 0;
		var max2 = 0;
		var cali = '';
		for(var i=1;i<=numpunt;i++){
			puntaje = eval ('document.form1.puntaje'+i+'.value');
			max2 = parseInt(puntaje.substr(0,3),10);
			cali = puntaje.substr(3);
			if (i==1)
				max1 = -1;
			else
			{
				max1 = i - 1;
				puntaje = eval ('document.form1.puntaje'+max1+'.value');
				max1 = parseInt(puntaje.substr(0,3),10);
			}
			if (total > max1 && total <= max2)
				totalQ = cali;
		}
		//retornar valor cualitativo de la nota
		eval ("document.form1.cali"+numalum+".value='"+totalQ+"'");
	}
	else
	{
		obj.value = '';
	}
}
function seleccionar()
{
	var id_asignatura = document.form0.id_asignatura.value;
	var id_profasig = document.form0.id_profasig.value;
	var pertipo = document.form0.pertipo.value;
	var periodo = document.form0.periodo.value;
	var tipoedu = document.form0.tipoedu.value;
	var theURL = 'notas_planilla.php?id_asignatura='+id_asignatura+'&id_profasig='+id_profasig+'&pertipo='+pertipo+'&periodo='+periodo+'&tipoedu='+tipoedu;
	window.location.href = theURL;
}
</script>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="1">
        <td></td>
        <td width="1000"></td>
        <td></td>
    </tr>
    <tr height="120">
        <td background="fondos/fondolat1.jpg">&nbsp;</td>
	<td width="1000" align="center"><?php include("includes/encabezado.php");?></td>
	<td background="fondos/fondolat2.jpg">&nbsp;</td>
    </tr>
    <tr height="12">
        <td background="fondos/marco_top.gif"></td>
        <td width="1000" background="fondos/marco_top.gif"></td>
        <td  background="fondos/marco_top.gif"></td>
    </tr>
    <tr>
        <td></td>
	<td align = "center" bgcolor="#FFFFFF"><?php include("includes/menu_principal.php");?></td>
	<td></td>
    </tr>
	<tr height="535">
		<td></td>
		<td valign="top">
                <br>
		<table border="0" cellpadding="0" cellspacing="0" width="1000" class="tabla_color">
			<tr height="40">
				<td width="50" align="center"><img src='images/notas.gif' width="40" height="40" border="0" align="absmiddle"></td>
				<td class="titulo">PLANILLA CURSO <?php echo $curso;?> - <?php echo $asignatura;?></td>
				<td align="right">
					<table border="0" cellpadding="1" cellspacing="1">
						<tr><td align="center" class="titulo_campos"><font color="#000000"><?php echo $pernomb;?></font></td></tr>
						<tr><td class="titulo_campos"><?php echo fecha_texto($fchinicio);?> A <?php echo fecha_texto($fchfinal);?> (PLAZO <?php echo fecha_texto($fchplazo);?>)&nbsp;</td></tr>
					</table>
				</td>
				<td width="75" class="titulo" bgcolor="<?php echo $gradocolor;?>" align="right"><font color="<?php echo $gradofont;?>">GRADO <?php echo $grado;?>&deg;</font>&nbsp;</td>
			</tr>
		</table>
		<table border="0" cellpadding="0" cellspacing="0" width="1000" class="tabla_dotted">
		<form action="" method="post" name="form0">
		<input type="hidden" name="id_asignatura" value="<?php echo $id_asignatura;?>">
		<input type="hidden" name="id_profasig" value="<?php echo $id_profasig;?>">
		<input type="hidden" name="pertipo" value="<?php echo $pertipo;?>">
		<input type="hidden" name="periodo" value="<?php echo $periodo;?>">
		<tr valign="top">
		<td>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr height="5"><td colspan="3"></td></tr>
				<tr height="25">
					<td width="15"></td>
					<td>
						<table border="0" width="100%">
							<tr class="titulo_campos">
								<td>DOCENTE: <?php echo $profesor;?></td>
							</tr>
							<?php if ($pertipo == "P") { ?>
							<tr>
								<td>
								<select name="tipoedu" onChange="javascript:seleccionar();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php
								$sql="SELECT * FROM listas WHERE listip = 'EDUCACION' order by id";
								$lista=viewsql($sql);
								foreach($lista as $reglis)
								{
									$mostrar = 1;
									if ($alumesp <=0 and $reglis->lisval == "E")
										$mostrar = 0;
									if ($mostrar == 1)
									{
										if ($reglis->lisval == $tipoedu)
											echo "<option value='$reglis->lisval' selected>LOGROS EDUCACION $reglis->lisnom</option>";
										else
											echo "<option value='$reglis->lisval'>LOGROS EDUCACION $reglis->lisnom</option>";
									}
								}
								?>																
								</select>
								</td>
							</tr>
							<tr>
								<td>
									<select name="id_logro" style="width: 750px;" size="<?php echo $numlogr;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									foreach($datlogr as $reglogr)
									{echo "<option value='$reglogr->id_logro'>$reglogr->logrocod - $reglogr->logro</option>";}
									?>																
									</select>
								</td>
							</tr>
							<?php } ?>
						</table>
					</td>
					<td width="15"></td>
				</tr>
			</table>
		</td>
		<td width="200" align="right">
			<table border="0" cellpadding="0" cellspacing="1" bgcolor="#006600">
			<tr><td colspan="4" class="titulo_campos" align="center"><font color="#FFFFFF">Evaluaci�n</font></td></tr>
			<?php
			foreach($datpunt as $regpunt)
			{echo "<tr height='20' bgcolor='#FFFFFF'><td width='25'>&nbsp;$regpunt->notaQ</td><td width='110'>&nbsp;$regpunt->notadet</td><td align='right'>&nbsp;$regpunt->notamin - $regpunt->notamax&nbsp;</td></tr>";}
			?>
			</table>
		</td>
		</tr>
		</form>
		</table>
		<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="1000" align="center">
		<form action="database/grabar_planillas.php" method="post" name="form1">
		<input type="hidden" name="op" value="planilla_periodo">
		<input type="hidden" name="id_asignatura" value="<?php echo $id_asignatura;?>">
		<input type="hidden" name="id_profasig" value="<?php echo $id_profasig;?>">
		<input type="hidden" name="pertipo" value="<?php echo $pertipo;?>">
		<input type="hidden" name="periodo" value="<?php echo $periodo;?>">
		<input type="hidden" name="tipoedu" value="<?php echo $tipoedu;?>">
			<?php
			//cargar hidden puntajes de evaluacion
			$numpunt = 0;
			foreach($datpunt as $regpunt)
			{
				$numpunt = $numpunt + 1;
				$punt = trim($regpunt->notamax);
				if (strlen($punt)==1)
					$punt = "00".$punt;
				if (strlen($punt)==2)
					$punt = "0".$punt;
				$name = "puntaje".$numpunt;
				$puntaje = $punt.$regpunt->notaQ;
				?>
				<input type="hidden" name="<?php echo $name;?>" id="<?php echo $name;?>" value="<?php echo $puntaje;?>">
				<?php
			}
			?>
			<tr bgcolor="#FFFFFF">
			<td colspan="9">
				<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<tr height="32">
						<td background="images/backsilver.gif" align="left" class="titulo">
							&nbsp;
							<?php echo $pernomb;?> - <?php echo $estado;?>
							<?php echo $fchentrega." ".$fchcierre;?>
						</td>
						<td background="images/backsilver.gif">
						</td>
						<td align="right" background="images/backsilver.gif">
							&nbsp;
							<?php if ($_SESSION["usutip"]=="DOCENTE") { ?>
							<a href="notas.php#<?php echo $seccion;?>"><img src="images/btn_regresar.png" border="0" width="70" height="24" alt="Regresar" hspace="0" align="absmiddle"></a>
							<?php } else { ?>
							<a href="notas.php?grado=<?php echo $grado;?>&curso=<?php echo $curso;?>#<?php echo $seccion;?>"><img src="images/btn_regresar.png" border="0" width="70" height="24" alt="Regresar" hspace="0" align="absmiddle"></a>
							<?php } ?>
							&nbsp;
						</td>
					</tr>
				</table>
			</td>
			</tr>
			<tr height="20">
				<th width="280">Apellidos y Nombres</th>
				<?php
				foreach($datperi as $regperi)
				{
					if ($regperi->pertipo==$pertipo and $regperi->periodo==$periodo)
					{
						$colspan = ($numlogr*2) + 4 +3;
						if ($numlogr == 0)
							$colspan = $colspan + 1;
						?>
						<th align="left">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr><th colspan="<?php echo $colspan;?>" title="<?php echo $regperi->pernomb;?>" style="background-color: #FFFFDF;"><font face="Times New Roman, Times, serif" size="1"><?php echo $regperi->perplan;?></font></th></tr>
								<tr height="1"><td colspan="<?php echo $colspan;?>" bgcolor="#C0C0C0"></td></tr>
								<tr height="15">
									<?php if ($numlogr > 0) { ?>
									<th width="20" style="background-color: #FFFFDF;"></th>
									<td width="1" bgcolor="#CCCCCC"></td>
									<?php
									}
									if ($numlogr > 0)
									{
										foreach($datlogr as $reglogr)
										{ ?>
										<th width="40" align="center" style="background-color: #FFFFDF;"><?php echo "L".$reglogr->logrocod;?></th>
										<td width="1" bgcolor="#CCCCCC"></td>
										<?php
										}
									}
									else
									{
										echo "<th width='40' style='background-color: #FFFFDF;'></th>";
									}
									?>
									<th width="40" align="center" title="Nota" style="background-color: #FFFFDF;">Nota</th>
									<td width="1" bgcolor="#CCCCCC"></td>
									<th width="40" align="center" title="Cualitativa" style="background-color: #FFFFDF;">NotaQ</th>
									<td width="1" bgcolor="#CCCCCC"></td>
									<th width="25" align="center" title="Observaci�n" style="background-color: #FFFFDF;"></th>
								</tr>
							</table>
						</th>
						<?php
					}
					else
					{ ?>
					<th width="30" title="<?php echo $regperi->pernomb;?>"><font face="Times New Roman, Times, serif" size="1"><?php echo $regperi->perplan;?></font></th>
					<?php
					}
				} ?> 
				<th width="62" title="Nota Final"><font face="Times New Roman, Times, serif" size="1">Final</font></th>
			</tr>
			<?php
			$numalum = 0;
			//RECORRER LISTA DE ALUMNOS PLANILLA
			foreach($datalum as $regalum)
			{
			$id_alumno = $regalum->id_alumno;
			$apellidos = $regalum->apellidos;
			$retiro = $regalum->fchretiro;
			$nombres = $regalum->nombres;
			$nombretiro = $regalum->nombres;
			$numalum = $numalum + 1;
				if ($retiro != '0000-00-00'){
					$nombretiro = $nombretiro. " (RETIRADO EL  ".$retiro. " )";
					}
			$actualizar = 0;
			if (trim($apellidos)=="" or trim($nombres)=="")
			{
				$sql = "SELECT apellidos, nombres FROM alumnos WHERE id_alumno = '$id_alumno' LIMIT 1";
				$datreg = viewsql($sql);
				foreach($datreg as $reg)
				{
					$apellidos = $reg->apellidos;
					$nombres = $reg->nombres;
					$actualizar = 1;
				}
			}
			if (($regalum->numero != $numalum and $pertipo == "P") or $actualizar==1) //se requiere para activar planilla por alumno (flechas)
			{
				$sql = "update $cursos set numero = '$numalum', apellidos = '$apellidos', nombres = '$nombres', tipoedu='$tipoedu' where id_alumno = '$id_alumno' limit 1";
				execsql($sql);
			}
			//traer notas totales del alumno a corte de periodos cerrados
			$nota = notas_asignatura($ano,$id_alumno,$id_asignatura,$corte,$datperi);
			//editar notas alumno
			$editar = 1;
			if (substr($regalum->fchretiro,0,1) > 0) //alumno retirado
				$editar = 0;
			if ($regalum->tipoedu != $tipoedu)
				$editar = 0;
			if ($pertipo == "E") //examen semestral, validar promedio de los periodos
			{
				$activmin = 0; //ignorar actividades para permitir editar nota
				$promexam = 0; //promedio para nota de examen semestral o final
				$promfin = 0; //promedio final
				$sumfin = 0;
				$numfin = 0;
				$suma = 0;
				$nume = 0;
				for($i=1; $i <= 5 ; $i++)
				{
					$sw = 1;
					if ($i==3) //nota examen 1 no promediarla
						$sw = 0;
					if ($periodo==1 and $i >= 3) //Examen semestral 1 promedia periodos 1 y 2
						$sw = 0;
					if ($periodo==2 and $i <= 3) //Examen semestral 2 promedia periodos 3 y 4
						$sw = 0;
					if ($sw==1 and $nota[$i]!="INAC" and $nota[$i]!="PEND")
					{
						$nume = $nume + 1;
						$suma = $suma + $nota[$i];
					}	
					if ($i!=3 and $nota[$i] > 0 and $nota[$i]!="INAC" and $nota[$i]!="PEND") //promedio definitivo
					{
						$numfin = $numfin + 1;
						$sumfin = $sumfin + $nota[$i];
					}	
				}
				if ($nume > 0)
					$promexam = round($suma/$nume,1);
				if ($numfin > 0)
					$promfin = round($sumfin/$numfin,1);
				 //no requiere examen si inactivo en los periodos
				if ($promexam==0)
					$editar = 0;
				 //no requiere examen si pasa promedio semestral o si pasa promedio final
				if ($promexam >= $notalim or $promfin >= $notalim)
					$editar = 0;
				//si tiene nota de examen activar para correccion
				if ($periodo==1 and $nota[3] > 0) //Examen semestral 1
					$editar = 1;
				if ($periodo==2 and $nota[6] > 0) //Examen semestral 2
					$editar = 1;
				if ($periodo==3 and $nota[7] > 0) //Examen final
					$editar = 1;
				$campoedi = "editar".$id_alumno;
				?>
				<input type="hidden" name="<?php echo $campoedi;?>" value="<?php echo $editar;?>">
				<?php
			}
			?>	
			<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
				<td><a class="link_list" href="alumnos_notas.php?id_alumno=<?php echo $id_alumno;?>" onclick="newWindow(this.href, '', 900, 630, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Ver..."><img src="images/alumnosm.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"></a><?php echo substr($apellidos."  ".$nombretiro,0,90);?></td>
				<?php
				//mostrar notas periodos
				$numnot = 0;
				$sumnot = 0; 
				foreach($datperi as $regperi)
				{
					$notatot = $nota[$regperi->notanum];
					$pendiente = 0;
					$inactivar = 0;
					//si corresponde al tipo y periodo de la planilla
					if ($regperi->pertipo==$pertipo and $regperi->periodo==$periodo)
					{
						//traer nuevamente nota total asignatura del periodo
						$sql="select nota, pendiente, inactivar from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = '$pertipo' and periodo = '$periodo' LIMIT 1";
						$datnot=viewsql($sql);
						foreach($datnot as $regnot)
						{
							$notatot = $regnot->nota;
							if (intval($regnot->pendiente) > 0)
								$notatot = "PEND";
							if (intval($regnot->inactivar) > 0)
								$notatot = "INAC";
						}
						$notatotQ = cualitativa($datpunt,$notatot);
						if (is_string($notatot))
						{
							if ($notatot=="INAC")
							{
								$notatot = 0;
								$inactivar = 1;
								$notatotQ = "INAC";
							}
							if ($notatot=="PEND")
							{
								$notatot = 0;
								$pendiente = 1;
								$notatotQ = "PEND";
							}
						}
						//nombres de los campos
						$camponot = "nota".$id_alumno;
						$campocal = "cali".$id_alumno;
						$campopen = "pend".$id_alumno;
						$campoina = "inac".$id_alumno;
						$porcentaje = 0;
						?>
						<td>
							<table border="0" cellpadding="0" cellspacing="0">
								<tr bgcolor="#FFFFDF">
								<?php
								$id_logro = 0;
								$campologro = "alu".$id_alumno."log".$id_logro;
								//notas varios logros
								if ($numlogr > 0)
								{
                                                                        if ($notaevi <= 0) {
									?>
									<td width="20" align="center"><a class="link_list" href="notas_alumnoact.php?numero=<?php echo $numalum;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>" onclick="newWindow(this.href, '', 950, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Planilla logros..."><img src="images/logrosm.png" width="16" height="16" border="0" hspace="0" align="absmiddle"></a></td>
									<td width="1" bgcolor="#CCCCCC"></td>
                                                                        <?php } else { ?>
									<td width="20" align="center"><a class="link_list" href="notas_alumno.php?numero=<?php echo $numalum;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>" onclick="newWindow(this.href, '', 970, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Planilla logros..."><img src="images/logrosm.png" width="16" height="16" border="0" hspace="0" align="absmiddle"></a></td>
									<td width="1" bgcolor="#CCCCCC"></td>
									<?php
                                                                        }
									$num = 0;
									foreach($datlogr as $reglogr)
									{
										$num = $num + 1;
										$id_logro = $reglogr->id_logro;
										$campologro = "alu".$id_alumno."log".$id_logro;
										$campoid = "alu".$id_alumno."log".$num;
										$notalog = "";
										$sql="select nota from $notasdet ".
											"where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
											"and id_evidencia='0' and id_actividad = '0' limit 1";
										$datnota=viewsql($sql);
										foreach($datnota as $regnota)
										{$notalog = intval($regnota->nota);}
										?>
										<td width="40" align="center" title="<?php echo "LOGRO ".$reglogr->logrocod;?>">
										<?php if (intval($reglogr->activmin) <= 0 and $editar==1) { ?>
										<input type="text" name="<?php echo $campologro;?>" id="<?php echo $campoid;?>" value="<?php echo $notalog;?>" maxlength="3" style="width: 25px; border: 1px solid <?php echo $gradocolor;?>;" onBlur="javascript:checkNota(this,'<?php echo $id_alumno;?>');" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
										<?php } else { if ($notalog == "") $notalog = 0; ?>
										<input type="text" name="<?php echo $campologro;?>" id="<?php echo $campoid;?>" value="<?php echo $notalog;?>" readonly style="width: 25px; border: 0px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
										<?php } ?>
										</td>
										<td width="1" bgcolor="#CCCCCC"></td>
										<?php
									}
								}
								else //nota sin logros
								{ ?>
								<td width="40" align="center">
									<?php if ($activmin <= 0 and $editar==1) { ?>
									<input type="text" name="<?php echo $campologro;?>" id="<?php echo $campologro;?>" value="<?php echo $notatot;?>" maxlength="3" style="width: 25px; border: 1px solid <?php echo $gradocolor;?>;" onBlur="javascript:checkNota(this,'<?php echo $id_alumno;?>');" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php } else { ?>
									<input type="text" name="<?php echo $campologro;?>" id="<?php echo $campologro;?>" value="<?php echo $notatot;?>" readonly style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php } ?>
								</td>
								<td width="1" bgcolor="#CCCCCC"></td>
								<?php } ?>
								<td width="40" align="center"><input type="text" name="<?php echo $camponot;?>" id="<?php echo $camponot;?>" value="<?php echo $notatot;?>" readonly style="width: 28px; border: 0px; text-align: right;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
								<td width="1" bgcolor="#CCCCCC"></td>
								<td width="40" align="center"><input type="text" name="<?php echo $campocal;?>" id="<?php echo $campocal;?>" value="<?php echo $notatotQ;?>" readonly style="width: 28px; border: 0px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
								<td width="1" bgcolor="#CCCCCC"></td>
								<td width="25" align="center">
									<a href="notas_observa.php?numero=<?php echo $numalum;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&pertipo=<?php echo $pertipo;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>" onclick="newWindow(this.href, '', 900, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Observaciones"><img src="images/comment.png" width="16" height="16" border="0" hspace="0" align="absmiddle"></a>
									<input type="hidden" name="<?php echo $campopen;?>" value="<?php echo $pendiente;?>">
									<input type="hidden" name="<?php echo $campoina;?>" value="<?php echo $inactivar;?>">
								</td>
								<td width="1" bgcolor="#CCCCCC"></td>
								</tr>
							</table>
						</td>
						<?php
					}
					else //si no es periodo de la planilla solo muestra la nota
					{ ?>
						<td align="center"><?php echo $notatot;?></td>
					<?php
					}
					if ($regperi->pertipo=="P" and $regperi->notanum <= $corte and $notatot!="PEND" and $notatot!="INAC")
					{
						$numnot = $numnot + 1;
						if ($regperi->notanum==$corte)
							$sumnot = $sumnot + $notatot;
						else
							$sumnot = $sumnot + $nota[$regperi->notanum];
					}
				} //cierre for periodos
				if ($estadoper=="ABIERTO" and $numnot > 0) //recalcular definitiva a�adiendo periodo actual
				{
					$sumnot = round($sumnot/$numnot,1);
					$nota[9] = $sumnot;
				}
				$notaQ = cualitativa($datpunt,$nota[9]);
				$color = "#000000";
				if ($nota[9] < $notalim) //asignatura perdida
					$color = "#E50000";
				?>
				<th align="center"><font color="<?php echo $color;?>"><?php echo $nota[9]." ".$notaQ;?></font></th>
			</tr>
			<?php
			} //cierre for alumnos ?>
			<tr height="36">
				<th align="left" bgcolor="#CCCCCC" background="images/backsilver.gif"> TOTAL ALUMNOS <?php echo $numalum;?></th>
				<th colspan="8" align="right" bgcolor="#CCCCCC" background="images/backsilver.gif">
					<?php if (($estado=="ABIERTA" and $_SESSION["nivnot"] >= 2 and $id_profesor == $_SESSION["usunro"]) or $_SESSION["nivnot"] >= 4) { ?>
					<select name="estado" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					<option value="" selected></option>
					<?php
					$sql="SELECT * FROM listas WHERE listip = 'PLANILLAS' order by id";
					if ($estado=="ABIERTA" or ($_SESSION["nivnot"] <= 3 and $estado!="CERRADA"))
						$sql="SELECT * FROM listas WHERE listip = 'PLANILLAS' AND lisnom!='CERRADA' order by id";
					$lista=viewsql($sql);
					foreach($lista as $reglis)
					{
						if ($estado == $reglis->lisnom)
							echo "<option value='$reglis->lisnom' selected>PLANILLA $reglis->lisnom</option>";
						else
							echo "<option value='$reglis->lisnom'>PLANILLA $reglis->lisnom</option>";
					}
					?>																
					</select>
					&nbsp;
					<input type="image" class="botones_img" name="grabar" src="images/btn_grabar.png" width="70" height="24" border="0" alt="Grabar" align="absmiddle" hspace="10">
					<?php } ?>
					<a href="notas_imprimir.php?id_asignatura=<?php echo $id_asignatura;?>&id_profasig=<?php echo $id_profasig;?>&grado=<?php echo $grado;?>&curso=<?php echo $curso;?>&pertipo=<?php echo $pertipo;?>&periodo=<?php echo $periodo;?>&notanum=<?php echo $corte;?>&tipoedu=<?php echo $tipoedu;?>" onclick="newWindow(this.href, '', 550, 320, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Imprimir..."><img src="images/btn_imprimir.png" width="70" height="24" border="0" hspace="10" align="absmiddle"></a>
					<?php if ($_SESSION["usutip"]=="DOCENTE") { ?>
					<a href="notas.php#<?php echo $seccion;?>"><img src="images/btn_regresar.png" border="0" width="70" height="24" alt="Regresar" hspace="10" align="absmiddle"></a>
					<?php } else { ?>
					<a href="notas.php?grado=<?php echo $grado;?>&curso=<?php echo $curso;?>#<?php echo $seccion;?>"><img src="images/btn_regresar.png" border="0" width="70" height="24" alt="Regresar" hspace="10" align="absmiddle"></a>
					<?php } ?>
				</th>
			</tr>
		</form>
		</table>		
    	</td>
		<td></td>
	</tr>
    <tr>
        <td colspan="3" valign="top">
            <div id="fondo_pie">
            <?php include("includes/menu_bottom.php");?>
            </div>
        </td>
    </tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>
