<?php
/***********************************************************************************************
* @file        : asignaturas_logros.php                                                        *
* @brief       : Lista y edita logros de la asignatura                                         *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 7-dic-2017                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
//error_reporting(E_ALL); 
//ini_set("display_errors", 1);
$id_asignatura = $_GET["id_asignatura"];
$id_logro = isset($_GET['id_logro']) ? $_GET['id_logro'] : null ;
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : null ;
if ($opcion == "")
	$opcion = "listar";
connect();
$sql="SELECT ano, grado, asignatura, evidemin, evideprom, evidepor, activmin, activprom, activpor FROM asignaturas WHERE id_asignatura = '$id_asignatura' limit 1";
$datasig=viewsql($sql);
foreach($datasig as $regasig)
{
	$ano = $regasig->ano;
	$grado = $regasig->grado;
	$asignatura = $regasig->asignatura;
	$evidemin = intval($regasig->evidemin);
	$evidepor = $regasig->evidepor;
	$evideprom = $regasig->evideprom;
	$activmin = intval($regasig->activmin);
	$activpor = $regasig->activpor;
	$activprom = $regasig->activprom;
	$aevidemin = $evidemin;
	$aevidepor = $evidepor;
	$aactivmin = $activmin;
	$aactivpor = $activpor;
}
$notaevi = 0;
$sql="SELECT notaevi FROM anoslect WHERE ano = '$ano' limit 1";
$datano=viewsql($sql);
foreach($datano as $regano)
{
    $notaevi = intval($regano->notaevi);
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/actividades.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.logrocod.value == '')
	{alert('Falta asignar C�digo de Logro...');document.form1.logrocod.focus(); return false;}
	if(document.form1.logro.value == '')
	{alert('Falta asignar texto b�sico del logro...');document.form1.logro.focus(); return false;}
	if(document.form1.logrosi.value == '')
	{alert('Falta asignar texto exitoso del logro...');document.form1.logrosi.focus(); return false;}
	if(document.form1.logrono.value == '')
	{alert('Falta asignar texto no exitoso del logro...');document.form1.logrono.focus(); return false;}
	if (document.form1.chkp1.value != 'X' && document.form1.chkp2.value != 'X' && document.form1.chkp3.value != 'X' && document.form1.chkp4.value != 'X')
	{alert('Falta seleccionar periodos de evaluaci�n...'); return false;}
	if(document.form1.evidemin.value > 0 && document.form1.evideprom.value <= 0 && document.form1.totalevi.value != 100)
	{alert('Porcentajes de evidencias incorrectos, no suman 100...'); return false;}
	if(document.form1.activmin.value > 0 && document.form1.activprom.value <= 0 && document.form1.total.value != 100)
	{alert('Porcentajes de actividades incorrectos, no suman 100...'); return false;}
	return true;
}
function borrarconf()
{
	ok=confirm("Se borrar�n las NOTAS cargadas a este logro en todos los cursos. Esta seguro de borrar notas de este Logro?");
	if(ok)
	{
		document.form1.op.value='borrarlogro';
		document.form1.submit();
	}
}
function eliminarconf()
{
	ok=confirm("Se eliminar� DEFINITIVAMENTE el logro, sus actividades y metodolog�as. Esta seguro de eliminar este Logro?");
	if(ok)
	{
		document.form1.op.value='eliminarlogro';
		document.form1.submit();
	}
}
function checkPeri(obj)
{
	var valobj = obj.value;
	if (valobj == '')
		obj.value = 'X';
	else
		obj.value = '';
}
function checkCont(obj)
{
	var valobj = obj.value;
	if (valobj != '')
		obj.value = 'X';
}
function checkLogroSi(obj)
{
	var valobj = obj.value;
	if (valobj == '')
	{
		var logro = eval ('document.form1.logro.value');
		eval ("document.form1.logrosi.value='"+'!FELICITACIONES! '+logro+"'");
	}
}
function checkLogroNo(obj)
{
	var valobj = obj.value;
	if (valobj == '')
	{
		var logro = eval ('document.form1.logro.value');
		eval ("document.form1.logrono.value='"+'* DEBE '+logro+"'");
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/logro.jpg' width="40" height="40" border="0" align="absmiddle"> LOGROS <?php echo $asignatura;?></td>
		<td class="subtitulo" align="right">GRADO <?php echo $grado;?> A�O <?php echo $ano;?></td>
	</tr>
</table>
<?php if ($opcion == "listar") {
$file = "y".$ano."logros";
$sql="SELECT id_logro, logro, periodos, tipoedu, logrocod, activmin FROM $file WHERE id_asignatura = '$id_asignatura' order by logrocod ";
$datlogr=viewsql($sql);
?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="420" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="35" rowspan="2">Logro</th>
					<th width="25" rowspan="2" title="Formal/Especial">Tipo</th>
					<th rowspan="2">Descripci�n Logro</th>
					<th rowspan="2" title="Metodolog�a">Metod</th>
                                        <?php if ($notaevi<=0) {?>
					<th colspan="4" width="135">Actividades Logro</th>
                                        <?php } else { ?>
					<th colspan="4" width="135">Evidencias - Actividades</th>
                                        <?php } ?>
				</tr>
				<tr>
					<th>I</th>
					<th>II</th>
					<th>III</th>
					<th>IV</th>
				</tr>
				<?php
				foreach($datlogr as $reglogr)
				{
				$id_logro = $reglogr->id_logro;
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><a class="link_list" href="asignaturas_logros.php?opcion=editar&id_asignatura=<?php echo $id_asignatura;?>&id_logro=<?php echo $id_logro;?>" title="C�digo Logro"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $reglogr->logrocod;?></a></td>
					<td align="center" title="Formal/Especial"><?php echo $reglogr->tipoedu;?></td>
					<td><?php echo $reglogr->logro;?></td>
					<td width="25" align="center"><a class="link_black" href="asignaturas_metod.php?id_asignatura=<?php echo $id_asignatura;?>&id_logro=<?php echo $id_logro;?>" title="Metodolog�a"><img src="images/academico.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"></a></td>					
					<?php
                                        if ($notaevi <= 0) {
                                            $url = "asignaturas_activ.php";
                                        }
                                        else {
                                            $url="asignaturas_evide.php";
                                        }
					for($i=1; $i <= 4 ; $i++)
					{
						$seekper = $i." ";
						$pos = strpos($reglogr->periodos,$seekper);
						if ($pos > 0 and $reglogr->activmin > 0) //si periodo en logro y actividades minimas
						{ ?>
						<td width="25"><a class="link_black" href="<?php echo $url;?>?id_asignatura=<?php echo $id_asignatura;?>&id_logro=<?php echo $id_logro;?>&periodo=<?php echo $i;?>" title="Periodo <?php echo $i;?>"><img src="images/actividadsm.png" width="12" height="12" border="0" hspace="0" align="absmiddle"><?php echo $i;?></a></td>
						<?php } else { ?>
						<td width="25"></td>
						<?php }
					}
					?>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 3 and $ano == $_SESSION["anoaca"] and $activmin > 0) { ?>
			<a href="asignaturas_logros.php?opcion=editar&id_asignatura=<?php echo $id_asignatura;?>"><img src="images/btn_nuevo.png" border="0" width="75" height="24" alt="Nuevo" hspace="10"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
<?php } if ($opcion == "editar") {
$titulo = "Nuevo Logro";
$titact = "Actividades Logro";
$datlogro = datos_logro($ano,$id_logro);
if ($id_logro > 0)
{
        $evidemin = intval($datlogro["evidemin"]);
	$evidepor = $datlogro["evidepor"];
	$activmin = intval($datlogro["activmin"]); 
	$activpor = $datlogro["activpor"]; 
        $sql = "SELECT id_evidencia FROM y".$ano."eviden WHERE id_asignatura='$id_asignatura' AND id_logro = '$id_logro'";
        $datevi=viewsql($sql);
        if (($evidemin <= 0 and $aevidemin > 0) or ($notaevi > 0 and count($datevi) <= 0)) //asigna evidencias de asignatura
        {
                $evidemin = $aevidemin;
                $evidepor = $aevidepor;
                $activmin = $aactivmin;
                $activpor = $aactivpor;
        }
}
$porevi = array();
$por = array();
for($i=1; $i<=10 ;$i++)
{
    $porevi[$i] = "";
    $por[$i] = "";
}
$pos = 0;
$totalevi = 0;
for($i=1; $i<=$evidemin ;$i++)
{
	$porevi[$i] = trim(substr($evidepor,$pos,3));
	$pos = $pos + 3;
	$totalevi = $totalevi + intval($porevi[$i]);
}
$pos = 0;
$total = 0;
for($i=1; $i<=$activmin ;$i++)
{
	$por[$i] = trim(substr($activpor,$pos,3));
	$pos = $pos + 3;
	$total = $total + intval($por[$i]);
}
$readonly = ""; //Si algun periodo ya se cerro no permite modificaciones del nombre ni porcentajes
$chkp1 = "";
$modp1 = 1;
$chkp2 = "";
$modp2 = 1;
$chkp3 = "";
$modp3 = 1;
$chkp4 = "";
$modp4 = 1;
if (strpos($datlogro["periodos"],'1') > 0)
	$chkp1 = "X";
if (strpos($datlogro["periodos"],'2') > 0)
	$chkp2 = "X";
if (strpos($datlogro["periodos"],'3') > 0)
	$chkp3 = "X";
if (strpos($datlogro["periodos"],'4') > 0)
	$chkp4 = "X";
//traer periodos
$sql="SELECT periodo, estado FROM periodos WHERE ano = '$ano' and pertipo = 'P' order by periodo";
$datperi = viewsql($sql);
if ($id_logro > 0)
{
	$titulo = "Editar Logro";
	//verificar que logro no tenga notas para autorizar modificaci�n de periodos
	foreach($datperi as $regperi)
	{
		$periodo = $regperi->periodo;
		if (strpos($datlogro["periodos"],$periodo) > 0) //si activo en el periodo
		{
			if ($regperi->estado == "CERRADO")
				$readonly = "readonly";
			$notasp = "y".$ano."notasp".$periodo;
			$sql="select id_nota from $notasp where id_logro = '$id_logro' and nota > '0' limit 1";
			$datnota=viewsql($sql);
			if (count($datnota) > 0)
			{
				if ($periodo==1)
					$modp1 = 0;
				if ($periodo==2)
					$modp2 = 0;
				if ($periodo==3)
					$modp3 = 0;
				if ($periodo==4)
					$modp4 = 0;
			}
		}
	}
}
//si tiene notas en algun periodo no se puede borrar
$swborrar = 1;
if ($modp1==0 or $modp2==0 or $modp3==0 or $modp4==0)
	$swborrar = 0;
//verificar estado de los periodos
foreach($datperi as $regperi)
{
	if ($regperi->periodo==1 and $regperi->estado=="CERRADO")
		$modp1 = 0;
	if ($regperi->periodo==2 and $regperi->estado=="CERRADO")
		$modp2 = 0;
	if ($regperi->periodo==3 and $regperi->estado=="CERRADO")
		$modp3 = 0;
	if ($regperi->periodo==4 and $regperi->estado=="CERRADO")
		$modp4 = 0;
}
//cursos del grado
$sql = "select curso from cursos where ano = '$ano' and grado = '$grado' order by curso"; 
$datcurs = viewsql($sql);
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarlogro'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type="hidden" name="grado" value="<?php echo $grado;?>">
<input type='hidden' name='id_logro' value='<?php echo $id_logro;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='oldperiodos' value='<?php echo $datlogro["periodos"];?>'>
<input type='hidden' name='evideprom' value='<?php echo $evideprom;?>'>
<input type='hidden' name='activprom' value='<?php echo $activprom;?>'>
	<tr height="10">
		<td width="20"></td>
		<td></td>
		<td width="20"></td>
		<td></td>
		<td width="20"></td>
	</tr>
	<tr valign="top">
		<td></td>
		<td>
                        <fieldset>
			<legend class="titulo_campos">Logro B�sico</legend>
			<table border="0" cellpadding="1" cellspacing="1">
                            <tr>
                                <td width='125'>C�digo en Planilla <font color="red">*</font> &nbsp;</td>
                                <td width="50"><input type="text" name="logrocod" style="width: 30px;" value="<?php echo $datlogro["logrocod"];?>" maxlength="3" <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				<td>
                                    <select name="tipoedu" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                        <?php
					$sql="SELECT * FROM listas WHERE listip = 'EDUCACION' order by id";
					$lista=viewsql($sql);
					foreach($lista as $reglis)
					{
						if ($datlogro["tipoedu"] == $reglis->lisval)
							echo "<option value='$reglis->lisval' selected>EDUCACION $reglis->lisnom</option>";
						else
							echo "<option value='$reglis->lisval'>EDUCACION $reglis->lisnom</option>";
					}
					?>																
                                    </select>
				</td>
                            </tr>
                            <?php if ($notaevi > 0) {
                            $titact = "Actividades Evidencia";
                            ?>
                            <tr>
				<td>Evidencias Logro &nbsp;</td>
                                <td>
                                    <?php if ($_SESSION["nivasi"] >= 4 and $modp1==1 and $modp2==1 and $modp3==1 and $modp4==1) { ?>
                                    <input type="text" name="evidemin" style="width: 30px;" value="<?php echo $evidemin;?>" maxlength="2" onBlur="javascript:checkEst(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    <?php } else { ?>
                                    <input type="text" name="evidemin" style="width: 30px;" value="<?php echo $evidemin;?>" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    <?php } ?>
                                </td>
				<td>
                                    <?php if ($evideprom <= 0) { ?>
                                    <table border="0" cellpadding="1" cellspacing="1" align="left">
                                        <tr align='left'>
                                            <td>Porcentajes %&nbsp;</td>
                                            <?php
                                            for($i=1; $i<=10 ;$i++)
                                            {
                                            $namediv = "divevi".$i;
                                            $namepor = "porevi".$i;
                                            $displayevi = "none";
                                            if ($i <= $evidemin)
                                                $displayevi = "block";
                                            ?>
                                            <td>
                                            <div id="<?php echo $namediv;?>" style="display:<?php echo $displayevi;?>;">
                                            <input type="text" name="<?php echo $namepor;?>" style="width: 30px;" value="<?php echo $porevi[$i];?>" maxlength="3" <?php echo $readonly;?> onBlur="javascript:checkPorEvi(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                            </div>
                                            </td>
                                            <?php } ?>
                                            <td><input type="hidden" name="totalevi" value="<?php echo $totalevi;?>"></td>
                                        </tr>
                                    </table>
                                    <?php } ?>
				</td>
                            </tr>
                            <?php } else { ?>
                            <input type="hidden" name="evidemin" value="0">
                            <input type="hidden" name="totalevi" value="0">
                            <?php } ?>
                            <tr>
				<td><?php echo $titact;?> &nbsp;</td>
                                <td>
                                    <?php if ($_SESSION["nivasi"] >= 4 and $modp1==1 and $modp2==1 and $modp3==1 and $modp4==1) { ?>
                                    <input type="text" name="activmin" style="width: 30px;" value="<?php echo $activmin;?>" maxlength="2" onBlur="javascript:checkEst(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    <?php } else { ?>
                                    <input type="text" name="activmin" style="width: 30px;" value="<?php echo $activmin;?>" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    <?php } ?>
                                </td>
				<td>
                                    <?php if ($activprom <= 0) { ?>
                                    <table border="0" cellpadding="1" cellspacing="1" align="left">
                                        <tr>
                                            <td>Porcentajes %&nbsp;</td>
                                            <?php
                                            for($i=1; $i<=10 ;$i++)
                                            {
                                            $namediv = "div".$i;
                                            $namepor = "por".$i;
                                            $display = "none";
                                            if ($i <= $activmin)
                                                    $display = "block";
                                            ?>
                                            <td>
                                                <div id="<?php echo $namediv;?>" style="display:<?php echo $display;?>;">
                                                <input type="text" name="<?php echo $namepor;?>" style="width: 30px;" value="<?php echo $por[$i];?>" maxlength="3" <?php echo $readonly;?> onBlur="javascript:checkPor(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                                </div>
                                            </td>
                                            <?php } ?>
                                            <td>&nbsp;(Default)<input type="hidden" name="total" value="<?php echo $total;?>"></td>
                                        </tr>
                                    </table>
                                    <?php } ?>
				</td>
                            </tr>
                            <tr>
                                <td colspan="3">
                                <textarea name="logro" rows="4" cols="125" style="text-transform:uppercase;" <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datlogro["logro"];?></textarea>
                                </td>
                            </tr>
			</table>
			</fieldset>
			<br>
			<fieldset>
			<legend class="titulo_campos">Logro Exitoso</legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td height="5"></td></tr>
				<tr>
					<td>
					<textarea name="logrosi" rows="4" cols="125" style="text-transform:uppercase;" <?php echo $readonly;?> onFocus="javascript:checkLogroSi(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datlogro["logrosi"];?></textarea>
					</td>
				</tr>
			</table>
			</fieldset>
			<br>
			<fieldset>
			<legend class="titulo_campos">Logro NO Exitoso</legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td height="5"></td></tr>
				<tr>
					<td>
					<textarea name="logrono" rows="4" cols="125" style="text-transform:uppercase;" <?php echo $readonly;?> onFocus="javascript:checkLogroNo(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datlogro["logrono"];?></textarea>
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Periodos</legend>
			<table border="0" align="center">
				<tr><td width="40" class="subtitulo" align="center">1
				<?php if ($modp1 == 1) { ?>
				<input type="text" name="chkp1" style="width: 12px; font-weight: bold;" value="<?php echo $chkp1;?>" maxlength="1" onClick="javascript:checkPeri(this);" onKeyUp="javascript:checkCont(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } else { ?>
				<input type="text" name="chkp1" style="width: 12px; font-weight: bold;" value="<?php echo $chkp1;?>" readonly="readonly" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } ?>
				</td></tr>
				<tr><td width="40" class="subtitulo" align="center">2
				<?php if ($modp2 == 1) { ?>
				<input type="text" name="chkp2" style="width: 12px; font-weight: bold;" value="<?php echo $chkp2;?>" maxlength="1" onClick="javascript:checkPeri(this);" onKeyUp="javascript:checkCont(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } else { ?>
				<input type="text" name="chkp2" style="width: 12px; font-weight: bold;" value="<?php echo $chkp2;?>" readonly="readonly" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } ?>
				</td></tr>
				<tr><td width="40" class="subtitulo" align="center">3
				<?php if ($modp3 == 1) { ?>
				<input type="text" name="chkp3" style="width: 12px; font-weight: bold;" value="<?php echo $chkp3;?>" maxlength="1" onClick="javascript:checkPeri(this);" onKeyUp="javascript:checkCont(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } else { ?>
				<input type="text" name="chkp3" style="width: 12px; font-weight: bold;" value="<?php echo $chkp3;?>" readonly="readonly" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } ?>
				</td></tr>
				<tr><td width="40" class="subtitulo" align="center">4
				<?php if ($modp4 == 1) { ?>
				<input type="text" name="chkp4" style="width: 12px; font-weight: bold;" value="<?php echo $chkp4;?>" maxlength="1" onClick="javascript:checkPeri(this);" onKeyUp="javascript:checkCont(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } else { ?>
				<input type="text" name="chkp4" style="width: 12px; font-weight: bold;" value="<?php echo $chkp4;?>" readonly="readonly" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } ?>
				</td></tr>
			</table>
			</fieldset>
			<br>
			<fieldset>
			<legend class="titulo_campos">Inactivar Cursos</legend>
			<table border="0" align="center">
                                <?php
                                //$datlogro["cursos"]
                                foreach($datcurs as $regcurs)
                                {
                                    $curso = $regcurs->curso;
                                    $chkcurso = "";
                                    if (strpos($datlogro["cursos"],$curso) > 0)
                                            $chkcurso = "X";
                                    ?>
                                    <tr height="25"><td class="subtitulo" align="left"><?php echo $curso;?>
                                    <input type="text" name="<?php echo $curso;?>" style="width: 12px; font-weight: bold;" value="<?php echo $chkcurso;?>"  maxlength="1" onClick="javascript:checkPeri(this);" onKeyUp="javascript:checkCont(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    </td></tr>
                           <?php } ?>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr height="36">
		<td colspan="5" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_logro > 0 and $ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 3 and $swborrar==1) { ?>
                        <?php if ($_SESSION["nivasi"] == 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_borrar.png" border="0" width="70" height="24" alt="Borrar Notas" hspace="10"></a>
                        <?php } ?>
			<a href="javascript:;" onClick="eliminarconf();"><img src="images/btn_eliminar.png" border="0" width="70" height="24" alt="Eliminar Logro" hspace="10"></a>
			<?php } ?>
			<a href="asignaturas_logros.php?id_asignatura=<?php echo $id_asignatura;?>"><img src="images/btn_regresar.png" border="0" width="70" height="24" alt="Cerrar" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
