<?php
/***********************************************************************************************
* @file        : notas_alumno.php                                                              *
* @brief       : planilla de notas alumno de logros, evidencias y actividades del periodo      *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-dic-2017                                                    *
* @Modificado  : Nora Rodriguez 27-ene-2017                                                    *
***********************************************************************************************/
 
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_notas.php");
include("libs/fechas.php");
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$numero = isset($_GET['numero']) ? $_GET['numero'] : null ;
$alumtot = isset($_GET['alumtot']) ? $_GET['alumtot'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_director = isset($_GET['id_director']) ? $_GET['id_director'] : null ;
$periodo = $_GET["periodo"];
$estado = $_GET["estado"];
$pertipo = "P";
connect();
//datos asignatura
$sql="select ano, grado, asignatura, evideprom, activprom from asignaturas where id_asignatura = '$id_asignatura' limit 1";
$datasig = viewsql($sql);
foreach($datasig as $regasig)
{
	$ano = $regasig->ano;
	$grado = $regasig->grado;
	$asignatura = $regasig->asignatura;
	$evideprom = $regasig->evideprom;
	$activprom = $regasig->activprom;
}
//traer id_alumno del curso
if ($numero > 0)
{
	$cursos = "y".$ano."cursos";
	$sql="SELECT id_alumno, apellidos, nombres, tipoedu, fchretiro FROM $cursos WHERE grado = '$grado' and curso = '$curso' and numero = '$numero' limit 1";
	$datcurs=viewsql($sql);
	foreach($datcurs as $regcurs)
	{
		$id_alumno = $regcurs->id_alumno;
		$alumno = $regcurs->apellidos." ".$regcurs->nombres;
		$tipoedu = $regcurs->tipoedu;
		$fchretiro = $regcurs->fchretiro;
	}
	$numero = intval($numero);
	$alumtot = intval($alumtot);
	$numeroant = $numero - 1;
        $numerosig = $numero + 1;
	if ($numeroant <= 0) {
          $numeroant = 1;  
        }
	if ($numerosig > $alumtot) {
           $numerosig = $alumtot; 
        }
}
else
{
	$sql="SELECT apellidos, nombres, tipoedu, fchretiro FROM alumnos WHERE id_alumno = '$id_alumno' limit 1";
	$datalum=viewsql($sql);
	foreach($datalum as $regalum)
	{
		$alumno = $regalum->apellidos." ".$regalum->nombres;
		$tipoedu = $regalum->tipoedu;
		$fchretiro = $regalum->fchretiro;
	}
}
//datos alumno
if ($tipoedu == "E")
	$alumno = $alumno." (ESPECIAL)";
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$areasper = $reganol->areasper;
	$notalim = $reganol->notalim;
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notaevi = $reganol->notaevi;
}
//puntajes
$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
$datpunt = viewsql($sql);
$numpunt = count($datpunt);
//logros de la asignatura y periodo
$file = "y".$ano."logros";
$sql = "SELECT id_logro, logrocod, logro, evidemin, activmin, cursos FROM $file WHERE id_asignatura = '$id_asignatura' AND periodos LIKE '%$periodo%' ";
$sql.="AND tipoedu = '$tipoedu' ORDER BY logrocod";
$datlogr=viewsql($sql);
$nrolog = count($datlogr);
//evidencias de los logros de la asignatura y periodo
$fileevi = "y".$ano."eviden";
$sql = "SELECT * FROM $fileevi WHERE id_asignatura = '$id_asignatura' AND periodo = '$periodo' ORDER BY id_logro, numero";
$datevid = viewsql($sql);
//actividades de los logros de la asignatura y periodo
$fileact = "y".$ano."activ";
$sql = "SELECT * FROM $fileact WHERE id_asignatura = '$id_asignatura' AND periodo = '$periodo' ORDER BY id_evidencia, numero";
$datacti = viewsql($sql);
$notas = "y".$ano."notas";
$notasdet = "y".$ano."notasp".$periodo;
$readonly="readonly";
$action="";
if (($_SESSION["nivnot"] >= 2 and $estado == "ABIERTA") or ($_SESSION["nivnot"] >= 4 and $estado!="CONSULTA"))
{
	$readonly="";
	$action="database/grabar_planillas.php";
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/notas.js'></script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/logro.jpg' width="40" height="40" border="0" align="absmiddle"> LOGROS Y ACTIVIDADES PERIODO <?php echo $periodo;?> - <?php echo $asignatura;?></td>
		<td class="subtitulo" align="right">GRADO <?php echo $grado;?> A�O <?php echo $ano;?></td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td colspan="2" height="30" class="tabla_color">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
					&nbsp;&nbsp;
					<?php if ($numero > 0) { ?>
					<a href="notas_alumno.php?numero=<?php echo $numeroant;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>"><img src="images/arrback.png" border="0" width="24" height="24" hspace="2" title="Alumno Anterior...<?php echo $numeroant;?>" align="absmiddle"></a>
					<?php } ?>
					<img src="images/alumnosm.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"> <b><?php echo $alumno;?></b>
					<?php if ($numero > 0) { ?>
					<a href="notas_alumno.php?numero=<?php echo $numerosig;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>"><img src="images/arrforwardt.png" border="0" width="24" height="24" hspace="2" title="Alumno Siguiente...<?php echo $numerosig;?>" align="absmiddle"></a>
					<?php } ?>
					</td>
					<td class="titulo_error" align="left">
					&nbsp;
					<?php if (substr($fchretiro,0,1) > 0) { ?>
					(RETIRADO EL <?php echo fecha_texto($fchretiro);?>)
					<?php } ?>
					</td>
					<td align="right">
					<a class="link_black" href="notas_observa.php?numero=<?php echo $numero;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&pertipo=<?php echo $pertipo;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>" onclick="newWindow(this.href, '', 900, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Observaciones"><img src="images/comment.png" width="16" height="16" border="0" hspace="0" align="absmiddle"> Observaciones</a>					
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="<?php echo $action;?>" method="post" name="form1">
<input type='hidden' name='op' value='planilla_alumno'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='curso' value='<?php echo $curso;?>'>
<input type='hidden' name='id_director' value='<?php echo $id_director;?>'>
<input type="hidden" name="periodo" value="<?php echo $periodo;?>">
<input type="hidden" name="estado" value="<?php echo $estado;?>">
<input type='hidden' name='numero' value='<?php echo $numero;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type="hidden" name="tipoedu" value="<?php echo $tipoedu;?>">
<input type="hidden" name="notamin" value="<?php echo $notamin;?>">
<input type="hidden" name="notamax" value="<?php echo $notamax;?>">
<input type='hidden' name='evideprom' value='<?php echo $evideprom;?>'>
<input type='hidden' name='activprom' value='<?php echo $activprom;?>'>
<input type='hidden' name='alumtot' value='<?php echo $alumtot;?>'>
<?php
//cargar puntajes de evaluacion
$numpunt = 0;
foreach($datpunt as $regpunt)
{
	$numpunt = $numpunt + 1;
	$punt = trim($regpunt->notamax);
	if (strlen($punt)==1)
		$punt = "00".$punt;
	if (strlen($punt)==2)
		$punt = "0".$punt;
	$name = "puntaje".$numpunt;
	$puntaje = $punt.$regpunt->notaQ;
	?>
	<input type="hidden" name="<?php echo $name;?>" id="<?php echo $name;?>" value="<?php echo $puntaje;?>">
	<?php
}
?>
	<tr height="20">
		<th width="30">Item</th>
		<th>Descripci�n</th>
		<th width="30" title="Pendiente Nota">Pend</th>
		<th width="30" title="Inactivar Promedios">Inac</th>
                <th width="35" title="Nota Actividad">Nota<br>Activ</th>
                <th width="35" title="Nota Evidencia">Nota<br>Evid</th>
		<th width="35">Nota<br>Logro</th>
		<th width="35">Ajuste<br>Examen</th>
	</tr>
	<?php
	$inactivar = 0; //inactivar asignatura
	$notatot = 0;
	$notatotQ = "";
	$notatotEx = "";
	$notatotExQ = "";
	$sql="SELECT nota, notaEx, notaExF, inactivar FROM $notas WHERE id_alumno = '$id_alumno' AND id_asignatura = '$id_asignatura' AND pertipo = '$pertipo' AND periodo = '$periodo' LIMIT 1";
	$dattot=viewsql($sql);
	foreach($dattot as $regtot)
	{
		$notatot = $regtot->nota;
		if ($regtot->notaEx > 0)
			$notatotEx = $regtot->notaEx;
		if ($regtot->notaExF > 0)
			$notatotEx = $regtot->notaExF;
		//$inactivar = $regtot->inactivar;
	}
	$notatotQ = cualitativa($datpunt,$notatot);
	$notatotExQ = cualitativa($datpunt,$notatotEx);
        $numlog = 0; //consecutivo logros
	foreach($datlogr as $reglogr)
	{
		$numlog = $numlog + 1;
		$id_logro = $reglogr->id_logro;
		$evidemin = intval($reglogr->evidemin);
		$activmin = intval($reglogr->activmin);
		$campolog = "logro".$id_logro;
		$campopen = "chkpen".$id_logro;
		$campoina = "chkina".$id_logro;
		$campologid = "logro".$numlog;
		$campopenid = "chkpen".$numlog;
		$campoinaid = "chkina".$numlog;
		$titleevi = "";
		if ($evidemin > 0)
			$titleevi = "Evidencias m�nimas ".$evidemin;
		//inactivar logro segun el curso
		$inactivarlog = 0;
		$chkina = "";
		$chkpen = "";
		$notalog = "";
		$notalogEx = "";
		if (strpos($reglogr->cursos,$curso) > 0)
		{
			$chkina = "checked";
			$inactivarlog = 1;
		}
                //traer notas alumno
		$sql="select nota, notaEx, numEx, pendiente, inactivar from $notasdet ".
			"where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
			"and id_evidencia = '0' and id_actividad = '0' limit 1";
		$datnota=viewsql($sql);
		foreach($datnota as $regnota)
		{
			$notalog = intval($regnota->nota);
			if ($regnota->notaEx > 0)
				$notalogEx = $regnota->notaEx." E".$regnota->numEx;
			$inactivarlog = intval($regnota->inactivar);
			if ($inactivarlog > 0 or $inactivar > 0) //inactivar logro alumno o inactivar asignatura
				$chkina = "checked";
			if ($regnota->pendiente > 0 and $inactivarlog==0)
				$chkpen = "checked";
		}
		$nroevi = 0; //evidencias por el logro
		foreach($datevid as $regevi)
		{
			if ($regevi->id_logro == $id_logro) {
				$nroevi = $nroevi + 1;
                        }
		}
		//editar notas alumno
		$editar = 1;
		if (substr($fchretiro,0,1) > 0 or $inactivar > 0 or $inactivarlog > 0) //alumno retirado o asignatura inactiva o logro inactivo
			$editar = 0;
		?>
		<tr height="20" bgcolor="#7AC52C">
			<td>L<?php echo $reglogr->logrocod;?></td>
			<td><?php echo $reglogr->logro;?></td>
			<td align="center" title="Pendiente Nota"><input type="checkbox" name="<?php echo $campopen;?>" id="<?php echo $campopenid;?>" <?php echo $chkpen;?> tabindex="1" /></td>
			<td align="center" title="Inactivar Promedios">
				<?php if ($_SESSION["nivnot"] >= 4) { ?>
				<input type="checkbox" name="<?php echo $campoina;?>" id="<?php echo $campoinaid;?>" <?php echo $chkina;?> <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
				<?php } else { ?>
				<input type="checkbox" name="<?php echo $campoina;?>" id="<?php echo $campoinaid;?>" <?php echo $chkina;?> readonly onclick="javascript: return false;"onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
				<?php } ?>
			</td>
			<td></td>
			<td></td>
			<td align="center">
                            <input type="text" name="<?php echo $campolog;?>" id="<?php echo $campologid;?>" value="<?php echo $notalog;?>" readonly style="width: 25px;" title="<?php echo $titleact;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			</td>
			<td align="center"><?php echo $notalogEx;?></td>
		</tr>
		<?php
                //evidencias logro
		$numevi = 0;
		foreach($datevid as $regevi)
		{
			if ($regevi->id_logro == $id_logro)
			{
				$numevi = $numevi + 1;
				$id_evidencia = $regevi->id_evidencia;
				$porcentaje = "";
				if ($evideprom <= 0)
					$porcentaje = "% ".$regevi->porcentaje;
				$camponom = "logro".$id_logro."evi".$id_evidencia;
				$camponid = "logro".$id_logro."evi".$numevi;
				$campopor = "logro".$id_logro."por".$numevi;
				$notaevi = "";
				$sql="select nota, notaEx, numEx from $notasdet ".
					"where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
					"and id_evidencia = '$id_evidencia' and id_actividad = '0' limit 1";
				$datnota=viewsql($sql);
				foreach($datnota as $regnota)
				{$notaevi = $regnota->nota;}
				?>		
				<tr height="20" bgcolor="#B8F865">
					<td align="right">E<?php echo $regevi->numero;?></td>
                                        <td colspan="3"><?php echo $regevi->evidencia;?></td>
					<td></td>
					<td align="center">
						<input type="text" name="<?php echo $camponom;?>" id="<?php echo $camponid;?>" value="<?php echo $notaevi;?>" readonly style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
						<input type="hidden" name="<?php echo $campopor;?>" id="<?php echo $campopor;?>" value="<?php echo intval($regevi->porcentaje);?>">
					</td>
					<td align="center"><?php echo $porcentaje;?></td>
					<td></td>
				</tr>
                                <?php
                                //actividades evidencia
                                $nroact = 0; //actividades por evidencia
                                foreach($datacti as $regacti)
                                {
                                        if ($regacti->id_evidencia == $id_evidencia)
                                                $nroact = $nroact + 1;
                                }
                                $numact = 0;
                                foreach($datacti as $regacti)
                                {
                                        if ($regacti->id_evidencia == $id_evidencia)
                                        {
                                                $numact = $numact + 1;
                                                $id_actividad = $regacti->id_actividad;
                                                $porcentaje = "";
                                                if ($activprom <= 0) {
                                                        $porcentaje = "% ".$regacti->porcentaje;
                                                }
                                                //$camponid = "logro".$numlog."evi".$numevi."act".$numact;
                                                $camponom = "logro".$id_logro."evi".$id_evidencia."act".$numact;
                                                $campopor = "logro".$id_logro."evi".$id_evidencia."por".$numact;
                                                $notaact = "";
                                                $sql="select nota, notaEx, numEx from $notasdet ".
                                                        "where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
                                                        "and id_evidencia = '$id_evidencia' and id_actividad = '$id_actividad' limit 1";
                                                $datnota=viewsql($sql);
                                                foreach($datnota as $regnota)
                                                {$notaact = intval($regnota->nota);}
                                                ?>		
                                                <tr height="20" bgcolor="#FFFFFF">
                                                        <td></td>
                                                        <td colspan="3"><?php echo $regacti->numero." - ".$regacti->actividad;?></td>
                                                        <td align="center">
                                                                <?php if ($editar==1) { ?>
                                                                <input type="text" name="<?php echo $camponom;?>" id="<?php echo $camponom;?>" value="<?php echo $notaact;?>" <?php echo $readonly;?> maxlength="3" style="width: 25px;  border: 1px solid #009900;" onBlur="javascript:checkNota(this,'<?php echo $id_logro;?>','<?php echo $id_evidencia;?>','<?php echo $nroevi;?>','<?php echo $nroact;?>');" onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
                                                                <?php } else { ?>
                                                                <input type="text" name="<?php echo $camponom;?>" id="<?php echo $camponom;?>" value="<?php echo $notaact;?>" readonly style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
                                                                <?php } ?>
                                                                <input type="hidden" name="<?php echo $campopor;?>" id="<?php echo $campopor;?>" value="<?php echo intval($regacti->porcentaje);?>">
                                                        </td>
                                                        <td align="center"><?php echo $porcentaje;?></td>
                                                        <td></td>
                                                        <td></td>
                                                </tr>
                                        <?php 
                                        }
                                } 
			}
		} 
	} ?>
	<tr height="22">
		<th colspan="5" align="right">NOTA ASIGNATURA&nbsp;</th>
		<th><input type="text" name="notatotQ" id="notatotQ" value="<?php echo $notatotQ;?>" readonly style="width: 25px; border: 0px; background-color: #E0E7E9; font-weight: bold;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></th>
		<th><input type="text" name="notatot" id="notatot" value="<?php echo $notatot;?>" readonly style="width: 30px; border: 0px; background-color: #E0E7E9; font-weight: bold;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></th>
		<th><?php echo $notatotEx." ".$notatotExQ;?></th>
	</tr>
	<tr height="36">
		<td colspan="8" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($action!="") { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
