<?php
/***********************************************************************************************
* @file        : transporte_conduct.php                                                        *
* @brief       : Listar y crear conductores rutas de transporte                                *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-Ago-2015                                                    *
* @Modificado  : Nora Rodriguez 21-Ago-2015                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_transporte.php");
include("libs/fechas.php");
include("libs/image_resize.php");
connect();
$id_monitor = isset($_GET['id_monitor']) ? $_GET['id_monitor'] : 0 ;
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : "listar" ;
$estado = isset($_GET['estado']) ? $_GET['estado'] : "ACTIVO" ;
$buscar = isset($_POST['buscar']) ? $_POST['buscar'] : null ;
$ano = $_SESSION["anoaca"];
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.apellidos.value == '')
	{alert('Falta asignar Apellidos...');document.form1.apellidos.focus(); return false;}
	if(document.form1.nombres.value == '')
	{alert('Falta asignar Nombres...');document.form1.nombres.focus(); return false;}
	if(document.form1.docutip.value == '')
	{alert('Falta asignar tipo de documento...');document.form1.docutip.focus(); return false;}
	if(document.form1.docunum.value <= 0)
	{alert('Falta asignar Identificaci�n...');document.form1.docunum.focus(); return false;}
	if(document.form1.docuexp.value == '')
	{alert('Falta asignar lugar de expedici�n del documento...');document.form1.docuexp.focus(); return false;}
    if(document.form1.rh.value == '')
	{alert('Falta asignar Rh del monitor...');document.form1.rh.focus(); return false;}
	if(document.form1.diaing.value == '' || document.form1.mesing.value == '' || document.form1.anoing.value == '')
	{alert("Favor asignar Fecha de Ingreso...");document.form1.diaing.focus(); return false;}
	if(document.form1.correo.value == '')
	{alert('Falta asignar correo...');document.form1.correo.focus(); return false;}
	if(document.form1.celular.value == '')
	{alert('Falta asignar celular...');document.form1.celular.focus(); return false;}
 
	if(document.form1.estado == 'RETIRADO')
	{

		if(document.form1.diaret.value == '' || document.form1.mesret.value == '' || document.form1.anoret.value == '')
		{alert("Favor asignar Fecha de Retiro...");document.form1.diaret.focus(); return false;}
	}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar este Monitor?");
	if(ok)
	{
		document.form1.op.value='borrar_monitor';
		document.form1.submit();
	}
}
function seleccionar()
{
	if (document.form0.estado[0].checked==true)
		var theURL = 'transporte_monit.php?estado=ACTIVO';
	else	
		var theURL = 'transporte_monit.php?estado=RETIRADO';
	window.location.href = theURL;
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/monitor.png' width="40" height="40" border="0" align="absmiddle"> Monitores Rutas</td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<?php
if ($opcion=="listar")
{
$chkact = "";
$chkret = "checked";
if ($estado != "RETIRADO")
{
	$chkact = "checked";
	$chkret = "";
}
$sql="SELECT id_monitor, apellidos, nombres, rh, docunum, celular, telefono, estado FROM monitores ";
if ($buscar != "")
	$sql.="WHERE (apellidos LIKE '$buscar%' OR nombres LIKE '$buscar%') ";
else 
	$sql.="WHERE estado = '$estado' ";
$sql.= "ORDER BY apellidos, nombres";
$dat=viewsql($sql);
?>
<table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
<form action="" method="post" name="form0">
	<tr><td colspan="3" height="5"></td></tr>
	<tr height="32">
		<td width="6" background="images/backsilver401.gif"></td>
		<td class="titulo_color" background="images/backsilver402.gif"><font color="#006633"><input type="radio" name="estado" value="ACTIVO" <?php echo $chkact;?> onClick="javascript:seleccionar();"> Activos &nbsp;&nbsp; <input type="radio" name="estado" value="RETIRADO" <?php echo $chkret;?> onClick="javascript:seleccionar();"> Retirados</font></td>
		<td align="right" background="images/backsilver402.gif">
			BUSCAR
			<input type="text" name="buscar" value="<?php echo $buscar;?>" size="15">
			&nbsp;
			<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
			&nbsp;
			<a href="transporte_monit.php?opcion=editar"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Nuevo Registro"></a>
			&nbsp;
		</td>
		<td width="6" background="images/backsilver403.gif"></td>
	</tr>
</form>
</table>
<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="95%" align="center">
	<tr height="20">
		<th>Apellidos y Nombres</th>
		<th width="40">ID</th>
		<th width="80">C�dula</th>
		<th width="100">Celular</th>
		<th width="100">Tel�fono</th>
		<th width="100">RH</th>
		<th width="40">Estado</th>
	</tr>
	<?php
	foreach($dat as $reg)
	{
	$id_monitor = $reg->id_monitor;
	?>		
	<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
		<td><?php echo $reg->apellidos." ".$reg->nombres;?></td>
		<td><a class="link_list" href="transporte_monit.php?opcion=editar&id_monitor=<?php echo $id_monitor;?>" title="ID Monitor"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"><?php echo $id_monitor;?></a></td>
		<td><?php echo $reg->docunum;?></td>
		<td><?php echo $reg->celular;?></td>
		<td><?php echo $reg->telefono;?></td>	
		<td><?php echo $reg->rh;?></td>
		<td><?php echo substr($reg->estado,0,3);?></td>
	</tr>
	<?php } ?>
</table>
<?php } else {
$titulo = "NUEVO MONITOR";
$datmonitor = datos_monitor($id_monitor);
if ($id_monitor > 0)
	$titulo = "ID N� ".$id_monitor;
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_transporte.php" method="post" name="form1" onSubmit="return validar();" enctype="multipart/form-data">
<input type='hidden' name='op' value='grabar_monitor'>
<input type='hidden' name='id_monitor' value='<?php echo $id_monitor;?>'>
<tr height="10">
		<td width="10"></td>
		<td></td>
		<td width="10"></td>
		<td></td>
		<td width="10"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Identificaci�n - ID <?php echo $id_monitor;?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td colspan="3" height="4"></td></tr>
				<tr>
											
					
					<td width="10"></td>
					<td>
						<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<tr>
								<td>Apellidos <font color="red">*</font></td>
								<td><input type="text" name="apellidos" size="25" onKeyUp="javascript:checkText(form1.apellidos);" value="<?php echo $datmonitor["apellidos"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;" autofocus></td>
							<tr>
							<tr>
								<td>Nombres <font color="red">*</font></td>				
								<td><input type="text" name="nombres" size="25" onKeyUp="javascript:checkText(form1.nombres);" value="<?php echo $datmonitor["nombres"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							<tr>
							<tr>
								<td>Identificaci�n <font color="red">*</font></td>
								<td>
									<select name="docutip" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<option value="" selected></option>
									<?php
									$sql="SELECT * FROM listas WHERE listip = 'IDENTIFICACION' order by id";
									$listide=viewsql($sql);
									foreach($listide as $reglis)
									{
										if ($datmonitor["docutip"] == $reglis->lisval)
											echo "<option value='$reglis->lisval' title='$reglis->lisnom' selected>$reglis->lisval</option>";
										else
											echo "<option value='$reglis->lisval' title='$reglis->lisnom'>$reglis->lisval</option>";
									}
									?>																
									</select>
									<input type="text" name="docunum"  style="width: 120px;" maxlength="15" value="<?php echo $datmonitor["docunum"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr>
								<td>Expedido en <font color="red">*</font></td>
								<td>
									<input type="text" name="docuexp" size="25" value="<?php echo $datmonitor["docuexp"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr>
								<td> RH: <font color="red">*</font></td>
								<td>
									<input type="text" name="rh" size="25" value="<?php echo $datmonitor["rh"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr>
								<td width="60">Estado</td>
								<td>
									<select name="estado" style="width: 100px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									$sql="SELECT * FROM listas WHERE listip = 'PROFESORES' order by id";
									$lista=viewsql($sql);
									foreach($lista as $reglis)
									{
										if ($datmonitor["estado"] == $reglis->lisnom)
											echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
										else
											echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							</table>
					</td>
				</tr>
			</table>
			</fieldset>
			<fieldset>
			<legend class="titulo_campos">Fechas</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr>
					<td>Ingreso</td>
					<td>
					<?php 
					select_dia($datmonitor["fchingreso"],'diaing');
					select_mes($datmonitor["fchingreso"],'mesing');
					select_ano($datmonitor["fchingreso"],'anoing');
					?>
					</td>
				</tr>
				<tr>
					<td>Retiro</td>
					<td>
					<?php 
					select_dia($datmonitor["fchretiro"],'diaret');
					select_mes($datmonitor["fchretiro"],'mesret');
					select_ano($datmonitor["fchretiro"],'anoret');
					?>
					</td>
				</tr>
			</table>
			</fieldset>
			
		</td>
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Domicilio y Ubicaci�n</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr height="2"><td ></td><td width="60"></td><td></td></tr>
				<tr>
					<td></td>
					<td>Direcci�n</td>
					<td><input type="text" name="direccion" size="40" value="<?php echo $datmonitor["direccion"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Barrio</td>
					<td>
						<input type="text" name="barrio" size="40" value="<?php echo $datmonitor["barrio"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>Tel�fonos</td>
					<td><input type="text" name="telefono" size="40" value="<?php echo $datmonitor["telefono"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Celulares  <font color="red">*</font></td>
					<td><input type="text" name="celular" size="40" value="<?php echo $datmonitor["celular"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Correo <font color="red">*</font></td>
					<td><input type="text" name="correo" id="correo" size="40" value="<?php echo $datmonitor["correo"];?>" onBlur="javascript:checkMail(form1.famicor);" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
			</table>
			</fieldset>
			
			</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr height="36" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
		<td></td>
		<td align="left">
			<?php if ($id_monitor > 0) { ?>
			ACTUALIZADO <?php echo substr($datmonitor["fchactual"],0,16);?> <?php echo $datmonitor["usuactual"];?>
			<?php } ?>
		</td>
		<td colspan="3" align="right">
			<?php if ($_SESSION["nivtra"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="5" alt="Grabar" align="absmiddle">
			<?php } if ($id_monitor > 0 and $_SESSION["nivtra"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="5" align="absmiddle"></a>
			<?php } ?>
			<a href="transporte_monit.php"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
