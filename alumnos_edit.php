<?php
/* * *********************************************************************************************
 * @file        : alumnos_edit.php                                                              *
 * @brief       : Edita y actualiza un registro de alumno                                       *
 * @version     : 1.0                                                                           *
 * @company     : IDIC                                                                          *
 * @author      : Nora Rodriguez 15-feb-2012                                                    *
 * @Modificado  : Nora Rodriguez 15-feb-2012                                                    *
 * ********************************************************************************************* */

session_start();
if ($_SESSION["usulog"] == "") {
    echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";
}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("libs/fechas.php");
include("libs/image_resize.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : $_SESSION["anoaca"];
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null;
$id_familiar = isset($_GET['id_familiar']) ? $_GET['id_familiar'] : null;
$opcion = "basicos";
connect();
include("includes/menu_titulo.php");
$datalumno = datos_alumno($id_alumno);
$datmatricula = ultima_matricula($id_alumno);
if ($id_familiar > 0)
    $datalumno["id_familiar"] = $id_familiar;
$datfamiliar = datos_familiar($datalumno["id_familiar"]);
//lista de grados
$sql = "SELECT * FROM grados order by id_grado";
$datgrad = viewsql($sql);
//lista de documentos
$sql = "SELECT * FROM listas WHERE listip = 'IDENTIFICACION' order by id";
$listide = viewsql($sql);
?>
<html>
    <head>
        <title>IDIC</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
        <link rel="stylesheet" type="text/css" href="estilos.css">
        <link rel="shortcut icon" type="image/icon" href="images/idic.ico">
        <script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
        <script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
        <script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
        <script language='JavaScript' type='text/JavaScript' src='js/validInput.js'></script>
        <script language="javascript" type='text/JavaScript'>
            function validar()
            {
            if(document.form1.apellidos.value == '')
            {alert('Falta asignar Apellidos...');document.form1.apellidos.focus(); return false;}
            if(document.form1.nombres.value == '')
            {alert('Falta asignar Nombres...');document.form1.nombres.focus(); return false;}
            if(document.form1.docutip.value == '')
            {alert('Falta asignar tipo de documento...');document.form1.docutip.focus(); return false;}
            if(document.form1.docunum.value <= 0)
            {alert('Falta asignar Identificaci�n...');document.form1.docunum.focus(); return false;}
            if(document.form1.docuexp.value == '')
            {alert('Falta asignar ciudad de expedici�n del documento...');document.form1.docuexp.focus(); return false;}
            if(document.form1.docudep.value == '')
            {alert('Falta asignar departamento de expedici�n del documento...');document.form1.docudep.focus(); return false;}
            if(document.form1.genero.value == '')
            {alert('Falta asignar Genero...');document.form1.genero.focus(); return false;}
            if(document.form1.tipoedu.value == '')
            {alert('Falta asignar Tipo de Educaci�n...');document.form1.tipoedu.focus(); return false;}
            if(document.form1.famitip.value == '')
            {alert('Falta seleccionar Familiar con quien vive...');document.form1.famitip.focus(); return false;}
            if(document.form1.famiape.value == '')
            {alert('Falta asignar apellidos del familiar...');document.form1.famiape.focus(); return false;}
            if(document.form1.faminom.value == '')
            {alert('Falta asignar nombre del familiar...');document.form1.faminom.focus(); return false;}
            if(document.form1.famidoc.value == '')
            {alert('Falta asignar tipo de documento del familiar...');document.form1.famidoc.focus(); return false;}
            if(document.form1.famiced.value == '')
            {alert('Falta asignar identificaci�n del familiar...');document.form1.famiced.focus(); return false;}
            if(document.form1.famiexp.value == '')
            {alert('Falta lugar expedici�n documento del familiar...');document.form1.famiexp.focus(); return false;}
            if(document.form1.famidir.value == '')
            {alert('Falta asignar direcci�n del familiar...');document.form1.famidir.focus(); return false;}
            if(document.form1.famibar.value == '')
            {alert('Falta asignar barrio del familiar...');document.form1.famibar.focus(); return false;}
            if(document.form1.famiest.value == '')
            {alert('Falta asignar estrato del familiar...');document.form1.famiest.focus(); return false;}
            if(document.form1.famitel.value == '')
            {alert('Falta asignar tel�fono del familiar...');document.form1.famitel.focus(); return false;}
            if(document.form1.ano.value == '')
            {alert('Falta asignar A�o de Matricula...');document.form1.ano.focus(); return false;}
            if(document.form1.grado.value == '')
            {alert('Falta asignar Grado de Matricula...');document.form1.grado.focus(); return false;}
            if(document.form1.dianac.value == ''  || document.form1.mesnac.value == '' || document.form1.anonac.value == '')
            {alert('Falta asignar fecha de nacimiento...');document.form1.dianac.focus(); return false;}
            if(document.form1.lugnace.value == '')
            {alert('Falta asignar ciudad de nacimiento...');document.form1.lugnace.focus(); return false;}
            if(document.form1.depnace.value == '')
            {alert('Falta asignar departamento de nacimiento...');document.form1.depnace.focus(); return false;}
            if(document.form1.estado.value == 'RETIRADO')
            {
            if(document.form1.diaret.value == '' || document.form1.mesret.value == '' || document.form1.anoret.value == '')
            {alert("Favor asignar Fecha de Retiro...");document.form1.diaret.focus(); return false;}
            }
            if(document.form1.estado.value == 'GRADUADO')
            {
            if(document.form1.diagra.value == '' || document.form1.mesgra.value == '' || document.form1.anogra.value == '')
            {alert("Favor asignar Fecha de Graduaci�n...");document.form1.diagra.focus(); return false;}
            }
            return true;
            }
            function borrarconf()
            {
            ok=confirm("Esta seguro de eliminar este Alumno ?");
            if(ok)
            {
            document.form1.op.value='borraralumno';
            document.form1.submit();
            }
            }
            function SeleccionarFamiliar()
            {
            var id_alumno = document.form1.id_alumno.value;
            var id_familiar = document.form1.id_familiar.value;
            var theURL = 'alumnos_edit.php?id_alumno='+id_alumno+'&id_familiar='+id_familiar;
            window.location.href = theURL;
            }
        </script>
    </head>
    <body>
        <?php include("includes/menu_alumnos.php"); ?>
        <table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
            <form action="database/grabar_alumnos.php" method="post" name="form1" onSubmit="return validar();" enctype="multipart/form-data">
                <input type='hidden' name='op' value='grabaralumno'>
                <input type='hidden' name='id_alumno' value='<?php echo $id_alumno; ?>'>
                <input type="hidden" name="oldfoto" value="<?php echo $datalumno["foto"]; ?>">
                <input type="hidden" name="id_matricula" value="<?php echo $datalumno["id_matricula"]; ?>">
                <tr height="10">
                    <td width="10"></td>
                    <td></td>
                    <td width="10"></td>
                    <td></td>
                    <td width="10"></td>
                </tr>
                <tr valign="top" align="left">
                    <td></td>
                    <td>
                        <fieldset>
                            <legend class="titulo_campos">Identificaci�n - ID <?php echo $id_alumno; ?></legend>
                            <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                <tr><td colspan="3" height="4"></td></tr>
                                <tr>
                                    <td width="160" height="160" bgcolor="#E0E7E9" align="center" valign="middle">
                                        <?php if ($datalumno["foto"] == "") { ?>
                                            <img src="images/foto.gif" width="80" height="80" border="0" align="absmiddle">
                                        <?php } else {
                                            $height = 150;
                                            $width = resize_width($datalumno["foto"], 150); ?>
                                            <img src="<?php echo $datalumno["foto"]; ?>" width="<?php echo $width; ?>" height="<?php echo $height; ?>" border="0" align="absmiddle">
                                        <?php } ?>
                                    </td>
                                    <td width="10"></td>
                                    <td>
                                        <table border="0" cellpadding="1" cellspacing="1" width="100%">
                                            <tr>
                                                <td width="80">Estado</td>
                                                <td>
                                                    <select name="estado" onKeyDown="if (event.keyCode == 13)
                                                                event.keyCode = 9;">
                                                        <?php
                                                        $sql = "SELECT * FROM listas WHERE listip = 'ALUMNOS' order by id";
                                                        $lista = viewsql($sql);
                                                        foreach ($lista as $reglis) {
                                                            if ($datalumno["estado"] == $reglis->lisnom)
                                                                echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
                                                            else
                                                                echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
                                                        }
                                                        ?>																
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Apellidos <font color="red">*</font></td>
                                                <td><input type="text" name="apellidos" size="27" onKeyUp="javascript:checkText(form1.apellidos);" value="<?php echo $datalumno["apellidos"]; ?>" style="text-transform:uppercase;" onKeyDown="if (event.keyCode == 13)
                                                            event.keyCode = 9;" autofocus></td>
                                            <tr>
                                            <tr>
                                                <td>Nombres <font color="red">*</font></td>				
                                                <td><input type="text" name="nombres" size="27" onKeyUp="javascript:checkText(form1.nombres);" value="<?php echo $datalumno["nombres"]; ?>" style="text-transform:uppercase;" onKeyDown="if (event.keyCode == 13)
                                                            event.keyCode = 9;"></td>
                                            <tr>
                                            <tr>
                                                <td>Identificaci�n <font color="red">*</font></td>
                                                <td>
                                                    <select name="docutip" onKeyDown="if (event.keyCode == 13)
                                                                event.keyCode = 9;">
                                                        <option value="" selected></option>
                                                        <?php
                                                        foreach ($listide as $reglis) {
                                                            if ($datalumno["docutip"] == $reglis->lisval)
                                                                echo "<option value='$reglis->lisval' title='$reglis->lisnom' selected>$reglis->lisval</option>";
                                                            else
                                                                echo "<option value='$reglis->lisval' title='$reglis->lisnom'>$reglis->lisval</option>";
                                                        }
                                                        ?>																
                                                    </select>
                                                    <input type="text" name="docunum" style="width: 130px;" maxlength="15" value="<?php echo $datalumno["docunum"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                                event.keyCode = 9;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Expedido en <font color="red">*</font></td>
                                                <td>
                                                    <input type="text" name="docuexp" size="27" value="<?php echo $datalumno["docuexp"]; ?>" style="text-transform:uppercase;" onKeyDown="if (event.keyCode == 13)
                                                                event.keyCode = 9;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Departamento <font color="red">*</font></td>
                                                <td>
                                                    <input type="text" name="docudep" size="27" value="<?php echo $datalumno["docudep"]; ?>" style="text-transform:uppercase;" onKeyDown="if (event.keyCode == 13)
                                                                event.keyCode = 9;">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>G�nero <font color="red">*</font></td>
                                                <td>
                                                    <select name="genero" onKeyDown="if (event.keyCode == 13)
                                                                event.keyCode = 9;">
                                                        <?php
                                                        $sql = "SELECT * FROM listas WHERE listip = 'GENERO' order by id";
                                                        $lista = viewsql($sql);
                                                        foreach ($lista as $reglis) {
                                                            if ($datalumno["genero"] == $reglis->lisval)
                                                                echo "<option value='$reglis->lisval' selected>$reglis->lisnom</option>";
                                                            else
                                                                echo "<option value='$reglis->lisval'>$reglis->lisnom</option>";
                                                        }
                                                        ?>																
                                                    </select>
                                                    C�digo
                                                    <input type="text" name="codigo"  style="width: 50px;" maxlength="6" value="<?php echo $datalumno["codigo"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                                event.keyCode = 9;">
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                            <legend class="titulo_campos">Datos Personales</legend>
                            <table border="0" cellpadding="1" cellspacing="1" width="100%">
                                <tr>
                                    <td>Educaci�n Tipo <font color="red">*</font>&nbsp;</td>
                                    <td>
                                        <select name="tipoedu" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                            <option value="" selected></option>
                                            <?php
                                            $sql = "SELECT * FROM listas WHERE listip = 'EDUCACION' order by id";
                                            $lista = viewsql($sql);
                                            foreach ($lista as $reglis) {
                                                if ($datalumno["tipoedu"] == $reglis->lisval)
                                                    echo "<option value='$reglis->lisval' selected>$reglis->lisnom</option>";
                                                else
                                                    echo "<option value='$reglis->lisval'>$reglis->lisnom</option>";
                                            }
                                            ?>																
                                        </select>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Entidad de Salud &nbsp;</td>
                                    <td>
                                        <select name="id_saludenti" style="width: 320px;" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                            <option value="" selected></option>
                                            <?php
                                            $sql = "select id_saludenti, entidad from saludenti order by entidad";
                                            $datsalu = viewsql($sql);
                                            foreach ($datsalu as $regsalu) {
                                                if ($datalumno["id_saludenti"] == $regsalu->id_saludenti)
                                                    echo "<option value='$regsalu->id_saludenti' selected>$regsalu->entidad</option>";
                                                else
                                                    echo "<option value='$regsalu->id_saludenti'>$regsalu->entidad</option>";
                                            }
                                            ?>																
                                        </select>
                                        <input type="hidden" name="entidad">
                                        <a class="link_white" href="setup_saludenti.php?id_alumno=<?php echo $id_alumno; ?>" onclick="newWindow(this.href, '', 700, 500, 1, 1, 0, 0, 0, 1, 0);
                                                return false;" target="_blank" title="Mi perfil..."><img src="images/salud.gif" border="0" width="20" height="20" align="absmiddle" title="Lista Entidades de Salud"></a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Correo Alumno</td>
                                    <td>
                                        <input type="text" name="correo" id="correo" size="35" value="<?php echo $datalumno["correo"]; ?>" onBlur="javascript:checkMail(form1.correo);" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                        Celular
                                        <input type="text" name="celular" size="8" value="<?php echo $datalumno["celular"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">						
                                    </td>
                                </tr>
                                <tr>
                                    <td>Anterior Instituci�n</td>
                                    <td>
                                        <input type="text" name="proceinsti" style="width: 270px;" value="<?php echo $datalumno["proceinsti"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                        Grado
                                        <select name="procegrado" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                            <option value="" selected></option>
<?php
foreach ($datgrad as $reggrad) {
    if ($datalumno["procegrado"] == $reggrad->grado)
        echo "<option value='$reggrad->grado' title='$reggrad->gradonom' selected>$reggrad->grado</option>";
    else
        echo "<option value='$reggrad->grado' title='$reggrad->gradonom'>$reggrad->grado</option>";
}
?>																
                                        </select>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                            <legend class="titulo_campos">Observaciones Generales</legend>
                            <table border="0" cellpadding="1" cellspacing="1" width="100%">
                                <tr>
                                    <td>
                                        <textarea name='detalles' style="width: 450px; height: 50px;" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;"><?php echo $datalumno["detalles"]; ?></textarea>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                            <legend class="titulo_campos">Cargar Foto JPG/GIF/PNG</legend>
                            <table border="0" cellpadding="1" cellspacing="1" width="100%">
                                <tr height="20">
                                    <td>
<?php if ($datalumno["foto"] != "") { ?>
                                            <a href="database/grabar_alumnos.php?op=borrarfoto&id_alumno=<?php echo $id_alumno; ?>&foto=<?php echo $datalumno["foto"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                        event.keyCode = 9;"><img src="images/b_drop.png" width="16" height="16" border="0" align="absmiddle" title="Borrar foto actual"></a>
<?php } ?>
                                    </td>
                                    <td>
                                        <input type="file" name="nwfoto" style="width: 250px;">
                                        <img src="images/ico_info.png" border="0" width="16" height="16" align="absmiddle" title="Tama�o 150 X 150 pixels. Si es superior el sistema la reducir� y puede deteriorarse la calidad.">
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td></td>
                    <td>
                        <fieldset>
                            <legend class="titulo_campos">Domicilio y Familiar Responsable</legend>
                            <table border="0" cellpadding="1" cellspacing="1" width="100%">
                                <tr height="2"><td ></td><td width="80"></td><td></td></tr>
                                <tr>
                                    <td></td>
                                    <td>Vive con <font color="red">*</font></td>
                                    <td>
                                            <?php if ($datalumno["id_familiar"] <= 0) { ?>
                                            <input type="hidden" name="id_familiar" value="0">
                                            <select name="famitip" style="width: 100px;" onKeyDown="if (event.keyCode == 13)
                                                        event.keyCode = 9;">
                                                <option value="" selected></option>
                                                <?php
                                                $sql = "SELECT * FROM listas WHERE listip = 'FAMILIARES' order by id";
                                                $lista = viewsql($sql);
                                                foreach ($lista as $reglis) {
                                                    echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
                                                }
                                                ?>																
                                            </select>
<?php } else { ?>
                                            <input type="hidden" name="old_familiar" value="<?php echo $id_familiar;?>">
                                            <select name="id_familiar" style="width: 100px;" onChange="javascript:SeleccionarFamiliar();" onKeyDown="if (event.keyCode == 13)
                                                        event.keyCode = 9;">
    <?php
    $sql = "SELECT id_familiar, tipo FROM familiares WHERE id_alumno = '$id_alumno'";
    $datfam = viewsql($sql);
    foreach ($datfam as $regfam) {
        $sel = "";
        if (intval($datalumno["id_familiar"]) == intval($regfam->id_familiar))
            $sel = "selected";
        echo "<option value='$regfam->id_familiar' $sel>$regfam->tipo</option>";
    }
    ?>																
                                            </select>
                                            <input type="hidden" name="famitip" value="<?php echo $datfamiliar["tipo"]; ?>">
<?php } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Apellidos <font color="red">*</font></td>
                                    <td><input type="text" name="famiape" size="25" onKeyUp="javascript:checkText(form1.famiape);" value="<?php echo $datfamiliar["apellidos"]; ?>" style="text-transform:uppercase;" onKeyDown="if (event.keyCode == 13)
                                                event.keyCode = 9;"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Nombres <font color="red">*</font></td>
                                    <td><input type="text" name="faminom" size="25" onKeyUp="javascript:checkText(form1.faminom);" value="<?php echo $datfamiliar["nombres"]; ?>" style="text-transform:uppercase;" onKeyDown="if (event.keyCode == 13)
                                                event.keyCode = 9;"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Identificaci�n <font color="red">*</font></td>
                                    <td>
                                        <select name="famidoc" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                            <option value="" selected></option>
<?php
foreach ($listide as $reglis) {
    if ($reglis->lisval != "NUIP" and $reglis->lisval != "TI") {
        if ($datfamiliar["docutip"] == $reglis->lisval)
            echo "<option value='$reglis->lisval' title='$reglis->lisnom' selected>$reglis->lisval</option>";
        else
            echo "<option value='$reglis->lisval' title='$reglis->lisnom'>$reglis->lisval</option>";
    }
}
?>																
                                        </select>
                                        <input type="text" name="famiced"  style="width: 70px;" maxlength="15" value="<?php echo $datfamiliar["docunum"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;" title="N�mero">
                                        <input type="text" name="famiexp" style="width: 107px; text-transform:uppercase;" value="<?php echo $datfamiliar["docuexp"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;" title="Ciudad expedici�n">
                                        <input type="text" name="famipai" style="width: 65px; text-transform:uppercase;" value="<?php echo $datfamiliar["pais"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;" title="Pa�s de origen">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Direcci�n <font color="red">*</font></td>
                                    <td><input type="text" name="famidir" style="width: 300px; text-transform:uppercase;" value="<?php echo $datfamiliar["direccion"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                event.keyCode = 9;"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Barrio <font color="red">*</font></td>
                                    <td>
                                        <input type="text" name="famibar" style="width: 242px; text-transform:uppercase;" value="<?php echo $datfamiliar["barrio"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                        Estrato
                                        <input type="text" name="famiest" style="width: 16px;" value="<?php echo $datfamiliar["estrato"]; ?>" maxlength="1" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                    </td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Correo</td>
                                    <td><input type="text" name="famicor" id="famicor" style="width: 300px;" value="<?php echo $datfamiliar["correo"]; ?>" onBlur="javascript:checkMail(form1.famicor);" onKeyDown="if (event.keyCode == 13)
                                                event.keyCode = 9;"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Profesi�n</td>
                                    <td><input type="text" name="famipro" style="width: 300px; text-transform:uppercase;" value="<?php echo $datfamiliar["profesion"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                event.keyCode = 9;"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td>Tel�fono <font color="red">*</font></td>
                                    <td>
                                        <input type="text" name="famitel" size="8" value="<?php echo $datfamiliar["telefono"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                        Oficina <input type="text" name="famiofi" size="8" value="<?php echo $datfamiliar["telefofi"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                        Celular <input type="text" name="famicel" size="8" value="<?php echo $datfamiliar["celular"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">						
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                            <legend class="titulo_campos">Matr�cula <?php echo $datmatricula["estado"]; ?></legend>
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr><td colspan="2" height="2"></td></tr>
                                <tr height="25">
                                    <td>A�o Lectivo <font color="red">*</font>&nbsp;</td>
                                    <td>
                                        <select name="ano" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                            <?php
                                            $sql = "SELECT ano FROM anoslect ORDER BY ano desc";
                                            $datanol = viewsql($sql);
                                            foreach ($datanol as $reganol) {
                                                $selano = "";
                                                if ($reganol->ano == $datmatricula["ano"])
                                                    $selano = "selected";
                                                echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
                                            }
                                            ?>
                                        </select>
                                        &nbsp;
                                        Grado <font color="red">*</font>
                                        <select name="grado" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                            <option value="" selected></option>
<?php
foreach ($datgrad as $reggrad) {
    if ($datmatricula["grado"] == $reggrad->grado)
        echo "<option value='$reggrad->nivel$reggrad->grado' title='$reggrad->gradonom' selected>$reggrad->grado</option>";
    else
        echo "<option value='$reggrad->nivel$reggrad->grado' title='$reggrad->gradonom'>$reggrad->grado</option>";
}
?>																
                                        </select>
                                        <?php if ($datalumno["id_matricula"] > 0 and $datmatricula["ano"] != $nextano) { ?>
                                            &nbsp;
                                            CURSO
                                            <select name="curso" onKeyDown="if (event.keyCode == 13)
                                                        event.keyCode = 9;">
                                                <option value="" selected></option>
    <?php
    $sql = "SELECT * FROM cursos where ano = '" . $datmatricula["ano"] . "' and grado = '" . $datmatricula["grado"] . "' order by curso";
    $datcurs = viewsql($sql);
    foreach ($datcurs as $regcurs) {
        if ($datmatricula["curso"] == $regcurs->curso)
            echo "<option value='$regcurs->curso' selected>$regcurs->curso</option>";
        else
            echo "<option value='$regcurs->curso'>$regcurs->curso</option>";
    }
    ?>																
                                            </select>
<?php } else { ?>
                                            <input type="hidden" name="curso" value="<?php echo $datmatricula["curso"]; ?>">
<?php } ?>
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                        <fieldset>
                            <legend class="titulo_campos">Fechas</legend>
                            <table border="0" width="100%">
                                <tr>
                                    <td width="80">Ingreso</td>
                                    <td>
                                        <?php
                                        select_dia($datalumno["fchingreso"], 'diaing');
                                        select_mes($datalumno["fchingreso"], 'mesing');
                                        select_ano($datalumno["fchingreso"], 'anoing');
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Retiro</td>
                                    <td>
<?php
select_dia($datalumno["fchretiro"], 'diaret');
select_mes($datalumno["fchretiro"], 'mesret');
select_ano($datalumno["fchretiro"], 'anoret');
?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Graduaci�n</td>
                                    <td>
<?php
select_dia($datalumno["fchgrado"], 'diagra');
select_mes($datalumno["fchgrado"], 'mesgra');
select_ano($datalumno["fchgrado"], 'anogra');
?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Nacimiento <font color="red">*</font></td>
                                    <td>
<?php
select_dia($datalumno["fchnace"], 'dianac');
select_mes($datalumno["fchnace"], 'mesnac');
select_ano($datalumno["fchnace"], 'anonac');
?>
                                        &nbsp;
                                        Edad
                                        <input type="text" name="edad" style="width: 20px;" value="<?php echo $datalumno["edad"]; ?>" readonly onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Ciudad <font color="red">*</font></td>
                                    <td>
                                        <input type="text" name="lugnace" style="width: 155px; text-transform:uppercase;" value="<?php echo $datalumno["lugnace"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Departamento <font color="red">*</font></td>
                                    <td>
                                        <input type="text" name="depnace" style="width: 155px; text-transform:uppercase;" value="<?php echo $datalumno["depnace"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                    </td>
                                </tr>
                                <tr>
                                    <td>Pais Origen <font color="red">*</font></td>
                                    <td>
                                        <input type="text" name="paisnace" style="width: 155px; text-transform:uppercase;" value="<?php echo $datalumno["paisnace"]; ?>" onKeyDown="if (event.keyCode == 13)
                                                    event.keyCode = 9;">
                                    </td>
                                </tr>
                            </table>
                        </fieldset>
                    </td>
                    <td></td>
                </tr>
                <tr><td colspan="5" height="20"></td></tr>
                <tr height="36" bgcolor="#CCCCCC" background="images/backsilver.gif">
                    <td></td>
                    <td>
<?php if ($id_alumno > 0) { ?>
                            ACTUALIZADO <?php echo substr($datalumno["fchactual"], 0, 16); ?> <?php echo $datalumno["usuactual"]; ?>
<?php } ?>
                    </td>
                    <td colspan="3" align="right" valign="middle">
<?php if ($_SESSION["nivalu"] >= 2) { ?>
                            <input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar" align="absmiddle">
<?php } if ($id_alumno > 0 and $_SESSION["nivalu"] >= 4) { ?>
                            <a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10" align="absmiddle"></a>
<?php } ?>
                        <a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
                    </td>
                </tr>
            </form>
        </table>
    </body>
</html>
<?php disconnect(); ?>
