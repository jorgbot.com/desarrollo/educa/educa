<?php
/******************************************************/
/* Envio de correos por intranet                      */
/******************************************************/
session_start();
if($_SESSION["instinom"]=="")
	$instinom = "instituto de Integración Cultural - I.D.I.C";
else
	$instinom = $_SESSION["instinom"];
$op = isset($_GET['op']) ? $_GET['op'] : null ;
$correo = isset($_GET['correo']) ? $_GET['correo'] : null ;
$origen = isset($_GET['origen']) ? $_GET['origen'] : null ;
$numero = isset($_GET['numero']) ? $_GET['numero'] : null ;
$contenido = isset($_GET['contenido']) ? $_GET['contenido'] : null ;
include ("database/database.php");
include ("libs/fechas.php");
if ($op == "")
	$op="seleccionar";
connect();
//enviar mensaje al contacto
if ($op=="enviar" and $numero > 0 and $contenido!="")
{
	$op="seleccionar";
	$subject=$asunto;
	$message=$contenido;
	$email=$correo;
	$numero=intval($numero);
	for($i=1;$i<=$numero ;$i++)
	{
		$long=strpos($correo,";");
		$email=substr($correo,0,$long);
		$email=trim($email);
		if ($email!="")
		{include("includes/mail_intranet.php");} // ENVIAR MAIL INFORMANDO AL USUARIO
		$email=$email.";"; //suprime ese correo
		$correo=eregi_replace($email,'',$correo);
	}
	echo"<script>alert('Mensaje enviado a lista seleccionada.');window.close();</script>";
}
if ($op=="enviar" and $contenido!="")
{
	$email=$correo;
	$subject=$asunto;
	$message=$contenido;
	include("includes/mail_intranet.php"); // ENVIAR MAIL INFORMANDO AL USUARIO
	$op="seleccionar";
	echo"<script>alert('Mensaje enviado a $email');window.close();</script>";
}
if ($op=="multiple")
{
	$correo="";
	$sql="select id_usuario, correo from usuarios order by id_usuario";
	$resultado=viewsql($sql);
	foreach($resultado as $registros)
	{
		$id=$registros->id_usuario;
		$co=$registros->correo;
		if($arreglo[$id])  // verificar si definido
		{
			$numero=$numero+1; $correo.=$co."; ";
		}
	}
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="estilos.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language="javascript">
<!--
function validar()
{
	if(document.form1.asunto.value=="")
	{alert("Debe escribir el asunto del mensaje");document.form1.asunto.focus(); return false;}
	return true;
}
-->
</script>
<!-- TinyMCE -->
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript"> 
	tinyMCE.init({
		mode : "textareas",
		theme : "simple"
		// advanced
	});
</script>
<!-- /TinyMCE -->
</script>
</head>
<body onLoad="document.form1.campofoc.focus();">
<table width='95%' height='100%' cellspacing='0' cellpadding='0' border='0' align="center">
	<tr height="50">
		<td align="left" class="titulo">
			<img src="images/mail.png" width="16" height="16" border="0" align="absmiddle">
			CORREO INSTITUCIONAL
		</td>
		<td class="subtitulo" align="right"><?php echo $instinom;?></td>
	</tr>
	<tr valign='top'>
		<td colspan="2" valign="top">
			<?php
			if ($op=="seleccionar")
			{
				?>
				<table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#B7C4DB">
				<form action="" method="post" name="form1">
				<input type='hidden' name='op' value='multiple'>
				<tr>
					<th>Nombre</th>
					<th>Cargo</th>
					<th>Correo</th>
					<th></th>
				</tr>
				<?php
				//traer datos
				$sql="select usuarios.id_usuario, usuarios.usuario, usuarios.correo, sedes.sede from usuarios, sedes where usuarios.id_sede = sedes.id_sede and usuarios.inactivar = '0' and usuarios.correo !='' order by usuarios.usuario";
				$resultado=viewsql($sql);
				
				foreach($resultado as $registros)
				{
					$id=$registros->id_usuario;
					?>
					<tr height="18" bgcolor="#F6F6F6" ONMOUSEOVER="this.bgColor='#FFFFE6'" ONMOUSEOUT="this.bgColor='#F6F6F6'" align="left">
					    <td>&nbsp;<a href='srv_correo.php?op=capturar&correo=<?php echo $registros->correo;?>'><?php echo $registros->usuario;?></a></td>
						<td><?php echo $registros->sede;?></td>
						<td><a href="srv_correo.php?op=editar&correo=<?php echo $registros->correo;?>"><?php echo $registros->correo;?></a></td>
						<td><input type='checkbox' name='arreglo[<?php echo $id;?>]'></td>
					</tr>
					<?php
				} ?>
				<tr><th colspan="4" height="30" align="right"><input type="submit" id="campofoc" value="Enviar selección multiple..." ></th></tr>
				</form>
				</table>
				<br>
				<?php
			}
			else
			{
				$fecha=date('Y-m-d');
				$tfecha=fecha_completa($fecha);
				$headers=$_SESSION["usucor"];
				if ($headers == "")
					$headers = "localhost@idic.com";
				?>
				<table width='100%' cellspacing='0' cellpadding='2' class='tabla_marco'>
				<form action='' method='post' name='form1' onSubmit="return validar();">
				<input type='hidden' name='op' value='enviar'>
				<input type='hidden' name='numero' value='<?php echo $numero;?>'>
				<input type='hidden' name='correo' value='<?php echo $correo;?>'>
					<tr>
						<th colspan="3"><?php echo $tfecha;?></th>
					</tr>
					<tr height="10"><td colspan="3"></td></tr>
					<tr height='25' class='txt_entrada' valign="top">
						<td width="10"></td>
						<td>Destinatario</td>
						<td>
							<table cellpadding="0" cellspacing="0" class="campo_read" style="background-color: #FFFFFF; font-size:11px; color: #000000; width: 556px;">
								<tr><td height="18"><?php echo $correo;?></td></tr>
							</table>
						</td>
					</tr>
					<tr class='txt_entrada'>
						<td></td>
						<td>Asunto</td>
						<td><input type="text" name="asunto" id="campofoc" style="width: 553px;"></td>
					</tr>
					<tr class='txt_entrada'>
						<td colspan="3" align="center">
							<textarea name="contenido" id="contenido" rows="15" cols="120" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></textarea>
						</td>
					</tr>
					<tr class='txt_entrada'>
						<td></td>
						<td colspan="2" class="titulo_campos"><?php if (isset($_SESSION["usucor"])) { ?>Remitente <?php echo $_SESSION["usucor"];?><?php } ?></td>
					</tr>
					<tr height="10"><td colspan="3"></td></tr>
					<tr height='32'>
						<th colspan="3" align='center' background="images/backsilver.gif">
							<?php if ($origen!="") { ?>
							<a href='srv_correo.php?op=seleccionar'><img src='images/btn_regresar.png' border='0' hspace='30' alt='Anterior' width="70" height="24"></a>
							<?php } if ($correo!="") { ?>
							<input type='image' class="botones_img" border='0' hspace='30' name='continuar' src='images/btn_continuar.png' width="70" height="24">
							<?php } ?>
						</th>
					</tr>	
				</form>
				</table>
				<?php
			}
			?>
		</td>
	</tr>
</table>
</body>
</html>
<?php disconnect(); ?>
