<?php
/***********************************************************************************************
* @file        : alumnos_transporte.php                                                        *
* @brief       : Lista y edita afiliaciones transporte de un alumno                            *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 17-Jul-2015                                                    *
* @Modificado  : Nora Rodriguez 17-Jul-2015                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("database/datos_transporte.php");
include("libs/fechas.php");
$id_transporte = isset($_GET['id_transporte']) ? $_GET['id_transporte'] : 0 ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : 0 ;
$id_ruta = isset($_GET['id_ruta']) ? $_GET['id_ruta'] : 0 ;
$id_familiar = isset($_GET['id_familiar']) ? $_GET['id_familiar'] : 0 ;
$id_rutaop = isset($_GET['id_rutaop']) ? $_GET['id_rutaop'] : 0 ;
$id_familiarop = isset($_GET['id_familiarop']) ? $_GET['id_familiarop'] : 0 ;
$ano = isset($_GET['ano']) ? $_GET['ano'] : $_SESSION["anoaca"] ;
$op = isset($_GET['op']) ? $_GET['op'] : "listar" ;
$opcion = "transporte";
connect();
include("includes/menu_titulo.php");
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/validInput.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.fchingreso.value == '')
	{alert('Falta fecha de ringreso...');document.form1.fchingreso.focus(); return false;}
	if(document.form1.estado.value == 'RETIRADO' && document.form1.fchretiro.value == '')
	{alert('Falta fecha de retiro...');document.form1.fchretiro.focus(); return false;}
	if(document.form1.id_ruta.value <= 0)
	{alert('Falta asignar Ruta Principal...');document.form1.id_ruta.focus(); return false;}
	if(document.form1.id_familiar.value == document.form1.id_familiarop.value)
	{alert('Acudiente de la tarde debe ser diferente...'); document.form1.id_familiarop.value=''; document.form1.id_familiarop.focus(); return false;}
	var gradovaltra = document.form1.gradovaltra.value;
	for(var i=2;i<=11;i++){
		if (i < 10)
			var mes = '0'+i;
		else
			var mes = i;
		var coment = eval ('document.form1.coment'+mes+'.value');
		var valor = eval ('document.form1.transp'+mes+'.value');
		if (valor != gradovaltra && coment=='')
		{
			alert('Debe justificar el ajuste en transporte del mes '+mes);
			eval ("document.form1.coment"+mes+".focus()");
			return false;		
		}
	}
	return true;
}
function seleccionar()
{
	var ano = document.form1.ano.value;
	var id_alumno = document.form1.id_alumno.value;
	var theURL = 'alumnos_transporte.php?op=editar&ano='+ano+'&id_alumno='+id_alumno;
	window.location.href = theURL;
}
function familiar()
{
	var ano = document.form1.ano.value;
	var id_alumno = document.form1.id_alumno.value;
	var id_transporte = document.form1.id_transporte.value;
	var id_ruta = document.form1.id_ruta.value;
	var id_familiar = document.form1.id_familiar.value;
	var id_rutaop = document.form1.id_rutaop.value;
	var id_familiarop = document.form1.id_familiarop.value;
	var theURL = 'alumnos_transporte.php?op=editar&ano='+ano+'&id_alumno='+id_alumno+'&id_transporte='+id_transporte+'&id_ruta='+id_ruta+'&id_familiar='+id_familiar+'&id_rutaop='+id_rutaop+'&id_familiarop='+id_familiarop;
	window.location.href = theURL;
}
function borrarconf()
{
	var ano = document.form1.ano.value;
	ok=confirm('Esta seguro de eliminar Transporte '+ano+' ?');
	if(ok)
	{
		document.form1.op.value='borrar_transporte';
		document.form1.submit();
	}
}
</script>
</head>
<body>
<?php include("includes/menu_alumnos.php");?>
<?php if ($op == "listar") { ?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="420" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="45">A�o</th>
					<th width="35">Grado</th>
					<th width="35">Curso</th>
					<th width="60">Estado</th>
					<th width="35">C�digo</th>
					<th width="70">Ingreso</th>
					<th width="70">Retiro</th>
					<th>Ruta</th>
					<th>Recorrido</th>
				</tr>
				<?php
				$sql="SELECT * FROM transporte WHERE id_alumno = '$id_alumno' order by ano desc";
				$dattra=viewsql($sql);
				foreach($dattra as $regtra)
				{
				$id_transporte = $regtra->id_transporte;
				$id_ruta = $regtra->id_ruta;
				$ruta = "";
				$recorrido = "";
				$sql = "select ruta, recorrido from rutas where id_ruta = '$id_ruta' limit 1";
				$datrut = viewsql($sql);
				foreach($datrut as $regrut)
				{
					$ruta = $regrut->ruta;
					$recorrido = $regrut->recorrido;
				}
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center"><a class="link_list" href="alumnos_transporte.php?op=editar&id_transporte=<?php echo $id_transporte;?>&id_alumno=<?php echo $id_alumno;?>" title="ID transporte"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"><?php echo $regtra->ano;?></a></td>
					<td align="center"><?php echo $regtra->grado;?></td>
					<td align="center"><?php echo $regtra->curso;?></td>
					<td><?php echo $regtra->estado;?></td>
					<td><?php echo $regtra->codigo;?></td>
					<td><?php echo fecha_texto($regtra->fchingreso);?></td>
					<td><?php echo fecha_texto($regtra->fchretiro);?></td>
					<td><?php echo $ruta;?></td>
					<td><?php echo $recorrido;?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivmat"] >= 3 or $_SESSION["nivtra"] >= 3) { ?>
			<a href="alumnos_transporte.php?op=editar&id_alumno=<?php echo $id_alumno;?>"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</table>
<?php } if ($op == "editar") {
if ($id_transporte > 0)
	$titulo = "Transporte ID ".$id_transporte;
else
	$titulo = "Nuevo Transporte";
$dattransp = datos_transporte($id_transporte);
if ($id_ruta <= 0)
	$id_ruta = $dattransp["id_ruta"];
else
	$dattransp["id_ruta"] = $id_ruta; 
if ($id_familiar <= 0)
	$id_familiar = $dattransp["id_familiar"];
else
	$dattransp["id_familiar"] = $id_familiar; 
if ($id_rutaop <= 0)
	$id_rutaop = $dattransp["id_rutaop"];
else
	$dattransp["id_rutaop"] = $id_rutaop; 
if ($id_familiarop <= 0)
	$id_familiarop = $dattransp["id_familiarop"]; 
else
	$dattransp["id_familiarop"] = $id_familiarop; 
if ($id_transporte <= 0)
{
    $sql = "SELECT id_respmatri FROM matriculas WHERE id_alumno = '$id_alumno' and ano = '$ano' limit 1";
    $datfam = viewsql($sql);
    foreach ($datfam as $regfam) {
        $id_familiar = $regfam->id_respmatri;
        $dattransp["id_familiar"] = $id_familiar; 
    }
}
$datfamiru = basicos_familiar($id_familiar);
$datfamiop = basicos_familiar($id_familiarop);
if ($id_transporte > 0)
	$ano = $dattransp["ano"];
else
{
	$sql="SELECT id_matricula, grado, curso, fecha, id_respmatri FROM matriculas WHERE id_alumno = '$id_alumno' and ano = '$ano' LIMIT 1";
	$datmat=viewsql($sql);
	foreach($datmat as $regmat)
	{
		$grado = $regmat->grado;
		$curso = $regmat->curso;
		$dattransp["grado"] = $grado;
		$dattransp["curso"] = $curso;
		$dattransp["fchingreso"] = fec_dma($regmat->fecha);
	}
}
$gradovaltra = 0;
$sql="SELECT transporte FROM gradosval WHERE ano = '$ano' AND grado = '".$dattransp["grado"]."' LIMIT 1";
$datval=viewsql($sql);
foreach($datval as $regval)
{$gradovaltra = $regval->transporte;}
for($i=2; $i<=11 ;$i++)
{
	if ($i <= 9)
	{
		$nomval = "transp0".$i;
		$nomtxt = "coment0".$i;
	}
	else
	{
		$nomval = "transp".$i;
		$nomtxt = "coment".$i;
	}
	if ($dattransp[$nomval] < $gradovaltra and trim($dattransp[$nomtxt])=="")
		$dattransp[$nomval] = $gradovaltra;
}
$check0 = "";
$check1 = "";
$check2 = "";
if ($dattransp["jornada"] <= 0)
	$check0 = "checked";
if ($dattransp["jornada"] == 1)
	$check1 = "checked";
if ($dattransp["jornada"] == 2)
	$check2 = "checked";
$readonly = "readonly";
if ($_SESSION["nivmat"] >= 3 or $_SESSION["nivtra"] >= 3)
	$readonly = "";
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_transporte.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabar_transporte'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type='hidden' name='id_transporte' value='<?php echo $id_transporte;?>'>
<input type='hidden' name='gradovaltra' value='<?php echo $gradovaltra;?>'>
	<tr height="15">
		<td width="20"></td>
		<td></td>
		<td width="10"></td>
		<td></td>
		<td width="20"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos"><img src="images/rutas32.png" width="32" height="32" border="0" align="absmiddle"><?php echo $titulo;?></legend>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr valign="top">
				<td>
					<table border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td width="85">A�o Matricula</td>
							<td width="60">
								<?php if ($id_transporte <= 0) { ?>
								<select name="ano" style="width: 90px;" onChange="javascript:seleccionar();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php
								$sql="SELECT id_matricula, ano, grado, curso FROM matriculas WHERE id_alumno = '$id_alumno' ORDER BY ano desc";
								$datanol=viewsql($sql);
								foreach($datanol as $reganol)
								{
									$selano = "";
									if ($reganol->ano==$ano)
										$selano = "selected";
									echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
								}
								?>
								</select>
								<?php } else { ?>
								<input type="text" name="ano" style="width: 90px;" value="<?php echo $dattransp["ano"];?>" readonly="readonly" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php } ?>
							</td>
						</tr>				
						<tr>
							<td>Grado y Curso </td>
							<td>
								<input type="text" name="grado" style="width: 40px;" value="<?php echo $dattransp["grado"];?>" readonly="readonly" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<input type="text" name="curso" style="width: 47px;" value="<?php echo $dattransp["curso"];?>" readonly="readonly" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
							</td>
						</tr>
						<tr>
							<td>Estado</td>
							<td>
								<select name="estado" style="width: 90px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php
								$sql="SELECT * FROM listas WHERE listip = 'PROFESORES' order by id";
								$lista=viewsql($sql);
								foreach($lista as $reglis)
								{
									if ($dattransp["estado"] == $reglis->lisnom)
										echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
									else
										echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
								}
								?>																
								</select>
							</td>
						</tr>
					</table>
				</td>
				<td width="20"></td>
				<td>
					<table border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td>Ingreso</td>
							<td><input type="text" name="fchingreso" id="fchingreso" value="<?php echo $dattransp["fchingreso"];?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchingreso, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
						</tr>
						<tr>
							<td>Retiro &nbsp; </td>
							<td><input type="text" name="fchretiro" id="fchretiro" value="<?php echo $dattransp["fchretiro"];?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchretiro, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
						</tr>
						<tr>
							<td>C�digo &nbsp; </td>
							<td><input type="text" name="codigo" style="width: 30px;" value="<?php echo $dattransp["codigo"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
						</tr>
					</table>
				</td>
			</tr>
			</table>
			</fieldset>
			<br>
			<fieldset>
			<legend class="titulo_campos">Recoger y Dejar <input type="radio" name="jornada" value="0" <?php echo $check0;?>>&nbsp; Solo Recoger <input type="radio" name="jornada" value="1" <?php echo $check1;?>>&nbsp; Solo Dejar <input type="radio" name="jornada" value="2" <?php echo $check2;?>></legend>
			<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Acudiente</td>
					<td>
						<select name="id_familiar" style="width: 330px;" onChange="javascript:familiar();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						$sql="select id_familiar, tipo, apellidos, nombres, direccion, barrio from familiares where id_alumno = '$id_alumno' order by apellidos, nombres";
						$datfam=viewsql($sql);
						foreach($datfam as $regfam)
						{
							if (intval($dattransp["id_familiar"]) == intval($regfam->id_familiar))
								echo "<option value='$regfam->id_familiar' selected title='$regfam->direccion $regfam->barrio'>$regfam->tipo - $regfam->apellidos $regfam->nombres</option>";
							else
								echo "<option value='$regfam->id_familiar' title='$regfam->direccion $regfam->barrio'>$regfam->tipo - $regfam->apellidos $regfam->nombres</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr>
					<td>Direcci�n</td>
					<td><input type="text" name="direccion" style="width: 330px; text-transform:uppercase;" value="<?php echo $datfamiru["direccion"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td>Barrio</td>
					<td><input type="text" name="barrio" style="width: 330px; text-transform:uppercase;" value="<?php echo $datfamiru["barrio"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td>Tel�fono</td>
					<td>
					<input type="text" name="telefono" style="width: 80px;" value="<?php echo $datfamiru["telefono"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					Celular
					<input type="text" name="celular" style="width: 80px;" value="<?php echo $datfamiru["celular"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					Oficina
					<input type="text" name="telefofi" style="width: 90px;" value="<?php echo $datfamiru["telefofi"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr>
					<td>Ruta </td>
					<td>
						<select name="id_ruta" style="width: 330px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						$sql="SELECT id_ruta, ruta, recorrido FROM rutas WHERE ano = '$ano' ORDER BY ruta";
						$datrut=viewsql($sql);
						foreach($datrut as $regrut)
						{
							if (intval($dattransp["id_ruta"]) == intval($regrut->id_ruta))
								echo "<option value='$regrut->id_ruta' selected>$regrut->ruta - $regrut->recorrido</option>";
							else
								echo "<option value='$regrut->id_ruta'>$regrut->ruta - $regrut->recorrido</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr>
					<td>Paradero</td>
					<td><input type="text" name="paradero" style="width: 330px; text-transform:uppercase;" value="<?php echo $dattransp["paradero"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
			</table>
			</fieldset>
			<br>
			<fieldset>
			<legend class="titulo_campos">Ruta Opcional (Dejar en otro Lugar)</legend>
			<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Acudiente</td>
					<td>
						<select name="id_familiarop" style="width: 330px;" onChange="javascript:familiar();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						foreach($datfam as $regfam)
						{
							if (intval($dattransp["id_familiarop"]) == intval($regfam->id_familiar))
								echo "<option value='$regfam->id_familiar' selected title='$regfam->direccion $regfam->barrio'>$regfam->tipo - $regfam->apellidos $regfam->nombres</option>";
							else
								echo "<option value='$regfam->id_familiar' title='$regfam->direccion $regfam->barrio'>$regfam->tipo - $regfam->apellidos $regfam->nombres</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr>
					<td>Direcci�n</td>
					<td><input type="text" name="direccionop" style="width: 330px; text-transform:uppercase;" value="<?php echo $datfamiop["direccion"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td>Barrio</td>
					<td><input type="text" name="barrioop" style="width: 330px; text-transform:uppercase;" value="<?php echo $datfamiop["barrio"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td>Tel�fono</td>
					<td>
					<input type="text" name="telefonoop" style="width: 80px;" value="<?php echo $datfamiop["telefono"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					Celular
					<input type="text" name="celularop" style="width: 80px;" value="<?php echo $datfamiop["celular"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					Oficina
					<input type="text" name="telefofiop" style="width: 90px;" value="<?php echo $datfamiop["telefofi"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr>
					<td>Ruta</td>
					<td>
						<select name="id_rutaop" style="width: 330px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						foreach($datrut as $regrut)
						{
							if (intval($dattransp["id_rutaop"]) == intval($regrut->id_ruta))
								echo "<option value='$regrut->id_ruta' selected>$regrut->ruta - $regrut->recorrido</option>";
							else
								echo "<option value='$regrut->id_ruta'>$regrut->ruta - $regrut->recorrido</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr>
					<td>Paradero</td>
					<td><input type="text" name="paraderoop" style="width: 330px; text-transform:uppercase;" value="<?php echo $dattransp["paraderoop"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
		<td>
			<table border="0" cellpadding="2" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="16"><th width="70" class="titulo_campos">Transporte</th><th>Valor</th><th>Comentario Ajustes en Valor</th></tr>
				<?php
				for($i=2; $i<=11 ;$i++)
				{
					if ($i <= 9)
					{
						$nomval = "transp0".$i;
						$nomtex = "coment0".$i;
						$mes = "0".$i;
					}
					else
					{
						$nomval = "transp".$i;
						$nomtex = "coment".$i;
						$mes = $i;
					}
					$mes = nombre_mes($mes);
					?>
					<tr height="20" bgcolor="#FFFFFF" align="center">
						<th><?php echo $mes;?></th>
						<td><input type="text" name="<?php echo $nomval;?>" style="width: 50px; text-align: right; font-size: 12px;" value="<?php echo $dattransp[$nomval];?>" <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
						<td><input type="text" name="<?php echo $nomtex;?>" style="width: 330px; text-transform:uppercase;" value="<?php echo $dattransp[$nomtex];?>" <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					</tr>
					<?php
				}
				?>
			</table>
			<br>
			<fieldset>
			<legend class="titulo_campos">Observaciones</legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
					<textarea name='detalles'  style="width: 430px; height: 70px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $dattransp["detalles"];?></textarea>
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr height="36" bgcolor="#CCCCCC" background="images/backsilver.gif">
		<td></td>
		<td>
			<?php if ($id_transporte > 0) { ?>
			ACTUALIZADO <?php echo substr($dattransp["fchactual"],0,16);?> <?php echo $dattransp["usuactual"];?>
			<?php } ?>
		</td>
		<td colspan="3" align="right" valign="middle">
			<?php if ($_SESSION["nivmat"] >= 2 or $_SESSION["nivtra"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="5" alt="Grabar" align="absmiddle">
			<input type="image" class="botones_img" src="images/btn_imprimir.png" border="0" name="imprimir"  width="70" height="24" hspace="5" alt="Imprimir" align="absmiddle">
			<?php } if ($id_transporte > 0 and ($_SESSION["nivmat"] >= 4 or $_SESSION["nivtra"] >= 4)) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="5" align="absmiddle"></a>
			<?php } ?>
			<a href="alumnos_transporte.php?id_alumno=<?php echo $id_alumno;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
