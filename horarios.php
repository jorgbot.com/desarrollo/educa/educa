<?php
/***********************************************************************************************
* @file        : asignaturas_horarios.php                                                          *
* @brief       : Crear y actualizar horarios por curso, grado y a�o acad�dmico                 *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 24-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 24-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
if ($ano=="")
	$ano = $_SESSION["anoaca"];
connect();
//traer cursos del a�o
$id_director = 0;
$director = "SIN DEFINIR";
$alumtot = 0;
$sql="SELECT cursos.grado, cursos.curso, cursos.alumtot, cursos.id_director, grados.gradonom FROM cursos, grados where cursos.ano = '$ano' and cursos.grado = grados.grado order by grados.id_grado, cursos.curso";
$datcurs=viewsql($sql);
foreach($datcurs as $regcurs)
{
	if ($grado == "")
		$grado = $regcurs->grado;
	if ($curso == "" and  $regcurs->grado == $grado)
		$curso = $regcurs->curso;
	if ($regcurs->grado==$grado and $regcurs->curso==$curso)
	{
		$alumtot = $regcurs->alumtot;
		$id_director = $regcurs->id_director;
		break;
	}
}
//nivel del grado para intervalos
$nivel = "";
$sql="SELECT nivel FROM grados WHERE grado = '$grado' limit 1";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{$nivel = $reggrad->nivel;}
//nombre director
if ($id_director >0)
{
	$sql="SELECT apellidos, nombres FROM profesores where id_profesor = '$id_director' limit 1";
	$datprof=viewsql($sql);
	foreach($datprof as $regprof)
	{$director = $regprof->nombres." ".$regprof->apellidos;}
}
//intervalos de horario
$sql="SELECT * FROM horariosint where nivel = '$nivel' order by intervalo";
$datinte=viewsql($sql);
$numint = count($datinte);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	var pos = document.form0.gradocurso.value.indexOf('*');
	var grado = document.form0.gradocurso.value.substr(0,pos);
	var curso = document.form0.gradocurso.value.substr(pos+1);
	if (document.form0.vista[0].checked==true)
		var theURL = 'horarios.php?grado='+grado+'&curso='+curso;
	else	
		var theURL = 'horarios_prof.php';
	window.location.href = theURL;
}
function imprimir()
{
	var ano = document.form1.ano.value;
	var grado = document.form1.grado.value;
	var curso = document.form1.curso.value;
	var id_director = document.form1.id_director.value;
	var id_profesor = 0;
	var formato = 'M';
	var implogo = 'S';
	var papel = 'LEGAL';
	if (document.form1.formato[1].checked==true)
		formato = 'P';
	if (document.form1.formato[2].checked==true)
		formato = 'G';
	if (document.form1.papel[1].checked==true)
		papel = 'LETTER';
	if (document.form1.chklogo.checked == false)
		implogo = '';
	var theURL = 'reports/rep_horario.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&id_director='+id_director+'&id_profesor='+id_profesor+'&formato='+formato+'&papel='+papel+'&implogo='+implogo;
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
function borrarconf()
{
	var ano = document.form1.ano.value;
	var grado = document.form1.grado.value;
	var curso = document.form1.curso.value;
	ok=confirm("Esta seguro de eliminar horario del curso "+curso+"?");
	if(ok)
	{
		var theURL = 'database/grabar_asignaturas.php?op=borrarhorario&ano='+ano+'&grado='+grado+'&curso='+curso;
		window.location.href = theURL;
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
<form action="" method="post" name="form0">
	<tr height="50">
		<td class="titulo">
		 HORARIO &nbsp;
		<select name="gradocurso" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
		<?php
		foreach($datcurs as $regcurs)
		{
			if ($regcurs->grado==$grado and $regcurs->curso==$curso)
			echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $regcurs->gradonom - CURSO $regcurs->curso</option>";
			else
			echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $regcurs->gradonom - CURSO $regcurs->curso</option>";
		}
		?>																
		</select>
		</td>
		<td class="subtitulo"><input type="radio" name="vista" value="CURSO" checked onClick="javascript:seleccionar();"> Vista Cursos &nbsp;&nbsp; <input type="radio" name="vista" value="DOCENTE" onClick="javascript:seleccionar();"> Vista Docentes</td>
		<td class="subtitulo" align="right">
			ALUMNOS <?php echo $alumtot;?> A�O <?php echo $ano;?><BR>DIRECTOR: <?php echo $director;?>
		</td>
	</tr>
</form>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" align="center">
<form action="javascript:imprimir();" method="post" name="form1">
<input type="hidden" name="ano" value="<?php echo $ano;?>">
<input type="hidden" name="grado" value="<?php echo $grado;?>">
<input type="hidden" name="curso" value="<?php echo $curso;?>">
<input type="hidden" name="id_director" value="<?php echo $id_director;?>">
	<tr><td height="20" background="fondos/rollo.jpg"></td></tr>
	<tr>
		<td>
			<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr valign="top" bgcolor="#FFFFFF">
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr><th height="26" width="90" background="images/backsilver2.gif"></th></tr>
							<tr><td height="1" bgcolor="#C0C0C0"></td></tr>
							<?php
							//columna intervalos
							$nint = 0;
							foreach($datinte as $reginte)
							{
								if ($reginte->tipo=="")
								{
									$nint = $nint +1;
									echo "<tr height='34'><td class='titulo_campos' bgcolor='#FFFFFF'>&nbsp;<font color='#E50000'>$nint.</font> $reginte->horas</td></tr><tr><td height='1' bgcolor='#C0C0C0'></td></tr>";
								}
								else
									echo "<tr height='15'><td class='titulo_campos' bgcolor='#C0C0C0' align='center'><font color='#000000'>$reginte->tipo</font></td></tr><tr><td height='1' bgcolor='#C0C0C0'></td></tr>";
							}
							?>
						</table>
					</td>
					<?php 
					//columnas por dia
					for($i=1; $i<6 ;$i++)
					{
					$dia = $i;
					$sql="SELECT lisnom FROM listas WHERE listip = 'HORARIO' and lisval='$dia' limit 1";
					$lista=viewsql($sql);
					foreach($lista as $reglis)
					{$dianom = $reglis->lisnom;}
					?>
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr>
							<th height="26" width="170" background="images/backsilver2.gif">
							<?php if ($ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 3 and $_SESSION["usutip"]=="OPERADOR") { ?>
							<a class="link_black" href="horarios_dias.php?grado=<?php echo $grado;?>&ano=<?php echo $ano;?>&numdia=<?php echo $i;?>&cursosel=<?php echo $curso;?>"><?php echo $dianom;?></a>
							<?php } else { ?>
							<?php echo $dianom;?>
							<?php } ?>
							</th>
						</tr>
						<tr><td height="1" bgcolor="#C0C0C0"></td></tr>
						<?php
						$sql="SELECT * FROM horarios where ano='$ano' and dia='$dia' and grado='$grado' and curso='$curso' order by intervalo";
						$dathora=viewsql($sql);
						//listar intervalos
						foreach($datinte as $reginte)
						{
							$intervalo = $reginte->intervalo;
							$bgcolor = "#E0E7E9";
							$asignatura = "";
							$profesor = "";
							foreach($dathora as $reghora)
							{
								if ($reghora->intervalo == $intervalo)
								{
									$sql="SELECT asignatura FROM asignaturas where id_asignatura = '".$reghora->id_asignatura."' limit 1";
									$datasig=viewsql($sql);
									foreach($datasig as $regasig)
									{$asignatura = $regasig->asignatura;}
									$sql="SELECT apellidos, nombres FROM profesores where id_profesor = '".$reghora->id_profesor."' limit 1";
									$datprof=viewsql($sql);
									foreach($datprof as $regprof)
									{$profesor = $regprof->nombres." ".$regprof->apellidos;}
									$bgcolor = "#FFFFFF";
									break;
								}
							}
							if ($reginte->tipo=="")
							{
							?>
							<tr height="34">
								<td bgcolor="<?php echo $bgcolor;?>" class="txt_peq" title="<?php echo $asignatura;?>">
								<?php if ($asignatura!="") { ?>
								<img src="images/asignaturasp.png" width="16" height="16" border="0" hspace="0" align="absmiddle"><?php echo substr($asignatura,0,28);?>
								<?php if ($profesor != "") { ?>
								<br/>
								<img src="images/profesorp.png" width="16" height="16" border="0" hspace="0" align="absmiddle"><?php echo substr($profesor,0,28);?>
								<?php } }?>
								</td>
							</tr>
							<tr><td height="1" bgcolor="#C0C0C0"></td></tr>
							<?php } else { ?>
							<tr height="16"><td bgcolor="#C0C0C0"></td></tr>
							<?php
							}
						}
						?>
						</table>
					</td>
					<?php
					}
					?>
				</tr>
				<tr height="34">
					<th colspan="6" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver402.gif">
						<input type="radio" name="formato" value="M" checked> Formato Normal &nbsp;&nbsp; <input type="radio" name="formato" value="P"> Peque�o &nbsp;&nbsp; <input type="radio" name="formato" value="G"> Grande 
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="radio" name="papel" value="LEGAL" checked> Papel Oficio &nbsp;&nbsp; <input type="radio" name="papel" value="LETTER"> Papel Carta
						&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input type="checkbox" name="chklogo"> Imprimir Logo IDIC
						&nbsp;&nbsp;
						<input type="image" class="botones_img" src="images/btn_imprimir.png" border="0" name="imprimir"  width="70" height="24" hspace="10" alt="Imprimir" align="absmiddle">
						<?php if ($ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 4) { ?>
						<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
						<?php } ?>
						<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
					</th>
				</tr>
			</table>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
