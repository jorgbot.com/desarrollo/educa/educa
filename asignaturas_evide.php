<?php
/***********************************************************************************************
* @file        : asignaturas_evide.php                                                         *
* @brief       : Lista y edita evidencias y actividades por logro                              *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 12-dic-2017                                                    *
* @Modificado  : Nora Rodriguez 12-dic-2017                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
$periodo = $_GET["periodo"];
$id_asignatura = $_GET["id_asignatura"];
$id_logro = $_GET["id_logro"];
$id_actividad = isset($_GET['id_actividad']) ? $_GET['id_actividad'] : null ;
connect();
$datasignatura = datos_asignatura($id_asignatura); //datos asignatura
$ano = intval($datasignatura["ano"]);
$evideprom = intval($datasignatura["evideprom"]);
$activprom = intval($datasignatura["activprom"]);
$datlogro = datos_logro($ano,$id_logro); //datos logro
$evidemin = intval($datlogro["evidemin"]);
$evidepor = $datlogro["evidepor"];
$activmin = intval($datlogro["activmin"]);
$activpor = $datlogro["activpor"];
//traer evidencias
$file = "y".$ano."eviden";
$sql="SELECT * FROM $file WHERE id_asignatura = '$id_asignatura' and id_logro = '$id_logro' and periodo = '$periodo' order by numero";
$datevide=viewsql($sql);
//echo "$sql<br>";
//cargar arreglos con los datos
$file = "y".$ano."activ";
$portotal = 0;
$posevi = 0;
$evide = array();
$activ = array();
$j = 0;
for($i=1; $i<=$evidemin ;$i++)
{
    $valpor = substr($evidepor,$posevi,3);
    $posevi = $posevi + 3;
    $evide[$i][1] = "";
    $evide[$i][2] = $i;
    $evide[$i][3] = "";
    $evide[$i][4] = "";
    $evide[$i][5] = $valpor;
    $portotal = $portotal + $valpor;
    $posact = 0;
    for($k=1; $k<=$activmin ;$k++)
    {
        $j = $j + 1;
        $valpor = substr($activpor,$posact,3);
        $posact = $posact + 3;
        $activ[$j][1] = "";
        $activ[$j][2] = $k;
        $activ[$j][3] = "";
        $activ[$j][4] = "";
        $activ[$j][5] = $valpor;
    }
}
if (count($datevide) > 0) {
    $portotal = 0;
}
$numevi = 0;
$numact = 0;
foreach($datevide as $regevide)
{
    $numevi = $numevi + 1;
    $id_evidencia = $regevide->id_evidencia;
    $evide[$numevi][1] = $regevide->id_evidencia;
    $evide[$numevi][2] = $regevide->numero;
    $evide[$numevi][3] = $regevide->codigo;
    $evide[$numevi][4] = $regevide->evidencia;
    $evide[$numevi][5] = $regevide->porcentaje;
    $portotal = $portotal + intval($regevide->porcentaje);
    $sql="SELECT * FROM $file WHERE id_asignatura = '$id_asignatura' and id_logro = '$id_logro' and id_evidencia = '$id_evidencia' and periodo = '$periodo' order by numero";
    $datacti=viewsql($sql);
    $j = 0;
    foreach($datacti as $regactiv)
    {
        $j = $j + 1;
        $numact = $numact + 1;
        $activ[$numact][1] = $regactiv->id_actividad;
        $activ[$numact][2] = $regactiv->numero;
        $activ[$numact][3] = $regactiv->codigo;
        $activ[$numact][4] = $regactiv->actividad;
        $activ[$numact][5] = $regactiv->porcentaje;
    }
    if ($j < $activmin) {
        $j = $activmin - $j;
        //$numact = $numact + $j;
        for($k=1; $k<=$j ;$k++)
        {
            $numact = $numact + 1;
            if (count($datacti) > 0) {
                $activ[$numact][5] = 0;
            }
        }
    }
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/actividades.js'></script>
<script language="javascript" type='text/JavaScript'>
function checkPor(obj)
{
        var valobj = obj.value;
	if(valobj < 10)
	{
            obj.value = 10;
            alert('Porcentaje minimo es 10...');
            return;
        }
	var cantmin = eval ('document.form1.evidemin.value');
	cantmin = parseInt(cantmin,10);
	//calcular porcentaje total sumando porcentajes
	var total = 0;
	var porcen = 0;
        var pordis = 0;
	for(var i=1;i<=cantmin;i++){
		porcen = eval ('document.form1.porevi'+i+'.value');
		if (porcen=='' || porcen=='NaN')
			porcen = 0;
		else
			porcen = parseInt(porcen,10);
		total = total + porcen;
		if (total > 100)
		{
			total = total - porcen;
			pordis = 100 - total;
                        total = total + pordis;
			eval ("document.form1.porevi"+i+".value='"+pordis+"'");
		}
	}
	//retornar porcentaje total acumulado
	eval ("document.form1.portotal.value='"+total+"'");
}
function checkPorEvi(obj)
{
        var valobj = obj.value;
	if(valobj > 0 && valobj < 10)
	{
            obj.value = 10;
            alert('Porcentaje debe ser >= 10 o igual a 0 ...');
            return;
        }
        var namobj = obj.name;
        var numevi = namobj.substr(6,1);
	var evidemin = eval ('document.form1.evidemin.value');
	var activmin = eval ('document.form1.activmin.value');
	evidemin = parseInt(evidemin,10);
	activmin = parseInt(activmin,10);
	//calcular porcentaje total sumando porcentajes
	var total = 0;
	var porcen = 0;
        var pordis = 0;
        var j = 0;
        for(var i=1;i<=evidemin;i++){
            if (i==numevi)
                break;
            for(var k=1;k<=activmin;k++){
              j = j + 1;  
            }
        }
	for(var i=1;i<=activmin;i++){
                j = j + 1;
		porcen = eval ('document.form1.poract'+numevi+j+'.value');
		if (porcen=='' || porcen=='NaN')
                    porcen = 0;
		else
                    porcen = parseInt(porcen,10);
                total = total + porcen;
		if (total > 100)
		{
			total = total - porcen;
			pordis = 100 - total;
                        total = total + pordis;
			eval ("document.form1.poract"+numevi+j+".value='"+pordis+"'");
		}
	}
	//retornar porcentaje total suma actividades
        eval ("document.form1.portotalevi"+numevi+".value='"+total+"'");
}
function validar()
{
	var evideprom = document.form1.evideprom.value;
	var evidemin = document.form1.evidemin.value;
	evidemin = parseInt(evidemin,10);
	var activprom = document.form1.activprom.value;
	var activmin = document.form1.activmin.value;
	activmin = parseInt(activmin,10);
	if (evideprom <= 0)
	{
		var portotal = eval ('document.form1.portotal.value');
		portotal = parseInt(portotal,10);
		if(portotal < 100)
		{alert('Evidencias incompletas, suma evidencias inferior a 100...');document.form1.portotal.focus(); return false;}
        }
	var numreg = 0;
        var j = 0;
        var codevi = "";
        var nomevi = "";
        var totevi = 0;
        var codact = "";
        var nomact = "";
        var poract = 0;
	for(var i=1;i<=evidemin;i++)
        {
            codevi = eval ('document.form1.codevi'+i+'.value');
            nomevi = eval ('document.form1.desevi'+i+'.value');
            totevi = eval ('document.form1.portotalevi'+i+'.value');
            totevi = parseInt(totevi,10);
            if(totevi < 100 && evideprom <= 0)
                {alert('Evidencia '+i+' incompleta, suma actividades inferior a 100'); return false;}
            if (codevi == '')
                {alert('Evidencia '+i+' incompleta, falta c�digo...'); return false;}
            if (nomevi == '')
                {alert('Evidencia '+i+' incompleta, falta descripci�n...'); return false;}
            if (evideprom <= 0)
            {
		porevi = eval ('document.form1.porevi'+i+'.value');
		if (porevi=='' || porevi=='NaN')
                    porevi = 0;
		else
                    porevi = parseInt(porevi,10);
		if (porevi > 0)
		{
                    numreg = numreg + 1;
		}
		else
		{
                    if (i <= evidemin)
                    {alert('Evidencia '+i+' incompleta, falta porcentaje...'); return false;}
		}
                if (activprom <= 0)
                {
                    for(var k=1;k<=activmin;k++)
                    {
                        j = j + 1;
                        codact = eval ('document.form1.codact'+i+j+'.value');
                        nomact = eval ('document.form1.desact'+i+j+'.value');
                        poract = eval ('document.form1.poract'+i+j+'.value');
                        if (poract > 0 && codact == '')
                            {alert('Evidencia '+i+', actividad'+k+' incompleta, falta c�digo...'); return false;}
                        if (poract > 0 && nomact == '')
                            {alert('Evidencia '+i+', actividad'+k+' incompleta, falta descripci�n...'); return false;}

                    }
                }
            }
            else
            {
              numreg = numreg + 1;  
            }
        }
	if (numreg < evidemin)
	{alert('Evidencias incompletas, n�mero m�nimo '+evidemin); return false;}
	return true;
}

function borrarconf()
{
	var periodo = document.form1.periodo.value;
	ok=confirm('Esta seguro de eliminar evidencias y actividades del Logro para el periodo '+periodo+' ?');
	if(ok)
	{
		document.form1.op.value='borrarevidencias';
		document.form1.submit();
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="5" cellspacing="0" border="0" align="center">
	<tr height="50">
            <td class="titulo"><img src='images/actividades.jpg' width="40" height="40" border="0" align="absmiddle"> EVIDENCIAS Y ACTIVIDADES LOGRO <?php echo $datlogro["logrocod"];?> - PERIODO <?php echo $periodo;?></td>
		<td class="subtitulo" align="right"><?php echo $datasignatura["asignatura"];?><BR>GRADO <?php echo $datasignatura["grado"];?> A�O <?php echo $datasignatura["ano"];?></td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr><td colspan="2" height="40" class="tabla_color"><p align="justify"><b><?php echo $datlogro["logro"];?></b></p></td></tr>
	<tr><td colspan="2" height="10"></td></tr>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarevidencias'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='periodo' value='<?php echo $periodo;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='id_logro' value='<?php echo $id_logro;?>'>
<input type='hidden' name='evidemin' value='<?php echo $evidemin;?>'>
<input type='hidden' name='evideprom' value='<?php echo $evideprom;?>'>
<input type='hidden' name='activmin' value='<?php echo $activmin;?>'>
<input type='hidden' name='activprom' value='<?php echo $activprom;?>'>
<tr height="360" bgcolor="#FFFFFF">
    <td align="center" valign="top">
        <table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#009900" align="center">
            <?php
            $j = 0; //posici�n actividad inicial por evidencia
            for($i=1; $i<=$evidemin ;$i++)
            {
                $nameid = "idevi".$i;
                $namenum = "numevi".$i;
                $namecod = "codevi".$i;
                $namedes = "desevi".$i;
                $namepor = "porevi".$i;
                $nametot = "portotalevi".$i;
                $portotalevi = 0;
                ?>
                <tr height="30" align="left">
                    <td>
                        <b>EVIDENCIA</b>
                        <input type="hidden" name="<?php echo $nameid;?>" value="<?php echo $evide[$i][1];?>">
                        <input type="text" name="<?php echo $namenum;?>" value="<?php echo $evide[$i][2];?>" style="width: 15px;" maxlength="2" title="N�mero" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                        <input type="text" name="<?php echo $namecod;?>" value="<?php echo $evide[$i][3];?>" style="width: 60px; text-transform:uppercase;" maxlength="10" title="C�digo" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                        <input type="text" name="<?php echo $namedes;?>" value="<?php echo $evide[$i][4];?>" style="width: 750px; text-transform:uppercase;" title="Descripci�n" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                    </td>
                    <td>
                       <?php if ($evideprom > 0) { ?>
                       <input type="text" name="<?php echo $namepor;?>" id="<?php echo $namepor;?>" value="<?php echo $evide[$i][5];?>" readonly="readonly" style="width: 25px; text-align: right;" title="Porcentaje" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                       <?php } else { ?>
                       <input type="text" name="<?php echo $namepor;?>" id="<?php echo $namepor;?>" value="<?php echo $evide[$i][5];?>" style="width: 25px; text-align: right;" maxlength="3" onBlur="javascript:checkPor(this);" title="Porcentaje" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                       <?php } ?>
                        
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#FAFEC3" align="right">
                        <table border="0" cellpadding="1" cellspacing="1" align="right">
                        <?php
                        for($k=1; $k<=$activmin ;$k++)
                        {
                            $j = $j + 1;
                            $nameid = "idact".$i.$j;
                            $namenum = "numact".$i.$j;
                            $namecod = "codact".$i.$j;
                            $namedes = "desact".$i.$j;
                            $namepor = "poract".$i.$j;
                            $portotalevi = $portotalevi + $activ[$j][5];
                            ?>
                            <tr height="25" align="left">
                                <td>
                                    ACTIVIDAD
                                    <input type="hidden" name="<?php echo $nameid;?>" value="<?php echo $activ[$j][1];?>">
                                    <input type="text" name="<?php echo $namenum;?>" value="<?php echo $activ[$j][2];?>" style="width: 15px;" maxlength="2" title="N�mero" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    <input type="text" name="<?php echo $namecod;?>" value="<?php echo $activ[$j][3];?>" style="width: 60px; text-transform:uppercase;" maxlength="10" title="C�digo" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    <input type="text" name="<?php echo $namedes;?>" value="<?php echo $activ[$j][4];?>" style="width: 690px; text-transform:uppercase;" title="Descripci�n" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    %
                                    <?php if ($activprom > 0) { ?>
                                    <input type="text" name="<?php echo $namepor;?>" id="<?php echo $namepor;?>" value="<?php echo $activ[$j][5];?>" readonly="readonly" style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    <?php } else { ?>
                                    <input type="text" name="<?php echo $namepor;?>" id="<?php echo $namepor;?>" value="<?php echo $activ[$j][5];?>" style="width: 25px;" maxlength="3" onBlur="javascript:checkPorEvi(this);" title="Porcentaje" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                    <?php } ?>
                                </td>
                            </tr>
                        <?php
                        } ?>
                        </table>
                        <input type="hidden" name="<?php echo $nametot;?>" value="<?php echo $portotalevi;?>">
                    </td>
                </tr>
                <?php
            } 
            if ($evideprom > 0) { ?>
                <tr><td colspan="2" align="right"><b>PROMEDIO EVIDENCIAS</b></td></tr>
            <?php } else { ?>
            <tr><td align="right"><b>TOTAL LOGRO %</b></td><td><input type="text" name="portotal" id="portotal" style="width: 25px; text-align: right;" value="<?php echo $portotal;?>" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td></tr>
            <?php } ?>
        </table>
    </td>
</tr>
<tr height="36">
    <td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
        <?php if ($_SESSION["nivasi"] >= 2 and $ano == $_SESSION["anoaca"]) { ?>
        <input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
        <?php } if ($_SESSION["nivasi"] >= 3 and $ano == $_SESSION["anoaca"]) { ?>
        <a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="70" height="24" alt="Eliminar" hspace="10"></a>
        <?php } ?>
        <a href="asignaturas_logros.php?id_asignatura=<?php echo $id_asignatura;?>"><img src="images/btn_regresar.png" border="0" width="70" height="24" alt="Regresar lista Logros" hspace="10"></a>
        <a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="10"></a>
    </td>
</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
