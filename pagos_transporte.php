<?php
/***********************************************************************************************
* @file        : pagos_transporte.php                                                          *
* @brief       : Formulario captura pagos de transporte                                        *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 17-Jul-2015                                                    *
* @Modificado  : Nora Rodriguez 17-Jul-2015                                                    *
***********************************************************************************************/
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td>
			<fieldset>
			<legend class="titulo_campos">Datos Pago</legend>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
						<table border="0" cellpadding="1" cellspacing="1">
							<tr height="22">
								<td>Entrada <?php echo $datpago["id_entrada"];?></td>
								<td><?php echo fecha_texto($datpago["fchentrada"]);?></td>
							</tr>
							<tr height="22">
								<td>A�o Lectivo&nbsp;</td>
								<td><input type="text" name="ano" value="<?php echo $datpago["ano"];?>" style="width: 35px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							<tr height="22">
								<td></td>
								<td>
									<input type="hidden" name="porcentaje" value="0">
									<input type="hidden" name="destino" value="0">
								</td>
							</tr>
						</table>
					</td>
					<td width="10"></td>
					<td>
						<table border="0" cellpadding="1" cellspacing="1">
							<tr height="22">
								<td colspan="2">
									Banco
									<select name="id_banco" style="width: 135px; border: 1px solid #A7CD2C;" onChange="javascript:SeleccionarBanco();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									$sql="SELECT id_banco, banco, numero FROM bancos ORDER BY numero";
									$dat=viewsql($sql);
									foreach($dat as $reg)
									{
										$sel = "";
										if ($reg->id_banco==$datpago["id_banco"])
											$sel = "selected";
										echo "<option value='$reg->id_banco' $sel>$reg->banco</option>";
									}
									?>
									</select>
								</td>
							</tr>
							<tr height="22">
								<td><div id="titulofecha">Consignaci�n</div></td>
								<td>
									<input type="text" name="anopago" value="<?php echo $anopago;?>" style="width: 34px; height: 18px; text-align: right; font-size: 12px; border: 1px solid #A7CD2C;" onBlur="javascript:checkValAno(this);" maxlength="4" title="A�o" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<input type="text" name="mespago" value="<?php echo substr($datpago["fchpago"],5,2);?>" style="width: 22px; height: 18px; text-align: right; font-size: 12px; border: 1px solid #A7CD2C;" onBlur="javascript:checkValMes(this);" maxlength="2" title="Mes" onKeyDown="if(event.keyCode==13) event.keyCode=9;" autofocus>
									<input type="text" name="diapago" value="<?php echo substr($datpago["fchpago"],8,2);?>" style="width: 22px; height: 18px; text-align: right; font-size: 12px; border: 1px solid #A7CD2C;" onBlur="javascript:checkValDia(this);" maxlength="2" title="Dia" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr height="22">
								<td>Valor Pagado $&nbsp;&nbsp;</td>
								<td><input type="text" name="valor" value="<?php echo $datpago["valor"];?>" style="width: 85px; height: 18px; text-align: right; font-size: 12px; border: 1px solid #A7CD2C;" onBlur="javascript:Pagos_Transporte();" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</fieldset>
			<br>
			<fieldset>
			<legend class="titulo_campos">Historial Pagos Transporte</legend>
			<table border="0" cellpadding="2" cellspacing="1" bgcolor="#C0C0C0">
			<tr>
				<th width="75">FchConsigna</th>
				<th colspan="2">Hoja Entrada</th>
				<th width="40">IDPago</th>
				<th width="50">Consigna</th>
				<th width="50">Efectivo</th>
				<th width="50">Total</th>
			</tr>
			<?php
			$numpag = 0;
			$pagosnet = 0;
			$filepag = "y".$ano."pagos";
			$sql="SELECT * FROM $filepag WHERE id_alumno = '$id_alumno' AND tipo = 'T' ORDER BY fchpago, id_pago";
			$datpag = viewsql($sql);
			foreach($datpag as $regpag)
			{
				$numpag = $numpag + 1;
				$pagosnet = $pagosnet + $regpag->valor;
				?>
				<tr bgcolor="#FFFFFF" height="22">
					<th align="left"><?php echo fecha_texto($regpag->fchpago);?></th>
					<td width="20"><?php echo $regpag->id_entrada;?></td>
					<td width="40"><?php echo fecha_texto($regpag->fchentrada)?></td>
					<td><?php echo $regpag->id_pago;?></td>
					<td align="right"><?php echo number_format($regpag->valor_con,0);?></td>
					<td align="right"><?php echo number_format($regpag->valor_efe,0);?></td>
					<td align="right"><?php echo number_format($regpag->valor,0);?></td>
				</tr>
			<?php
			} ?>
                            <tr> 
                                <th colspan="7" align="right"><?php echo number_format($pagosnet,0);?></th>
                            </tr>        
			</table>			
			</fieldset>
		</td>
		<td width="10"></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Liquidaci�n Pago</legend>
			<table border="0" cellpadding="2" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr>
					<th colspan="2">Transporte</th>
					<th width="50">Pagos a Hoy</th>
					<th width="50">Debe a Hoy</th>
					<th width="60">Entrada</th>
				</tr>
				<?php
				if ($id_pago > 0)
				{
					$pagosdet = "y".$ano."pagosdet";
					$sql="SELECT id_item, mes, valor FROM $pagosdet WHERE id_pago = '$id_pago' ";
					$datite = viewsql($sql);
				}
				$valortot = 0;
				$pagostot = 0;
				$saldotot = 0;
				$netopago = 0;
				for($i=2; $i<=11 ;$i++)
				{
					if ($i <= 9)
						$mes = "0".$i;
					else
						$mes = $i;
					$campomes = "valortra".$mes;
					$camposal = "saldotra".$mes;
					$valorpag = $saldos[$mes];
					if ($i > $meshoy)
						$valorpag = 0;
					$valortot = $valortot + $valores[$mes];
					$pagostot = $pagostot + $pagos[$mes];
					$saldotot = $saldotot + $valorpag;
					//Activar meses pagados en el ID Pago
					if ($id_pago > 0)
					{
						$valorpag = 0;
						foreach($datite as $regite)
						{
							if (intval($regite->mes)==$i)
							{
								$valorpag = $regite->valor;
								$netopago = $netopago + $regite->valor;
							}
						}
					}
					?>
					<tr bgcolor="#FFFFFF" align="center" height="22">
						<th width="25" align="left"><?php echo strtoupper(substr(nombre_mes($mes),0,3));?></th>
						<td align="right"><?php echo number_format($valores[$mes],0);?></td>
						<td align="right"><?php echo number_format($pagos[$mes],0);?></td>
						<td align="right"><input type="hidden" name="<?php echo $camposal;?>" value="<?php echo $saldos[$mes];?>"><?php echo number_format($valorpag,0);?></td>
						<td align="right"><input type="text" name="<?php echo $campomes;?>" value="<?php echo $valorpag;?>" style="width: 55px;" class="disabled_yellow" disabled></td>
					</tr>
					<?php
				} //fin for meses
				$remantra="";
				if ($netopago < $datpago["neto"] and $id_pago > 0)
				{
					$remantra = $datpago["neto"] - $netopago;
					$pagostot = $pagostot + $remantra;
				}
                                $saldoFin = $pagosnet - $pagostot;
				?>
				<tr bgcolor="#FFFFFF" align="center" height="22">
					<th title="Saldo a Favor">+</th>
					<td></td>
					<td align="right"><?php if ($saldoFin > 0) echo number_format($saldoFin,0);?></td>
					<td align="right"></td>
					<td align="right"><input type="text" name="remantra" value="<?php echo $remantra;?>" style="width: 55px;" class="disabled_yellow" disabled></td>
				</tr>
				<tr height="22">
					<th>TOT</th>
					<th align="right"><?php echo number_format($valortot,0);?></th>
					<th align="right"><?php echo number_format($pagosnet,0);?></th>
					<th align="right"><?php echo number_format($saldotot,0);?></th>
					<th align="right">
						<input type='hidden' name='interes' value='0'>
						<input type="text" name="neto" value="<?php echo $datpago["neto"];?>" style="width: 55px;" class="disabled_yellow" readonly>
					</th>
				</tr>
			</table>
			</fieldset>
		</td>
	</tr>
</table>			
