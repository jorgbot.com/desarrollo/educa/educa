<?php
/***********************************************************************************************
* @file        : inicio.php                                                                    *
* @brief       : P�gina de incio, informaci�n general, autorizada a todos los usuarios         *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 06-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 06-Mar-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivpar"] <= 0)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</script>";}
include("database/database.php");
include("libs/fechas.php");
$op = isset($_GET['op']) ? $_GET['op'] : null ;
connect();
$sql="select * from institucion";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$instinom = $reginst->instinom;
	$instisig = $reginst->instisig;
	$nit = $reginst->nit;
	$ciudad = $reginst->ciudad;
	$direccion = $reginst->direccion;
	$telefono = $reginst->telefono;
	$correo1 = $reginst->correo1;
	$correo2 = $reginst->correo2;
	$correo3 = $reginst->correo3;
	$resolseed = $reginst->resolseed;
	$resoldane = $reginst->resoldane;
	$resolkin = $reginst->resolkin;
	$resolpri = $reginst->resolpri;
	$resolsec = $reginst->resolsec;
	$notaria = $reginst->notaria;
	$rector = $reginst->rector;
	$secretaria = $reginst->secretaria;
	$banco = $reginst->banco;
}
if ($op=="logros")
{
	$sql = "select id_logro, id_asignatura from y2012logros where grado = '' order by id_logro";
	$datlogr=viewsql($sql);
	foreach($datlogr as $reglogr)
	{
		$id_logro = $reglogr->id_logro;
		$id_asignatura = $reglogr->id_asignatura;
		$grado = 0;
		$sql = "select grado from asignaturas where id_asignatura = '$id_asignatura' limit 1";
		$datasig=viewsql($sql);
		foreach($datasig as $regasig)
		{$grado = $regasig->grado;}
		$sql = "update y2012logros set grado = '$grado' where id_logro = '$id_logro' limit 1";
		execsql($sql);
	}
	echo "REPARACION DE LOGROS EJECUTADA...";
}
if ($op=="cursos")
{
	$sql = "select id_curso, id_alumno, grado from y2012cursos order by id_curso";
	$datcurs=viewsql($sql);
	foreach($datcurs as $regcurs)
	{
		$id_curso = $regcurs->id_curso;
		$id_alumno = $regcurs->id_alumno;
		$grado = $regcurs->grado;
		$nivel = "";
		$sql = "select nivel from grados where grado = '$grado' limit 1";
		$datgrad=viewsql($sql);
		foreach($datgrad as $reggrad)
		{$nivel = $reggrad->nivel;}
		$sql="SELECT apellidos, nombres, tipoedu, estado FROM alumnos WHERE id_alumno = '$id_alumno' limit 1";
		$datalum=viewsql($sql);
		foreach($datalum as $regalum)
		{
			$apellidos = $regalum->apellidos;
			$nombres = $regalum->nombres;
			$tipoedu = $regalum->tipoedu;
			$estado = $regalum->estado;
			$sql = "update y2012cursos set apellidos = '$apellidos', nombres = '$nombres', tipoedu = '$tipoedu', nivel = '$nivel' where id_curso = '$id_curso' limit 1";
			execsql($sql);
		}
		$sql = "update matriculas set nivel = '$nivel' where id_alumno = '$id_alumno' and grado = '$grado' limit 1";
		execsql($sql);
	}
	echo "REPARACION DE CURSOS EJECUTADA...";
}
if ($op=="fallas")
{
	$numreg = 0;
	$id_alumno = 0;
	$fecha = "";
	$sql = "select id_falla, id_alumno, fecha from y2012fallas where id_asignatura = '0' order by id_alumno, id_falla";
	$dat=viewsql($sql);
	foreach($dat as $reg)
	{
		$id_falla = $reg->id_falla;
		if ($reg->fecha != $fecha or $reg->id_alumno != $id_alumno)
		{
			$id_alumno = $reg->id_alumno;
			$fecha = $reg->fecha;
			//buscar falla repetida
			$idfall = 0;
			$sql = "select id_falla from y2012fallas where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura = '0' and id_falla != '$id_falla' order by id_falla";
			$datfall=viewsql($sql);
			foreach($datfall as $regfall)
			{
				$idfall = $regfall->id_falla;
				$sql = "delete from y2012fallas where id_falla = '$idfall' limit 1";
				execsql($sql);
			}
			if ($idfall > 0) //registro repetido
			{
				$numreg = $numreg + 1;
				$id_asignatura = 0;
				$sql = "select id_falla, id_asignatura from y2012fallas where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura > '0' order by id_asignatura, id_falla";
				$datfall=viewsql($sql);
				foreach($datfall as $regfall)
				{
					$idfall = $regfall->id_falla;
					if ($id_asignatura != $regfall->id_asignatura)
					{
						$id_asignatura = $regfall->id_asignatura;
						$sql = "select id_falla from y2012fallas where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura = '$id_asignatura' and id_falla != '$idfall' order by id_falla";
						$datrep=viewsql($sql);
						foreach($datrep as $regrep)
						{
							$rfall = $regrep->id_falla;
							$sql = "delete from y2012fallas where id_falla = '$rfall' limit 1";
							execsql($sql);
						}
					}
				}
			}
		}
	}
	echo "REPARACION DE FALLAS EJECUTADA... REPETIDAS $numreg";
}
?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>IDIC</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="robots" content="noindex"/>
<meta name="googlebot" content="noindex"/>
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="estilos.css">
<link href="estilosfot.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#FFFF66">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="150" bgcolor="#006600">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr height="40" bgcolor="#006600">
		<td background="fondos/top_verde.jpg"></td>
		<td background="fondos/top_verde.jpg" class="titulo_color">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_top.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/top_verde.jpg"></td>
	</tr>
	<tr height="3" bgcolor="#FFFF66">
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_top.jpg"></td></tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="200">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10"></td>
				<td>
					<?php include("includes/menu_setup.php");?>
					<br/>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr height="32">
							<td width="6" background="images/backsilver401.gif"></td>
							<td align="left"
							 background="images/backsilver402.gif" class="titulo"><img src="images/ok28.png" width="28" height="28" border="0" align="absmiddle"> 
							Par�metros Instituci�n
							</td>
							<td width="6" background="images/backsilver403.gif"></td>
						</tr>
					</table>
					<table width="100%" border="0" cellpadding="0" cellspacing="0" bgcolor="#C0C0C0" class="tabla_marco">
					<form action="database/grabar_setup.php" method="post" name="form1" enctype="multipart/form-data">
					<input type='hidden' name='op' value='grabarparametros'>
					<tr height="10"><td width="30"></td><td></td><td width="30"></td></tr>
					<tr>
						<td></td>
						<td>
							<fieldset><legend class="titulo_campos"><?php echo $instisig;?></legend>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
									<tr height="5">
										<td width="20"></td><td width="80"></td><td></td>
									</tr>
									<tr>
										<td></td>
										<td>Identificaci�n NIT</td>
										<td><input type="text" name="nit" style="width: 90px;" value="<?php echo $nit;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
									</tr>
									<tr>
										<td></td>
										<td>Direcci�n</td>
										<td><input type="text" name="direccion" style="width: 220px;" value="<?php echo $direccion;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
									</tr>
									<tr>
										<td></td>
										<td>Ciudad</td>
										<td>
											<input type="text" name="ciudad" style="width: 220px;" value="<?php echo $ciudad;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
										</td>
									</tr>
									<tr>
										<td></td>
										<td>Tel�fonos</td>
										<td>
											<input type="text" name="telefono" style="width:220px;" value="<?php echo $telefono;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
										</td>
									</tr>
									<tr>
										<td></td>
										<td>Rector</td>
										<td><input type="text" name="rector" style="width: 220px; text-transform:uppercase;" value="<?php echo $rector;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
									</tr>
									</table>
								</td>
								<td width="5"></td>
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
									<tr height="5">
										<td width="20"></td><td width="100"></td><td></td>
									</tr>
									<tr>
										<td></td>
										<td>Secretaria</td>
										<td><input type="text" name="secretaria" style="width: 220px; text-transform:uppercase;" value="<?php echo $secretaria;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
									</tr>
									<tr>
										<td></td>
										<td>Correo Coordinaci�n</td>
										<td><input type="text" name="correo1" style="width: 220px;" value="<?php echo $correo1;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
									</tr>
									<tr>
										<td></td>
										<td>Correo Secretar�a</td>
										<td><input type="text" name="correo2" style="width: 220px;" value="<?php echo $correo2;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
									</tr>
									<tr>
										<td></td>
										<td>Correo Administrador</td>
										<td><input type="text" name="correo3" style="width: 220px;" value="<?php echo $correo3;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
									</tr>
									<tr>
										<td></td>
										<td>Banco y Convenio</td>
										<td><input type="text" name="banco" style="width: 220px;" value="<?php echo $banco;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
									</tr>
									</table>
								</td>
								<td></td>
							</tr>
							</table>
							</fieldset>
							<fieldset><legend class="titulo_campos">Resoluciones</legend>
							<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<tr height="5">
								<td width="20"></td><td width="80"></td><td></td>
							</tr>
							<tr>
								<td></td>
								<td>Preescolar</td>
								<td><input type="text" name="resolkin" style="width: 520px;" value="<?php echo $resolkin;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							<tr>
								<td></td>
								<td>Primaria</td>
								<td><input type="text" name="resolpri" style="width: 520px;" value="<?php echo $resolpri;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							<tr>
								<td></td>
								<td>Secundaria</td>
								<td><input type="text" name="resolsec" style="width: 520px;" value="<?php echo $resolsec;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							<tr>
								<td></td>
								<td>Dane</td>
								<td><input type="text" name="resoldane" style="width: 520px;" value="<?php echo $resoldane;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							<tr>
								<td></td>
								<td>Sec.Educaci�n</td>
								<td><input type="text" name="resolseed" style="width: 520px;" value="<?php echo $resolseed;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							<tr>
								<td></td>
								<td>Notar�a</td>
								<td><input type="text" name="notaria" style="width: 520px;" value="<?php echo $notaria;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							</table>
							</fieldset>
						</td>
						<td></td>
					</tr>
					<tr><td colspan="3" height="30"></td></tr>
					<tr height="36">
						<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
							<?php if ($_SESSION["nivpar"] >= 3) { ?>
							<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
							<?php } ?>
						</td>
					</tr>
					</form>					
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_bot.jpg"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#D3A919"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FFDB4B"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FCF463"></td></tr>
	<tr>
		<td colspan="3" valign="top">
		<div id="fondo_pie">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</div>
		</td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>