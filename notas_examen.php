<?php
/***********************************************************************************************
* @file        : notas_examen.php                                                              *
* @brief       : formulario de captura para notas de examenes semestrales, finales o superaci�n*
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 18-ene-2013                                                    *
* @Modificado  : Nora Rodriguez 18-feb-2013                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_notas.php");
include("libs/fechas.php");
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
$pertipo = isset($_GET['pertipo']) ? $_GET['pertipo'] : null ;
$periodo = isset($_GET['periodo']) ? $_GET['periodo'] : null ;
$estado = isset($_GET['estado']) ? $_GET['estado'] : null ;
$examen = isset($_GET['examen']) ? $_GET['examen'] : null ;
$ano = $_SESSION["anoaca"];
connect();
//datos asignatura
$sql="select ano, grado, asignatura, activprom, id_area from asignaturas where id_asignatura = '$id_asignatura' limit 1";
$datasig = viewsql($sql);
foreach($datasig as $regasig)
{
	$ano = $regasig->ano;
	$grado = $regasig->grado;
	$asignatura = $regasig->asignatura;
	$activprom = $regasig->activprom;
	$id_area = $regasig->id_area;
}
//traer id_alumno del curso
$sql="SELECT apellidos, nombres, tipoedu, fchretiro FROM alumnos WHERE id_alumno = '$id_alumno' limit 1";
$datalum=viewsql($sql);
foreach($datalum as $regalum)
{
	$alumno = $regalum->apellidos." ".$regalum->nombres;
	$tipoedu = $regalum->tipoedu;
	$fchretiro = $regalum->fchretiro;
}
//datos alumno
if ($tipoedu == "E")
	$alumno = $alumno." (ESPECIAL)";
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$areasper = $reganol->areasper;
	$notalim = $reganol->notalim;
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
}
$notas = "y".$ano."notas";
$notaex = "";
$sql="select nota from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = '$pertipo' and periodo = '$periodo' LIMIT 1";
$datrec=viewsql($sql);
foreach($datrec as $regrec)
{$notaex = $regrec->nota;}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript'>
function checkNota(obj)
{
	var valobj = obj.value;
	var notamin = '<?php echo $notamin;?>';
	var notamax = '<?php echo $notamax;?>';
	notamin = parseInt(notamin,10);
	notamax = parseInt(notamax,10);
	if (valobj=='' || valobj=='NaN')
		var intNota = 0;
	else
		var intNota = parseInt(valobj,10);
	if (intNota > 0)
	{
		//validar minimo y maximo de la nota
		if (intNota < notamin)
		{
			obj.value = '';
			obj.focus();
			alert('Nota inv�lida, m�nima '+notamin);
			return false;
		}
		if (intNota > notamax)
		{
			obj.value = '';
			obj.focus();
			alert('Nota inv�lida, m�xima '+notamax);
			return false;
		}
	}
	else
	{
		obj.value = '';
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/salud.png' width="40" height="40" border="0" align="absmiddle"> <?php echo $examen;?></td>
		<td class="subtitulo" align="right">GRADO <?php echo $grado;?> A�O <?php echo $ano;?></td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td colspan="2" height="30" class="tabla_color">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
					&nbsp;&nbsp;
					<img src="images/alumnosm.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"> <b><?php echo $alumno;?></b>
					</td>
					<td class="titulo_error" align="left">
					&nbsp;
					<?php if (substr($fchretiro,0,1) > 0) { ?>
					(RETIRADO EL <?php echo fecha_texto($fchretiro);?>)
					<?php } ?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="database/grabar_planillas.php" method="post" name="form1">
<input type='hidden' name='op' value='planilla_examen'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='grado' value='<?php echo $grado;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='id_area' value='<?php echo $id_area;?>'>
<input type='hidden' name='tipoedu' value='<?php echo $tipoedu;?>'>
<input type='hidden' name='pertipo' value='<?php echo $pertipo;?>'>
<input type='hidden' name='periodo' value='<?php echo $periodo;?>'>
<input type='hidden' name='notalim' value='<?php echo $notalim;?>'>
<input type='hidden' name='notamin' value='<?php echo $notamin;?>'>
<input type='hidden' name='notamax' value='<?php echo $notamax;?>'>
	<tr height="20">
		<th>Asignatura</th>
		<th width="35">Nota</th>
	</tr>
	<tr height="70" bgcolor="#FFFFFF">
		<td>&nbsp;<?php echo $asignatura;?></td>
		<td align="center">
			<input type="text" name="notaex" id="notaex" value="<?php echo $notaex;?>" style="width: 30px; border: 1px solid #009900;" onBlur="javascript:checkNota(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
		</td>
	</tr>
	<tr height="36">
		<td colspan="2" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivnot"] >= 4 and $estado == "ABIERTO") { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
