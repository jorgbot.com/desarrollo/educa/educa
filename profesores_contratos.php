<?php
/***********************************************************************************************
* @file        : profesores_contratos.php                                                      *
* @brief       : Lista y edita contratos anuales de un profesor                                *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 10-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 10-Mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_profesores.php");
include("libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$id_profesor = $_GET["id_profesor"];
$id_contrato = isset($_GET['id_contrato']) ? $_GET['id_contrato'] : null ;
$op = isset($_GET['op']) ? $_GET['op'] : null ;
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
if ($op == "")
	$op = "listar";
$opcion = "contratos";
connect();
$sql="SELECT apellidos, nombres FROM profesores WHERE id_profesor = '$id_profesor' limit 1";
$datprof=viewsql($sql);
foreach($datprof as $regprof)
{$titulo = "DOCENTE: ".$regprof->apellidos." ".$regprof->nombres;}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/validInput.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.estado.value == 'CERRADA' && document.form1.curso.value == '')
	{alert('Falta asignar curso en estado CERRADA...');document.form1.curso.focus(); return false;}
	return true;
}
function seleccionar()
{
	var ano = document.form1.ano.value;
	var id_profesor = document.form1.id_profesor.value;
	var id_contrato = document.form1.id_contrato.value;
	var theURL = 'profesores_contratos.php?op=editar&ano='+ano+'&id_profesor='+id_profesor+'&id_contrato='+id_contrato;
	window.location.href = theURL;
}
</script>
</head>
<body>
<?php include("includes/menu_profesores.php");?>
<?php if ($op == "listar") { ?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="420" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="35">A�o</th>
					<th width="50">ID</th>
					<th width="60">Estado</th>
					<th width="60">Inicio</th>
					<th width="60">Final</th>
					<th width="35">tipo</th>
					<th>Grados</th>
					<th>Observaciones</th>
				</tr>
				<?php
				$sql="SELECT * FROM profesorescont WHERE id_profesor = '$id_profesor' order by ano desc";
				$datcont=viewsql($sql);
				foreach($datcont as $regcont)
				{
				$id_contrato = $regcont->id_contrato;
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center"><?php echo $regcont->ano;?></td>
					<td><a class="link_list" href="profesores_contratos.php?op=editar&id_contrato=<?php echo $id_contrato;?>&id_profesor=<?php echo $id_profesor;?>" title="ID contrato"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $id_contrato;?></a></td>
					<td><?php echo $regcont->estado;?></td>
					<td><?php echo fecha_texto($regcont->fchinicio);?></td>
					<td><?php echo fecha_texto($regcont->fchfinal);?></td>
					<td><?php echo $regcont->tipocont;?></td>
					<td><?php echo $regcont->grados;?></td>
					<td><?php echo $regcont->detalles;?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivpro"] >= 3) { ?>
			<a href="profesores_contratos.php?op=editar&id_profesor=<?php echo $id_profesor;?>"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
<?php } if ($op == "editar") {
$titulo = "Nuevo Registro";
$datcontrato = datos_contrato($id_contrato);
if ($id_contrato > 0)
	$titulo = "Editar Contrato ID ".$id_contrato;
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_profesores.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarmatricula'>
<input type='hidden' name='id_contrato' value='<?php echo $id_contrato;?>'>
<input type='hidden' name='id_profesor' value='<?php echo $id_profesor;?>'>
<input type='hidden' name='tipoedu' value='<?php echo $tipoedu;?>'>
<input type='hidden' name='oldcurso' value='<?php echo $datcontrato["curso"];?>'>
	<tr height="10">
		<td width="30"></td>
		<td></td>
		<td width="30"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><th colspan="2" align="left" height="20" class="titulo_campos"><?php echo $titulo;?></th></tr>
				<tr><td colspan="2" height="25"></td></tr>
				<tr height="30">
					<td>A�o <font color="red">*</font>&nbsp;</td>
					<td>
						<select name="ano" onChange="javascript:seleccionar();">
						<?php
						for($i=2011; $i<=$ano+1 ;$i++)
						{
							$selano = "";
							if ($i==$ano)
								$selano="selected";
							echo "<option value='$i' $selano>$i</option>";
						}
						?>
						</select>
					</td>
				</tr>				
				<tr height="30">
					<td>Estado <font color="red">*</font>&nbsp;</td>
					<td>
						<select name="estado" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php
						$sql="SELECT * FROM listas WHERE listip = 'PROFESORES' order by id";
						$lista=viewsql($sql);
						foreach($lista as $reglis)
						{
							if ($datcontrato["estado"] == $reglis->lisnom)
								echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
							else
								echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
						}
						?>																
						</select>
						&nbsp;
						Inicio
						<input type="text" name="fchinicio" style="width: 65px;" value="<?php echo $datcontrato["fchinicio"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						Final
						<input type="text" name="fchfinal" style="width: 65px;" value="<?php echo $datcontrato["fchfinal"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						Tipo
						<input type="text" name="tipocont" style="width: 100px; text-transform:uppercase;" value="<?php echo $datcontrato["tipocont"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">						
					</td>
				</tr>
				<tr height="30">
					<td>Grados </td>
					<td>
					<table border="0" cellpadding="0" cellspacing="0">
					<?php
					$sql="SELECT grado FROM grados order by id_grado";
					$lista=viewsql($sql);
					foreach($lista as $reglis)
					{
						$grado = $reglis->grado." ";
						$chk = "";
						if (strpos($datcontrato["grados"],$grado) > 0)
							$chk = "checked";
						if ($reglis->grado > 0)
							$grado.="&deg;";
						?>
						<td><b><?php echo $grado;?></b> <input type='checkbox' name='grado' <?php echo $chk;?>>&nbsp;</td>
						<?php
					}
					?>
					</table>																
					</td>
				</tr>
				<tr>
					<td>Observaciones</td>
					<td>
					<textarea name='detalles' rows="4" cols="102" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datcontrato["detalles"];?></textarea>
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivpro"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } ?>
			<a href="profesores_contratos.php?id_profesor=<?php echo $id_profesor;?>&id_contrato=<?php echo $id_contrato;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
