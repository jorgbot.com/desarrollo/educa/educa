<?php
/***********************************************************************************************
* @file        : index.php                                                                     *
* @brief       : Pagina principal para ingreso al aplicativo                                   *
* @version     : 2.0                                                                           *
* @company     : EDUCA #00923F                                                                 *
* @author      : Nora Rodriguez 01-jul-2019                                                    *
* @Modificado  : Nora Rodriguez 13-jul-2019                                                    *
***********************************************************************************************/
include("libs/fechas.php");
include("database/database.php");
$msgerror = isset($_GET['msgerror']) ? $_GET['msgerror'] : null ;
$anoaca = 2019;
$tema = "";
connectdbmaster();
$sql="SELECT * FROM temas WHERE id_tema = '1' LIMIT 1";
$dattema=viewsql($sql);
foreach($dattema as $regtema)
{
	$tema = $regtema->temtex;
}
?>
<!DOCTYPE html>
<html lang="es">
<head>
<title>Educa</title>
<meta charset="utf-8" />
<meta name="robots" content="noindex" />
<meta name="googlebot" content="noindex" />
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" href="estilos.css" />
<SCRIPT language="JavaScript">
<!--
	function validarlogin(formulario)
	{
		if (formulario.login.value == '')
		{
			alert('Debe escribir su Login de Usuario');
			formulario.login.focus();
			return false;
		}
		if (formulario.pass.value == '')
		{
			alert('Debe escribir su contrase�a');
			formulario.pass.focus();
			return false;
		}
		return true;
	}
-->
</SCRIPT>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="1">
        <td></td>
        <td width="1000"></td>
        <td></td>
    </tr>
    <tr height="150">
        <td background="fondos/fondolat1.jpg">&nbsp;</td>
        <td width="1000" align="center">
            <table width="1000" border="0" cellpadding="0" cellspacing="0" align="center" background="fondos/fondo.jpg">
                <tr height="150" valign="top">
                    <td width="850">&nbsp;</td>
                    <td><font color="#FFFFFF" size="+3"><br><?php echo $anoaca;?></font></td>
                </tr>
            </table>                
        </td>
	<td background="fondos/fondolat2.jpg">&nbsp;</td>
    </tr>
    <tr height="12">
        <td background="fondos/marco_top.gif"></td>
        <td width="1000" background="fondos/marco_top.gif"></td>
        <td  background="fondos/marco_top.gif"></td>
    </tr>
    <tr height="50">
        <td></td>
	<td width="1000" align = "center" valign="middle" bgcolor="#FFFFFF" class="titulo">
            Plataforma en la nube para Gesti&oacute;n Escolar
        </td>
	<td></td>
    </tr>
    <tr height="460">
        <td></td>
        <td width="1000" align = "center" bgcolor="#FFFFFF">
						<table border="0" cellpadding="0" cellspacing="0" width="50%">
							<tr>
							<td align='center' valign='top'>
								<table border="0" cellpadding="0" cellspacing="0" width="100%" height="185">
								<form action="libs/login.php" method="post" name="formulario" onsubmit="return validarlogin(this)">
									<tr height="70">
                                                                                <td align="center" class="titulo" bgcolor="#FFFF00" background="fondos/back_yellowB.jpg"><img src="images/educacion70.png" border="0" width="70" height="70" align="absmiddle">Iniciar Sesi&oacute;n</td>
									</tr>
									<tr>
										<td>
											<div id="fondo_pie">
											<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
												<tr height="30"><td colspan="3" align="center" class="titulo_error"><?php echo $msgerror;?></td></tr>
												<tr height="25">
													<td></td>
													<td class="txt_med" align="right">Usuario &nbsp;&nbsp;</td>
													<td><input type='text' name='login' size='40' autofocus onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
												</tr>
												<tr height="25">
													<td></td>
                                                                                                        <td class="txt_med" align="right">Contrase&nacute;a &nbsp;&nbsp;</td>
													<td><input type='password' name='pass' size='40'></td>
												</tr>
												<tr height="55"><td colspan="3" align="center"><input type="image" class="botones_img" border="0" name="continuar" src="images/btn_ingresar.png" width="70" height="24" hspace="15"></td></tr>
											</table>
											</div>
										</td>
									</tr>
									<tr bgcolor="#FFFFF0" height="15">
													<td background="images/marco_bot0.gif" width="100%"></td>
									</tr>
								</form>
								</table>
							</td>
							</tr>
							<tr><td height="20"></td></tr>
							<tr>
								<td>
									<table border="0" cellpadding="4" cellspacing="4" width="100%" style="border: 1px solid #FD8900;">
										<tr><td height="20" colspan="2" style="font-family: Arial Narrow, Arial; font-size: 13px; font-weight: bold; color: #FD8900;" align="center"><u>PLATAFORMA VIRTUAL EDUCATIVA</u></td></tr>
										<tr>
											<td style="font-family: Arial Narrow, Arial; font-size: 13px; font-weight: bold; color: #FD8900;">
                                                                                            <?php echo $tema;?>
											</td>
											<td width="72" align="right"><img src="fondos/idiclibros.jpg" border="0" width="72" height="140" hspace="0" align="right"></td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
                
        </td>
	<td></td>
    </tr>
    <tr>
        <td colspan="3" valign="top">
            <div id="fondo_pie">
            <?php include("includes/menu_bottom.php");?>
            </div>
        </td>
    </tr>
</table>
</body>
</html>
