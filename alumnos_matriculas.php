<?php
/***********************************************************************************************
* @file        : alumnos_matriculas.php                                                        *
* @brief       : Lista y edita matriculas de un alumno                                         *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 10-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 06-Nov-2018                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("libs/fechas.php");
$id_matricula = isset($_GET['id_matricula']) ? $_GET['id_matricula'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] : $_SESSION["anoaca"] ;
$gradocurso = isset($_GET['gradocurso']) ? $_GET['gradocurso'] : null ;
$op = isset($_GET['op']) ? $_GET['op'] : "listar" ;
$opcion = "matriculas";
connect();
include("includes/menu_titulo.php");
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/validInput.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	var pos = document.form1.gradocurso.value.indexOf('*');
	var grado = document.form1.gradocurso.value.substr(0,pos);
	var curso = document.form1.gradocurso.value.substr(pos+1);
	if(document.form1.fecha.value == '')
	{alert('Falta asignar fecha de matricula...');document.form1.fecha.focus(); return false;}
	if(document.form1.estado.value == 'CERRADA' && curso == '')
	{alert('Falta asignar curso para para estado CERRADA...');document.form1.gradocurso.focus(); return false;}
	if(document.form1.estado.value == 'RETIRADA' && document.form1.fchretiro.value == '')
	{alert('Falta fecha de retiro para para estado de RETIRADA...');document.form1.fchretiro.focus(); return false;}
	if(curso == '' && document.form1.notas.value > 0)
	{alert('Falta asignar curso, matricula con notas...');document.form1.gradocurso.focus(); return false;}
	if(document.form1.notas.value > 0 && document.form1.estado.value!='CERRADA' && document.form1.estado.value!='RETIRADA')
	{alert('Estado no v�lido, matr�cula con notas debe estar CERRADA o RETIRADA...');document.form1.estado.focus(); return false;}
	if(document.form1.beca.value <= 0 && document.form1.matricula.value=='0')
	{alert('Falta valor de matricula...');document.form1.matricula.focus(); return false;}
	if(document.form1.id_respmatri.value == '')
	{alert('Falta asignar acudiente responsable matricula...');document.form1.id_respmatri.focus(); return false;}
	if(document.form1.id_resppagos.value == '')
	{alert('Falta asignar acudiente responsable de pagos...');document.form1.id_resppagos.focus(); return false;}
	var gradovalpen = document.form1.gradovalpen.value;
	for(var i=2;i<=11;i++){
		if (i < 10)
			var mes = '0'+i;
		else
			var mes = i;
		var coment = eval ('document.form1.coment'+mes+'.value');
		var valor = eval ('document.form1.pension'+mes+'.value');
		if (valor != '' && coment=='')
		{
			alert('Debe justificar el ajuste en pensi�n del mes '+mes);
			eval ("document.form1.coment"+mes+".focus()");
			return false;		
		}
	}
	return true;
}
function seleccionar()
{
	var ano = document.form1.ano.value;
	var gradocurso = document.form1.gradocurso.value;
	var id_alumno = document.form1.id_alumno.value;
	var id_matricula = document.form1.id_matricula.value;
	var theURL = 'alumnos_matriculas.php?op=editar&ano='+ano+'&gradocurso='+gradocurso+'&id_alumno='+id_alumno+'&id_matricula='+id_matricula;
	window.location.href = theURL;
}
function LiquidarBeca()
{
	var beca = parseInt(document.form1.beca.value);
	if (isNaN(beca))
		beca = 0;
	var gradovalpen = parseInt(document.form1.gradovalpen.value);
	if (isNaN(gradovalpen))
		gradovalpen = 0;
	if (beca > 0 && gradovalpen > 0)
	{
		var coment = '';
		var pension = 0;
		for(var i=2;i<=11;i++){
			if (i < 10)
				var mes = '0'+i;
			else
				var mes = i;
			coment = eval ('document.form1.coment'+mes+'.value');
			pension = 0;
			if (coment=='')
			{
				pension = Math.floor(gradovalpen * (beca/100));
				if (beca==100)
					pension = 0;
				eval ("document.form1.pension"+mes+".value='"+pension+"'");
				eval ("document.form1.coment"+mes+".value='APLICACION BECA'");
			}
		}
	}
}
function borrarMatricula()
{
	var ano = document.form1.ano.value;
	ok=confirm('Esta seguro de eliminar Matricula '+ano+' ?');
	if(ok)
	{
		document.form1.op.value='borrarmatricula';
		document.form1.submit();
	}
}
//imprimir hoja de matricula
function imprimir()
{
	var id_alumno = document.form1.id_alumno.value;
	var ano = document.form1.ano.value;
	var theURL = 'reports/matricula.php?id_alumno='+id_alumno+'&ano='+ano;
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
</script>
</head>
<body>
<?php include("includes/menu_alumnos.php");?>
<?php if ($op == "listar") { ?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="420" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="45">A�o</th>
					<th width="35">Grado</th>
					<th width="35">Curso</th>
					<th width="60">Estado</th>
					<th width="70">Fecha</th>
					<th width="50">Folio</th>
					<th width="35">Beca</th>
					<th>Perdida Area/A�o</th>
					<th>Observaciones</th>
				</tr>
				<?php
				$sql="SELECT * FROM matriculas WHERE id_alumno = '$id_alumno' order by ano desc";
				$datmatr=viewsql($sql);
				foreach($datmatr as $regmatr)
				{
				$id_matricula = $regmatr->id_matricula;
				$fecha = $regmatr->fecha;
				$areaper = "";
				$id_areaper = 0;
				$cursos = "y".$regmatr->ano."cursos";
				$sql = "select pierde, id_areaper, fchretiro from $cursos where id_alumno='$id_alumno' limit 1";
				$datcurs = viewsql($sql);
				foreach($datcurs as $regcurs)
				{
					$id_areaper = $regcurs->id_areaper;
					if ($regcurs->pierde > 0)
						$areaper = "PERDIDA DE A�O";
					if ($regmatr->estado=="RETIRADA")
						$fecha = $regcurs->fchretiro;
				}
				if ($areaper=="" and $id_areaper > 0)
				{
					$sql="select area from areas where id_area = '$id_areaper' limit 1";
					$datarea=viewsql($sql);
					foreach($datarea as $regarea)
					{$areaper=$regarea->area;}
				}
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center"><a class="link_list" href="alumnos_matriculas.php?op=editar&id_matricula=<?php echo $id_matricula;?>&id_alumno=<?php echo $id_alumno;?>" title="ID matricula"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"><?php echo $regmatr->ano;?></a></td>
					<td align="center"><?php echo $regmatr->grado;?></td>
					<td align="center"><?php echo $regmatr->curso;?></td>
					<td><?php echo $regmatr->estado;?></td>
					<td><?php echo fecha_texto($fecha);?></td>
					<td><?php echo $regmatr->folio;?></td>
					<td><?php if ($regmatr->beca > 0) echo "% ".$regmatr->beca;?></td>
					<td><?php echo $areaper;?></td>
					<td><?php echo $regmatr->detalles;?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivmat"] >= 3) { ?>
			<a href="alumnos_matriculas.php?op=editar&id_alumno=<?php echo $id_alumno;?>"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</table>
<?php } if ($op == "editar") {
$datmatricula = datos_matricula($id_matricula);
$sql="SELECT ano, interes FROM anoslect ORDER BY ano desc";
$datanol=viewsql($sql);
$pos = strpos($gradocurso,"*");
$curso = substr($gradocurso,$pos+1);
$grado = substr($gradocurso,0,$pos);
if ($id_matricula > 0)
{
	$titulo = "Matr�cula ID ".$id_matricula;
	$ano = $datmatricula["ano"];
	if ($gradocurso!="")
	{
		$datmatricula["grado"] = $grado;
		$datmatricula["curso"] = $curso;
	}
	$grado = $datmatricula["grado"];
	$curso = $datmatricula["curso"];
}
else
{
	$titulo = "Nueva Matr�cula";
	$datmatricula["grado"] = $grado;
	$datmatricula["curso"] = $curso;
        foreach($datanol as $regano)
        {
            $datmatricula["interes"] = $regano->interes;
        }
}
$totpagos = 0;
$gradovalmat = 0;
$gradovalpen = 0;
if ($grado!="")
{
	$sql="SELECT matricula, pension FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
	$datval=viewsql($sql);
	foreach($datval as $regval)
	{
		$gradovalmat = $regval->matricula;
		$gradovalpen = $regval->pension;
	}
}
if ($datmatricula["matricula"] <= 0)
	$datmatricula["matricula"] = $gradovalmat;
//cargar ajustes pensiones
$pagosaju = "y".$ano."pagosaju";
$sql="SELECT mes, valor, detalles FROM $pagosaju WHERE id_alumno = '$id_alumno' AND tipo = 'P'";
$dataju = viewsql($sql);
foreach($dataju as $reg)
{
    $mes = $reg->mes;
    if (intval($reg->mes) <= 9)
    {
	$nomval = "pension0".$mes;
	$nomtxt = "coment0".$mes;
    }
    else
    {
	$nomval = "pension".$mes;
	$nomtxt = "coment".$mes;
    }
    $datmatricula[$nomval] = intval($reg->valor);
    $datmatricula[$nomtxt] = $reg->detalles;
}
//Datos de Cursos areas perdidas
$inactivar = "";
$pierde = 0;
$id_areaper = 0;
$promedio1 = "";
$puesto1 = "";
$promedio2 = "";
$puesto2 = "";
$promedio3 = "";
$puesto3 = "";
$promedio4 = "";
$puesto4 = "";
$promedio = "";
$puesto = "";
$notas = 0;
$id_areadeb = 0;
$id_areaper = 0;
$fchretiro="";
$promedio = array();
$puesto = array();
for($i=1; $i<=5 ;$i++)
{
	$promedio[$i] = "";
	$puesto[$i] = "";
}
$cursos = "y".$ano."cursos";
$sql="SELECT * FROM $cursos WHERE id_alumno = '$id_alumno' limit 1";
$datcurs=viewsql($sql);
foreach($datcurs as $regcurs)
{
	$inactivar = $regcurs->inactivar;
	$fchretiro = fec_dma($regcurs->fchretiro);
	if (substr($fchretiro,0,2)=="00")
		$fchretiro="";
	$id_areadeb = intval($regcurs->id_areadeb);
	$id_areaper = intval($regcurs->id_areaper);
	$pierde = intval($regcurs->pierde);
	$promedio[1] = $regcurs->promedio1;
	$puesto[1] = $regcurs->puesto1;
	$promedio[2] = $regcurs->promedio2;
	$puesto[2] = $regcurs->puesto2;
	$promedio[3] = $regcurs->promedio3;
	$puesto[3] = $regcurs->puesto3;
	$promedio[4] = $regcurs->promedio4;
	$puesto[4] = $regcurs->puesto4;
	$promedio[5] = $regcurs->promedio;
	$puesto[5] = $regcurs->puesto;
}
$file = "y".$ano."notas";
$sql="SELECT count(*) as notas FROM $file WHERE id_alumno = '$id_alumno' limit 1";
$datnota=viewsql($sql);
foreach($datnota as $regnota)
{$notas = $regnota->notas;}
$readonly = "";
if ($_SESSION["nivmat"] <= 3)
 $readonly = "readonly";
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_alumnos.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabar_matricula'>
<input type='hidden' name='id_matricula' value='<?php echo $id_matricula;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type='hidden' name='tipoedu' value='<?php echo $tipoedu;?>'>
<input type='hidden' name='oldcurso' value='<?php echo $datmatricula["curso"];?>'>
<input type='hidden' name='oldinactivar' value='<?php echo $inactivar;?>'>
<input type='hidden' name='notas' value='<?php echo $notas;?>'>
<input type='hidden' name='gradovalpen' value='<?php echo $gradovalpen;?>'>
<input type='hidden' name='oldtotpagos' value='<?php echo $totpagos;?>'>
	<tr height="15">
		<td width="20"></td>
		<td></td>
		<td width="10"></td>
		<td></td>
		<td width="20"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr valign="top">
				<td>
					<fieldset>
					<legend class="titulo_campos"><?php echo $titulo;?></legend>
					<table border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td width="85">A�o Lectivo <font color="red">*</font></td>
							<td width="60">
								<?php if ($id_matricula <= 0) { ?>
								<select name="ano" style="width: 55px;" onChange="javascript:seleccionar();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php
								foreach($datanol as $reganol)
								{
									$selano = "";
									if ($reganol->ano==$ano)
										$selano = "selected";
									echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
								}
								?>
								</select>
								<?php } else { ?>
								<input type="text" name="ano" style="width: 55px;" value="<?php echo $datmatricula["ano"];?>" readonly="readonly" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php } ?>
							</td>
							<td>
								Estado <font color="red">*</font>
								<select name="estado" style="width: 108px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php
								$sql="SELECT * FROM listas WHERE listip = 'MATRICULAS' order by id";
								$lista=viewsql($sql);
								foreach($lista as $reglis)
								{
									if ($datmatricula["estado"] == $reglis->lisnom)
										echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
									else
										echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
								}
								?>																
								</select>
							</td>
						</tr>				
						<tr>
							<td>Grado y Curso <font color="red">*</font></td>
							<td colspan="2">
								<select name="gradocurso" style="width: 215px;" onChange="javascript:seleccionar();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php
								$sql = "SELECT grado, gradonom FROM grados order by id_grado";
								$datgrad = viewsql($sql);
								foreach($datgrad as $reggrad)
								{
									$lgrado = $reggrad->grado;
									$lcurso = "";
									if ($datmatricula["grado"]==$lgrado and $datmatricula["curso"]==$lcurso)
										echo "<option value='$lgrado*$lcurso' selected>GRADO $reggrad->gradonom</option>";
									else
										echo "<option value='$lgrado*$lcurso'>GRADO $reggrad->gradonom</option>";
									$sql = "SELECT grado, curso FROM cursos WHERE ano = '$ano' AND grado = '$lgrado' order by curso";
									$datcurs = viewsql($sql);
									foreach($datcurs as $regcurs)
									{
										if ($datmatricula["grado"]==$regcurs->grado and $datmatricula["curso"]==$regcurs->curso)
										echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
										else
										echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
									}
								}
								?>																
								</select>
							</td>
						</tr>
						<tr>
							<td>Matr�cula $</td>
							<td>
								<input type="text" name="matricula" style="width: 55px; text-align: right; font-size: 12px;" value="<?php echo $datmatricula["matricula"];?>" <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;">
							</td>
							<td>
                                                                Interes%
                                                                <input type="text" name="interes" style="width: 20px; text-align: right; font-size: 12px;" value="<?php echo $datmatricula["interes"];?>" <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								&nbsp; Beca % &nbsp;
								<?php if ($readonly=="") { ?>
								<input type="text" name="beca" style="width: 30px; text-align: right; font-size: 12px;" value="<?php echo $datmatricula["beca"];?>" maxlength="3" onBlur="javascript:LiquidarBeca();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php } else { ?>
								<input type="text" name="beca" style="width: 30px; text-align: right; font-size: 12px;" value="<?php echo $datmatricula["beca"];?>" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<?php } ?>
							</td>
						</tr>				
					</table>
					</fieldset>
				</td>
				<td width="10"></td>
				<td>
					<fieldset>
					<legend class="titulo_campos">Fechas</legend>
					<table border="0" cellpadding="1" cellspacing="1">
						<tr>
							<td>Matricula</td>
							<td><input type="text" name="fecha" id="fecha" value="<?php echo $datmatricula["fecha"];?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fecha, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
						</tr>
						<tr>
							<td>Retiro &nbsp; </td>
							<td><input type="text" name="fchretiro" id="fchretiro" value="<?php echo $datmatricula["fchretiro"];?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchretiro, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
						</tr>
						<tr>
							<td>Folio N�</td>
							<td><input type="text" name="folio" style="width: 65px;" value="<?php echo $datmatricula["folio"];?>" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
						</tr>				
					</table>
					</fieldset>
				</td>
			</tr>
			</table>		
			<fieldset>
			<legend class="titulo_campos">Responsables</legend>
			<table border="0" cellpadding="1" cellspacing="1">
				<tr>
					<td>Acudiente Matr�cula <font color="red">*</font></td>
					<td>
						<select name="id_respmatri" style="width: 320px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						$sql="select id_familiar, tipo, apellidos, nombres from familiares where id_alumno = '$id_alumno' order by apellidos, nombres";
						$datfam=viewsql($sql);
						foreach($datfam as $regfam)
						{
							if (intval($datmatricula["id_respmatri"]) == intval($regfam->id_familiar))
								echo "<option value='$regfam->id_familiar' selected>$regfam->tipo - $regfam->apellidos $regfam->nombres</option>";
							else
								echo "<option value='$regfam->id_familiar'>$regfam->tipo - $regfam->apellidos $regfam->nombres</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr>
					<td>Pagos y Pagar�s<font color="red">*</font></td>
					<td>
						<select name="id_resppagos" style="width: 320px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						foreach($datfam as $regfam)
						{
							if (intval($datmatricula["id_resppagos"]) == intval($regfam->id_familiar))
								echo "<option value='$regfam->id_familiar' selected>$regfam->tipo - $regfam->apellidos $regfam->nombres</option>";
							else
								echo "<option value='$regfam->id_familiar'>$regfam->tipo - $regfam->apellidos $regfam->nombres</option>";
						}
						?>																
						</select>
					</td>
				</tr>
			</table>
			</fieldset>
			<br>
                        <fieldset>
                        <legend class="titulo_campos">Pensi�n $ <?php echo number_format($gradovalpen,0);?></legend>
                        
			<table border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="2">
						<table border="0" cellpadding="2" cellspacing="1" bgcolor="#C0C0C0" align="center">
							<tr height="16"><th width="70" class="titulo_campos"></th><th class="titulo_campos">Ajuste $</th><th class="titulo_campos">Concepto del Ajuste</th></tr>
							<?php
							for($i=2; $i<=11 ;$i++)
							{
								if ($i <= 9)
								{
									$nomval = "pension0".$i;
									$nomtex = "coment0".$i;
									$mes = "0".$i;
								}
								else
								{
									$nomval = "pension".$i;
									$nomtex = "coment".$i;
									$mes = $i;
								}
								$mes = nombre_mes($mes);
								?>
								<tr height="20" bgcolor="#FFFFFF" align="center">
									<th><?php echo $mes;?></th>
									<td><input type="text" name="<?php echo $nomval;?>" style="width: 50px; text-align: right; font-size: 12px;" value="<?php echo $datmatricula[$nomval];?>" <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
									<td><input type="text" name="<?php echo $nomtex;?>" style="width: 330px; text-transform:uppercase;" value="<?php echo $datmatricula[$nomtex];?>" <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
								</tr>
								<?php
							}
							?>
						</table>
					</td>
				</tr>
			</table>
                        </fieldset>
		</td>
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Promedios</legend>
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="16"><th>Periodo</th><th>Promedio</th><th>Puesto</th><th class="titulo_campos">Inactivo Notas</th></tr>
				<?php
				$sql="SELECT periodo, perplan, pertipo, fchfinal FROM periodos WHERE ano = '".$_SESSION["anoaca"]."' order by notanum";
				$datperi=viewsql($sql);
				foreach($datperi as $regperi)
				{
					if ($regperi->pertipo=='P')
					{
					$periodo = $regperi->periodo;
					$chkname = "chkp".$periodo;
					?>
					<tr height="18" bgcolor="#FFFFFF" align="center">
						<th><?php echo $regperi->perplan;?></th>
						<td><?php echo $promedio[$periodo];?></td>
						<td><?php echo $puesto[$periodo];?></td>
						<td>
							<?php
							if (strpos($inactivar,$periodo) > 0)
								echo "<input type='checkbox' name='$chkname' checked>";
							else
								echo "<input type='checkbox' name='$chkname'>";
							?>
						</td>
					</tr>
					<?php
					}
					$fchfinal = $regperi->fchfinal;
				}
				?>
				<tr height="18" bgcolor="#FFFFFF" align="center">
					<th>FINAL</th>
					<td><?php echo $promedio[5];?></td>
					<td><?php echo $puesto[5];?></td>
					<td><input type='hidden' name='fchfinal' value='<?php echo $fchfinal;?>'></td>
				</tr>
			</table>
			</fieldset>
			<br>
			<fieldset>
			<legend class="titulo_campos">Cierre Acad�mico</legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td colspan="2" height="1"></td></tr>
				<tr height="20">
					<td>Area pendiente <?php echo $ano-1;?>&nbsp;</td>
					<td>
						<?php
						$areadeb = "";
						$sql="select area from areas where id_area = '$id_areadeb' limit 1";
						$datarea=viewsql($sql);
						foreach($datarea as $regarea)
						{$areadeb=$regarea->area;}
						?>
						<input type='hidden' name='id_areadeb' value='<?php echo $id_areadeb;?>'>
						<input type="text" name="areadeb" style="width: 280px;" value="<?php echo $areadeb;?>" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr height="20">
					<td>Area perdida <?php echo $ano;?></td>
					<td>
						<select name="id_areaper" style="width: 280px;"onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						$sql="select id_area, area from areas where grado = '".$datmatricula["grado"]."' order by area";
						$datcateg=viewsql($sql);
						foreach($datcateg as $regcateg)
						{
							if ($id_areaper==intval($regcateg->id_area))
								echo "<option value='$regcateg->id_area' selected>$regcateg->area</option>";
							else
								echo "<option value='$regcateg->id_area'>$regcateg->area</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr height="20">
					<td>Pierde a�o <?php echo $ano;?></td>
					<td>
						<select name="pierde" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php if ($pierde <= 0) { ?>
						<option value="0" selected>NO</option>
						<option value="1">SI</option>
						<?php } else { ?>
						<option value="0">NO</option>
						<option value="1" selected>SI</option>
						<?php } ?>
						</select>
						&nbsp;&nbsp;&nbsp;
						<?php if (intval($datmatricula["renovar"]) > 0) { ?>
						Renovar Matr�cula: SI<input type="radio" name="renovar" value="1" checked> &nbsp;NO <input type="radio" name="renovar" value="0">
						<?php } else { ?>
						Renovar Matr�cula: SI<input type="radio" name="renovar" value="1"> &nbsp;NO <input type="radio" name="renovar" value="0" checked>
						<?php } ?>
					</td>
				</tr>
			</table>
			</fieldset>
			<br>
			<fieldset>
			<legend class="titulo_campos">Observaciones</legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
					<textarea name='detalles' rows="10" cols="60" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datmatricula["detalles"];?></textarea>
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr height="36" bgcolor="#CCCCCC" background="images/backsilver.gif">
		<td></td>
		<td>
			<?php if ($id_matricula > 0) { ?>
			ACTUALIZADO <?php echo substr($datmatricula["fchactual"],0,16);?> <?php echo $datmatricula["usuactual"];?>
			<?php } ?>
		</td>
		<td colspan="3" align="right" valign="middle">
			<?php if ($_SESSION["nivmat"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="5" alt="Grabar" align="absmiddle">
			<a href="javascript:;" onClick="imprimir();"><img src="images/btn_imprimir.png" border="0" width="70" height="24" alt="Imprimir" hspace="5" align="absmiddle"></a>
			<?php } if ($id_matricula > 0 and $_SESSION["nivalu"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarMatricula();"><img src="images/btn_eliminar.png" border="0" width="70" height="24" alt="Eliminar" hspace="5" align="absmiddle"></a>
			<?php } ?>
			<a href="alumnos_matriculas.php?id_alumno=<?php echo $id_alumno;?>&id_matricula=<?php echo $id_matricula;?>"><img src="images/btn_regresar.png" border="0" width="70" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
