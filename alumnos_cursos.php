<?php
/***********************************************************************************************
* @file        : alumnos_cursos.php                                                            *
* @brief       : Crear y actualizar cursos por grado y a�o acad�dmico                          *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 24-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 24-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
$ano = $_SESSION["anoaca"];
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$op = isset($_GET['op']) ? $_GET['op'] : null ;
connect();
//promediar notas periodo o examen seleccionado
if ($op=="promediar_curso")
{
	include_once("database/grabar_notas.php");
	include_once("database/datos_notas.php");
	$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
	$periodo = isset($_GET['periodo']) ? $_GET['periodo'] : null ;
	$pertipo = substr($periodo,0,1);
	$periodo = substr($periodo,1,1);
	$titulo = "PERIODO";
	$id_alumno = 0;
	if ($pertipo == "P") //periodo de notas
	{
		if ($periodo <= 4)
			promediar_notas($ano, $periodo, $grado, $curso, $id_alumno);
		else
			promediar_anolect($ano, $grado, $curso, $id_alumno);
	}
	else //examenes
	{
		$titulo = "EXAMEN";
		promediar_examen($ano, $periodo, $grado, $curso, $id_alumno);
	}
	echo"<script>alert('Curso $curso retotalizado, A�O $ano $titulo $periodo');</script>";
}
//lista de grados
$sql="SELECT grado, gradonom FROM grados order by id_grado";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{
	if ($grado == "")
	{
		$grado = $reggrad->grado;
		break;
	}
}
//lista de cursos
$sql = "SELECT * FROM cursos where ano = '$ano' and grado = '$grado' order by curso";
$datcurs = viewsql($sql);
//lista de profesores con contrato para el grado
$sql="SELECT profesorescont.id_profesor, profesores.apellidos, profesores.nombres FROM profesores, profesorescont WHERE ".
	"profesorescont.ano = '$ano' AND grados like '% $grado %' ".
	"AND profesorescont.id_profesor = profesores.id_profesor ORDER BY profesores.apellidos, profesores.nombres";
$datprof=viewsql($sql);
//periodos
$cierre="";
$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
$datperi = viewsql($sql);
$numperi = count($datperi);
//lista estados periodos
$sql="SELECT * FROM listas WHERE listip = 'PERIODOS' order by id";
$lista=viewsql($sql);
$planillas = "y".$ano."planillas";
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	var grado = document.form0.grado.value;
	var theURL = 'alumnos_cursos.php?grado='+grado;
	window.location.href = theURL;
}
function totalizar(grado, curso, periodo)
{
	var tipo = periodo.substr(0,1);
	var nume = periodo.substr(1,1);
	if (tipo=='P')
		tipo = 'PERIODO';
	else
		tipo = 'EXAMEN';
	if (nume==5)
		nume = '5 (CIERRE)';
	ok=confirm('Esta opci�n totaliza nuevamente las notas del curso '+curso+' '+tipo+' '+nume+', desea continuar?');
	if(ok)
	{
		window.location.href='alumnos_cursos.php?op=promediar_curso&grado='+grado+'&curso='+curso+'&periodo='+periodo;
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
<form action="" method="post" name="form0">
	<tr height="50">
		<td class="titulo">
			<img src='images/cursos.jpg' width="40" height="40" border="0" align="absmiddle"> CURSOS 
			<select name="grado" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
			<?php
			foreach($datgrad as $reggrad)
			{
				if ($grado == $reggrad->grado)
					echo "<option value='$reggrad->grado' selected>GRADO $reggrad->gradonom</option>";
				else
					echo "<option value='$reggrad->grado'>GRADO $reggrad->gradonom</option>";
			}
			?>																
			</select>
		</td>
		<td class="subtitulo" align="right">A�O <?php echo $ano;?></td>
	</tr>
</form>
</table>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_alumnos.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarcursos'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='grado' value='<?php echo $grado;?>'>
	<tr height="10">
		<td width="20"></td>
		<td></td>
		<td width="20"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="40">Curso</th>
					<th width="20">Tot</th>
					<th>Director</th>
					<?php
					foreach($datperi as $regperi)
					{echo "<th width='30'>$regperi->perplan</th>";}					
					?>
					<th width="30">Final</th>
					<th width="30"></th>
					<th width="20"></th>
				</tr>
				<?php
				//listar cursos
				$num = 0;
				$curper = array();
				foreach($datcurs as $regcurs)
				{
				$num = $num + 1;
				$id_curso = $regcurs->id_curso;
				$curso = $regcurs->curso;
				$id_director = $regcurs->id_director;
				$name = "curso".$num;
				$dire = "direc".$num;
				for($i=0; $i<=$numperi ;$i++)
				{$curper[$i] = "";}
				$curper[0] = "CERRADO";
				$sql = "select pertipo, periodo, estado from $planillas where id_asignatura = '0' and grado='$grado' and curso='$curso' order by id_planilla";
				$datplan=viewsql($sql);
				foreach($datplan as $regplan)
				{
					if ($regplan->pertipo=="P" and $regplan->periodo=="1")
						$curper[1] = $regplan->estado;
					if ($regplan->pertipo=="P" and $regplan->periodo=="2")
						$curper[2] = $regplan->estado;
					if ($regplan->pertipo=="E" and $regplan->periodo=="1")
						$curper[3] = $regplan->estado;
					if ($regplan->pertipo=="P" and $regplan->periodo=="3")
						$curper[4] = $regplan->estado;
					if ($regplan->pertipo=="P" and $regplan->periodo=="4")
						$curper[5] = $regplan->estado;
					if ($regplan->pertipo=="E" and $regplan->periodo=="2")
						$curper[6] = $regplan->estado;
					if ($regplan->pertipo=="E" and $regplan->periodo=="3")
						$curper[7] = $regplan->estado;
				}
				?>		
				<tr height="25" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center"><input type="text" name="<?php echo $name;?>" value="<?php echo $regcurs->curso;?>" style="width: 35px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center"><?php echo $regcurs->alumtot;?></td>
					<td align="center">
						<select name="<?php echo $dire;?>" style="width: 220px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						foreach($datprof as $regprof)
						{
							if ($id_director == $regprof->id_profesor)
								echo "<option value='$regprof->id_profesor' selected>$regprof->apellidos $regprof->nombres </option>";
							else
								echo "<option value='$regprof->id_profesor'>$regprof->apellidos $regprof->nombres</option>";
						}
						?>																
						</select>
					</td>
					<?php
					foreach($datperi as $regperi)
					{
						$notanum = $regperi->notanum;
						$pertipo = $regperi->pertipo;
						$periodo = $regperi->periodo;
						$pernomb = $regperi->pernomb;
						$perplan = $regperi->perplan;
						$peri = "peri".$num.$notanum;
						$ante = $notanum-1;
						if ($pertipo=="P")
							$cierre = $regperi->estado;
						?>
						<td align="center" valign="middle">
						<?php if ($curper[$ante] == "CERRADO" and $curper[$notanum] != "CERRADO") { ?>  
						<select name="<?php echo $peri;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php
						foreach($lista as $reglis)
						{
							if ($curper[$notanum] == $reglis->lisnom)
								echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
							else
								echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
						}
						?>																
						</select>
						<a class="link_list" href="reports/rep_planillaspen.php?ano=<?php echo $ano;?>&pertipo=<?php echo $pertipo;?>&periodo=<?php echo $periodo;?>&pernomb=<?php echo $pernomb;?>&grado=<?php echo $grado;?>&curso=<?php echo $regcurs->curso;?>" title="Planillas Pendientes..." onclick="newWindow(this.href, '', 1024, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src="images/ico_alerta.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"></a>
						<?php } ?>
						<?php if ($curper[$notanum] == "CERRADO") { ?>
						Cerrado
						<?php if ($_SESSION["nivasi"] >= 4) { ?>
						<a href="javascript:;" onClick="totalizar('<?php echo $grado;?>','<?php echo $curso;?>','<?php echo $pertipo.$periodo;?>');" title="Totalizar Notas y Puestos..."><img src="images/sumar.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"></a>
						<?php } }?>
						</td>
						<?php
					}				
					?>
					<td align="center">
						<?php if ($cierre=="CERRADO" and $_SESSION["nivasi"] >= 4) { ?>
						 <a href="javascript:;" onClick="totalizar('<?php echo $grado;?>','<?php echo $curso;?>','P5');" title="Definitivas y Cierre..."><u>Cerrar</u></a>
						<?php } ?>
					</td>
					<td align="center"><a class="link_list" href="horarios.php?ano=<?php echo $ano;?>&grado=<?php echo $grado;?>&curso=<?php echo $regcurs->curso;?>" title="Horario" onclick="newWindow(this.href, '', 1024, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src="images/horariosm.png" width="20" height="20" border="0" hspace="0" align="absmiddle"></a></td>
					<td align="center"><?php if ($regcurs->alumtot <= 0 and $_SESSION["nivasi"] >= 4) { ?> <a href="database/grabar_alumnos.php?op=borrarcurso&ano=<?php echo $ano;?>&grado=<?php echo $grado;?>&id_curso=<?php echo $id_curso;?>" title="Eliminar Curso..."><img src="images/b_drop.png" width="16" height="16" border="0" hspace="0" align="absmiddle"></a><?php } ?></td>
				</tr>
				<?php
				}
				if ($num < 10 and $ano >= $_SESSION["anoaca"])
				{
				$ini = $num+1;
				for($i=$ini; $i <= 10 ; $i++)
				{
				$name = "curso".$i;
				?>
				<tr height="25" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center"><input type="text" name="<?php echo $name;?>" style="width: 35px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center">0</td>
					<td></td>
					<?php
					foreach($datperi as $regperi)
					{echo "<td></td>";}					
					?>
					<td></td>
					<td></td>
					<td></td>
				</tr>
				<?php
				} } ?>
			</table>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 4) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<div id="carga" style="display:none">
    <img src="images/proceso.gif" />
</div>
</body>
</html>
<?php disconnect();?>
