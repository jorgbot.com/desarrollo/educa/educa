<?php
/***********************************************************************************************
* @file        : asignaturas_edit.php                                                          *
* @brief       : Crear y actualizar asignatura por grado                                       *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
if ($ano <= 0 )
	$ano = $_SESSION["anoaca"];
connect();
$notaevi = 0;
$sql="SELECT notaevi FROM anoslect WHERE ano = '$ano' limit 1";
$datano=viewsql($sql);
foreach($datano as $regano)
{
	$notaevi = $regano->notaevi;
}
$sql="SELECT * FROM grados WHERE grado = '$grado' limit 1";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{
	$gradonom = "GRADO ".$reggrad->gradonom;
	$gradocolor = $reggrad->gradocolor;
	$gradofont = $reggrad->gradofont;
	$nivel = $reggrad->nivel;
}
$titulo = "Nueva Asignatura";
$titact = "Actividades por Logro";
$datasignatura = datos_asignatura($id_asignatura);
$evidemin = intval($datasignatura["evidemin"]);
$activmin = intval($datasignatura["activmin"]);
$porevi = array();
$por = array();
for($i=1; $i<=10 ;$i++)
{
	$porevi[$i] = "";
	$por[$i] = "";
}
$pos = 0;
$totalevi = 0;
for($i=1; $i<=$evidemin ;$i++)
{
	$porevi[$i] = substr($datasignatura["evidepor"],$pos,3);
	$pos = $pos + 3;
	$totalevi = $totalevi + intval($porevi[$i]);
}
$pos = 0;
$total = 0;
for($i=1; $i<=$activmin ;$i++)
{
	$por[$i] = substr($datasignatura["activpor"],$pos,3);
	$pos = $pos + 3;
	$total = $total + intval($por[$i]);
}
$chkbol = "";
$chknot = "";
$chkper = "";
$chkpro = "";
$chkobs = "";
if ($datasignatura["boletin"] > 0)
	$chkbol = "checked";
if ($datasignatura["notas"] > 0)
	$chknot = "checked";
if ($datasignatura["perdida"] > 0)
	$chkper = "checked";
if ($datasignatura["promedio"] > 0)
	$chkpro = "checked";
if ($datasignatura["observacion"] > 0)
	$chkobs = "checked";
if ($id_asignatura > 0)
	$titulo = "Editar Asignatura";
$sql="SELECT * FROM areas where grado = '$grado' order by area";
$datarea = viewsql($sql);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script type="text/javascript" src="js/dynamicoptionlist.js"></script>
<script language='JavaScript' type='text/JavaScript' src='js/actividades.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.id_area.value == '')
	{alert('Falta Seleccionar Area...');document.form1.id_area.focus(); return false;}
	if(document.form1.codigo.value <= 0)
	{alert('Falta asignar C�digo...');document.form1.codigo.focus(); return false;}
	if(document.form1.evidemin.value > 0 && document.form1.evideprom.value <= 0 && document.form1.totalevi.value != 100)
	{alert('Porcentajes de evidencias incorrectos, no suman 100...'); return false;}
	if(document.form1.activmin.value > 0 && document.form1.activprom.value <= 0 && document.form1.total.value != 100)
	{alert('Porcentajes de actividades incorrectos, no suman 100...'); return false;}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar esta Asignatura?");
	if(ok)
	{
		document.form1.op.value='borrarasignatura';
		document.form1.submit();
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/asignaturas.png' width="40" height="40" border="0" align="absmiddle"> Asignaturas Grado <?php echo $grado;?></td>
		<td class="subtitulo" align="right">A�O <?php echo $ano;?></td>
	</tr>
</table>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarasignatura'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='grado' value='<?php echo $grado;?>'>
<input type='hidden' name='gradocolor' value='<?php echo $gradocolor;?>'>
<input type='hidden' name='gradofont' value='<?php echo $gradofont;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
	<tr height="10">
		<td width="10"></td>
		<td></td>
		<td width="10"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos"><?php echo $titulo;?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td height="5"></td></tr>
				<tr height="25">
					<td width="80">Area  <font color="red">*</font></td>
					<td>
						<SELECT NAME="id_area" STYLE="width: 480px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<OPTION VALUE="" selected></OPTION>
						<?php 
						foreach($datarea as $regarea)
						{
						if ($regarea->id_area == $datasignatura["id_area"])
						{
						?>
						<OPTION VALUE="<?php echo $regarea->id_area;?>" selected><?php echo $regarea->area;?></OPTION>
						<?php } else { ?>
						<OPTION VALUE="<?php echo $regarea->id_area;?>"><?php echo $regarea->area;?></OPTION>
						<?php } } ?>
						</SELECT>
					</td>
				</tr>
				<tr height="25">
					<td>Asignatura </td>
					<td>
						<input type="text" name="asignatura" value="<?php echo $datasignatura["asignatura"];?>" style="width: 480px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr>
					<td colspan="2">
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="24" align="left">
											<td width="80">Nota Fija</td>
											<td><input type="text" name="notaQ" style="width: 65px; text-transform:uppercase;" value="<?php echo $datasignatura["notaQ"];?>" maxlength="10" onKeyDown="if(event.keyCode==13) event.keyCode=9;"> &nbsp; (Nota Bolet�n)</td>
										</tr>
										<tr height="24" align="left">
											<td>C�digo  <font color="red">*</font></td>
											<td><input type="text" name="codigo" style="width: 30px;" value="<?php echo $datasignatura["codigo"];?>" maxlength="4" onKeyDown="if(event.keyCode==13) event.keyCode=9;"> &nbsp; (Ubicaci�n Bolet�n)</td>
										</tr>
										<tr>
											<td height="24">Intensidad</td>
											<td><input type="text" name="intensidad" style="width: 30px;" value="<?php echo $datasignatura["intensidad"];?>" maxlength="2" onKeyDown="if(event.keyCode==13) event.keyCode=9;"> &nbsp; (Horas Semanales)</td>
										</tr>
										<tr>
											<td height="24">Porcentaje %</td>
											<td><input type="text" name="porcentaje" style="width: 30px;" value="<?php echo $datasignatura["porcentaje"];?>" maxlength="3" onKeyDown="if(event.keyCode==13) event.keyCode=9;"> &nbsp; (Vac�o = Promedio)</td>
										</tr>
									</table>
								</td>
								<td width="15"></td>
								<td>
									<fieldset>
									<legend class="titulo_campos">Propiedades Asignatura</legend>
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr><td height="25"><input type="checkbox" name="chkbol" <?php echo $chkbol;?>> Imprimir en Bolet�n</td></tr>
										<tr><td height="25"><input type="checkbox" name="chknot" <?php echo $chknot;?>> Requiere Nota en Planilla</td></tr>
										<tr><td height="25"><input type="checkbox" name="chkobs" <?php echo $chkobs;?>> Requiere Observaci�n</td></tr>
									</table>
									</fieldset>
								</td>
								<td width="10"></td>
                                                                <td>
									<fieldset>
									<legend class="titulo_campos">Propiedades Area</legend>
									<table border="0" cellpadding="0" cellspacing="0" width="100%">
										<tr><td height="25"><input type="checkbox" name="chkper" <?php echo $chkper;?>> Incidencia P�rdida de a�o</td></tr>
										<tr><td height="25"><input type="checkbox" name="chkpro" <?php echo $chkpro;?>> Acumula Promedio del Alumno</td></tr>
                                                                                <tr><td height="25"></td></tr>
									</table>
									</fieldset>
                                                                </td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</fieldset>
			<fieldset>
                        <legend class="titulo_campos">Configuraci&oacute;n Logros</legend>
			<table border="0" cellpadding="0" cellspacing="0">
                            <?php if ($notaevi > 0) {
                            $titact = "Actividades por Evidencia";
                            ?>
                            <tr>
                                <td height="30">Evidencias por Logro &nbsp;</td>
                                <td><input type="text" name="evidemin" style="width: 25px;" value="<?php echo $datasignatura["evidemin"];?>" maxlength="2" onBlur="javascript:checkEst(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
                                <td align="right">&nbsp;Promediar Evidencias</td>
				<td>
                                    <?php if ($datasignatura["evideprom"] <= 0) { ?>
                                    <input type="radio" name="evideprom" value="1">
                                    <?php } else { ?>
                                    <input type="radio" name="evideprom" value="1" checked>
                                    <?php } ?>
                                    <?php if ($datasignatura["evideprom"] <= 0) { ?>
                                    Porcentajes <input type="radio" name="evideprom" value="0" checked>
                                    <?php } else { ?>
                                    Porcentajes <input type="radio" name="evideprom" value="0">
                                    <?php } ?>
                                    &nbsp;
				</td>
				<td align="left">
                                    <table border="0" cellpadding="0" cellspacing="2">
					<tr>
                                            <td>
						%
                                            </td>
                                            <?php
                                            for($i=1; $i<=10 ;$i++)
                                            {
						$namediv = "divevi".$i;
						$namepor = "porevi".$i;
						$displayevi = "none";
						if ($i <= $evidemin)
                                                    $displayevi = "block";
						?>
						<td>
						<div id="<?php echo $namediv;?>" style="display:<?php echo $displayevi;?>;">
						<input type="text" name="<?php echo $namepor;?>"  style="width: 25px;" value="<?php echo $porevi[$i];?>" maxlength="3" onBlur="javascript:checkPorEvi(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						</div>
						</td>
						<?php
                                            }
                                            ?>
                                            <td><input type="hidden" name="totalevi" value="<?php echo $totalevi;?>"></td>
					</tr>
                                    </table>
				</td>
                            </tr>
                            <?php } else { ?>
                            <input type="hidden" name="evidemin" value="0">
                            <input type="hidden" name="evideprom" value="1">
                            <input type="hidden" name="totalevi" value="0">
                            <?php } ?>
                            <tr>
                                <td height="30"><?php echo $titact;?> &nbsp;</td>
                                <td><input type="text" name="activmin" style="width: 25px;" value="<?php echo $datasignatura["activmin"];?>" maxlength="2" onBlur="javascript:checkEst(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
                                <td align="right">&nbsp;Promediar Actividades</td>
				<td>
                                    <?php if ($datasignatura["activprom"] <= 0) { ?>
                                    <input type="radio" name="activprom" value="1">
                                    <?php } else { ?>
                                    <input type="radio" name="activprom" value="1" checked>
                                    <?php } ?>
                                    <?php if ($datasignatura["activprom"] <= 0) { ?>
                                    Porcentajes <input type="radio" name="activprom" value="0" checked>
                                    <?php } else { ?>
                                    Porcentajes <input type="radio" name="activprom" value="0">
                                    <?php } ?>
                                    &nbsp;
				</td>
				<td align="left">
                                    <table border="0" cellpadding="0" cellspacing="2">
					<tr>
                                            <td>
                                                %
                                            </td>
                                            <?php
                                            for($i=1; $i<=10 ;$i++)
                                            {
						$namediv = "div".$i;
						$namepor = "por".$i;
						$display = "none";
						if ($i <= $activmin)
                                                    $display = "block";
						?>
						<td>
						<div id="<?php echo $namediv;?>" style="display:<?php echo $display;?>;">
						<input type="text" name="<?php echo $namepor;?>"  style="width: 25px;" value="<?php echo $por[$i];?>" maxlength="3" onBlur="javascript:checkPor(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						</div>
						</td>
						<?php
                                            }
                                            ?>
                                            <td><input type="hidden" name="total" value="<?php echo $total;?>"></td>
					</tr>
                                    </table>
				</td>
                            </tr>
                            <tr>
                            </tr>
                        </table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 4) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_asignatura!="" and $ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
