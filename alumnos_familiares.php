<?php
/***********************************************************************************************
* @file        : alumnos_familiares.php                                                        *
* @brief       : Lista y edita familiares de un alumno                                         *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 10-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 16-Sep-2015                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("libs/fechas.php");
include("libs/image_resize.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] :  $_SESSION["anoaca"] ;
$id_alumno = $_GET["id_alumno"];
$id_familiar = isset($_GET['id_familiar']) ? $_GET['id_familiar'] : null ;
$op = isset($_GET['op']) ? $_GET['op'] : "listar" ;
$opcion = "familiares";
connect();
include("includes/menu_titulo.php");
$resppagos = 0;
$respmatri = 0;
$respnotas = 0;
$sql = "select id_resppagos, id_respmatri, id_respnotas FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
$dat = viewsql($sql);
foreach($dat as $reg)
{
	$resppagos = $reg->id_resppagos;
	$respmatri = $reg->id_respmatri;
	$respnotas = $reg->id_respnotas;
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/validInput.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.famiape.value == '')
	{alert('Falta asignar apellidos del familiar...');document.form1.famiape.focus(); return false;}
	if(document.form1.faminom.value == '')
	{alert('Falta asignar nombre del familiar...');document.form1.faminom.focus(); return false;}
	if(document.form1.famidoc.value == '')
	{alert('Falta asignar tipo de documento del familiar...');document.form1.famidoc.focus(); return false;}
	if(document.form1.famiced.value == '')
	{alert('Falta asignar identificaci�n del familiar...');document.form1.famiced.focus(); return false;}
	if(document.form1.famiexp.value == '')
	{alert('Falta lugar expedici�n documento del familiar...');document.form1.famiexp.focus(); return false;}
	if(document.form1.famigen.value == '')
	{alert('Falta G�nero...');document.form1.famigen.focus(); return false;}
	if(document.form1.famitip.value == '')
	{alert('Falta v�nculo con el Alumno...');document.form1.famitip.focus(); return false;}
	if(document.form1.famidir.value == '')
	{alert('Falta asignar direcci�n del familiar...');document.form1.famidir.focus(); return false;}
	if(document.form1.famibar.value == '')
	{alert('Falta asignar barrio del familiar...');document.form1.famibar.focus(); return false;}
	if(document.form1.famiest.value == '')
	{alert('Falta asignar estrato del familiar...');document.form1.famiest.focus(); return false;}
	if(document.form1.famitel.value == '')
	{alert('Falta asignar tel�fono del familiar...');document.form1.famitel.focus(); return false;}
	return true;
}

function borrarFamiliar()
{
	ok=confirm('Esta seguro de eliminar este Familiar ?');
	if(ok)
	{
		document.form1.op.value='borrarfamiliar';
		document.form1.submit();
	}
}
</script>
</head>
<body>
<?php include("includes/menu_alumnos.php");?>
<?php if ($op == "listar") { ?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="420" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th>Apellidos y Nombres</th>
					<th width="60">V�nculo</th>
					<th width="80">Celular</th>
					<th width="80">Tel�fono</th>
					<th width="80">Oficina</th>
					<th width="150">Profesi�n</th>
					<th width="30">A�o</th>
					<th width="50">Vive con<br/>Alumno</th>
					<th width="30">Resp<br/>Matri</th>
					<th width="30">Resp<br/>Pagos</th>
				</tr>
				<?php
				$sql="SELECT * FROM familiares WHERE id_alumno = '$id_alumno' order by apellidos, nombres";
				$datfami = viewsql($sql);
				foreach($datfami as $regfami)
				{
				$id_familiar = $regfami->id_familiar;
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><a class="link_list" href="alumnos_familiares.php?op=editar&id_familiar=<?php echo $id_familiar;?>&id_alumno=<?php echo $id_alumno;?>"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"><?php echo $regfami->apellidos." ".$regfami->nombres;?></a></td>
					<td><?php echo $regfami->tipo;?></td>
					<td><?php echo $regfami->celular;?></td>
					<td><?php echo $regfami->telefono;?></td>
					<td><?php echo $regfami->telefofi;?></td>
					<td><?php echo $regfami->profesion;?></td>
					<td><?php echo $ano;?></td>
					<td align="center"><?php if ($id_familiar==$vive) { ?><img src="images/ok.png" border="0" width="16" height="16" align="absmiddle"/><?php } ?></td>
					<td align="center"><?php if ($id_familiar==$respmatri) { ?><img src="images/ok.png" border="0" width="16" height="16" align="absmiddle"/><?php } ?></td>
					<td align="center"><?php if ($id_familiar==$resppagos) { ?><img src="images/ok.png" border="0" width="16" height="16" align="absmiddle"/><?php } ?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivmat"] >= 3) { ?>
			<a href="alumnos_familiares.php?op=editar&id_alumno=<?php echo $id_alumno;?>"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</table>
<?php } if ($op == "editar") {
$titulo = "Nuevo Registro";
$datfamiliar = datos_familiar($id_familiar);
if ($id_familiar > 0)
	$titulo = "Editar familiar ID ".$id_familiar;
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_alumnos.php" method="post" name="form1" onSubmit="return validar();" enctype="multipart/form-data">
<input type='hidden' name='op' value='grabarfamiliar'>
<input type='hidden' name='id_familiar' value='<?php echo $id_familiar;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type="hidden" name="oldfoto" value="<?php echo $datfamiliar["foto"];?>">
	<tr height="30">
		<td width="10"></td>
		<td></td>
		<td width="10"></td>
		<td></td>
		<td width="10"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Identificaci�n - ID <?php echo $id_familiar;?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td colspan="3" height="4"></td></tr>
				<tr>
					<td width="160" height="160" bgcolor="#E0E7E9" align="center" valign="middle">
						<?php if ($datfamiliar["foto"] == "") { ?>
						<img src="images/foto.gif" width="80" height="80" border="0" align="absmiddle">
						<?php } else {  $height = 150; $width = resize_width($datfamiliar["foto"],150); ?>
						<img src="<?php echo $datfamiliar["foto"];?>" width="<?php echo $width;?>" height="<?php echo $height;?>" border="0" align="absmiddle">
						<?php } ?>
					</td>
					<td width="10"></td>
					<td>
						<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<tr>
								<td width="80">V�nculo <font color="red">*</font></td>
								<td>
									<select name="famitip" style="width: 100px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<option value="" selected></option>
									<?php
									$sql="SELECT lisnom FROM listas WHERE listip = 'FAMILIARES' order by lisnom";
									$lista=viewsql($sql);
									foreach($lista as $reglis)
									{
										if ($datfamiliar["tipo"] == $reglis->lisnom)
											echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
										else
											echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Apellidos <font color="red">*</font></td>
								<td><input type="text" name="famiape" size="25" onKeyUp="javascript:checkText(form1.apellidos);" value="<?php echo $datfamiliar["apellidos"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							<tr>
							<tr>
								<td>Nombres <font color="red">*</font></td>				
								<td><input type="text" name="faminom" size="25" onKeyUp="javascript:checkText(form1.nombres);" value="<?php echo $datfamiliar["nombres"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							<tr>
							<tr>
								<td>Identificaci�n <font color="red">*</font></td>
								<td>
									<select name="famidoc" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<option value="" selected></option>
									<?php
									$sql="SELECT * FROM listas WHERE listip = 'IDENTIFICACION' order by id";
									$listide=viewsql($sql);
									foreach($listide as $reglis)
									{
										if ($datfamiliar["docutip"] == $reglis->lisval)
											echo "<option value='$reglis->lisval' title='$reglis->lisnom' selected>$reglis->lisval</option>";
										else
											echo "<option value='$reglis->lisval' title='$reglis->lisnom'>$reglis->lisval</option>";
									}
									?>																
									</select>
									<input type="text" name="famiced"  style="width: 120px;" maxlength="15" value="<?php echo $datfamiliar["docunum"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr>
								<td>Expedido en <font color="red">*</font></td>
								<td>
									<input type="text" name="famiexp" size="25" value="<?php echo $datfamiliar["docuexp"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr>
								<td>Pa�s origen <font color="red">*</font></td>
								<td>
									<input type="text" name="famipai" size="25" value="<?php echo $datfamiliar["pais"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr>
								<td>Profesi�n</td>
								<td><input type="text" name="famipro" size="25" value="<?php echo $datfamiliar["profesion"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</fieldset>
			<fieldset>
			<legend class="titulo_campos">Responsable <?php echo $ano;?></legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr height="25">
					<td>
						Matr�cula &nbsp;
						<?php if ($respmatri==$id_familiar) { ?>
						<input type="checkbox" name="chkmatri" checked onKeyDown="if(event.keyCode==13) event.keyCode=9;" disabled/>
						<?php } else { ?>
						<input type="checkbox" name="chkmatri" onKeyDown="if(event.keyCode==13) event.keyCode=9;" disabled/>
						<?php } ?>
					</td>
					<td>
						Pagos y Pagar�s&nbsp;
						<?php if ($resppagos==$id_familiar) { ?>
						<input type="checkbox" name="chkpagos" checked onKeyDown="if(event.keyCode==13) event.keyCode=9;" disabled/>
						<?php } else { ?>
						<input type="checkbox" name="chkpagos" onKeyDown="if(event.keyCode==13) event.keyCode=9;" disabled/>
						<?php } ?>
					</td>
					<td>
						Domicilio Alumno &nbsp;
						<?php if ($vive == $id_familiar) { ?>
						<input type="checkbox" name="chkvive" checked onKeyDown="if(event.keyCode==13) event.keyCode=9;" disabled/>
						<?php } else { ?>
						<input type="checkbox" name="chkvive" onKeyDown="if(event.keyCode==13) event.keyCode=9;" disabled/>
						<?php } ?>
					</td>
				</tr>
			</table>
			</fieldset>
			<fieldset>
			<legend class="titulo_campos">Cargar Foto JPG/GIF/PNG</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr height="25">
					<td>
						<?php if ($datfamiliar["foto"]!="") { ?>
						<a href="database/grabar_alumnos.php?op=borrarfotofami&id_alumno=<?php echo $id_alumno;?>&id_familiar=<?php echo $id_familiar;?>&foto=<?php echo $datfamiliar["foto"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><img src="images/b_drop.png" width="16" height="16" border="0" align="absmiddle" title="Borrar foto actual"></a>
						<?php } ?>
					</td>
					<td>
						<input type="file" name="nwfoto" size="55" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<img src="images/ico_info.png" border="0" width="16" height="16" align="absmiddle" title="Tama�o 150 X 150 pixels. Si es superior el sistema la reducir� y puede deteriorarse la calidad.">
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Domicilio y Ubicaci�n</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr>
					<td></td>
					<td>Estrato <font color="red">*</font></td>
					<td><input type="text" name="famiest" style="width: 16px;" value="<?php echo $datfamiliar["estrato"];?>" maxlength="1" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Direcci�n <font color="red">*</font></td>
					<td><input type="text" name="famidir" size="45" value="<?php echo $datfamiliar["direccion"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Barrio, Ciudad <font color="red">*</font></td>
					<td>
						<input type="text" name="famibar" size="30" value="<?php echo $datfamiliar["barrio"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">&nbsp;<input type="text" name="famiciu" size="10" value="<?php echo $datfamiliar["ciudad"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>Correo</td>
					<td><input type="text" name="famicor" id="famicor" size="45" value="<?php echo $datfamiliar["correo"];?>" onBlur="javascript:checkMail(form1.famicor);" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Tel�fono <font color="red">*</font></td>
					<td><input type="text" name="famitel" size="25" value="<?php echo $datfamiliar["telefono"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Tel.Oficina</td>
					<td><input type="text" name="famiofi" size="25" value="<?php echo $datfamiliar["telefofi"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Celulares</td>
					<td><input type="text" name="famicel" size="25" value="<?php echo $datfamiliar["celular"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
			</table>
			</fieldset>
			<fieldset>
			<legend class="titulo_campos">Observaciones Generales</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr>
					<td><textarea name="famidet" rows="5" cols="62" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datfamiliar["detalles"];?></textarea></td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="30"></td></tr>
	<tr height="36">
		<td colspan="5" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivalu"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar" align="absmiddle">
			<?php } if ($id_familiar > 0 and $vive!=$id_familiar and $resppagos!=$id_familiar and $respmatri!=$id_familiar and $_SESSION["nivalu"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarFamiliar();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10" align="absmiddle"></a>
			<?php } ?>
			<a href="alumnos_familiares.php?id_alumno=<?php echo $id_alumno;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
