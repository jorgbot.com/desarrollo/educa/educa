<nav>
<ul>
    <li><a title="Home" href="inicio.php">Inicio</a></li>
    <li><a title="Registrar Notas" href="notas.php">Notas</a></li>
    <li><a title="Pagos Pensiones" href="pagos.php">Pensiones</a></li>
    <li><a title="Pagos Transportes" href="transporte.php">Transporte</a></li>
    <li><a title="Ausentismo y Fallas" href="fallas.php">Ausentismo</a></li>
    <li><a title="Registrar Alumnos" href="alumnos.php">Alumnos</a></li>
    <li><a title="Registrar Profesores" href="profesores.php">Profesores</a></li>
    <li><a title="Correo interno" href="#">Convivencia</a></li>
    <li><a title="Asignaturas, Logros" href="#">Asignaturas</a></li>
    <li><a title="Parámetros" href="setup.php">Setup</a></li>
    <li><a title="Salir" href="libs/logout.php">Cerrar</a></li>
</ul>
</nav>
