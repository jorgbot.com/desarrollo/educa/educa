<?php
//menu pago
if ($tipo=="T")
	$titulo = "PAGOS TRANSPORTE ";
else
	$titulo = "PAGOS PENSION ";
$op1_back1="images/backsilver1.gif";
$op1_back2="images/backsilver2.gif";
$op1_back3="images/backsilver3.gif";
$op2_back1="images/backsilver1.gif";
$op2_back2="images/backsilver2.gif";
$op2_back3="images/backsilver3.gif";
$op3_back1="images/backsilver1.gif";
$op3_back2="images/backsilver2.gif";
$op3_back3="images/backsilver3.gif";
$op4_back1="images/backsilver1.gif";
$op4_back2="images/backsilver2.gif";
$op4_back3="images/backsilver3.gif";
if ($opcion=="liquidar")
{
	$op1_back1="images/backgreen1.gif";
	$op1_back2="images/backgreen2.gif";
	$op1_back3="images/backgreen3.gif";
}
if ($opcion=="historial")
{
	$op2_back1="images/backgreen1.gif";
	$op2_back2="images/backgreen2.gif";
	$op2_back3="images/backgreen3.gif";
}
if ($opcion=="ajustesint")
{
	$op3_back1="images/backgreen1.gif";
	$op3_back2="images/backgreen2.gif";
	$op3_back3="images/backgreen3.gif";
}
if ($opcion=="ajustespen")
{
	$op4_back1="images/backgreen1.gif";
	$op4_back2="images/backgreen2.gif";
	$op4_back3="images/backgreen3.gif";
}
?>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="40">
		<?php if ($tipo=="T") { ?>
		<td class="titulo"><img src='images/transporte.png' width="40" height="40" border="0" align="absmiddle"> <?php echo $titulo." ".$ano;?></td>
		<?php } else { ?>
		<td class="titulo"><img src='images/pagos.png' width="40" height="40" border="0" align="absmiddle"> <?php echo $titulo." ".$ano;?></td>
		<?php } if ($opcion=="liquidar") { ?>
		<td align="right" class="subtitulo">ENTRADA <?php echo $id_entrada;?> &nbsp;-&nbsp;<?php echo fecha_completa($fchentrada);?></td>
		<?php } ?>
	</tr>
	<tr>
		<td colspan="2" align="right" valign="bottom">
			<?php if ($id_alumno > 0) { ?>
			<table cellpadding="0" cellspacing="0" border="0">
				<tr height="25" bgcolor="#FFFFFF">
					<td width="6" background="<?php echo $op1_back1;?>"></td>
					<td background="<?php echo $op1_back2;?>" align="center">&nbsp;&nbsp;<a href="pagos_edit.php?op=editar&opcion=liquidar&id_pago=<?php echo $id_pago;?>&id_alumno=<?php echo $id_alumno;?>&ano=<?php echo $ano;?>&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&tipo=<?php echo $tipo;?>" class="link_menu">Liquidación Pago</a>&nbsp;&nbsp;</td>
					<td width="6" background="<?php echo $op1_back3;?>"></td>
					<td width="6" background="<?php echo $op2_back1;?>"></td>
					<td background="<?php echo $op2_back2;?>" align="center">&nbsp;&nbsp;<a href="pagos_edit.php?op=historial&opcion=historial&id_pago=<?php echo $id_pago;?>&id_alumno=<?php echo $id_alumno;?>&ano=<?php echo $ano;?>&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&tipo=P" class="link_menu">Historial Pagos Detallados</a>&nbsp;&nbsp;</td>
					<td width="6" background="<?php echo $op2_back3;?>"></td>
					<?php if ($tipo!="T") { ?>
					<td width="6" background="<?php echo $op3_back1;?>"></td>
					<td background="<?php echo $op3_back2;?>" align="center">&nbsp;&nbsp;<a href="pagos_ajuste.php?op=ajustesint&id_alumno=<?php echo $id_alumno;?>&id_pago=<?php echo $id_pago;?>&ano=<?php echo $ano;?>&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&tipo=I" class="link_menu">Ajustes Intereses</a>&nbsp;&nbsp;</td>
					<td width="6" background="<?php echo $op3_back3;?>"></td>
					<td width="6" background="<?php echo $op4_back1;?>"></td>
					<td background="<?php echo $op4_back2;?>" align="center">&nbsp;&nbsp;<a href="pagos_ajuste.php?op=ajustespen&id_alumno=<?php echo $id_alumno;?>&id_pago=<?php echo $id_pago;?>&ano=<?php echo $ano;?>&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&tipo=P" class="link_menu">Ajustes Pensiones</a>&nbsp;&nbsp;</td>
					<td width="6" background="<?php echo $op4_back3;?>"></td>
					<?php } ?>
				</tr>
			</table>
			<?php } else { ?>
			<br><br>
			<?php } ?>
		</td>
	</tr>
	<tr><td colspan="2" height="2" bgcolor="#BBE040"></td></tr>
	<tr><td colspan="2" height="1" bgcolor="#A7CD2C"></td></tr>
</table>
