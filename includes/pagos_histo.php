<?php
$file = "y".$ano."pagos";
$filedet = "y".$ano."pagosdet";
$origen = "";
$metodo = "I"; //Intereses o Rangos
$pensionbas = 0;
$pensionfin = 0;
$pensionret = 0;
$sql="SELECT pension, pensionfin, pensionret FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{$pensionbas = $reggrad->pension; $pensionfin = $reggrad->pensionfin; $pensionret = $reggrad->pensionret;}
if ($pensionfin > 0 and $pensionret > 0) {
    $metodo = "R";
    $porcen = 0;
}
?>
<table border="0" cellpadding="2" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr>
		<th>FchConsigna</th>
		<th>IDPago</th>
		<th colspan="2">Hoja Entrada</th>
		<th>Consigna</th>
		<th>Efectivo</th>
		<th width="50">Total</th>
		<th>Mes</th>
		<?php if ($tipo=="P") { ?>
		<th>Saldo<br>Pension</th>
		<th>Pagos<br>Pensiones</th>
		<th>Interes<br>Liquid</th>
		<th>Interes<br>Ajuste</th>
		<th>Pago<br>Interes</th>
		<th>Pago<br>Total Mes</th>
		<th>Debe<br>Pensi�n</th>
		<th>Debe<br>Inter�s</th>
		<?php } else { ?>
		<th>Saldo<br>Transporte</th>
		<th width="50">Pagos</th>
		<th width="50">Debe</th>
		<?php } ?>
	</tr>
	<?php
	if ($tipo=="P")
		$valores = valores_pension($id_alumno,$ano);
	else
		$valores = valores_transporte($id_alumno,$ano);
	$saldomes = array();
	$saldoint = array();
	for($i=1; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$saldomes[$mes] = $valores[$mes];
		$saldoint[$mes] = 0;
	}
	$sql="SELECT * FROM $filedet WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' ORDER BY id_pago, mes";
	$datdet = viewsql($sql);
	$sql="SELECT * FROM $file WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' ORDER BY fchpago, id_pago";
	$datpag = viewsql($sql);
	$numpag = 0;
	$pagostot = 0;
        $pagosnet = 0;
        $pagosint = 0;
        $neto = 0;
	$maxmes = 0;
	foreach($datpag as $regpag)
	{
		$idpago = $regpag->id_pago;
		$numpag = $numpag + 1;
		$pagostot = $pagostot + $regpag->valor;
		$pagosnet = $pagosnet + $regpag->neto;
		$pagosint = $pagosint + $regpag->interes;
                $fchpago = $regpag->fchpago;
		?>
		<tr bgcolor="#FFFFE8">
			<td class="titulo_error"><?php echo fecha_texto($regpag->fchpago);?></td>
			<?php if ($origen=="") { ?>
			<td><a class="link_list" href="pagos_edit.php?op=editar&id_pago=<?php echo $idpago;?>&tipo=<?php echo $tipo;?>&ano=<?php echo $ano;?>&id_alumno=<?php echo $id_alumno;?>"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $idpago;?></a></td>
			<?php } else { ?>
			<td><a class="link_list" href="pagos_edit.php?op=editar&id_pago=<?php echo $idpago;?>&tipo=<?php echo $tipo;?>&ano=<?php echo $ano;?>&id_alumno=<?php echo $id_alumno;?>" onclick="newWindow(this.href, '', 870, 580, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="PAGO"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $idpago;?></a></td>
			<?php } ?>
			<td width="20"><?php echo $regpag->id_entrada;?></td>
			<td width="40"><?php echo fecha_texto($regpag->fchentrada)?></td>
			<td align="right"><?php echo number_format($regpag->valor_con,0);?></td>
			<td align="right"><?php echo number_format($regpag->valor_efe,0);?></td>
			<td align="right"><?php echo number_format($regpag->valor,0);?></td>
			<td></td>
			<td></td>
			<td></td>
			<?php if ($tipo=="P") { ?>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<?php } else { ?>
			<td></td>
			<?php } ?>
			</tr>
			<?php
			$num = 0;
			foreach($datdet as $regdet)
			{
                            if ($regdet->id_pago==$idpago)
                            {
				$num = $num + 1;
				$nmes = $regdet->mes;
				$mes = $regdet->mes;
				if ($mes > $maxmes)
					$maxmes = $mes;
				if ($nmes < 10)
					$mes = "0".$mes;
                                //saldo del pago
                                $saldo = $valores[$mes];
                                $saldomes[$mes] = $saldo;
                                //saldo acumulado descontando el pago
                                $neto = $neto + $regdet->valor;
				$saldomes[$mes] = $saldomes[$mes] - $regdet->valor;
				$nommes = strtoupper(nombre_mes($regdet->mes));
				if (intval($regdet->mes)==1)
					$nommes = "MATRICULA";
				$salint = intval($regdet->interesliq) - intval($regdet->interes) - intval($regdet->interesaju);
				$saldoint[$mes] = $saldoint[$mes] + $salint;
				if ($salint < 0)
					$salint = 0;
				?>
				<tr bgcolor="#FFFFFF">
				<td colspan="7"></td>
				<td class="titulo_error"><?php echo substr($nommes,0,3);?></td>
				<td align="right"><?php echo number_format($saldo,0);?></td>
				<?php if ($tipo=="P") { ?>
				<td align="right"><?php echo number_format($regdet->valor,0);?></td>
				<td align="right"><?php echo number_format($regdet->interesliq,0);?></td>
				<td align="right"><?php echo number_format($regdet->interesaju,0);?></td>
				<td align="right"><?php echo number_format($regdet->interes,0);?></td>
				<td align="right"><?php echo number_format(intval($regdet->valor)+intval($regdet->interes),0);?></td>
				<td align="right" class="titulo_error" title="Debe pension"><?php echo number_format($saldomes[$mes],0);?></td>
				<td align="right" class="titulo_error" title="Debe interes"><?php echo number_format($salint,0);?></td>
				<?php } else { ?>
				<td align="right"><?php echo number_format($regdet->valor,0);?></td>
				<td align="right"><?php echo number_format($saldomes[$mes],0);?></td>
				<?php } ?>
				</tr>
				<?php
				$valores[$mes] = $valores[$mes] - $regdet->valor;
                            }
			}
			?>
		<?php
	} ?>
	<?php
	$totmes = 0;
	$totint = 0;
	$nommes = "";
	for($i=1; $i<=$maxmes ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$totmes = $totmes + $saldomes[$mes];
		$totint = $totint + $saldoint[$mes];
	}
	$nommes = strtoupper(nombre_mes($mes));
	if (intval($mes)=="01")
		$nommes = "MATRICULA";
        if ($pagosnet > $neto)
        {
            $totmes = $neto - $pagosnet;
            $totint = 0;
        }
	if ($tipo=="P")
	{
	?>
 	<tr align="right" bgcolor="#E0E7E9" height="24">
	<td colspan="7" class="titulo_total" align="left">TOTAL PAGOS <?php echo $numpag;?> (SALDOS A <?php echo $nommes;?>)</td>
        <td colspan="2" class="titulo_total" title="Valor pensiones"><?php echo number_format($neto,0);?></td>
        <td class="titulo_total" title="Pagos pensiones"><?php echo number_format($pagosnet,0);?></td>
	<td></td>
	<td></td>
        <td class="titulo_total" title="Pagos intereses"><?php echo number_format($pagosint,0);?></td>
        <td class="titulo_total" title="Total pagos"><?php echo number_format($pagostot,0);?></td>        
        <td class="titulo_error" title="Saldo pensiones"><?php echo number_format($totmes,0);?></td>
        <td class="titulo_error" title="Saldo intereses"><?php echo number_format($totint,0);?></td>
	<?php } else { ?>
 	<tr align="right" bgcolor="#E0E7E9" height="24">
	<td colspan="8" align="left" class="titulo_total">TOTAL PAGOS <?php echo $numpag;?></td>
	<td align="right" class="titulo_total" title="Valor transporte"><?php echo number_format($neto,0);?></td>
	<td align="right" class="titulo_total" title="Pagos transporte"><?php echo number_format($pagosnet,0);?></td>
	<td align="right" class="titulo_error" title="Saldo transporte"><?php echo number_format($totmes,0);?></td>
	<?php } ?>
	</tr>
</table>

