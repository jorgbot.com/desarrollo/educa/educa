<?php
$ano = $_SESSION["anoaca"];
$fecha = date('Y-m-d');
$diasel = substr($fecha,8,2);
$numdia = date("w",mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2)), intval(substr($fecha,0,4))));
//fecha inicio de semana
$fchini = mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2))-($numdia-1), intval(substr($fecha,0,4)));
$fchini = date('Y-m-d',$fchini);
$mesini = substr($fchini,5,2);
$diaini = substr($fchini,8,2);
$titini = substr(fecha_larga($fchini),0,10);
//fecha final de semana
$fchfin=mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2))+(7-$numdia), intval(substr($fecha,0,4)));
$fchfin=date('Y-m-d',$fchfin);
$mesfin = substr($fchfin,5,2);
$diafin = substr($fchfin,8,2);
$titfin = substr(fecha_larga($fchfin),0,10);
//crear tabla temporal
$filetmp="tmpcum".$_SESSION["usuid"];
$sql="create temporary table ".$filetmp." select campo1, campo2, campo3, campo4, campo5 from importar limit 1";
execsql($sql);
execsql("delete from ".$filetmp);
//traer cumpleaņos alumnos
$sql="SELECT alumnos.apellidos, alumnos.nombres, alumnos.fchnace, alumnos.foto, matriculas.ano, matriculas.grado, matriculas.curso FROM alumnos, matriculas ".
	"WHERE alumnos.id_alumno = matriculas.id_alumno AND matriculas.ano = '$ano' AND alumnos.estado = 'ACTIVO' ";
if ($mesini == $mesfin)
	$sql.="AND MONTH(alumnos.fchnace)='$mesini' and (DAY(alumnos.fchnace) >= '$diaini' AND DAY(alumnos.fchnace) <= '$diafin') ";
else
	$sql.="AND ((MONTH(alumnos.fchnace)='$mesini' and DAY(alumnos.fchnace) >= '$diaini') OR (MONTH(alumnos.fchnace)='$mesfin' and DAY(alumnos.fchnace) <= '$diafin')) ";
$sql.="ORDER BY date_format(alumnos.fchnace,'%d-%m-%Y')";	
$cumple=viewsql($sql);
//echo "$sql<br>";
foreach($cumple as $registros)
{
	$fchcum = substr($registros->fchnace,8);
	$nombre = $registros->nombres." ".$registros->apellidos;
	$foto = $registros->foto;
	$curso = $registros->grado."-".$registros->curso;
	$sql = "insert into $filetmp (campo1,campo2,campo3,campo4,campo5) Values('$fchcum','$nombre','$foto','$curso','A')";
	execsql($sql);
}
//traer cumpleaņos docentes
$sql="SELECT apellidos, nombres, fchnace, foto FROM profesores WHERE  estado = 'ACTIVO' ";
if ($mesini == $mesfin)
	$sql.="AND MONTH(fchnace)='$mesini' and (DAY(fchnace) >= '$diaini' AND DAY(fchnace) <= '$diafin') order by date_format(fchnace,'%d-%m-%Y')";
else
	$sql.="AND ((MONTH(fchnace)='$mesini' and DAY(fchnace) >= '$diaini') OR (MONTH(fchnace)='$mesfin' and DAY(fchnace) <= '$diafin')) order by date_format(fchnace,'%d-%m-%Y')";
$cumple=viewsql($sql);
//echo "$sql<br>";
foreach($cumple as $registros)
{
	$fchcum = substr($registros->fchnace,8);
	$nombre = $registros->nombres." ".$registros->apellidos;
	$foto = $registros->foto;
	$curso = "";
	$sql = "insert into $filetmp (campo1,campo2,campo3,campo4,campo5) Values('$fchcum','$nombre','$foto','$curso','P') ";
	execsql($sql);
}
$sql="select * from $filetmp order by campo1";
$cumple=viewsql($sql);
?>