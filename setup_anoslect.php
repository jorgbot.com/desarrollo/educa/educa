<?php
/***********************************************************************************************
* @file        : setup_anoslect.php                                                            *
* @brief       : Lista anoslect del aplicativo                                                 *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 20-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 20-Mar-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivpar"] <= 0)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</script>";}
include("database/database.php");
include("libs/fechas.php");
connect();
?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>IDIC</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="robots" content="noindex"/>
<meta name="googlebot" content="noindex"/>
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="estilos.css">
<link href="estilosfot.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript'>
function seleccionar()
{
	var ano = document.form0.perfil.value;
	var theURL = 'setup_anoslect.php?perfil='+perfil;
	window.location.href = theURL;
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#FFFF66">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="150" bgcolor="#006600">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr height="40" bgcolor="#006600">
		<td background="fondos/top_verde.jpg"></td>
		<td background="fondos/top_verde.jpg" class="titulo_color">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_top.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/top_verde.jpg"></td>
	</tr>
	<tr height="3" bgcolor="#FFFF66">
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_top.jpg"></td></tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="200">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10"></td>
				<td>
					<?php include("includes/menu_setup.php");?>
					<br/>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<form action="" method="post" name="form0">
					<input type="hidden" name="op" value="buscar">
						<tr><td colspan="3" height="15"></td></tr>
						<tr height="32">
							<td width="6" background="images/backsilver401.gif"></td>
							<td background="images/backsilver402.gif" class="titulo"><img src="images/idicestrella.png" border="0" width="24" height="24" align="absmiddle" hspace="1">A�os Lectivos</td>
							<td align="right" background="images/backsilver402.gif">
								<?php if ($_SESSION["nivpar"] >= 4) { ?>
								&nbsp;
								<a href="setup_anoslectedit.php" onclick="newWindow(this.href, '', 750, 400, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
								<?php } ?>
							</td>
							<td width="6" background="images/backsilver403.gif"></td>
						</tr>
					</form>
					</table>
					<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="100%">
						<tr height="20">
							<th width="50">A�o</th>
							<th width="60">Estado</th>
							<th>NotaMin</th>
							<th>NotaMax</th>
							<th>NotaLim</th>
							<th>Areas</th>
							<th>Protocolizaci�n</th>
						</tr>
						<?php
						$sql="SELECT * FROM anoslect ORDER BY ano desc";
						$datanol=viewsql($sql);
						foreach($datanol as $reganol)
						{
						$ano = $reganol->ano;
						?>		
						<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
							<td><a class="link_list" href="setup_anoslectedit.php?ano=<?php echo $ano;?>" onclick="newWindow(this.href, '', 750, 400, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Editar"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $ano;?></a></td>
							<td><?php echo $reganol->estado;?></td>
							<td align="center"><?php echo $reganol->notamin;?></td>
							<td align="center"><?php echo $reganol->notamax;?></td>
							<td align="center"><?php echo $reganol->notalim;?></td>
							<td align="center"><?php echo $reganol->areasper;?></td>
							<td><?php echo $reganol->protocolo;?></td>
						</tr>
						<?php } ?>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_bot.jpg"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#D3A919"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FFDB4B"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FCF463"></td></tr>
	<tr>
		<td colspan="3" valign="top">
		<div id="fondo_pie">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</div>
		</td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>