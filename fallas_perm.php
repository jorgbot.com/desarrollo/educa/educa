<?php
/***********************************************************************************************
* @file        : fallas_perm.php                                                            *
* @brief       : Lista alumnos para selecci�n  (debe permitir de todos los a�os)               *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("libs/fechas.php");
connect();
$ano = isset($_GET['ano']) ? $_GET['ano'] : $_SESSION["anoaca"];
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : null ;
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : "buscar" ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : 0 ;
$id_falla = isset($_GET['id_falla']) ? $_GET['id_falla'] : 0 ;
$buscar = isset($_POST['buscar']) ? $_POST['buscar'] : null ;
$fecha = isset($_GET['fecha']) ? $_GET['fecha'] : "";
if ($fecha=="")
    $fecha = isset($_POST['fecha']) ? $_POST['fecha'] : date('Y-m-d') ;
$titulo = "";
$cursos = "y".$ano."cursos";
if ($id_alumno > 0) {
    $sql = "SELECT $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres FROM $cursos WHERE id_alumno = '$id_alumno' LIMIT 1";
    $datalu=viewsql($sql);
    foreach($datalu as $regalu)
    {
        $titulo = "ID ".$id_alumno." ".$regalu->apellidos." ".$regalu->nombres.", ".$regalu->grado."&deg; CURSO ".$regalu->curso;
    }
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language="javascript" type='text/JavaScript'>
function validarPermiso()
{
	if(document.form1.fecha.value == '' || document.form1.fecha.value.substr(0,2) == '00')
	{alert('Debe seleccionar fecha...');document.form1.fecha.focus(); return false;}
	if(document.form1.fecha.value.substr(6,4) != document.form1.ano.value)
	{alert('Fecha incorrecta, no corresponde al a�o '+document.form1.ano.value+' !');document.form1.fecha.focus(); return false;}
	if(document.form1.concepto.value  == '')
	{alert('Debe especificar el concepto...'); document.form1.concepto.focus(); return false;}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar este Registro?");
	if(ok)
	{
		document.form1.op.value='borrarfalla';
		document.form1.submit();
	}
}
function seleccionar()
{
	var ano = document.form0.ano.value;
	var tipo = document.form0.tipo.value;
	var pos = document.form0.gradocurso.value.indexOf('*');
	var grado = document.form0.gradocurso.value.substr(0,pos);
	var curso = document.form0.gradocurso.value.substr(pos+1);
	var theURL = 'fallas_perm.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&tipo='+tipo;
	window.location.href = theURL;
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="40">
		<td class="titulo">
                    <?php if ($tipo==="A") { ?>
                    <img src='images/permiso.png' width="40" height="40" border="0" align="absmiddle"> AUTORIZACIONES
                    <?php } if ($tipo==="C") { ?>
                    <img src='images/calendar.png' width="40" height="40" border="0" align="absmiddle"> CITACIONES
                    <?php } if ($tipo==="I") { ?>
                    <img src='images/fallas.png' width="40" height="40" border="0" align="absmiddle"> INASISTENCIAS
                    <?php } ?>
                </td>
		<td class="subtitulo" align="right">A�O <?php echo $ano;?></td>
	</tr>
</table>
<?php if ($opcion=="buscar")
{
//traer alumnos
$numalum = 0;
if ($buscar!="" or $grado!="")
{
	$sql="SELECT id_alumno,apellidos, nombres, grado, curso, fchretiro FROM $cursos WHERE id_alumno > '0' ";
	if ($buscar != "")
	{
		if ($buscar > 0)
			$sql.="AND id_alumno = '$buscar' ";
		else
			$sql.="AND (apellidos like '$buscar%' or nombres like '$buscar%') ";
	}
	else
	{
		if ($grado != "")
			$sql.="AND grado = '$grado' ";
		if ($curso != "")
			$sql.="AND curso = '$curso' ";
	}
	$sql.= "ORDER BY apellidos, nombres";
	$datalum=viewsql($sql);
	$numalum = count($datalum);
} 
?>
<table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
<form action="" method="post" name="form0">
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='tipo' value='<?php echo $tipo;?>'>
	<tr><td colspan="3" height="15"></td></tr>
	<tr height="32">
		<td width="6" background="images/backsilver401.gif"></td>
		<td align="right" background="images/backsilver402.gif">
			ALUMNO...
			<input type="text" name="buscar" value="<?php echo $buscar;?>" size="20" style="text-transform:uppercase;">
			&nbsp;
			<select name="gradocurso" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
			<option value="" selected>Grado y Curso...</option>
			<?php
			$sql = "SELECT grado, gradonom FROM grados order by id_grado";
			$datgrad = viewsql($sql);
			foreach($datgrad as $reggrad)
			{
				$lgrado = $reggrad->grado;
				$lcurso = "";
				if ($lgrado==$grado and $lcurso==$curso)
					echo "<option value='$lgrado*$lcurso' selected>GRADO $reggrad->gradonom</option>";
				else
					echo "<option value='$lgrado*$lcurso'>GRADO $reggrad->gradonom</option>";
				$sql = "SELECT grado, curso FROM cursos WHERE ano = '$ano' AND grado = '$lgrado' order by curso";
				$datcurs = viewsql($sql);
				foreach($datcurs as $regcurs)
				{
					if ($regcurs->grado==$grado and $regcurs->curso==$curso)
					echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
					else
					echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
				}
			}
			?>																
			</select>
			<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
			&nbsp;
		</td>
		<td width="6" background="images/backsilver403.gif"></td>
	</tr>
</form>
</table>
<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="95%" align="center">
	<tr height="20">
		<th>Apellidos y Nombres</th>
		<th width="50">Id</th>
		<th width="60">Retirado</th>
		<th width="30">Grado</th>
		<th width="30">Curso</th>
                <th colspan="2">Acciones</th>
	</tr>
	<?php
	if ($numalum > 0)
	{
	foreach($datalum as $regalum)
	{
	$id_alumno = $regalum->id_alumno;
	$alumno = trim($regalum->apellidos)." ".trim($regalum->nombres);
	?>		
	<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
		<td><?php echo $regalum->apellidos." ".$regalum->nombres;?></td>
                <td align="center"><?php echo $id_alumno;?></td>
		<td><?php echo fecha_texto($regalum->fchretiro);?></td>
		<td align="center"><?php echo $regalum->grado;?></td>
		<td align="center"><?php echo $regalum->curso;?></td>
                <td align="center"><a class="link_list" href="fallas_perm.php?opcion=listar&id_alumno=<?php echo $id_alumno;?>&tipo=<?php echo $tipo;?>"><u>Listar</u></a></td>
                <td align="center"><a class="link_list" href="fallas_perm.php?opcion=editar&id_alumno=<?php echo $id_alumno;?>&tipo=<?php echo $tipo;?>"><u>Nuevo</u></a></td>
	</tr>
	<?php } } else { ?>
	<tr><td colspan="7" bgcolor="#FFFFFF" height="100" class="subtitulo" align="center">Asigne una condici�n de b�squeda...</td></tr>
	<?php } ?>
	<tr height="36">
		<td colspan="8" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
<?php } ?>
<?php if ($opcion=="listar") 
{
?>
<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
    <tr><td colspan="3" height="15"></td></tr>
    <tr height="32">
	<td width="6" background="images/backsilver401.gif"></td>
        <td align="left" class="titulo" background="images/backsilver402.gif">
            <?php echo $titulo;?>
        </td>
        <td align="right" background="images/backsilver402.gif">
            <a class="link_list" href="fallas_perm.php?opcion=editar&tipo=<?php echo $tipo;?>&id_alumno=<?php echo $id_alumno;?>"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
	</td>
	<td width="6" background="images/backsilver403.gif"></td>
    </tr>
</table>
<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="90%" align="center">
    <tr height="20">
	<th width="100">Fecha</th>
        <?php if ($tipo=="A") {?>
	<th width="100">Hasta</th>
	<th>Concepto</th>
	<th width="100">Autorizada</th>
        <?php } else { ?>
	<th width="40">Hora</th>
	<th>Concepto</th>
	<th width="100">Solicitud</th>
        <?php }?>
        <th colspan="2">Acciones</th>
    </tr>
    <?php
    //traer encabezado de fallas
    $fallas = "y".$ano."fallas";
    $sql = "SELECT * FROM $fallas WHERE id_alumno = '$id_alumno' and tipo='$tipo' ORDER BY fecha desc ";
    $datfall=viewsql($sql);
    foreach($datfall as $regfall)
    {
        $id_falla = $regfall->id_falla;
    ?>		
    <tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
        <td><?php echo fecha_larga($regfall->fecha);?></td>
        <?php if ($tipo=="A") {?>
        <td><?php echo fecha_larga($regfall->fchfin);?></td>
        <?php } else { ?>
        <td align="center"><?php echo $regfall->hcita;?></td>
        <?php }?>
        <td><?php echo $regfall->concepto;?></td>
        <td><?php echo fecha_larga($regfall->fchjust);?></td>
        <td width="40" align="center"><a class="link_list" href="fallas_perm.php?opcion=editar&ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>&id_alumno=<?php echo $id_alumno;?>&tipo=<?php echo $tipo;?>"><u>Editar</u></a></td>
        <td width="50" align="center"><a name="imprimir" class="link_list" href="reports/rep_fallaspapper.php?ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>" onclick="newWindow(this.href, '', 980, 500, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Editar"><u>Imprimir</u></a></td>
    </tr>
    <?php } ?>
</table>   
<?php } ?>
<?php if ($opcion=="editar") 
{
$estado = "ABIERTO";
$swano = 1;
$estado = "";
$pernomb = "";
$periodo = 0;
$sql="SELECT periodo, pernomb, estado FROM periodos WHERE ano = '$ano' AND pertipo = 'P' AND '$fecha' BETWEEN date_format(fchfallasi,'%Y-%m-%d') AND date_format(fchfallasf,'%Y-%m-%d') LIMIT 1";
$datperi = viewsql($sql);
foreach($datperi as $regperi)
{
	$estado = $regperi->estado;
	$pernomb = $regperi->pernomb;
	$periodo = $regperi->periodo;
}
if ($estado == "")
	$estado = "ABIERTO";
if ($ano!=$_SESSION["anoaca"])
{
	$estado = "CERRADO";
	$swano = 0;
}
$datfalla = datos_falla($ano,$id_falla);
if ($id_falla > 0)
{
	$fecha = $datfalla["fecha"];
        $tipo = $datfalla["tipo"];
}
else 
{
    $datfalla["fecha"] = $fecha;
    $datfalla["fchfin"] = $fecha;
}
//traer curso del alumno
$cursos = "y".$ano."cursos";
$nivel = "";
$grado = "";
$curso = "";
$sql = "select $cursos.grado, $cursos.curso, grados.gradonom, grados.nivel from $cursos, grados where $cursos.id_alumno = '$id_alumno' and $cursos.grado = grados.grado limit 1";
$datcurs = viewsql($sql);
foreach($datcurs as $regcurs)
{
	$nivel = $regcurs->nivel;
	$grado = $regcurs->grado;
	$gradonom = $regcurs->gradonom;
	$curso = $regcurs->curso;
}
?>
<br><br>
<table width="70%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_fallas.php" method="post" name="form1" onSubmit="return validarPermiso();">
<input type='hidden' name='op' value='grabarfalla'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='tipo' value='<?php echo $tipo;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type='hidden' name='id_falla' value='<?php echo $id_falla;?>'>
<input type='hidden' name='id_categoria' value='0'>
<input type='hidden' name='periodo' value='<?php echo $periodo;?>'>
<input type='hidden' name='page' value='fallaspermiso'>
	<tr height="50">
		<td width="30"></td>
                <td class="titulo_campos"><?php echo $titulo;?></td>
                <td></td>
		<td width="30"></td>
	</tr>
	<tr valign = 'top'>
	<td></td>
	<td>
            Fecha &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <input type="text" name="fecha" id="fecha" value="<?php echo fec_dma($datfalla["fecha"]);?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fecha, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
            <?php if ($tipo==="A") { ?>
            &nbsp;Vigente Hasta&nbsp;
            <input type="text" name="fchfin" id="fchfin" value="<?php echo fec_dma($datfalla["fchfin"]);?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchfin, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
            <input type='hidden' name='hcita' value=''>
            <?php } else { ?>
            &nbsp;Hora citaci�n&nbsp;
            <input type="text" name="hcita" value="<?php echo $datfalla["hcita"];?>" style="width: 65px;" title="hh:mm" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
            <input type='hidden' name='fchfin' value='00-00-0000'>
            <?php } ?>
            <br>
            <br>
            Concepto &nbsp; 
            <input type="text" name="concepto" value="<?php echo $datfalla["concepto"];?>" style="width: 240px; text-transform:uppercase;">
            <?php if ($tipo=="A") {?>
            &nbsp;Autorizado &nbsp;
            <?php } else { ?>
            &nbsp;Solicitud &nbsp;
            <?php }?>
            <input type="text" name="fchjust" id="fchjust" value="<?php echo fec_dma($datfalla["fchjust"]);?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchjust, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                <br><br>Observaciones<br>
		<textarea name='detalles' rows="5" cols="65" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datfalla["detalles"];?></textarea>
                <br>PERIODO FALLAS <?php echo $estado;?>
	</td>
	<td></td>
        </tr>				
        <tr><td colspan="4" height="20"></td></tr>
	<tr height="36">
		<td colspan="4" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if (($_SESSION["nivfal"] >= 2 and $estado != "CERRADO" and $swano==1) or ($_SESSION["nivfal"] >= 4 and $swano==1)) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_falla > 0 and $_SESSION["nivfal"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="70" height="24" alt="Eliminar" hspace="10"></a>
			<?php } ?>
                        <?php if ($id_falla > 0 and $datfalla["id_categoria"] > 0) {?><a name="imprimir" class="link_list" href="reports/rep_fallaspapper.php?ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>" onclick="newWindow(this.href, '', 980, 500, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Imprimir"><img src="images/btn_imprimir.png" border="0" width="70" height="24" alt="Cerrar" hspace="10"></a><?php }?>
			<a href="fallas_perm.php?opcion=listar&id_alumno=<?php echo $id_alumno;?>&tipo=<?php echo $tipo;?>"><img src="images/btn_regresar.png" border="0" width="70" height="24" alt="Regresar" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<br>
<?php } ?>
<?php if ($opcion=="inasistencias")
{ 
$sql="select id_categoria, categoria from categorias where tip_categoria = 'INASISTENCIAS' order by categoria";
$datcateg=viewsql($sql);
$fallas = "y".$ano."fallas";
$sql = "SELECT * FROM $fallas WHERE id_alumno = '$id_alumno' AND (tipo='I' OR tipo='R' OR tipo='P') AND id_asignatura='0' ORDER BY fecha desc";
$datfall=viewsql($sql);
?>
<table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
    <tr><td colspan="3" height="15"></td></tr>
    <tr height="32">
	<td width="6" background="images/backsilver401.gif"></td>
        <td align="left" class="titulo" background="images/backsilver402.gif">
            <?php echo $titulo;?>
        </td>
        <td align="right" background="images/backsilver402.gif">
	</td>
	<td width="6" background="images/backsilver403.gif"></td>
    </tr>
</table>
<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="95%" align="center">
<form action="database/grabar_fallas.php" method="post" name="form1">
<input type='hidden' name='op' value='grabarfallasalumno'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
    <tr height="20">
	<th width="100">Fecha</th>
        <th>Falla</th>
        <th>Retardo</th>
        <th>Permiso</th>
        <th>Concepto</th>
        <th colspan="2">Excusa y Fecha</th>
        <th width="50">Acciones</th>
    </tr>
    <?php
    foreach($datfall as $regfall)
    {
        $id_falla = $regfall->id_falla;
        $id_categoria = intval($regfall->id_categoria);
        $tipo = $regfall->tipo;
        $fchjust = fecha_texto($regfall->fchjust);
        $checkina = "";
        $checkret = "";
        $checkper = "";
        if ($regfall->tipo == "R") {
           $checkret = "checked";  
        }
        if ($regfall->tipo == "I") {
           $checkina = "checked";
        }
        if ($regfall->tipo == "P") {
           $checkper = "checked";
        }
        $seccion = "seccion".$id_falla;
        $nameop = "op".$id_falla;
        $namect = "ct".$id_falla;
        $namepe = "pe".$id_falla;
    ?>		
    <tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
        <td width="70"><?php echo fecha_larga($regfall->fecha);?></td>
        <td align="center"><input type="radio" name="<?php echo $nameop;?>" value="I" <?php echo $checkina;?> ></td>
	<td align="center"><input type="radio" name="<?php echo $nameop;?>" value="R" <?php echo $checkret;?> ></td>
	<td align="center"><input type="radio" name="<?php echo $nameop;?>" value="R" <?php echo $checkper;?> ></td>
        <?php if ($tipo=="P") { ?>
        <td><input type="text" name="<?php echo $namepe;?>" value="<?php echo $regfall->concepto;?>" style="width: 170px; text-transform:uppercase;"></td>
        <?php } else { ?>
        <td><input type='hidden' name='<?php echo $namepe;?>' value=''></td>
        <?php } ?>
        <td align="center">
            <select name="<?php echo $namect;?>" style="width: 200px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
            <?php
            echo "<option value='0' selected></option>";
            foreach($datcateg as $regcateg)
            {
                if ($id_categoria == intval($regcateg->id_categoria))
                  echo "<option value='$regcateg->id_categoria' selected>$regcateg->categoria</option>";
                else
                  echo "<option value='$regcateg->id_categoria'>$regcateg->categoria</option>";
            }
            ?>																
            </select>
        </td>
        <td width="70"><?php echo $fchjust;?></td>
        <td width="50" align="center">
            <?php if ($tipo != "P" and $id_falla > 0 and $id_categoria > 0) {?><a name="imprimir" class="link_list" href="reports/rep_fallaspap.php?ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>" onclick="newWindow(this.href, '', 1000, 500, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Editar"><u>Imprimir</u></a><?php }?>
            <?php if ($tipo == "P" and $id_falla > 0 and $id_categoria > 0) {?><a name="imprimir" class="link_list" href="reports/rep_fallaspapper.php?ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>" onclick="newWindow(this.href, '', 1000, 500, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Editar"><u>Imprimir</u></a><?php }?>
        </td>
    </tr>
    <?php } ?>
    <tr height="30">
        <td colspan="8" background="images/backsilver402.gif" align="right">
            <input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="5" alt="Grabar" align="absmiddle">
            <a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" align="absmiddle" hspace="5"></a>
        </td>
    </tr>
 </form>
</table>   
<?php } ?>
</body>
</html>
<?php disconnect();?>
