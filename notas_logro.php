<?php
/***********************************************************************************************
* @file        : notas_logro.php                                                               *
* @brief       : planilla de notas de actividades por logro                                    *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
$id_asignatura = $_GET["id_asignatura"];
$curso = $_GET["curso"];
$periodo = $_GET["periodo"];
$estado = $_GET["estado"];
$tipoedu = $_GET["tipoedu"];
$id_logro = $_GET["id_logro"];
$id_alumno = $_GET["id_alumno"];
$alumno = $_GET["alumno"];
$fchretiro = $_GET["fchretiro"];
$pertipo = "P";
connect();
//datos asignatura
$sql="select ano, grado, asignatura, activprom from asignaturas where id_asignatura = '$id_asignatura' limit 1";
$datasig = viewsql($sql);
foreach($datasig as $regasig)
{
	$ano = $regasig->ano;
	$grado = $regasig->grado;
	$asignatura = $regasig->asignatura;
	$activprom = $regasig->activprom;
}
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$areasper = $reganol->areasper;
	$notalim = $reganol->notalim;
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notaevi = $reganol->notaevi;
}
//inactivar asignatura
$inactivar = 0;
$notas = "y".$ano."notas";
$sql="SELECT inactivar FROM $notas WHERE id_alumno = '$id_alumno' AND id_asignatura = '$id_asignatura' AND pertipo = '$pertipo' AND periodo = '$periodo' LIMIT 1";
$dattot=viewsql($sql);
foreach($dattot as $regtot)
{$inactivar = $regtot->inactivar;}
//inactivar logro por curso
$noactivar = 0;
$chklogro = "";
$datlogro = datos_logro($ano,$id_logro);
if (strpos($datlogro["cursos"],$curso) > 0)
{
	$chklogro = "checked";
	$noactivar = 1;
}
//traer evidencias
$file = "y".$ano."eviden";
$sql="SELECT * FROM $file WHERE id_asignatura = '$id_asignatura' and id_logro = '$id_logro' and periodo = '$periodo' order by numero";
$datevide=viewsql($sql);
//6. traer actividades de los logros de la asignatura y periodo
$file = "y".$ano."activ";
$sql = "SELECT * FROM $file WHERE id_logro = '$id_logro' AND periodo = '$periodo' ORDER BY numero";
$datacti = viewsql($sql);
$numacti = count($datacti);
$notasdet = "y".$ano."notasp".$periodo;
$readonly="readonly";
$action="";
if (($_SESSION["nivnot"] >= 2 and $estado == "ABIERTA") or $_SESSION["nivnot"] >= 4)
{
	$readonly="";
	$action="database/grabar_planillas.php";
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript'>
function checkNota(obj,idalumno)
{
	var valobj = obj.value;
	var total = 0;
	var totalQ = '';
	var notamin = '<?php echo $notamin;?>';
	var notamax = '<?php echo $notamax;?>';
	var numacti = '<?php echo $numacti;?>';
	var activprom = document.form1.activprom.value;
	notamin = parseInt(notamin,10);
	notamax = parseInt(notamax,10);
	numacti = parseInt(numacti,10);
	var idlogro = eval ('document.form1.id_logro.value');
	if (valobj=='' || valobj=='NaN')
		var intNota = 0;
	else
		var intNota = parseInt(valobj,10);
	if (intNota > 0)
	{
		//validar minimo y maximo de la nota
		if (intNota < notamin)
		{
			obj.value = '';
			obj.focus();
			alert('Nota inv�lida, m�nima '+notamin);
			return false;
		}
		if (intNota > notamax)
		{
			obj.value = '';
			obj.focus();
			alert('Nota inv�lida, m�xima '+notamax);
			return false;
		}
	}
	else
	{
		obj.value = '';
	}
	//calcular nota total sumando porcentajes de actividades
	var notact = 0;
	var poract = 0;
	for(var i=1;i<=numacti;i++){
		notact = eval ('document.form1.alu'+idalumno+'act'+i+'.value');
		poract = eval ('document.form1.alu'+idalumno+'por'+i+'.value');
		if (notact=='' || notact=='NaN')
			notact = 0;
		else
			notact = parseInt(notact,10);
		if (poract=='' || poract=='NaN')
			poract = 0;
		else
			poract = parseInt(poract,10);
		if (activprom <= 0 && notact > 0 && poract > 0) //actividad por porcentaje
		{
			notact = notact*(poract/100);
			notact = Math.round(notact*100)/100;
		}
		total = total + notact;
	}
	if (activprom > 0 && numacti > 0) //actividad por promedio
		total = total/numacti;
	//ajustar topes nota total logro
	if (total > 0)
	{
		total = Math.round(total * 10) / 10; //redondear a 1 decimal			
		total = parseInt(total,10); //tomar parte entera
	}
	if (total < notamin && total > 0)
		total = notamin; 
	if (total > notamax)
		total = notamax;
	//retornar nota total logro
	eval ("document.form1.nota"+idalumno+".value='"+total+"'");
	//retornar nota total logro a la planilla padre
	eval ("opener.document.form1.alu"+idalumno+"log"+idlogro+".value='"+total+"'");
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/actividades.png' width="28" height="28" border="0" align="absmiddle"> PERIODO <?php echo $periodo;?> - ACTIVIDADES <?php echo $asignatura;?></td>
		<td class="subtitulo" align="right">GRADO <?php echo $grado;?> A�O <?php echo $ano;?></td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td colspan="2">
		<table border="0" cellpadding="0" cellspacing="0" width="100%" class="tabla_color">
			<tr height="20"><td width="15"></td><td width="100"><img src="images/alumnosm.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"> <b>ALUMNO:</b></td><td><b><?php echo $alumno;?></b></td></tr>
			<tr height="20"><td></td><td><img src="images/logrosm.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"> <b>LOGRO <?php echo $datlogro["logrocod"];?>:</b></td><td><b><?php echo $datlogro["logro"];?></b></td></tr>			
		</table>
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="<?php echo $action;?>" method="post" name="form1">
<input type='hidden' name='op' value='planilla_logro'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='grado' value='<?php echo $grado;?>'>
<input type='hidden' name='curso' value='<?php echo $curso;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='id_logro' value='<?php echo $id_logro;?>'>
<input type="hidden" name="periodo" value="<?php echo $periodo;?>">
<input type="hidden" name="estado" value="<?php echo $estado;?>">
<input type="hidden" name="tipoedu" value="<?php echo $tipoedu;?>">
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type='hidden' name='notamin' value='<?php echo $notamin;?>'>
<input type='hidden' name='activprom' value='<?php echo $activprom;?>'>
	<tr height="20">
		<th width="30">N�</th>
		<th>Actividad</th>
		<?php if ($activprom > 0) { ?>
		<th width="35"></th>
		<?php } else { ?>
		<th width="35" title="Porcentaje">%Act</th>
		<?php } ?>
		<th width="50">Nota</th>
	</tr>
	<?php
	$campolog = "nota".$id_alumno;
	$inactivarlog = 0;
	$notalogro = "";
	$notalogroEx = "";
	$chkpen = "";
	$chkina = "";
	$sql="select nota, notaEx, pendiente, inactivar from $notasdet ".
		"where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
		"and id_actividad = '0' limit 1";
	$datnota=viewsql($sql);
	foreach($datnota as $regnota)
	{
		$notalogro = intval($regnota->nota);
		if ($regnota->notaEx > 0)
			$notalogroEx = intval($regnota->notaEx);
		$inactivarlog = intval($regnota->inactivar);
		if ($regnota->pendiente > 0 and $inactivarlog==0)
			$chkpen = "checked";
		if ($inactivarlog > 0)
			$chkina = "checked";
	}
	$num = 0;
	//capturar notas actividades del logro
	foreach($datacti as $regacti)
	{
		$num = $num +1;
		$id_actividad = $regacti->id_actividad;
		$porcentaje = "";
		if ($activprom <= 0)
			$porcentaje = "% ".$regacti->porcentaje;
		$campoact = "alu".$id_alumno."act".$id_actividad;
		$campopor = "alu".$id_alumno."por".$num;
		$campoid = "alu".$id_alumno."act".$num;
		$notaact = "";
		$sql="select nota from $notasdet ".
			"where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
			"and id_actividad = '$id_actividad' limit 1";
		$datnota=viewsql($sql);
		foreach($datnota as $regnota)
		{$notaact = intval($regnota->nota);}
		//editar notas alumno
		$editar = 1;
		if (substr($fchretiro,0,1) > 0 or $inactivar > 0 or $inactivarlog > 0) //alumno retirado o asignatura inactiva o logro inactivo
			$editar = 0;
		?>		
		<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
			<td align="center"><?php echo $regacti->numero;?></td>
			<td><?php echo $regacti->actividad;?></td>
			<td align="center"><?php echo $porcentaje;?></td>
			<td align="center">
				<?php if ($editar==1) { ?>
				<input type="text" name="<?php echo $campoact;?>" id="<?php echo $campoid;?>" value="<?php echo $notaact;?>" <?php echo $readonly;?> maxlength="3" style="width: 25px;" onBlur="javascript:checkNota(this,'<?php echo $id_alumno;?>');" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } else { ?>
				<input type="text" name="<?php echo $campoact;?>" id="<?php echo $campoid;?>" value="<?php echo $notaact;?>" readonly="readonly"  style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } ?>
				<input type="hidden" name="<?php echo $campopor;?>" id="<?php echo $campopor;?>" value="<?php echo intval($regacti->porcentaje);?>">
			</td>
		</tr>
	<?php 
	} //fin actividades ?>
	<tr height="22">
		<th colspan="3" align="right">
		Logro Pendiente
		<input type="checkbox" name="chkpen" id="chkpen" <?php echo $chkpen;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
		&nbsp;
		Inactivo
		<?php if ($_SESSION["nivnot"] >= 4) { ?>
		<input type="checkbox" name="chkina" id="chkina" <?php echo $chkina;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
		<?php } else { ?>
		<input type="checkbox" name="chkina" id="chkina" <?php echo $chkina;?> readonly="readonly" onclick="javascript: return false;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
		<?php } ?>
		&nbsp;&nbsp;
		NOTA LOGRO &nbsp;
		</th>
		<th><input type="text" name="<?php echo $campolog;?>" id="<?php echo $campolog;?>" value="<?php echo $notalogro;?>" readonly style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></th>
	</tr>
	<tr height="22">
		<th colspan="3" align="right">
		AJUSTE EXAMENES &nbsp;
		</th>
		<th><?php echo $notalogroEx;?></th>
	</tr>
	<tr height="36">
		<td colspan="4" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr>
				<td align="left">
				&nbsp;<b>INACTIVAR LOGRO EN EL CURSO</b>
				<?php if ($_SESSION["nivnot"] >= 4) { ?>
				<input type="checkbox" name="chklogro" <?php echo $chklogro;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
				<?php } else { ?>
				<input type="checkbox" name="chklogro" <?php echo $chklogro;?> readonly="readonly" onclick="javascript: return false;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
				<?php } ?>
				<input type="hidden" name="noactivar" value="<?php echo $noactivar;?>">
				</td>
				<td align="right">
				<?php if ($action!="") { ?>
				<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar" align="absmiddle">
				<?php } ?>
				<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
				</td>
			</tr>
			</table>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
