<?php
/***********************************************************************************************
* @file        : asignaturas_activ.php                                                         *
* @brief       : Lista y edita actividades por logro y asignatura                              *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
$periodo = $_GET["periodo"];
$id_asignatura = $_GET["id_asignatura"];
$id_logro = $_GET["id_logro"];
$id_actividad = isset($_GET['id_actividad']) ? $_GET['id_actividad'] : null ;
connect();
$datasignatura = datos_asignatura($id_asignatura);
$ano = intval($datasignatura["ano"]);
$activprom = intval($datasignatura["activprom"]);
$datlogro = datos_logro($ano,$id_logro); //datos logro
$activmin = intval($datlogro["activmin"]);
$activpor = $datlogro["activpor"];
$activmax = $activmin;
//traer acividades
$file = "y".$ano."activ";
$sql="SELECT * FROM $file WHERE id_asignatura = '$id_asignatura' and id_logro = '$id_logro' and periodo = '$periodo' order by numero";
$datacti=viewsql($sql);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/actividades.js'></script>
<script language="javascript" type='text/JavaScript'>
function checkPor(obj)
{
	var valobj = obj.value;
	var activmax = eval ('document.form1.activmax.value');
	activmax = parseInt(activmax,10);
	if (valobj=='' || valobj=='NaN')
		var intPor = 0;
	else
		var intPor = parseInt(valobj,10);
	//calcular porcentaje total sumando porcentajes de actividades
	var total = 0;
	var poract = 0;
	for(var i=1;i<=activmax;i++){
		poract = eval ('document.form1.porcentaje'+i+'.value');
		if (poract=='' || poract=='NaN')
			poract = 0;
		else
			poract = parseInt(poract,10);
		total = total + poract;
		if (total > 100)
		{
			total = 100;
			pordis = 100 - total;
			eval ("document.form1.porcentaje"+i+".value='"+pordis+"'");
		}
	}
	//retornar porcentaje total acumulado
	eval ("document.form1.portotal.value='"+total+"'");
}
function validar()
{
	var prome = document.form1.activprom.value;
	var activmin = eval ('document.form1.activmin.value');
	activmin = parseInt(activmin,10);
	var activmax = eval ('document.form1.activmax.value');
	activmax = parseInt(activmax,10);
	var numreg = 0;
	if (prome <= 0)
	{
		var portotal = eval ('document.form1.portotal.value');
		portotal = parseInt(portotal,10);
		if(portotal < 100)
		{alert('Actividades incompletas, porcentaje total inferior a 100...');document.form1.portotal.focus(); return false;}
		var codact = '';
		var nomact = '';
		var poract = 0;
		for(var i=1;i<=activmax;i++){
			poract = eval ('document.form1.porcentaje'+i+'.value');
			if (poract=='' || poract=='NaN')
				poract = 0;
			else
				poract = parseInt(poract,10);
			if (poract > 0)
			{
				codact = eval ('document.form1.codigo'+i+'.value');
				nomact = eval ('document.form1.actividad'+i+'.value');
				if (codact == '')
					{alert('Actividad '+i+' incompleta, falta c�digo...'); return false;}
				if (nomact == '')
					{alert('Actividad '+i+' incompleta, falta descripci�n...'); return false;}
				numreg = numreg + 1;
			}
			else
			{
				if (i <= activmin)
					{alert('Actividad '+i+' incompleta, falta porcentaje...'); return false;}
			}
		}
	}
	else
	{
		for(var i=1;i<=activmax;i++){
			codact = eval ('document.form1.codigo'+i+'.value');
			nomact = eval ('document.form1.actividad'+i+'.value');
			if (codact == '')
				{alert('Actividad '+i+' incompleta, falta c�digo...'); return false;}
			if (nomact == '')
				{alert('Actividad '+i+' incompleta, falta descripci�n...'); return false;}
			numreg = numreg + 1;
		}
	}
	if (numreg < activmin)
	{alert('Actividades incompletas, n�mero m�nimo '+activmin); return false;}
	return true;
}

function borrarconf()
{
	var periodo = document.form1.periodo.value;
	ok=confirm('Esta seguro de eliminar todas las actividades del Logro para el periodo '+periodo+' ?');
	if(ok)
	{
		document.form1.op.value='borraractividades';
		document.form1.submit();
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="5" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/actividades.jpg' width="40" height="40" border="0" align="absmiddle"> ACTIVIDADES LOGRO <?php echo $datlogro["logrocod"];?> - PERIODO <?php echo $periodo;?></td>
		<td class="subtitulo" align="right"><?php echo $datasignatura["asignatura"];?><BR>GRADO <?php echo $datasignatura["grado"];?> A�O <?php echo $datasignatura["ano"];?></td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr><td colspan="2" height="40" class="tabla_color"><p align="justify"><b>LOGRO <?php echo $datlogro["logrocod"];?>: <?php echo $datlogro["logro"];?></b></p></td></tr>
	<tr><td colspan="2" height="10"></td></tr>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabaractividades'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='periodo' value='<?php echo $periodo;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='id_logro' value='<?php echo $id_logro;?>'>
<input type='hidden' name='activmin' value='<?php echo $activmin;?>'>
<input type='hidden' name='activmax' value='<?php echo $activmax;?>'>
<input type='hidden' name='activprom' value='<?php echo $activprom;?>'>
	<tr height="380" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="30">Nro</th>
					<th width="60">C�digo</th>
					<th>Actividad Evaluada</th>
					<?php if ($activprom > 0) { ?>
					<th width="35"></th>
					<?php } else { ?>
					<th width="35" title="Porcentaje">%Act</th>
					<?php } ?>
				</tr>
				<?php
				$num = 0;
				$portotal = 0;
				foreach($datacti as $regacti)
				{
				$num = $num + 1;
				$id_actividad = $regacti->id_actividad;
				$nameid = "id".$num;
				$namenum = "numero".$num;
				$namecod = "codigo".$num;
				$nameact = "actividad".$num;
				$namepor = "porcentaje".$num;
				$portotal = $portotal + intval($regacti->porcentaje);
				?>		
				<tr height="25" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center"><input type="text" name="<?php echo $namenum;?>" style="width: 25px;" maxlength="3" value="<?php echo $regacti->numero;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td><input type="text" name="<?php echo $namecod;?>" size="10" style="text-transform:uppercase;" value="<?php echo $regacti->codigo;?>" maxlength="10" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td>
						<input type="text" name="<?php echo $nameact;?>" size="117" style="text-transform:uppercase;" value="<?php echo $regacti->actividad;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<input type="hidden" name="<?php echo $nameid;?>" value="<?php echo $id_actividad;?>">
					</td>
					<?php if ($activprom > 0) { ?>
					<td align="center"><input type="text" name="<?php echo $namepor;?>" id="<?php echo $namepor;?>" style="width: 25px;" readonly="readonly"  value="" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<?php } else { ?>
					<td align="center"><input type="text" name="<?php echo $namepor;?>" id="<?php echo $namepor;?>" style="width: 25px;" maxlength="3" value="<?php echo $regacti->porcentaje;?>" onBlur="javascript:checkPor(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<?php } ?>
				</tr>
				<?php
				}
				if ($num < $activmax)
				{
				$ini = $num+1;
				if ($ini == 1)
					$pos = 0;
				else
					$pos = ($ini-1)*3;
				for($i=$ini; $i <= $activmax ; $i++)
				{
				$nameid = "id".$i;
				$namenum = "numero".$i;
				$namecod = "codigo".$i;
				$nameact = "actividad".$i;
				$nameper = "periodo".$i;
				$namepor = "porcentaje".$i;
				$numero = "";
				$valpor = "";
				if ($i <= $activmin)
				{
					$numero = $i;
					$valpor = substr($activpor,$pos,3);
					$pos = $pos + 3;
					$portotal = $portotal + intval($valpor);
				}
				?>
				<tr height="25" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center"><input type="text" name="<?php echo $namenum;?>" value="<?php echo $numero;?>" style="width: 30px;" maxlength="3" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td><input type="text" name="<?php echo $namecod;?>" size="10" style="text-transform:uppercase;" maxlength="10" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td><input type="text" name="<?php echo $nameact;?>" size="117" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center">
					<?php if ($activprom > 0) { ?>
					<input type="text" name="<?php echo $namepor;?>" id="<?php echo $namepor;?>" value="" readonly="readonly" style="width: 30px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					<?php } else { ?>
					<input type="text" name="<?php echo $namepor;?>" id="<?php echo $namepor;?>" value="<?php echo $valpor;?>" style="width: 30px;" maxlength="3" onBlur="javascript:checkPor(this);" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					<?php } ?>
					<input type="hidden" name="<?php echo $nameid;?>" value="0">
					</td>
				</tr>
				<?php
				} }
				if ($activprom > 0) { ?>
				<tr><th colspan="4" align="right">PROMEDIO ACTIVIDADES</th></tr>
				<?php } else { ?>
				<tr><th colspan="3" align="right">TOTAL PORCENTAJES</th><th><input type="text" name="portotal" id="portotal" style="width: 30px;" value="<?php echo $portotal;?>" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;"></th></tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 2 and $ano == $_SESSION["anoaca"]) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_actividad > 0 and $_SESSION["nivasi"] >= 3) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10"></a>
			<?php } ?>
			<a href="asignaturas_logros.php?id_asignatura=<?php echo $id_asignatura;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Regresar lista Logros" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
