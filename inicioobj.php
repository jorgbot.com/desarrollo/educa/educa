<?php
/***********************************************************************************************
* @file        : inicio.php                                                                    *
* @brief       : P�gina de incio, informaci�n general, autorizada a todos los usuarios         *
* @version     : 2.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 01-Jul-2019                                                    *
* @Modificado  : Nora Rodriguez 13-Jul-2019                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
//include("database/database.php");
require_once("database/conectar.php"); 
include("libs/fechas.php");
include("libs/image_resize.php");
$ano = 2019;
//connect();
//include("includes/registros_cumple.php");

$periodos=new Periodos(); //instanciamos el objeto Productos
$periodo=$periodos->get_periodos();
$size=sizeof($periodo);
echo "SIZE $size<br>";
                                                               
for($i=0;$i<sizeof($periodo);$i++){
    $pernomb = $periodo[$i]["pernomb"];
        echo $pernomb."<br/>";
        
    }                                                                

?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />    
<meta name="robots" content="noindex" />
<meta name="googlebot" content="noindex" />
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" href="estilos.css" />
<link rel="stylesheet" href="estilosmenu.css" />
<link rel="stylesheet" href="estilosfoto.css" />
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fotos.js'></script>
<title>Educaci�n</title>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="1">
        <td></td>
        <td width="1000"></td>
        <td></td>
    </tr>
    <tr height="120">
        <td background="fondos/fondolat1.jpg">&nbsp;</td>
	<td width="1000" align="center"><?php include("includes/encabezado.php");?></td>
	<td background="fondos/fondolat2.jpg">&nbsp;</td>
    </tr>
    <tr>
        <td></td>
	<td align = "center" bgcolor="#FFFFFF"><?php include("includes/menu_principal.php");?></td>
	<td></td>
    </tr>
    <tr height="535">
        <td></td>
	<td valign="top"  bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="80">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10"></td>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr valign="top">
							<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr height="32">
									<td width="6" background="images/backsilver401.gif"></td>
									<td align="left" background="images/backsilver402.gif" class="titulo"> 
                                                                            Peri&oacute;dos Acad&eacute;micos <?php echo $ano;?>
									</td>
									<td width="6" background="images/backsilver403.gif"></td>
								</tr>
							</table>
							<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
								<tr height="20">
									<th></th>
									<th>Estado</th>
									<th>Inicio</th>
									<th>Final</th>
									<th>Fallas</th>
									<th>Plazo Notas</th>
									<th>Boletines</th>
								</tr>
							</table>
							</td>
							<td width="10"></td>
							<td width="300">
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<div id="capaVentana" onmouseout="this.style.display='none'"  onclick="this.style.display='none'" onmouseover="this.style.display='block'">
		<table border="0" cellpadding="1" cellspacing="0" width="100%">
		<form action="" method="post" name="formulario">
			<tr><th><span id="data" style="background-color: #E0E7E9; font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight: bold; padding-top: 0px; padding-bottom: 0px;"></span></th></tr>
			<tr>
				<td height="160" align="center">
					<?php
					if ($foto == "")
					{$foto = "images/foto.gif"; $height = 80; $width = 80;}
					else
					{$height = 150; $width = 150;}
					?>
					<img name="foto" src="<?php echo $foto;?>" width="<?php echo $width;?>" height="<?php echo $height;?>" border="0" align="absmiddle">
				</td>
			</tr>
		</form>
		</table>
		</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" valign="top">
            <div id="fondo_pie">
            <?php include("includes/menu_bottom.php");?>
            </div>
        </td>
    </tr>
</table>
</body>
</html>
<?php disconnect();?>