<?php
/***********************************************************************************************
* @file        : pagos_edit.php                                                                *
* @brief       : Crear o Editar pagos de matricula, pensiones, transporte  vvv                 *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 17-Jul-2015                                                    *
* @Modificado  : Nora Rodriguez 06-nov-2018                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_pagos.php");
include("libs/fechas.php");
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$id_pago = isset($_GET['id_pago']) ? $_GET['id_pago'] : 0 ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : 0 ;
$id_entrada = isset($_GET['id_entrada']) ? $_GET['id_entrada'] : null ;
$fchentrada = isset($_GET['fchentrada']) ? $_GET['fchentrada'] : date('Y-m-d') ;
$ano = isset($_GET['ano']) ? $_GET['ano'] :  $_SESSION["anoaca"] ;
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : "liquidar" ;
$op = isset($_GET['op']) ? $_GET['op'] : "" ;
if ($op=="")
	$op = isset($_POST['op']) ? $_POST['op'] : "buscar";
if ($op=="listar")
{
	$op = "listar";
	$tipo = $_POST['tipo'];
	$buscar = $_POST['buscar'];
	$buscargrado = $_POST['buscargrado'];
	$ano = $_POST['ano'];
	$id_entrada = $_POST['id_entrada'];
	$pos = strpos($id_entrada,"-");
	$fchentrada = substr($id_entrada,$pos+1);
	$id_entrada = substr($id_entrada,0,$pos);
}
if ($op=="seleccionar")
{
	$op = "editar";
	$tipo = $_POST['tipo'];
	$ano = $_POST['ano'];
	$id_alumno = $_POST['id_alumno'];
	$id_entrada = $_POST['id_entrada'];
	$fchentrada = $_POST['fchentrada'];
}
if ($op=="buscar" and $id_alumno > 0)
{
	$buscar = $id_alumno;
	$buscargrado = "";
	$op = "listar";
}
connect();
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/pagos.js'></script>
</head>
<body>
<?php include("includes/menu_pago.php");?>
<?php if ($op == "buscar") { ?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
	<tr height="200">
		<td align="center" valign="top">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<form action="pagos_edit.php" method="post" name="form0">
			<input type="hidden" name="op" value="listar">
			<input type="hidden" name="tipo" value="<?php echo $tipo;?>">
			<input type="hidden" name="ano" value="<?php echo $ano;?>">
				<tr height="200">
					<td align="center">
						BUSCAR ALUMNO...
						<input type="text" name="buscar" size="25" style="text-transform:uppercase;" autofocus> 
						&nbsp;
						GRADO
						<input type="text" name="buscargrado" size="3"> 
						&nbsp;
						ENTRADA
						<select name="id_entrada">
						<?php
						if ($tipo=="T")
							$file = "y".$ano."pagostra";
						else
							$file = "y".$ano."pagosent";
						$sql="SELECT id_entrada, fchentrada FROM $file WHERE estado != 'CERRADA' ORDER BY id_entrada desc";
						$datent=viewsql($sql);
						foreach($datent as $regent)
						{
						 $efec = fecha_larga($regent->fchentrada);
						 $sele = "";
						 if ($regent->id_entrada==$id_entrada)
						 	$sele = "selected";
						 echo "<option value='$regent->id_entrada-$regent->fchentrada' $sele>$regent->id_entrada - $efec</option>";
						}
						?>
						</select>
						&nbsp;
						<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
					</td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<a href="javascript:;" onClick="window.opener.document.location.reload(); window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</table>
<?php }
if ($op == "listar")
{
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
	<form action="pagos_edit.php" method="post" name="form0" onSubmit="return validarAlumno();">
	<input type="hidden" name="op" value="seleccionar">
	<input type="hidden" name="tipo" value="<?php echo $tipo;?>">
	<input type='hidden' name='id_entrada' value='<?php echo $id_entrada;?>'>
	<input type='hidden' name='fchentrada' value='<?php echo $fchentrada;?>'>
	<input type="hidden" name="ano" value="<?php echo $ano;?>">
	<tr height="320">
		<td colspan="2" align="center" valign="top">
			<iframe id="MyFrame" src="pagos_selectalum.php?tipo=<?php echo $tipo;?>&ano=<?php echo $ano;?>&buscar=<?php echo $buscar;?>&buscargrado=<?php echo $buscargrado;?>&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>" height="400" width="100%" scrolling="yes" frameborder="0"></iframe>
		</td>
	</tr>
	<tr height="36" bgcolor="#CCCCCC" background="images/backsilver.gif">
		<td valign="middle">&nbsp;&nbsp;SELECCIONAR ALUMNO # <input type="text" name="id_alumno" size="8" autofocus></td>
		<td align="right">
			<a href="pagos_edit.php?op=buscar&tipo=<?php echo $tipo;?>&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&ano=<?php echo $ano;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Regresar" hspace="5" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.opener.document.location.reload(); window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
	</form>
</table>
<?php } if ($op == "editar") {
//Ajuste intereses sobre pension 
if ($tipo=="I")
	echo"<script>window.location.href='pagos_ajuste.php?id_alumno=$id_alumno&ano=$ano&id_entrada=$id_entrada&fchentrada=$fchentrada';</script>";
//Datos del pago
$titulo = "Nuevo Pago";
$anopago = substr($fchentrada,0,4); //a�o hoja entrada activa
$datpago = datos_pago($id_pago,$ano);
$estado = "ABIERTA";
$porcentaje = 0;
$sql="SELECT interes FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcentaje = $reganol->interes;}
if ($id_pago > 0)
{
	$titulo = "PAGO ID ".$id_pago;
	$id_alumno = $datpago["id_alumno"];
	$id_entrada = $datpago["id_entrada"];
	$fchentrada = $datpago["fchentrada"];
	$anopago = substr($datpago["fchpago"],0,4);
	$ano = $datpago["ano"];
	if ($tipo=="T")
		$file = "y".$ano."pagostra";
	else
		$file = "y".$ano."pagosent";
	$sql="select estado from $file where id_entrada = '$id_entrada' limit 1";
	$datent=viewsql($sql);
	foreach($datent as $regent)
	{$estado = $regent->estado;}
}
else
{
	$datpago["id_alumno"] = $id_alumno;
	$datpago["id_entrada"] = $id_entrada;
	$datpago["fchentrada"] = $fchentrada;
	$datpago["tipo"] = $tipo;
	$datpago["porcentaje"] = $porcentaje;
}
//traer datos matriculas y nombres del a�o activo en pagos
$alumno = "";
if ($tipo=="P")
	$sql="SELECT matriculas.ano, matriculas.grado, matriculas.curso, alumnos.apellidos, alumnos.nombres FROM matriculas, alumnos WHERE matriculas.id_alumno = '$id_alumno' AND matriculas.ano = '$ano' AND  matriculas.id_alumno = alumnos.id_alumno LIMIT 1";
else
	$sql="SELECT transporte.ano, transporte.grado, transporte.curso, alumnos.apellidos, alumnos.nombres FROM transporte, alumnos WHERE transporte.id_alumno = '$id_alumno' AND transporte.ano = '$ano' AND  transporte.id_alumno = alumnos.id_alumno LIMIT 1";
$datmat=viewsql($sql);
foreach($datmat as $regmat)
{
	$alumno = $regmat->apellidos." ".$regmat->nombres;
	if ($id_pago <= 0)
	{
		$datpago["grado"] = $regmat->grado;
		$datpago["curso"] = $regmat->curso;
		$datpago["ano"] = $regmat->ano;
	}
	$grado = $datpago["grado"];
	$curso = $datpago["curso"];
}
if ($id_alumno <= 0 or $alumno=="")
	echo"<script>window.location.href='pagos_edit.php?tipo=$tipo&id_entrada=$id_entrada&fchentrada=$fchentrada&ano=$ano';</script>";
//cargar valores del grado
$metodo = "I"; //Intereses o Rangos
$pensionbas = 0;
$pensionfin = 0;
$pensionret = 0;
$sql="SELECT pension, pensionfin, pensionret FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{$pensionbas = $reggrad->pension; $pensionfin = $reggrad->pensionfin; $pensionret = $reggrad->pensionret;}
if ($pensionfin > 0 and $pensionret > 0) {
    $metodo = "R";
    $porcentaje = 0;
    $datpago["porcentaje"] = 0;
}
//cargar valores del alumno
if ($tipo=="P") //Pensi�n
{
	$valores = valores_pension($id_alumno,$ano); //arreglo pensiones del alumno
	$pagos = pagos_pension($id_alumno,$ano); //arreglo pagos del alumno
}
else //Transporte
{
	$valores = valores_transporte($id_alumno,$ano); //arreglo transportes del alumno
	$pagos = pagos_transporte($id_alumno,$ano); //arreglo pagos del alumno
}
$pagosdet = "y".$ano."pagosdet";
$sql="SELECT * FROM $pagosdet WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' ORDER BY id_pago, mes";
$datdet = viewsql($sql);

//Liquidar saldos a la fecha de hoy
$fchhoy = date('Y-m-d');
//$fchhoy = '2017-04-01';
$anohoy = intval(substr($fchhoy,0,4));
$meshoy = intval(substr($fchhoy,5,2));
$diahoy = intval(substr($fchhoy,8,2));
$valortot = 0;
$intertot = 0;
$saldotot = 0;
$saldos = array(); //arreglo saldos a pagar
$interes = array(); //arreglo intereses a pagar	
$interesliq = array(); //arreglo intereses liquidados
//Liquidar siempre saldos e intereses a la fecha de hoy
for($i=1; $i<=11 ;$i++)
{
	if ($i <= 9)
		$mes = "0".$i;
	else
		$mes = $i;
	$saldos[$mes] = 0;
	$interes[$mes] = 0;
	$interesliq[$mes] = 0;
}

/*
for($i=1; $i<=11 ;$i++)
{
	$saldo = 0;
	$valinteres = 0;
        if ($metodo == "R" and $valores[$mes]==$pensionbas) {
            $diapago = substr($fchhoy,8,2);
            $mespago = substr($fchhoy,5,2);
            $anopago = substr($fchhoy,0,4);
            if ($diapago >= 16 and $mespago == $mes) {
                $valores[$mes] = $pensionfin;  
            }
            if ($mespago > $mes or $anopago > $ano) {
                $valores[$mes] = $pensionret;  
            }
        }
	$saldo = $valores[$mes] - $pagos[$mes];
	$saldos[$mes] = $saldo;
	$salint = 0;
	if ($i > $meshoy)
		$saldo = 0;
	if ($tipo=="P" and $metodo == "I")
	{
		$campointsal = "intesal".$mes;					
		$salint = $pagos[$campointsal];
		if ($saldo > 0 and $i >= 2)
		{
			$valinteres = interes_pension($ano,$mes,$saldo,$porcentaje,$fchhoy);
			$interesliq[$mes] = $valinteres;
		}
		$salint = $salint + $valinteres;
		if ($salint < 0)
			$salint = 0;
		$interes[$mes] = $salint;				
	}
	$valortot = $valortot + $saldo;
	$intertot = $intertot + $salint;
	$saldotot = $saldotot + $saldo + $salint;
} //fin meses
*/

if ($id_pago==0)
{
	$datpago["valor_con"] = $saldotot;
	$datpago["valor"] = $saldotot;
	$datpago["neto"] = $valortot;
	$datpago["interes"] = $intertot;
}
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<?php if ($estado=="ABIERTA") { ?>
<form action="database/grabar_pagos.php" method="post" name="form1" onSubmit="return validarPago();">
<?php } else { ?>
<form action="" method="post" name="form1" onSubmit="return validarPago();">
<?php } ?>
<input type='hidden' name='op' value='grabar_pago'>
<input type='hidden' name='id_pago' value='<?php echo $id_pago;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $datpago["id_alumno"];?>'>
<input type='hidden' name='tipo' value='<?php echo $datpago["tipo"];?>'>
<input type='hidden' name='grado' value='<?php echo $datpago["grado"];?>'>
<input type='hidden' name='curso' value='<?php echo $datpago["curso"];?>'>
<input type='hidden' name='id_entrada' value='<?php echo $datpago["id_entrada"];?>'>
<input type='hidden' name='fchentrada' value='<?php echo $datpago["fchentrada"];?>'>
<input type='hidden' name='metodo' value='<?php echo $metodo;?>'>
<input type='hidden' name='pensionbas' value='<?php echo $pensionbas;?>'>
<input type='hidden' name='pensionfin' value='<?php echo $pensionfin;?>'>
<input type='hidden' name='pensionret' value='<?php echo $pensionret;?>'>
<input type='hidden' name='fchhoy' value='<?php echo date('d/m/Y');?>'>
	<tr height="25">
		<th width="15"></th>
		<th colspan="2" class="titulo" align="left"><?php echo "ID ".$id_alumno." ".$alumno.", ".$grado."&deg; CURSO ".$curso;?></th>
		<th align="right" class="titulo_rojo">
			<?php if ($id_pago > 0) { ?>
			ID PAGO N� <?php echo $id_pago;?>
			<?php } else { ?>
			NUEVO PAGO
			<?php } ?>
		</th>
		<th width="15"></th>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr>
		<td></td>
		<td colspan="3">
			<?php
			if ($tipo=="P")
				include("pagos_pension.php");
			else
				include("pagos_transporte.php");
			?>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr height="36" bgcolor="#CCCCCC" background="images/backsilver.gif">
		<td></td>
		<td colspan="4">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td align="left">
						<?php if ($id_pago > 0) { ?>
						ACTUALIZADO <?php echo substr($datpago["fchactual"],0,16);?> <?php echo $datpago["usuactual"];?>
						<?php } ?>
					</td>
					<td align="right" valign="middle">
						<?php if ($estado=="ABIERTA") { if (($tipo=="P" and $_SESSION["nivpag"] >= 2) or ($tipo=="T" and $_SESSION["nivtra"] >= 2)) { ?>
						<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="3" alt="Grabar" align="absmiddle">
						<?php } if ($id_pago > 0) { ?>
						<a href="pagos_edit.php?op=editar&tipo=<?php echo $tipo;?>&id_alumno=<?php echo $id_alumno;?>&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&ano=<?php echo $ano;?>"><img src='images/btn_nuevopago.png' border="0" width="90" height="24" alt="Nuevo" hspace="3" align="absmiddle" title="Nuevo Pago Alumno"></a>
						<?php } ?>
						<a href="pagos_edit.php?op=buscar&tipo=<?php echo $tipo;?>&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&ano=<?php echo $ano;?>"><img src="images/btn_buscar.png" border="0" width="100" height="24" alt="Regresar" hspace="3" align="absmiddle"></a>
						<?php if ($id_pago > 0 and ($tipo=="P" and $_SESSION["nivpag"] >= 4) or ($tipo=="T" and $_SESSION["nivtra"] >= 4)) { ?>
						<a href="javascript:;" onClick="borrarPago();"><img src="images/btn_eliminar.png" border="0" width="70" height="24" alt="Eliminar" hspace="3" align="absmiddle"></a>
						<?php } } if ($id_pago > 0) { ?>
						<a href="javascript:;" onClick="imprimirPago();"><img src="images/btn_printer.png" border="0" width="24" height="24" alt="Imprimir" hspace="3" align="absmiddle"></a>
						<?php } ?>
						<a href="javascript:;" onClick="window.opener.document.location.reload(); window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="3" align="absmiddle"></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</form>
</table>
<?php } if ($op == "historial") {
$alumno = "";
$porcen = 0;
if ($tipo=="P")
	$sql="SELECT matriculas.ano, matriculas.grado, matriculas.curso, matriculas.interes, alumnos.apellidos, alumnos.nombres FROM matriculas, alumnos WHERE matriculas.id_alumno = '$id_alumno' AND matriculas.ano = '$ano' AND  matriculas.id_alumno = alumnos.id_alumno LIMIT 1";
else
	$sql="SELECT transporte.ano, transporte.grado, transporte.curso, alumnos.apellidos, alumnos.nombres FROM transporte, alumnos WHERE transporte.id_alumno = '$id_alumno' AND transporte.ano = '$ano' AND  transporte.id_alumno = alumnos.id_alumno LIMIT 1";
$datmat=viewsql($sql);
foreach($datmat as $regmat)
{
	$alumno = $regmat->apellidos." ".$regmat->nombres;
	$grado = $regmat->grado;
	$curso = $regmat->curso;
        if ($tipo=="P")
          $porcen = $regmat->interes;
}
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="javascript:imprimirCuenta();" method="post" name="form1">
<input type="hidden" name="id_alumno" value="<?php echo $id_alumno;?>">
<input type="hidden" name="ano" value="<?php echo $ano;?>">
<input type='hidden' name='tipo' value='<?php echo $tipo;?>'>
	<tr height="25">
		<th width="15"></th>
		<th class="titulo" align="left"><?php echo "ID ".$id_alumno." ".$alumno.", ".$grado."&deg; CURSO ".$curso;?></th>
		<th width="15"></th>
	</tr>
	<tr height="370">
		<td colspan="3" align="center" valign="top">
			<br>
			<?php include("includes/pagos_histo.php");?>
			<br>
		</td>
	</tr>
	<tr height="35">
		<th colspan="3" align="right" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<a href="javascript:;" onClick="imprimirCuenta();"><img src="images/btn_imprimir.png" border="0" width="70" height="24" alt="Imprimir" hspace="5" align="absmiddle"></a>
			&nbsp;&nbsp;
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</th>
	</tr>
</form>
</table>
<br>
<?php } ?>
</body>
</html>
<?php disconnect();?>
