<?php
/***********************************************************************************************
* @file        : transporte.php                                                               *
* @brief       : Muestra la lista general de areas, materias y asignatutas por a�o             *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 25-ago-2012                                                    *
* @Modificado  : Nora Rodriguez 25-ago-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivtra"] <= 0)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</script>";}
include("database/database.php");
include("libs/fechas.php");
connect();
$id_entrada = isset($_GET['id_entrada']) ? $_GET['id_entrada'] : 0 ;
$buscaralu = isset($_POST['buscaralu']) ? $_POST['buscaralu'] : null ;
$buscarpag = isset($_POST['buscarpag']) ? $_POST['buscarpag'] : null ;
$buscarent = isset($_POST['buscarent']) ? $_POST['buscarent'] : null ;
$fecha = isset($_POST['fecha']) ? $_POST['fecha'] : null ;
$op = isset($_POST['op']) ? $_POST['op'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] :  $_SESSION["anoaca"] ;
$fchentrada = "";
$estado = "";
//lista de entradas transporte
$file = "y".$ano."pagostra";
if ($buscarent > 0 or $id_entrada > 0)
{
	if ($id_entrada > 0)
		$buscarent = $id_entrada;
	$sql="select id_entrada, fchentrada, estado from $file where id_entrada = '$buscarent' limit 1";
	$datent=viewsql($sql);
	foreach($datent as $regent)
	{
		$id_entrada = $regent->id_entrada;
		$fchentrada = $regent->fchentrada;
		$estado = $regent->estado;
	}
	if (count($datent) <= 0)
		$id_entrada = 0;
}
if ($id_entrada <= 0)
{
	$sql="select id_entrada, fchentrada, estado from $file order by id_entrada desc limit 1";
	$datent=viewsql($sql);
	foreach($datent as $regent)
	{
		$id_entrada = $regent->id_entrada;
		$fchentrada = $regent->fchentrada;
		$estado = $regent->estado;
	}
}
$sig_entrada = $id_entrada + 1;
$ant_entrada = $id_entrada - 1;
$file = "y".$ano."pagos";
$campos="SELECT $file.id_alumno, $file.grado, $file.curso, $file.id_pago, $file.id_banco, $file.id_entrada, $file.fchentrada, $file.tipo, $file.fchpago, $file.valor_con, $file.valor_efe, $file.valor, $file.neto, $file.interes, alumnos.apellidos, alumnos.nombres FROM $file, alumnos ";
$sql="";
if ($buscarpag > 0)
{
	$sql=$campos."WHERE $file.id_pago = '$buscarpag' AND $file.id_alumno = alumnos.id_alumno LIMIT 1";
}
else
{
	if ($buscaralu!="")
	{
		if ($buscaralu > 0)
			$sql="WHERE $file.id_alumno = '$buscaralu' AND $file.id_alumno = alumnos.id_alumno ";
		else
			$sql="WHERE $file.id_entrada = '$id_entrada' AND alumnos.apellidos LIKE '$buscaralu%' AND $file.id_alumno = alumnos.id_alumno ";
	}
	if ($fecha!="")
		$sql="WHERE $file.fchentrada = '".fec_amd($fecha)."' AND $file.id_alumno = alumnos.id_alumno ";
	if ($sql=="")
		$sql="WHERE $file.id_entrada = '$id_entrada' AND $file.id_alumno = alumnos.id_alumno ";
	$sql.="AND $file.tipo = 'T' ";
	if ($buscaralu > 0)
		$sql=$campos.$sql."ORDER BY $file.id_pago ";
	else
		$sql=$campos.$sql."ORDER BY alumnos.apellidos, alumnos.nombres, id_pago ";
}
$datpag=viewsql($sql);
//echo "$sql<br>";
?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>EDUCA</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="robots" content="noindex"/>
<meta name="googlebot" content="noindex"/>
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	var ano = document.form0.ano.value;
	var theURL = 'transporte.php?ano='+ano;
	window.location.href = theURL;
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#FFFF66">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="150" bgcolor="#006600">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr height="40" bgcolor="#006600">
		<td background="fondos/top.jpg"></td>
		<td background="fondos/top.jpg" class="titulo_color">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_top.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/top.jpg"></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_top.jpg"></td></tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="150">
					<?php include("includes/menu.php");?>
				</td>
				<td width="10">
				</td>
				<td align="center">
					<div class="div_marco">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr height="40">
							<td width="50"><img src='images/rutas.png' width="40" height="40" border="0" align="absmiddle"></td>
							<td class="titulo">
								<a href="transporte.php?id_entrada=<?php echo $ant_entrada;?>&ano=<?php echo $ano;?>"><img src="images/arrback.png" border="0" width="24" height="24" hspace="2" title="Entrada Anterior..." align="absmiddle"></a>
								<?php echo "PAGOS $ano"; if ($id_entrada > 0) echo " - ENTRADA ".$id_entrada." - ".fecha_larga($fchentrada);?>
								<a href="transporte.php?id_entrada=<?php echo $sig_entrada;?>&ano=<?php echo $ano;?>"><img src="images/arrforwardt.png" border="0" width="24" height="24" hspace="2" title="Entrada Siguiente..." align="absmiddle"></a>
							</td>
							<td align="right">
								<table border="0" cellpadding="0" cellspacing="0">
									<tr>
										<td>&nbsp;&nbsp;<a class="link_black" href="pagos_entradas.php?tipo=T&ano=<?php echo $ano;?>" onclick="newWindow(this.href, '', 600, 500, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Entradas..."><img src="images/entradas.png" width="32" height="32" border="0" hspace="0" align="absmiddle"> Hojas Entrada</a>&nbsp;&nbsp;</td>
										<td width="1" bgcolor="#FFCC00"></td>
										<td>&nbsp;&nbsp;<a class="link_black" href="transporte_rutas.php" onclick="newWindow(this.href, '', 900, 590, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Entradas..."><img src="images/rutas32.png" width="32" height="32" border="0" hspace="0" align="absmiddle"> Rutas</a>&nbsp;&nbsp;</td>
										<td width="1" bgcolor="#FFCC00"></td>
										<td>&nbsp;&nbsp;<a class="link_black" href="transporte_conduct.php" onclick="newWindow(this.href, '', 900, 590, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Entradas..."><img src="images/conductor32.png" width="32" height="32" border="0" hspace="0" align="absmiddle"> Conductores</a>&nbsp;&nbsp;</td>
										<td width="1" bgcolor="#FFCC00"></td>
										<td>&nbsp;&nbsp;<a class="link_black" href="transporte_monit.php" onclick="newWindow(this.href, '', 900, 590, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Entradas..."><img src="images/monitor.png" width="32" height="32" border="0" hspace="0" align="absmiddle"> Monitores</a>&nbsp;&nbsp;</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					</div>
					<br><br>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<form action="" method="post" name="form0">
					<input type="hidden" name="op" value="buscaralu">
						<tr height="32">
							<td width="6" background="images/backsilver401.gif"></td>
							<td background="images/backsilver402.gif" class="titulo" align="left">
								<select name="ano" style="font-size: 14px; font-weight: bold;" onChange="javascript:seleccionar();">
								<?php
								$sql="SELECT ano FROM anoslect ORDER BY ano desc";
								$datanol=viewsql($sql);
								foreach($datanol as $reganol)
								{
									$selano = "";
									if ($reganol->ano==$ano)
										$selano = "selected";
									echo "<option value='$reganol->ano' $selano>A�O $reganol->ano</option>";
								}
								?>
								</select>
							</td>
							<?php if ($id_entrada > 0) { ?>
							<td align="right" background="images/backsilver402.gif">
								ALUMNO N�<input type="text" name="buscaralu" value="<?php echo $buscaralu;?>" size="3" style="text-transform:uppercase;">
								&nbsp;
								PAGO N�<input type="text" name="buscarpag" size="3"> 
								&nbsp;
								ENTRADA N�<input type="text" name="buscarent" size="3"> 
								&nbsp;
								FECHA <input type="text" name="fecha" id="fecha" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form0.fecha, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<input type="image" class="botones_img" name="btnbuscaralu" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
								<?php if ($estado=="ABIERTA" and $_SESSION["nivtra"] >= 3) { ?>
								<a href="pagos_edit.php?tipo=T&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&ano=<?php echo $ano;?>" onclick="newWindow(this.href, '', 850, 580, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_nuevopago.png' border="0" width="90" height="24" alt="Nuevo" align="absmiddle" title="Nuevo registro de Pago"></a>
								<?php } ?>
								<a href="reports/rep_pagosent.php?id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&ano=<?php echo $ano;?>&tipo=T" title="Reporte..." onclick="newWindow(this.href, '', 1024, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_printer.png' border="0" width="24" height="24" alt="Imprimir" align="absmiddle" title="Imprimir Entrada"></a>
							</td>
							<?php } ?>
							<td width="6" background="images/backsilver403.gif"></td>
						</tr>
					</form>
					</table>
					<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="100%">
						<tr height="20">
							<th>Alumno</th>
							<th width="52">IDPago</th>
							<th width="38">IDAlum</th>
							<th width="20">Grado</th>
							<th width="30">Curso</th>
							<th colspan="2">Entrada</th>
							<th width="70">FechaPago</th>
							<th width="65">Consignado</th>
							<th width="55">Efectivo</th>
							<th width="65">Total</th>
						</tr>
						<?php
						$numpag = 0;
						$totcon = 0;
						$totefe = 0;
						$total = 0;
						foreach($datpag as $regpag)
						{
						$numpag = $numpag + 1;
						$totcon = $totcon + $regpag->valor_con;
						$totefe = $totefe + $regpag->valor_efe;
						$total = $total + $regpag->valor; 
						$id_pago = $regpag->id_pago;
						$id_alumno = $regpag->id_alumno;
						$tipo = $regpag->tipo;
						?>		
						<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
							<td><?php echo substr($regpag->apellidos." ".$regpag->nombres,0,35);?></td>
							<td><a class="link_list" href="pagos_edit.php?op=editar&id_pago=<?php echo $id_pago;?>&tipo=<?php echo $tipo;?>&ano=<?php echo $ano;?>&id_alumno=<?php echo $id_alumno;?>" onclick="newWindow(this.href, '', 850, 580, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="CODIGO"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $id_pago;?></a></td>
							<td align="center"><?php echo $regpag->id_alumno;?></td>
							<td align="center"><?php echo $regpag->grado;?>&deg;</td>
							<td align="center"><?php echo $regpag->curso;?></td>
							<td align="center" width="30"><?php echo $regpag->id_entrada;?></td>
							<td width="40"><?php echo substr(fecha_texto($regpag->fchentrada),0,6);?></td>
							<td><?php echo fecha_texto($regpag->fchpago);?></td>
							<td align="right"><?php echo number_format($regpag->valor_con,0);?></td>
							<td align="right"><?php echo number_format($regpag->valor_efe,0);?></td>
							<td align="right"><?php echo number_format($regpag->valor,0);?></td>
						</tr>
						<?php }
						if ($numpag <= 0) { ?>
						<tr height="100" bgcolor="#FFFFFF"><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
						<?php } ?>
						<tr height="20">
							<th colspan="8" align="left">&nbsp;&nbsp;Total <?php echo number_format($numpag,0);?></th>
							<th align="right"><?php echo number_format($totcon,0);?></th>
							<th align="right"><?php echo number_format($totefe,0);?></th>
							<th align="right"><?php echo number_format($total,0);?></th>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_bot.jpg"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#D3A919"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FFDB4B"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FCF463"></td></tr>
	<tr>
		<td colspan="3" valign="top">
		<div id="fondo_pie">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</div>
		</td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>