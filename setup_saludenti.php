<?php
/***********************************************************************************************
* @file        : setup_saludenti.php                                                           *
* @brief       : Lista y edita entidades de salud                                              *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 10-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 10-Mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_setup.php");
include("libs/fechas.php");
include("libs/image_resize.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$id_saludenti = isset($_GET['id_saludenti']) ? $_GET['id_saludenti'] : null ;
$op = isset($_GET['op']) ? $_GET['op'] : null ;
if ($op == "")
	$op = "listar";
connect();
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.entidad.value == '')
	{alert('Falta asignar nombre de entidad...');document.form1.entidad.focus(); return false;}
	return true;
}
function devuelve(campo1,campo2)
{
	eval ("opener.document.form1.id_saludenti.value='" +  campo1 + "'")
	eval ("opener.document.form1.entidad.value='" +  campo2 + "'")
	window.close()
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50"><td class="titulo"><img src='images/salud.jpg' width="40" height="40" border="0" align="absmiddle"> ENTIDADES DE SALUD</td></tr>
</table>
<?php if ($op == "listar") { ?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="380" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th>Entidad</th>
					<th width="150">Tel�fonos</th>
					<th width="50">ID</th>
				</tr>
				<?php
				$sql="SELECT * FROM saludenti order by entidad";
				$datsale = viewsql($sql);
				foreach($datsale as $regsale)
				{
				$id_saludenti = $regsale->id_saludenti;
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><a href="javascript:;" onClick="devuelve('<?php echo $id_saludenti;?>','<?php echo $regsale->entidad;?>');"><img src="images/ok.png" border="0" width="16" height="16" alt="Seleccionar" title="" hspace="0" align="absmiddle"><?php echo $regsale->entidad;?></a></td>
					<td><?php echo $regsale->telefono;?></td>
					<td align="center"><a class="link_list" href="setup_saludenti.php?op=editar&id_saludenti=<?php echo $id_saludenti;?>"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle">Editar</a></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivalu"] >= 3) { ?>
			<a href="setup_saludenti.php?op=editar"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</table>
<br/><br/>
<?php } if ($op == "editar") {
$titulo = "Nuevo Registro";
$datsaludenti = datos_saludenti($id_saludenti);
if ($id_saludenti > 0)
	$titulo = "Editar Entidad de Salud ID ".$id_saludenti;
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_setup.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarsaludenti'>
<input type='hidden' name='id_saludenti' value='<?php echo $id_saludenti;?>'>
	<tr height="30">
		<td width="20"></td>
		<td></td>
		<td width="20"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Entidad - ID <?php echo $id_saludenti;?></legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr>
					<td>Entidad <font color="red">*</font></td>
					<td><input type="text" name="entidad" size="60" value="<?php echo $datsaludenti["entidad"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				<tr>
				<tr>
					<td>Direcci�n</td>				
					<td><input type="text" name="direccion" size="60" value="<?php echo $datsaludenti["direccion"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				<tr>
				<tr>
					<td>Tel�fonos</td>				
					<td><input type="text" name="telefono" size="60" value="<?php echo $datsaludenti["telefono"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				<tr>
				<tr>
					<td>Correo</td>				
					<td><input type="text" name="correo" size="60" value="<?php echo $datsaludenti["correo"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				<tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr height="30"><td colspan="3"></td></tr>
	<tr height="35">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivalu"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar" align="absmiddle">
			<?php } if ($id_saludenti > '0' and $_SESSION["nivalu"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10" align="absmiddle"></a>
			<?php } ?>
			<a href="setup_saludenti.php"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
