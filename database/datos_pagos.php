<?php

Function datos_pago($id_pago,$ano)
{
	$datpago = array();
	$datpago["id_alumno"] = 0;
	$datpago["id_banco"] = 1;
	$datpago["id_entrada"] = 0;
	$datpago["fchentrada"] = "";
	$datpago["tipo"] = "P";
	$datpago["destino"] = 0;
	$datpago["ano"] = $ano;
	$datpago["grado"] = "";
	$datpago["curso"] = "";
	$datpago["fchpago"] = "";
	$datpago["valor"] = "";
	$datpago["valor_con"] = "";
	$datpago["valor_efe"] = "";
	$datpago["neto"] = "";
	$datpago["interes"] = "";
	$datpago["porcentaje"] = "";
	$datpago["fchactual"] = "";
	$datpago["usuactual"] = "";
	if ($id_pago > 0)
	{
		$pagos = "y".$ano."pagos";
		$sql="SELECT * FROM $pagos WHERE id_pago = '$id_pago' LIMIT 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$datpago["id_alumno"] = $reg->id_alumno;
			$datpago["id_banco"] = $reg->id_banco;
			$datpago["id_entrada"] = $reg->id_entrada;
			$datpago["fchentrada"] = $reg->fchentrada;
			$datpago["tipo"] = $reg->tipo;
			$datpago["destino"] = $reg->destino;
			$datpago["ano"] = $reg->ano;
			$datpago["grado"] = $reg->grado;
			$datpago["curso"] = $reg->curso;
			$datpago["fchpago"] = $reg->fchpago;
			$datpago["valor"] = $reg->valor;
			$datpago["valor_con"] = $reg->valor_con;
			$datpago["valor_efe"] = $reg->valor_efe;
			$datpago["neto"] = $reg->neto;
			$datpago["interes"] = $reg->interes;
			$datpago["porcentaje"] = $reg->porcentaje;
			$datpago["fchactual"] = $reg->fchactual;
			$datpago["usuactual"] = $reg->usuactual;
		}
	}
	return $datpago;
}

Function datos_ajuste($id_ajuste,$ano)
{
	$datajuste = array();
	$datajuste["id_alumno"] = 0;
	$datajuste["tipo"] = "I"; //Interes
	$datajuste["ano"] = $ano;
	$datajuste["mes"] = "";
	$datajuste["valor"] = "";
	$datajuste["detalles"] = "";
	$datajuste["fchactual"] = "";
	$datajuste["usuactual"] = "";
	if ($id_ajuste > 0)
	{
		$ajustes = "y".$ano."pagosaju";
		$sql="SELECT * FROM $ajustes WHERE id_ajuste = '$id_ajuste' LIMIT 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$datajuste["id_alumno"] = $reg->id_alumno;
			$datajuste["tipo"] = $reg->tipo;
			$datajuste["ano"] = $reg->ano;
			$datajuste["mes"] = $reg->mes;
			$datajuste["valor"] = $reg->valor;
			$datajuste["detalles"] = $reg->detalles;
			$datajuste["fchactual"] = $reg->fchactual;
			$datajuste["usuactual"] = $reg->usuactual;
		}
	}
	return $datajuste;
}


Function valores_pension($id_alumno,$ano)
{
	$valpen = array();
	$valpen["01"] = 0;
	$valpen["02"] = 0;
	$valpen["03"] = 0;
	$valpen["04"] = 0;
	$valpen["05"] = 0;
	$valpen["06"] = 0;
	$valpen["07"] = 0;
	$valpen["08"] = 0;
	$valpen["09"] = 0;
	$valpen["10"] = 0;
	$valpen["11"] = 0;
	$valpen["saldoFin"] = 0;
	$sql="SELECT matricula, grado, saldoFin FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		$valpen["01"] = intval($reg->matricula);
                $grado = $reg->grado;
                $valpen["saldoFin"] = intval($reg->saldoFin);
	}
        $pensionbas = 0;
        $sql="SELECT pension FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
        $datgrad=viewsql($sql);
        foreach($datgrad as $reggrad)
        {$pensionbas = $reggrad->pension;}
	$valpen["02"] = $pensionbas;
	$valpen["03"] = $pensionbas;
	$valpen["04"] = $pensionbas;
	$valpen["05"] = $pensionbas;
	$valpen["06"] = $pensionbas;
	$valpen["07"] = $pensionbas;
	$valpen["08"] = $pensionbas;
	$valpen["09"] = $pensionbas;
	$valpen["10"] = $pensionbas;
	$valpen["11"] = $pensionbas;
	//cargar ajustes pensiones
	$pagosaju = "y".$ano."pagosaju";
	$sql="SELECT mes, valor FROM $pagosaju WHERE id_alumno = '$id_alumno' AND tipo = 'P' ORDER BY mes";
	$dataju = viewsql($sql);
	foreach($dataju as $reg)
	{
            if (intval($reg->mes) <= 9)
                $mes = "0".$reg->mes;
            else
                $mes = $reg->mes;
            $valpen[$mes] = intval($reg->valor);
	}
        return $valpen;
}

Function valores_pensionold($id_alumno,$ano)
{
	$valpen = array();
	$valpen["01"] = 0;
	$valpen["02"] = 0;
	$valpen["03"] = 0;
	$valpen["04"] = 0;
	$valpen["05"] = 0;
	$valpen["06"] = 0;
	$valpen["07"] = 0;
	$valpen["08"] = 0;
	$valpen["09"] = 0;
	$valpen["10"] = 0;
	$valpen["11"] = 0;
	$valpen["saldoFin"] = 0;
	$sql="SELECT matricula,pension02,pension03,pension04,pension05,pension06,pension07,pension08,pension09,pension10,pension11,saldoFin FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		$valpen["01"] = intval($reg->matricula);
		$valpen["02"] = intval($reg->pension02);
		$valpen["03"] = intval($reg->pension03);
		$valpen["04"] = intval($reg->pension04);
		$valpen["05"] = intval($reg->pension05);
		$valpen["06"] = intval($reg->pension06);
		$valpen["07"] = intval($reg->pension07);
		$valpen["08"] = intval($reg->pension08);
		$valpen["09"] = intval($reg->pension09);
		$valpen["10"] = intval($reg->pension10);
		$valpen["11"] = intval($reg->pension11);
		$valpen["saldoFin"] = intval($reg->saldoFin);
	}
	return $valpen;
}

Function pagos_pension($id_alumno,$ano)
{
	$pagpen = array();	
	$ajuste = array();	
	for($i=1; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$int = "interes".$mes;
		$sal = "intesal".$mes;
		$pagpen[$mes] = 0; //pagos pension
		$pagpen[$int] = 0; //pagos intereses
		$pagpen[$sal] = 0; //saldo intereses
		$ajuste[$mes] = 0; //ajustes intereses
	}
	//traer ajustes intereses
	$pagosaju = "y".$ano."pagosaju";
	$sql="SELECT mes, valor FROM $pagosaju WHERE id_alumno = '$id_alumno' AND tipo = 'I' ";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		if (intval($reg->mes) <= 9)
			$mes = "0".$reg->mes;
		else
			$mes = $reg->mes;
		$ajuste[$mes] = $ajuste[$mes] + intval($reg->valor);			
	}
	//traer pagos pensiones y matricula
	$pagosdet = "y".$ano."pagosdet";
	$sql="SELECT mes, valor, interes, interesliq, interesaju FROM $pagosdet WHERE id_alumno = '$id_alumno' AND tipo = 'P' ";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		if (intval($reg->mes) <= 9)
			$mes = "0".$reg->mes;
		else
			$mes = $reg->mes;
		$int = "interes".$mes;
		$sal = "intesal".$mes;
		//ojo, conservar saldo interes negativo para que descuente pagos de solo intereses
		$saldoint = intval($reg->interesliq) - intval($reg->interes) - intval($reg->interesaju);
		$pagpen[$mes] = $pagpen[$mes] + intval($reg->valor);
		$pagpen[$int] = $pagpen[$int] + intval($reg->interes);
		$pagpen[$sal] = $pagpen[$sal] + $saldoint;
	}
	//descontar ajustes intereses pensiones
	for($i=2; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$sal = "intesal".$mes;
		$pagpen[$sal] = $pagpen[$sal] - $ajuste[$mes];
		if ($pagpen[$sal] < 0)
			$pagpen[$sal] = 0;
	}
	return $pagpen;
}

Function valores_transporte($id_alumno,$ano)
{
	$valtra = array();
	$valtra["01"] = 0;
	$valtra["02"] = 0;
	$valtra["03"] = 0;
	$valtra["04"] = 0;
	$valtra["05"] = 0;
	$valtra["06"] = 0;
	$valtra["07"] = 0;
	$valtra["08"] = 0;
	$valtra["09"] = 0;
	$valtra["10"] = 0;
	$valtra["11"] = 0;
	$valtra["saldoFin"] = 0;
	$sql="SELECT transp02,transp03,transp04,transp05,transp06,transp07,transp08,transp09,transp10,transp11,saldoFin FROM transporte WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		$valtra["02"] = intval($reg->transp02);
		$valtra["03"] = intval($reg->transp03);
		$valtra["04"] = intval($reg->transp04);
		$valtra["05"] = intval($reg->transp05);
		$valtra["06"] = intval($reg->transp06);
		$valtra["07"] = intval($reg->transp07);
		$valtra["08"] = intval($reg->transp08);
		$valtra["09"] = intval($reg->transp09);
		$valtra["10"] = intval($reg->transp10);
		$valtra["11"] = intval($reg->transp11);
		$valtra["saldoFin"] = intval($reg->saldoFin);
	}
	return $valtra;
}

Function pagos_transporte($id_alumno,$ano)
{
	$pagtra = array();	
	for($i=1; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$pagtra[$mes] = 0;
	}
	$pagosdet = "y".$ano."pagosdet";
	$sql="SELECT mes, valor FROM $pagosdet WHERE id_alumno = '$id_alumno' AND tipo = 'T' AND ano = '$ano' ";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		if (intval($reg->mes) <= 9)
			$mes = "0".$reg->mes;
		else
			$mes = $reg->mes;
		$pagtra[$mes] = $pagtra[$mes] + intval($reg->valor);
	}
	return $pagtra;
}

Function interes_pension($anopen,$mespen,$valpen,$porcentaje,$fchpago)
{
	$interes = 0;
        if ($valpen > 0 and $porcentaje > 0 and $mespen > 1)
        {
            $dias = 0;
            $meses = 0;
            $anopen = intval($anopen);
            $mespen = intval($mespen);
            $anopag = intval(substr($fchpago,0,4));
            $mespag = intval(substr($fchpago,5,2));
            $diapag = intval(substr($fchpago,8,2));
            if ($diapag > 30)
                    $diapag = 30;
            if ($anopag==$anopen)
            {
                    $meses = $mespag - $mespen - 1;
                    //if ($diapag > 5 and $meses >= 0)
                    if ($meses >= 0)
                            $dias = $diapag;
                    if ($meses==0 and $diapag <= 5)
                            $dias = 0;
                    if ($meses > 0)
                            $dias = $dias + ($meses*30);
            }
            if ($anopag > $anopen)
            {
                    $anos = $anopag - $anopen-1;
                    if ($anos > 0)
                            $meses = $anos*12;
                    $meses = $meses + (12 - $mespen); 
                    $meses = $meses + ($mespag - 1);
                    $dias = $meses * 30;
                    $dias = $dias + $diapag;
            }
            if ($dias > 0 and $porcentaje > 0)
            {
                    $interesmes = $valpen*($porcentaje/100);
                    $interesdia = intval($interesmes/30);
                    $interes = $interesdia * $dias;
            }
        }
	return $interes;
}

Function reliquidar_pensiones($ano,$id_alumno)
{
	$valores = array(); //valores pension	
	$pagos = array(); //pagos pension	
	$saldos = array(); //saldo intereses pendientes	
	$ajuint = array(); // ajustes intereses
	$ajupen = array(); // ajustes pensiones
	$ajureg = array(); // ajustes id 
        $oldsaldofin = 0; //saldo final a favor
        $grado = 0; //grado alumno
        $fecha_hora = date('Y-m-d') . " ". date('H:i:s');
	for($i=1; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$valores[$mes] = 0;
		$pagos[$mes] = 0;
		$saldos[$mes] = 0;
		$ajuint[$mes] = 0;
		$ajupen[$mes] = 0;
		$ajureg[$mes] = 0;
	}
        //cargar valores pensiones
        $porcentaje = 0;
	$sql="SELECT matricula,grado,interes,saldofin FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		$valores["01"] = intval($reg->matricula);
                $oldsaldofin = intval($reg->saldofin);
		$grado = $reg->grado;
		$porcentaje = $reg->interes;
	}
        //cargar valores del grado
        $metodo = "I"; //Intereses por mora
        $pensionbas = 0;
        $pensionfin = 0;
        $pensionret = 0;
        $sql="SELECT pension, pensionfin, pensionret FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
        $datgrad=viewsql($sql);
        foreach($datgrad as $reggrad)
        {$pensionbas = $reggrad->pension; $pensionfin = $reggrad->pensionfin; $pensionret = $reggrad->pensionret;}
        if ($pensionfin > 0 and $pensionret > 0) {
            $metodo = "R";
        }
        $pensionmit = round($pensionbas/2,0);
        //cargar pensiones meses
	$valores["02"] = $pensionbas;
	$valores["03"] = $pensionbas;
	$valores["04"] = $pensionbas;
	$valores["05"] = $pensionbas;
	$valores["06"] = $pensionbas;
	$valores["07"] = $pensionbas;
	$valores["08"] = $pensionbas;
	$valores["09"] = $pensionbas;
	$valores["10"] = $pensionbas;
	$valores["11"] = $pensionbas;
	//cargar ajustes intereses y pensiones del usuario
        $pagosaju = "y".$ano."pagosaju";
	$sql="SELECT id_ajuste, mes, valor, tipo, usucrea FROM $pagosaju WHERE id_alumno = '$id_alumno' AND (tipo = 'I' or tipo = 'P')";
	$dataju = viewsql($sql);
	foreach($dataju as $regaju)
	{
            if (intval($regaju->mes) <= 9)
                $mes = "0".$regaju->mes;
            else
                $mes = $regaju->mes;
            if ($regaju->tipo=="P") //carga ajuste pension
            {
                if ($regaju->usucrea == "") //si ajuste automatico carga id
                {
                    $ajureg[$mes] = $regaju->id_ajuste;
                }
                else //si ajuste manual carga valores
                {
                    $valores[$mes] = intval($regaju->valor); //asigna valor del ajuste
                    $ajupen[$mes] = $ajupen[$mes] + intval($regaju->valor);
                    if ($metodo==='R')
                        $ajuint[$mes] = 1; //indicador ajuste manual
                }
            }
            else //carga ajuste interes
            {
                $ajuint[$mes] = $ajuint[$mes] + intval($regaju->valor);
            }
	}
	//traer todos los valores pagados
	$file = "y".$ano."pagos";
	$filedet = "y".$ano."pagosdet";
	$sql = "SELECT id_alumno, id_item, id_pago, mes, valor, interes, interesliq, interesaju, fchpago FROM $filedet WHERE id_alumno = '$id_alumno' AND tipo = 'P' ORDER BY id_pago";
	$datdet = viewsql($sql);
	foreach($datdet as $regdet)
	{
            //marcar registros no actualizados para luego borrarlos
            $regdet->id_alumno = 0;
        }
	$sql="SELECT id_pago, valor, neto, interes, fchpago, destino FROM $file WHERE id_alumno = '$id_alumno' AND tipo = 'P' ORDER BY fchpago, id_pago";
	$datpag = viewsql($sql);
        //cargar para cada pago realizado los meses que afecta
	foreach($datpag as $regpag)
	{
		$id_pago = $regpag->id_pago;
		$fchpago = $regpag->fchpago;
		$destino = intval($regpag->destino);
		$oldneto = intval($regpag->neto);
		$oldinteres = intval($regpag->interes);
		$pagotot = intval($regpag->valor);
                $anopag = intval(substr($fchpago,0,4));
                $mespag = intval(substr($fchpago,5,2));
                $diapag = intval(substr($fchpago,8,2));
		$neto = 0;
		$interes = 0;
//echo "<br>**************** PAGO VALOR $pagotot ID $id_pago FCHPAGO $fchpago DESTINO $destino % $porcentaje  ****************<br>";
		//cargar pagos mensuales e intereses
		for($i=1; $i<=11 ;$i++)
		{
			$valmes = 0;
			$intmes = 0;
			if ($i <= 9) {
                            $mes = "0".$i;
                        }
			else {
                            $mes = $i;
                        }
			$pension = $valores[$mes];
                        //verificar pension metodo por Rangos si no han pagado menos de la mitad ajusta la mensualidad
                        if ($metodo==='R' and $i > 1 and $pagos[$mes]<=$pensionmit and $ajuint[$mes] == 0 and $pension >= $pensionbas and $anopag >= $ano) {
                            $coment = "";
                            if ($mespag == $i and $diapag >= 16) {
                                $pension = $pensionfin;  
                                $coment = "PAGO DESPUES DEL 15";
                            }
                            if ($mespag > $i or $anopag > $ano) {
                                $pension = $pensionret;  
                                $coment = "PAGO DESPUES DEL 30";
                            }
                            if ($coment!="") //si no existen ajustes manuales de pension
                            {
                                $valores[$mes] = $pension;
                                $ajupen[$mes] = $pension;
                                $id_ajuste = $ajureg[$mes];
                                if ($id_ajuste <= 0) {
                                    $sql= "INSERT INTO $pagosaju (id_alumno,tipo,mes,ano,valor,detalles,fchcrea) ".
                                    "VALUES ('$id_alumno','P','".intval($mes)."','$ano','$pension','$coment','$fecha_hora')";
                                    execsql($sql);
                                    $sql="SELECT id_ajuste FROM $pagosaju WHERE id_alumno = '$id_alumno' AND mes = '".intval($mes)."' AND tipo = 'P' LIMIT 1";
                                    $dataju = viewsql($sql);
                                    foreach($dataju as $regaju)
                                    {$id_ajuste = $regaju->id_ajuste;}
                                    $ajureg[$mes] = $id_ajuste;
                                    
                                } else {
                                    $sql="UPDATE $pagosaju SET valor='$pension', detalles='$coment', fchactual='$fecha_hora' WHERE id_ajuste = '$id_ajuste' LIMIT 1";
                                    execsql($sql);
                                }
                            }
                        }
                        //valor base de pension pendiente de pago del mes
			$pension = $pension - $pagos[$mes];
                        if ($pension < 0){
                            $pension = 0;
                        }
			if ($pension > $pagotot) {
                            $pension = $pagotot;
                        }
                        $intliq = 0;
                        $intsum = 0;
                        $intaju = 0;

                        //liquidar intereses desde febrero
                        if ($metodo==='I')
                        {
                            $intliq = interes_pension($ano,$mes,$pension,$porcentaje,$fchpago);
                            $intsum = $intliq + $saldos[$mes];
                            //restar ajuste interes
                            if ($ajuint[$mes] > 0 and $intsum > 0)
                            {
                                    $nwaju = 0;
                                    if ($ajuint[$mes] <= $intsum)
                                    {
                                        $intsum = $intsum - $ajuint[$mes];
                                        $intaju = $ajuint[$mes];
                                    }
                                    else
                                    {
                                        $nwaju = $ajuint[$mes] - $intsum;
                                        $intaju = $intsum;
                                        $intsum = 0;
                                    }
                                    $ajuint[$mes] = $nwaju;
                                    if ($saldos[$mes] > 0) {
                                        $saldos[$mes] = $saldos[$mes] - $intaju;}
                            }
                        }
//echo "MES $mes Saldo Pension: $pension Pagos $pagos[$mes] Interes Liquida: $intliq Interes SaldoAnt: $saldos[$mes] Interes suma: $intsum Interes Ajuste: $intaju <br>";
			//descontar pension y/o intereses del acumulado total del pago
                        if ($destino != 1) {
                            if ($destino==0 and $pension > 0) {
                                $valmes = $pension;
                                $pagotot = $pagotot - $valmes;
                                $pagos[$mes] = $pagos[$mes] + $valmes;
                            }
                            if ($pension==0 or $intsum > 0) {
                                $intmes = $intsum;
                                $nwsal = 0;
                                if ($intmes >= $pagotot)
                                {
                                    $nwsal = $intmes - $pagotot;
                                    $intmes = $pagotot;
                                    $pagotot = 0;
                                }
                                else
                                {
                                    $pagotot = $pagotot - $intmes;
                                }
                                $saldos[$mes] = $nwsal;
                            }
                        }
			else //descontar solo pension
			{
                            $valmes = $pension;
                            $pagotot = $pagotot - $valmes;
                            $pagos[$mes] = $pagos[$mes] + $valmes;
                            if ($intliq > 0) {
                                $saldos[$mes] = $saldos[$mes] + $intsum; //acumula interes liquidado y ajustado asi no lo descuente
                            }
			}
                        //totalizar
//echo "Total Pagos $pagos[$mes] Nuevo Saldo interes: $saldos[$mes]<br>Acumulado$ $pagotot<br>";                        
			$neto = $neto + $valmes;
			$interes = $interes + $intmes;
                        //actualizar tabla detalle de pagos mensuales
			$id_item = 0;
			foreach($datdet as $regdet)
			{
				if ($regdet->id_pago==$id_pago and intval($regdet->mes)==$i)
				{
					$id_item = $regdet->id_item;
                                        if ($valmes > 0 or $intmes > 0) //no actualizar ID para que borre registros en cero
                                        {
                                            $regdet->id_alumno = $id_alumno;
                                        }
					if (intval($regdet->valor)!=$valmes or intval($regdet->interes)!=$intmes or intval($regdet->interesliq)!=$intliq or intval($regdet->interesaju)!=$intaju or $regdet->fchpago!=$fchpago)
					{
                                            $sql="UPDATE $filedet SET mes='$i',valor='$valmes',interes='$intmes',interesliq='$intliq',interesaju='$intaju',fchpago='$fchpago' WHERE id_item = '$id_item' LIMIT 1";
                                            execsql($sql);
//echo "$sql<br>";                                            
                                        }
				}
			} //fin detalle pagos
			//grabar pagos mensuales si carga pension o carga interes
			if ($id_item==0 and ($valmes > 0 or $intmes > 0))
			{
				$sql= "INSERT INTO $filedet (id_pago,id_alumno,mes,ano,tipo,valor,interes,interesliq,interesaju,fchpago) VALUES ('$id_pago','$id_alumno','$i','$ano','P','$valmes','$intmes','$intliq','$intaju','$fchpago')";
				execsql($sql);
//echo "$sql<br>";                                            

                        }
                        if ($pagotot <= 0) {
                            break;
                        }
		} //FIN for meses

                //carga saldo final de a�o a favor
		if (($pagotot > 0 and $pagos["11"] > 0) or ($oldsaldofin > 0 and $pagotot!=$oldsaldofin))
		{
			$sql="UPDATE matriculas SET saldoFin='$pagotot' WHERE id_alumno = '$id_alumno' AND ano='$ano' LIMIT 1";
			execsql($sql);
			$neto = $neto + $pagotot;
		}
		//actualizar pago principal
		if ($neto != $oldneto or $interes != $oldinteres)
		{
			$sql="UPDATE $file SET neto='$neto',interes='$interes' WHERE id_pago = '$id_pago' LIMIT 1";
			execsql($sql);
		}
	} //Fin foreach pagos
        //borrar ajustes de pension no actualizados
	for($i=1; $i<=11 ;$i++)
	{
            if ($i <= 9) {
                $mes = "0".$i;
            }
            else {
                $mes = $i;
            }
            if ($ajureg[$mes] > 0 and $ajupen[$mes] == 0)
            {
                $id_ajuste = $ajureg[$mes];
                $sql = "DELETE FROM $pagosaju WHERE id_ajuste = '$id_ajuste' LIMIT 1";
                execsql($sql);
            }
        }
        //borrar registros sobrantes de meses que no se modificaron
        foreach($datdet as $regdet)
	{
            $id_item = $regdet->id_item;
            if ($regdet->id_alumno==0)
            {
                $sql="DELETE FROM $filedet WHERE id_item = '$id_item' LIMIT 1";
                execsql($sql);
            }
        }

}

Function reliquidar_transportes($ano,$id_alumno)
{
	$valores = array(); //valores pension	
	$pagos = array(); //pagos pension	
	$saldos = array(); //saldo intereses pendientes	
	$ajustes = array(); // ajustes intereses
        $oldsaldofin = 0; //saldo final a favor
	for($i=1; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$valores[$mes] = 0;
		$pagos[$mes] = 0;
		$saldos[$mes] = 0;
		$ajustes[$mes] = 0;
	}
	$sql="SELECT saldofin,transp02,transp03,transp04,transp05,transp06,transp07,transp08,transp09,transp10,transp11 FROM transporte WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		$valores["02"] = intval($reg->transp02);
		$valores["03"] = intval($reg->transp03);
		$valores["04"] = intval($reg->transp04);
		$valores["05"] = intval($reg->transp05);
		$valores["06"] = intval($reg->transp06);
		$valores["07"] = intval($reg->transp07);
		$valores["08"] = intval($reg->transp08);
		$valores["09"] = intval($reg->transp09);
		$valores["10"] = intval($reg->transp10);
		$valores["11"] = intval($reg->transp11);
                $oldsaldofin = intval($reg->saldofin);
	}
	//traer todos los valores pagados
	$file = "y".$ano."pagos";
	$filedet = "y".$ano."pagosdet";
	$sql = "SELECT id_item, id_pago, mes, valor, interes, interesliq, interesaju, fchpago FROM $filedet WHERE id_alumno = '$id_alumno' AND tipo = 'T' ORDER BY id_pago";
	$datdet = viewsql($sql);
	$sql="SELECT id_pago, valor, neto, interes, fchpago, porcentaje, destino FROM $file WHERE id_alumno = '$id_alumno' AND tipo = 'T' ORDER BY fchpago, id_pago";
	$datpag = viewsql($sql);
	foreach($datpag as $regpag)
	{
		$id_pago = $regpag->id_pago;
		$fchpago = $regpag->fchpago;
		$destino = intval($regpag->destino);
		$porcentaje = intval($regpag->porcentaje);
		$oldneto = intval($regpag->neto);
		$oldinteres = intval($regpag->interes);
		$pagotot = intval($regpag->valor);
		$neto = 0;
		$interes = 0;
		//reliquidar pagos mensuales e intereses
		for($i=1; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			//descontar saldo pension			
			$valmes = $valores[$mes] - $pagos[$mes];
			if ($valmes < 0)
                        {
                            $valmes = 0;
                        }
			//descontar neto del acumulado del pago
			if ($valmes >= $pagotot)
			{
                		$valmes = $pagotot;
        			$pagotot = 0;
			}
			else
                        {
                        	$pagotot = $pagotot - $valmes;
                        }
			$intmes = 0;
			$intliq = 0;
			$intaju = 0;
			//actualizar tabla detalle pagos mensuales
			$id_item = 0;
			foreach($datdet as $regdet)
			{
				if ($regdet->id_pago==$id_pago and intval($regdet->mes)==$i)
				{
					$id_item = $regdet->id_item;
					if ($valmes <= 0 and $intmes <= 0) //no paga nada en ese mes
					{
						$sql="DELETE FROM $filedet WHERE id_item = '$id_item' LIMIT 1";
						execsql($sql);
					}
					else
					{
						if (intval($regdet->valor)!=$valmes or intval($regdet->interes)!=$intmes or intval($regdet->interesliq)!=$intliq or intval($regdet->interesaju)!=$intaju or $regdet->fchpago!=$fchpago)
						{
							$sql="UPDATE $filedet SET valor='$valmes',interes='$intmes',interesliq='$intliq',interesaju='$intaju',fchpago='$fchpago' WHERE id_item = '$id_item' LIMIT 1";
							execsql($sql);
						}
					}
				}
			} //fin detalle pagos
			//grabar nuevo registro tabla detalle pagos mensuales
			if ($id_item==0 and ($valmes > 0 or $intmes > 0))
			{
				$sql= "INSERT INTO $filedet (id_pago,id_alumno,mes,ano,tipo,valor,interes,interesliq,interesaju,fchpago) VALUES ('$id_pago','$id_alumno','$i','$ano','T','$valmes','$intmes','$intliq','$intaju','$fchpago')";
				execsql($sql);
			}
			//totalizar
			$pagos[$mes] = $pagos[$mes]	+ $valmes;
			$neto = $neto + $valmes;
			$interes = $interes + $intmes;
		} //Fin for meses
		//carga saldo final de a�o a favor
		if (($pagotot > 0 and $pagos["11"] > 0) or ($oldsaldofin > 0 and $pagotot!=$oldsaldofin))
		{
			$sql="UPDATE transporte SET saldoFin='$pagotot' WHERE id_alumno = '$id_alumno' AND ano='$ano' LIMIT 1";
			execsql($sql);
			$neto = $neto + $pagotot;
		}
		//actualizar pago principal
		if ($neto != $oldneto or $interes != $oldinteres)
		{
			$sql="UPDATE $file SET neto='$neto',interes='$interes' WHERE id_pago = '$id_pago' LIMIT 1";
			execsql($sql);
		}
	} //Fin foreach pagos
}

Function saldo_pensiones($ano,$id_alumno)
{
    $valorpen = 0;
    $pagospen = 0;
    $saldopen = 0;
    $sql="SELECT matricula,pension02,pension03,pension04,pension05,pension06,pension07,pension08,pension09,pension10,pension11 FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
    $dat = viewsql($sql);
    foreach($dat as $reg)
    {
        $valorpen = $valorpen + intval($reg->matricula);
        $valorpen = $valorpen + intval($reg->pension02);
	$valorpen = $valorpen + intval($reg->pension03);
	$valorpen = $valorpen + intval($reg->pension04);
	$valorpen = $valorpen + intval($reg->pension05);
	$valorpen = $valorpen + intval($reg->pension06);
	$valorpen = $valorpen + intval($reg->pension07);
	$valorpen = $valorpen + intval($reg->pension08);
	$valorpen = $valorpen + intval($reg->pension09);
	$valorpen = $valorpen + intval($reg->pension10);
	$valorpen = $valorpen + intval($reg->pension11);
    }
    $file = "y".$ano."pagos";
    $sql="SELECT valor, neto, interes FROM $file WHERE id_alumno = '$id_alumno' AND tipo = 'P'";
    $datpag = viewsql($sql);
    foreach($datpag as $regpag)
    {
        $pagospen = $pagospen + intval($regpag->neto);
    }
    $saldopen = $valorpen - $pagospen;
    return $saldopen;
}

Function saldo_transporte($ano,$id_alumno)
{
    $valortra = 0;
    $pagostra = 0;
    $saldotra = 0;
    $sql="SELECT transp02,transp03,transp04,transp05,transp06,transp07,transp08,transp09,transp10,transp11 FROM transporte WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
    $dat = viewsql($sql);
    foreach($dat as $reg)
    {
	$valortra = $valortra + intval($reg->transp02);
	$valortra = $valortra + intval($reg->transp03);
	$valortra = $valortra + intval($reg->transp04);
	$valortra = $valortra + intval($reg->transp05);
	$valortra = $valortra + intval($reg->transp06);
	$valortra = $valortra + intval($reg->transp07);
	$valortra = $valortra + intval($reg->transp08);
	$valortra = $valortra + intval($reg->transp09);
	$valortra = $valortra + intval($reg->transp10);
	$valortra = $valortra + intval($reg->transp11);
    }
    $file = "y".$ano."pagos";
    $sql="SELECT valor, neto, interes FROM $file WHERE id_alumno = '$id_alumno' AND tipo = 'T'";
    $datpag = viewsql($sql);
    foreach($datpag as $regpag)
    {
        $pagostra = $pagostra + intval($regpag->neto);
    }
    $saldotra = $valortra - $pagostra;
    return $saldotra;
}

?>