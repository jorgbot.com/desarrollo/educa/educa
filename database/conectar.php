<?php
class Conectar{
    //Creamos un m�todo est�tico que no necesita ser instanciado
    public static function conexion(){
    $instid = $_SESSION["instid"];
    $bd="educa".$instid;
    $hostbd="localhost";
    $userbd="root";
    $password="root";
     
    //new mysqli creamos o instanciamos el objeto mysqli
    //new mysqli('servidor','usuario','contrase�a','nombre de la BD');
        $conexion=new mysqli($hostbd, $userbd, $password, $bd);
             
       //llamamos a la conexi�n y hacemos una consulta para utilizar UTF-8
        $conexion->query("SET NAMES 'utf8'");
 
       //devolvemos la conexi�n para que pueda ser utilizada en otros m�todos
        return $conexion;
    }
}
 
class Periodos{
 private $db;        //atributo que almacena la conexi�n a la BD
 private $periodos; //propiedad que almacenara los resultados de la BD
  
    public function __construct(){
       $this->periodos=array(); //Le decimos que sea un array
       $this->db=Conectar::conexion(); //Almacenamos en db la llamada la clase est�tica Conectar
    }
     
    public function get_periodos(){
        //hacemos una consulta
        $consulta=$this->db->query("select * from periodos where ano = 2019;");
         
    //recorremos el resultado con while y el m�todo fetch_assoc
/*la diferencia entre fetch_assoc y fetch_array es que fetch_assoc te permite hacer SOLO arrays asociativos */
        while($filas=$consulta->fetch_assoc()){
          //metemos cada fila de la tabla (que son arrays) dentro del array periodos
            $this->periodos[]=$filas;
        }
 
    return $this->periodos; //devolvemos el array que trataremos en la vista
    }
}
?>