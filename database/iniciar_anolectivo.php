<?php
/************************************************************************************************
* @file        : iniciar_anolectivo.php                                                         *
* @brief       : Proceso que inicializa tablas de nuevo a�o lectivo                             *
* @version     : 1.0                                                                            *
* @author      : Nora Rodriguez 01-jun-2012                                                     *
* @Modificado  : Nora Rodriguez 06-nov-2018                                                     *
************************************************************************************************/
ini_set('max_execution_time', 0); //600 seconds = 10 minutes	

Function iniciar_anolectivo($ano)
{
	//crear tablas anuales
	crear_actividades($ano);
	crear_cursos($ano);
        crear_evidencias($ano);
	crear_fallas($ano);
	crear_logros($ano);
	crear_metodologias($ano);
	crear_notasasignaturas($ano);
	crear_notasareas($ano);
	crear_notasperiodos($ano);
	crear_observaciones($ano);
	crear_pagos($ano);
	crear_pagosaju($ano);
	crear_pagosdet($ano);
	crear_pagosent($ano);
	crear_pagostra($ano);
	crear_planillas($ano);
	crear_profasig($ano);
	//cargar parametros anuales
	cargar_periodos($ano);
	cargar_puntajes($ano);
	cargar_valores($ano);
	cargar_rutas($ano);
	cargar_cursos($ano);
	cargar_asignaturas($ano);
}

Function iniciar_matriculas($ano)
{
	cargar_matriculas($ano);
}
Function iniciar_contratos($ano)
{
	cargar_contratos($ano);
}

//tabla anual de actividades por evidencias de logro
Function crear_actividades($ano)
{
	$db = "idic";
	$file = "y".$ano."activ";
	$sql="CREATE TABLE $db.$file ( `id_actividad` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_asignatura` int( 8 ) NOT NULL COMMENT 'id asignatura asociada',
	`id_logro` int( 6 ) NOT NULL COMMENT 'id logro asociado',
	`id_evidencia` int( 6 ) NOT NULL COMMENT 'id evidencia asociada',
	`periodo` int( 1 ) NOT NULL COMMENT 'periodo en que aplica',
	`actividad` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripcion larga actividad',
	`porcentaje` int( 3 ) NOT NULL COMMENT 'porcentaje nota del logro',
	`codigo` varchar( 10 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'codigo o nombre corto',
	`numero` varchar( 2 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'numero en planilla',
	`fchcrea` datetime NOT NULL COMMENT 'fecha creacion',
	`usucrea` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario creacion',
	`fchactual` datetime NOT NULL COMMENT 'fecha actualizacion',
	`usuactual` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario actualizacion',
	PRIMARY KEY ( `id_actividad` ) ,
	KEY `id_asignatura` ( `id_asignatura` , `id_logro` , `periodo` ) 
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'actividades por logro a calificar'";
	execsql($sql);
}

//tabla anual de alumnos por cursos
Function crear_cursos($ano)
{
	$db = "idic";
	$file = "y".$ano."cursos";
	$sql="CREATE TABLE $db.$file ( `id_curso` int( 5 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`nivel` char( 1 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'K P S',
	`grado` char( 2 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'grado numero',
	`curso` varchar( 5 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'curso numero',
	`id_alumno` int( 6 ) NOT NULL COMMENT 'id alumno asociado',
	`numero` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'alumno en planilla',
	`apellidos` varchar( 30 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'primer y segundo apellido',
	`nombres` varchar( 30 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'primer y segundo nombre',
	`tipoedu` char( 1 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'educacion formal o especial',
	`inactivar` varchar( 10 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'periodos a inctivar en notas',
	`id_areadeb` int( 4 ) NOT NULL DEFAULT '0' COMMENT 'id area que debe grado anterior',
	`fchretiro` date NOT NULL DEFAULT '0000-00-00' COMMENT 'fecha retiro',
	`promedio1` decimal( 6, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'promedio periodo 1',
	`puesto1` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'puesto periodo 1',
	`promedio2` decimal( 6, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'promedio periodo 2',
	`puesto2` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'puesto periodo 2',
	`promedio3` decimal( 6, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'promedio periodo 3',
	`puesto3` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'puesto periodo 3',
	`promedio4` decimal( 6, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'promedio periodo 4',
	`puesto4` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'puesto periodo 4',
	`promedio` decimal( 6, 2 ) NOT NULL DEFAULT '0.00' COMMENT 'promedio final',
	`puesto` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'puesto final',
	`areasper` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'cantidad areas que pierde',
	`id_areaper` int( 4 ) NOT NULL DEFAULT '0' COMMENT 'id area perdida siguiente a�o',
	`pierde` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'pierde o no a�o',
	PRIMARY KEY ( `id_curso` ) ,
	KEY `grado` ( `grado` , `curso` , `id_alumno` ) 
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'alumnos por curso grado y anualidad'";
	execsql($sql);
}

//tabla anual de evidencias por logro
Function crear_evidencias($ano)
{
	$db = "idic";
	$file = "y".$ano."eviden";
	$sql="CREATE TABLE $db.$file ( `id_evidencia` int( 6 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_asignatura` int( 8 ) NOT NULL COMMENT 'id asignatura asociada',
	`id_logro` int( 6 ) NOT NULL COMMENT 'id logro asociado',
	`periodo` int( 1 ) NOT NULL COMMENT 'periodo en que aplica',
	`evidencia` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripcion larga evidencia',
	`porcentaje` int( 3 ) NOT NULL COMMENT 'porcentaje nota del logro',
	`codigo` varchar( 10 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'codigo o nombre corto',
	`numero` varchar( 2 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'numero en planilla',
	`fchcrea` datetime NOT NULL COMMENT 'fecha creacion',
	`usucrea` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario creacion',
	`fchactual` datetime NOT NULL COMMENT 'fecha actualizacion',
	`usuactual` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario actualizacion',
	PRIMARY KEY ( `id_evidencia` ) ,
	KEY `id_asignatura` ( `id_asignatura` , `id_logro` , `periodo` ) 
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'evidencias por logro a calificar'";
	execsql($sql);
}

//tabla anual de fallas
Function crear_fallas($ano)
{
	$db = "idic";
	$file = "y".$ano."fallas";
	$sql = "CREATE TABLE $db.$file ( `id_falla` int( 12 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_alumno` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id alumno asociado',
	`id_asignatura` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'id asignatura asociada',
	`id_categoria` int( 3 ) NOT NULL DEFAULT '0' COMMENT 'id categoria asociada',
	`tipo` char( 1 ) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'I' COMMENT 'I R P C A',
	`concepto` varchar( 250 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'concepto novedad',
	`fecha` date NOT NULL COMMENT 'fecha de falla',
	`fchfin` date NOT NULL COMMENT 'fecha final',
	`fchjust` date NOT NULL COMMENT 'fecha de justificacion',
       	`hcita` char( 5 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'hora de la cita',
	`periodo` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'periodo academico',
	`horas` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'cantidad de horas por asignatura',
	`intervalos` varchar( 50 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'intervalos del dia por asignatura',
	`detalles` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'comentario general',
	`fchcrea` datetime NOT NULL COMMENT 'fecha creacion',
	`usucrea` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario creacion',
	`fchactual` datetime NOT NULL COMMENT 'fecha actualizacion',
	`usuactual` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario actualizacion',
	PRIMARY KEY ( `id_falla` ) ,
	KEY `id_alumno` ( `id_alumno` , `fecha` , `id_asignatura` ) 
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'fallas del alumno por fecha y asignatura'";
	execsql($sql);
}

//tabla anual de logros
Function crear_logros($ano)
{
	$db = "idic";
	$file = "y".$ano."logros";
	$sql = "CREATE TABLE $db.$file ( `id_logro` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_asignatura` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'id asignatura asociada',
	`logro` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripcion logro basico',
	`logrosi` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripcion logro exitoso',
	`logrono` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripcion logro no exitoso' ,
	`logrocod` int( 3 ) NOT NULL DEFAULT '0' COMMENT 'codigo planilla',
	`periodos` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'periodos en que aplica',
	`tipoedu` char( 1 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'educacion formal o especial',
	`evidemin` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'cantidad evidencias minimas',
	`evidepor` varchar( 50 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'porcentajes evidencias',
        `activmin` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'cantidad actividades minimas',
	`activpor` varchar( 50 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'porcentajes actividades',
	`grado` varchar( 2 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'grado en que aplica',
	`cursos` varchar( 50 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'cursos inactivacion',
	`fchcrea` datetime NOT NULL COMMENT 'fecha creacion',
	`usucrea` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario creacion',
	`fchactual` datetime NOT NULL COMMENT 'fecha actualizacion',
	`usuactual` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario actualizacion',
	PRIMARY KEY ( `id_logro` ) ,
	KEY `id_asignatura` ( `id_asignatura` )
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'logros por asignatura y periodos'";
	execsql($sql);
}

//tabla anual de metodologias logros
Function crear_metodologias($ano)
{
	$db = "idic";
	$file = "y".$ano."metod";
	$sql = "CREATE TABLE $db.$file ( `id_metodologia` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_asignatura` int( 8 ) NOT NULL COMMENT 'id asignatura asociada',
	`id_logro` int( 6 ) NOT NULL COMMENT 'id logro asociado',
	`fecha` date NOT NULL COMMENT 'fecha de aplicacion',
	`metodologia` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'descripcion metodologia',
	`numclases` int( 3 ) NOT NULL COMMENT 'numero de clases',
	`recursos` varchar( 250 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'recursos utilizados',
	`fchcrea` datetime NOT NULL COMMENT 'fecha creacion',
	`usucrea` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario creacion',
	`fchactual` datetime NOT NULL COMMENT 'fecha actualizacion',
	`usuactual` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario actualizacion',
	PRIMARY KEY ( `id_metodologia` ) ,
	KEY `id_asignatura` ( `id_asignatura` , `id_logro` )
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'metodologias por logro o parselacion'";
	execsql($sql);
}

//crear tabla anual de notas de asignaturas
Function crear_notasasignaturas($ano)
{
	$db = "idic";
	$file = "y".$ano."notas";
	$sql="CREATE TABLE $db.$file (
	`id_nota` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_alumno` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id alumno asociado',
	`id_asignatura` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'id asignatura asociada',
	`pertipo` char( 1 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'tipo periodo o examen',
	`periodo` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'numero periodo o examen',
	`nota` decimal( 5, 1 ) NOT NULL DEFAULT '0.0' COMMENT 'nota asignatura',
	`notaEx` decimal( 5, 1 ) NOT NULL DEFAULT '0.0' COMMENT 'nota asignatura ajustada examenes semestrales',
	`notaExF` decimal( 5, 1 ) NOT NULL DEFAULT '0.0' COMMENT 'nota asignatura ajustada examenes finales',
	`pendiente` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'nota pendiente',
	`inactivar` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'nota inactiva',
	PRIMARY KEY ( `id_nota` ) ,
	KEY `id_alumno` ( `id_alumno` , `id_asignatura` , `pertipo` ) 
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'notas totales por asignatura'";
	execsql($sql);
}

//crear tabla anual notas de areas
Function crear_notasareas($ano)
{
	$db = "idic";
	$file = "y".$ano."notasfa";
	$sql = "CREATE TABLE $db.$file (
	`id_nota` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_alumno` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id alumno asociado',
	`id_area` int( 4 ) NOT NULL DEFAULT '0' COMMENT 'id area asociada',
	`periodo` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'periodo en que aplica',
	`nota` decimal( 5, 1 ) NOT NULL DEFAULT '0.0' COMMENT 'nota area',
	`notaEx` decimal( 5, 1 ) NOT NULL DEFAULT '0.0' COMMENT 'nota area ajustada examenes semestrales',
	`notaExF` decimal( 5, 1 ) NOT NULL DEFAULT '0.0' COMMENT 'nota area ajustada examenes finales',
	PRIMARY KEY ( `id_nota` ) ,
	KEY `id_alumno` ( `id_alumno` , `id_area` ) 
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'notas finales por area'";
	execsql($sql);
}

//crear tabla anual notas de cada periodo, con los periodos del a�o anterior
Function crear_notasperiodos($ano)
{
	$db = "idic";
	$antano = intval($ano) - 1;
	$sql="SELECT periodo FROM periodos WHERE ano = '$antano' and pertipo='P' order by notanum";
	$datperi=viewsql($sql);
	foreach($datperi as $regperi)
	{
		$periodo = $regperi->periodo;
		$file = "y".$ano."notasp".$periodo;
		$sql = "CREATE TABLE $db.$file (
		`id_nota` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
		`id_alumno` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id alumno asociado',
		`id_asignatura` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'id asignatura asociada',
		`id_logro` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id logro asociado',
                `id_evidencia` int( 6 ) NOT NULL COMMENT 'id evidencia asociada',
                `id_actividad` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'id actividad asociada',
		`nota` decimal( 5, 1 ) NOT NULL DEFAULT '0' COMMENT 'nota actividad o logro',
		`notaEx` decimal( 5, 1 ) NOT NULL DEFAULT '0' COMMENT 'nota ajustada examenes',
		`numEx` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'numero examen ajuste de nota',
		`pendiente` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'nota pendiente',
		`inactivar` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'nota inactiva',
		PRIMARY KEY ( `id_nota` ) ,
		KEY `id_alumno` ( `id_alumno` , `id_asignatura` , `id_logro` , `id_actividad` ) 
		) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'notas por actividad y logro periodo $periodo'";
		execsql($sql);
	}
}

//crear tabla anual observaciones alumnos en periodos
Function crear_observaciones($ano)
{
	$db = "idic";
	$file = "y".$ano."observa";
	$sql = "CREATE TABLE $db.$file (
	`id_obseasig` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_alumno` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id alumno asociado',
	`id_asignatura` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'id asignatura asociada',
	`pertipo` char( 1 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'tipo periodo o examen',
	`periodo` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'periodo numero',
	`id_observacion` int( 4 ) NOT NULL DEFAULT '0' COMMENT 'id observacion asociada',
	`observacion` text COLLATE utf8_spanish_ci NOT NULL COMMENT 'observacion',
	PRIMARY KEY ( `id_obseasig` ) ,
	KEY `id_alumno` ( `id_alumno` , `id_asignatura` , `id_observacion` ) 
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = DYNAMIC COMMENT = 'observaciones por alumno aignatura y periodo'";
	execsql($sql);
}

//crear tabla anual de pagos matriculas, pensiones, transporte
Function crear_pagos($ano)
{
	$db = "idic";
	$file = "y".$ano."pagos";
	$sql = "CREATE TABLE $db.$file ( `id_pago` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_alumno` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id alumno asociado',
	`id_banco` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'id banco asociado',
	`id_entrada` int( 4 ) NOT NULL DEFAULT '0' COMMENT 'id entrada asociada',
	`fchentrada` date NOT NULL COMMENT 'fecha de entrada',
	`tipo` char( 1 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Pension o Transporte',
	`destino` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'destinar a pension o interes o ambas',
	`ano` int( 4 ) NOT NULL DEFAULT '0' COMMENT 'a�o lectivo a pagar',
	`grado` char( 2 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'grado numero',
	`curso` char( 5 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'curso numero',
	`fchpago` date NOT NULL COMMENT 'fecha del pago',
	`valor` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor total pagado',
	`valor_con` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor consignado',
	`valor_efe` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor efectivo',
	`neto` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor neto pagado',
	`interes` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor intereses pagados',
	`porcentaje` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'porcentaje de interes por mora',
	`fchcrea` datetime NOT NULL COMMENT 'fecha creacion',
	`usucrea` char( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario creacion',
	`fchactual` datetime NOT NULL COMMENT 'fecha actualizacion',
	`usuactual` char( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario actualizacion',
	PRIMARY KEY ( `id_pago` ) ,
	KEY `id_alumno` ( `id_alumno` , `fchpago` )
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'Pagos matriculas pensiones transporte'";
	execsql($sql);
}

//crear tabla anual de ajustes intereses pensiones
Function crear_pagosaju($ano)
{
	$db = "idic";
	$file = "y".$ano."pagosaju";
	$sql = "CREATE TABLE $db.$file ( `id_ajuste` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_alumno` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id alumno asociado',
	`tipo` char( 1 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Interes Pension Transporte',
	`ano` int( 4 ) NOT NULL DEFAULT '0' COMMENT 'ano lectivo ajuste',
	`mes` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'mes lectivo ajuste',
	`valor` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor ajuste',
	`detalles` varchar( 300 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'detalles del ajuste',
	`fchcrea` datetime NOT NULL COMMENT 'fecha creacion',
	`usucrea` char( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario creacion',
	`fchactual` datetime NOT NULL COMMENT 'fecha actualizacion',
	`usuactual` char( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'usuario actualizacion',
	PRIMARY KEY ( `id_ajuste` ) ,
	KEY `id_alumno` ( `id_alumno` )
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'Ajustes pagos de intereses pensiones'";
	execsql($sql);
}

//crear tabla anual detalle de pagos mensualidades
Function crear_pagosdet($ano)
{
	$db = "idic";
	$file = "y".$ano."pagosdet";
	$sql = "CREATE TABLE $db.$file ( `id_item` int( 12 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_pago` int( 3 ) NOT NULL DEFAULT '0' COMMENT 'id pago asociado',
	`id_alumno` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id alumno asociado',
	`tipo` char( 1 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Matricula Pension Transporte',
	`ano` int( 4 ) NOT NULL DEFAULT '0' COMMENT 'a�o lectivo a pagar',
	`mes` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'mes lectivo a pagar',
	`valor` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pagado',
	`interes` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor intereses pagados',
	`interesliq` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor intereses liquidados fecha de pago',
	`interesaju` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor ajuste sobre intereses',
	`fchpago` date NOT NULL COMMENT 'fecha del pago',
	PRIMARY KEY ( `id_item` ) ,
	KEY `id_pago` ( `id_pago` ) ,
	KEY `id_alumno` ( `id_alumno` , `mes` )
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'Pagos detalle mensual'";
	execsql($sql);
}

//crear tabla anual entradas por fecha para pagos matricula y pensiones
Function crear_pagosent($ano)
{
	$db = "idic";
	$file = "y".$ano."pagosent";
	$sql = "CREATE TABLE $db.$file (
	`id_entrada` int( 4 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`fchentrada` date NOT NULL COMMENT 'fecha de entrada',
	`estado` char( 7 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'estado entrada',
	PRIMARY KEY ( `id_entrada` ) ,
	KEY `fchentrada` ( `fchentrada` )
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'consecutivo entrada de pagos pensiones por fecha'";
	execsql($sql);
}

//crear tabla anual entradas por fecha para pagos transporte
Function crear_pagostra($ano)
{
	$db = "idic";
	$file = "y".$ano."pagostra";
	$sql = "CREATE TABLE $db.$file (
	`id_entrada` int( 4 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`fchentrada` date NOT NULL COMMENT 'fecha de entrada',
	`estado` char( 7 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'estado entrada',
	PRIMARY KEY ( `id_entrada` ) ,
	KEY `fchentrada` ( `fchentrada` )
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'consecutivo entrada de pagos transporte por fecha'";
	execsql($sql);
}

//crear tabla anual planillas asignaturas
Function crear_planillas($ano)
{
	$db = "idic";
	$file = "y".$ano."planillas";
	$sql = "CREATE TABLE $db.$file ( `id_planilla` int( 8 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`grado` varchar( 2 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'grado numero',
	`curso` varchar( 5 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'curso numero',
	`id_asignatura` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'id asignatura asociada',
	`pertipo` char( 1 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'tipo periodo o examen',
	`periodo` int( 1 ) NOT NULL DEFAULT '0' COMMENT 'periodo numero',
	`id_profesor` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'id profesor asociado',
	`estado` varchar( 15 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'estado planilla',
	`fchentrega` datetime NOT NULL COMMENT 'fecha entrega',
	`fchcierre` datetime NOT NULL COMMENT 'fecha cierre',
	PRIMARY KEY ( `id_planilla` ) ,
	KEY `id_asignatura` ( `id_asignatura` , `grado` , `curso` )
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci ROW_FORMAT = DYNAMIC COMMENT = 'estado de planillas por asignatura, curso y periodo'";
	execsql($sql);
}

//crear tabla anual profesores asignaturas
Function crear_profasig($ano)
{
	$db = "idic";
	$file = "y".$ano."profasig";
	$sql = "CREATE TABLE $db.$file (
	`id_profasig` int( 6 ) NOT NULL AUTO_INCREMENT COMMENT 'id unico automatico',
	`id_profesor` int( 6 ) NOT NULL COMMENT 'id profesor asociado',
	`profesor` varchar( 60 ) COLLATE utf8_spanish_ci NOT NULL ,
	`id_asignatura` int( 8 ) NOT NULL COMMENT 'id asignatura asociada',
	`grado` varchar( 2 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'grado asignado',
	`curso` varchar( 5 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'curso asignado',
	PRIMARY KEY ( `id_profasig` ) ,
	KEY `id_profesor` ( `id_profesor` ) ,
	KEY `id_asignatura` ( `id_asignatura` , `curso` ) 
	) ENGINE = MYISAM DEFAULT CHARSET = utf8 COLLATE = utf8_spanish_ci COMMENT = 'profesores por asignatura y curso'";
	execsql($sql);
}

//cargar registros de periodos
Function cargar_periodos($ano)
{
	$anoant = $ano - 1;
	$sql="SELECT * FROM periodos WHERE ano = '$anoant' order by notanum";
	$datperi=viewsql($sql);
	foreach($datperi as $regperi)
	{
		$sql= "INSERT INTO periodos (pertipo, pernomb, perplan, periodo, notanum, ano) VALUES ('".$regperi->pertipo."', '".$regperi->pernomb."', '".$regperi->perplan."', '".$regperi->periodo."', '".$regperi->notanum."', '$ano')";
		execsql($sql);
	}
}

//cargar registros de puntajes calificaciones
Function cargar_puntajes($ano)
{
	$anoant = $ano - 1;
	$sql = "select * from puntajes where ano = '$anoant' order by id_puntaje";
	$datpunt = viewsql($sql);
	foreach($datpunt as $regpunt)
	{
		$sql= "INSERT INTO puntajes (notaQ, notamin, notamax, notadet, tipoedu, ano) VALUES ('".$regpunt->notaQ."', '".$regpunt->notamin."', '".$regpunt->notamax."', '".$regpunt->notadet."', '".$regpunt->tipoedu."', '$ano')";
		execsql($sql);
	}
}

//cargar registros de valores anuales matricula, pension, transporte
Function cargar_valores($ano)
{
	//crear valores por grados
	$sql="SELECT grado FROM grados ORDER BY id_grado";
	$datgrado=viewsql($sql);
	foreach($datgrado as $reggrado)
	{
		$grado = $reggrado->grado;
		$sql= "INSERT INTO gradosval (ano, grado, matricula, pension, transporte) VALUES ('$ano','$grado','0','0','0')";
		execsql($sql);
	}
}

//cargar rutas de transporte
Function cargar_rutas($ano)
{
	$anoant = $ano - 1;
	$sql = "select * from rutas where ano = '$anoant' order by id_ruta";
	$datrut = viewsql($sql);
	foreach($datrut as $regrut)
	{
		$sql= "INSERT INTO rutas (ano, ruta, recorrido, id_conductor, detalles) VALUES ('$ano', '".$regrut->ruta."', '".$regrut->recorrido."', '".$regrut->id_conductor."', '".$regrut->detalles."')";
		execsql($sql);
	}
}

//cargar contratos profesores
Function cargar_contratos($ano)
{
	$fecha_hora = date('Y-m-d') . " ". date('H:i:s');
	$anoant = $ano - 1;
	$sql = "select * from profesorescont where ano = '$anoant' and estado='ACTIVO' order by id_profesor";
	$datprof = viewsql($sql);
	foreach($datprof as $regprof)
	{
		$ini = $ano.substr($regprof->fchinicio,4);
		$fin = $ano.substr($regprof->fchfinal,4);
		$sql= "INSERT INTO profesorescont (id_profesor, ano, fchinicio, fchfinal, estado, tipocont, tiempo, grados, fchcrea, usucrea) VALUES ('".$regprof->id_profesor."', '$ano','$ini','$fin','ACTIVO','".$regprof->tipocont."','".$regprof->tiempo."','".$regprof->grados."', '$fecha_hora', '".$_SESSION["usulog"]."')";
		execsql($sql);
	}
}

//cargar registros de asignaturas y de logros
Function cargar_asignaturas($ano)
{
	$anoant = $ano - 1;
	$sql="select * from asignaturas where ano = '$anoant' ORDER BY grado, id_area";
	$datasig = viewsql($sql);
	foreach($datasig as $reg)
	{
		$idasig = $reg->id_asignatura;
		$sql= "INSERT INTO asignaturas (ano, grado, asignatura, id_area, area, codigo, intensidad, porcentaje, activmin, activprom, activpor, boletin, notas, perdida, promedio, observacion, notaQ, gradocolor, gradofont, fchcrea, usucrea, fchactual, usuactual) ".
		"VALUES ('$ano', '".$reg->grado."', '".$reg->asignatura."', '".$reg->id_area."', '".$reg->area."', '".$reg->codigo."', '".$reg->intensidad."', '".$reg->porcentaje."', '".$reg->activmin."', '".$reg->activprom."', '".$reg->activpor."', '".$reg->boletin."', '".
		$reg->notas."', '".$reg->perdida."', '".$reg->promedio."', '".$reg->observacion."', '".$reg->notaQ."', '".$reg->gradocolor."', '".$reg->gradofont."', '".$reg->fchcrea."', '".$reg->usucrea."', '".$reg->fchactual."', '".$reg->usuactual."')";
		execsql($sql);
		//traer nuevo id asignatura
		$sql = "select id_asignatura from asignaturas WHERE ano = '$ano' AND grado='".$reg->grado."' AND id_area = '".$reg->id_area."' AND asignatura = '".$reg->asignatura."' LIMIT 1";
		$datasignw = viewsql($sql);
		foreach($datasignw as $regasignw)
		{$id_asignatura = $regasignw->id_asignatura;}
		//crear logros asignatura
		$logros = "y".$anoant."logros";
		$logrosnw = "y".$ano."logros";
		$sql = "select * from $logros WHERE id_asignatura = '$idasig' ORDER BY id_logro";
		$datlogr = viewsql($sql);
		foreach($datlogr as $regl)
		{
			$sql= "INSERT INTO $logrosnw (id_asignatura, logro, logrosi, logrono, logrocod, periodos, tipoedu, activmin, activpor, grado, fchcrea, usucrea, fchactual, usuactual) ".
			"VALUES ('$id_asignatura', '".$regl->logro."', '".$regl->logrosi."', '".$regl->logrono."', '".$regl->logrocod."', '".$regl->periodos."', '".$regl->tipoedu."', '".
			$regl->activmin."', '".$regl->activpor."', '".$regl->grado."', '".$regl->fchcrea."', '".$regl->usucrea."', '".$regl->fchactual."', '".$reg->usuactual."')";
			execsql($sql);
		}		
		//crear horarios asignatura
		$sql="select * from horarios WHERE ano = '$anoant' AND id_asignatura = '$idasig' ORDER BY dia, intervalo, grado, curso";
		$dathora = viewsql($sql);
		foreach($dathora as $regh)
		{
			$sql= "INSERT INTO horarios (ano,dia,intervalo,grado,curso,id_profesor,id_asignatura) VALUES ('$ano','".$regh->dia."','".$regh->intervalo."','".$regh->grado."','".$regh->curso."','".$regh->id_profesor."','$id_asignatura')";
			execsql($sql);
		}
		//carga acad�mica profesores
		$profasig = "y".$anoant."profasig";
		$profasignw = "y".$ano."profasig";
		$sql = "select * from $profasig WHERE id_asignatura = '$idasig' ORDER BY id_profasig";
		$datprof = viewsql($sql);
		foreach($datprof as $regp)
		{
			$sql= "INSERT INTO $profasignw (id_asignatura, id_profesor, profesor, grado, curso) ".
			"VALUES ('$id_asignatura', '".$regp->id_profesor."', '".$regp->profesor."', '".$regp->grado."', '".$regp->curso."')";
			execsql($sql);
		}
	}
}

//cargar registros de cursos
Function cargar_cursos($ano)
{
	$anoant = $ano - 1;
	$sql="select * from cursos where ano = '$anoant' ORDER BY grado, curso";
	$datcurs = viewsql($sql);
	foreach($datcurs as $reg)
	{
		$sql= "INSERT INTO cursos (ano,grado,curso,id_director) VALUES ('$ano','".$reg->grado."','".$reg->curso."','".$reg->id_director."')";
		execsql($sql);
	}
}

//crear matriculas del a�o seg�n resultados a�o anterior y en estado PENDIENTE
Function cargar_matriculas($ano)
{
	$fecha_hora = date('Y-m-d') . " ". date('H:i:s');
	$anoant = $ano - 1;
	$ycursoant = "y".$anoant."cursos";
	$sql="SELECT * FROM anoslect WHERE ano = '$ano' LIMIT 1";
	$datanol=viewsql($sql);
	foreach($datanol as $reganol)
	{
		$fchmatpre = $reganol->fchmatpre;
		$fchmatpri = $reganol->fchmatpri;
		$fchmatbac = $reganol->fchmatbac;
	}
	//matriculas a�o anterior, cerradas, no grado 11 y alumnos activos
	$sql="SELECT matriculas.nivel,matriculas.grado,matriculas.id_respmatri,matriculas.id_respnotas,matriculas.id_resppagos,alumnos.id_alumno FROM matriculas, alumnos WHERE matriculas.id_alumno = alumnos.id_alumno AND matriculas.ano = '$anoant' AND matriculas.grado != '11' AND matriculas.estado = 'CERRADA' AND matriculas.renovar = '1' AND alumnos.estado!='RETIRADO' ORDER BY alumnos.id_alumno";
	$datmatr = viewsql($sql);
	$nummatr = count($datmatr);
	foreach($datmatr as $reg)
	{
		$id_alumno = $reg->id_alumno;
		$id_respmatri = $reg->id_respmatri;
		$id_respnotas = $reg->id_respnotas;
		$id_resppagos = $reg->id_resppagos;
		$nivel = $reg->nivel;
		$grado = $reg->grado;
		$pierde = 0;
		$sql="SELECT pierde FROM $ycursoant WHERE id_alumno = '$id_alumno' LIMIT 1";
		$datcurs=viewsql($sql);
		foreach($datcurs as $regcurs)
		{$pierde = intval($regcurs->pierde);}
		if ($pierde <= 0)
		{
			$sql="SELECT gradosig FROM grados WHERE grado = '$grado' LIMIT 1";
			$datgrad=viewsql($sql);
			foreach($datgrad as $reggrad)
			{$grado = $reggrad->gradosig;}
			$sql="SELECT nivel FROM grados WHERE grado = '$grado' LIMIT 1";
			$datgrad=viewsql($sql);
			foreach($datgrad as $reggrad)
			{$nivel = $reggrad->nivel;}
		}
		$fecha = "";
		if ($nivel == "K")
			$fecha = $fchmatpre; 
		if ($nivel == "P")
			$fecha = $fchmatpri; 
		if ($nivel == "S")
			$fecha = $fchmatbac;
		$gradovalmat = 0;
		$gradovalpen = 0;
		$sql="SELECT matricula, pension FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
		$datval=viewsql($sql);
		foreach($datval as $regval)
		{
			$gradovalmat = $regval->matricula;
			$gradovalpen = $regval->pension;
		}
		$sql="select id_matricula from matriculas where id_alumno = '$id_alumno' and ano = '$ano' LIMIT 1";
		$datreg = viewsql($sql);
		if (count($datreg) <= 0)
			$sql = "INSERT INTO matriculas (id_alumno,ano,nivel,grado,estado,fecha,id_respmatri,id_resppagos,id_respnotas,matricula,pension02,pension03,pension04,pension05,pension06,pension07,pension08,pension09,pension10,pension11,fchcrea,usucrea) ".
			"VALUES ('$id_alumno','$ano','$nivel','$grado','CERRADA','$fecha','$id_respmatri','$id_resppagos','$id_respnotas','$gradovalmat','$gradovalpen','$gradovalpen','$gradovalpen','$gradovalpen','$gradovalpen','$gradovalpen','$gradovalpen','$gradovalpen','$gradovalpen','$gradovalpen','$fecha_hora','".$_SESSION["usulog"]."')";
		else
			$sql = "UPDATE matriculas SET nivel='$nivel',grado='$grado',estado='CERRADA',fecha='$fecha' WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1"; 
		execsql($sql);
	}
}

/********************************************************************************/
/* Funci�n que asigna curso a alumnos matriculados en inicio de a�o             */
/* Recibe a�o y grado                                                           */ 
/********************************************************************************/
function asignar_cursos($ano, $grado)
{
	$ycurso = "y".$ano."cursos";
	$anoant = $ano - 1;
	$ycursoant = "y".$anoant."cursos";
	//traer nivel del grado
	$nivel = "";
	$perdida = 0; //preescolar, primero, segundo no pierden a�o ni se cambian de curso
	$sql = "select nivel, perdida from grados where grado = '$grado' limit 1";
	$datniv=viewsql($sql);
	foreach($datniv as $regniv)
	{
		$nivel = $regniv->nivel;
		$perdida = $regniv->perdida;
	}
	//traer cursos del grado
	$sql = "select curso from cursos where ano = '$ano' and grado = '$grado' order by curso";
	$datcurs=viewsql($sql);
	$cursos = array();
	$num = 0;
	foreach($datcurs as $regcurs)
	{
		$num = $num + 1;
		$cursos[$num] = $regcurs->curso;
	}
	$total = $num;
	//traer matriculas cerradas
	$num = 0;
	$sql="SELECT matriculas.id_matricula, matriculas.id_alumno, alumnos.apellidos, alumnos.nombres, alumnos.tipoedu FROM matriculas, alumnos WHERE matriculas.ano = '$ano' and matriculas.grado='$grado' and matriculas.estado = 'CERRADA' and matriculas.id_alumno = alumnos.id_alumno ORDER BY alumnos.genero, alumnos.apellidos, alumnos.nombres";
	$datalum=viewsql($sql);
	foreach($datalum as $regalum)
	{
		$id_matricula = $regalum->id_matricula;
		$id_alumno = $regalum->id_alumno;
		$apellidos = $regalum->apellidos;
		$nombres = $regalum->nombres;
		$tipoedu = $regalum->tipoedu;
		//area perdida a�o anterior
		$id_areaper = 0;
		$cursoant = "";
		$sql = "select id_areaper, curso from $ycursoant where id_alumno = '$id_alumno' limit 1";
		$data=viewsql($sql);
		foreach($data as $reg)
		{
			$id_areaper = $reg->id_areaper;
			$cursoant = $reg->curso;
		}
		//si cambia de curso (grados que pierden a�o)
		if ($perdida==1)
		{
			$num = $num + 1;
			if ($num > $total)
				$num = 1;
			$curso = $cursos[$num];
		}
		else //si no cambia conserva el mismo grupo del a�o anterior
		{
			$curso = "";
			if ($cursoant!="")
			{
				$lon = strlen($cursoant);
				$numcur = substr($cursoant,$lon-1,1);
				if ($numcur > 0) //curso en numero
					$curso = $cursos[$numcur];
				else //curso en letra
					$curso = $numcur;
			}
		}
		//crear registro de curso a�o actual
		$id_curso = 0;
		$sql = "select id_curso from $ycurso where id_alumno = '$id_alumno' limit 1";
		$data=viewsql($sql);
		foreach($data as $reg)
		{$id_curso = $reg->id_curso;}
		if ($id_curso <= 0)
			$sql= "INSERT INTO $ycurso (nivel, grado, curso, id_alumno, apellidos, nombres, tipoedu, id_areadeb) VALUES ('$nivel','$grado','$curso','$id_alumno','$apellidos','$nombres','$tipoedu','$id_areaper')";
		else
			$sql="UPDATE $ycurso SET curso = '$curso', id_areadeb = '$id_areaper' WHERE id_curso = '$id_curso' limit 1";
		execsql($sql);
		$sql="UPDATE matriculas SET curso = '$curso' WHERE id_matricula = '$id_matricula' limit 1";
		execsql($sql);
	}
}
?>