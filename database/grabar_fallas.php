<?php

/* * **********************************************************************************************
 * @file        : grabar_fallas.php                                                              *
 * @brief       : Graba fallas alumnos o actualiza los datos                                     *
 * @version     : 1.0                                                                            *
 * @company     : IDIC                                                                           *
 * @author      : Nora Rodriguez 15-feb-2012                                                     *
 * @Modificado  : Nora Rodriguez 22-oct-2018                                                     *
 * ********************************************************************************************** */
session_start();
if ($_SESSION["usulog"] == "") {
    echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";
}
include("../webparam.php");
include("../libs/fechas.php");
include("database.php");
connect();
$op = isset($_POST['op']) ? $_POST['op'] : null;
if ($op == "")
    $op = isset($_GET['op']) ? $_GET['op'] : null;
$fecha_hora = date('Y-m-d') . " " . date('H:i:s');

//grabar falla del alumno en una fecha por horas de asignatura (Pantalla de edicion de novedad)
//registro principal con la fecha y categoria, subregistros con la fecha, asignatura y horas
if ($op == "grabarfalla") {
    $ano = $_POST["ano"];
    $fecha = fec_amd($_POST["fecha"]);
    $fchfin = fec_amd($_POST["fchfin"]);
    $fchjust = fec_amd($_POST["fchjust"]);
    $hcita = $_POST["hcita"];
    $periodo = $_POST["periodo"];
    $id_alumno = $_POST["id_alumno"];
    $id_categoria = $_POST["id_categoria"];
    $tipo = $_POST["tipo"];
    $concepto = strtoupper($_POST["concepto"]);
    $page = isset($_POST["page"]) ? $_POST["page"] : null;
    if ($fchfin=="")
        $fchfin = "0000-00-00";
    if ($fchjust=="" and $id_categoria > 0)
        $fchjust = date('Y-m-d');
    if ($fchjust=="")
        $fchjust = "0000-00-00";
     //verificar existencia de la falla
    $file = "y" . $_POST["ano"] . "fallas";
    $id_falla = 0;
    $sql = "select id_falla from $file where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura = '0' limit 1";
    $datfall = viewsql($sql);
    foreach ($datfall as $regfall) {
        $id_falla = $regfall->id_falla;
    }
    $total = 0;
    //actualizar falla principal en la fecha y alumno
    if ($id_falla <= 0) {
        $sql = "INSERT INTO $file (id_alumno, id_categoria, tipo, concepto, fecha, fchfin, fchjust, hcita, periodo, detalles, fchcrea, usucrea) VALUES ('$id_alumno', '$id_categoria', '$tipo', '$concepto', '$fecha', '$fchfin', '$fchjust', '$hcita', '$periodo', '" . strtoupper($_POST["detalles"]) . "', '$fecha_hora', '" . $_SESSION["usulog"] . "') ";
        execsql($sql);
        $total = 1;
    }
    else {
        $sql = "UPDATE $file SET tipo='$tipo', id_categoria='$id_categoria', concepto='$concepto', fchfin='$fchfin', fchjust='$fchjust', hcita='$hcita', detalles = '" . strtoupper($_POST["detalles"]) . "', fchactual='$fecha_hora', usuactual = '" . $_SESSION["usulog"] . "' where id_falla = '$id_falla' limit 1";
        execsql($sql);
        //Si no es AUTORIZACION ni tampoco CITACION borra fallas asignaturas ara volverlas a cargar
        if ($tipo!="A" and $tipo!="C")
        {
            $sql = "select id_falla from $file where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura > '0' ORDER BY id_falla";
            $datfall = viewsql($sql);
            foreach ($datfall as $regfall) {
               $id_falla = $regfall->id_falla;
               $sql = "DELETE FROM $file WHERE id_falla = '$id_falla' limit 1";
               execsql($sql);
            }
        }
    }
    //actualizar asignaturas con Inasistencia (Si no es Autorizacion no Citacion)
    if ($tipo!="A" and $tipo!="C") {
        $grado = $_POST["grado"];
        $curso = $_POST["curso"];
        $numdia = $_POST["numdia"];
        cargar_fallas($ano, $grado, $curso, $numdia, $id_alumno, $fecha, $periodo, $tipo, $id_categoria, $total);
    }
    if (substr($page, 0, 6) == "fallas") {
        if ($page == "fallascurso")
            echo"<script>alert('Registro actualizado...');window.close();</script>";
        else
            echo"<script>window.location.href='../fallas_perm.php?opcion=listar&id_alumno=$id_alumno&tipo=$tipo';</script>";
    } else
        echo"<script>window.location.href='../alumnos_fallas.php?id_alumno=$id_alumno';</script>";
}

//borrar y eliminar documento del alumno
if ($op == "borrarfalla") {
    $file = "y" . $_POST["ano"] . "fallas";
    $id_alumno = $_POST["id_alumno"];
    $fecha = fec_amd($_POST["fecha"]);
    $page = isset($_POST["page"]) ? $_POST["page"] : null;
    $sql = "select id_falla from $file where id_alumno = '$id_alumno' and fecha = '$fecha' ORDER BY id_falla";
    $datfall = viewsql($sql);
    foreach ($datfall as $regfall) {
        $id_falla = $regfall->id_falla;
        $sql = "DELETE FROM $file WHERE id_falla = '$id_falla' limit 1";
        execsql($sql);
    }
    disconnect();
    if (substr($page, 0, 6) == "fallas") {
        if ($page == "fallascurso")
            echo"<script>alert('Inasistencia eliminada...');window.opener.document.location.reload();window.close();</script>";
        else
            echo"<script>window.location.href='../fallas.php?opcion=registrofecha&fecha=$fecha';</script>";
    } else
        echo"<script>window.location.href='../alumnos_fallas.php?id_alumno=$id_alumno';</script>";
}

//grabar fallas de la fecha por cursos
if ($op == "grabarfallascurso") {
    $ano = $_POST["ano"];
    $fecha = $_POST["fecha"];
    $periodo = $_POST["periodo"];
    $numdia = $_POST["numdia"];
    $grado = $_POST["grado"];
    $curso = $_POST["curso"];
    $page = isset($_POST["page"]) ? $_POST["page"] : null;
    $file = "y".$ano."fallas";
    $cursos = "y".$ano."cursos";
    $sql="SELECT * FROM $cursos WHERE grado = '$grado' AND curso = '$curso' ORDER BY apellidos, nombres";
    $datalum=viewsql($sql);
    foreach($datalum as $regalum)
    {
        $id_alumno = $regalum->id_alumno;
	$campo1 = "op".$id_alumno;
	$tipo = isset($_POST[$campo1]) ? $_POST[$campo1] : null ;
	$campo2 = "ct".$id_alumno;
	$id_categoria = isset($_POST[$campo2]) ? $_POST[$campo2] : 0 ;
        $fchjust="0000/00/00";
        if ($id_categoria > 0) {
            $fchjust = date('Y-m-d');
        }
        if ($tipo!="") //si hay falla o retardo
        {
            $id_falla = 0;
            $total = 0;
            $sql = "select id_falla, fchjust from $file where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura = '0' limit 1";
            $datfall = viewsql($sql);
            foreach ($datfall as $regfall) {
                $id_falla = $regfall->id_falla;
                if (substr($regfall->fchjust,0,4)!="0000")
                    $fchhjust = $regfall->fchjust;
            }
            if ($id_falla <= 0) {
                $sql = "INSERT INTO $file (id_alumno, fecha, periodo, tipo, id_categoria, fchjust,  fchcrea, usucrea) VALUES ('$id_alumno', '$fecha', '$periodo', '$tipo', '$id_categoria', '$fchjust', '$fecha_hora', '" . $_SESSION["usulog"] . "') ";
                execsql($sql);
                $total = 1;
            } else {
                //no actualiza el tipo de novedad
                $sql = "UPDATE $file SET periodo='$periodo', id_categoria='$id_categoria', fchjust = '$fchjust', fchactual='$fecha_hora', usuactual = '" . $_SESSION["usulog"] . "' where id_falla = '$id_falla' limit 1";
                execsql($sql);
            }
            if ($tipo=="I") //Inasistencia
            {
                if ($id_falla <= 0) {
                    $sql = "select id_falla from $file where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura > '0' ORDER BY id_falla";
                    $datfall = viewsql($sql);
                    foreach ($datfall as $regfall) {
                        $id_falla = $regfall->id_falla;
                        $sql = "DELETE FROM $file WHERE id_falla = '$id_falla' limit 1";
                        execsql($sql);
                    }
                }
                cargar_fallas($ano, $grado, $curso, $numdia, $id_alumno, $fecha, $periodo, $tipo, $id_categoria, $total);
            }
        }
    }
    echo"<script>window.location.href='../fallas.php?fecha=$fecha&grado=" . $_POST["grado"] . "&curso=" . $_POST["curso"] . "';</script>";
}

//grabar fallas del alumno en varias fechas 
if ($op == "grabarfallasalumno") {
    $ano = $_POST["ano"];
    $id_alumno = $_POST["id_alumno"];
    $fallas = "y".$ano."fallas";
    $sql = "SELECT * FROM $fallas WHERE id_alumno = '$id_alumno' AND (tipo='I' OR tipo='R' OR tipo='P') ORDER BY fecha desc";
    $datfall=viewsql($sql);
    foreach($datfall as $regfall)
    {
        $id_falla = $regfall->id_falla;
        $fecha = $regfall->fecha;
	$campo1 = "op".$id_falla;
	$tipo = isset($_POST[$campo1]) ? $_POST[$campo1] : null ;
	$campo2 = "ct".$id_falla;
	$id_categoria = isset($_POST[$campo2]) ? $_POST[$campo2] : 0 ;
	$campo3 = "pe".$id_falla;
	$concepto = isset($_POST[$campo3]) ? $_POST[$campo3] : "" ;
        $concepto = strtoupper($concepto);
        $fchjust=$regfall->fchjust;
        if (substr($regfall->fchjust,0,4)=="0000") {
            $fchjust = date('Y-m-d');
        }
        if (($id_categoria > 0 and $id_categoria != $regfall->id_categoria) or $concepto!=$regfall->concepto) //si cambia concepto o excusa
        {
            $sql = "UPDATE $fallas SET concepto='$concepto', id_categoria='$id_categoria', fchjust = '$fchjust', fchactual='$fecha_hora', usuactual = '" . $_SESSION["usulog"] . "' where id_falla = '$id_falla' limit 1";
            execsql($sql);
            $sql = "select id_falla from $fallas where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura > '0' ORDER BY id_falla";
            $datfall = viewsql($sql);
            foreach ($datfall as $regfall) {
               $id_falla = $regfall->id_falla;
               $sql = "UPDATE $fallas SET id_categoria='$id_categoria', fchactual='$fecha_hora', usuactual = '" . $_SESSION["usulog"] . "' where id_falla = '$id_falla' limit 1";
               execsql($sql);
            }
        }
    }
    echo"<script>window.location.href='../fallas_perm.php?opcion=inasistencias&tipo=I&id_alumno=$id_alumno';</script>";
}


/* * ***************************************************************************** */
/* Funci�n que carga fallas por asignaturas por alumno y feha                     */
/* Recibe a�o, grado, curso                                                      */
/* * ***************************************************************************** */
function cargar_fallas($ano, $grado, $curso, $numdia, $id_alumno, $fecha, $periodo, $tipo, $id_categoria, $total)
{
    $file = "y".$ano."fallas";
    $asigna = array(); //arreglo de asignaturas
    $nhoras = array(); //arreglo de horas
    $interv = array(); //arreglo de intervalos
    for ($i = 1; $i <= 12; $i++) {
        $asigna[$i] = 0;
        $nhoras[$i] = 0;
        $interv[$i] = "";
    }
    //cargar arreglo de asignaturas y fallas para cada intervalo
    $sql = "select intervalo, id_asignatura from horarios where ano='$ano' and dia='$numdia' and grado='$grado' and curso='$curso' order by abs(intervalo)";
    $dathora = viewsql($sql);
    foreach ($dathora as $reghora)
    {
        $id_asignatura = $reghora->id_asignatura;
        $intervalo = $reghora->intervalo;
        $nameF = "F" . $intervalo;
        $chk = isset($_POST[$nameF]) ? $_POST[$nameF] : null;
        $falla = 0;
        if (($chk == "on" or $total == 1) and $id_asignatura > 0) {
            $falla = 1;  
        }
        $asigna[$intervalo] = $id_asignatura;
        $nhoras[$intervalo] = $falla;
        $interv[$intervalo] = $reghora->intervalo;
    }
    //acumula cantidad de horas e intervalos de la misma asignatura
    for ($i = 1; $i <= 12; $i++) {
        $id_asignatura = $asigna[$i];
        $horas = $nhoras[$i];
        $intervalos = $interv[$i];
        //verificar misma asignatura en diferente intervalo y acumular
        for ($j = 1; $j <= 12; $j++) {
            if ($j != $i and $asigna[$j] == $id_asignatura and $horas > 0 and $nhoras[$j] > 0)
            {
                $horas = $horas + $nhoras[$j];
                $intervalos = $intervalos . " " . $interv[$j];
                $asigna[$j] = 0;
                $nhoras[$j] = 0;
                $interv[$j] = "";
            }
        }
        $nhoras[$i] = $horas;
        $interv[$i] = $intervalos;
    }
    //crear registro de falla por cada asignatura con acumulado de horas e intervalos
    for ($i = 1; $i <= 12; $i++) {
        $id_asignatura = $asigna[$i];
        $horas = $nhoras[$i];
        $intervalos = " " . $interv[$i] . " ";
        if ($id_asignatura > 0 and $horas > 0) {
            $sql = "INSERT INTO $file (id_alumno, id_asignatura, id_categoria, tipo, fecha, periodo, horas, intervalos) VALUES ('$id_alumno', '$id_asignatura', '$id_categoria', '$tipo', '$fecha', '$periodo', '$horas', '$intervalos') ";
            execsql($sql);
        }
    }
}

?>