<?php

Function datos_plantilla($id_plantilla)
{
	$plantilla = "";
	$texto = "";
	$categoria = "";
	$setup = 0;
	if ($id_plantilla > 0)
	{
		$sql="select * from plantillas where id_plantilla = '$id_plantilla' limit 1";
		$datplan = viewsql($sql);
		foreach($datplan as $regplan)
		{
			$plantilla = $regplan->plantilla;
			$texto = $regplan->texto;
			$categoria = $regplan->categoria;
			$setup = $regplan->setup;
		}
	}
	$datplantilla = array();
	$datplantilla["plantilla"] = $plantilla;
	$datplantilla["texto"] = $texto;
	$datplantilla["categoria"] = $categoria;
	$datplantilla["setup"] = $setup;
	return $datplantilla;
}

Function datos_usuario($id_usuario)
{
	$id_tipo = 0;
	$tipo = "OPERADOR";
	$login = "";
	$apellidos = "";
	$nombres ="";
	$docunum = "";
	$correo = "";
	$password = "";		
	$foto = "";		
	$nivalu = 0;
	$nivmat = 0;
	$nivpro = 0;
	$nivasi = 0;
	$nivnot = 0;
	$nivfal = 0;
	$nivpag = 0;
	$nivtra = 0;
	$nivusu = 0;
	$nivpar = 0;
	$inactivar = 0;
	if ($id_usuario > 0)
	{
		$sql="select * from usuarios where id_usuario='$id_usuario' limit 1";
		$data=viewsql($sql);
		foreach($data as $reg)
		{
			$id_tipo = $reg->id_tipo;
			$tipo = $reg->tipo;
			$login = $reg->login;
			$apellidos = $reg->apellidos;
			$nombres = $reg->nombres;
			$docunum = $reg->docunum;
			$login = $reg->login;
			$correo = $reg->correo;
			$password = $reg->password;
			$foto = $reg->foto;
			$nivalu = $reg->nivalu;
			$nivmat = $reg->nivmat;
			$nivpro = $reg->nivpro;
			$nivasi = $reg->nivasi;
			$nivnot = $reg->nivnot;
			$nivfal = $reg->nivfal;
			$nivpag = $reg->nivpag;
			$nivtra = $reg->nivtra;
			$nivusu = $reg->nivusu;
			$nivpar = $reg->nivpar;
			$inactivar=$reg->inactivar;
			if (!file_exists($foto))
				$foto = "";
		}
	}
	$datusuario = array();
	$datusuario["id_tipo"] = $id_tipo;
	$datusuario["tipo"] = $tipo;
	$datusuario["login"] = $login;
	$datusuario["apellidos"] = $apellidos;
	$datusuario["nombres"] = $nombres;
	$datusuario["docunum"] = $docunum;
	$datusuario["correo"] = $correo;
	$datusuario["password"] = $password;
	$datusuario["foto"] = $foto;
	$datusuario["nivalu"] = $nivalu;
	$datusuario["nivmat"] = $nivmat;
	$datusuario["nivpro"] = $nivpro;
	$datusuario["nivasi"] = $nivasi;
	$datusuario["nivnot"] = $nivnot;
	$datusuario["nivfal"] = $nivfal;
	$datusuario["nivpag"] = $nivpag;
	$datusuario["nivtra"] = $nivtra;
	$datusuario["nivusu"] = $nivusu;
	$datusuario["nivpar"] = $nivpar;
	$datusuario["inactivar"] = $inactivar;
	//print_r($datusuario);	
	return $datusuario;
}

Function datos_anolect($ano)
{
	$estado = "ABIERTO";
	$notamin = 0;
	$notamax = 0;
	$notalim = 0;
        $notaevi = 0;
        $printevi = 0;
	$areasper = 0;
	$fallasper = 0;
	$protocolo = "";
	$fchmatpre = "";
	$fchmatpri = "";
	$fchmatbac = "";
	$interes = 0;
	if ($ano > 0)
	{
		$sql="SELECT * FROM anoslect WHERE ano = '$ano' LIMIT 1";
		$datanol=viewsql($sql);
		foreach($datanol as $reganol)
		{
			$estado = $reganol->estado;
			$notamin = $reganol->notamin;
			$notamax = $reganol->notamax;
			$notalim = $reganol->notalim;
			$notaevi = $reganol->notaevi;
			$printevi = $reganol->printevi;
			$areasper = $reganol->areasper;
			$fallasper = $reganol->fallasper;
			$protocolo = $reganol->protocolo;
			$interes = $reganol->interes;
			if (substr($reganol->fchmatpre,0,2) != "00")
				$fchmatpre = fec_dma($reganol->fchmatpre);
			if (substr($reganol->fchmatpri,0,2) != "00")
				$fchmatpri = fec_dma($reganol->fchmatpri);
			if (substr($reganol->fchmatbac,0,2) != "00")
				$fchmatbac = fec_dma($reganol->fchmatbac);
		}
	}
	else
	{
		$sql="SELECT * FROM anoslect ORDER BY ano desc LIMIT 1"; //parametros a�o anterior
		$datanol=viewsql($sql);
		foreach($datanol as $reganol)
		{
			$ano = intval($reganol->ano) + 1;
			$notamin = $reganol->notamin;
			$notamax = $reganol->notamax;
			$notalim = $reganol->notalim;
                        $notaevi = $reganol->notaevi;
                        $printevi = $reganol->printevi;
			$areasper = $reganol->areasper;
			$fallasper = $reganol->fallasper;
			$protocolo = $reganol->protocolo;
			$interes = $reganol->interes;
		}
	}
	$datanolect = array();
	$datanolect["ano"] = $ano;
	$datanolect["estado"] = $estado;
	$datanolect["notamin"] = $notamin;
	$datanolect["notamax"] = $notamax;
	$datanolect["notalim"] = $notalim;
	$datanolect["notaevi"] = $notaevi;
	$datanolect["printevi"] = $printevi;
	$datanolect["areasper"] = $areasper;
	$datanolect["fallasper"] = $fallasper;
	$datanolect["protocolo"] = $protocolo;
	$datanolect["fchmatpre"] = $fchmatpre;
	$datanolect["fchmatpri"] = $fchmatpri;
	$datanolect["fchmatbac"] = $fchmatbac;
	$datanolect["interes"] = $interes;
	return $datanolect;
}

Function datos_saludenti($id_saludenti)
{
	$entidad = "";
	$direccion = "";
	$telefono = "";
	$correo = "";
	if ($id_saludenti > 0)
	{
		$sql="select * from saludenti where id_saludenti = '$id_saludenti' limit 1";
		$datsale = viewsql($sql);
		foreach($datsale as $regsale)
		{
			$entidad = $regsale->entidad;
			$direccion = $regsale->direccion;
			$telefono = $regsale->telefono;
			$correo = $regsale->correo;
		}
	}
	$datsaludenti = array();
	$datsaludenti["entidad"] = $entidad;
	$datsaludenti["direccion"] = $direccion;
	$datsaludenti["telefono"] = $telefono;
	$datsaludenti["correo"] = $correo;
	return $datsaludenti;
}

?>