<?php
//print_r($datlogro);
Function datos_asignatura($id_asignatura)
{
	$ano = 0;
	$grado = "";
	$asignatura = "";
	$id_area = 0;
	$area = "";
	$codigo = "";
	$intensidad = "";
	$porcentaje = "";
	$evidemin = 3;
	$evideprom = 0;
	$evidepor = "";
	$activmin = 3;
	$activprom = 0;
	$activpor = "";
	$boletin = 1;
	$notas = 1;
	$promedio = 1;
	$perdida = 1;
	$observacion = 0;
	$notaQ = "";
	if ($id_asignatura > 0)
	{
		$sql="select * from asignaturas where id_asignatura = '$id_asignatura' limit 1";
		$datasig = viewsql($sql);
		foreach($datasig as $regasig)
		{
			$ano = $regasig->ano;
			$grado = $regasig->grado;
			$asignatura = $regasig->asignatura;
			$id_area = $regasig->id_area;
			$area = $regasig->area;
			$codigo = $regasig->codigo;
			$intensidad = $regasig->intensidad;
			$porcentaje = $regasig->porcentaje;
			$evidemin = $regasig->evidemin;
			$evideprom = $regasig->evideprom;
			$evidepor = $regasig->evidepor;
			$activmin = $regasig->activmin;
			$activprom = $regasig->activprom;
			$activpor = $regasig->activpor;
			$boletin = $regasig->boletin;
			$notas = $regasig->notas;
			$promedio = $regasig->promedio;
			$perdida = $regasig->perdida;
			$observacion = $regasig->observacion;
			$notaQ = $regasig->notaQ;
		}
	}
	$datasignatura = array();
	$datasignatura["ano"] = $ano;
	$datasignatura["grado"] = $grado;
	$datasignatura["asignatura"] = $asignatura;
	$datasignatura["id_area"] = $id_area;
	$datasignatura["area"] = $area;
	$datasignatura["codigo"] = $codigo;
	$datasignatura["intensidad"] = $intensidad;
	$datasignatura["porcentaje"] = $porcentaje;
	$datasignatura["evidemin"] = $evidemin;
	$datasignatura["evideprom"] = $evideprom;
	$datasignatura["evidepor"] = $evidepor;
	$datasignatura["activmin"] = $activmin;
	$datasignatura["activprom"] = $activprom;
	$datasignatura["activpor"] = $activpor;
	$datasignatura["boletin"] = $boletin;
	$datasignatura["notas"] = $notas;
	$datasignatura["promedio"] = $promedio;
	$datasignatura["perdida"] = $perdida;
	$datasignatura["observacion"] = $observacion;
	$datasignatura["notaQ"] = $notaQ;
	return $datasignatura;
}

Function datos_grado($id_grado)
{
	$grado = "";
	$gradonom = "";
	$gradosig = "";
	$nivel = "";
	$educacion = "";
	$gradocolor = "";
	$gradofont = "";
	$perdida = 1;
	$inactivar = 0;
	$superacion = 0;
	if ($id_grado > 0)
	{
		$sql="select * from grados where id_grado = '$id_grado' limit 1";
		$datgrado = viewsql($sql);
		foreach($datgrado as $reggrado)
		{
			$grado = $reggrado->grado;
			$gradonom = $reggrado->gradonom;
			$gradosig = $reggrado->gradosig;
			$nivel = $reggrado->nivel;
			$educacion = $reggrado->educacion;
			$gradocolor = $reggrado->gradocolor;
			$gradofont = $reggrado->gradofont;
			$perdida = $reggrado->perdida;
			$superacion = $reggrado->superacion;
			$inactivar = $reggrado->inactivar;
		}
	}
	$datgrado = array();
	$datgrado["grado"] = $grado;
	$datgrado["gradonom"] = $gradonom;
	$datgrado["gradosig"] = $gradosig;
	$datgrado["nivel"] = $nivel;
	$datgrado["educacion"] = $educacion;
	$datgrado["gradocolor"] = $gradocolor;
	$datgrado["gradofont"] = $gradofont;
	$datgrado["perdida"] = $perdida;
	$datgrado["superacion"] = $superacion;
	$datgrado["inactivar"] = $inactivar;
	return $datgrado;
}

Function datos_logro($ano,$id_logro)
{
	$logrocod = "";
	$tipoedu = "";
	$logro = "";
	$logrosi = "";
	$logrono = "";
	$periodos = "";
	$evidemin = 0;
	$evidepor = "";
	$activmin = 0;
	$activpor = "";
	$cursos = "";
	if ($id_logro > 0)
	{
		$file = "y".$ano."logros";
		$sql="select * from $file where id_logro = '$id_logro' limit 1";
		$datlogr = viewsql($sql);
		foreach($datlogr as $reglogr)
		{
			$logrocod = $reglogr->logrocod;
			$tipoedu = $reglogr->tipoedu;
			$logro = $reglogr->logro;
			$logrosi = $reglogr->logrosi;
			$logrono = $reglogr->logrono;
			$periodos = $reglogr->periodos;
			$evidemin = $reglogr->evidemin;
			$evidepor = $reglogr->evidepor;
			$activmin = $reglogr->activmin;
			$activpor = $reglogr->activpor;
			$cursos = $reglogr->cursos;
		}
	}
	$datlogro = array();
	$datlogro["logrocod"] = $logrocod;
	$datlogro["tipoedu"] = $tipoedu;
	$datlogro["logro"] = $logro;
	$datlogro["logrosi"] = $logrosi;
	$datlogro["logrono"] = $logrono;
	$datlogro["periodos"] = $periodos;
	$datlogro["evidemin"] = $evidemin;
	$datlogro["evidepor"] = $evidepor;
	$datlogro["activmin"] = $activmin;
	$datlogro["activpor"] = $activpor;
	$datlogro["cursos"] = $cursos;
	return $datlogro;
}

Function datos_actividad($ano,$id_actividad)
{
	$periodo = 0;
	$actividad = "";
	$porcentaje = 0;
	$codigo = "";
	$numero = "";
	if ($id_actividad > 0)
	{
		$file = "y".$ano."activ";
		$sql="select * from $file where id_actividad = '$id_actividad' limit 1";
		$datacti = viewsql($sql);
		foreach($datacti as $regacti)
		{
			$periodo = $regacti->periodo;
			$actividad = $regacti->actividad;
			$porcentaje = $regacti->porcentaje;
			$codigo = $regacti->codigo;
			$numero = $regacti->numero;
		}
	}
	$datactividad = array();
	$datactividad["periodo"] = $periodo;
	$datactividad["actividad"] = $actividad;
	$datactividad["porcentaje"] = $porcentaje;
	$datactividad["codigo"] = $codigo;
	$datactividad["numero"] = $numero;
	return $datactividad;
}

Function datos_observacion($id_observacion)
{
	$codigo = "";
	$observacion = "";
	$niveles = "";
	$id_categoria = 0;
	$director = 0;
	$inactivar = 0;
	if ($id_observacion > 0)
	{
		$sql="select * from observaciones where id_observacion = '$id_observacion' limit 1";
		$datobse = viewsql($sql);
		foreach($datobse as $regobse)
		{
			$codigo = $regobse->codigo;
			$observacion = $regobse->observacion;
			$niveles = $regobse->niveles;
			$id_categoria = $regobse->id_categoria;
			$director = $regobse->director;
			$inactivar = $regobse->inactivar;
		}
	}
	$datobservacion = array();
	$datobservacion["codigo"] = $codigo;
	$datobservacion["observacion"] = $observacion;
	$datobservacion["niveles"] = $niveles;
	$datobservacion["id_categoria"] = $id_categoria;
	$datobservacion["director"] = $director;
	$datobservacion["inactivar"] = $inactivar;
	return $datobservacion;
}

Function datos_profasig($ano,$id_profasig)
{
	$id_profesor = 0;
	$curso = "";
	if ($id_profasig > 0)
	{
		$file = "y".$ano."profasig";
		$sql="select * from $file where id_profasig = '$id_profasig' limit 1";
		$datprof = viewsql($sql);
		foreach($datprof as $regprof)
		{
			$id_profesor = $regprof->id_profesor;
			$curso = $regprof->curso;
		}
	}
	$datprofasig = array();
	$datprofasig["id_profesor"] = $id_profesor;
	$datprofasig["curso"] = $curso;
	return $datprofasig;
}

Function lista_profesores($ano,$grado)
{
	$sql="SELECT profesorescont.id_profesor, profesores.apellidos, profesores.nombres FROM profesores, profesorescont WHERE ".
	"profesorescont.fchinicio like '$ano%' AND profesorescont.grados LIKE '% $grado %' ";
	$sql.="AND profesorescont.id_profesor = profesores.id_profesor ORDER BY profesores.apellidos, profesores.nombres";
	$datprof = viewsql($sql);
	return $datprof;
}
?>