<?php
//1. datos asignatura
$sql="select * from asignaturas where id_asignatura = '$id_asignatura' limit 1";
$datasig = viewsql($sql);
foreach($datasig as $regasig)
{
	$ano = $regasig->ano;
	$grado = $regasig->grado;
	$gradocolor = $regasig->gradocolor;	
	$gradofont = $regasig->gradofont;	
	$id_area = $regasig->id_area;
	$asignatura = $regasig->asignatura;
	$intensidad = $regasig->intensidad;
	$porcentaje = $regasig->porcentaje;
	$activmin = $regasig->activmin;
	$codigo = $regasig->codigo;
}
//periodos del a�o
$estadoper="ABIERTO";
$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
$datperi = viewsql($sql);
foreach($datperi as $regperi)
{
	if ($regperi->pertipo == $pertipo and $regperi->periodo == $periodo)
	{
		$corte = $regperi->notanum;
		$pernomb = $regperi->pernomb;
		$fchinicio = $regperi->fchinicio;
		$fchfinal = $regperi->fchfinal;
		$fchplazo = $regperi->fchplazo;
		if ($regperi->estado!="")
			$estadoper=$regperi->estado;
		if ($pertipo=="E")
			$regperi->estado = "CERRADO";
	}
}
//2. curso y estado de la asignatura al profesor
$seccion = "seccion".$id_profasig;
$profasig = "y".$ano."profasig";
$sql="select curso, id_profesor, profesor from $profasig where id_profasig = '$id_profasig' limit 1";
$datasig=viewsql($sql);
foreach($datasig as $regasig)
{
	$curso = $regasig->curso;
	$id_profesor = $regasig->id_profesor;
	$profesor = $regasig->profesor;
}
$estado = "ABIERTA";
$fchentrega = "";
$fchcierre = "";
$planillas = "y".$ano."planillas";
$sql = "select * from $planillas where grado = '$grado' and curso = '$curso' and id_asignatura = '$id_asignatura' and pertipo = '$pertipo' and periodo = '$periodo' limit 1";
$datasig=viewsql($sql);
foreach($datasig as $regasig)
{
	$estado = trim($regasig->estado);
	if ($estado == "ENTREGADA")
		$fchentrega = fecha_texto($regasig->fchentrega);
	if ($estado == "CERRADA")
		$fchcierre = fecha_texto($regasig->fchcierre);
	if ($regasig->id_profesor != $id_profesor) //planilla entregada por otro profesor deja el que la entreg�
		$id_profesor = $regasig->id_profesor;
	if ($estado=="")
		$estado = "ABIERTA";
}
if ($estadoper=="CERRADO")
	$estado = "CERRADA";
//3. notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = $reganol->notalim;
        $notaevi = $reganol->notaevi;
	$areasper = $reganol->areasper;
}
//4. puntajes
$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
$datpunt = viewsql($sql);
$numpunt = count($datpunt);
//5. traer logros de la asignatura, periodo y tipo de educaci�n
$numlogr = 0;
$numacti = 0;
if ($pertipo == "P")
{
	$logrosasig = "y".$ano."logros";
	$sql = "SELECT id_logro, logrocod, logro, activmin FROM $logrosasig WHERE id_asignatura = '$id_asignatura' AND periodos LIKE '% $periodo %' ";
	$sql.="AND tipoedu = '$tipoedu' ORDER BY logrocod";
	$datlogr=viewsql($sql);
	$numlogr = count($datlogr);
        $evidencias = "y".$ano."eviden";
	$sql = "SELECT id_evidencia, evidencia, id_logro, codigo, numero FROM $evidencias WHERE id_asignatura = '$id_asignatura' AND periodo = '$periodo' order by id_logro, numero";
	$datevid = viewsql($sql);
	$numevid = count($datevid);
        $actividades = "y".$ano."activ";
	$sql = "SELECT id_actividad, actividad, id_logro, id_evidencia, codigo, numero FROM $actividades WHERE id_asignatura = '$id_asignatura' AND periodo = '$periodo' order by id_evidencia, numero";
	$datacti = viewsql($sql);
	$numacti = count($datacti);
}
//6. verificar si existen alumnos educaci�n especial en el curso
$alumtot = 0;
$alumesp = 0;
$id_director = 0;
$sql = "SELECT alumesp, alumtot, id_director from cursos WHERE ano = '$ano' and grado = '$grado' and curso = '$curso' limit 1";
$datcurs = viewsql($sql);
foreach($datcurs as $regcurs)
{
	$alumesp = $regcurs->alumesp;
	$alumtot = $regcurs->alumtot;
	if (intval($regcurs->id_director)==intval($id_profesor)) //si usuario es director de grupo del curso
		$id_director = $regcurs->id_director;
}
//7. traer lista de alumnos del grado, curso y tipo de educaci�n
$notas = "y".$ano."notas";
$notasdet = "y".$ano."notasp".$periodo;
$cursos = "y".$ano."cursos";
$sql="SELECT * FROM $cursos WHERE grado = '$grado' AND curso = '$curso' ";
if ($general == "N")
	$sql.="AND tipoedu = '$tipoedu' ";
$sql.="ORDER BY apellidos, nombres";
$datalum=viewsql($sql);
?>