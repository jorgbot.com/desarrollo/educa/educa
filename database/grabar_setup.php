<?php
/************************************************************************************************
* @file        : grabar_alumno.php                                                              *
* @brief       : Graba o actualiza tablas generales de setup                                    *
* @version     : 1.0                                                                            *
* @company     : IDIC                                                                           *
* @author      : Nora Rodriguez 28-mar-2012                                                     *
* @Modificado  : Nora Rodriguez 06-nov-2018                                                     *
************************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
include("database.php");
include("../webparam.php");
include("../libs/fechas.php");
connect();
$op = isset($_POST['op']) ? $_POST['op'] : null ;
if ($op == "")
	$op = isset($_GET['op']) ? $_GET['op'] : null ;
$fecha_hora = date('Y-m-d') . " ". date('H:i:s');

//activar a�o lectivo en sesi�n
if ($op == "activaranolect")
{
	$ano = $_POST["ano"];
	$_SESSION["anoaca"] = $ano;
	echo"<script>alert('A�o $ano activado en su sesi�n de usuario.');window.opener.document.location.reload();window.close();</script>";		
}

//grabar a�o lectivo
if ($op == "grabaranolect")
{
	$ano = $_POST["ano"];
	$fchmatpri = fec_amd($_POST["fchmatpri"]);
	$fchmatpre = fec_amd($_POST["fchmatpre"]);
	$fchmatbac = fec_amd($_POST["fchmatbac"]);
        $chkevi = isset($_POST['chkevi']) ? $_POST['chkevi'] : null ;
        $chkprintevi = isset($_POST['chkprintevi']) ? $_POST['chkprintevi'] : null ;
        $notaevi = 0;
        if ($chkevi=="on") {
            $notaevi = 1;}
        $printevi = 0;
        if ($chkprintevi=="on") {
            $printevi = 1;}
	$sql="SELECT * FROM anoslect WHERE ano = '$ano' LIMIT 1"; //parametros a�o anterior
	$datanol=viewsql($sql);
	if (count($datanol) > 0)
	{
		$sql="UPDATE anoslect SET estado='".$_POST["estado"]."', notamin='".intval($_POST["notamin"])."',notamax='".intval($_POST["notamax"])."',notalim='".intval($_POST["notalim"])."',notaevi='$notaevi',printevi='$printevi',areasper='".intval($_POST["areasper"])."',fallasper='".intval($_POST["fallasper"])."',interes='".intval($_POST["interes"])."', protocolo='".$_POST["protocolo"]."', fchmatpre='$fchmatpre', fchmatpri='$fchmatpri', fchmatbac='$fchmatbac' ";
		$sql.="WHERE ano = '$ano' limit 1";
		execsql($sql);
	}
	else
	{
		$sql= "INSERT INTO anoslect (estado,notamin,notamax,notalim,notaevi,printevi,areasper,fallasper,interes,fchmatpre,fchmatpri,fchmatbac,protocolo) VALUES ('ABIERTO', '".intval($_POST["notamin"])."','".intval($_POST["notamax"])."','".intval($_POST["notalim"])."','".$notaevi."','".$printevi."','".intval($_POST["areasper"])."','".intval($_POST["fallasper"])."','".intval($_POST["interes"])."','$fchmatpre','$fchmatpri','$fchmatbac','".$_POST["protocolo"]."')";
		execsql($sql);
		include_once("iniciar_anolectivo.php");
		iniciar_anolectivo($ano);
	}
	echo"<script>alert('A�o lectivo actualizado...');window.opener.document.location.reload();window.close();</script>";		
}

//grabar categoria
if ($op == "grabarcategoria")
{
	$id_categoria = $_POST["id_categoria"];
	$categoria = strtoupper($_POST["categoria"]);
	$tip_categoria = $_POST["tip_categoria"];
	//verificar que no se duplique
	$error = "";
	$sql="select id_categoria from categorias where tip_categoria = '$tip_categoria' and categoria = '$categoria' and id_categoria != '$id_categoria' limit 1";
	$datcategoria = viewsql($sql);
	if (count($datcategoria) > 0 )
		$error = "categoria ya existente...";
	if ($error == "")
	{
		if ($id_categoria > 0)
			$sql="UPDATE categorias SET categoria='$categoria' WHERE id_categoria = '$id_categoria' limit 1";
		else
			$sql= "INSERT INTO categorias (categoria, tip_categoria) VALUES ('$categoria','$tip_categoria')";
		execsql($sql);
		disconnect();
		echo"<script> window.location.href='../setup_categorias.php?tip_categoria=$tip_categoria';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../setup_categorias.php?id_categoria=$id_categoria&tip_categoria=$tip_categoria';</script>";
	}
}

//grabar plantilla
if ($op == "grabarplantilla")
{
	$id_plantilla = $_POST["id_plantilla"];
	//verificar que no se duplique titulo
	$error = "";
	$sql="select id_plantilla from plantillas where categoria = '".$_POST["categoria"]."' and plantilla = '".strtoupper($_POST["plantilla"])."' and id_plantilla != '$id_plantilla' limit 1";
	$datplan = viewsql($sql);
	if (count($datplan) > 0 )
		$error = "Plantilla ya existente...";
	if ($error == "")
	{
		if ($id_plantilla > 0)
			$sql="UPDATE plantillas SET plantilla='".strtoupper($_POST["plantilla"])."',categoria='".$_POST["categoria"]."',texto='".$_POST["texto"]."' WHERE id_plantilla = '$id_plantilla' limit 1";
		else
			$sql= "INSERT INTO plantillas (plantilla, categoria, texto) VALUES ('".strtoupper($_POST["plantilla"])."','".$_POST["categoria"]."','".$_POST["texto"]."')";
		execsql($sql);
		disconnect();
		echo"<script>window.location.href='../setup_plantillas.php?opcion=editar&id_plantilla=$id_plantilla';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../setup_plantillas.php?opcion=editar&id_plantilla=$id_plantilla';</script>";
	}
}

//grabar usuario
if($op=="grabarusuario")
{
	$id_usuario = $_POST["id_usuario"];
	$login = strtoupper($_POST["login"]);
	$oldpass = $_POST["oldpass"];
	$psw = $_POST["psw"];
	//variables
	if ($psw!=$oldpass)
		$psw = md5($psw);
	//verificar login
	$error = "";
	$sql="select login from usuarios where login='$login' and id_usuario!='$id_usuario' limit 1";
	$res=viewsql($sql);
	if (count($res) > 0 )
		$error = "Login duplicado...";
	if ($error == "")
	{	
		if ($id_usuario <= 0)
		{
			$sql = "insert into usuarios (id_tipo, tipo, login, apellidos, nombres, docunum, correo, password, foto, nivalu, nivmat, nivpro, nivasi, nivnot, nivfal, nivpag, nivtra, nivusu, nivpar, fchcrea,usucrea) ".
			"Values('".$_POST["id_tipo"]."','".$_POST["tipo"]."','$login','".strtoupper($_POST["apellidos"])."','".strtoupper($_POST["nombres"])."','".intval($_POST["docunum"])."','".$_POST["correo"].
			"','$psw', '".$_POST["foto"]."', '".$_POST["nivusu"]."','".$_POST["nivmat"]."','".$_POST["nivpro"]."','".$_POST["nivasi"]."','".$_POST["nivnot"]."','".$_POST["nivfal"]."','".$_POST["nivpag"]."','".$_POST["nivtra"]."','".$_POST["nivusu"]."','".$_POST["nivpar"]."','$fecha_hora','".$_SESSION["usulog"]."')";
			execsql($sql);
			$sql="select id_usuario from usuarios where login='$login' limit 1";
			$data=viewsql($sql);
			foreach($data as $registros)
			{$id_usuario=$registros->id_usuario;}
		}
		else
		{
			if ($_POST["origen"]=="USUARIO")
				$sql="update usuarios set password='$psw' where id_usuario='$id_usuario' limit 1";
			else
			{
				$sql="update usuarios set login='$login', ";
				if ($_POST["tipo"]=="OPERADOR")
					$sql.="apellidos='".strtoupper($_POST["apellidos"])."', nombres='".strtoupper($_POST["nombres"])."', docunum='".intval($_POST["docunum"])."',";
				$sql.="nivalu='".$_POST["nivalu"]."',nivmat='".$_POST["nivmat"]."', nivpro='".$_POST["nivpro"]."', nivasi='".$_POST["nivasi"]."', nivnot='".$_POST["nivnot"]."', nivfal='".$_POST["nivfal"]."', nivpag='".$_POST["nivpag"]."', nivtra='".$_POST["nivtra"]."', nivusu='".$_POST["nivusu"]."', nivpar='".$_POST["nivpar"]."', ";
				$sql.="correo='".$_POST["correo"]."', password='$psw', foto='".$_POST["foto"]."', fchactual='$fecha_hora', usuactual='".$_SESSION["usulog"]."' where id_usuario='$id_usuario' limit 1";
			}
			execsql($sql);
		}
		disconnect();
		echo"<script> alert('Usuario Grabado.'); window.location.href='../setup_usuariosedit.php?id_usuario=$id_usuario';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../setup_usuariosedit.php?id_usuario=$id_usuario';</script>";
	}
}

//borrar usuario
if ($op == "borrarusuario")
{
	$id_usuario=$_POST["id_usuario"];
	$sql = "delete from usuarios where id_usuario='$id_usuario' limit 1";
	execsql($sql);
	echo"<script>alert('Usuario Eliminado...');window.opener.document.location.reload();window.close();</script>";
}

//grabar entidad de salud
if ($op == "grabarsaludenti")
{
	$id_saludenti = $_POST["id_saludenti"];
	//verificar que no se duplique titulo
	$error = "";
	$sql="select id_saludenti from saludenti where entidad = '".strtoupper($_POST["entidad"])."' and id_saludenti != '$id_saludenti' limit 1";
	$datsalu = viewsql($sql);
	if (count($datsalu) > 0 )
		$error = "Entidad ya existente...";
	if ($error == "")
	{
		if ($id_saludenti > 0)
			$sql="UPDATE saludenti SET entidad='".strtoupper($_POST["entidad"])."',direccion='".$_POST["direccion"]."',telefono='".$_POST["telefono"]."',correo='".$_POST["correo"]."' WHERE id_saludenti = '$id_saludenti' limit 1";
		else
			$sql= "INSERT INTO saludenti (entidad, direccion ,telefono, correo) VALUES ('".strtoupper($_POST["entidad"])."','".$_POST["direccion"]."','".$_POST["telefono"]."','".$_POST["correo"]."')";
		execsql($sql);
		disconnect();
		echo"<script>window.location.href='../setup_saludenti.php';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../setup_saludenti.php?opcion=editar&id_saludenti=$id_saludenti';</script>";
	}
}

//grabar parametros
if ($op == "grabarparametros")
{
	$sql="UPDATE institucion SET nit='".$_POST["nit"]."',ciudad='".$_POST["ciudad"]."',direccion='".$_POST["direccion"]."',telefono='".$_POST["telefono"]."',correo1='".$_POST["correo1"]."',correo2='".$_POST["correo2"]."',correo3='".$_POST["correo3"]."',".
	"rector='".strtoupper($_POST["rector"])."',secretaria='".strtoupper($_POST["secretaria"])."',banco='".strtoupper($_POST["banco"])."',resolseed='".$_POST["resolseed"]."',resoldane='".$_POST["resoldane"]."',resolkin='".$_POST["resolkin"]."',resolpri='".$_POST["resolpri"]."',resolsec='".$_POST["resolsec"]."',notaria='".$_POST["notaria"]."' LIMIT 1";
	execsql($sql);
	echo"<script>alert('Par�metros grabados...'); window.location.href='../setup_param.php';</script>";		
}

//Iniciar matriculas seg�n resultados
if ($op == "matriculas")
{
	$ano = $_GET["ano"];
	include_once("iniciar_anolectivo.php");
	iniciar_matriculas($ano);
	echo"<script>alert('Matriculas actualizadas...'); window.location.href='../setup_anoslectedit.php?ano=$ano';</script>";
}

//Iniciar Docentes contratos activos
if ($op == "contratos")
{
	$ano = $_GET["ano"];
	include_once("iniciar_anolectivo.php");
	iniciar_contratos($ano);
	echo"<script>alert('Contratos actualizados...'); window.location.href='../setup_anoslectedit.php?ano=$ano';</script>";
}

//grabar folios de matricula
if ($op == "foliar")
{
	$ano = $_GET["ano"];
	$cursos = "y".$ano."cursos";
	$sql="SELECT nivel FROM niveles order by id_nivel";
	$datniv = viewsql($sql);
	foreach($datniv as $regniv)
	{
		$nivel = $regniv->nivel;
		$folio = 0;
		$sql="SELECT id_alumno, grado, curso FROM $cursos WHERE nivel = '$nivel' order by sign(grado), cast(SUBSTRING_INDEX(grado, '-', 1) as decimal), curso, apellidos, nombres";
		$datmatr=viewsql($sql);
		foreach($datmatr as $regmatr)
		{
			$folio = $folio + 1;
			$id_alumno = $regmatr->id_alumno;
			$grado = $regmatr->grado;
			$curso = $regmatr->curso;
			$sql="UPDATE matriculas SET folio = '$folio' where id_alumno = '$id_alumno' and ano = '$ano' limit 1";
			execsql($sql);
		}
	}
	echo"<script>alert('Folios grabados $ano...'); window.location.href='../setup_anoslectedit.php?ano=$ano';</script>";
}

//cargar registros de periodos (lo hace el proceso de iniciar a�o lectivo)
if ($op == "periodos")
{
	$ano = $_GET["ano"];
	$anoant = $ano - 1;
	$sql="SELECT * FROM periodos WHERE ano = '$anoant' order by notanum";
	$datperi=viewsql($sql);
	foreach($datperi as $regperi)
	{
		$sql= "INSERT INTO periodos (pertipo, pernomb, perplan, periodo, notanum, ano) VALUES ('".$regperi->pertipo."', '".$regperi->pernomb."', '".$regperi->perplan."', '".$regperi->periodo."', '".$regperi->notanum."', '$ano')";
		execsql($sql);
	}
	echo"<script>alert('Periodos cargados $ano...'); window.location.href='../setup_anoslectedit.php?ano=$ano';</script>";
}
//cargar registros de ajustes manuales de pensiones
if ($op == "Ajustes_Pension")
{
        //1. cargar ajustes pensiones
        include_once("datos_pagos.php");
        $ano = $_GET["ano"];
        $pagosaju = "y".$ano."pagosaju";
        $sql = "DELETE FROM $pagosaju WHERE tipo = 'P' AND usucrea = ''";
        execsql($sql);
        $sql="SELECT grado, pension FROM gradosval WHERE ano = '$ano' ";
        $datgrad=viewsql($sql);
        foreach($datgrad as $reggrad)
        {
            $grado = $reggrad->grado;
            $pension = $reggrad->pension;
            $valores = array(); //valores pension	
            $comentarios = array(); //pagos pension	
            //alumnos matriculados del grado
            $sql="SELECT * FROM matriculas WHERE ano = '$ano' AND grado = '$grado' order by id_alumno";
            $datmatr=viewsql($sql);
            foreach($datmatr as $regmat)
            {
                $id_alumno = $regmat->id_alumno; 
                $valores["02"] = $regmat->pension02;
                $valores["03"] = $regmat->pension03;
                $valores["04"] = $regmat->pension04;
                $valores["05"] = $regmat->pension05;
                $valores["06"] = $regmat->pension06;
                $valores["07"] = $regmat->pension07;
                $valores["08"] = $regmat->pension08;
                $valores["09"] = $regmat->pension09;
                $valores["10"] = $regmat->pension10;
                $valores["11"] = $regmat->pension11;
                $comenta["02"] = $regmat->coment02;
                $comenta["03"] = $regmat->coment03;
                $comenta["04"] = $regmat->coment04;
                $comenta["05"] = $regmat->coment05;
                $comenta["06"] = $regmat->coment06;
                $comenta["07"] = $regmat->coment07;
                $comenta["08"] = $regmat->coment08;
                $comenta["09"] = $regmat->coment09;
                $comenta["10"] = $regmat->coment10;
                $comenta["11"] = $regmat->coment11;
                for($i=2; $i<=11 ;$i++)
                {
                    if ($i <= 9)
                    {
                        $mes = "0".$i;
                    }
                    else
                    {
                        $mes = $i;
                    }
                    $valor = $valores[$mes];
                    $coment = trim($comenta[$mes]);
                    //cargar ajustes de pension manuales
                    if (intval($valor) != intval($pension)) //ojo puede ser cero
                    {
                       $sql= "INSERT INTO $pagosaju (id_alumno,tipo,mes,ano,valor,detalles,fchcrea,usucrea) ".
                       "VALUES ('$id_alumno','P','$i','$ano','$valor','$coment','$fecha_hora','SETUP')";
                       execsql($sql);
                    }
                }
                if ($ano <= 2018 and $grado < 0)
                {
                    //reliquidar los saldos de los pagos realizados
                    reliquidar_pensiones($ano,$id_alumno);
                }
            }
        }
        //2. ajustar tabla fallas
        $fallas = "y".$ano."fallas";
        $sql = "ALTER TABLE $fallas ADD `tipo` char( 1 ) COLLATE utf8_spanish_ci NOT NULL DEFAULT 'I' COMMENT 'Inasistencia Retardo Permiso Citacion Autorizacion' AFTER id_categoria";
	execsql($sql);
        $sql = "ALTER TABLE $fallas ADD `concepto` varchar( 250 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Concepto Novedad' AFTER tipo";
	execsql($sql);
	$sql = "ALTER TABLE $fallas ADD `fchfin` date NOT NULL COMMENT 'fecha final permiso' AFTER fecha";
	execsql($sql);
        $sql = "ALTER TABLE $fallas ADD `hcita` char( 5 ) COLLATE utf8_spanish_ci NOT NULL COMMENT 'Hora Citacion' AFTER periodo";
	execsql($sql);
        echo"<script>alert('Ajustes en Pensiones cargados a�o $ano...'); window.location.href='../setup_anoslectedit.php?ano=$ano';</script>";
}

?>