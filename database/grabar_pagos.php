<?php
/************************************************************************************************
* @file        : grabar_pagos.php                                                               *
* @brief       : Grabar pagos matricula, pensiones, transporte                                  *
* @version     : 1.0                                                                            *
* @company     : IDIC                                                                           *
* @author      : Nora Rodriguez 15-feb-2012                                                     *
* @Modificado  : Nora Rodriguez 30-sep-2018                                                     *
************************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
include_once("../webparam.php");
include_once("../libs/fechas.php");
include_once("../libs/image_resize.php");
include_once("database.php");
include_once("datos_pagos.php");
connect();
$op = isset($_POST['op']) ? $_POST['op'] : null ;
if ($op == "")
	$op = isset($_GET['op']) ? $_GET['op'] : null ;
$fecha_hora = date('Y-m-d') . " ". date('H:i:s');

//grabar pago
if ($op == "grabar_pago")
{
	$id_pago = $_POST["id_pago"];
	$id_alumno = $_POST["id_alumno"];
	$ano = $_POST["ano"];
	$tipo = $_POST["tipo"];
        $destino = $_POST["destino"];
        if ($_POST["metodo"]=="R") {
            $destino = 1;
        }
	$porcentaje = $_POST["porcentaje"];
	$mespago = intval($_POST["mespago"]);
	if ($mespago < 10)
		$mespago = "0".$mespago;
	$diapago = intval($_POST["diapago"]);
	if ($diapago < 10)
		$diapago = "0".$diapago;
	$fchpago =  $_POST["anopago"]."-".$mespago."-".$diapago;
	$neto = $_POST["neto"];
	$interes = $_POST["interes"];
	$valor = $_POST["valor"];
	if ($tipo=="P" and $destino==2 and $valor > $interes)
		$valor = $interes;
	if ($_POST["id_banco"] <= 0) //pago en efectivo
	{
		$valor_con = 0;
		$valor_efe = $valor;
		$fchpago = $_POST["fchentrada"];
	}
	else
	{
		$valor_con = $valor;
		$valor_efe = 0;
	}
	$file = "y".$_POST["ano"]."pagos";
	$filedet = "y".$_POST["ano"]."pagosdet";
	if ($id_pago > 0)
	{
		$sql="UPDATE $file SET id_banco='".$_POST["id_banco"]."',fchpago='$fchpago',valor_con='$valor_con',valor_efe='$valor_efe',valor='$valor',neto='$neto',interes='$interes',porcentaje='$porcentaje',fchactual='$fecha_hora',usuactual='".$_SESSION["usulog"]."' WHERE id_pago = '$id_pago' LIMIT 1";
		execsql($sql);
                if ($tipo=="P") {
                reliquidar_pensiones($ano,$id_alumno);}
                else {
                reliquidar_transportes($ano,$id_alumno);}
                disconnect();
                echo"<script> window.location.href='../pagos_edit.php?op=editar&id_pago=$id_pago&id_alumno=$id_alumno&tipo=$tipo&ano=$ano';</script>";
	}
	else
	{
		//crear totales pago
		$sql= "INSERT INTO $file (id_alumno,id_banco,id_entrada,fchentrada,tipo,destino,ano,grado,curso,fchpago,valor_con,valor_efe,valor,neto,interes,porcentaje,fchcrea,usucrea,fchactual,usuactual) ".
		"VALUES ('".$_POST["id_alumno"]."','".$_POST["id_banco"]."','".$_POST["id_entrada"]."','".$_POST["fchentrada"]."','".$_POST["tipo"]."','$destino','".$_POST["ano"]."','".$_POST["grado"]."','".$_POST["curso"]."',".
		"'$fchpago','$valor_con','$valor_efe','$valor','$neto','$interes','$porcentaje','$fecha_hora','".$_SESSION["usulog"]."','$fecha_hora','".$_SESSION["usulog"]."')";
		execsql($sql);
		//traer ID pago		
		$sql="SELECT id_pago FROM $file WHERE id_alumno = '".$_POST["id_alumno"]."' AND tipo = '".$_POST["tipo"]."' and fchcrea = '$fecha_hora' LIMIT 1";
		$datpag = viewsql($sql);
		foreach($datpag as $regpag)
		{$id_pago = $regpag->id_pago;}
		if ($id_pago > 0) {
                    if ($tipo=="P") {
                    reliquidar_pensiones($ano,$id_alumno);}
                    else {
                    reliquidar_transportes($ano,$id_alumno);}
                }
                disconnect();
                echo"<script> window.location.href='../pagos_edit.php?op=editar&id_pago=$id_pago&id_alumno=$id_alumno&tipo=$tipo&ano=$ano';</script>";
	}
}

//borrar pago
if ($op == "borrar_pago")
{
	$ano = $_POST["ano"];
	$id_alumno = $_POST["id_alumno"];
	$id_pago = $_POST["id_pago"];
	$tipo = $_POST["tipo"];
	//borrar pago principal
	$file = "y".$_POST["ano"]."pagos";
	$sql = "delete from $file where id_pago = '$id_pago' limit 1";
	execsql($sql);
	//borrar pago meses
	$file = "y".$_POST["ano"]."pagosdet";
	$sql = "delete from $file where id_pago = '$id_pago'";
	execsql($sql);
        if ($tipo=="P") {
        reliquidar_pensiones($ano,$id_alumno);}
        else {
        reliquidar_transportes($ano,$id_alumno);}
	disconnect();
	echo"<script>window.opener.document.location.reload(); window.close();</script>";
}

//grabar ajuste intereses
if ($op == "grabar_ajuste")
{
	$ano = $_POST["ano"];
	$id_alumno = $_POST["id_alumno"];
	$id_ajuste = 0;
	$file = "y".$_POST["ano"]."pagosaju";
	$sql="SELECT id_ajuste FROM $file WHERE id_alumno = '".$_POST["id_alumno"]."' AND mes = '".$_POST["mes"]."' AND tipo = '".$_POST["tipo"]."' LIMIT 1";
	$dataju = viewsql($sql);
	foreach($dataju as $regaju)
	{$id_ajuste = $regaju->id_ajuste;}
	if ($id_ajuste > 0)
	{
		$sql="UPDATE $file SET valor='".$_POST["valor"]."', detalles='".strtoupper($_POST["detalles"])."', fchactual='$fecha_hora', usuactual='".$_SESSION["usulog"]."' WHERE id_ajuste = '$id_ajuste' LIMIT 1";
		execsql($sql);
	}
	else
	{
		$sql= "INSERT INTO $file (id_alumno,tipo,mes,ano,valor,detalles,fchcrea,usucrea,fchactual,usuactual) ".
		"VALUES ('".$_POST["id_alumno"]."','".$_POST["tipo"]."','".$_POST["mes"]."','".$_POST["ano"]."','".$_POST["valor"]."','".strtoupper($_POST["detalles"])."',".
		"'$fecha_hora','".$_SESSION["usulog"]."','$fecha_hora','".$_SESSION["usulog"]."')";
		execsql($sql);
	}
	reliquidar_pensiones($ano,$id_alumno);
	disconnect();
 	echo"<script> window.location.href='../pagos_ajuste.php?id_alumno=".$_POST["id_alumno"]."&id_pago=".$_POST["id_pago"]."&id_entrada=".$_POST["id_entrada"]."&fchentrada=".$_POST["fchentrada"]."&tipo=".$_POST["tipo"]."&ano=".$_POST["ano"]."';</script>";
 }

//borrar ajuste
if ($op == "borrar_ajuste")
{
	$ano = $_POST["ano"];
	$id_alumno = $_POST["id_alumno"];
	$id_ajuste = $_POST["id_ajuste"];
        $tipo = $_POST["tipo"];
	$file = "y".$ano."pagosaju";
	$sql = "delete from $file where id_ajuste = '$id_ajuste' limit 1";
	execsql($sql);
	reliquidar_pensiones($ano,$id_alumno);
	disconnect();
	echo"<script> window.location.href='../pagos_ajuste.php?id_alumno=$id_alumno&ano=$ano&tipo=$tipo';</script>";
}

if ($op == "grabar_entrada")
{
	$id_entrada = $_POST["id_entrada"];
	$mesent = intval($_POST["mesent"]);
	if ($mesent < 10)
		$mesent = "0".$mesent;
	$diaent = intval($_POST["diaent"]);
	if ($diaent < 10)
		$diaent = "0".$diaent;
	$fchentrada =  $_POST["anoent"]."-".$mesent."-".$diaent;
	if ($_POST["tipo"]=="T")
		$file = "y".$_POST["ano"]."pagostra";
	else
		$file = "y".$_POST["ano"]."pagosent";
	if ($id_entrada > 0)
	{
		$sql="UPDATE $file SET fchentrada='$fchentrada',estado='".$_POST["estado"]."' WHERE id_entrada = '$id_entrada' LIMIT 1";
		execsql($sql);
		if ($_POST["oldfchentrada"] != $fchentrada)
		{
			$file = "y".$_POST["ano"]."pagos";		
			$sql="UPDATE $file SET fchentrada='$fchentrada' WHERE id_entrada = '$id_entrada' AND tipo = '".$_POST["tipo"]."'";
			execsql($sql);
		}
		disconnect();
		echo"<script> window.location.href='../pagos_entradas.php?tipo=".$_POST["tipo"]."&ano=".$_POST["ano"]."';</script>";
	}
	else
	{
		$sql= "INSERT INTO $file (fchentrada,estado) VALUES ('$fchentrada','".$_POST["estado"]."')";
		execsql($sql);
                $sql = "select id_entrada from $file where fchentrada = '$fchentrada' order by id_entrada";
                $datent = viewsql($sql);
                foreach ($datent as $regent) {
                    $id_entrada = $regent->id_entrada;
                }
		disconnect();
		echo"<script>alert('CREADA LA ENTRADA $id_entrada FECHA $fchentrada'); window.opener.document.location.reload(); window.location.href='../pagos_entradas.php?tipo=".$_POST["tipo"]."&ano=".$_POST["ano"]."';</script>";
	}
}

if ($op == "seleccionar")
{
	echo"<script>parent.window.location.href='../pagos_edit.php?op=editar&id_alumno=".$_GET["id_alumno"]."&ano=".$_GET["ano"]."&id_entrada=".$_GET["id_entrada"]."&fchentrada=".$_GET["fchentrada"]."&tipo=".$_GET["tipo"]."';</script>";
}

?>