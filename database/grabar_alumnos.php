<?php

/* * **********************************************************************************************
 * @file        : grabar_alumno.php                                                              *
 * @brief       : Graba nuevo alumno o actualiza los datos                                       *
 * @version     : 1.0                                                                            *
 * @company     : IDIC                                                                           *
 * @author      : Nora Rodriguez 15-feb-2012                                                     *
 * @Modificado  : Nora Rodriguez 06-nov-2018                                                     *
 * ********************************************************************************************** */
session_start();
if ($_SESSION["usulog"] == "") {
    echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";
}
include("../webparam.php");
include("../libs/fechas.php");
include("../libs/image_resize.php");
include("database.php");
include_once("datos_notas.php");
connect();
$op = isset($_POST['op']) ? $_POST['op'] : null;
if ($op == "")
    $op = isset($_GET['op']) ? $_GET['op'] : null;
$fecha_hora = date('Y-m-d') . " " . date('H:i:s');

//grabar datos basicos del alumno
if ($op == "grabaralumno") {
    //verificar numero de identificacion sea inexistente
    $id_alumno = $_POST["id_alumno"];
    $id_familiar = intval($_POST["id_familiar"]);
    $docunum = $_POST["docunum"];
    $docunum = str_replace('.', '', $docunum);
    $docunum = str_replace(',', '', $docunum);
    $docunum = str_replace('-', '', $docunum);
    $error = "";
    $sql = "select docunum from alumnos where docunum = '$docunum' and id_alumno != '$id_alumno' limit 1";
    $datalum = viewsql($sql);
    if (count($datalum) > 0)
        $error = "N�mero de identificaci�n duplicado...";
    if ($error == "" and intval($_POST['codigo']) > 0) {
        $sql = "select codigo from alumnos where codigo = '" . intval($_POST['codigo']) . "' and id_alumno != '$id_alumno' limit 1";
        $datalum = viewsql($sql);
        if (count($datalum) > 0)
            $error = "N�mero de c�digo duplicado...";
    }
    if ($error == "") {
        $oldapellidos = "";
        $oldnombres = "";
        $oldtipoedu = "";
        $oldestado = "";
        $oldfchretiro = "";
        if ($id_alumno > 0) {
            $sql = "select apellidos, nombres, tipoedu, estado, fchretiro from alumnos where id_alumno = '$id_alumno' limit 1";
            $datalum = viewsql($sql);
            foreach ($datalum as $regalum) {
                $oldapellidos = $regalum->apellidos;
                $oldnombres = $regalum->nombres;
                $oldtipoedu = $regalum->tipoedu;
                $oldestado = $regalum->estado;
                $oldfchretiro = $regalum->fchretiro;
            }
        }
        $fchnace = "";
        $fchingreso = "";
        $fchretiro = "";
        $fchgrado = "";
        if ($_POST["anonac"] > 0 and $_POST["mesnac"] > 0 and $_POST["dianac"] > 0)
            $fchnace = $_POST["anonac"] . "-" . $_POST["mesnac"] . "-" . $_POST["dianac"];
        if ($_POST["anoing"] > 0 and $_POST["mesing"] > 0 and $_POST["diaing"] > 0)
            $fchingreso = $_POST["anoing"] . "-" . $_POST["mesing"] . "-" . $_POST["diaing"];
        if ($_POST["anoret"] > 0 and $_POST["mesret"] > 0 and $_POST["diaret"] > 0 and $_POST["estado"] == "RETIRADO")
            $fchretiro = $_POST["anoret"] . "-" . $_POST["mesret"] . "-" . $_POST["diaret"];
        if ($_POST["anogra"] > 0 and $_POST["mesgra"] > 0 and $_POST["diagra"] > 0 and $_POST["estado"] == "GRADUADO")
            $fchgrado = $_POST["anogra"] . "-" . $_POST["mesgra"] . "-" . $_POST["diagra"];
        $fname = basename($_FILES['nwfoto']['name']);
        if ($id_alumno > 0) {
            $sql = "UPDATE alumnos SET apellidos='" . strtoupper($_POST["apellidos"]) . "', nombres='" . strtoupper($_POST["nombres"]) . "', docutip='" . $_POST["docutip"] . "', docunum='$docunum', codigo='" . $_POST["codigo"] . "', " .
                    "docuexp='" . strtoupper($_POST["docuexp"]) . "', docudep='" . strtoupper($_POST["docudep"]) . "', genero='" . $_POST["genero"] . "', estado='" . $_POST["estado"] . "', tipoedu='" . $_POST["tipoedu"] . "', fchnace='$fchnace', lugnace='" . strtoupper($_POST["lugnace"]) . "', " .
                    "depnace='" . strtoupper($_POST["depnace"]) . "', paisnace='" . strtoupper($_POST["paisnace"]) . "', correo='" . $_POST["correo"] . "', celular='" . $_POST["celular"] . "', fchingreso='$fchingreso', procegrado='" . $_POST["procegrado"] . "', " .
                    "proceinsti='" . strtoupper($_POST["proceinsti"]) . "', id_saludenti='" . $_POST["id_saludenti"] . "', fchretiro='$fchretiro', detalles='" . $_POST["detalles"] . "', " .
                    "id_familiar='$id_familiar', fchactual='$fecha_hora', usuactual = '" . $_SESSION["usulog"] . "' " .
                    "WHERE id_alumno = '$id_alumno' limit 1";
            execsql($sql);
            //actualizar curso si cambio de apellidos o de nombres
            //fecha de retiro en matricula y curso debe hacerse desde la matricula
            if (strtoupper($_POST["apellidos"]) != $oldapellidos or strtoupper($_POST["nombres"]) != $oldnombres or $fchretiro != $oldfchretiro) {
                $sql = "SELECT ano FROM anoslect ORDER BY ano";
                $datanol = viewsql($sql);
                foreach ($datanol as $reganol) {
                    $cursos = "y" . $reganol->ano . "cursos";
                    $sql = "UPDATE $cursos SET apellidos = '" . strtoupper($_POST["apellidos"]) . "', nombres = '" . strtoupper($_POST["nombres"]) . "'";
                    if ($fchretiro != $oldfchretiro and substr($fchretiro,0,4)==$reganol->ano)
                        $sql.=", fchretiro = '$fchretiro'";
                    $sql.=" where id_alumno='$id_alumno' limit 1";
                    execsql($sql);
                }
            }
            
        } else {
            $sql = "INSERT INTO alumnos (apellidos, nombres, docunum, docutip, docuexp, docudep, estado, codigo, fchnace, lugnace, depnace, paisnace, genero, correo, celular, tipoedu, fchingreso, fchretiro, id_saludenti, procegrado, proceinsti, detalles, fchcrea, usucrea) " .
                    "VALUES ('" . strtoupper($_POST["apellidos"]) . "', '" . strtoupper($_POST["nombres"]) . "', '$docunum', '" . $_POST["docutip"] . "', '" . strtoupper($_POST["docuexp"]) . "', '" . strtoupper($_POST["docudep"]) . "', '" . $_POST["estado"] . "', '" . $_POST["codigo"] . "', " .
                    "'$fchnace', '" . strtoupper($_POST["lugnace"]) . "', '" . strtoupper($_POST["depnace"]) . "', '" . strtoupper($_POST["paisnace"]) . "', '" . $_POST["genero"] . "', '" . $_POST["correo"] . "', '" . $_POST["celular"] . "', '" . $_POST["tipoedu"] . "', " .
                    "'$fchingreso','$fchretiro', '" . $_POST["id_saludenti"] . "', '" . $_POST["procegrado"] . "', '" . strtoupper($_POST["proceinsti"]) . "', " .
                    "'" . $_POST["detalles"] . "','$fecha_hora', '" . $_SESSION["usulog"] . "')";
            execsql($sql);
            $sql = "select id_alumno from alumnos where docunum = '$docunum' limit 1";
            $datalum = viewsql($sql);
            foreach ($datalum as $regalum) {
                $id_alumno = $regalum->id_alumno;
            }
        }
        //grabar foto
        if ($fname != "" and $id_alumno > 0) {
            $len = strlen($fname);
            $ext = substr($fname, $len - 3, 3);
            //borrar archivo foto anterior
            $oldfoto = ROOT_DIR . "/" . $_POST["oldfoto"];
            if (file_exists($oldfoto))
            {@unlink($oldfoto);}
             //cargar nueva foto con id alumno y fecha
            $nwfecha = str_replace ( ":" , "" , $fecha_hora );
            $nwfecha = str_replace ( "-" , "" , $nwfecha );
            $nwfecha = str_replace ( " " , "" , $nwfecha );
            $foto = "fotoalu" . $id_alumno."-".$nwfecha;
            $foto = "fotos/" . $foto . "." . $ext;
            $rutafoto = ROOT_DIR . "/" . $foto;
            ini_set('memory_limit', '200M');
            ini_set('upload_max_filesize', '200M');
            if (move_uploaded_file($_FILES['nwfoto']['tmp_name'], $rutafoto)) {
		if (strtoupper($ext)=="GIF" or strtoupper($ext)=="JPG" or strtoupper($ext)=="PNG") 
                {resize_image($rutafoto,150,150);}
            } else {
                $foto = "";
            }
            $sql = "update alumnos set foto='$foto' where id_alumno='$id_alumno' limit 1";
            execsql($sql);
        }
        //grabar familiar
        if ($id_familiar > 0) {
            $sql = "UPDATE familiares SET apellidos='" . strtoupper($_POST["famiape"]) . "', nombres='" . strtoupper($_POST["faminom"]) . "', docutip='" . $_POST["famidoc"] . "', docunum='" . $_POST["famiced"] . "', docuexp='" . strtoupper($_POST["famiexp"]) . "', pais='" . strtoupper($_POST["famipai"]) . "', " .
                    "direccion='" . strtoupper($_POST["famidir"]) . "', barrio='" . strtoupper($_POST["famibar"]) . "', estrato='" . $_POST["famiest"] . "', telefono='" . $_POST["famitel"] . "', " .
                    "telefofi='" . $_POST["famiofi"] . "', celular='" . $_POST["famicel"] . "', correo='" . $_POST["famicor"] . "', profesion='" . strtoupper($_POST["famipro"]) . "', " .
                    "fchactual='$fecha_hora', usuactual = '" . $_SESSION["usulog"] . "' " .
                    "WHERE id_familiar = '$id_familiar' limit 1";
            execsql($sql);
        } else {
            $sql = "INSERT INTO familiares (id_alumno, tipo, apellidos, nombres, docunum, docutip, docuexp, pais, direccion, barrio, estrato, telefono, telefofi, celular, correo, profesion, fchcrea, usucrea) " .
                    "VALUES ('$id_alumno', '" . $_POST["famitip"] . "', '" . strtoupper($_POST["famiape"]) . "', '" . strtoupper($_POST["faminom"]) . "', '" . $_POST["famiced"] . "', '" . $_POST["famidoc"] . "', '" . strtoupper($_POST["famiexp"]) . "', '" . strtoupper($_POST["famipai"]) . "', '" . strtoupper($_POST["famidir"]) . "', " .
                    "'" . strtoupper($_POST["famibar"]) . "', '" . $_POST["famiest"] . "', '" . $_POST["famitel"] . "', '" . $_POST["famiofi"] . "', '" . $_POST["famicel"] . "', '" . $_POST["famicor"] . "', '" . strtoupper($_POST["famipro"]) . "', " .
                    "'$fecha_hora', '" . $_SESSION["usulog"] . "')";
            execsql($sql);
            $sql = "select id_familiar from familiares where tipo = '" . $_POST["famitip"] . "' and id_alumno = '$id_alumno' limit 1";
            $datfami = viewsql($sql);
            foreach ($datfami as $regfami) {
                $id_familiar = $regfami->id_familiar;
            }
            $codigo = intval($_POST['codigo']);
            if ($codigo <= 0)
                $codigo = $id_alumno;
            $sql = "UPDATE alumnos set id_familiar = '$id_familiar', codigo = '$codigo' where id_alumno='$id_alumno' limit 1";
            execsql($sql);
        }
        //grabar matricula
        $id_matricula = intval($_POST["id_matricula"]);
        $ano = $_POST["ano"];
        $grado = $_POST["grado"];
        $nivel = substr($grado, 0, 1);
        $grado = substr($grado, 1);
        $matestado = "PENDIENTE";
        $matcurso = "";
        if ($id_matricula <= 0) {
            $sql = "select id_matricula from matriculas where id_alumno = '$id_alumno' order by ano desc limit 1";
            $datmatr = viewsql($sql);
            foreach ($datmatr as $regmatr) {
                $id_matricula = $regmatr->id_matricula;
            }
            if ($id_matricula <= 0) {
                $gradovalmat = 0;
                $gradovalpen = 0;
                $sql = "SELECT matricula, pension FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
                $datval = viewsql($sql);
                foreach ($datval as $regval) {
                    $gradovalmat = $regval->matricula;
                    $gradovalpen = $regval->pension;
                }
                $sql = "INSERT INTO matriculas (id_alumno,ano,nivel,grado,estado,renovar,fecha,matricula,fchcrea,usucrea,fchactual,usuactual) VALUES ('$id_alumno','$ano','$nivel','$grado','PENDIENTE','1','$fecha_hora','$gradovalmat','$fecha_hora','" . $_SESSION["usulog"] . "','$fecha_hora','" . $_SESSION["usulog"] . "')";
                execsql($sql);
                $sql = "select id_matricula from matriculas where id_alumno='$id_alumno' limit 1";
                $datmatr = viewsql($sql);
                foreach ($datmatr as $regmatr) {
                    $id_matricula = $regmatr->id_matricula;
                }
            }
            $sql = "UPDATE alumnos SET id_matricula = '$id_matricula' where id_alumno='$id_alumno' limit 1";
            execsql($sql);
        } else {
            $sql = "select estado, curso from matriculas where id_matricula = '$id_matricula' limit 1";
            $datmatr = viewsql($sql);
            foreach ($datmatr as $regmatr) {
                $matestado = $regmatr->estado;
                $matcurso = $regmatr->curso;
            }
        }
        //reasignar curso y retotalizar si matricula cerrada y cambio de grado o curso o tipo de educaci�n
        if ($_POST["ano"] == $_SESSION["anoaca"] and $_POST["curso"] != "" and $matestado == "CERRADA" and ( $_POST["curso"] != $matcurso or $_POST["tipoedu"] != $oldtipoedu)) {
            $sql = "UPDATE matriculas SET curso = '" . $_POST["curso"] . "' where id_matricula='$id_matricula' limit 1";
            execsql($sql);
            actualizar_curso($_POST["ano"], $nivel, $grado, $_POST["curso"], $id_alumno, $matcurso);
        }
        disconnect();
        echo"<script> alert('Alumno Grabado.'); window.location.href='../alumnos_edit.php?id_alumno=$id_alumno';</script>";
    } else {
        disconnect();
        echo"<script> alert('Error! $error'); window.location.href='../alumnos_edit.php?id_alumno=$id_alumno';</script>";
    }
}

//borrar alumno del archivo
if ($op == "borraralumno") {
    $id_alumno = $_POST["id_alumno"];
    $error = 0;
    //verificar primero que no tenga notas en ningun a�o
    $sql = "SELECT ano FROM anoslect ORDER BY ano desc";
    $datanol = viewsql($sql);
    foreach ($datanol as $reganol) {
        $ano = $reganol->ano;
        $file = "y" . $ano . "notas";
        $sql = "SELECT nota FROM $file WHERE id_alumno = '$id_alumno' limit 1";
        $datnota = viewsql($sql);
        if (count($datnota) > 0)
            $error = $error + 1;
    }
    if ($error == 0) {
        $sql = "delete from alumnos where id_alumno = '$id_alumno' limit 1";
        execsql($sql);
        $foto = ROOT_DIR . "/" . $_POST["oldfoto"];
        if (file_exists($foto)) {
            @unlink(foto);
        }
        $sql = "delete from matriculas where id_alumno = '$id_alumno'";
        execsql($sql);
        $sql = "delete from transporte where id_alumno = '$id_alumno'";
        execsql($sql);
        $sql = "delete from familiares where id_alumno = '$id_alumno'";
        execsql($sql);
        //borrarlo de los cursos
        foreach ($datanol as $reganol) {
            $ano = $reganol->ano;
            $cursos = "y" . $ano . "cursos";
            $sql = "delete from $cursos where id_alumno = '$id_alumno' limit 1";
            execsql($sql);
            //totalizar_curso($ano, $grado , $curso);
        }
        disconnect();
        echo"<script>alert('Alumno Eliminado...');window.opener.document.location.reload();window.close();</script>";
    } else {
        disconnect();
        echo"<script>alert('Imposible eliminar Alumno, Matriculas con NOTAS!'); window.opener.document.location.reload();window.close();</script>";
    }
}

//borrar foto del alumno
if ($op == "borrarfoto") {
    $id_alumno = $_GET["id_alumno"];
    $foto = ROOT_DIR . "/" . $_GET["foto"];
    if (file_exists($foto)) {
        @unlink($foto);
    }
    $sql = "update alumnos set foto='' where id_alumno='$id_alumno' limit 1";
    execsql($sql);
    disconnect();
    echo"<script>window.location.href='../alumnos_edit.php?id_alumno=$id_alumno';</script>";
}

//grabar datos de matricula
if ($op == "grabar_matricula") {
    $ano = $_POST["ano"];
    $id_alumno = $_POST["id_alumno"];
    $estado = $_POST["estado"];
    $fecha = fec_amd($_POST["fecha"]);
    $fchretiro = fec_amd($_POST["fchretiro"]);
    $pos = strpos($_POST["gradocurso"], "*");
    $grado = substr($_POST["gradocurso"], 0, $pos);
    $curso = substr($_POST["gradocurso"], $pos + 1);
    $nivel = "";
    $sql = "select nivel from grados where grado = '$grado' limit 1";
    $datgrad = viewsql($sql);
    foreach ($datgrad as $reggrad) {
        $nivel = $reggrad->nivel;
    }
    //primer y ultimo a�o lectivo existentes en el sistema
    $primer_ano = 0;
    $ultimo_ano = 0;
    $sql = "SELECT ano FROM anoslect ORDER BY ano";
    $datanol = viewsql($sql);
    foreach ($datanol as $reganol) {
        if ($primer_ano == 0)
            $primer_ano = $reganol->ano;
        $ultimo_ano = $reganol->ano;
    }
    //verificar matricula no repetida para un mismo alumno y a�o
    $id_matricula = 0;
    $oldestado = "PENDIENTE";
    $sql = "select id_matricula, estado from matriculas where id_alumno = '$id_alumno' and ano = '" . $_POST["ano"] . "' limit 1";
    $datmatr = viewsql($sql);
    foreach ($datmatr as $regmatr) {
        $id_matricula = $regmatr->id_matricula;
        $oldestado = $regmatr->estado;
    }
    //if ($curso == "" and $oldestado!="")
    //	$estado = $oldestado;
    if ($id_matricula <= 0) 
    {
        $sql = "INSERT INTO matriculas (id_alumno,ano,nivel,grado,curso,estado,fecha,folio,id_respmatri,id_resppagos,beca,matricula,interes,detalles,fchcrea,usucrea) " .
                "VALUES ('$id_alumno','" . $_POST["ano"] . "','$nivel','$grado','$curso','$estado','$fecha','" . $_POST["folio"] . "','" . $_POST["id_respmatri"] . "','" . $_POST["id_resppagos"] . "','" . $_POST["beca"] . "','" . $_POST["matricula"] . "','" . $_POST["interes"] .
                "','" . $_POST["detalles"] . "','$fecha_hora','" . $_SESSION["usulog"] . "')";
        execsql($sql);
    } else {
        $renovar = $_POST["renovar"];
        if ($estado == "RETIRADA")
            $renovar = 0;
        if ($fchretiro == "")
            $fchretiro = "0000-00-00";
        $sql = "UPDATE matriculas SET nivel='$nivel',grado='$grado',curso='$curso',estado='$estado',renovar='$renovar',fecha='$fecha',fchretiro='$fchretiro',folio='" . $_POST["folio"] . "',id_respmatri='" . $_POST["id_respmatri"] . "',id_resppagos='" . $_POST["id_resppagos"] . "',";
        if ($_SESSION["nivpag"] >= 4) {
            $sql.= "beca='" . $_POST["beca"] . "',matricula='" . $_POST["matricula"] . "',interes='" . $_POST["interes"] . "',"; 
        }
        $sql.= "detalles='" . $_POST["detalles"] . "',fchactual='$fecha_hora',usuactual='" . $_SESSION["usulog"] . "' WHERE id_matricula='$id_matricula' LIMIT 1";
        execsql($sql);
    }
    //Ajustes de pensiones
    $pagosaju = "y".$ano."pagosaju";
    $totpagos = 0;
    for($i=2; $i<=11 ;$i++)
    {
        if ($i <= 9) {
            $mes = "0".$i;
        }
        else {
            $mes = $i;
        }
        $valor = $_POST["pension".$mes];
        $coment = strtoupper($_POST["coment".$mes]);
        $id_ajuste = 0;
        $sql = "select id_ajuste from $pagosaju where id_alumno = '$id_alumno' and tipo = 'P' and mes = '$i' limit 1";
        $dataju = viewsql($sql);
        foreach ($dataju as $regaju) {
            $id_ajuste = $regaju->id_ajuste;
        }
        if ($id_ajuste <= 0 and $valor!="" and $coment!="")
        {
            $sql= "INSERT INTO $pagosaju (id_alumno,tipo,mes,ano,valor,detalles,fchcrea,usucrea) ".
            "VALUES ('$id_alumno','P','$i','$ano','$valor','$coment','$fecha_hora','".$_SESSION["usulog"]."')";
            execsql($sql);
        }
        if ($id_ajuste > 0) {
            if ($valor!="" and $coment!="") {
                $sql = "UPDATE $pagosaju SET valor='$valor',detalles='$coment',fchactual='$fecha_hora',usuactual='" . $_SESSION["usulog"] . "' WHERE id_ajuste ='$id_ajuste' LIMIT 1";
                execsql($sql);
            } else {
                $sql = "DELETE FROM $pagosaju WHERE id_ajuste ='$id_ajuste' LIMIT 1";
                execsql($sql);
            }
        }
        if ($valor!="" and $coment!="") {
          $totpagos = $totpagos + intval($valor);  
        } else {
          $totpagos = $totpagos + intval($_POST["gradovalpen"]);  
        }
    }
    /*
    if ($totpagos != intval($_POST["oldtotpagos"])) {
        include_once("datos_pagos.php");
        $ano = $_POST["ano"];
        reliquidar_saldos($ano, $id_alumno, 'P');
    }
    */

    //actualizar curso, areas pendientes y perdidas
    if ($curso != "") 
    {
        $cursos = "y" . $_POST["ano"] . "cursos";
        $anoant = intval($_POST["ano"]) - 1;
        $anosig = intval($_POST["ano"]) + 1;
        $id_areadeb = $_POST["id_areadeb"];
        $id_areaper = $_POST["id_areaper"];
        $pierde = $_POST["pierde"];
        //traer area perdida a�o anterior
        if ($anoant >= $primer_ano) {
            $cursoant = "y" . $anoant . "cursos";
            $sql = "select id_areaper from $cursoant where id_alumno='$id_alumno' limit 1";
            $datcurs = viewsql($sql);
            foreach ($datcurs as $regcurs) {
                $id_areadeb = $regcurs->id_areaper;
            }
        }
//echo "curso $curso estado $estado $fchretiro<br>";		
        if ($estado == "CERRADA") {
            //reasignar curso si nueva matricula o cambio de curso
            actualizar_curso($_POST["ano"], $nivel, $grado, $curso, $id_alumno, $_POST["oldcurso"]);
            //inactivar periodos
            $inactivar = "";
            $sql = "SELECT id_periodo, periodo FROM periodos WHERE ano = '" . $_POST["ano"] . "' and pertipo='P' order by notanum";
            $datperi = viewsql($sql);
            foreach ($datperi as $regperi) {
                $periodo = $regperi->periodo;
                $chkname = "chkp" . $periodo;
                $chkper = isset($_POST[$chkname]) ? $_POST[$chkname] : null;
                if ($chkper == "on")
                    $inactivar = $inactivar . " " . $periodo;
            }
            //actualizar campos curso a�o actual
            $sql = "update $cursos set id_areadeb='$id_areadeb',id_areaper='$id_areaper',fchretiro='0000-00-00',pierde='$pierde' where id_alumno='$id_alumno' limit 1";
            execsql($sql);
            if ($_POST["id_areaper"] > 0 and $_POST["ano"] < $ultimo_ano) {
                $cursosig = "y" . $anosig . "cursos";
                $sql = "update $cursosig set id_areadeb='$id_areaper' where id_alumno='$id_alumno' limit 1";
                execsql($sql);
            }
            if ($inactivar != $_POST["oldinactivar"]) {
                include_once("grabar_notas.php");
                $sql = "update $cursos set inactivar = '$inactivar' where id_alumno='$id_alumno' limit 1";
                execsql($sql);
                //traer asignaturas del grado
                $sql = "SELECT id_asignatura FROM asignaturas WHERE ano = '" . $_POST["ano"] . "' AND grado = '$grado' AND notas='1' ORDER BY id_asignatura";
                $datasig = viewsql($sql);
                //inactivar asignaturas periodos
                foreach ($datperi as $regperi) {
                    $periodo = $regperi->periodo;
                    foreach ($datasig as $regasig) {
                        $id_asignatura = $regasig->id_asignatura;
                        if (strpos($inactivar, $periodo) > 0)
                            grabar_notaasig($_POST["ano"], $id_alumno, $id_asignatura, 'P', $periodo, '0', '0', '1');
                        else {
                            if (strpos($_POST["oldinactivar"], $periodo) > 0) //activar nuevamente si estaba inactivo
                                grabar_notaasig($_POST["ano"], $id_alumno, $id_asignatura, 'P', $periodo, '0', '0', '0');
                        }
                    }
                }
            }
        }
        else {
            if ($estado == "RETIRADA" and $fchretiro != "" and $fchretiro != "0000-00-00") {
                $sql = "UPDATE alumnos SET estado = 'RETIRADO',fchretiro = '$fchretiro' WHERE id_alumno='$id_alumno' limit 1";
                execsql($sql);
                //if (strtotime($fchretiro) >= strtotime($_POST["fchfinal"])) //Si termina a�o escolar no grabar fecha retiro en el curso
                // $fchretiro = "0000-00-00";
            }
            //actualizar campos curso a�o actual
            $sql = "update $cursos set id_areadeb='$id_areadeb',id_areaper='$id_areaper',fchretiro='$fchretiro',pierde='$pierde' where id_alumno='$id_alumno' limit 1";
            execsql($sql);
        }
    }
    echo"<script>alert('Matricula actualizada');window.location.href='../alumnos_matriculas.php?id_alumno=$id_alumno';</script>";
}

//borrar matricula
if ($op == "borrarmatricula") {
    $id_matricula = $_POST["id_matricula"];
    $id_alumno = $_POST["id_alumno"];
    $file = "y" . $_POST["ano"] . "notas";
    $error = "";
    $sql = "SELECT id_matricula FROM alumnos WHERE id_alumno = '$id_alumno' ORDER BY ano desc LIMIT 1";
    $datalum = viewsql($sql);
    foreach ($datalum as $regalum) {
        if ($regalum->id_matricula == $id_matricula)
            $error = "Ultima matr�cula asociada al alumno no puede ser eliminada. Para eliminarla debe borrar al alumno.";
    }
    if ($error == "") {
        $sql = "SELECT * FROM $file WHERE id_alumno = '$id_alumno' limit 1";
        $datnota = viewsql($sql);
        if (count($datnota) == 0) {
            $sql = "delete from matriculas where id_matricula = '$id_matricula' limit 1";
            execsql($sql);
            $file = "y" . $_POST["ano"] . "cursos";
            $sql = "delete from $file where id_alumno = '$id_alumno' limit 1";
            execsql($sql);
        } else {
            $error = "Imposible eliminar Matricula, tiene NOTAS para ese a�o!";
        }
    }
    disconnect();
    if ($error == "")
        echo"<script>window.location.href='../alumnos_matriculas.php?id_alumno=$id_alumno';</script>";
    else
        echo"<script>alert('Advertencia! $error'); window.location.href='../alumnos_matriculas.php?id_alumno=$id_alumno';</script>";
}

//grabar familiar de alumno
if ($op == "grabarfamiliar") {
    $id_alumno = $_POST["id_alumno"];
    $id_familiar = $_POST["id_familiar"];
    $fname = $_FILES['nwfoto']['name'];
    $error = "";
    $numfami = 0;
    $sql = "select tipo from familiares where id_alumno = '$id_alumno' and id_familiar != '$id_familiar' ";
    $datfami = viewsql($sql);
    foreach ($datfami as $regfami) {
        $numfami = $numfami + 1;
        if ($regfami->tipo == $_POST["famitip"])
            $error = "Tipo de familiar ya existente para el alumno....";
    }
    //verificar cedula familiar no repetida
    $sql = "select id_familiar from familiares where id_alumno='$id_alumno' AND docunum = '" . $_POST["famiced"] . "' and id_familiar != '$id_familiar' limit 1";
    $datfami = viewsql($sql);
    if (count($datfami) > 0)
        $error = "N�mero de documento ya existente para otro familiar del alumno....";
    //verificar tipo de familiar no este duplicado para el alumno
    if ($error == "") {
        if ($id_familiar > 0) {
            $sql = "UPDATE familiares SET apellidos='" . strtoupper($_POST["famiape"]) . "', nombres='" . strtoupper($_POST["faminom"]) . "', docutip='" . $_POST["famidoc"] . "', docunum='" . $_POST["famiced"] . "', docuexp='" . strtoupper($_POST["famiexp"]) . "', pais='" . strtoupper($_POST["famipai"]) . "', " .
                    "direccion='" . strtoupper($_POST["famidir"]) . "', barrio='" . strtoupper($_POST["famibar"]) . "', ciudad='" . strtoupper($_POST["famiciu"]) . "', estrato='" . $_POST["famiest"] . "', telefono='" . $_POST["famitel"] . "', " .
                    "telefofi='" . $_POST["famiofi"] . "', celular='" . $_POST["famicel"] . "', correo='" . $_POST["famicor"] . "', profesion='" . strtoupper($_POST["famipro"]) . "', detalles='" . $_POST["famidet"] . "', " .
                    "fchactual='$fecha_hora', usuactual = '" . $_SESSION["usulog"] . "' " .
                    "WHERE id_familiar = '$id_familiar' limit 1";
            execsql($sql);
        } else {
            $sql = "INSERT INTO familiares (id_alumno, tipo, apellidos, nombres, docunum, docutip, docuexp, pais, direccion, barrio, ciudad, estrato, telefono, telefofi, celular, correo, profesion, detalles, fchcrea, usucrea) " .
                    "VALUES ('$id_alumno', '" . $_POST["famitip"] . "', '" . strtoupper($_POST["famiape"]) . "', '" . strtoupper($_POST["faminom"]) . "', '" . $_POST["famiced"] . "', '" . $_POST["famidoc"] . "', '" . strtoupper($_POST["famiexp"]) . "', '" . strtoupper($_POST["famipai"]) . "', '" . strtoupper($_POST["famidir"]) . "', " .
                    "'" . strtoupper($_POST["famibar"]) . "', '" . strtoupper($_POST["famiciu"]) . "', '" . $_POST["famiest"] . "', '" . $_POST["famitel"] . "', '" . $_POST["famiofi"] . "', '" . $_POST["famicel"] . "', '" . $_POST["famicor"] . "', '" . strtoupper($_POST["famipro"]) . "', '" . $_POST["famidet"] . "', " .
                    "'$fecha_hora', '" . $_SESSION["usulog"] . "')";
            execsql($sql);
            $sql = "select id_familiar from familiares where id_alumno = '$id_alumno' and tipo = '" . $_POST["famitip"] . "' limit 1";
            $datfami = viewsql($sql);
            foreach ($datfami as $regfami) {
                $id_familiar = $regfami->id_familiar;
            }
        }
        //grabar foto familiar
        if ($fname != "" and $id_familiar > 0) {
            $pos = strrpos($fname, "."); //ultima aparicion
            $ext = substr($fname, $pos + 1);
            $foto = "fotos/fotofam" . $id_familiar . "." . $ext;
            if (file_exists($_POST["oldfoto"])) {
                @unlink($_POST["oldfoto"]);
            }
            if (file_exists($foto)) {
                @unlink($foto);
            }
            $rutafoto = ROOT_DIR . "/" . $foto;
            if (move_uploaded_file($_FILES['nwfoto']['tmp_name'], $rutafoto)) {
                if (strtoupper($ext) == "GIF" or strtoupper($ext) == "JPG" or strtoupper($ext) == "PNG")
                    resize_image($rutafoto, 150, 150);
            }
            else {
                $foto = "";
            }
            $sql = "update familiares set foto='$foto' where id_familiar='$id_familiar' limit 1";
            execsql($sql);
        }
    }
    disconnect();
    if ($error == "")
        echo"<script>window.location.href='../alumnos_familiares.php?id_alumno=$id_alumno';</script>";
    else
        echo"<script> alert('Error! $error'); window.location.href='../alumnos_familiares.php?id_alumno=$id_alumno';</script>";
}

//borrar familiar del alumno
if ($op == "borrarfamiliar") {
    $id_alumno = $_POST["id_alumno"];
    $sql = "delete from familiares where id_familiar = '" . $_POST["id_familiar"] . "' limit 1";
    execsql($sql);
    $foto = ROOT_DIR . "/" . $_POST["oldfoto"];
    if (file_exists($foto)) {
        @unlink(foto);
    }
    disconnect();
    echo"<script>window.location.href='../alumnos_familiares.php?id_alumno=$id_alumno';</script>";
}

//borrar foto del familiar
if ($op == "borrarfotofami") {
    $id_alumno = $_GET["id_alumno"];
    $id_familiar = $_GET["id_familiar"];
    $foto = ROOT_DIR . "/" . $_GET["foto"];
    if (file_exists($foto)) {
        @unlink($foto);
    }
    $sql = "update familiares set foto='' where id_familiar='$id_familiar' limit 1";
    execsql($sql);
    disconnect();
    echo"<script>window.location.href='../alumnos_familiares.php?op=editar&id_alumno=$id_alumno&id_familiar=$id_familiar';</script>";
}

//grabar y cargar documento de alumno
if ($op == "grabardocumento") {
    $id_alumno = $_POST["id_alumno"];
    $documento = trim(strtoupper($_POST["documento"]));
    $fname = $_FILES['archivo']['name'];
    $mensaje = "";
    $sql = "select id_documento from documentos where id_alumno = '$id_alumno' and documento = '$documento' limit 1";
    $datdocu = viewsql($sql);
    if (count($datdocu) > 0)
        $mensaje = "Documento con ese nombre ya existente...";
    $id_documento = 0;
    if ($mensaje == "" and $fname != "") {
        $sql = "insert into documentos (id_alumno,documento,fchcrea,usucrea) values ('$id_alumno','$documento','$fecha_hora','" . $_SESSION["usulog"] . "')";
        execsql($sql);
        $sql = "select id_documento from documentos where id_alumno = '$id_alumno' and documento = '$documento' limit 1";
        $datdocu = viewsql($sql);
        foreach ($datdocu as $regdocu) {
            $id_documento = $regdocu->id_documento;
        }
    }
    if ($id_documento > 0) {
        $pos = strrpos($fname, "."); //ultima aparicion
        $ext = substr($fname, $pos + 1);
        $file = "docum/docualu" . $id_alumno . "_" . $id_documento . "." . $ext;
        if (file_exists($file)) {
            @unlink($file);
        }
        $rutafile = ROOT_DIR . "/" . $file;
        if (move_uploaded_file($_FILES['archivo']['tmp_name'], $rutafile)) {
            if (strtoupper($ext) == "GIF" or strtoupper($ext) == "JPG" or strtoupper($ext) == "PNG")
                resize_image($file, 600, 600);
            $sql = "update documentos set archivo = '$file' where id_documento = '$id_documento' limit 1";
            execsql($sql);
        }
        else {
            $sql = "delete from documentos where id_documento = '$id_documento' limit 1";
            execsql($sql);
            $mensaje = "Error al cargar el documento.";
        }
    }
    disconnect();
    if ($mensaje == "")
        echo"<script>window.location.href='../alumnos_docum.php?id_alumno=$id_alumno';</script>";
    else
        echo"<script> alert('$mensaje'); window.location.href='../alumnos_docum.php?id_alumno=$id_alumno';</script>";
}

//borrar y eliminar documento del alumno
if ($op == "borrardocumento") {
    $id_alumno = $_GET["id_alumno"];
    $id_documento = $_GET["id_documento"];
    $archivo = ROOT_DIR . "/" . $_GET["archivo"];
    if (file_exists($archivo)) {
        @unlink($archivo);
    }
    $sql = "delete from documentos where id_documento = '$id_documento' limit 1";
    execsql($sql);
    disconnect();
    echo"<script>window.location.href='../alumnos_docum.php?id_alumno=$id_alumno';</script>";
}

//grabar falla del alumno en una fecha por horas de asignatura
//registro principal con la fecha y categoria, subregistros con la fecha, asignatura y horas
/*
if ($op == "grabarfalla") {
    $file = "y" . $_POST["ano"] . "fallas";
    $fecha = fec_amd($_POST["fecha"]);
    $periodo = $_POST["periodo"];
    $id_alumno = $_POST["id_alumno"];
    $chkdia = isset($_POST["chkdia"]) ? $_POST["chkdia"] : null;
    $page = isset($_POST["page"]) ? $_POST["page"] : null;
    $total = 0;
    if ($chkdia == "on")
        $total = 1;
     //verificar existencia de la falla
    $id_falla = 0;
    $sql = "select id_falla from $file where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura = '0' limit 1";
    $datfall = viewsql($sql);
    foreach ($datfall as $regfall) {
        $id_falla = $regfall->id_falla;
    }
    //actualizar falla principal en la fecha y alumno
    if ($id_falla <= 0)
        $sql = "INSERT INTO $file (id_alumno, id_categoria, fecha, fchjust, periodo, detalles, fchcrea, usucrea) VALUES ('$id_alumno', '" . $_POST["id_categoria"] . "', '$fecha', '" . fec_amd($_POST["fchjust"]) . "', '$periodo', '" . strtoupper($_POST["detalles"]) . "', '$fecha_hora', '" . $_SESSION["usulog"] . "') ";
    else
        $sql = "UPDATE $file SET id_categoria='" . $_POST["id_categoria"] . "', fchjust='" . fec_amd($_POST["fchjust"]) . "', detalles = '" . strtoupper($_POST["detalles"]) . "', fchactual='$fecha_hora', usuactual = '" . $_SESSION["usulog"] . "' where id_falla = '$id_falla' limit 1";
    execsql($sql);
    //contar horas de falla por asignatura
    $num = 0; //contador de diferentes asignaturas del dia
    $asigna = array(); //arreglo de asignaturas
    $nhoras = array(); //arreglo de horas
    $interv = array(); //arreglo de intervalos
    $idasig = "";
    for ($i = 1; $i <= 12; $i++) {
        $asigna[$i] = 0;
        $nhoras[$i] = 0;
        $interv[$i] = "";
    }
    //cargar arreglo de asignaturas y fallas para cada intervalo
    $num = 0;
    $sql = "select intervalo from horarios where ano='" . $_POST["ano"] . "' and dia='" . $_POST["numdia"] . "' and grado='" . $_POST["grado"] . "' and curso='" . $_POST["curso"] . "' order by abs(intervalo)";
    $dathora = viewsql($sql);
    foreach ($dathora as $reghora) {
        $nameA = "A" . $reghora->intervalo;
        $id_asignatura = intval($_POST[$nameA]);
        $nameF = "F" . $reghora->intervalo;
        $chk = isset($_POST[$nameF]) ? $_POST[$nameF] : null;
        if (($chk == "on" or $total == 1) and $id_asignatura > 0)
            $falla = 1;
        else
            $falla = 0;
        $num = $num + 1;
        $asigna[$num] = $id_asignatura;
        $nhoras[$num] = $falla;
        $interv[$num] = $reghora->intervalo;
    }
    //acumula horas e intervalos de la misma asignatura
    for ($i = 1; $i <= 12; $i++) {
        $id_asignatura = $asigna[$i];
        $horas = $nhoras[$i];
        $intervalos = $interv[$i];
        for ($j = 1; $j <= 12; $j++) {
            if ($j != $i and $asigna[$j] == $id_asignatura) { //misma asignatura en diferente intervalo
                $horas = $horas + $nhoras[$j];
                $intervalos = $intervalos . " " . $interv[$j];
                $asigna[$j] = 0;
                $nhoras[$j] = 0;
                $interv[$j] = "";
            }
        }
        $nhoras[$i] = $horas;
        $interv[$i] = $intervalos;
    }
    //si modifica registro, borrar fallas de asignaturas grabadas
    if ($id_falla > 0) {
        $sql = "select id_falla from $file where id_alumno = '$id_alumno' and fecha = '$fecha' and id_asignatura > '0' ORDER BY id_falla";
        $datfall = viewsql($sql);
        foreach ($datfall as $regfall) {
            $id_falla = $regfall->id_falla;
            $sql = "DELETE FROM $file WHERE id_falla = '$id_falla' limit 1";
            execsql($sql);
        }
    }
    //crear falla por cada asignatura con acumulado de horas
    for ($i = 1; $i <= 12; $i++) {
        $id_asignatura = $asigna[$i];
        $horas = $nhoras[$i];
        $intervalos = " " . $interv[$i] . " ";
        if ($id_asignatura > 0 and $horas > 0) {
            $sql = "INSERT INTO $file (id_alumno, id_asignatura, id_categoria, fecha, periodo, horas, intervalos) VALUES ('$id_alumno', '$id_asignatura', '" . $_POST["id_categoria"] . "', '$fecha', '$periodo', '$horas', '$intervalos') ";
            execsql($sql);
        }
    }
    if (substr($page, 0, 6) == "fallas") {
        if ($page == "fallascurso")
            echo"<script>window.location.href='../fallas.php?fecha=$fecha&grado=" . $_POST["grado"] . "&curso=" . $_POST["curso"] . "';</script>";
        else
            echo"<script>window.location.href='../fallas.php?opcion=registrofecha&fecha=$fecha';</script>";
    } else
        echo"<script>window.location.href='../alumnos_fallas.php?id_alumno=$id_alumno';</script>";
}

//borrar y eliminar documento del alumno
if ($op == "borrarfalla") {
    $file = "y" . $_POST["ano"] . "fallas";
    $id_alumno = $_POST["id_alumno"];
    $fecha = fec_amd($_POST["fecha"]);
    $page = isset($_POST["page"]) ? $_POST["page"] : null;
    $sql = "select id_falla from $file where id_alumno = '$id_alumno' and fecha = '$fecha' ORDER BY id_falla";
    $datfall = viewsql($sql);
    foreach ($datfall as $regfall) {
        $id_falla = $regfall->id_falla;
        $sql = "DELETE FROM $file WHERE id_falla = '$id_falla' limit 1";
        execsql($sql);
    }
    disconnect();
    if (substr($page, 0, 6) == "fallas") {
        if ($page == "fallascurso")
            echo"<script>window.location.href='../fallas.php?fecha=$fecha&grado=" . $_POST["grado"] . "&curso=" . $_POST["curso"] . "';</script>";
        else
            echo"<script>window.location.href='../fallas.php?opcion=registrofecha&fecha=$fecha';</script>";
    } else
        echo"<script>window.location.href='../alumnos_fallas.php?id_alumno=$id_alumno';</script>";
}

//grabar falla del alumno en una fecha por horas de asignatura
//registro principal con la fecha y categoria, subregistros con la fecha, asignatura y horas
if ($op == "grabarfallascurso") {
    $file = "y" . $_POST["ano"] . "fallas";
    $fecha = fec_amd($_POST["fecha"]);
    $periodo = $_POST["periodo"];
    $sql="SELECT * FROM $cursos WHERE grado = '$grado' AND curso = '$curso' ORDER BY apellidos, nombres";
    $datalum=viewsql($sql);
    foreach($datalum as $regalum)
    {
        $id_alumno = $regalum->id_alumno;
	$campo = "op".$id_alumno;
	$tipo = isset($_POST[$campo]) ? $_POST[$campo] : null ;
        if ($tipo=="I") {

            
        }
    }
    echo"<script>window.location.href='../fallas.php?fecha=$fecha&grado=" . $_POST["grado"] . "&curso=" . $_POST["curso"] . "';</script>";
    
}

*/

//grabar cursos por a�o y grado
if ($op == "grabarcursos") {
    $ano = $_POST["ano"];
    $grado = $_POST["grado"];
    $error = "";
    for ($i = 1; $i <= 10; $i++) {
        $name = "curso" . $i;
        $dire = "direc" . $i;
        $curso = isset($_POST[$name]) ? $_POST[$name] : null;
        $id_director = isset($_POST[$dire]) ? $_POST[$dire] : null;
        $curso = trim(strtoupper($curso));
        if ($curso != "") {
            $id_curso = 0;
            $sql = "select id_curso from cursos where ano = '$ano' and grado = '$grado' and curso = '$curso' limit 1";
            $datcurs = viewsql($sql);
            foreach ($datcurs as $regcurs) {
                $id_curso = $regcurs->id_curso;
            }
            if ($id_curso == 0) {
                $sql = "INSERT INTO cursos (ano,grado,curso,id_director) VALUES ('$ano','$grado','$curso','$id_director')";
                execsql($sql);
            } else {
                $sql = "UPDATE cursos SET id_director='$id_director' WHERE id_curso='$id_curso' LIMIT 1";
                execsql($sql);
                //cierre de planillas abiertas del curso
                $planillas = "y" . $_POST["ano"] . "planillas";
                $sql = "SELECT pertipo, periodo, notanum FROM periodos WHERE ano = '$ano' and estado = 'ABIERTO' order by notanum";
                $datperi = viewsql($sql);
                foreach ($datperi as $regperi) {
                    $notanum = $regperi->notanum;
                    $pertipo = $regperi->pertipo;
                    $periodo = $regperi->periodo;
                    $peri = "peri" . $i . $notanum;
                    $varperi = isset($_POST[$peri]) ? $_POST[$peri] : null;
                    //estado anterior del curso
                    $oldestado = "";
                    $sql = "select estado from $planillas where id_asignatura = '0' AND grado = '$grado' AND curso = '$curso' AND pertipo = '$pertipo' AND periodo = '$periodo' LIMIT 1";
                    $datplan = viewsql($sql);
                    foreach ($datplan as $regplan) {
                        $oldestado = $regplan->estado;
                    }
                    if ($varperi == "CERRADO" and $oldestado != "CERRADO") {
                        //verificar que asignaturas que requieren nota tengan las planillas entregadas en el curso
                        $numreg = 0;
                        $sql = "select id_asignatura from asignaturas where ano = '$ano' and grado = '$grado' and notas = '1' order by id_asignatura";
                        $datasig = viewsql($sql);
                        foreach ($datasig as $regasig) {
                            $id_asignatura = $regasig->id_asignatura;
                            $estado = "ABIERTA";
                            $sql = "SELECT id_planilla, estado FROM $planillas WHERE id_asignatura = '$id_asignatura' AND grado = '$grado' AND curso = '$curso' AND pertipo = '$pertipo' AND periodo = '$periodo' limit 1";
                            $datplan = viewsql($sql);
                            foreach ($datplan as $regplan) {
                                $estado = trim($regplan->estado);
                            }
                            if ($estado === "ABIERTA" or $estado === "")
                                $numreg = $numreg + 1;
                        }
                        if ($numreg == 0) { //todas las planillas ENTREGADAS
                            //cierre planilla general del curso y periodo
                            $id_planilla = 0;
                            $sql = "select id_planilla from $planillas where id_asignatura = '0' and grado='$grado' and curso='$curso' and pertipo = '$pertipo' and periodo = '$periodo' limit 1";
                            $datreg = viewsql($sql);
                            foreach ($datreg as $reg) {
                                $id_planilla = intval($reg->id_planilla);
                            }
                            //cerrar periodo del curso en tabla planillas, id_asignatura = 0
                            if ($id_planilla > 0)
                                $sql = "UPDATE $planillas SET estado = 'CERRADO', fchcierre = '$fecha_hora' WHERE id_planilla = '$id_planilla' LIMIT 1";
                            else
                                $sql = "INSERT INTO $planillas (grado, curso, id_asignatura, pertipo, periodo, estado, fchcierre) VALUES ('$grado', '$curso', '0', '$pertipo', '$periodo', 'CERRADO', '$fecha_hora')";
                            execsql($sql);
                            //cerrar planillas asignaturas del curso
                            $sql = "SELECT id_planilla FROM $planillas WHERE id_asignatura > '0' AND grado = '$grado' AND curso = '$curso' AND pertipo = '$pertipo' AND periodo = '$periodo' ORDER BY id_planilla";
                            $datplan = viewsql($sql);
                            foreach ($datplan as $regplan) {
                                $id_planilla = $regplan->id_planilla;
                                $sql = "UPDATE $planillas SET estado = 'CERRADA', fchcierre = '$fecha_hora' WHERE id_planilla = '$id_planilla' LIMIT 1";
                                execsql($sql);
                            }
                            //retotalizar nuevamente notas del periodo y curso o finales
                            include_once("grabar_notas.php");
                            if ($pertipo == "P") //examen notas finales
                                promediar_notas($ano, $periodo, $grado, $curso, '0');
                            else
                                promediar_finales($ano, $grado, $curso, '0');
                        }
                        else {
                            $error .= "Existen $numreg planillas SIN ENTREGAR para el curso $curso.";
                        }
                    } //fin periodo que cierra
                }
            }
        }
    } //fin for cursos
    if ($error != "")
        echo"<script> alert('Error! $error'); window.location.href='../alumnos_cursos.php?ano=$ano&grado=$grado';</script>";
    else
        echo"<script>window.location.href='../alumnos_cursos.php?ano=$ano&grado=$grado';</script>";
}

//borrar un curso por a�o y grado
if ($op == "borrarcurso") {
    $ano = $_GET["ano"];
    $grado = $_GET["grado"];
    $id_curso = $_GET["id_curso"];
    $sql = "delete from cursos where id_curso = '$id_curso' limit 1";
    execsql($sql);
    disconnect();
    echo"<script>window.location.href='../alumnos_cursos.php?ano=$ano&grado=$grado';</script>";
}

//retotaliza notas, promedios y puestos del curso
if ($op == "promediar_curso") {
    $ano = $_GET["ano"];
    $periodo = $_GET["periodo"];
    $grado = $_GET["grado"];
    $curso = $_GET["curso"];
    $id_alumno = 0;
    $pertipo = substr($periodo, 0, 1);
    $periodo = substr($periodo, 1, 1);
    include_once("grabar_notas.php");
    if ($pertipo == "P") { //periodo de notas
        if ($periodo <= 4)
            promediar_notas($ano, $periodo, $grado, $curso, $id_alumno);
        else
            promediar_anolect($ano, $grado, $curso, $id_alumno);
    } else //examenes
        promediar_examen($ano, $periodo, $grado, $curso, $id_alumno);
    disconnect();
    echo"<script> alert('Notas y promedios del curso $curso $pertipo$periodo actualizados.'); window.location.href='../alumnos_cursos.php?ano=$ano&grado=$grado';</script>";
}

/* * ***************************************************************************** */
/* Funci�n que actualiza un alumno en el curso                                  */
/* Recibe a�o, nivel, grado, curso, alumno, anterior curso                      */
/* * ***************************************************************************** */

function actualizar_curso($ano, $nivel, $grado, $curso, $id_alumno, $oldcurso) {
    $file = "y" . $ano . "cursos";
    $apellidos = "";
    $nombres = "";
    $tipoedu = "";
    $sql = "SELECT apellidos, nombres, tipoedu FROM alumnos WHERE id_alumno = '$id_alumno' limit 1";
    $datalum = viewsql($sql);
    foreach ($datalum as $regalum) {
        $apellidos = $regalum->apellidos;
        $nombres = $regalum->nombres;
        $tipoedu = $regalum->tipoedu;
    }
    $id_curso = 0;
    $sql = "select id_curso from $file where id_alumno = '$id_alumno' limit 1";
    $datcurs = viewsql($sql);
    foreach ($datcurs as $regcurs) {
        $id_curso = intval($regcurs->id_curso);
    }
    if ($id_curso <= 0)
    {
        $sql = "INSERT INTO $file (id_alumno, nivel, grado, curso, apellidos, nombres, tipoedu) VALUES ('$id_alumno', '$nivel', '$grado', '$curso', '$apellidos', '$nombres', '$tipoedu') ";
        execsql($sql);
    }
    else
    {
        $sql = "UPDATE $file SET nivel='$nivel', grado='$grado', curso = '$curso', apellidos = '$apellidos', nombres = '$nombres', tipoedu = '$tipoedu' where id_alumno='$id_alumno' limit 1";
        execsql($sql);
        $sql = "UPDATE transporte SET curso = '$curso' WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
        execsql($sql);
    }
    //retotalizar alumnos de nuevo curso
    totalizar_curso($ano, $grado, $curso);
    //retotalizar alumnos de anterior curso
    if ($curso != $oldcurso)
        totalizar_curso($ano, $grado, $oldcurso);
}

/* * ***************************************************************************** */
/* Funci�n que retotaliza numero de alumnos por curso                           */
/* Recibe a�o, grado, curso                                                     */
/* * ***************************************************************************** */
function totalizar_curso($ano, $grado, $curso) {
    $numT = 0;
    $numE = 0;
    $file = "y" . $ano . "cursos";
    $sql = "select count(*) as numreg from $file where grado='$grado' and curso='$curso'";
    $datcurs = viewsql($sql);
    foreach ($datcurs as $regcurs) {
        $numT = $regcurs->numreg;
    }
    $sql = "select count(*) as numreg from $file where grado='$grado' and curso='$curso' and tipoedu='E'";
    $datcurs = viewsql($sql);
    foreach ($datcurs as $regcurs) {
        $numE = $regcurs->numreg;
    }
    $sql = "UPDATE cursos SET alumtot='$numT',alumesp='$numE' WHERE ano = '$ano' and grado='$grado' and curso = '$curso' limit 1";
    execsql($sql);
}


?>