<?php

Function datos_profesor($id_profesor)
{
	$apellidos = "";
	$nombres ="";
	$docutip = "";
	$docunum = "";
	$docuexp = "";
	$genero = "";
	$tipo = "";
	$cargo = "";
	$escgrado = "";
	$escresol = "";
	$especialidad = "";
	$correo = "";
	$estrato = 0;
	$barrio = "";
	$direccion = "";
	$telefono = "";
	$celular = "";
	$detalles = "";
	$lugnace = "";
	$fchnace = "";
	$fchingreso = "";
	$fchretiro = "";
	$foto = "";
	$estado = "ACTIVO";
	if ($id_profesor > 0)
	{
		$sql="select * from profesores where id_profesor = '$id_profesor' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$apellidos = $reg->apellidos;
			$nombres = $reg->nombres;
			$docutip = $reg->docutip;
			$docunum = $reg->docunum;
			$docuexp = $reg->docuexp;
			$genero = $reg->genero;
			$tipo = $reg->tipo;
			$cargo = $reg->cargo;
			$escgrado = $reg->escgrado;
			$escresol = $reg->escresol;
			$especialidad = $reg->especialidad;
			$correo = $reg->correo;
			$estrato = $reg->estrato;
			$barrio = $reg->barrio;
			$direccion = $reg->direccion;
			$telefono = $reg->telefono;
			$celular = $reg->celular;
			$detalles = $reg->detalles;
			$lugnace = $reg->lugnace;
			$fchnace = $reg->fchnace;
			$fchingreso =$reg->fchingreso;
			$fchretiro = $reg->fchretiro;
			$foto = $reg->foto;
			$estado = $reg->estado;
			if (!file_exists($foto))
				$foto = "";
		}
	}
	$datprofesor = array();
	$datprofesor["apellidos"] = $apellidos;
	$datprofesor["nombres"] = $nombres;
	$datprofesor["docutip"] = $docutip;
	$datprofesor["docunum"] = $docunum;
	$datprofesor["docuexp"] = $docuexp;
	$datprofesor["genero"] = $genero;
	$datprofesor["tipo"] = $tipo;
	$datprofesor["cargo"] = $cargo;
	$datprofesor["escgrado"] = $escgrado;
	$datprofesor["escresol"] = $escresol;
	$datprofesor["especialidad"] = $especialidad;
	$datprofesor["correo"] = $correo;
	$datprofesor["estrato"] = $estrato;
	$datprofesor["barrio"] = $barrio;
	$datprofesor["direccion"] = $direccion;
	$datprofesor["telefono"] = $telefono;
	$datprofesor["celular"] = $celular;
	$datprofesor["detalles"] = $detalles;
	$datprofesor["lugnace"] = $lugnace;
	$datprofesor["fchnace"] = $fchnace;
	$datprofesor["fchingreso"] = $fchingreso;
	$datprofesor["fchretiro"] = $fchretiro;
	$datprofesor["estado"] = $estado;
	$datprofesor["foto"] = $foto;
	return $datprofesor;
}

Function datos_contrato($id_contrato)
{
	$ano = 0;
	$fchinicio = "";
	$fchfinal = "";
	$tipocont = "";
	$tiempo = "";
	$horas = "";
	$estado = "";
	$grados = "";
	$detalles = "";
	if ($id_contrato > 0)
	{
		$sql="select * from profesorescont where id_contrato = '$id_contrato' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$ano = $reg->ano;
			$fchinicio = fec_dma($reg->fchinicio);
			$fchfinal = fec_dma($reg->fchfinal);
			$tipocont = $reg->tipocont;
			$tiempo = $reg->tiempo;
			$horas = $reg->horas;
			$estado = $reg->estado;
			$grados = $reg->grados;
			$detalles = $reg->detalles;
		}
	}
	$datcontrato = array();
	$datcontrato["ano"] = $ano;
	$datcontrato["fchinicio"] = $fchinicio;
	$datcontrato["fchfinal"] = $fchfinal;
	$datcontrato["tipocont"] = $tipocont;
	$datcontrato["tiempo"] = $tiempo;
	$datcontrato["horas"] = $horas;
	$datcontrato["estado"] = $estado;
	$datcontrato["grados"] = $grados;
	$datcontrato["detalles"] = $detalles;
	return $datcontrato;
}

?>