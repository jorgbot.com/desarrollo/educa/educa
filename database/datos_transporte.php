<?php

Function datos_transporte($id_transporte)
{
	$dattransp = array();
	$dattransp["ano"] = $_SESSION["anoaca"];
	$dattransp["grado"] = "";
	$dattransp["curso"] = "";
	$dattransp["codigo"] = "";
	$dattransp["estado"] = "";
	$dattransp["fchingreso"] = "";
	$dattransp["fchretiro"] = "";
	$dattransp["id_ruta"] = 0;
	$dattransp["id_familiar"] = 0;
	$dattransp["paradero"] = "";
	$dattransp["jornada"] = 0;
	$dattransp["id_rutaop"] = 0;
	$dattransp["id_familiarop"] = 0;
	$dattransp["paraderoop"] = "";
	$dattransp["jornadaop"] = 0;
	$dattransp["transp02"] = "";
	$dattransp["transp03"] = "";
	$dattransp["transp04"] = "";
	$dattransp["transp05"] = "";
	$dattransp["transp06"] = "";
	$dattransp["transp07"] = "";
	$dattransp["transp08"] = "";
	$dattransp["transp09"] = "";
	$dattransp["transp10"] = "";
	$dattransp["transp11"] = "";
	$dattransp["coment02"] = "";
	$dattransp["coment03"] = "";
	$dattransp["coment04"] = "";
	$dattransp["coment05"] = "";
	$dattransp["coment06"] = "";
	$dattransp["coment07"] = "";
	$dattransp["coment08"] = "";
	$dattransp["coment09"] = "";
	$dattransp["coment10"] = "";
	$dattransp["coment11"] = "";
	$dattransp["detalles"] = "";
	$dattransp["fchactual"] = "";
	$dattransp["usuactual"] = "";
	if ($id_transporte > 0)
	{
		$sql="select * from transporte where id_transporte = '$id_transporte' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$dattransp["ano"] = $reg->ano;
			$dattransp["grado"] = $reg->grado;
			$dattransp["curso"] = $reg->curso;
			$dattransp["codigo"] = $reg->codigo;
			$dattransp["estado"] = $reg->estado;
			$dattransp["fchingreso"] = fec_dma($reg->fchingreso);
			$dattransp["fchretiro"] = fec_dma($reg->fchretiro);
			$dattransp["id_ruta"] = $reg->id_ruta;
			$dattransp["id_familiar"] = $reg->id_familiar;
			$dattransp["paradero"] = $reg->paradero;
			$dattransp["jornada"] = $reg->jornada;
			$dattransp["id_rutaop"] = $reg->id_rutaop;
			$dattransp["id_familiarop"] = $reg->id_familiarop;
			$dattransp["paraderoop"] = $reg->paraderoop;
			$dattransp["jornadaop"] = $reg->jornadaop;
			if ($reg->transp02 > 0)
				$dattransp["transp02"] = $reg->transp02;
			if ($reg->transp03 > 0)
				$dattransp["transp03"] = $reg->transp03;
			if ($reg->transp04 > 0)
				$dattransp["transp04"] = $reg->transp04;
			if ($reg->transp05 > 0)
				$dattransp["transp05"] = $reg->transp05;
			if ($reg->transp06 > 0)
				$dattransp["transp06"] = $reg->transp06;
			if ($reg->transp07 > 0)
				$dattransp["transp07"] = $reg->transp07;
			if ($reg->transp08 > 0)
				$dattransp["transp08"] = $reg->transp08;
			if ($reg->transp09 > 0)
				$dattransp["transp09"] = $reg->transp09;
			if ($reg->transp10 > 0)
				$dattransp["transp10"] = $reg->transp10;
			if ($reg->transp11 > 0)
				$dattransp["transp11"] = $reg->transp11;
			$dattransp["coment02"] = $reg->coment02;
			$dattransp["coment03"] = $reg->coment03;
			$dattransp["coment04"] = $reg->coment04;
			$dattransp["coment05"] = $reg->coment05;
			$dattransp["coment06"] = $reg->coment06;
			$dattransp["coment07"] = $reg->coment07;
			$dattransp["coment08"] = $reg->coment08;
			$dattransp["coment09"] = $reg->coment09;
			$dattransp["coment10"] = $reg->coment10;
			$dattransp["coment11"] = $reg->coment11;
			$dattransp["detalles"] = $reg->detalles;
			$dattransp["fchactual"] = $reg->fchactual;
			$dattransp["usuactual"] = $reg->usuactual;
		}
	}
	return $dattransp;
}

Function datos_ruta($id_ruta)
{
	$datruta = array();
	$datruta["ano"] = $_SESSION["anoaca"];
	$datruta["ruta"] = "";
	$datruta["recorrido"] = "";
	$datruta["id_conductor"] = "";
	$datruta["id_monitor"] = "";
	$datruta["detalles"] = "";
	$datruta["fchactual"] = "";
	$datruta["usuactual"] = "";
	if ($id_ruta > 0)
	{
		$sql="SELECT * FROM rutas WHERE id_ruta = '$id_ruta' LIMIT 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$datruta["ano"] = $reg->ano;
			$datruta["ruta"] = $reg->ruta;
			$datruta["recorrido"] = $reg->recorrido;
			$datruta["id_conductor"] = $reg->id_conductor;
			$datruta["id_monitor"] = $reg->id_monitor;
			$datruta["detalles"] = $reg->detalles;
			$datruta["fchactual"] = $reg->fchactual;
			$datruta["usuactual"] = $reg->usuactual;
		}
	}
	return $datruta;
}

Function datos_conductor($id_conductor)
{
	$datconductor = array();
	$datconductor["apellidos"] = "";
	$datconductor["nombres"] = "";
	$datconductor["rh"] = "";
	$datconductor["categoria"] = "";
	$datconductor["docutip"] = "CC";
	$datconductor["docunum"] = "";
	$datconductor["docuexp"] = "";
	$datconductor["correo"] = "";
	$datconductor["barrio"] = "";
	$datconductor["direccion"] ="";
	$datconductor["telefono"] = "";
	$datconductor["celular"] = "";
	$datconductor["placa"] = "";
	$datconductor["marca"] = "";
	$datconductor["color"] = "";
	$datconductor["modelo"] = "";
	$datconductor["capacidad"] = "";
	$datconductor["detalles"] = "";
	$datconductor["fchingreso"] = "";
	$datconductor["fchretiro"] = "";
	$datconductor["estado"] = "ACTIVO";
	$datconductor["foto"] = "";
	$datconductor["fchactual"] = "";
	$datconductor["usuactual"] = "";
	if ($id_conductor > 0)
	{
		$sql="select * from conductores where id_conductor = '$id_conductor' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$datconductor["apellidos"] = $reg->apellidos;
			$datconductor["nombres"] = $reg->nombres;
			$datconductor["rh"] = $reg->rh;
			$datconductor["categoria"] = $reg->categoria;
			$datconductor["docutip"] = $reg->docutip;
			$datconductor["docunum"] = $reg->docunum;
			$datconductor["docuexp"] = $reg->docuexp;
			$datconductor["correo"] = $reg->correo;
			$datconductor["barrio"] = $reg->barrio;
			$datconductor["direccion"] = $reg->direccion;
			$datconductor["telefono"] = $reg->telefono;
			$datconductor["celular"] = $reg->celular;
			$datconductor["placa"] = $reg->placa;
			$datconductor["marca"] = $reg->marca;
			$datconductor["color"] = $reg->color;
			$datconductor["modelo"] = $reg->modelo;
			$datconductor["capacidad"] = $reg->capacidad;
			$datconductor["detalles"] = $reg->detalles;
			$datconductor["fchingreso"] = $reg->fchingreso;
			$datconductor["fchretiro"] = $reg->fchretiro;
			$datconductor["estado"] = $reg->estado;
			$datconductor["fchactual"] = $reg->fchactual;
			$datconductor["usuactual"] = $reg->usuactual;
			$foto = $reg->foto;
			if (!file_exists($foto))
				$foto = "";
			$datconductor["foto"] = $reg->foto;
		}
	}
	return $datconductor;
}

Function datos_monitor($id_monitor)
{
	$datmonitor = array();
	$datmonitor["apellidos"] = "";
	$datmonitor["nombres"] = "";
	$datmonitor["docutip"] = "CC";
	$datmonitor["docunum"] = "";
	$datmonitor["docuexp"] = "";
	$datmonitor["rh"] = "";
	$datmonitor["correo"] = "";
	$datmonitor["barrio"] = "";
	$datmonitor["direccion"] ="";
	$datmonitor["telefono"] = "";
	$datmonitor["celular"] = "";
    $datmonitor["fchingreso"] = "";
	$datmonitor["fchretiro"] = "";
	$datmonitor["estado"] = "ACTIVO";
	$datmonitor["fchactual"] = "";
	$datmonitor["usuactual"] = "";
	if ($id_monitor > 0)
	{
		$sql="select * from monitores where id_monitor = '$id_monitor' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$datmonitor["apellidos"] = $reg->apellidos;
			$datmonitor["nombres"] = $reg->nombres;
			$datmonitor["docutip"] = $reg->docutip;  
			$datmonitor["docunum"] = $reg->docunum;
			$datmonitor["docuexp"] = $reg->docuexp;
			$datmonitor["rh"] = $reg->rh;
			$datmonitor["correo"] = $reg->correo;
			$datmonitor["barrio"] = $reg->barrio;
			$datmonitor["direccion"] = $reg->direccion;
			$datmonitor["telefono"] = $reg->telefono;
			$datmonitor["celular"] = $reg->celular;
			$datmonitor["fchingreso"] = $reg->fchingreso;
			$datmonitor["fchretiro"] = $reg->fchretiro;
			$datmonitor["estado"] = $reg->estado;
			$datmonitor["fchactual"] = $reg->fchactual;
			$datmonitor["usuactual"] = $reg->usuactual;
			
		}
	}
	return $datmonitor;
}

?>