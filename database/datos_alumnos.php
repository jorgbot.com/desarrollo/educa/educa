<?php

Function datos_alumno($id_alumno)
{
	$apellidos = "";
	$nombres ="";
	$docutip = "TI";
	$docunum = "";
	$docuexp = "";
	$docudep = "";
	$genero = "";
	$tipoedu = "F";
	$id_saludenti = 0;
	$correo = "";
	$celular = "";
	$proceinsti = "";
	$procegrado = "";
	$detalles = "";
	$fchnace = "";
	$lugnace = "BOGOTA";
	$depnace = "BOGOTA";
	$paisnace = "COLOMBIA";
	$fchingreso = "";
	$fchretiro = "";
	$fchgrado = "";
	$foto = "";
	$estado = "ACTIVO";
	$codigo = "";
	$id_familiar = 0;
	$id_matricula = 0;
	$edad = 0;
	$fchactual = "";
	$usuactual = "";
	if ($id_alumno > 0)
	{
		$sql="select * from alumnos where id_alumno = '$id_alumno' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$apellidos = $reg->apellidos;
			$nombres = $reg->nombres;
			$docutip = $reg->docutip;
			$docunum = $reg->docunum;
			$docuexp = $reg->docuexp;
			$docudep = $reg->docudep;
			$genero = $reg->genero;
			$tipoedu = $reg->tipoedu;
			$id_saludenti = $reg->id_saludenti;
			$correo = $reg->correo;
			$celular = $reg->celular;
			$proceinsti = $reg->proceinsti;
			$procegrado = $reg->procegrado;
			$detalles = $reg->detalles;
			$fchnace = $reg->fchnace;
			$lugnace = $reg->lugnace;
			$depnace = $reg->depnace;
			$paisnace = $reg->paisnace;
			$fchingreso =$reg->fchingreso;
			$fchretiro = $reg->fchretiro;
			$fchgrado = $reg->fchgrado;
			$foto = $reg->foto;
			$estado = $reg->estado;
			if (!file_exists($foto))
				$foto = "";
			$codigo = $reg->codigo;
			$id_familiar = $reg->id_familiar;
			$edad = calculo_edad($reg->fchnace);
			$fchactual = $reg->fchactual;
			$usuactual = $reg->usuactual;
			//ultima matricula
			$sql = "select id_matricula from matriculas where id_alumno = '$id_alumno' order by ano desc limit 1";
			$datmatr = viewsql($sql);
			foreach($datmatr as $regmatr)
			{$id_matricula = $regmatr->id_matricula;}
		}
	}
	$datalumno = array();
	$datalumno["apellidos"] = $apellidos;
	$datalumno["nombres"] = $nombres;
	$datalumno["docutip"] = $docutip;
	$datalumno["docunum"] = $docunum;
	$datalumno["docuexp"] = $docuexp;
	$datalumno["docudep"] = $docudep;
	$datalumno["genero"] = $genero;
	$datalumno["tipoedu"] = $tipoedu;
	$datalumno["id_saludenti"] = $id_saludenti;
	$datalumno["correo"] = $correo;
	$datalumno["celular"] = $celular;
	$datalumno["proceinsti"] = $proceinsti;
	$datalumno["procegrado"] = $procegrado;
	$datalumno["detalles"] = $detalles;
	$datalumno["estado"] = $estado;
	$datalumno["codigo"] = $codigo;
	$datalumno["id_familiar"] = $id_familiar;
	$datalumno["id_matricula"] = $id_matricula;
	$datalumno["foto"] = $foto;
	$datalumno["edad"] = $edad;
	$datalumno["fchnace"] = $fchnace;
	$datalumno["lugnace"] = $lugnace;
	$datalumno["depnace"] = $depnace;
	$datalumno["paisnace"] = $paisnace;
	$datalumno["fchingreso"] = $fchingreso;
	$datalumno["fchretiro"] = $fchretiro;
	$datalumno["fchgrado"] = $fchgrado;
	$datalumno["fchactual"] = $fchactual;
	$datalumno["usuactual"] = $usuactual;
	return $datalumno;
}

Function datos_matricula($id_matricula)
{
	$ano = $_SESSION["anoaca"];
	$grado = "";
	$curso = "";
	$fecha = "";
	$fchretiro = "";
	$renovar = 1;
	$estado = "";
	$folio = "";
	$id_respmatri = "";
	$id_resppagos = "";
	$id_respnotas = "";
	$beca = 0;
	$matricula = "";
        $interes = 0;
	$pension02 = "";
	$pension03 = "";
	$pension04 = "";
	$pension05 = "";
	$pension06 = "";
	$pension07 = "";
	$pension08 = "";
	$pension09 = "";
	$pension10 = "";
	$pension11 = "";
	$coment02 = "";
	$coment03 = "";
	$coment04 = "";
	$coment05 = "";
	$coment06 = "";
	$coment07 = "";
	$coment08 = "";
	$coment09 = "";
	$coment10 = "";
	$coment11 = "";
	$detalles = "";
	$fchactual = "";
	$usuactual = "";
	if ($id_matricula > 0)
	{
		$sql="select * from matriculas where id_matricula = '$id_matricula' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$ano = $reg->ano;
			$grado = $reg->grado;
			$curso = $reg->curso;
			$fecha = fec_dma($reg->fecha);
			$fchretiro = fec_dma($reg->fchretiro);
			$estado = $reg->estado;
			$renovar = $reg->renovar;
			$folio = $reg->folio;
			$id_respmatri = $reg->id_respmatri;
			$id_resppagos = $reg->id_resppagos;
			$id_respnotas = $reg->id_respnotas;
			$beca = $reg->beca;
			if ($reg->matricula > 0)
				$matricula = $reg->matricula;
                        $interes = $reg->interes;
                        /*
			if ($reg->pension02 > 0)
				$pension02 = $reg->pension02;
			if ($reg->pension03 > 0)
				$pension03 = $reg->pension03;
			if ($reg->pension04 > 0)
				$pension04 = $reg->pension04;
			if ($reg->pension05 > 0)
				$pension05 = $reg->pension05;
			if ($reg->pension06 > 0)
				$pension06 = $reg->pension06;
			if ($reg->pension07 > 0)
				$pension07 = $reg->pension07;
			if ($reg->pension08 > 0)
				$pension08 = $reg->pension08;
			if ($reg->pension09 > 0)
				$pension09 = $reg->pension09;
			if ($reg->pension10 > 0)
				$pension10 = $reg->pension10;
			if ($reg->pension11 > 0)
				$pension11 = $reg->pension11;
			$coment02 = $reg->coment02;
			$coment03 = $reg->coment03;
			$coment04 = $reg->coment04;
			$coment05 = $reg->coment05;
			$coment06 = $reg->coment06;
			$coment07 = $reg->coment07;
			$coment08 = $reg->coment08;
			$coment09 = $reg->coment09;
			$coment10 = $reg->coment10;
			$coment11 = $reg->coment11;
                         */
			$detalles = $reg->detalles;
			$fchactual = $reg->fchactual;
			$usuactual = $reg->usuactual;
		}
	}
	$datmatricula = array();
	$datmatricula["id_matricula"] = $id_matricula;
	$datmatricula["ano"] = $ano;
	$datmatricula["grado"] = $grado;
	$datmatricula["curso"] = $curso;
	$datmatricula["fecha"] = $fecha;
	$datmatricula["fchretiro"] = $fchretiro;
	$datmatricula["renovar"] = $renovar;
	$datmatricula["estado"] = $estado;
	$datmatricula["folio"] = $folio;
	$datmatricula["id_respmatri"] = $id_respmatri;
	$datmatricula["id_resppagos"] = $id_resppagos;
	$datmatricula["id_respnotas"] = $id_respnotas;
	$datmatricula["beca"] = $beca;
	$datmatricula["matricula"] = $matricula;
	$datmatricula["interes"] = $interes;
	$datmatricula["pension02"] = $pension02;
	$datmatricula["pension03"] = $pension03;
	$datmatricula["pension04"] = $pension04;
	$datmatricula["pension05"] = $pension05;
	$datmatricula["pension06"] = $pension06;
	$datmatricula["pension07"] = $pension07;
	$datmatricula["pension08"] = $pension08;
	$datmatricula["pension09"] = $pension09;
	$datmatricula["pension10"] = $pension10;
	$datmatricula["pension11"] = $pension11;
	$datmatricula["coment02"] = $coment02;
	$datmatricula["coment03"] = $coment03;
	$datmatricula["coment04"] = $coment04;
	$datmatricula["coment05"] = $coment05;
	$datmatricula["coment06"] = $coment06;
	$datmatricula["coment07"] = $coment07;
	$datmatricula["coment08"] = $coment08;
	$datmatricula["coment09"] = $coment09;
	$datmatricula["coment10"] = $coment10;
	$datmatricula["coment11"] = $coment11;
	$datmatricula["detalles"] = $detalles;
	$datmatricula["fchactual"] = $fchactual;
	$datmatricula["usuactual"] = $usuactual;
	return $datmatricula;
}

Function ultima_matricula($id_alumno)
{
	$id_matricula = 0;
	$ano = "";
	$grado = "";
	$curso = "";
	$estado = "";
	$sql="select id_matricula, ano, grado, curso, estado from matriculas where id_alumno = '$id_alumno' order by ano desc limit 1";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{
		$id_matricula = $reg->id_matricula;
		$ano = $reg->ano;
		$grado = $reg->grado;
		$curso = $reg->curso;
		$estado = $reg->estado;
	}
	$datmatricula = array();
	$datmatricula["id_matricula"] = $id_matricula;
	$datmatricula["ano"] = $ano;
	$datmatricula["grado"] = $grado;
	$datmatricula["curso"] = $curso;
	$datmatricula["estado"] = $estado;
	return $datmatricula;
}

Function datos_familiar($id_familiar)
{
	$datfamiliar = array();
	$datfamiliar["tipo"] = "";
	$datfamiliar["apellidos"] = "";
	$datfamiliar["nombres"] = "";
	$datfamiliar["docutip"] = "CC";
	$datfamiliar["docunum"] = "";
	$datfamiliar["docuexp"] = "BOGOTA";
	$datfamiliar["pais"] = "COLOMBIA";
	$datfamiliar["correo"] = "";
	$datfamiliar["estrato"] = 0;
	$datfamiliar["ciudad"] = "BOGOTA";
	$datfamiliar["barrio"] = "";
	$datfamiliar["direccion"] = "";
	$datfamiliar["telefono"] = "";
	$datfamiliar["telefofi"] = "";
	$datfamiliar["celular"] = "";
	$datfamiliar["profesion"] = "";
	$datfamiliar["foto"] = "";
	$datfamiliar["detalles"] = "";
	if ($id_familiar > 0)
	{
		$sql="select * from familiares where id_familiar = '$id_familiar' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$datfamiliar["tipo"] = $reg->tipo;
			$datfamiliar["apellidos"] = $reg->apellidos;
			$datfamiliar["nombres"] = $reg->nombres;
			$datfamiliar["docutip"] = $reg->docutip;
			$datfamiliar["docunum"] = $reg->docunum;
			$datfamiliar["docuexp"] = $reg->docuexp;
			$datfamiliar["pais"] = $reg->pais;
			$datfamiliar["correo"] = $reg->correo;
			$datfamiliar["estrato"] = $reg->estrato;
			$datfamiliar["ciudad"] = $reg->ciudad;
			$datfamiliar["barrio"] = $reg->barrio;
			$datfamiliar["direccion"] = $reg->direccion;
			$datfamiliar["telefono"] = $reg->telefono;
			$datfamiliar["telefofi"] = $reg->telefofi;
			$datfamiliar["celular"] = $reg->celular;
			$datfamiliar["profesion"] = $reg->profesion;
			$datfamiliar["foto"] = $reg->foto;
			$datfamiliar["detalles"] = $reg->detalles;
			if (!file_exists($datfamiliar["foto"]))
				$datfamiliar["foto"] = "";
		}
	}
	return $datfamiliar;
}

Function basicos_familiar($id_familiar)
{
	$tipo = "";
	$apellidos = "";
	$nombres ="";
	$barrio = "";
	$direccion = "";
	$telefono = "";
	$telefofi = "";
	$celular = "";
	if ($id_familiar > 0)
	{
		$sql="select tipo,apellidos,nombres,barrio,direccion,telefono,telefofi,celular from familiares where id_familiar = '$id_familiar' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$tipo = $reg->tipo;
			$apellidos = $reg->apellidos;
			$nombres = $reg->nombres;
			$barrio = $reg->barrio;
			$direccion = $reg->direccion;
			$telefono = $reg->telefono;
			$telefofi = $reg->telefofi;
			$celular = $reg->celular;
		}
	}
	$datfamiliar = array();
	$datfamiliar["tipo"] = $tipo;
	$datfamiliar["apellidos"] = $apellidos;
	$datfamiliar["nombres"] = $nombres;
	$datfamiliar["barrio"] = $barrio;
	$datfamiliar["direccion"] = $direccion;
	$datfamiliar["telefono"] = $telefono;
	$datfamiliar["telefofi"] = $telefofi;
	$datfamiliar["celular"] = $celular;
	return $datfamiliar;
}

Function datos_falla($ano, $id_falla)
{
	$fecha = date('Y-m-d');
	$id_categoria = 0;
        $tipo = "I";
        $concepto = "";
	$fchfin = "";
	$fchjust = "";
        $hcita = "08:00";
	$periodo = 0;
	$detalles = "";
	if ($id_falla > 0)
	{
		$fallas = "y".$ano."fallas";
		$sql="select * from $fallas where id_falla = '$id_falla' limit 1";
		$dat = viewsql($sql);
		foreach($dat as $reg)
		{
			$tipo = $reg->tipo;
			$concepto = $reg->concepto;
			$id_categoria = $reg->id_categoria;
			$fecha = $reg->fecha;
			if (substr($reg->fchfin,0,2)!="00")
				$fchfin = $reg->fchfin;
			if (substr($reg->fchjust,0,2)!="00")
				$fchjust = $reg->fchjust;
                        $hcita = $reg->hcita;
			$periodo = $reg->periodo;
			$detalles = $reg->detalles;
		}
	}
	$datfalla = array();
	$datfalla["tipo"] = $tipo;
	$datfalla["concepto"] = $concepto;
	$datfalla["fecha"] = $fecha;
	$datfalla["fchfin"] = $fchfin;
	$datfalla["fchjust"] = $fchjust;
	$datfalla["periodo"] = $periodo;
	$datfalla["hcita"] = $hcita;
	$datfalla["id_categoria"] = $id_categoria;
	$datfalla["detalles"] = $detalles;
	return $datfalla;
}

?>