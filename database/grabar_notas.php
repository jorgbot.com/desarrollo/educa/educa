<?php
/************************************************************************************************
* @file        : grabar_notas.php                                                               *
* @brief       : Librer�a funciones para grabar notas: actividades, logros, asignaturas, areas  *
* @version     : 1.0                                                                            *
* @author      : Nora Rodriguez 15-feb-2012                                                     *
* @Modificado  : Nora Rodriguez 11-abr-2018                                                     *
************************************************************************************************/

/********************************************************************************/
/* funci�n que graba nota de una actividad de una evidencia y logro             */
/* recibe a�o, periodo, alumno, asignatura, logro, actividad, nota actividad    */ 
/********************************************************************************/
function grabar_notaactiv($ano, $id_alumno, $id_asignatura, $id_logro, $id_evidencia, $id_actividad, $periodo, $nota)
{
	$notasdet = "y".$ano."notasp".$periodo;
	$id_nota = 0;
	$sql="select id_nota from $notasdet where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' ".
		"and id_logro = '$id_logro' and id_evidencia = '$id_evidencia' and id_actividad = '$id_actividad' limit 1";
	$datnota=viewsql($sql);
	foreach($datnota as $regnota)
	{$id_nota = intval($regnota->id_nota);}
	if ($id_nota > 0)
	{
		$sql = "UPDATE $notasdet SET nota = '$nota' WHERE id_nota = '$id_nota' LIMIT 1";
		execsql($sql);
	}
	else
	{
		if ($nota > 0)
		{
			$sql= "INSERT INTO $notasdet (id_alumno, id_asignatura, id_logro, id_evidencia, id_actividad, nota) ".
				"VALUES ('$id_alumno', '$id_asignatura', '$id_logro', '$id_evidencia', '$id_actividad', '$nota')";
			execsql($sql);
		}
	}
}

/********************************************************************************/
/* funci�n que graba nota de una evidencia de un logro por periodo              */
/* recibe a�o, periodo, alumno, asignatura, logro, evidencia, nota evidencia    */ 
/********************************************************************************/
function grabar_notaevide($ano, $id_alumno, $id_asignatura, $id_logro, $id_evidencia, $periodo, $nota)
{
	$notasdet = "y".$ano."notasp".$periodo;
	$id_nota = 0;
	$sql="select id_nota from $notasdet where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' ".
		"and id_logro = '$id_logro' and id_evidencia = '$id_evidencia' and id_actividad = '0' limit 1";
	$datnota=viewsql($sql);
	foreach($datnota as $regnota)
	{$id_nota = intval($regnota->id_nota);}
	if ($id_nota > 0)
	{
		$sql = "UPDATE $notasdet SET nota = '$nota' WHERE id_nota = '$id_nota' LIMIT 1";
		execsql($sql);
	}
	else
	{
		if ($nota > 0)
		{
			$sql= "INSERT INTO $notasdet (id_alumno, id_asignatura, id_logro, id_evidencia, nota) ".
				"VALUES ('$id_alumno', '$id_asignatura', '$id_logro', '$id_evidencia', '$nota')";
			execsql($sql);
		}
	}
}

/********************************************************************************/
/* funci�n que graba nota total del logro                                       */
/* permite grabar nota en CERO para obligar nota de logro en promedio asignatura*/
/* recibe a�o, periodo, alumno, asignatura, logro, nota logro                   */ 
/********************************************************************************/
function grabar_notalogro($ano, $id_alumno, $id_asignatura, $id_logro, $periodo, $notalogro, $pendiente, $inactivar)
{
	$notasdet = "y".$ano."notasp".$periodo;
	$id_nota = 0;
	$sql="select id_nota, nota, pendiente, inactivar from $notasdet where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' ".
		"and id_logro = '$id_logro' and id_evidencia = '0' and id_actividad = '0' limit 1";
	$datnota=viewsql($sql);
	foreach($datnota as $regnota)
	{
		$id_nota = intval($regnota->id_nota);
		if ($notalogro==="X")
			$notalogro = $regnota->nota;
		if ($pendiente==="X")
			$pendiente = intval($regnota->pendiente);
		if ($inactivar==="X")
			$inactivar = intval($regnota->inactivar);
	}
	if ($id_nota > 0)
	{
		$sql = "UPDATE $notasdet SET nota = '$notalogro', pendiente = '$pendiente', inactivar = '$inactivar' WHERE id_nota = '$id_nota' LIMIT 1";
		execsql($sql);
	}
	else
	{
		$sql= "INSERT INTO $notasdet (id_alumno, id_asignatura, id_logro, id_actividad, nota, pendiente, inactivar) ".
			"VALUES ('$id_alumno', '$id_asignatura', '$id_logro', '0', '$notalogro', '$pendiente', '$inactivar')";
		execsql($sql);
	}
}

/********************************************************************************/
/* funci�n que graba nota total de la asignatura en el periodo o examen         */
/* permite grabar nota en CERO para permitir INACTIVAR  en promedio anual       */
/* recibe a�o, tipo periodo, periodo, alumno, asignatura, nota                  */ 
/********************************************************************************/
function grabar_notaasig($ano, $id_alumno, $id_asignatura, $pertipo, $periodo, $notatot, $pendiente, $inactivar)
{
	$notas = "y".$ano."notas";
	$id_nota = 0;
	$sql="select id_nota from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = '$pertipo' and periodo = '$periodo' limit 1";
	$datnota=viewsql($sql);
	foreach($datnota as $regnota)
	{
		$id_nota = intval($regnota->id_nota);
	}
	if ($id_nota > 0)
	{
		$sql = "UPDATE $notas SET nota = '$notatot', pendiente = '$pendiente', inactivar = '$inactivar' WHERE id_nota = '$id_nota' LIMIT 1";
		if ($pertipo=="E" and intval($notatot) <= 0) //borrar nota de examen si est� nula
			$sql = "delete from $notas WHERE id_nota = '$id_nota' LIMIT 1";
		execsql($sql);
	}
	else
	{
		if ($notatot > 0 or $pendiente > 0 or $inactivar > 0 or $pertipo == "P") //si tiene nota, o pendiente o inactiva o es periodo 
		{
			$sql= "INSERT INTO $notas (id_alumno,id_asignatura,pertipo,periodo,nota,pendiente,inactivar) ".
					"VALUES ('$id_alumno','$id_asignatura','$pertipo','$periodo','$notatot','$pendiente','$inactivar')";
			execsql($sql);
		}
	}
}

/********************************************************************************/
/* Funci�n que graba nota final acumulada de todos los periodos de la asignatura*/
/* Calcula nota final promediando periodos teniendo en cuenta ajuste por examen */
/* Recibe a�o, tipo periodo, periodo, alumno, asignatura, nota                  */ 
/********************************************************************************/
function grabar_notaasigfin($ano, $id_alumno, $id_asignatura)
{
	$notas = "y".$ano."notas";
	//traer notas verificando estado de periodos y examenes
	$sql="SELECT notanum, pertipo, periodo, perplan, pernomb, estado FROM periodos WHERE ano = '$ano' order by notanum";
	$datperi=viewsql($sql);
	$arrnotas = notas_asignatura($ano,$id_alumno,$id_asignatura,'9',$datperi);
	$notafin = $arrnotas[9];
	//grabar registro nota definitiva asignatura
	$id_nota = 0;
	$numnotas = 0;
	$sql="SELECT id_nota FROM $notas WHERE id_alumno = '$id_alumno' AND id_asignatura = '$id_asignatura' AND pertipo = 'F' ORDER BY id_nota";
	$datnota=viewsql($sql);
	foreach($datnota as $regnota)
	{
		if ($id_nota <= 0)
			$id_nota = intval($regnota->id_nota);
		$numnotas = $numnotas + 1;
	}
	if ($id_nota > 0)
	{
		$sql = "UPDATE $notas SET nota = '$notafin',notaEx='0' WHERE id_nota = '$id_nota' LIMIT 1";
		execsql($sql);
		if ($numnotas > 1)
		{
			$sql = "DELETE FROM $notas WHERE id_alumno = '$id_alumno' AND id_asignatura = '$id_asignatura' AND pertipo = 'F' AND id_nota != '$id_nota'";
			execsql($sql);
		}
	}
	else
	{
		if ($notafin > 0)
		{
			$sql= "INSERT INTO $notas (id_alumno, id_asignatura, pertipo, nota) ".
				"VALUES ('$id_alumno', '$id_asignatura', 'F', '$notafin')";
			execsql($sql);
		}
	}
}

/********************************************************************************/
/* Grabar nota del area en el periodo promediando asignaturas                   */
/* recibe a�o, grado, area, alumno y periodo                                    */ 
/********************************************************************************/
function grabar_notaarea($ano, $grado, $id_area, $id_alumno, $periodo)
{
	$notaarea = 0;
	$notaareaEx = 0;
	$notaareaExF = 0;
	$notas = "y".$ano."notas";
	$notasfa = "y".$ano."notasfa";
	//traer asignaturas del a�o, grado y area que requieren nota
	$sql="select id_asignatura, asignatura, porcentaje from asignaturas where ano = '$ano' and grado = '$grado' and id_area='$id_area' and notas = '1' order by id_asignatura";
	$datasig = viewsql($sql);
	$numasig = count($datasig);
	foreach($datasig as $regasig)
	{
		$id_asignatura = $regasig->id_asignatura;
		$porcentaje = intval($regasig->porcentaje);
		if ($porcentaje <= 0)
		{
			if ($numasig == 1)
				$porcentaje = 100;
			else
				$porcentaje = round(100/$numasig,3);
		}
		//traer nota asignatura del periodo para acumularla seg�n porcentaje
		$sql = "select nota, notaEx, notaExF, pendiente, inactivar from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = 'P' and periodo = '$periodo' limit 1";
		$datnot = viewsql($sql);
		foreach($datnot as $regnot)
		{
			if ($regnot->pendiente==0 and $regnot->inactivar==0)
			{
				$notapar = round($regnot->nota*($porcentaje/100),2);
				$notaarea = $notaarea + $notapar;
				if ($regnot->notaEx > 0)
					$notapar = round($regnot->notaEx*($porcentaje/100),2);
				if ($regnot->notaExF > 0)
					$notapar = round($regnot->notaExF*($porcentaje/100),2);
				$notaareaEx = $notaareaEx + $notapar;
				$notaareaExF = $notaareaExF + $notapar;
			}
		}
	}
	//nota final area
	if ($notaarea > 0)
	{
		$notaarea = round($notaarea,1);
		$notaareaEx = round($notaareaEx,1);
		$notaareaExF = round($notaareaExF,1);
		if ($notaareaEx === $notaarea)
			$notaareaEx = 0;
		if ($notaareaExF === $notaarea)
			$notaareaExF = 0;
	}
	$id_nota = 0;
	$sql="select id_nota from $notasfa where id_alumno = '$id_alumno' and id_area = '$id_area' and periodo = '$periodo' limit 1";
	$datnota=viewsql($sql);
	foreach($datnota as $regnota)
	{$id_nota = intval($regnota->id_nota);}
	if ($id_nota > 0)
	{
		$sql = "UPDATE $notasfa SET nota = '$notaarea', notaEx = '$notaareaEx', notaExF = '$notaareaExF' WHERE id_nota = '$id_nota' LIMIT 1";
		execsql($sql);
	}
	else
	{
		if ($notaarea > 0)
		{
			$sql= "INSERT INTO $notasfa (id_alumno, id_area, periodo, nota, notaEx, notaExF) ".
				"VALUES ('$id_alumno', '$id_area', '$periodo', '$notaarea', '$notaareaEx', '$notaareaExF')";
			execsql($sql);
		}
	}
}

/********************************************************************************/
/* Grabar nota final del area acumulada en todos los periodos                   */
/* Calcula nota final promediando periodos teniendo en cuenta ajuste por examen */
/* Recibe a�o, area, alumno                                                     */ 
/********************************************************************************/
function grabar_notaareafin($ano, $id_area, $id_alumno)
{
	$notasfa = "y".$ano."notasfa";
	//traer notas verificando estado de periodos y examenes
	$sql="SELECT notanum, pertipo, periodo, perplan, pernomb, estado FROM periodos WHERE ano = '$ano' order by notanum";
	$datperi=viewsql($sql);
	$arrnotas = notas_area($ano,$id_alumno,$id_area,'9',$datperi);
	$notafin = $arrnotas[9];
	//grabar registro nota definitiva
	$id_nota = 0;
	$sql="select id_nota from $notasfa where id_alumno = '$id_alumno' and id_area = '$id_area' and periodo = '0' limit 1";
	$datnota=viewsql($sql);
	foreach($datnota as $regnota)
	{$id_nota = intval($regnota->id_nota);}
	if ($id_nota > 0)
	{
		$sql = "UPDATE $notasfa SET nota = '$notafin' WHERE id_nota = '$id_nota' LIMIT 1";
		execsql($sql);
	}
	else
	{
		if ($notafin > 0)
		{
			$sql= "INSERT INTO $notasfa (id_alumno, id_area, periodo, nota) ".
				"VALUES ('$id_alumno', '$id_area', '0', '$notafin')";
			execsql($sql);
		}
	}
}

/****************************************************************************************/
/* Funci�n promedia actividades y evidencias para grabar nota total del logro en periodo*/
/* ajusta nota (con m�mina) del logro perdido en notaEx recuperado con examen semestral */ 
/* recibe a�o, alumno, logro, periodo, tipo promedio o tipo porcentaje                  */ 
/****************************************************************************************/
function promediar_logro($ano, $id_alumno, $id_logro, $periodo, $evideprom, $activprom)
{
	$notalogro = 0;
        $numevi = 0;
	$fileacti = "y".$ano."activ"; //tabla actividades
	$notasdet = "y".$ano."notasp".$periodo; //tabla notas actividades
	//notas limite a�o lectivo
	$sql = "select notalim, notamin, notamax from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{
            $notalim = intval($reganol->notalim);
            $notamin = intval($reganol->notamin);
            $notamax = intval($reganol->notamax);
        }
        //traer evidencias del logro
        $fileevi = "y".$ano."eviden";
        $sql = "SELECT id_asignatura, id_logro, id_evidencia, porcentaje FROM $fileevi WHERE id_logro = '$id_logro' AND periodo = '$periodo' ORDER BY id_logro, numero";
        $datevid = viewsql($sql);
        //traer actividades del logro
        $fileact = "y".$ano."activ";
        $sql = "SELECT id_evidencia, id_actividad, porcentaje FROM $fileact WHERE id_logro = '$id_logro' AND periodo = '$periodo' ORDER BY id_evidencia, numero";
        $datacti = viewsql($sql);
        //evidencias logro
	foreach($datevid as $regevi)
	{
            $numevi = $numevi + 1;
            $id_asignatura = $regevi->id_asignatura;
            $id_evidencia = $regevi->id_evidencia;
            $porcentevi = $regevi->porcentaje;
            //actividades por evidencia
            $notaevid = 0;
            $numact = 0;
            foreach($datacti as $regacti)
            {
                if ($regacti->id_evidencia == $id_evidencia)
                {
                    $numact = $numact + 1;
                    $id_actividad = $regacti->id_actividad;
                    $porcentaje = $regacti->porcentaje;
                    //traer nota actividad
                    $nota = 0;
                    $sql="select nota from $notasdet where id_alumno = '$id_alumno' and id_logro = '$id_logro' and id_evidencia = '$id_evidencia' and id_actividad = '$id_actividad' LIMIT 1";
                    $datnota=viewsql($sql);
                    foreach($datnota as $regnota)
                    {$nota = intval($regnota->nota);}
                    if ($nota > 0)
                    {
                        $notapar = $nota;
                        if ($activprom <= 0 and $porcentaje > 0)
                            $notapar = round($notapar*($porcentaje/100),1);
                        $notaevid = $notaevid + $notapar;		
                     }
                }
            } 
            if ($activprom > 0 and $numact > 0) {
               $notaevid = round($notaevid/$numact,1); }
            //actualizar nota de la evidencia
            grabar_notaevide($ano, $id_alumno, $id_asignatura, $id_logro, $id_evidencia, $periodo, $notaevid);   
            //acumular evidencia en nota del logro  
            $notapar = $notaevid;
            if ($evideprom <= 0 and $porcentevi > 0) {
               $notapar = round($notapar*($porcentevi/100),1);}
            $notalogro = $notalogro + $notapar;		
        }
        if ($evideprom > 0 and $numevi > 0) {
            $notalogro = $notalogro/$numevi;
        }
        if ($notalogro > 0) {
            $notalogro = intval(round($notalogro,1)); //parte entera del redondeo a 1 decimal
            if ($notalogro > $notamax) {
                $notalogro = $notamax;
            }
	}
	return($notalogro);
}

/****************************************************************************************/
/* Promedia logros para grabar nota total asignatura en el periodo                      */
/* Graba una nota promedio sin notas de examen y otra nota con logros recuperados examen*/ 
/* Recibe a�o, alumno, asignatura, periodo, tipo educacion                              */ 
/****************************************************************************************/
function promediar_asignatura($ano, $id_alumno, $id_asignatura, $periodo, $tipoedu)
{
	$notas = "y".$ano."notas";
	$notasdet = "y".$ano."notasp".$periodo;
	$file = "y".$ano."logros";
	$pertipo = "P";
	$totnotas = 0; //nota asignatura
	$totnotasEx = 0; //nota asignatura ajustada examenes semestrales
	$totnotasExF = 0; //nota asignatura ajustada examenes finales
	$notatot = 0;
	$notatotEx = 0;
	$notatotExF = 0;
	$ajustes = 0;
	$numEx = 0;
	//notas limite a�o lectivo
	$sql = "select notalim, notamin, notamax from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{
            $notalim = intval($reganol->notalim);
            $notamin = intval($reganol->notamin);
            $notamax = intval($reganol->notamax);
        }
	//traer notas de logros de la asignatura en el periodo (incluye notas en CERO para obligar que exista nota del logro)
	$sql="select id_nota, nota, notaEx, numEx, pendiente, inactivar, id_logro, id_asignatura from $notasdet where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_evidencia='0' and id_actividad = '0' order by id_logro";
	$datnota=viewsql($sql);
        //traer logros de la asignatura en el periodo
	$sql = "SELECT id_logro FROM $file WHERE id_asignatura = '$id_asignatura' AND periodos LIKE '%$periodo%' AND tipoedu = '$tipoedu' ORDER BY id_logro";
	$datlogr=viewsql($sql);
	$numlogros = 0;
	foreach($datlogr as $reglogr)
	{
		$id_logro = $reglogr->id_logro;
		$numlogros = $numlogros + 1;
		foreach($datnota as $regnota)
		{
			if ($regnota->id_logro == $id_logro)
			{
				if ($regnota->inactivar == 0 and $regnota->pendiente == 0) //toma logros no inactivos ni pendientes para el promedio
				{
					$nota = intval($regnota->nota);
					$notaEx = intval($regnota->notaEx);
					$numEx = $regnota->numEx;
					$totnotas = $totnotas + $nota;
                                        if ($nota >= $notalim and $notaEx > 0) //blanquear examen si pasa logros del periodo
					{
						$notaEx = 0;
						$id_nota = $regnota->id_nota;
						$sql = "UPDATE $notasdet SET notaEx = '0', numEx = '0' WHERE id_nota = '$id_nota' LIMIT 1";
						execsql($sql);
					}
					if ($notaEx > 0) //nota logro ajustada por examen
					{
						if ($numEx <= 2) //examenes semestrales
							$totnotasEx = $totnotasEx + $notaEx;
						else //examenes finales
							$totnotasExF = $totnotasExF + $notaEx;
					}
					else //nota normal del periodo
					{
						$totnotasEx = $totnotasEx + $nota;
						$totnotasExF = $totnotasExF + $nota;
					}
				}
				else
					$numlogros = $numlogros - 1;
				break;
			}
		}
	}
	if ($numlogros > 0)
	{
		$notatot = round($totnotas/$numlogros,1);
                if($notatot > $notamax)
                    $notatot = $notamax;
		if ($totnotasEx > 0) //si existen logros ajustados por examen semestral se calcula otro promedio
			$notatotEx = round($totnotasEx/$numlogros,1);
		if ($totnotasExF > 0) //si existen logros ajustados por examen final se calcula otro promedio
			$notatotExF = round($totnotasExF/$numlogros,1);
	}
	if ($notatotEx < $notalim)
		$notatotEx = 0;
	if ($notatotExF < $notalim)
		$notatotExF = 0;
	$id_nota = 0;
	$numnotas = 0;
	$sql="SELECT id_nota, nota FROM $notas WHERE id_alumno = '$id_alumno' AND id_asignatura = '$id_asignatura' AND pertipo = 'P' AND periodo = '$periodo' ORDER BY id_nota";
	$datnota=viewsql($sql);
	foreach($datnota as $regnota)
	{
		if ($id_nota <= 0) //selecciona primer registro de nota (aveces se repitieron)
		{
			$id_nota = intval($regnota->id_nota);
			if ($numlogros <= 0) //carga nota digitada a la asignatura cuando no tiene logros
				$notatot = $regnota->nota;
		}
		$numnotas = $numnotas + 1;
	}
	if ($id_nota > 0)
	{
		$sql = "UPDATE $notas SET nota = '$notatot', notaEx = '$notatotEx', notaExF = '$notatotExF' WHERE id_nota = '$id_nota' LIMIT 1";
		execsql($sql);
		if ($numnotas > 1) //corregir registros eliminando notas repetidas del periodo
		{
			$sql = "DELETE FROM $notas WHERE id_alumno = '$id_alumno' AND id_asignatura = '$id_asignatura' AND pertipo = 'P' AND periodo = '$periodo' AND id_nota != '$id_nota'";
			execsql($sql);
		}
	}
	else
	{
		if ($notatot > 0)
		{
			$sql= "INSERT INTO $notas (id_alumno, id_asignatura, pertipo, periodo, nota, notaEx, notaExF, pendiente, inactivar) ".
					"VALUES ('$id_alumno', '$id_asignatura', '$pertipo', '$periodo', '$notatot', '$notatotEx', '$notatotExF', '0', '0')";
			execsql($sql);
		}
	}
	return($notatot);
}

/********************************************************************************/
/* Funci�n promedia todas las areas del alumno que requieren notas en el periodo*/
/* Recibe alumno, a�o, grado, periodo                                           */ 
/********************************************************************************/
function promediar_areas($id_alumno, $ano, $grado, $periodo)
{
	$sql="SELECT id_area FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' group by id_area ORDER BY id_area";
	$datarea = viewsql($sql);
	foreach($datarea as $regarea)
	{
		$id_area = $regarea->id_area;
		grabar_notaarea($ano, $grado, $id_area, $id_alumno, $periodo);
	}
}

/********************************************************************************/
/* Funci�n promedia todas las areas del alumno que requieren notas en el periodo*/
/* Recibe alumno, a�o, grado, periodo                                           */ 
/********************************************************************************/
function totalizar_areas($id_alumno, $ano, $grado, $curso, $periodo)
{
	//lista alumnos
	$cursos = "y".$ano."cursos";
	$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' ORDER BY id_alumno";
	if ($curso != "")
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' and curso = '$curso' ORDER BY id_alumno";
	if ($id_alumno > 0)
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE id_alumno = '$id_alumno' LIMIT 1";
	$datalum=viewsql($sql);
	//lista areas
	$sql="SELECT id_area FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' group by id_area ORDER BY id_area";
	$datarea = viewsql($sql);
	foreach($datarea as $regarea)
	{
		$id_area = $regarea->id_area;
		foreach($datalum as $regalum)
		{
			$id_alumno = $regalum->id_alumno;
			grabar_notaarea($ano, $grado, $id_area, $id_alumno, 1);
			grabar_notaarea($ano, $grado, $id_area, $id_alumno, 2);
			if ($periodo >= 2)
			{
				grabar_notaarea($ano, $grado, $id_area, $id_alumno, 3);
				grabar_notaarea($ano, $grado, $id_area, $id_alumno, 4);
			}
		}
	}
}

/********************************************************************************/
/* Funci�n que calcula promedio del alumno de todas las �reas para el periodo   */
/* Recibe alumno, a�o, grado, periodo                                           */ 
/********************************************************************************/
function promediar_periodo($id_alumno, $ano, $grado, $periodo)
{
	$sumnotas = 0; //suma de areas promedio alumno
	$numnotas = 0; //numero de areas promedio alumno
	$notasfa = "y".$ano."notasfa";
	//areas que afectan promedio del alumno
	$sql="SELECT id_area FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' and promedio = '1' group by id_area ORDER BY id_area";
	$datarea = viewsql($sql);
	foreach($datarea as $regarea)
	{
		$id_area = $regarea->id_area;
		$notafin = 0;
		//nota de area del periodo sin tener en cuenta examenes
		$sql="select nota from $notasfa where id_alumno = '$id_alumno' and id_area = '$id_area' and periodo = '$periodo' limit 1";
		$datnot=viewsql($sql);
		foreach($datnot as $regnot)
		{
			$notafin = $regnot->nota;
		}
		$sumnotas = $sumnotas + $notafin;
		$numnotas = $numnotas + 1;
	}
	$promedio = 0;
	if ($numnotas > 0)
		$promedio = round($sumnotas/$numnotas,2);
	$campoprom = "promedio".$periodo;
	$cursos = "y".$ano."cursos";
	$sql = "UPDATE $cursos SET $campoprom = '$promedio' WHERE id_alumno = '$id_alumno' LIMIT 1";
	execsql($sql);
}

/********************************************************************************/
/* funci�n que calcula puesto del alumno en el curso seg�n promedio del periodo */
/* recibe a�o, periodo, grado, curso                                            */ 
/********************************************************************************/
function puestos($ano, $periodo, $grado, $curso)
{
	$cursos = "y".$ano."cursos";
	$campoprom = "promedio";
	$campopues = "puesto";
	if ($periodo <= 4)
	{
		$campoprom.=$periodo;
		$campopues.=$periodo;
	}
	$sql = "SELECT id_curso, $campoprom as prom FROM $cursos WHERE grado = '$grado' AND curso = '$curso' AND $campoprom > '0' ORDER BY prom DESC";
	$datcurs = viewsql($sql);
	$puesto = 0;
	$promedio = 200;
	foreach($datcurs as $regcurs)
	{
		if ($regcurs->prom < $promedio)
		{
			$puesto = $puesto + 1;
			$promedio = $regcurs->prom;
		}
		$id_curso = $regcurs->id_curso;
		$sql = "UPDATE $cursos SET $campopues = '$puesto' WHERE id_curso = '$id_curso' LIMIT 1";
		execsql($sql);
	}
}

/*********************************************************************************************/
/* Recalcula promedios de logros, asignaturas, areas, promedio y puesto para un periodo      */
/* Recalcula por grado, o curso, o alumno                                                    */
/* Recibe a�o, periodo, grado, curso y alumno (curso y alumno pueden venir en blanco)        */ 
/*********************************************************************************************/
function promediar_notas($ano, $periodo, $grado, $curso, $id_alumno)
{
	$cursos = "y".$ano."cursos";
	$logros = "y".$ano."logros";
	//alumnos del grado
	$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' ORDER BY id_alumno";
	if ($curso != "")
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' and curso = '$curso' ORDER BY id_alumno";
	if ($id_alumno > 0)
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE id_alumno = '$id_alumno' LIMIT 1";
	$datalum=viewsql($sql);
	//notas limite a�o lectivo
	$sql = "select notalim, notamin, notamax from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{
            $notalim = intval($reganol->notalim);
            $notamin = intval($reganol->notamin);
            $notamax = intval($reganol->notamax);
        }
	//traer asignaturas del grado
	$sql="SELECT id_area, id_asignatura, evideprom, activprom FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' ORDER BY id_area, id_asignatura";
	$datasig=viewsql($sql);
	foreach($datasig as $regasig)
	{
		$id_asignatura = $regasig->id_asignatura;
		$evideprom =  $regasig->evideprom;
		$activprom =  $regasig->activprom;
		//traer logros del periodo
		$sql = "SELECT id_logro, tipoedu FROM $logros WHERE id_asignatura = '$id_asignatura' AND periodos LIKE '% $periodo %' ORDER BY id_logro";
		$datlogr=viewsql($sql);
		$numlogr = count($datlogr);
		//procesar alumnos
		foreach($datalum as $regalum)
		{
			$id_alumno = $regalum->id_alumno;
			$tipoedu = $regalum->tipoedu;
			if ($numlogr > 0)
			{
				foreach($datlogr as $reglogr)
				{
					$id_logro = $reglogr->id_logro;
					if ($reglogr->tipoedu == $tipoedu)
					{
						$notalogro = promediar_logro($ano, $id_alumno, $id_logro, $periodo, $evideprom, $activprom);
						grabar_notalogro($ano, $id_alumno, $id_asignatura, $id_logro, $periodo, $notalogro, 'X', 'X');
                                        }
				}		
				$notaasig = promediar_asignatura($ano, $id_alumno, $id_asignatura, $periodo, $tipoedu);
			}
		}
	}
	//definitivas areas del periodo
	$id_area = 0;
	foreach($datasig as $regasig)
	{
		if ($id_area!=$regasig->id_area)
		{
			$id_area = $regasig->id_area;
			foreach($datalum as $regalum)
			{
				$id_alumno = $regalum->id_alumno;
				grabar_notaarea($ano, $grado, $id_area, $id_alumno, $periodo);
			}
		}
	}
	//promedio general del periodo por alumno
	foreach($datalum as $regalum)
	{
		$id_alumno = $regalum->id_alumno;
		promediar_periodo($id_alumno, $ano, $grado, $periodo);
	}
	//grabar puestos por curso y cerrar planillas
	if ($curso == "")
	{
		$sql = "SELECT curso FROM cursos WHERE ano = '$ano' AND grado = '$grado' ORDER BY curso";
		$datcurs = viewsql($sql);
		foreach($datcurs as $regcurs)
		{
			$curso = $regcurs->curso;
			puestos($ano, $periodo, $grado, $curso);
		}
	}
	else
	{
		puestos($ano, $periodo, $grado, $curso);
	}
}

/*********************************************************************************************/
/* Ajusta notas de logros y notas asignaturas de periodos seg�n tipo de examen               */
/* Recibe a�o, periodo, grado, curso, alumno                                                 */ 
/*********************************************************************************************/
function promediar_examen($ano, $periodo, $grado, $curso, $id_alumno)
{
	$cursos = "y".$ano."cursos";
	$notas = "y".$ano."notas";
	$logros = "y".$ano."logros";
	//asignaturas del grado que requieren notas
	$sql="SELECT id_area, id_asignatura FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' ORDER BY id_area, id_asignatura";
	$datasig=viewsql($sql);
	//alumnos del grado
	$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' ORDER BY id_alumno";
	if ($curso!="")
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' and curso = '$curso' ORDER BY id_alumno";
	if ($id_alumno > 0)
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE id_alumno = '$id_alumno' limit 1";
	$datalum=viewsql($sql);
	foreach($datalum as $regalum)
	{
		$id_alumno = $regalum->id_alumno;
		$tipoedu = $regalum->tipoedu;
		//ajustar logros y promediar asignaturas alumno
		foreach($datasig as $regasig)
		{
			$id_asignatura = $regasig->id_asignatura;
			//traer nota examen del periodo
			$examen = 0;
			$sql = "select nota from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = 'E' and periodo = '$periodo' LIMIT 1";
			$datexa = viewsql($sql);
			foreach($datexa as $regexa)
			{$examen = $regexa->nota;}
			//ajustar logros perdidos en los periodos segun nota examen
			ajustar_logros($ano, $periodo, $id_asignatura, $id_alumno, $tipoedu, $examen);
			//promediar asignaturas periodos que cubre examen
			$pini = 1;
			$pfin = 4;
			if (intval($periodo)==1) //semestral 1
				$pfin = 2;
			if (intval($periodo)==2) //semestral 2
				$pini = 3;
			for($i=$pini; $i<=$pfin ;$i++) //periodos de notas
			{
				$notanw = promediar_asignatura($ano, $id_alumno, $id_asignatura, $i, $tipoedu);
			}
		}
		//promediar areas del alumno
		$id_area = 0;
		foreach($datasig as $regasig)
		{
			if ($regasig->id_area != $id_area)
			{
				$id_area = $regasig->id_area;
				//ajustar notas de area
				if ($periodo!=2) // si examen semestral 1, final o superacion
				{
					grabar_notaarea($ano, $grado, $id_area, $id_alumno, 1);
					grabar_notaarea($ano, $grado, $id_area, $id_alumno, 2);
				}
				if ($periodo!=1) //si examen semestral 2, final o superacion
				{
					grabar_notaarea($ano, $grado, $id_area, $id_alumno, 3);
					grabar_notaarea($ano, $grado, $id_area, $id_alumno, 4);
				}
			}
		}
	} //fin alumnos
}

/***********************************************************************************************/
/* Ajusta notas de logros si examen aprobado con la nota limite, graba en otro campo           */
/* Examen Semestral 1 (periodos 1-2), Semestral 2 (periodos 3-4), Final y Superacion (1-2-3-4) */
/* Recibe a�o, periodo, asignatura, alumno, nota examen                                        */ 
/***********************************************************************************************/
function ajustar_logros($ano, $periodo, $id_asignatura, $id_alumno, $tipoedu, $examen)
{
	$notas = "y".$ano."notas";
	$pini = 1;
	$pfin = 4;
	if (intval($periodo)==1) //semestral 1
		$pfin = 2;
	if (intval($periodo)==2) //semestral 2
		$pini = 3;
	//notas limite a�o lectivo
	$sql = "select notalim, notamin, notamax from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{
            $notalim = intval($reganol->notalim);
            $notamin = intval($reganol->notamin);
            $notamax = intval($reganol->notamax);
        }
	//ajusta notas periodos de los logros perdidos
	for($i=$pini; $i<=$pfin ;$i++) //periodos de notas
	{
		$notasdet = "y".$ano."notasp".$i;
		$sql = "SELECT id_nota, notaEx, numEx FROM $notasdet WHERE id_alumno = '$id_alumno' AND id_asignatura = '$id_asignatura' AND id_evidencia = '0' AND id_actividad = '0' AND nota < '$notalim' ORDER BY id_nota ";
		$datnota = viewsql($sql);
		foreach($datnota as $regnota)
		{
			$id_nota = intval($regnota->id_nota);
			$notaEx = intval($regnota->notaEx);
			$numEx = intval($regnota->numEx);
			$sql = "";
			//ajustar notas logros perdidos recuperados con examen semestral o final
			if ($examen >= $notalim)
				$sql = "UPDATE $notasdet SET notaEx = '$notalim', numEx = '$periodo' WHERE id_nota = '$id_nota' LIMIT 1";
			else //blanquear notas recuperadas en examen
			{
				$sql = "UPDATE $notasdet SET notaEx = '0', numEx = '0' WHERE id_nota = '$id_nota' LIMIT 1";
				if (intval($periodo) >= 3 and $notaEx > 0 and $numEx <= 2) //si examen final o de superacion y habia recuperado antes
				{$sql="";}
			}
			if ($sql!="")
			{
				execsql($sql);
			}
		}
	} //fin periodos
}

/*********************************************************************************************/
/* Funci�n que recalcula notas finales de asignaturas y areas despues de examen final        */
/* Recalcula por grado, o curso, o alumno                                                    */
/* Recibe a�o, periodo, grado, curso y alumno (curso y alumno pueden venir en blanco)        */ 
/*********************************************************************************************/
function promediar_finales($ano, $grado, $curso, $id_alumno)
{
	$cursos = "y".$ano."cursos";
	//alumnos del grado
	$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' ORDER BY id_alumno";
	if ($curso != "")
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' and curso = '$curso' ORDER BY id_alumno";
	if ($id_alumno > 0)
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE id_alumno = '$id_alumno' LIMIT 1";
	$datalum=viewsql($sql);
	//traer asignaturas del grado
	$sql="SELECT id_asignatura, activprom FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' ORDER BY id_area, id_asignatura";
	$datasig=viewsql($sql);
	foreach($datasig as $regasig)
	{
		$id_asignatura = $regasig->id_asignatura;
		$activprom =  $regasig->activprom;
		foreach($datalum as $regalum)
		{
			$id_alumno = $regalum->id_alumno;
			$tipoedu = $regalum->tipoedu;
			grabar_notaasigfin($ano, $id_alumno, $id_asignatura);
		}
	}
	//areas del grado
	$sql="SELECT id_area FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' group by id_area ORDER BY id_area";
	$datarea = viewsql($sql);
	foreach($datarea as $regarea)
	{
		$id_area = $regarea->id_area;
		foreach($datalum as $regalum)
		{
			$id_alumno = $regalum->id_alumno;
			grabar_notaareafin($ano, $id_area, $id_alumno);
		}
	}
}

/********************************************************************************/
/* Cierre a�o lectivo: Promedio final del alumno, perdida de areas y de a�o     */
/* recibe a�o, grado, curso y alumno                                            */ 
/********************************************************************************/
function promediar_anolect($ano, $grado, $curso, $id_alumno)
{
	//notas limite a�o lectivo
	$sql = "select * from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{
		$notamin = $reganol->notamin;
		$notamax = $reganol->notamax;
		$notalim = $reganol->notalim;
		$areasper = $reganol->areasper;
	}
	//periodos academicos
	$sql="SELECT notanum, pertipo, periodo, perplan, pernomb, estado FROM periodos WHERE ano = '$ano' order by notanum";
	$datperi=viewsql($sql);
	//verificar perdida de a�o del grado
	$perdida = 1;
	$sql="select perdida from grados where grado = '$grado' limit 1";
	$datgrado = viewsql($sql);
	foreach($datgrado as $reggrado)
	{$perdida = $reggrado->perdida;}
	//lista de areas que inciden en perdida de a�o
	$cursos = "y".$ano."cursos";
	$notasfa = "y".$ano."notasfa";
	$anoant = intval($ano) - 1;
	$cursosant = "y".$anoant."cursos";
	$sql="SELECT id_area, promedio, perdida FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' and notas > '0' GROUP BY id_area ORDER BY id_area";
	$datarea = viewsql($sql);
	//alumnos del grado
	$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' ORDER BY id_alumno";
	if ($curso != "")
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE grado = '$grado' and curso = '$curso' ORDER BY id_alumno";
	if ($id_alumno > 0)
		$sql="SELECT id_alumno, tipoedu FROM $cursos WHERE id_alumno = '$id_alumno' LIMIT 1";
	$datalum=viewsql($sql);
	foreach($datalum as $regalum)
	{
		$id_alumno = $regalum->id_alumno;
		$tipoedu = $regalum->tipoedu;
		$numareas = 0; //numero de areas perdidas del alumno
		$id_areaper = 0; //area perdida uno
		$pierde = 0;
		foreach($datarea as $regarea)
		{
			$id_area = $regarea->id_area;
			$arrnotas = notas_area($ano,$id_alumno,$id_area,'9',$datperi);
			if ($arrnotas[9] > 0)
			{
				if ($arrnotas[9] < $notalim and $regarea->perdida > 0) //area afecta perdida del a�o
				{
					if ($id_areaper <= 0)
						$id_areaper = $id_area;
					$numareas = $numareas + 1;
				}
			}
		}
		//pierde a�o si el area perdida es la misma del a�o anterior cuando puede perder a�o en ese grado
		if ($numareas==1 and $perdida > 0)
		{
			$areacod = "";
			$sql="SELECT areacod FROM areas WHERE id_area = '$id_areaper' limit 1";
			$datcod = viewsql($sql);
			foreach($datcod as $regcod)
			{$areacod = $regcod->areacod;}
			$sql="SELECT $cursosant.id_areaper, areas.areacod FROM $cursosant, areas WHERE $cursosant.id_alumno = '$id_alumno' and $cursosant.id_areaper = areas.id_area limit 1";
			$datmatr = viewsql($sql);
			foreach($datmatr as $regmatr)
			{
				if ($regmatr->areacod==$areacod)
				{
					$pierde = 1;
					$id_areaper = 0;
				}
			}
		}
		//pierde a�o si pierde mas areas de lo permitido cuando puede perder a�o en ese grado
		if ($numareas >= $areasper and $perdida > 0)
		{
			$pierde = 1;
			$id_areaper = 0;
		}
		//promedio final Alumno con promedios de los 4 periodos (notas definitivas sin examenes)
		$numprom = 0;
		$sumaprom = 0;
		$promedio = 0;
		$final = 0;
		$sql="SELECT promedio1, promedio2, promedio3, promedio4 FROM $cursos WHERE id_alumno = '$id_alumno' limit 1";
		$datcurs = viewsql($sql);
		foreach($datcurs as $regcurs)
		{
			if ($regcurs->promedio1 > 0)
			{
				$numprom = $numprom + 1;
				$sumaprom = $sumaprom + $regcurs->promedio1;
			}
			if ($regcurs->promedio2 > 0)
			{
				$numprom = $numprom + 1;
				$sumaprom = $sumaprom + $regcurs->promedio2;
			}
			if ($regcurs->promedio3 > 0)
			{
				$numprom = $numprom + 1;
				$sumaprom = $sumaprom + $regcurs->promedio3;
			}
			if ($regcurs->promedio4 > 0)
			{
				$numprom = $numprom + 1;
				$sumaprom = $sumaprom + $regcurs->promedio4;
				$final = 1;
			}
		}
		if ($numprom > 0)
			$promedio = round($sumaprom/$numprom,2);
		if ($final > 0) //existe promedio final periodo 4
		{ 
			$sql = "UPDATE $cursos SET promedio = '$promedio', areasper = '$numareas', id_areaper = '$id_areaper', pierde = '$pierde' WHERE id_alumno = '$id_alumno' LIMIT 1";
			execsql($sql);
		}
	}
}
?>