<?php
/************************************************************************************************
* @file        : grabar_asignaturas.php                                                         *
* @brief       : Actualizar tablas de areas, materias, asignaturas, logros, actividades         *
* @version     : 1.0                                                                            *
* @author      : Nora Rodriguez 21-feb-2012                                                     *
* @Modificado  : Nora Rodriguez 23-feb-2012                                                     *
************************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
include("database.php");
include("../webparam.php");
include("../libs/fechas.php");
connect();
$fecha_hora = date('Y-m-d') . " ". date('H:i:s');
$op = isset($_POST['op']) ? $_POST['op'] : null ;
if ($op == "")
	$op = isset($_GET['op']) ? $_GET['op'] : null ;

//grabar area academica
if ($op == "grabararea")
{
	$id_area = $_POST["id_area"];
	$area = strtoupper($_POST["area"]);
	$areacod = $_POST["areacod"];
	$grado = $_POST["grado"];
	$inactivar = intval($_POST["inactivar"]);
	//verificar que no se duplique
	$error = "";
	$sql="select id_area from areas where grado = '$grado' and area = '$area' and id_area != '$id_area' limit 1";
	$datarea = viewsql($sql);
	if (count($datarea) > 0 )
		$error = "Area ya existente para el grado ...";
	if ($error == "")
	{
		if ($id_area > 0)
		{
			$sql="UPDATE areas SET area='$area', inactivar='$inactivar' WHERE id_area = '$id_area' limit 1";
			execsql($sql);
			//actualizar asignaturas a�o vigente
			$oldarea = "";
			foreach($datarea as $regarea)
			{$oldarea = $regarea->area;}
			if ($oldarea != $area)
			{
				$ano = $_SESSION["anoaca"];
				$sql="UPDATE asignaturas SET area='$area', areacod='$areacod' WHERE ano = '$ano' and id_area = '$id_area'";
				execsql($sql);
			}
		}
		else
		{
			$sql= "INSERT INTO areas (area, areacod, grado, inactivar) VALUES ('$area','$areacod','$grado','$inactivar')";
			execsql($sql);
		}
		//grabar codigo unico de areas
		if ($areacod=="")
		{
			$codigo = 0;
			$area = "";
			$sql="SELECT id_area, area FROM areas ORDER BY area";
			$datarea=viewsql($sql);
			foreach($datarea as $regarea)
			{
				$id_area = $regarea->id_area;
				$nwarea = trim($regarea->area);
				if ($nwarea != $area)
				{	
					$area = $nwarea;
					$codigo = $codigo +1;
				}
				$sql="UPDATE areas SET areacod='$codigo', area = '$nwarea' WHERE id_area = '$id_area' limit 1";
				execsql($sql);
			}
		}
		disconnect();
		echo"<script>window.location.href='../asignaturas_areas.php?grado=$grado';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../asignaturas_areas.php?opcion=editar&id_area=$id_area&grado=$grado';</script>";
	}
}

//borrar area acad�mica
if ($op == "borrararea")
{
	$id_area = $_POST["id_area"];
	$grado = $_POST["grado"];
	//verificar que no tenga asignaturas
	$sql="SELECT id_asignatura FROM asignaturas WHERE id_area = '$id_area' limit 1";
	$datasig=viewsql($sql);
	if (count($datasig)==0)
	{
		$sql = "delete from areas where id_area = '$id_area' limit 1";
		execsql($sql);
		disconnect();
		echo"<script>window.location.href='../asignaturas_areas.php?grado=$grado';</script>";
	}
	else
	{
		disconnect();
		echo"<script>alert('Imposible eliminar Area, tiene asignaturas registradas!'); window.location.href='../asignaturas_areas.php?opcion=editar&id_area=$id_area&grado=$grado';</script>";
	}
}

//grabar grado
if ($op == "grabargrado")
{
	$id_grado = $_POST["id_grado"];
	$perdida = intval($_POST["perdida"]);
	$superacion = intval($_POST["superacion"]);
	$inactivar = intval($_POST["inactivar"]);
	//verificar que no se duplique
	$error = "";
	$sql="select grado from grados where grado = '".strtoupper($_POST["grado"])."' and id_grado != '$id_grado' limit 1";
	$datgrad = viewsql($sql);
	if (count($datgrad) > 0 )
		$error = "Grado ya existente ...";
	if ($error == "")
	{
		if ($id_grado > 0)
		{
			$sql="UPDATE grados SET educacion='".$_POST["educacion"]."', gradocolor='".strtoupper($_POST["gradocolor"])."', gradofont='".strtoupper($_POST["gradofont"])."', gradosig='".$_POST["gradosig"]."', perdida='$perdida', superacion='$superacion', inactivar='$inactivar' WHERE id_grado = '$id_grado' limit 1";
			execsql($sql);
			//actualizar colores asignaturas a�o vigente
			if (strtoupper($_POST["gradocolor"])!=$_POST["oldgradocolor"] or strtoupper($_POST["gradofont"])!=$_POST["oldgradofont"])
			{
				$ano = $_SESSION["anoaca"];
				$sql="UPDATE asignaturas SET gradocolor='".strtoupper($_POST["gradocolor"])."', gradofont='".strtoupper($_POST["gradofont"])."' WHERE ano = '$ano' and grado = '".strtoupper($_POST["grado"])."'";
				execsql($sql);
			}
		}
		else
		{
			$sql= "INSERT INTO grados (grado, gradonom, gradosig, nivel, educacion, gradocolor, gradofont, perdida, superacion) VALUES ('".strtoupper($_POST["grado"])."','".strtoupper($_POST["gradonom"])."','".strtoupper($_POST["gradosig"])."','".$_POST["nivel"]."','".$_POST["educacion"]."','".strtoupper($_POST["gradocolor"])."','".strtoupper($_POST["gradofont"])."','$perdida','$superacion')";
			execsql($sql);
		}
		disconnect();
		echo"<script> alert('Grado Grabado.'); window.location.href='../asignaturas_grados.php';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../asignaturas_grados.php?id_grado=$id_grado';</script>";
	}
}

//grabar asignatura por a�o y grado
if ($op == "grabarasignatura")
{
	$ano = $_POST["ano"];
	$grado = $_POST["grado"];
	$id_asignatura = $_POST["id_asignatura"];
	$id_area = $_POST["id_area"];
	$asignatura = trim(strtoupper($_POST["asignatura"]));
	$evidemin = intval($_POST["evidemin"]);
	$activmin = intval($_POST["activmin"]);
	$chkbol = isset($_POST['chkbol']) ? $_POST['chkbol'] : null ;
	$chknot = isset($_POST['chknot']) ? $_POST['chknot'] : null ;
	$chkper = isset($_POST['chkper']) ? $_POST['chkper'] : null ;
	$chkpro = isset($_POST['chkpro']) ? $_POST['chkpro'] : null ;
	$chkobs = isset($_POST['chkobs']) ? $_POST['chkobs'] : null ;
	$area = "";
	$boletin = 0;
	$notas = 0;
	$perdida = 0;
	$promedio = 0;
	$observacion = 0;
	if ($chkbol=="on")
		$boletin = 1;
	if ($chknot=="on")
		$notas = 1;
	if ($chkper=="on")
		$perdida = 1;
	if ($chkpro=="on")
		$promedio = 1;
	if ($chkobs=="on")
		$observacion = 1;
	//concatenar porcentajes evidencias
	$evidepor = "";
	if (intval($_POST["evideprom"] <= 0))
	{
		for($i=1; $i<=$evidemin ;$i++)
		{
			$namepor = "porevi".$i;
			$evipor = intval($_POST[$namepor]);
			if ($evipor < 100)
				$evipor = $evipor." ";
			$evidepor = $evidepor.$evipor;
		}
	}
	//concatenar porcentajes actividades
	$activpor = "";
	if (intval($_POST["activprom"] <= 0))
	{
		for($i=1; $i<=$activmin ;$i++)
		{
			$namepor = "por".$i;
			$actpor = intval($_POST[$namepor]);
			if ($actpor < 100)
				$actpor = $actpor." ";
			$activpor = $activpor.$actpor;
		}
	}
	//traer area, lista dinamica solo trae el id
	$error = "";
	$sql="select * from areas where id_area = '$id_area' limit 1";
	$datarea = viewsql($sql);
	foreach($datarea as $regarea)
	{
		$area = $regarea->area;
		if ($regarea->inactivar == 1)
			$error = "Area Inactiva ...";
	}
	//verificar que no se duplique la asignatura
	if ($error == "")
	{	
		$sql="select id_asignatura from asignaturas where ano = '$ano' and grado = '$grado' and id_area = '$id_area' and asignatura = '$asignatura' and id_asignatura != '$id_asignatura' limit 1";
		$datasig = viewsql($sql);
		if (count($datasig) > 0 )
			$error = "Asignatura para ese grado ya existente ...";
	}
	if ($error == "")
	{
		if ($asignatura == "")
			$asignatura = $area;
		if ($id_asignatura > 0)
		{
			$sql="UPDATE asignaturas SET id_area='$id_area', area='$area', asignatura='$asignatura', intensidad='".$_POST["intensidad"]."', porcentaje='".intval($_POST["porcentaje"])."', evidemin='$evidemin', evideprom='".$_POST["evideprom"]."', evidepor='$evidepor', activmin='$activmin', activprom='".$_POST["activprom"]."', activpor='$activpor', notaQ='".strtoupper($_POST["notaQ"])."', codigo='".$_POST["codigo"]."', ".
			"boletin='$boletin', notas='$notas', perdida='$perdida', promedio='$promedio', observacion='$observacion', fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' ".
			"WHERE id_asignatura = '$id_asignatura' limit 1";
			execsql($sql);
		}
		else
		{
			$sql= "INSERT INTO asignaturas (ano, grado, asignatura, id_area, area, intensidad, porcentaje, evidemin, evideprom, evidepor, activmin, activprom, activpor, notaQ, codigo, boletin, notas, perdida, promedio, observacion, gradocolor, gradofont, fchcrea, usucrea) ".
			"VALUES ('$ano', '$grado', '$asignatura', '$id_area', '$area', '".$_POST["intensidad"]."', '".intval($_POST["porcentaje"])."', '$evidemin', '".$_POST["evideprom"]."', '$evidepor', '$activmin', '".$_POST["activprom"]."', '$activpor', '".strtoupper($_POST["notaQ"])."', '".$_POST["codigo"]."', '$boletin', '$notas', '$perdida', '$promedio', '$observacion', '".$_POST["gradocolor"]."', '".$_POST["gradofont"]."', ".
			"'$fecha_hora', '".$_SESSION["usulog"]."')";
			execsql($sql);
		} 
		disconnect();
		echo"<script> window.close(); window.opener.document.location.reload();</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../asignaturas_edit.php?id_asignatura=$id_asignatura';</script>";
	}
}

//borrar asignatura por a�o y grado
if ($op == "borrarasignatura")
{
	$ano = $_POST["ano"];
	$id_asignatura = $_POST["id_asignatura"];
	//verificar que no tenga logros
	$file = "y".$_POST["ano"]."logros";
	$sql="SELECT * FROM $file WHERE id_asignatura='$id_asignatura' limit 1";
	$datlogr=viewsql($sql);
	//verificar que no tenga notas
	$file = "y".$_POST["ano"]."notas";
	$sql="SELECT * FROM $file WHERE id_asignatura = '$id_asignatura' AND (nota > '0' OR pendiente > '0' OR inactivar > '0') LIMIT 1";
	$datnota=viewsql($sql);
	if (count($datlogr)==0 and count($datnota)==0)
	{
		$sql = "delete from asignaturas where id_asignatura = '$id_asignatura' limit 1";
		execsql($sql);
		$sql="DELETE FROM $file WHERE id_asignatura = '$id_asignatura' AND nota = '0'";
		execsql($sql);
		disconnect();
		echo"<script> alert('Asignatura Eliminada.'); window.close(); window.opener.document.location.reload();</script>";
	}
	else
	{
		disconnect();
		echo"<script>alert('Imposible eliminar Asignatura, tiene logros y/o notas registradas!'); window.location.href='../asignaturas_edit.php?id_asignatura=$id_asignatura';</script>";
	}
}

//grabar logro
if ($op == "grabarlogro")
{
	$file = "y".$_POST["ano"]."logros";
	$id_logro = $_POST["id_logro"];
	$id_asignatura = $_POST["id_asignatura"];
	$evidemin = intval($_POST["evidemin"]);
	$activmin = intval($_POST["activmin"]);
	//concatenar porcentajes evidencias
	$evidepor = "";
	for($i=1; $i<=$evidemin ;$i++)
	{
		$namepor = "porevi".$i;
		$evipor = isset($_POST[$namepor]) ? $_POST[$namepor] : null ;
		if ($evipor < 100)
			$evipor = $evipor." ";
		$evidepor = $evidepor.$evipor;
	}
	//concatenar porcentajes actividades
	$activpor = "";
	for($i=1; $i<=$activmin ;$i++)
	{
		$namepor = "por".$i;
		$actpor = isset($_POST[$namepor]) ? $_POST[$namepor] : null ;
		if ($actpor < 100)
			$actpor = $actpor." ";
		$activpor = $activpor.$actpor;
	}
	//concatenar periodos validando el estado de los periodos
	$oldperiodos = "*".$_POST["oldperiodos"];
	$periodos = " ";
	$swnombre = 1; //no permite modificar nombre ni numero de logro si pertenece a algun periodo cerrado
	$sql="SELECT periodo, estado FROM periodos WHERE ano = '".$_POST["ano"]."' and pertipo = 'P' order by periodo";
	$datperi = viewsql($sql);
	foreach($datperi as $regperi)
	{
		$i = $regperi->periodo;
		$crear = 0;
		$antes = 0;
		if (strpos($oldperiodos,$i) > 0)	
			$antes = 1;
		$namep = "chkp".$i;
		$chkp = isset($_POST[$namep]) ? $_POST[$namep] : null ;
		if ($chkp=="X")
		{
			//a�adir periodo si existia antes o si no esta cerrado
			if ($antes==1 or $regperi->estado!="CERRADO")
				$crear = 1;
		}
		else
		{
			//no eliminar si periodo esta cerrado y existia antes
			if ($antes==1 and $regperi->estado=="CERRADO")
				$crear = 1;
		}
		if ($crear==1)
		{
			$periodos = $periodos.$i." ";
			if ($regperi->estado=="CERRADO")
				$swnombre = 0;
		} 
	}
        //inactivar en cursos del grado
        $cursos = " ";
        $sql = "select curso from cursos where ano = '".$_POST["ano"]."' and grado = '".$_POST["grado"]."' order by curso"; 
        $datcurs = viewsql($sql);
        foreach($datcurs as $regcurs)
        {
           $curso = $regcurs->curso;
           $chkcurso = isset($_POST[$curso]) ? $_POST[$curso] : null ;
            if ($chkcurso=="X")
            {
                $cursos = $cursos.$curso." ";
                if ($id_logro > 0)
                {
                    $filecursos = "y".$_POST["ano"]."cursos";
                    $sql="SELECT id_alumno FROM $filecursos WHERE grado = '".$_POST["grado"]."' AND curso = '$curso' ";
                    $datalum=viewsql($sql);
                    foreach($datalum as $regalum)
                    {
                        $id_alumno = $regalum->id_alumno;
                        for($i=1; $i<5 ;$i++)
                        {
                            if (strpos($periodos,$i) > 0)
                            {
                                $notasdet = "y".$ano."notasp".$i;
                                $sql = "UPDATE $notasdet SET inactivar = '1' WHERE id_alumno = '$id_alumno' and id_logro = '$id_logro' and id_actividad = '0' LIMIT 1";
                                execsql($sql);
                            }
                        }
                    }
                }
            }
        }
        //nuevo logro
	if ($id_logro > 0)
	{
		$sql="UPDATE $file SET periodos='$periodos', cursos='$cursos', evidemin='$evidemin', evidepor='$evidepor', activmin='$activmin', activpor='$activpor', ";
		if ($swnombre==1)
			$sql.="logrocod='".$_POST["logrocod"]."', tipoedu='".$_POST["tipoedu"]."', logro='".trim(strtoupper($_POST["logro"]))."', logrosi='".trim(strtoupper($_POST["logrosi"]))."', logrono='".trim(strtoupper($_POST["logrono"]))."', ";
		$sql.="fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' WHERE id_logro = '$id_logro' limit 1";
		execsql($sql);
	}
	else
	{
		$sql= "INSERT INTO $file (grado, id_asignatura, logrocod, tipoedu, periodos, logro, logrosi, logrono, evidemin, evidepor, activmin, activpor, fchcrea, usucrea) ".
		"VALUES ('".$_POST["grado"]."', '".$_POST["id_asignatura"]."', '".$_POST["logrocod"]."', '".$_POST["tipoedu"]."', ".
		"'$periodos', '".trim(strtoupper($_POST["logro"]))."', '".trim(strtoupper($_POST["logrosi"]))."', '".trim(strtoupper($_POST["logrono"]))."', '$evidemin', '$evidepor', '$activmin', '$activpor', ".
		"'$fecha_hora', '".$_SESSION["usulog"]."')";
		execsql($sql);
	}
	disconnect();
	echo"<script>window.location.href='../asignaturas_logros.php?id_asignatura=$id_asignatura';</script>";
}

//borrar notas del logro
if ($op == "borrarlogro")
{
	$id_logro = $_POST["id_logro"];
	$id_asignatura = $_POST["id_asignatura"];
	//borrar notas del logro en distintos periodos
	for($i=1; $i <= 4 ; $i++)
	{
		$file = "y".$_POST["ano"]."notasp".$i;
		$sql="DELETE FROM $file WHERE id_logro = '$id_logro'";
		execsql($sql);
	}
	disconnect();
	echo"<script>alert('Se han eliminado notas del logro.'); window.location.href='../asignaturas_logros.php?id_asignatura=$id_asignatura';</script>";
}

//eliminar logro
if ($op == "eliminarlogro")
{
	$id_logro = $_POST["id_logro"];
	$id_asignatura = $_POST["id_asignatura"];
	//verificar que no tenga notas
	$numnotas = 0;
	for($i=1; $i <= 4 ; $i++)
	{
		$file = "y".$_POST["ano"]."notasp".$i;
		$sql="SELECT id_nota FROM $file WHERE id_logro = '$id_logro' limit 1";
		$datnota=viewsql($sql);
		$numnotas = $numnotas + count($datnota);
	}
	if ($numnotas==0)
	{
            //eliminar actividades del logro
            $file = "y".$_POST["ano"]."activ";
            $sql="DELETE FROM $file WHERE id_logro = '$id_logro'";
            execsql($sql);
            //eliminar metodologias del logro
            $file = "y".$_POST["ano"]."metod";
            $sql="DELETE FROM $file WHERE id_logro = '$id_logro'";
            execsql($sql);
            //eliminar logro
            $file = "y".$_POST["ano"]."logros";
            $sql = "delete from $file where id_logro = '$id_logro' limit 1";
            execsql($sql);
            disconnect();
            echo"<script>alert('Se ha eliminado el logro sus actividades y metodolog�as.'); window.location.href='../asignaturas_logros.php?id_asignatura=$id_asignatura';</script>";
	}
	else
	{
            $error = "Imposible eliminar logro, tiene notas registradas o pendientes o inactivas!";
            disconnect();
            echo"<script>alert('$error'); window.location.href='../asignaturas_logros.php?id_asignatura=$id_asignatura';</script>";
	}
}

//grabar evidencias del logro por periodo
if ($op == "grabarevidencias")
{
	$fileevi = "y".$_POST["ano"]."eviden";
	$fileact = "y".$_POST["ano"]."activ";
	$id_asignatura = $_POST["id_asignatura"];
	$id_logro = $_POST["id_logro"];
	$periodo = $_POST["periodo"];
	$evidemin = intval($_POST["evidemin"]);
	$evideprom = intval($_POST["evideprom"]);
        $activmin = intval($_POST["activmin"]);
	$activprom = intval($_POST["activprom"]);
        $j = 0;
        for($i=1; $i<=$evidemin ;$i++)
        {
            $nameid = "idevi".$i;
            $namenum = "numevi".$i;
            $namecod = "codevi".$i;
            $namedes = "desevi".$i;
            $namepor = "porevi".$i;
            $nametot = "totalact".$i;
            $id_evidencia = intval($_POST[$nameid]);
            $numero = intval($_POST[$namenum]);
            if ($numero <= 0) {
                $numero = $i;
            }
            $codigo = strtoupper($_POST[$namecod]);
            $evidencia = strtoupper($_POST[$namedes]);
            $porcentaje = intval($_POST[$namepor]);
            //grabar evidencia
            if ($codigo != "" and $evidencia != "")
            {
		if ($id_evidencia > 0)
		{
                    $sql="UPDATE $fileevi SET codigo='$codigo', periodo='$periodo', evidencia='$evidencia', porcentaje='$porcentaje', numero='$numero', ".
                    "fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' ".
                    "WHERE id_evidencia = '$id_evidencia' limit 1";
                    execsql($sql);
		}
		else
		{
                    $sql= "INSERT INTO $fileevi (id_asignatura, id_logro, periodo, evidencia, porcentaje, codigo, numero, fchcrea, usucrea) ".
                    "VALUES ('$id_asignatura', '$id_logro', '$periodo', '$evidencia', '$porcentaje', '$codigo', '$numero', ".
                    "'$fecha_hora', '".$_SESSION["usulog"]."')";
                    execsql($sql);
                    $sql = "SELECT id_evidencia FROM $fileevi WHERE id_asignatura = '$id_asignatura' AND id_logro = '$id_logro' and periodo = '$periodo' and numero = '$numero' LIMIT 1";
                    $datevide=viewsql($sql);
                    foreach($datevide as $regevide)
                    {
                        $id_evidencia = $regevide->id_evidencia;
                    }                    
		}
            }
            //grabar actividades evidencia
            for($k=1; $k<=$activmin ;$k++)
            {
                $j = $j + 1;
                $nameid = "idact".$i.$j;
                $namenum = "numact".$i.$j;
                $namecod = "codact".$i.$j;
                $namedes = "desact".$i.$j;
                $namepor = "poract".$i.$j;
		$id_actividad = intval($_POST[$nameid]);
		$numero = intval($_POST[$namenum]);
		if ($numero <= 0)
			$numero = $k;
		$codigo = strtoupper($_POST[$namecod]);
		$actividad = strtoupper($_POST[$namedes]);
		$porcentaje = intval($_POST[$namepor]);
		//grabar actividad
		if ($id_evidencia > 0 and $codigo != "" and $actividad != "")
		{
			if ($id_actividad > 0)
			{
				$sql="UPDATE $fileact SET id_evidencia='$id_evidencia', codigo='$codigo', periodo='$periodo', actividad='$actividad', porcentaje='$porcentaje', numero='$numero', ".
				"fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' ".
				"WHERE id_actividad = '$id_actividad' limit 1";
				execsql($sql);
			}
			else
			{
				$sql= "INSERT INTO $fileact (id_asignatura, id_logro, id_evidencia, periodo, actividad, porcentaje, codigo, numero, fchcrea, usucrea) ".
				"VALUES ('$id_asignatura', '$id_logro', '$id_evidencia', '$periodo', '$actividad', '$porcentaje', '$codigo', '$numero', ".
				"'$fecha_hora', '".$_SESSION["usulog"]."')";
				execsql($sql);
                                
			}
		}
            }
        }
	disconnect();
	echo"<script>window.location.href='../asignaturas_logros.php?id_asignatura=$id_asignatura';</script>";
}

//borrar evidencias y actividades del logro por periodo
if ($op == "borrarevidencias")
{
	$id_logro = $_POST["id_logro"];
	$id_asignatura = $_POST["id_asignatura"];
	$periodo = $_POST["periodo"];
        //traer evidencias del periodo
        $file = "y".$_POST["ano"]."eviden";
        $sql="SELECT id_evidencia FROM $file WHERE id_asignatura = '$id_asignatura' and id_logro = '$id_logro' and periodo = '$periodo' order by numero";
        $datevide=viewsql($sql);
        foreach($datevide as $regevide)
        {
            $id_evidencia = $regevide->id_evidencia;
            $notasp = "y".$_POST["ano"]."notasp".$periodo;
            $sql="select id_nota from $notasp ".
            "where id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
            "and id_evidencia = '$id_evidencia' and id_actividad = '0' and nota > '0'";
            $datnota=viewsql($sql);
            if (count($datnota) <= 0)
            {
                //BORRAR EVIDENCIAS
		$sql = "delete from $file where id_asignatura = '$id_asignatura' and id_logro = '$id_logro' and periodo = '$periodo'";
		execsql($sql);
                //BORRAR ACTIVIDADES
		$file = "y".$_POST["ano"]."activ";
		$sql = "delete from $file where id_asignatura = '$id_asignatura' and id_logro = '$id_logro' and id_evidencia = '$id_evidencia' and periodo = '$periodo'";
		execsql($sql);
                //BORRAR NOTAS EN CERO POR SI QUEDARON
		$sql="DELETE FROM $notasp WHERE id_asignatura = '$id_asignatura' AND id_logro = '$id_logro' and id_evidencia = '$id_evidencia' AND nota = '0'";
		execsql($sql);
            }
        }
	disconnect();
	echo"<script>alert('Para el logro se eliminaron las evidencias y actividades SIN NOTA periodo $periodo !'); window.location.href='../asignaturas_evide.php?id_asignatura=$id_asignatura&id_logro=$id_logro&periodo=$periodo';</script>";
}

//grabar actividades del logro por periodo
if ($op == "grabaractividades")
{
	$file = "y".$_POST["ano"]."activ";
	$periodo = $_POST["periodo"];
	$id_asignatura = $_POST["id_asignatura"];
	$id_logro = $_POST["id_logro"];
	$activmin = intval($_POST["activmin"]);
	$activprom = intval($_POST["activprom"]);
	for($i=1; $i <= $activmin ; $i++)
	{
		$nameid = "id".$i;
		$namenum = "numero".$i;
		$namecod = "codigo".$i;
		$nameact = "actividad".$i;
		$namepor = "porcentaje".$i;
		$id_actividad = intval($_POST[$nameid]);
		$numero = intval($_POST[$namenum]);
		if ($numero <= 0)
			$numero = $i;
		$codigo = strtoupper($_POST[$namecod]);
		$actividad = strtoupper($_POST[$nameact]);
		$porcentaje = intval($_POST[$namepor]);
		//borrar actividad si superior al minimo actividades sin porcentaje
		if ($activprom <= 0 and $porcentaje <= 0 and $id_actividad > 0 and $i > $activmin)
		{
			$numnotas = 0;
			for($i=1; $i <= 4 ; $i++)
			{
				$file = "y".$_POST["ano"]."notasp".$i;
				$sql="SELECT id_nota FROM $file WHERE id_actividad = '$id_actividad' limit 1";
				$datnota=viewsql($sql);
				$numnotas = $numnotas + count($datnota);
			}
			if ($numnotas==0)
			{
				$file = "y".$_POST["ano"]."activ";
				$sql = "delete from $file where id_actividad = '$id_actividad' limit 1";
				execsql($sql);
			}
		}
		//grabar actividad
		if ($codigo != "" and $actividad != "")
		{
			if ($id_actividad > 0)
			{
				$sql="UPDATE $file SET codigo='$codigo', periodo='$periodo', actividad='$actividad', porcentaje='$porcentaje', numero='$numero', ".
				"fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' ".
				"WHERE id_actividad = '$id_actividad' limit 1";
				execsql($sql);
			}
			else
			{
				$sql= "INSERT INTO $file (id_asignatura, id_logro, periodo, actividad, porcentaje, codigo, numero, fchcrea, usucrea) ".
				"VALUES ('$id_asignatura', '$id_logro', '$periodo', '$actividad', '$porcentaje', '$codigo', '$numero', ".
				"'$fecha_hora', '".$_SESSION["usulog"]."')";
				execsql($sql);
			}
		}
	}
	disconnect();
	echo"<script>window.location.href='../asignaturas_logros.php?id_asignatura=$id_asignatura';</script>";
}

//borrar actividades del logro por periodo
if ($op == "borraractividades")
{
	$id_logro = $_POST["id_logro"];
	$id_asignatura = $_POST["id_asignatura"];
	$periodo = $_POST["periodo"];
	//verificar que no existan notas de actividades para el logro en el periodo
	$notasp = "y".$_POST["ano"]."notasp".$periodo;
	$sql="SELECT id_nota FROM $notasp WHERE id_logro = '$id_logro' and id_actividad > '0' and nota > '0' limit 1";
	$datnota=viewsql($sql);
	if (count($datnota) <= 0)
	{
		$file = "y".$_POST["ano"]."activ";
		$sql = "delete from $file where id_asignatura = '$id_asignatura' and id_logro = '$id_logro' and periodo = '$periodo'";
		execsql($sql);
		$sql="DELETE FROM $notasp WHERE id_asignatura = '$id_asignatura' AND id_logro = '$id_logro' AND nota = '0'";
		execsql($sql);
		disconnect();
		echo"<script>window.location.href='../asignaturas_logros.php?id_asignatura=$id_asignatura';</script>";
	}
	else
	{
		disconnect();
		echo"<script>alert('Imposible eliminar las actividades, tiene notas registradas en el periodo $periodo !'); window.location.href='../asignaturas_activ.php?id_asignatura=$id_asignatura&id_logro=$id_logro&periodo=$periodo';</script>";
	}
}

//grabar metodologias del logro
if ($op == "grabarmetodologias")
{
	$file = "y".$_POST["ano"]."metod";
	$id_asignatura = $_POST["id_asignatura"];
	$id_logro = $_POST["id_logro"];
	$numero = $_POST["numero"];
	for($i=1; $i <= $numero ; $i++)
	{
		$nameid = "id".$i;
		$namedia = "dia".$i;
		$namemes = "mes".$i;
		$namecla = "clases".$i;
		$namemet = "metodologia".$i;
		$namerec = "recursos".$i;
		$id_metodologia = intval($_POST[$nameid]);
		$metodologia = strtoupper($_POST[$namemet]);
		$recursos = strtoupper($_POST[$namerec]);
		$numclases = intval($_POST[$namecla]);
		$fecha = "";
		if ($_POST[$namemes]!="" and $_POST[$namedia]!="")
			$fecha = $_POST["ano"]."-".$_POST[$namemes]."-".$_POST[$namedia];
		if ($metodologia != "")
		{
			if ($id_metodologia > 0)
			{
				$sql="UPDATE $file SET metodologia='$metodologia', fecha='$fecha', numclases='$numclases', recursos='$recursos', ".
				"fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' ".
				"WHERE id_metodologia = '$id_metodologia' limit 1";
				execsql($sql);
			}
			else
			{
				$sql= "INSERT INTO $file (id_asignatura, id_logro, fecha, metodologia, numclases, recursos, fchcrea, usucrea) ".
				"VALUES ('$id_asignatura', '$id_logro', '$fecha', '$metodologia', '$numclases', '$recursos', ".
				"'$fecha_hora', '".$_SESSION["usulog"]."')";
				execsql($sql);
			}
		}
	}
	disconnect();
	echo"<script>window.location.href='../asignaturas_logros.php?id_asignatura=$id_asignatura';</script>";
}

//borrar registro de metodologia del logro
if ($op == "borrarmetodologia")
{
	$id_logro = $_GET["id_logro"];
	$id_asignatura = $_GET["id_asignatura"];
	$id_metodologia = $_GET["id_metodologia"];
	$file = "y".$_GET["ano"]."metod";
	$sql = "delete from $file where id_metodologia = '$id_metodologia' limit 1";
	execsql($sql);
	disconnect();
	echo"<script>window.location.href='../asignaturas_metod.php?id_asignatura=$id_asignatura&id_logro=$id_logro';</script>";
}

//grabar observacion
if ($op == "grabarobservacion")
{
	$id_observacion = intval($_POST["id_observacion"]);
	$id_categoria = intval($_POST["id_categoria"]);
	$observacion = strtoupper($_POST["observacion"]);
	$codigo = intval($_POST["codigo"]);
	$director = intval($_POST["director"]);
	$inactivar = intval($_POST["inactivar"]);
	$nivel = $_POST["nivel"];
	//verificar que no se duplique
	$error = "";
	$sql="select id_observacion from observaciones where observacion = '$observacion' and id_observacion != '$id_observacion' limit 1";
	$datobse = viewsql($sql);
	if (count($datobse) > 0 )
		$error = "Observaci�n ya existente ...";
	if ($error == "")
	{
		$niveles = " ";
		$sql= "select nivel, nombre from niveles order by id_nivel";
		$datnive=viewsql($sql);
		foreach($datnive as $regnive)
		{
			$niv = $regnive->nivel;
			$name = "nivel".$niv;
			$chkniv = isset($_POST[$name]) ? $_POST[$name] : null ;
			if ($chkniv=="on")
				$niveles = $niveles.$niv." ";
		}
		if ($id_observacion > 0)
			$sql="UPDATE observaciones SET id_categoria='$id_categoria', observacion='$observacion', codigo='$codigo', niveles='$niveles', director='$director', inactivar='$inactivar', fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' WHERE id_observacion = '$id_observacion' limit 1";
		else
			$sql= "INSERT INTO observaciones (id_categoria, observacion, codigo, niveles, director, inactivar, fchcrea, usucrea) VALUES ('$id_categoria','$observacion','$codigo','$niveles','$director','$inactivar','$fecha_hora', '".$_SESSION["usulog"]."' )";
		execsql($sql);
		disconnect();
		echo"<script>window.location.href='../asignaturas_observa.php?nivel=$nivel';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../asignaturas_observa.php?id_observacion=$id_observacion&nivel=$nivel';</script>";
	}
}

//borrar observacion
if ($op == "borrarobservacion")
{
	$id_observacion = $_POST["id_observacion"];
	$nivel = $_POST["nivel"];
	//verificar que no tenga registros de observaciones en ningun a�o
	$numobse = 0;
	$sql="SELECT ano FROM anoslect ORDER BY ano desc";
	$datanol=viewsql($sql);
	foreach($datanol as $reganol)
	{
		$ano = $reganol->ano;
		$file = "y".$i."observa";
		$sql="SELECT id_observacion FROM $file WHERE id_observacion = '$id_observacion' limit 1";
		$datobse = viewsql($sql);
		$numobse = $numobse + count($datobse);
	}
	if ($numobse==0)
	{
		$sql = "delete from observaciones where id_observacion = '$id_observacion' limit 1";
		execsql($sql);
		disconnect();
		echo"<script>window.location.href='../asignaturas_observa.php?nivel=$nivel';</script>";
	}
	else
	{
		disconnect();
		echo"<script>alert('Imposible eliminar observaci�n, tiene registros en planillas!'); window.location.href='../asignaturas_observa.php?nivel=$nivel';</script>";
	}
}

//grabar profesor asignatura
if ($op == "grabarprofasig")
{
	$ano = $_POST["ano"];
	$id_profasig = $_POST["id_profasig"];
	$id_asignatura = $_POST["id_asignatura"];
	$grado = $_POST["grado"];
	$curso = trim($_POST["curso"]);
	$oldcurso = trim($_POST["oldcurso"]);
	//verificar que no se duplique el curso, asignatura y tipo de educacion
	$file = "y".$_POST["ano"]."profasig";
	$sql="select curso from $file where id_asignatura = '$id_asignatura' and curso = '$curso' and id_profasig != '$id_profasig' limit 1";
	$datprof = viewsql($sql);
	$error = "";	
	if (count($datprof) > 0 )
		$error = "Asignatura y Curso ya existente...";
	if ($error == "")
	{
		$pos=strpos($_POST["id_profesor"],"-");
		$profesor = substr($_POST["id_profesor"],$pos+1);
		$id_profesor = substr($_POST["id_profesor"],0,$pos);
		if ($id_profasig > 0)
		{
			$sql="UPDATE $file SET id_profesor='$id_profesor', profesor = '$profesor' WHERE id_profasig = '$id_profasig' limit 1";
			execsql($sql);
		}
		else
		{
			$sql= "INSERT INTO $file (id_asignatura, id_profesor, profesor, grado, curso) ".
			"VALUES ('$id_asignatura', '$id_profesor', '$profesor', '$grado', '$curso')";
			execsql($sql);
		}
		//si cambio de curso del profesor liberar horario del curso para la asignatura
		if ($curso != $oldcurso and $oldcurso!="")
		{
			$sql="SELECT id_horario FROM horarios WHERE ano='$ano' and grado='$grado' and curso='$oldcurso' and id_asignatura='$id_asignatura' ORDER BY id_horario";
			$dathora=viewsql($sql);
			foreach($dathora as $reghora)
			{
				$id_horario = $reghora->id_horario;
				$sql="UPDATE horarios SET id_profesor = '0' WHERE id_horario = '$id_horario' LIMIT 1";
				execsql($sql);
			}
		}
		disconnect();
		echo"<script>window.location.href='../asignaturas_profe.php?id_asignatura=$id_asignatura';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../asignaturas_profe.php?id_asignatura=$id_asignatura';</script>";
	}
}

//borrar profesor asignatura
if ($op == "borrarprofasig")
{
	$ano = $_POST["ano"];
	$file = "y".$_POST["ano"]."profasig";
	$id_profasig = $_POST["id_profasig"];
	$id_asignatura = $_POST["id_asignatura"];
	$grado = $_POST["grado"];
	$oldcurso = trim($_POST["oldcurso"]);
	$sql = "delete from $file where id_profasig = '$id_profasig' limit 1";
	execsql($sql);
	//eliminar profesor, liberar horario del curso para la asignatura
	$sql="SELECT id_horario FROM horarios WHERE ano='$ano' and grado='$grado' and curso='$oldcurso' and id_asignatura='$id_asignatura' ORDER BY id_horario";
	$dathora=viewsql($sql);
	foreach($dathora as $reghora)
	{
		$id_horario = $reghora->id_horario;
		$sql="UPDATE horarios SET id_profesor = '0' WHERE id_horario = '$id_horario' LIMIT 1";
		execsql($sql);
	}
	disconnect();
	echo"<script>window.location.href='../asignaturas_profe.php?id_asignatura=$id_asignatura';</script>";
}

//grabar carga academica a otro docente
if ($op == "grabarcarga")
{
	$ano = $_POST["ano"];
	$id_profasig = $_POST["id_profasig"];
	$pos=strpos($_POST["nw_profesor"],"-");
	$profesor = substr($_POST["nw_profesor"],$pos+1);
	$id_profesor = substr($_POST["nw_profesor"],0,$pos);
	//actualizar carga asignaturas
	$file = "y".$_POST["ano"]."profasig";
	$sql="UPDATE $file SET id_profesor='$id_profesor', profesor = '$profesor' WHERE id_profesor = '$id_profasig'";
	execsql($sql);
	//actualizar planillas
	$file = "y".$_POST["ano"]."planillas";
	$sql="UPDATE $file SET id_profesor='$id_profesor' WHERE id_profesor = '$id_profasig'";
	execsql($sql);
	//actualizar horarios
	$sql="UPDATE horarios SET id_profesor='$id_profesor' WHERE id_profesor = '$id_profasig' and ano = '$ano'";
	execsql($sql);
	disconnect();
	echo"<script>window.location.href='../profesores_asignat.php?id_profesor=$id_profasig';</script>";
}

//grabar horario por grado y dia para todos los cursos
if ($op == "grabarhorario")
{
	$ano = $_POST["ano"];
	$grado = $_POST["grado"];
	$nivel = $_POST["nivel"];
	$numdia = $_POST["numdia"];
	$cursosel = $_POST["cursosel"];
	//intervalos del nivel
	$sql = "SELECT * FROM horariosint where nivel = '$nivel' order by intervalo";
	$datinte = viewsql($sql);
	//cursos del grado
	$sql = "select curso from cursos where ano = '$ano' and grado = '$grado' order by curso"; 
	$datcurs = viewsql($sql);
	foreach($datcurs as $regcurs)
	{
		$curso = $regcurs->curso;
		foreach($datinte as $reginte)
		{
			$intervalo = $reginte->intervalo;
			$name = "CURSO".$curso."I".$intervalo;
			if (trim($reginte->tipo)=="") //no descanso ni menu
			{
				$id_profesor = trim($_POST[$name]);
				$pos = strpos($id_profesor,"-");
				$id_asignatura = intval(substr($id_profesor,$pos+1));
				$id_profesor = intval(substr($id_profesor,0,$pos));
				if ($id_asignatura > 0) //profesor puede estar sin asignar
				{
					$id_horario = 0;
					$sql="select id_horario from horarios where ano = '$ano' and dia = '$numdia' and intervalo = '$intervalo' and grado = '$grado' and curso = '$curso' limit 1";
					$dathora = viewsql($sql);
					foreach($dathora as $reghora)
					{$id_horario = intval($reghora->id_horario);}
					if ($id_horario <= 0)
						$sql= "INSERT INTO horarios (ano,dia,intervalo,grado,curso,id_profesor,id_asignatura) VALUES ('$ano','$numdia','$intervalo','$grado','$curso','$id_profesor','$id_asignatura')";
					else
						$sql = "UPDATE horarios SET id_profesor='$id_profesor',id_asignatura='$id_asignatura' where id_horario = '$id_horario' limit 1";
					execsql($sql);
				}					
			}
		}
	}
	$sigdia = $numdia + 1;
	if ($sigdia >= 6)
		$sigdia = 1;
	echo"<script>alert('Horario Dia $numdia grabado.'); window.location.href='../horarios_dias.php?ano=$ano&grado=$grado&numdia=$sigdia&cursosel=$cursosel';</script>";
}

//borrar horario por grado y curso
if ($op == "borrarhorario")
{
	$ano = $_GET["ano"];
	$grado = $_GET["grado"];
	$curso = $_GET["curso"];
	$sql = "delete from horarios where ano = '$ano' and grado = '$grado' and curso = '$curso'";
	execsql($sql);
	echo"<script>alert('Horario $curso Eliminado.'); window.location.href='../horarios.php?ano=$ano&grado=$grado&curso=$curso';</script>";
}

//Grabar valores anuales por grados
if ($op == "grabar_valores")
{
	$ano = $_POST["ano"];
	$mensaje = "Valores a�o ".$ano." grabados...";
	$sql="SELECT gradosval.matricula, gradosval.pension, gradosval.transporte, grados.* FROM gradosval, grados WHERE gradosval.ano = '$ano' and gradosval.grado = grados.grado order by grados.id_grado";
	$datgrado=viewsql($sql);
	foreach($datgrado as $reggrado)
	{
		$id_grado = $reggrado->id_grado;
		$grado = $reggrado->grado;
		$valmat = "valmat".$id_grado;
		$valpen = "valpen".$id_grado;
		$valtra = "valtra".$id_grado;
		$valfin = "valfin".$id_grado;
		$valret = "valret".$id_grado;
		$sql = "UPDATE gradosval SET matricula='".$_POST[$valmat]."',pension='".$_POST[$valpen]."',pensionfin='".$_POST[$valfin]."',pensionret='".$_POST[$valret]."',transporte='".$_POST[$valtra]."' WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
		execsql($sql);
		if ($_POST[$valmat] > 0 and $_POST[$valpen] > 0)
		{
			$sql = "UPDATE matriculas SET matricula='".$_POST[$valmat]."',pension02='".$_POST[$valpen]."',pension03='".$_POST[$valpen]."',pension04='".$_POST[$valpen]."',pension05='".$_POST[$valpen]."',pension06='".$_POST[$valpen]."',pension07='".$_POST[$valpen]."',pension08='".$_POST[$valpen]."',pension09='".$_POST[$valpen]."',pension10='".$_POST[$valpen]."',pension11='".$_POST[$valpen]."' WHERE ano = '$ano' AND grado = '$grado' AND matricula = '0' AND pension06 = '0' ";
			execsql($sql);
		}
		if ($_POST[$valtra] > 0)
		{
			$sql = "UPDATE transporte SET transp02='".$_POST[$valtra]."',transp03='".$_POST[$valtra]."',transp04='".$_POST[$valtra]."',transp05='".$_POST[$valtra]."',transp06='".$_POST[$valtra]."',transp07='".$_POST[$valtra]."',transp08='".$_POST[$valtra]."',transp09='".$_POST[$valtra]."',transp10='".$_POST[$valtra]."',transp11='".$_POST[$valtra]."' WHERE ano = '$ano' AND grado = '$grado' AND transp02 = '0' AND transp06 = '0' AND transp11 = '0' ";
			execsql($sql);
		}
	}
	if (count($datgrado) <= 0)
	{
		$mensaje = "A�o ".$ano." iniciado para registrar valores...";
		$sql="SELECT grado FROM grados ORDER BY id_grado";
		$datgrado=viewsql($sql);
		foreach($datgrado as $reggrado)
		{
			$grado = $reggrado->grado;
			$sql= "INSERT INTO gradosval (ano, grado, matricula, pension, transporte) VALUES ('$ano','$grado','0','0','0')";
			execsql($sql);
		}
	}
	echo"<script>alert('$mensaje');window.location.href='../asignaturas_grados.php?opcion=valores&ano=$ano';</script>";
}

?>