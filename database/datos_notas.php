<?php
/********************************************************************************/
/* Funci�n que retorna nota cualitativa corta de nota num�rica                  */
/* recibe arreglo con datos tabla de puntajes y la nota num�rica a evaluar      */ 
/********************************************************************************/
function cualitativa($datpunt,$notaval)
{
	$notaQ = "";
	if ($notaval > 0)
	{
		foreach($datpunt as $regpunt)
		{
			if (intval($notaval) >= intval($regpunt->notamin) and intval($notaval) <= intval($regpunt->notamax))
			{
				$notaQ = $regpunt->notaQ;
				break;
			}
		}
	}
	return ($notaQ);
}

/********************************************************************************/
/* Funci�n que retorna nota cualitativa larga (detallada) de nota num�rica      */
/* recibe arreglo con datos tabla de puntajes y la nota num�rica a evaluar      */ 
/********************************************************************************/
function conceptual($datpunt,$notaval)
{
	$notaD = "";
	if ($notaval > 0)
	{
		foreach($datpunt as $regpunt)
		{
			if (intval($notaval) >= intval($regpunt->notamin) and intval($notaval) <= intval($regpunt->notamax))
			{
				$notaD = $regpunt->notadet;
				break;
			}
		}
	}
	return ($notaD);
}

/***********************************************************************************/
/* Funci�n que retorna arreglo de notas de asignatura de todo el a�o               */
/* recibe variables a�o, id_alumno, id_asignatura, periodo corte, arreglo periodos */ 
/***********************************************************************************/
Function notas_asignatura($ano,$id_alumno,$id_asignatura,$corte,$datperi)
{
	$notas = "y".$ano."notas";
	$nota = array();
	for($i=1; $i<=9 ;$i++)
	{
		$nota[$i] = "";
	}
	if ($corte <= 0) //definitivo
		$corte=9;
	//estado de los periodos y estado examenes
	$estper1="";
	$estper2="";
	$estper3="";
	$estper4="";
	$estexa1="";
	$estexa2="";
	$estexa3="";
	foreach($datperi as $regperi)
	{
		if ($regperi->pertipo=="P")
		{
			if ($regperi->periodo==1)
				$estper1=$regperi->estado;
			if ($regperi->periodo==2)
				$estper2=$regperi->estado;
			if ($regperi->periodo==3)
				$estper3=$regperi->estado;
			if ($regperi->periodo==4)
				$estper4=$regperi->estado;
		}
		else
		{
			if ($regperi->periodo==1)
				$estexa1=$regperi->estado;
			if ($regperi->periodo==2)
				$estexa2=$regperi->estado;
			if ($regperi->periodo==3)
				$estexa3=$regperi->estado;
		}
	}
	//inactivacion de periodos en matricula
	$inactperi = "";
	$cursos = "y".$ano."cursos";
	$sql="SELECT inactivar FROM $cursos WHERE id_alumno = '$id_alumno' limit 1";
	$datcurs=viewsql($sql);
	foreach($datcurs as $regcurs)
	{$inactperi = $regcurs->inactivar;}
	//notas finales de la asignatura en todos los periodos y examenes del alumno
	$numnot=0;
	$totnot=0;
	$sql="select pertipo, periodo, nota, notaEx, notaExF, pendiente, inactivar from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' order by pertipo, periodo";
	$dattot=viewsql($sql);
	foreach($dattot as $regtot)
	{
		$periodo=0;
		$notanum = 0;
		$valnota = $regtot->nota;
		if ($regtot->pertipo=="E") //notas examenes
		{
			if ($regtot->periodo==1 and $estexa1=="CERRADO" and $corte >= 3) //semestral 1
				$notanum = 3;
			if ($regtot->periodo==2 and $estexa2=="CERRADO" and $corte >= 6) //semestral 2
				$notanum = 6;
			if ($regtot->periodo==3 and $estexa3=="CERRADO" and $corte >= 7) //final
				$notanum = 7;
			if ($regtot->periodo==4 and $estexa3=="CERRADO" and $corte >= 7) //superacion
				$notanum = 8;
		}
		else //notas periodos
		{
			$periodo = $regtot->periodo;
			if ($regtot->periodo==1 and $estper1=="CERRADO" and $corte >= 1)
			{
				$notanum = 1;
				if ($regtot->notaEx > 0 and $corte >= 3 and $estexa1=="CERRADO") //ajuste con examen semestral 1
					$valnota = $regtot->notaEx;
				if ($regtot->notaExF > 0 and $corte >= 7 and $estexa3=="CERRADO") //ajuste con examen Final
					$valnota = $regtot->notaExF;
			}
			if ($regtot->periodo==2 and $estper2=="CERRADO" and $corte >= 2)
			{
				$notanum = 2;
				if ($regtot->notaEx > 0 and $corte >= 3 and $estexa1=="CERRADO") //ajuste con examen semestral 1
					$valnota = $regtot->notaEx;
				if ($regtot->notaExF > 0 and $corte >= 7 and $estexa3=="CERRADO") //ajuste con examen Final
					$valnota = $regtot->notaExF;
			}
			if ($regtot->periodo==3 and $estper3=="CERRADO" and $corte >= 4)
			{
				$notanum = 4;
				if ($regtot->notaEx > 0 and $corte >= 6 and $estexa2=="CERRADO") //ajuste con examen semestral 2
					$valnota = $regtot->notaEx;
				if ($regtot->notaExF > 0 and $corte >= 7 and $estexa3=="CERRADO") //ajuste con examen Final
					$valnota = $regtot->notaExF;
			}
			if ($regtot->periodo==4 and $estper4=="CERRADO" and $corte >= 5)
			{
				$notanum = 5;
				if ($regtot->notaEx > 0 and $corte >= 6 and $estexa2=="CERRADO") //ajuste con examen semestral 2
					$valnota = $regtot->notaEx;
				if ($regtot->notaExF > 0 and $corte >= 7 and $estexa3=="CERRADO") //ajuste con examen Final
					$valnota = $regtot->notaExF;
			}
			if ($corte == 99) //TRAER NOTAS ULTIMO PERIODO ABIERTO PARA CONSULTAS
			{
				if ($regtot->periodo==1 and $estper1=="ABIERTO")
					$notanum = 1;
				if ($regtot->periodo==2 and $estper2=="ABIERTO")
					$notanum = 2;
				if ($regtot->periodo==3 and $estper3=="ABIERTO")
					$notanum = 4;
				if ($regtot->periodo==4 and $estper4=="ABIERTO")
					$notanum = 5;
			}
		}
		if ($notanum > 0)
		{
			if ($regtot->pendiente == 1)
				$nota[$notanum] = "PEND";
			if ($regtot->inactivar == 1)
				$nota[$notanum] = "INAC";
			if ($periodo > 0 and strpos($inactperi,$periodo) > 0) //inactivado periodo en matricula			
				$nota[$notanum] = "INAC";
			if ($nota[$notanum]=="")
			{
				//validar decimales
				$decimal = ($valnota-intval($valnota))*100;
				if ($decimal <= 0)
					$valnota = intval($valnota);
				$nota[$notanum] = $valnota;
				if ($regtot->pertipo=="P")
				{
					$numnot = $numnot + 1;
					$totnot = $totnot + $valnota; 
				}
			}
		}
	}
	//recalcula nota definitiva de area de los periodos cerrados
	if ($numnot > 0)
	{
		$valnota = round($totnot/$numnot,1);
		$nota[9] = $valnota;
	}
	//print_r($nota);
	return $nota;
}

/***********************************************************************************/
/* Funci�n que retorna arreglo de notas de area de todo el a�o                     */
/* recibe variables a�o, id_alumno, id_area, corte periodo notas, arreglo periodos */ 
/***********************************************************************************/
Function notas_area($ano,$id_alumno,$id_area,$corte,$datperi)
{
	$notasfa = "y".$ano."notasfa";
	$nota = array();
	for($i=1; $i<=9 ;$i++)
	{
		$nota[$i] = "";
	}
	if ($corte <= 0) //definitivo
		$corte=9;
	//estado de los periodos y estado de examenes
	$estper1="";
	$estper2="";
	$estper3="";
	$estper4="";
	$estexa1="";
	$estexa2="";
	$estexa3="";
	foreach($datperi as $regperi)
	{
		if ($regperi->pertipo=="P")
		{
			if ($regperi->periodo==1)
				$estper1=$regperi->estado;
			if ($regperi->periodo==2)
				$estper2=$regperi->estado;
			if ($regperi->periodo==3)
				$estper3=$regperi->estado;
			if ($regperi->periodo==4)
				$estper4=$regperi->estado;
		}
		else
		{
			if ($regperi->periodo==1)
				$estexa1=$regperi->estado;
			if ($regperi->periodo==2)
				$estexa2=$regperi->estado;
			if ($regperi->periodo==3)
				$estexa3=$regperi->estado;
		}
	}
	//inactivacion de periodos en matricula
	$inactperi = "";
	$cursos = "y".$ano."cursos";
	$sql="SELECT inactivar FROM $cursos WHERE id_alumno = '$id_alumno' limit 1";
	$datcurs=viewsql($sql);
	foreach($datcurs as $regcurs)
	{$inactperi = $regcurs->inactivar;}
	//traer notas finales del area en todos los periodos del alumno
	$numnot=0;
	$totnot=0;
	$sql="select periodo, nota, notaEx, notaExF from $notasfa where id_alumno = '$id_alumno' and id_area = '$id_area' order by periodo";
	$dattot=viewsql($sql);
	foreach($dattot as $regtot)
	{
		$notanum = 0;
		$valnota = $regtot->nota;
		$periodo = $regtot->periodo;
		if ($regtot->periodo==1 and $estper1=="CERRADO" and $corte >= 1)
		{
			$notanum = 1;
			if ($regtot->notaEx > 0 and $corte >= 3 and $estexa1=="CERRADO") //ajuste con examen semestral 1
				$valnota = $regtot->notaEx;
			if ($regtot->notaExF > 0 and $corte >= 7 and $estexa3=="CERRADO") //ajuste con examen Final
				$valnota = $regtot->notaExF;
		}
		if ($regtot->periodo==2 and $estper2=="CERRADO" and $corte >= 2)
		{
			$notanum = 2;
			if ($regtot->notaEx > 0 and $corte >= 3 and $estexa1=="CERRADO") //ajuste con examen semestral 1
				$valnota = $regtot->notaEx;
			if ($regtot->notaExF > 0 and $corte >= 7 and $estexa3=="CERRADO") //ajuste con examen Final
				$valnota = $regtot->notaExF;
		}
		if ($regtot->periodo==3 and $estper3=="CERRADO" and $corte >= 4)
		{
			$notanum = 4;
			if ($regtot->notaEx > 0 and $corte >= 6 and $estexa2=="CERRADO") //ajuste con examen semestral 2
				$valnota = $regtot->notaEx;
			if ($regtot->notaExF > 0 and $corte >= 7 and $estexa3=="CERRADO") //ajuste con examen Final
				$valnota = $regtot->notaExF;
		}
		if ($regtot->periodo==4 and $estper4=="CERRADO" and $corte >= 5)
		{
			$notanum = 5;
			if ($regtot->notaEx > 0 and $corte >= 6 and $estexa2=="CERRADO") //promedio con examen semestral 2
				$valnota = $regtot->notaEx;
			if ($regtot->notaExF > 0 and $corte >= 7 and $estexa3=="CERRADO") //promedio con examen Final
				$valnota = $regtot->notaExF;
		}
		if ($corte == 99) //TRAER NOTAS ULTIMO PERIODO ABIERTO PARA CONSULTAS
		{
			if ($regtot->periodo==1 and $estper1=="ABIERTO")
				$notanum = 1;
			if ($regtot->periodo==2 and $estper2=="ABIERTO")
				$notanum = 2;
			if ($regtot->periodo==3 and $estper3=="ABIERTO")
				$notanum = 4;
			if ($regtot->periodo==4 and $estper4=="ABIERTO")
				$notanum = 5;
		}
		if ($notanum > 0 and strpos($inactperi,$periodo) > 0) //inactivado periodo en matricula
			$notanum = 0;
		if ($notanum > 0)
		{
			//validar decimales
			$decimal = ($valnota-intval($valnota))*100;
			if ($decimal <= 0)
				$valnota = intval($valnota);
			$numnot = $numnot + 1;
			$totnot = $totnot + $valnota; 
			$nota[$notanum] = $valnota;
		}
	}
	//promedia nota definitiva de area de los periodos cerrados
	if ($numnot > 0)
	{
		$valnota = round($totnot/$numnot,1);
		$nota[9] = $valnota;
	}
	//print_r($nota);
	return $nota;
}
?>