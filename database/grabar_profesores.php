<?php
/************************************************************************************************
* @file        : grabar_profesores.php                                                          *
* @brief       : Grabar o actualizar profesores y tablas asociadas (contratos y documentos)     *
* @version     : 1.0                                                                            *
* @company     : IDIC                                                                           *
* @author      : Nora Rodriguez 20-mar-2012                                                     *
* @Modificado  : Nora Rodriguez 20-mar-2012                                                     *
************************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
include("database.php");
include("../webparam.php");
include("../libs/fechas.php");
include("../libs/image_resize.php");
connect();
$op = isset($_POST['op']) ? $_POST['op'] : null ;
if ($op == "")
	$op = isset($_GET['op']) ? $_GET['op'] : null ;
$fecha_hora = date('Y-m-d') . " ". date('H:i:s');

//grabar datos basicos del profesor
if ($op == "grabarprofesor")
{
	//verificar numero de identificacion sea inexistente
	$id_profesor = $_POST["id_profesor"];
	$docunum = $_POST["docunum"];
	$docunum = str_replace('.','',$docunum);
	$docunum = str_replace(',','',$docunum);
	$docunum = str_replace('-','',$docunum);
	$docunum = intval($docunum);
	$error = "";
	if ($docunum <= 0)
		$error = "N�mero de identificaci�n inv�lido...";
	$sql="select docunum from profesores where docunum = '$docunum' and id_profesor != '$id_profesor' limit 1";
	$datprof = viewsql($sql);
	if (count($datprof) > 0 )
		$error = "N�mero de identificaci�n duplicado...";
	if ($error == "")
	{
                $fname = basename($_FILES['nwfoto']['name']);                
		$fchnace = $_POST["anonac"]."-".$_POST["mesnac"]."-".$_POST["dianac"];
		$fchingreso = "";
		if ($_POST["anoing"] > 0 and $_POST["mesing"] > 0 and $_POST["diaing"] > 0)
			$fchingreso = $_POST["anoing"]."-".$_POST["mesing"]."-".$_POST["diaing"];
		$fchretiro = "";
		if ($_POST["anoret"] > 0 and $_POST["mesret"] >0 and $_POST["diaret"] > 0)
			$fchretiro = $_POST["anoret"]."-".$_POST["mesret"]."-".$_POST["diaret"];
		if ($id_profesor > 0)
		{
			$sql = "UPDATE profesores SET apellidos='".strtoupper($_POST["apellidos"])."', nombres='".strtoupper($_POST["nombres"])."', docutip='".$_POST["docutip"]."', docunum='$docunum', ".
			"docuexp='".strtoupper($_POST["docuexp"])."', genero='".$_POST["genero"]."', estado='".$_POST["estado"]."', tipo='".$_POST["tipo"]."', fchnace='$fchnace', lugnace='".strtoupper($_POST["lugnace"])."', ".
			"correo='".$_POST["correo"]."', celular='".$_POST["celular"]."', fchingreso='$fchingreso', fchretiro='$fchretiro', cargo='".strtoupper($_POST["cargo"])."', ".
			"direccion='".strtoupper($_POST["direccion"])."', barrio='".strtoupper($_POST["barrio"])."', telefono='".$_POST["telefono"]."', ".
			"escgrado='".strtoupper($_POST["escgrado"])."', escresol='".strtoupper($_POST["escresol"])."', especialidad='".strtoupper($_POST["especialidad"])."', detalles='".$_POST["detalles"]."', ".
			"fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' ".
			"WHERE id_profesor = '$id_profesor' limit 1";
			execsql($sql);
			if (strtoupper($_POST["nombres"])!=$_POST["oldnombres"] or strtoupper($_POST["apellidos"])!=$_POST["oldapellidos"])
			{
				//actualizar asignaturas a�o actual
				$ano = $_SESSION["anoaca"];
				$profasig = "Y".$ano."profasig";
				$profesor = strtoupper($_POST["apellidos"])." ".strtoupper($_POST["nombres"]);
				$sql = "UPDATE $profasig SET profesor='$profesor' WHERE id_profesor = '$id_profesor'";
				execsql($sql);
				//actualizar usuarios
				$sql = "UPDATE usuarios SET apellidos='".strtoupper($_POST["apellidos"])."', nombres='".strtoupper($_POST["nombres"])."' WHERE tipo = 'DOCENTE' and id_tipo = '$id_profesor'";
				execsql($sql);
			}
                        if ($_POST["estado"]=="RETIRADO")
                        {
				$sql = "UPDATE profesorescont SET estado='INACTIVO' WHERE id_profesor = '$id_profesor'";
				execsql($sql);
                        }
		}
		else
		{
			$sql = "INSERT INTO profesores (apellidos, nombres, docunum, docutip, docuexp, genero, fchnace, lugnace, correo, direccion, barrio, telefono, celular, escgrado, escresol, ".
			"especialidad, cargo, fchingreso, fchretiro, tipo, estado, detalles, fchcrea, usucrea) ".
			"VALUES ('".strtoupper($_POST["apellidos"])."', '".strtoupper($_POST["nombres"])."', '$docunum', '".$_POST["docutip"]."', '".strtoupper($_POST["docuexp"])."', '".$_POST["genero"]."', ".
			"'$fchnace', '".strtoupper($_POST["lugnace"])."', '".$_POST["correo"]."', '".strtoupper($_POST["direccion"])."', '".strtoupper($_POST["barrio"])."', '".$_POST["telefono"]."', ".
			"'".$_POST["celular"]."', '".strtoupper($_POST["escgrado"])."', '".strtoupper($_POST["escresol"])."', '".strtoupper($_POST["especialidad"])."', '".$_POST["cargo"]."', ".
			"'$fchingreso','$fchretiro', '".$_POST["tipo"]."', '".$_POST["estado"]."', '".strtoupper($_POST["detalles"])."', ".
			"'$fecha_hora', '".$_SESSION["usulog"]."')";
			execsql($sql);
			$sql = "select id_profesor from profesores where docunum = '$docunum' limit 1";
			$datprof = viewsql($sql);
			foreach($datprof as $regprof)
			{$id_profesor = $regprof->id_profesor;}
		}

		//grabar foto
		if ($fname!="" and $id_profesor > 0)
		{
                    $len = strlen($fname);
                    $ext = substr($fname, $len - 3, 3);
                    //borrar archivo foto anterior
                    $oldfoto = ROOT_DIR . "/" . $_POST["oldfoto"];
                    if (file_exists($oldfoto))
                    {@unlink($oldfoto);}
                    //cargar nueva foto con id profesor y fecha
                    $nwfecha = str_replace ( ":" , "" , $fecha_hora );
                    $nwfecha = str_replace ( "-" , "" , $nwfecha );
                    $nwfecha = str_replace ( " " , "" , $nwfecha );
                    $foto = "fotopro" . $id_profesor."-".$nwfecha;
                    $foto = "fotos/" . $foto . "." . $ext;
                    $rutafoto = ROOT_DIR . "/" . $foto;
                    ini_set('memory_limit', '200M');
                    ini_set('upload_max_filesize', '200M');
                    if (move_uploaded_file($_FILES['nwfoto']['tmp_name'], $rutafoto)) {
                        if (strtoupper($ext)=="GIF" or strtoupper($ext)=="JPG" or strtoupper($ext)=="PNG") 
                        {resize_image($rutafoto,150,150);}
                    } else {
                        $foto = "";
                    }
                    $sql = "update profesores set foto='$foto' where id_profesor='$id_profesor' limit 1";
                    execsql($sql);
		}
		disconnect();
		echo"<script> alert('Docente Grabado.'); window.location.href='../profesores_edit.php?id_profesor=$id_profesor';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../profesores_edit.php?id_profesor=$id_profesor';</script>";
	}
}

//borrar profesor del archivo
if ($op == "borrarprofesor")
{
	$id_profesor = $_POST["id_profesor"];
	$sql = "delete from profesores where id_profesor = '$id_profesor' limit 1";
	execsql($sql);
	$foto = ROOT_DIR."/".$_POST["oldfoto"];
	if (file_exists($foto))
	{@unlink(foto);}
	disconnect();
	echo"<script>alert('Docente Eliminado...');window.opener.document.location.reload();window.close();</script>";
}

//borrar foto del profesor
if ($op == "borrarfoto")
{
	$id_profesor = $_GET["id_profesor"];
	$foto = ROOT_DIR."/".$_GET["foto"];
	if (file_exists($foto))
	{@unlink($foto);}
	$sql = "update profesores set foto='' where id_profesor='$id_profesor' limit 1";
	execsql($sql);
	disconnect();
	echo"<script>window.location.href='../profesores_edit.php?id_profesor=$id_profesor';</script>";
}

//grabar contrato anual del profesor
if ($op == "grabarcontrato")
{
	$id_profesor = $_POST["id_profesor"];
        $id_contrato = $_POST["id_contrato"];
	$grados = "";
	$sql="SELECT grado FROM grados order by id_grado";
	$lista=viewsql($sql);
	foreach($lista as $reglis)
	{
		$grado = $reglis->grado;
		$chk = isset($_POST[$grado]) ? $_POST[$grado] : null ;
		if ($chk=="on")
			$grados.=$grado." ";
	}
	if ($grados!="")
		$grados = " ".$grados;
        if ($id_contrato <= 0)
        {
            $sql = "select id_contrato from profesorescont where id_profesor = '$id_profesor' and ano = '".$_POST["ano"]."' limit 1";
            $datcont = viewsql($sql);
            foreach($datcont as $regcont)
            {$id_contrato = $regcont->id_contrato;}
        }
	if ($id_contrato <= 0 )
	{
		$sql= "INSERT INTO profesorescont (id_profesor, ano, fchinicio, fchfinal, tipocont, tiempo, horas, estado, grados, detalles, fchcrea, usucrea) ".
		"VALUES ('$id_profesor','".$_POST["ano"]."', '".fec_amd($_POST["fchinicio"])."', '".fec_amd($_POST["fchfinal"])."', '".strtoupper($_POST["tipocont"])."', '".strtoupper($_POST["tiempo"])."', '".strtoupper($_POST["horas"])."', '".$_POST["estado"]."', '$grados', '".$_POST["detalles"]."', ".
		"'$fecha_hora', '".$_SESSION["usulog"]."')";
		execsql($sql);
	}
	else
	{
		$sql = "UPDATE profesorescont SET grados='$grados', fchinicio='".fec_amd($_POST["fchinicio"])."', fchfinal='".fec_amd($_POST["fchfinal"])."', tipocont='".strtoupper($_POST["tipocont"])."', tiempo='".strtoupper($_POST["tiempo"])."', horas='".strtoupper($_POST["horas"])."', estado='".$_POST["estado"]."', detalles='".$_POST["detalles"]."', ".
		"fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' ".
		"where id_contrato = '$id_contrato' limit 1";
		execsql($sql);
	}
	echo"<script>window.location.href='../profesores_contrat.php?id_profesor=$id_profesor';</script>";
}

//borrar contrato de profesor
if ($op == "borrarcontrato")
{
	$id_contrato = $_POST["id_contrato"];
	$id_profesor = $_POST["id_profesor"];
	$file = "y".$_POST["ano"]."profasig";
	$sql="SELECT * FROM $file WHERE id_profesor = '$id_profesor' limit 1";
	$datasig=viewsql($sql);
	if (count($datasig)==0)
	{
		$sql = "delete from profesorescont where id_contrato = '$id_contrato' limit 1";
		execsql($sql);
		disconnect();
		echo"<script>alert('Contrato Eliminado.'); window.location.href='../profesores_contrat.php?id_profesor=$id_profesor';</script>";
	}
	else
	{
		disconnect();
		echo"<script>alert('Imposible eliminar Contrato, tiene asignaturas para ese a�o!'); window.location.href='../profesores_contrat.php?id_profesor=$id_profesor';</script>";
	}
}

//grabar y cargar documento de profesor
if ($op == "grabardocumento")
{
	$id_profesor = $_POST["id_profesor"];
	$documento = trim(strtoupper($_POST["documento"]));
	$fname = $_FILES['archivo']['name'];
	$mensaje = "";
	$sql="select id_documento from documentos where id_profesor = '$id_profesor' and documento = '$documento' limit 1";
	$datdocu = viewsql($sql);
	if (count($datdocu) > 0 )
		$mensaje = "Documento con ese nombre ya existente...";
	$id_documento = 0;
	if ($mensaje == "" and $fname != "")
	{
		$sql="insert into documentos (id_profesor,documento,fchcrea,usucrea) values ('$id_profesor','$documento','$fecha_hora','".$_SESSION["usulog"]."')";
		execsql($sql);
		$sql="select id_documento from documentos where id_profesor = '$id_profesor' and documento = '$documento' limit 1";
		$datdocu = viewsql($sql);
		foreach($datdocu as $regdocu)
		{$id_documento = $regdocu->id_documento;}
	}
	if ($id_documento > 0)
	{
		$pos = strrpos($fname,"."); //ultima aparicion
		$ext = substr($fname,$pos+1);
		$file = "docum/docupro".$id_profesor."_".$id_documento.".".$ext;
		if (file_exists($file))
		{@unlink($file);}
		$rutafile = ROOT_DIR."/".$file;
		if (move_uploaded_file($_FILES['archivo']['tmp_name'], $rutafile))
		{
			if (strtoupper($ext)=="GIF" or strtoupper($ext)=="JPG" or strtoupper($ext)=="PNG") 
				resize_image($file,600,600);
			$sql = "update documentos set archivo = '$file' where id_documento = '$id_documento' limit 1";
			execsql($sql);
		}
		else
		{
			$sql = "delete from documentos where id_documento = '$id_documento' limit 1";
			execsql($sql);
			$mensaje = "Error al cargar el documento.";
		}
	}
	disconnect();
	if ($mensaje == "")
		echo"<script>window.location.href='../profesores_docum.php?id_profesor=$id_profesor';</script>";
	else
		echo"<script> alert('$mensaje'); window.location.href='../profesores_docum.php?id_profesor=$id_profesor';</script>";
}

//borrar y eliminar documento del profesor
if ($op == "borrardocumento")
{
	$id_profesor = $_GET["id_profesor"];
	$id_documento = $_GET["id_documento"];
	$archivo = ROOT_DIR."/".$_GET["archivo"];
	if (file_exists($archivo))
	{@unlink($archivo);}
	$sql = "delete from documentos where id_documento = '$id_documento' limit 1";
	execsql($sql);
	disconnect();
	echo"<script>window.location.href='../profesores_docum.php?id_profesor=$id_profesor';</script>";
}

?>