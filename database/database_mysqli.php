<?php
/************************************************************************************************
* @file        : database.php                                                                   *
* @brief       : Funciones para conectarse, consultar, ejecutar o desconectarse a base de datos *
* @version     : 1.0                                                                            *
* @company     : IDIC                                                                           *
* @author      : Nora Rodriguez 15-feb-2012                                                     *
* @Modificado  : Nora Rodriguez 03-dic-2018                                                     *
************************************************************************************************/

$dbh=0;
Function connectdbmaster() //conectarse como usuario al root servicio
{
        global $dbh;
	$bd="educa";
	$hostbd="localhost:8889";
	$userbd="root";
	$password="root";
        $dberror = "";
        $dbh = mysqli_connect($hostbd , $userbd , $password);
        mysqli_select_db($dbh, $bd);
}
Function connect() //conectarse como usuario root
{
	global $dbh;
        $instid = $_SESSION["instid"];
	$bd="educa".$instid;
	$hostbd="localhost:8889";
	$userbd="root";
	$password="root";
        $dbh = mysqli_connect($hostbd , $userbd , $password);
        mysqli_select_db($dbh, $bd);
}
Function execsql($sql) //ejecutar insert, update o delete
{
	global $dbh;
        $tildes = $dbh->query("SET NAMES 'utf8'"); //Para que se muestren las tildes
        return mysqli_query($dbh, $sql);
}
Function viewsql($sql) //seleccionar y cargar set de datos del sql en objetos
{
	$resultset=array();
	$rsh=execsql($sql);
	while ($obj=mysql_fetch_object($rsh))
		$resultset[]=$obj;
	mysql_free_result($rsh);
	return $resultset;
}
Function disconnect() //cerrar conexion
{
	global $dbh;
	mysqli_close($dbh);
}
?>