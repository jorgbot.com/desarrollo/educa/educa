<?php
/************************************************************************************************
* @file        : grabar_alumno.php                                                              *
* @brief       : Graba o actualiza tablas generales de setup                                    *
* @version     : 1.0                                                                            *
* @company     : IDIC                                                                           *
* @author      : Nora Rodriguez 28-mar-2012                                                     *
* @Modificado  : Nora Rodriguez 28-mar-2012                                                     *
************************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
include("database.php");
include("iniciar_anolectivo.php");
include("../webparam.php");
include("../libs/fechas.php");
connect();
$op = isset($_POST['op']) ? $_POST['op'] : null ;
if ($op == "")
	$op = isset($_GET['op']) ? $_GET['op'] : null ;
$fecha_hora = date('Y-m-d') . " ". date('H:i:s');

//ajustes para pensiones
if ($op == "pensiones")
{
	$ano = $_GET["ano"];
	if ($ano=="2015")
	{
		//ajuste nit institucion
		$sql = "ALTER TABLE institucion ADD `nit` VARCHAR( 15 ) NOT NULL COMMENT 'numero de identificacion tributaria' AFTER insticolor";
		execsql($sql);
		//ajuste interes pensiones
		$sql = "ALTER TABLE anoslect ADD `interes` int( 2 ) NOT NULL DEFAULT '0' COMMENT 'porcentaje interes por mora en pensiones'";
		execsql($sql);
		//ajustes permisos usuarios
		$sql = "ALTER TABLE usuarios ADD `nivtra` smallint( 1 ) NOT NULL DEFAULT '0' COMMENT 'nivel permiso transporte' AFTER nivfal";
		execsql($sql);
		$sql = "ALTER TABLE usuarios ADD `nivpag` smallint( 1 ) NOT NULL DEFAULT '0' COMMENT 'nivel permiso pagos' AFTER nivtra";
		execsql($sql);
		$sql="UPDATE usuarios SET nivtra='4',nivpag='4' WHERE nivpar='4' LIMIT 1";
		execsql($sql);
		//ajustes tabla matriculas
		$sql = "ALTER TABLE matriculas ADD `fchretiro` date NOT NULL DEFAULT '0000-00-00' COMMENT 'fecha retiro matricula' AFTER fecha";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `renovar` int( 1 ) NOT NULL DEFAULT '1' COMMENT 'renovar matricula' AFTER fchretiro";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `id_respmatri` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'responsable matricula' AFTER folio";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `id_resppagos` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'responsable de pagos' AFTER id_respmatri";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `id_respnotas` int( 6 ) NOT NULL DEFAULT '0' COMMENT 'responsable Notas' AFTER id_resppagos";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `matricula` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor matricula' AFTER beca";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension02` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension febrero' AFTER matricula";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension03` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension marzo' AFTER pension02";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension04` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension abril' AFTER pension03";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension05` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension mayo' AFTER pension04";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension06` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension junio' AFTER pension05";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension07` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension julio' AFTER pension06";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension08` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension agosto' AFTER pension07";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension09` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension septiembre' AFTER pension08";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension10` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension octubre' AFTER pension09";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `pension11` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'valor pension noviembre' AFTER pension10";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment02` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension febrero' AFTER pension11";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment03` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension marzo' AFTER coment02";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment04` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension abril' AFTER coment03";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment05` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension mayo' AFTER coment04";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment06` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension junio' AFTER coment05";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment07` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension julio' AFTER coment06";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment08` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension agosto' AFTER coment07";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment09` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension septiembre' AFTER coment08";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment10` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension octubre' AFTER coment09";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `coment11` VARCHAR( 60 ) NOT NULL COMMENT 'Comentario pension noviembre' AFTER coment10";
		execsql($sql);
		$sql = "ALTER TABLE matriculas ADD `saldoFin` int( 8 ) NOT NULL DEFAULT '0' COMMENT 'Saldo a favor del alumno' AFTER coment11";
		execsql($sql);
		$sql="UPDATE matriculas SET estado = 'RETIRADA' where estado = 'RETIRO'";
		execsql($sql);
		$sql="UPDATE listas SET lisnom = 'RETIRADA' where listip = 'MATRICULAS' and lisnom = 'RETIRO' LIMIT 1";
		execsql($sql);
		$sql="select id_matricula, id_alumno from matriculas where ano = '2015' order by id_alumno";
		$datalum=viewsql($sql);
		foreach($datalum as $regalum)
		{
			$id_matricula = $regalum->id_matricula;
			$id_alumno = $regalum->id_alumno;
			$id_respmatri = 0;
			$id_respnotas = 0;
			$id_resppagos = 0;
			$sql="select id_familiar, respnotas, respmatri, resppagos from familiares where id_alumno = '$id_alumno' ";
			$datfam=viewsql($sql);
			foreach($datfam as $regfam)
			{
				if ($regfam->respmatri > 0)
					$id_respmatri = $regfam->id_familiar;
				if ($regfam->respnotas > 0)
					$id_respnotas = $regfam->id_familiar;
				if ($regfam->resppagos > 0)
					$id_resppagos = $regfam->id_familiar;
			}
			$sql="UPDATE matriculas SET id_respnotas = '$id_respnotas', id_respmatri = '$id_respmatri', id_resppagos = '$id_resppagos' WHERE id_alumno = '$id_alumno'";
			execsql($sql);
		}		
		//ajustes tabla familiares
		$sql = "ALTER TABLE familiares ADD `pais` VARCHAR( 30 ) NOT NULL COMMENT 'pais origen' AFTER docuexp";
		execsql($sql);
		$sql = "ALTER TABLE familiares ADD `ciudad` VARCHAR( 30 ) NOT NULL COMMENT 'ciudad domicilio' AFTER estrato";
		execsql($sql);
		$sql="UPDATE familiares SET pais = 'COLOMBIA', ciudad = 'BOGOTA'";
		execsql($sql);
		//ajustes tabla alumnos
		$sql = "ALTER TABLE alumnos ADD `paisnace` VARCHAR( 30 ) NOT NULL COMMENT 'pais origen' AFTER depnace";
		execsql($sql);
		$sql="UPDATE alumnos SET paisnace = 'COLOMBIA'";
		execsql($sql);
	}
	crear_pagos($ano);
	crear_pagosaju($ano);
	crear_pagosdet($ano);
	crear_pagosent($ano);
	crear_pagostra($ano);
	echo"<script>alert('Tablas ajustadas y creadas para pensiones $ano');window.close();window.opener.document.location.href='../libs/logout.php';</script>";
}

?>