<?php
/************************************************************************************************
* @file        : grabar_transporte.php                                                          *
* @brief       : Grabar o actualizar transporte, rutas, conductores                            *
* @version     : 1.0                                                                            *
* @company     : IDIC                                                                           *
* @author      : Nora Rodriguez 28-ago-2015                                                     *
* @Modificado  : Nora Rodriguez 28-ago-2015                                                     *
************************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
include("database.php");
include("../webparam.php");
include("../libs/fechas.php");
include("../libs/image_resize.php");
connect();
$op = isset($_POST['op']) ? $_POST['op'] : null ;
if ($op == "")
	$op = isset($_GET['op']) ? $_GET['op'] : null ;
$fecha_hora = date('Y-m-d') . " ". date('H:i:s');

//grabar transporte Alumno
if ($op == "grabar_transporte")
{
	$id_alumno = $_POST["id_alumno"];
	$fchingreso = fec_amd($_POST["fchingreso"]);
	$fchretiro = fec_amd($_POST["fchretiro"]);
	$jornada = $_POST["jornada"];
	$jornadaop = 0;
	if ($_POST["id_rutaop"] > 0)
	{
		$jornada = 1;
		$jornadaop = 2;
	}
	//verificar matricula no repetida para un mismo alumno y a�o
	$id_transporte = 0;
	$sql = "select id_transporte from transporte where id_alumno = '$id_alumno' and ano = '".$_POST["ano"]."' limit 1";
	$datmatr = viewsql($sql);
	foreach($datmatr as $regmatr)
	{$id_transporte = $regmatr->id_transporte;}
	if ($id_transporte <= 0 )
	{
		$sql= "INSERT INTO transporte (id_alumno,ano,grado,curso,codigo,estado,fchingreso,fchretiro,id_ruta,id_familiar,paradero,jornada,id_rutaop,id_familiarop,paraderoop,jornadaop,transp02,transp03,transp04,transp05,transp06,transp07,transp08,transp09,transp10,transp11,coment02,coment03,coment04,coment05,coment06,coment07,coment08,coment09,coment10,coment11,detalles,fchcrea,usucrea) ".
		"VALUES ('$id_alumno','".$_POST["ano"]."','".$_POST["grado"]."','".$_POST["curso"]."','".$_POST["codigo"]."','".$_POST["estado"]."','$fchingreso','$fchretiro','".$_POST["id_ruta"]."','".$_POST["id_familiar"]."','".strtoupper($_POST["paradero"])."','$jornada','".$_POST["id_rutaop"]."','".$_POST["id_familiarop"]."','".strtoupper($_POST["paraderoop"])."','$jornadaop','".$_POST["transp02"].
		"','".$_POST["transp03"]."','".$_POST["transp04"]."','".$_POST["transp05"]."','".$_POST["transp06"]."','".$_POST["transp07"]."','".$_POST["transp08"]."','".$_POST["transp09"]."','".$_POST["transp10"]."','".$_POST["transp11"]."','".strtoupper($_POST["coment02"])."','".strtoupper($_POST["coment03"]).
		"','".strtoupper($_POST["coment04"])."','".strtoupper($_POST["coment05"])."','".strtoupper($_POST["coment06"])."','".strtoupper($_POST["coment07"])."','".strtoupper($_POST["coment08"])."','".strtoupper($_POST["coment09"])."','".strtoupper($_POST["coment10"])."','".strtoupper($_POST["coment11"])."','".$_POST["detalles"]."',".
		"'$fecha_hora', '".$_SESSION["usulog"]."')";
		execsql($sql);
	}
	else
	{
		$sql = "UPDATE transporte SET estado='".$_POST["estado"]."',codigo='".$_POST["codigo"]."',fchingreso='$fchingreso',fchretiro='$fchretiro',id_ruta='".$_POST["id_ruta"]."',id_familiar='".$_POST["id_familiar"]."',paradero='".strtoupper($_POST["paradero"])."',jornada='$jornada"."',id_rutaop='".$_POST["id_rutaop"]."',id_familiarop='".$_POST["id_familiarop"]."',paraderoop='".strtoupper($_POST["paraderoop"])."',jornadaop='$jornadaop',";
		if ($_SESSION["nivmat"] >= 4 or $_SESSION["nivtra"] >= 4)
		{
			$sql.="transp02='".intval($_POST["transp02"])."',transp03='".intval($_POST["transp03"])."',transp04='".intval($_POST["transp04"])."',transp05='".intval($_POST["transp05"])."',transp06='".intval($_POST["transp06"])."',transp07='".intval($_POST["transp07"]).
			"',transp08='".intval($_POST["transp08"])."',transp09='".intval($_POST["transp09"])."',transp10='".intval($_POST["transp10"])."',transp11='".intval($_POST["transp11"])."',coment02='".strtoupper($_POST["coment02"])."',coment03='".strtoupper($_POST["coment03"])."',coment04='".strtoupper($_POST["coment04"]).
			"',coment05='".strtoupper($_POST["coment05"])."',coment06='".strtoupper($_POST["coment06"])."',coment07='".strtoupper($_POST["coment07"])."',coment08='".strtoupper($_POST["coment08"])."',coment09='".strtoupper($_POST["coment09"])."',coment10='".strtoupper($_POST["coment10"])."',coment11='".strtoupper($_POST["coment11"])."',";
		}
		$sql.="detalles='".$_POST["detalles"]."',fchactual='$fecha_hora',usuactual='".$_SESSION["usulog"]."' WHERE id_transporte='$id_transporte' LIMIT 1";
		execsql($sql);
	}
	if ($_POST["id_familiar"] > 0)
	{
		$sql = "UPDATE familiares SET barrio='".strtoupper($_POST["barrio"])."',direccion='".strtoupper($_POST["direccion"])."',telefono='".$_POST["telefono"]."',telefofi='".$_POST["telefofi"]."',celular='".$_POST["celular"]."',fchactual='$fecha_hora',usuactual='".$_SESSION["usulog"]."' WHERE id_familiar='".$_POST["id_familiar"]."' LIMIT 1";
		execsql($sql);
	}
	if ($_POST["id_familiarop"] > 0 and $_POST["id_familiarop"]!=$_POST["id_familiar"])
	{
		$sql = "UPDATE familiares SET barrio='".strtoupper($_POST["barrioop"])."',direccion='".strtoupper($_POST["direccionop"])."',telefono='".$_POST["telefonoop"]."',telefofi='".$_POST["telefofiop"]."',celular='".$_POST["celularop"]."',fchactual='$fecha_hora',usuactual='".$_SESSION["usulog"]."' WHERE id_familiar='".$_POST["id_familiarop"]."' LIMIT 1";
		execsql($sql);
	}
	echo"<script>window.location.href='../alumnos_transporte.php?id_alumno=$id_alumno';</script>";
}

//borrar alumno de transporte si no tiene pagos
if ($op == "borrar_transporte")
{
	$id_transporte = $_POST["id_transporte"];
       	$id_alumno = $_POST["id_alumno"];
	$file = "y".$_POST["ano"]."pagos";
	$sql="SELECT id_pago FROM $file WHERE id_alumno = '$id_alumno' limit 1";
	$datpag=viewsql($sql);
	if (count($datpag)==0)
	{
            $sql = "delete from transporte where id_alumno = '$id_alumno' and ano = '".$_POST["ano"]."' limit 1";
            execsql($sql);
            disconnect();
            echo"<script> alert('Transporte Eliminado.'); window.location.href='../alumnos_transporte.php?op=listar&id_alumno=$id_alumno';</script>";
	}
	else
	{
		disconnect();
                echo"<script> alert('Imposible eliminar transporte, tiene pagos registrados.'); window.location.href='../alumnos_transporte.php?op=listar&id_alumno=$id_alumno';</script>";
	}
}

//grabar datos basicos conductor
if ($op == "grabar_conductor")
{
	//verificar numero de identificacion sea inexistente
	$id_conductor = $_POST["id_conductor"];
	$docunum = $_POST["docunum"];
	$docunum = str_replace('.','',$docunum);
	$docunum = str_replace(',','',$docunum);
	$docunum = str_replace('-','',$docunum);
	$docunum = intval($docunum);
	$error = "";
	if ($docunum <= 0)
		$error = "N�mero de identificaci�n inv�lido...";
	$sql="select docunum from conductores where docunum = '$docunum' and id_conductor != '$id_conductor' limit 1";
	$datcond = viewsql($sql);
	if (count($datcond) > 0 )
		$error = "N�mero de identificaci�n duplicado...";
	if ($error == "")
	{
		$fname= $_FILES['nwfoto']['name'];
		$fchingreso = "";
		if ($_POST["anoing"] > 0 and $_POST["mesing"] > 0 and $_POST["diaing"] > 0)
			$fchingreso = $_POST["anoing"]."-".$_POST["mesing"]."-".$_POST["diaing"];
		$fchretiro = "";
		if ($_POST["anoret"] > 0 and $_POST["mesret"] >0 and $_POST["diaret"] > 0)
			$fchretiro = $_POST["anoret"]."-".$_POST["mesret"]."-".$_POST["diaret"];
		if ($id_conductor > 0)
		{
			$sql = "UPDATE conductores SET apellidos='".strtoupper($_POST["apellidos"])."', nombres='".strtoupper($_POST["nombres"])."', docutip='".$_POST["docutip"]."', rh='".strtoupper($_POST["rh"])."' , categoria='".strtoupper($_POST["categoria"])."', docunum='$docunum', ".
			"docuexp='".strtoupper($_POST["docuexp"])."', estado='".$_POST["estado"]."', correo='".$_POST["correo"]."', celular='".$_POST["celular"]."', fchingreso='$fchingreso', fchretiro='$fchretiro', ".
			"direccion='".strtoupper($_POST["direccion"])."', barrio='".strtoupper($_POST["barrio"])."', telefono='".$_POST["telefono"]."', detalles='".$_POST["detalles"]."', ".
			"placa='".strtoupper($_POST["placa"])."', marca='".strtoupper($_POST["marca"])."', color='".strtoupper($_POST["color"])."', modelo='".$_POST["modelo"]."', capacidad='".$_POST["capacidad"]."', ".
			"fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' ".
			"WHERE id_conductor = '$id_conductor' limit 1";
			execsql($sql);
		}
		else
		{
			$sql = "INSERT INTO conductores (apellidos, nombres, docunum, docutip, docuexp, rh, categoria, correo, direccion, barrio, telefono, celular, placa, marca, color, modelo, capacidad, fchingreso, fchretiro, estado, detalles, fchcrea, usucrea) ".
			"VALUES ('".strtoupper($_POST["apellidos"])."', '".strtoupper($_POST["nombres"])."','$docunum', '".$_POST["docutip"]."', '".strtoupper($_POST["docuexp"])."', '".strtoupper($_POST["rh"])."', '".strtoupper($_POST["categoria"])."' , ".
			"'".$_POST["correo"]."', '".strtoupper($_POST["barrio"])."', '".strtoupper($_POST["direccion"])."', '".$_POST["telefono"]."', '".$_POST["celular"]."', ".
			"'".strtoupper($_POST["placa"])."', '".strtoupper($_POST["marca"])."', '".strtoupper($_POST["color"])."', '".$_POST["modelo"]."', '".$_POST["capacidad"]."', ".
			"'$fchingreso','$fchretiro', '".$_POST["estado"]."', '".strtoupper($_POST["detalles"])."', ".
			"'$fecha_hora', '".$_SESSION["usulog"]."')";
			execsql($sql);
			$sql = "select id_conductor from conductores where docunum = '$docunum' limit 1";
			$datcond = viewsql($sql);
			foreach($datcond as $regcond)
			{$id_conductor = $regcond->id_conductor;}
		}
		//grabar foto
		if ($fname!="" and $id_conductor > 0)
		{
                    $len = strlen($fname);
                    $ext = substr($fname, $len - 3, 3);
                    //borrar archivo foto anterior
                    $oldfoto = ROOT_DIR . "/" . $_POST["oldfoto"];
                    if (file_exists($oldfoto))
                    {@unlink($oldfoto);}
                     //cargar nueva foto con id conductor y fecha
                    $nwfecha = str_replace ( ":" , "" , $fecha_hora );
                    $nwfecha = str_replace ( "-" , "" , $nwfecha );
                    $nwfecha = str_replace ( " " , "" , $nwfecha );
                    $foto = "fotocon" . $id_conductor."-".$nwfecha;
                    $foto = "fotos/" . $foto . "." . $ext;
                    $rutafoto = ROOT_DIR . "/" . $foto;
                    ini_set('memory_limit', '200M');
                    ini_set('upload_max_filesize', '200M');
                    if (move_uploaded_file($_FILES['nwfoto']['tmp_name'], $rutafoto)) {
                        if (strtoupper($ext)=="GIF" or strtoupper($ext)=="JPG" or strtoupper($ext)=="PNG") 
                        {resize_image($rutafoto,150,150);}
                    } else {
                        $foto = "";
                    }
                    $sql = "update conductores set foto='$foto' where id_conductor='$id_conductor' limit 1";
                    execsql($sql);
		}
		disconnect();
		echo"<script> alert('Conductor Grabado.'); window.location.href='../transporte_conduct.php';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../transporte_conduct.php?opcion=editar&id_conductor=$id_conductor';</script>";
	}
}

//borrar conductor del archivo
if ($op == "borrar_conductor")
{
	$id_conductor = $_POST["id_conductor"];
	$sql="SELECT id_ruta FROM rutas WHERE id_conductor = '$id_conductor' limit 1";
	$datrut=viewsql($sql);
	if (count($datrut)==0)
	{
            $sql = "delete from conductores where id_conductor = '$id_conductor' limit 1";
            execsql($sql);
            $foto = ROOT_DIR."/".$_POST["oldfoto"];
            if (file_exists($foto))
            {@unlink(foto);}
            disconnect();
            echo"<script> alert('Conductor Eliminado.'); window.location.href='../transporte_conduct.php';</script>";
	}
	else
	{
		disconnect();
                echo"<script> alert('Imposible eliminar conductor, tiene rutas asignadas.'); window.location.href='../transporte_conduct.php';</script>";
	}
}

//borrar foto del conductor
if ($op == "borrar_foto")
{
	$id_conductor = $_GET["id_conductor"];
	$foto = ROOT_DIR."/".$_GET["foto"];
	if (file_exists($foto))
	{@unlink($foto);}
	$sql = "update conductores set foto='' where id_conductor='$id_conductor' limit 1";
	execsql($sql);
	disconnect();
	echo"<script>window.location.href='../transporte_conduct.php?opcion=editar&id_conductor=$id_conductor';</script>";
}

//grabar ruta
if ($op == "grabar_ruta")
{
	$id_ruta = $_POST["id_ruta"];
	$id_monitor = $_POST["id_monitor"];
	$id_conductor = $_POST["id_conductor"];
        $merror = "";
        //verificar numero de ruta repetida
        $sql="SELECT id_ruta FROM rutas WHERE ano = '".$_POST["ano"]."' and ruta = '".$_POST["ruta"]."' and id_ruta != '$id_ruta' limit 1";
	$rut=viewsql($sql);
        if (count($rut)>0)
        {
            $merror = " N�mero de ruta repetida.";
        }
        //verificar conductor ya asignado en otra ruta
        if ($_POST["id_conductor"] > 0)
        {
            $sql="SELECT id_ruta FROM rutas WHERE ano = '".$_POST["ano"]."' and id_conductor = '".$_POST["id_conductor"]."' and id_ruta != '$id_ruta' limit 1";
            $rut=viewsql($sql);
            if (count($rut)>0)
            {
                $merror = "Conductor ya asignado en otra Ruta.";
            }
        }
        //verificar monitor ya asignado en otra ruta
        if ($_POST["id_monitor"] > 0)
        {
            $sql="SELECT id_ruta FROM rutas WHERE ano = '".$_POST["ano"]."' and id_monitor = '".$_POST["id_monitor"]."' and id_ruta != '$id_ruta' limit 1";
            $rut=viewsql($sql);
            if (count($rut)>0)
            {
                $merror = "Monitor ya asignado en otra Ruta.";
            }
        }
        if ($merror!="")
        {
            echo"<script> alert('Error! $merror'); window.location.href='../transporte_rutas.php';</script>";
        }
        else
        {
            if ($id_ruta > 0)
            {
                    $sql="UPDATE rutas SET ruta='".$_POST["ruta"]."',recorrido='".strtoupper($_POST["recorrido"])."',id_conductor='".$_POST["id_conductor"]."',id_monitor='".$_POST["id_monitor"]."',detalles='".$_POST["detalles"]."',fchactual='$fecha_hora',usuactual='".$_SESSION["usulog"]."' WHERE id_ruta = '$id_ruta' LIMIT 1";
                    execsql($sql);
            }
            else
            {
                    $sql= "INSERT INTO rutas (ano,ruta,recorrido,id_conductor, id_monitor, detalles,fchactual,usuactual) ".
                    "VALUES ('".$_POST["ano"]."','".$_POST["ruta"]."','".strtoupper($_POST["recorrido"])."','".$_POST["id_conductor"]."','".$_POST["id_monitor"]."','".$_POST["detalles"]."','$fecha_hora','".$_SESSION["usulog"]."')";
                    execsql($sql);
            }
            echo"<script> window.location.href='../transporte_rutas.php';</script>";
        }
}


if ($op == "grabar_monitor")
{
	//verificar numero de identificacion sea inexistente
	$id_monitor = $_POST["id_monitor"];
	$docunum = $_POST["docunum"];
	$docunum = str_replace('.','',$docunum);
	$docunum = str_replace(',','',$docunum);
	$docunum = str_replace('-','',$docunum);
	$docunum = intval($docunum);
	$error = "";
	if ($docunum <= 0)
		$error = "N�mero de identificaci�n inv�lido...";
	$sql="select docunum from monitores where docunum = '$docunum' and id_monitor != '$id_monitor' limit 1";
	$datcond = viewsql($sql);
	if (count($datcond) > 0 )
		$error = "N�mero de identificaci�n duplicado...";
	if ($error == "")
	{
		
		$fchingreso = "";
		if ($_POST["anoing"] > 0 and $_POST["mesing"] > 0 and $_POST["diaing"] > 0)
			$fchingreso = $_POST["anoing"]."-".$_POST["mesing"]."-".$_POST["diaing"];
		$fchretiro = "";
		if ($_POST["anoret"] > 0 and $_POST["mesret"] >0 and $_POST["diaret"] > 0)
			$fchretiro = $_POST["anoret"]."-".$_POST["mesret"]."-".$_POST["diaret"];
		if ($id_monitor > 0)
		{
			$sql = "UPDATE monitores SET apellidos='".strtoupper($_POST["apellidos"])."', nombres='".strtoupper($_POST["nombres"])."', docutip='".$_POST["docutip"]."',docunum='$docunum', ".
			"docuexp='".strtoupper($_POST["docuexp"])."',rh='".strtoupper($_POST["rh"])."', estado='".$_POST["estado"]."', correo='".$_POST["correo"]."', celular='".$_POST["celular"]."', fchingreso='$fchingreso', fchretiro='$fchretiro', ".
			"direccion='".strtoupper($_POST["direccion"])."', barrio='".strtoupper($_POST["barrio"])."', telefono='".$_POST["telefono"]."', ".
			"fchactual='$fecha_hora', usuactual = '".$_SESSION["usulog"]."' ".
			"WHERE id_monitor = '$id_monitor' limit 1";
			execsql($sql);
		}
		else
		{
			$sql = "INSERT INTO monitores (apellidos, nombres, docunum, docutip, docuexp,rh, correo, direccion, barrio, telefono, celular, fchingreso, fchretiro, estado,fchcrea, usucrea) ".
			"VALUES ('".strtoupper($_POST["apellidos"])."', '".strtoupper($_POST["nombres"])."','$docunum', '".$_POST["docutip"]."', '".strtoupper($_POST["docuexp"])."','".strtoupper($_POST["rh"])."' ,".
			"'".$_POST["correo"]."', '".strtoupper($_POST["barrio"])."', '".strtoupper($_POST["direccion"])."', '".$_POST["telefono"]."', '".$_POST["celular"]."', ".
			"'$fchingreso','$fchretiro', '".$_POST["estado"]."', ".
			"'$fecha_hora', '".$_SESSION["usulog"]."')";
			execsql($sql);
			$sql = "select id_monitor from monitores where docunum = '$docunum' limit 1";
			$datmon = viewsql($sql);
			foreach($datmon as $regmon)
			{$id_monitor = $regmon->id_monitor;}
		}
		
		disconnect();
		echo"<script> alert('Monitor Grabado.'); window.location.href='../transporte_monit.php';</script>";
	}
	else
	{
		disconnect();
		echo"<script> alert('Error! $error'); window.location.href='../transporte_monit.php?opcion=editar&id_monitor=$id_monitor';</script>";
	}
}
if ($op == "borrar_monitor")
{
	$id_monitor = $_POST["id_monitor"];
	$sql="SELECT id_ruta FROM rutas WHERE id_monitor = '$id_monitor' limit 1";
	$datrut=viewsql($sql);
	if (count($datrut)==0)
	{
            $sql = "delete from monitores where id_monitor = '$id_monitor' limit 1";
            execsql($sql);
            disconnect();
            echo"<script> alert('Monitor Eliminado.'); window.location.href='../transporte_monit.php';</script>";
	}
	else
	{
		disconnect();
                echo"<script> alert('Imposible eliminar monitor, tiene rutas asignadas.'); window.location.href='../transporte_monit.php';</script>";
	}
}
?>