<?php
/************************************************************************************************
* @file        : grabar_planillas.php                                                           *
* @brief       : Graba lote de notas de alumnos por logro y actividades del logro               *
* @version     : 1.0                                                                            *
* @company     : IDIC                                                                           *
* @author      : Nora Rodriguez 15-feb-2012                                                     *
* @Modificado  : Nora Rodriguez 11-abr-2018 10:45pm                                                    *
************************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
include("database.php");
include("grabar_notas.php");
include("../webparam.php");
include("../libs/fechas.php");
connect();
$fecha_hora=date('Y-m-d')." ".date('H:i:s');

/********************************************************************************/
/* proceso que graba planilla de notas totales por periodo o examen             */
/* recibe variables POST de la captura de la planilla                           */ 
/********************************************************************************/
if ($_POST["op"] == "planilla_periodo")
{
	$id_asignatura = $_POST["id_asignatura"];
	$id_profasig = $_POST["id_profasig"];
	$pertipo = $_POST["pertipo"];
	$periodo = $_POST["periodo"];
	$tipoedu = $_POST["tipoedu"];
	$estado = $_POST["estado"];
	//datos asignatura
	$sql="select ano, grado, id_area, notas, observacion, evideprom, activprom from asignaturas where id_asignatura = '$id_asignatura' limit 1";
	$datasig = viewsql($sql);
	foreach($datasig as $regasig)
	{
		$ano = $regasig->ano;
		$grado = $regasig->grado;
		$id_area = $regasig->id_area;
		$requiere_not = $regasig->notas;
		$requiere_obs = $regasig->observacion;
		$evideprom = $regasig->evideprom;
		$activprom = $regasig->activprom;
	}
	//notas limite a�o lectivo
	$sql = "select notamin, notamax, notalim, notaevi from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{
		$notamin = intval($reganol->notamin);
		$notamax = intval($reganol->notamax);
		$notalim = intval($reganol->notalim);
		$notaevi = intval($reganol->notaevi);
	}
	//datos asignatura profesor
	$profasig = "y".$ano."profasig";
	$sql="select curso, id_profesor from $profasig where id_profasig = '$id_profasig' limit 1";
	$datasig=viewsql($sql);
	foreach($datasig as $regasig)
	{
		$curso = $regasig->curso;
		$id_profesor = $regasig->id_profesor;		
	}
	//traer logros de la signatura y periodo
	$numlogr = 0;
	if ($pertipo == "P")
	{
		$logrosasig = "y".$ano."logros";
		$sql = "SELECT id_logro, evidemin, activmin FROM $logrosasig WHERE id_asignatura = '$id_asignatura' AND periodos LIKE '% $periodo %' ";
		$sql.="AND tipoedu = '$tipoedu' ORDER BY logrocod";
		$logros=viewsql($sql);
		$numlogr = count($logros);
		$evidencias = "y".$ano."eviden";
		$actividades = "y".$ano."activ";
		$notasdet = "y".$ano."notasp".$periodo;
	}
	//ACTUALIZAR NOTAS ALUMNOS
	$errornot = 0; //contador alumnos sin nota final
	$erroract = 0; //contador alumnos sin nota en actividades logros
	$errorobs = 0; //contador alumnos sin observaci�n obligatoria
	$fileobs = "y".$ano."observa";
	$notas = "y".$ano."notas";
	$cursos = "y".$ano."cursos";
	$sql="SELECT id_alumno, fchretiro FROM $cursos WHERE grado = '$grado' AND curso = '$curso' AND tipoedu = '$tipoedu' ORDER BY id_alumno";
	$datalum=viewsql($sql);
	foreach($datalum as $regalum)
	{
		$id_alumno = $regalum->id_alumno;
		$camponot = "nota".$id_alumno;
		$campopen = "pend".$id_alumno;
		$campoina = "inac".$id_alumno;
		$pendiente = isset($_POST[$campopen]) ? $_POST[$campopen] : null ;
		$inactivar = isset($_POST[$campoina]) ? $_POST[$campoina] : null ;
		$retirado = 0;
		if (substr($regalum->fchretiro,0,1) > 0)
			$retirado = 1;		
		$id_logro = 0;
		$errorlogros = 0; //logros con errores del alumno
		$oklogros = 0; //logros OK del alumno
		$notatot = 0;
		$campologro = "alu".$id_alumno."log".$id_logro;
		if ($pertipo == "P") //peridodo academico
		{
			if ($numlogr > 0) //asignatura CON logros
			{
				$oklogros = $numlogr;
				$numactiv = 0;
				$okactiv = 0;
				if ($pendiente <= 0 and $inactivar <= 0 and $retirado <= 0) //nota asignatura del alumno pendiente o inactiva
				{
					$oklogros = 0;
					foreach($logros as $reglogr)
					{
						$id_logro = $reglogr->id_logro;
						$evidemin = $reglogr->evidemin;
						$activmin = $reglogr->activmin;
						$campologro = "alu".$id_alumno."log".$id_logro;
						$notalogro = abs(intval($_POST[$campologro]));
						if ($notalogro > $notamax and $notalogro > 0)
							$notalogro = $notamax;
						if ($notalogro < $notamin and $notalogro > 0)
							$notalogro = $notamin;
                                                $swgrabar = 0;
                                                if ($notaevi > 0) //Si se manejan evidencias en el a�o lectivo
                                                {
                                                    $sqlact = "SELECT id_actividad FROM $actividades WHERE id_logro = '$id_logro' AND periodo = '$periodo' AND porcentaje > '0' ";
                                                } else {
                                                    $sqlact = "SELECT id_actividad FROM $actividades WHERE id_logro = '$id_logro' AND periodo = '$periodo'";
                                                }
                                                $datacti = viewsql($sqlact);
                                                $numactiv = count($datacti);
                                                if ($numactiv <= 0) //sin actividades graba la nota capturada en la planilla
                                                {
                                                   $oklogros = $oklogros + 1;
                                                   grabar_notalogro($ano, $id_alumno, $id_asignatura, $id_logro, $periodo, $notalogro, 'X', 'X');
                                                   $swgrabar = 1;
                                                }
                                                if ($swgrabar == 0) //no grabo logro sin actividades
						{
                                                        //verificar que todas las actividades tengan nota
							$okactiv = 0;
                                                        $sqlnot = "SELECT nota, pendiente, inactivar, id_evidencia, id_actividad FROM $notasdet WHERE id_alumno = '$id_alumno' AND id_logro = '$id_logro' ORDER BY id_nota";
							$datnota=viewsql($sqlnot);
							foreach($datnota as $regnota)
							{
                                                            if ($regnota->id_actividad > 0 and $regnota->nota > 0)
                                                            {
                                                               $okactiv = $okactiv + 1;
                                                            }
                                                            //logro pendiente o inactivo
                                                            if ($regnota->id_evidencia <= 0 and $regnota->id_actividad <= 0 and ($regnota->pendiente > 0 or $regnota->inactivar > 0))
                                                            {
                                                               $okactiv = $okactiv + $numactiv;
                                                            }
                                                            //Actualizar notas evidencia y logro
                                                            $notalogro = promediar_logro($ano, $id_alumno, $id_logro, $periodo, $evideprom, $activprom);
                                                            grabar_notalogro($ano, $id_alumno, $id_asignatura, $id_logro, $periodo, $notalogro, 'X', 'X');
							}
							if ($okactiv >= $numactiv) {
                                                            $oklogros = $oklogros + 1;
                                                        }
							else {
                                                            $errorlogros = $errorlogros + 1; 
                                                        }
						}
					} //fin for logros
				} //fin si pendiente o inactiva nota del periodo
				//promediar logros de la asignatura para total del periodo y graba nota asignatura
				if ($oklogros >= $numlogr)
					$notatot = promediar_asignatura($ano, $id_alumno, $id_asignatura, $periodo, $tipoedu);
				if ($notatot <= 0 and $errorlogros > 0)
					$errornot = $errornot + 1;
				//si logros con actividades faltantes incrementa contador de alumnos
				if ($errorlogros > 0)
					$erroract = $erroract + 1;
			}
			else //asignatura SIN logros, campo de la planilla
			{
				$notatot = abs(intval($_POST[$campologro]));
				if ($requiere_not <= 0) //no requiere nota
					$notatot = 0;
				if ($notatot > 0)
				{
					if ($notatot > $notamax)
						$notatot = $notamax;
					if ($notatot < $notamin)
						$notatot = $notamin;
					grabar_notaasig($ano, $id_alumno, $id_asignatura, $pertipo, $periodo, $notatot, '0', '0');
				}
				else
				{
					if ($pendiente > 0 or $inactivar > 0)
						grabar_notaasig($ano, $id_alumno, $id_asignatura, $pertipo, $periodo, '0', $pendiente, $inactivar);
					if ($pendiente <= 0 and $inactivar <= 0 and $retirado <= 0 and $requiere_not > 0)
						$errornot = $errornot + 1;
				}
			}
			if ($requiere_obs > 0 and $estado == "ENTREGADA" and $inactivar <= 0 and $retirado <= 0) //asignatura con observacion obligatoria no inactiva
			{
				$sql="select id_obseasig from $fileobs where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' ".
					"and pertipo = '$pertipo' and periodo = '$periodo' limit 1";
				$datobse=viewsql($sql);
				if (count($datobse) <= 0)
					$errorobs = $errorobs + 1;
			}
		}
		else //examen semestral (1 y 2) o final (3)
		{
			$campoedi = "editar".$id_alumno;
			$editar = intval($_POST[$campoedi]); //requiere examen
			$notatot = intval($_POST[$campologro]); //nota examen
			//si alumno requiere examen
			if ($editar > 0)
			{
				if ($notatot > $notamax)
					$notatot = $notamax;
				if ($notatot < $notamin and $notatot > 0)
					$notatot = $notamin;
				if ($notatot <= 0 and $pendiente <= 0 and $inactivar <= 0) //requiere nota de examen
					$errornot = $errornot + 1;
				else
				{
					//ajustar nota del examen
					grabar_notaasig($ano, $id_alumno, $id_asignatura, $pertipo, $periodo, $notatot, $pendiente, $inactivar);
					//ajustar logros perdidos en los periodos
					ajustar_logros($ano, $periodo, $id_asignatura, $id_alumno, $tipoedu, $notatot);
					//promediar asignaturas periodos que cubre examen
					$pini = 1;
					$pfin = 4;
					if (intval($periodo)==1) //semestral 1
						$pfin = 2;
					if (intval($periodo)==2) //semestral 2
						$pini = 3;
					for($i=$pini; $i<=$pfin ;$i++) //ajustar periodos de notas
					{
						$notanw = promediar_asignatura($ano, $id_alumno, $id_asignatura, $i, $tipoedu);
					}
				}
			}
		}
	}
	//ENTREGA DE PLANILLA
	$mensaje = "Planilla Grabada.";
	$id_planilla = 0;
	$oldestado = "ABIERTA";
	$file = "y".$ano."planillas";
	$sql = "select id_planilla, estado from $file where id_asignatura = '$id_asignatura' and grado='$grado' and curso='$curso' and pertipo = '$pertipo' and periodo = '$periodo' limit 1";
	$datplan=viewsql($sql);
	foreach($datplan as $regplan)
	{
		$id_planilla = intval($regplan->id_planilla);
		$oldestado = $regplan->estado;
	}
	if ($id_planilla <= 0) //CREAR PLANILLA SI NO EXISTE
	{
		$mensaje = "Planilla Grabada en estado ABIERTA";		
		$sql = "INSERT INTO $file (grado, curso, id_asignatura, pertipo, periodo, id_profesor, estado, fchentrega) VALUES ('$grado', '$curso', '$id_asignatura', '$pertipo', '$periodo', '$id_profesor', 'ABIERTA', '$fecha_hora')";
		execsql($sql);
		$sql = "select id_planilla, estado from $file where id_asignatura = '$id_asignatura' and grado='$grado' and curso='$curso' and pertipo = '$pertipo' and periodo = '$periodo' limit 1";
		$datplan=viewsql($sql);
		foreach($datplan as $regplan)
		{
			$id_planilla = intval($regplan->id_planilla);
		}
	}
	if ($estado=="ENTREGADA" or $estado=="CERRADA")
	{
		if ($errornot == 0 and $errorobs == 0 and $erroract == 0)
		{
			if ($pertipo == "P") //peridodo academico
			{
				//grabar nota del area y periodo
				foreach($datalum as $regalum)
				{
					$id_alumno = $regalum->id_alumno;
					grabar_notaarea($ano, $grado, $id_area, $id_alumno, $periodo);
				}
			}
			else
			{
				//grabar nota del area y periodos
				foreach($datalum as $regalum)
				{
					$id_alumno = $regalum->id_alumno;
					if ($periodo!=2) // si examen semestral 1, final o superacion
					{
						grabar_notaarea($ano, $grado, $id_area, $id_alumno, 1);
						grabar_notaarea($ano, $grado, $id_area, $id_alumno, 2);
					}
					if ($periodo!=1) //si examen semestral 2, final o superacion
					{
						grabar_notaarea($ano, $grado, $id_area, $id_alumno, 3);
						grabar_notaarea($ano, $grado, $id_area, $id_alumno, 4);
					}
				}
			}
			//actualizar estado planilla
			if ($id_planilla > 0)
			{
				if ($estado=="ENTREGADA" and $oldestado=="ABIERTA")
				{
					$sql = "UPDATE $file SET estado = '$estado', id_profesor='$id_profesor', fchentrega = '$fecha_hora' WHERE id_planilla = '$id_planilla' LIMIT 1";
					execsql($sql);
					$mensaje = "Planilla Grabada en estado ".$estado;
				}
				if ($estado=="CERRADA" and $oldestado=="ENTREGADA")
				{
					$sql = "UPDATE $file SET estado = '$estado', id_profesor='$id_profesor', fchentrega = '$fecha_hora' WHERE id_planilla = '$id_planilla' LIMIT 1";
					execsql($sql);
					$mensaje = "Planilla Grabada en estado ".$estado;					
				}
			}
		}
		if ($errornot > 0)
			$mensaje = "Planilla NO $estado, existen $errornot alumnos SIN NOTA!";
		if ($erroract > 0)
			$mensaje = "Planilla NO $estado, existen $erroract alumnos con ACTIVIDADES SIN NOTA!";
		if ($errorobs > 0 )
			$mensaje = "Planilla NO $estado, existen $errorobs alumnos SIN OBSERVACION!";
	}
	else //ESTADO ABIERTA
	{
		if ($id_planilla > 0 and $estado!=$oldestado) //Volver a abrir planilla
		{
			$sql = "UPDATE $file SET estado = '$estado' WHERE id_planilla = '$id_planilla' LIMIT 1";
			execsql($sql);
			$mensaje = "Planilla Grabada en estado ".$estado;		
		}
	}
	disconnect();
	echo"<script> alert('$mensaje'); window.location.href='../notas_planilla.php?id_asignatura=$id_asignatura&id_profasig=$id_profasig&pertipo=$pertipo&periodo=$periodo&tipoedu=$tipoedu';</script>";
}

/***********************************************************************************/
/* proceso que graba planilla de notas logros, evidencias y actividades por alumno */
/* y periodo recibe variables POST de la captura de la planilla de un alumno       */ 
/***********************************************************************************/
if ($_POST["op"] === "planilla_alumno")
{
	$ano = $_POST["ano"];
	$id_asignatura = $_POST["id_asignatura"];
	$id_alumno = $_POST["id_alumno"];
	$periodo = $_POST["periodo"];
	$tipoedu = $_POST["tipoedu"];
        $evideprom = $_POST["evideprom"];
        $activprom = $_POST["activprom"];
	$alumtot = intval($_POST["alumtot"]);
	$pertipo = "P";
	$numerosig = intval($_POST["numero"]) + 1; //siguiente alumno
	if ($numerosig > $alumtot) {
          $numerosig = $alumtot;  
        }
	//notas limite a�o lectivo
	$sql = "select notamin, notamax, notalim from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{
		$notamin = intval($reganol->notamin);
		$notamax = intval($reganol->notamax);
		$notalim = intval($reganol->notalim);
	}
	//logros asignatura, periodo y tipoedu
        $file = "y".$ano."logros";
        $sql = "SELECT id_logro FROM $file WHERE id_asignatura = '$id_asignatura' AND periodos LIKE '%$periodo%' ";
        $sql.="AND tipoedu = '$tipoedu' ORDER BY logrocod";
        $datlogr=viewsql($sql);
        $nrolog = count($datlogr);
        $numlogr = count($datlogr);
        //evidencias de los logros de la asignatura y periodo
	$notalogro = 0;
        $fileevi = "y".$ano."eviden";
        $sql = "SELECT id_logro, id_evidencia, porcentaje FROM $fileevi WHERE id_asignatura = '$id_asignatura' AND periodo = '$periodo' ORDER BY id_logro, numero";
        $datevid = viewsql($sql);
        //actividades de los logros de la asignatura y periodo
        $fileact = "y".$ano."activ";
        $sql = "SELECT id_logro, id_evidencia, id_actividad, porcentaje FROM $fileact WHERE id_asignatura = '$id_asignatura' AND periodo = '$periodo' ORDER BY id_evidencia, numero";
        $datacti = viewsql($sql);
        //calcular notas para cada logro
	foreach($datlogr as $reglogr)
	{
		$id_logro = $reglogr->id_logro;
		$campopen = "chkpen".$id_logro;
		$campoina = "chkina".$id_logro;
		$chkpen = isset($_POST[$campopen]) ? $_POST[$campopen] : null ;
		$chkina = isset($_POST[$campoina]) ? $_POST[$campoina] : null ;
		$pendiente = 0;
		$inactivar = 0;
		if ($chkina=="on")
		{
			$inactivar = 1;
			$pendiente = 0;
		}
		if ($chkpen=="on" and $inactivar == 0)
			$pendiente = 1;
                //evidencias logro
                $notalogro = 0;
		$numevi = 0;
		foreach($datevid as $regevi)
		{
			if ($regevi->id_logro == $id_logro)
			{
				$numevi = $numevi + 1;
				$id_evidencia = $regevi->id_evidencia;
				$porcentevi = $regevi->porcentaje;
                                //actividades por evidencia
                                $notaevid = 0;
                                $numact = 0;
                                foreach($datacti as $regacti)
                                {
                                        if ($regacti->id_evidencia == $id_evidencia)
                                        {
                                            $numact = $numact + 1;
                                            $id_actividad = $regacti->id_actividad;
                                            $porcentaje = $regacti->porcentaje;
                                            $campoact = "logro".$id_logro."evi".$id_evidencia."act".$numact;
                                            $nota = abs(intval($_POST[$campoact])); //quitar signos y decimales
                                            if ($nota > 0)
                                            {
                                                if ($nota > $notamax)
                                                    $nota = $notamax;
                                                if ($nota < $notamin)
                                                    $nota = $notamin;
                                                $notapar = $nota;
                                                if ($activprom <= 0 and $porcentaje > 0)
                                                    $notapar = round($notapar*($porcentaje/100),1);
                                                $notaevid = $notaevid + $notapar;		
                                            }
                                            grabar_notaactiv($ano, $id_alumno, $id_asignatura, $id_logro, $id_evidencia, $id_actividad, $periodo, $nota);
                                        }
                                } 
                                if ($activprom > 0 and $numact > 0) {
                                    $notaevid = round($notaevid/$numact,1); }
                                //$notaevid = number_format($notaevid, 1, '.', '');
                                grabar_notaevide($ano, $id_alumno, $id_asignatura, $id_logro, $id_evidencia, $periodo, $notaevid);
                                $notapar = $notaevid;
                                if ($evideprom <= 0 and $porcentevi > 0) {
                                    $notapar = round($notapar*($porcentevi/100),2);}
                                $notalogro = $notalogro + $notapar;		
                        }
                }
                if ($evideprom > 0 and $numevi > 0) {
                    $notalogro = $notalogro/$numevi;}
                if ($notalogro > 0)
		{
			$notalogro = intval(round($notalogro,1)); //parte entera del redondeo a 1 decimal
			if ($notalogro > $notamax) {
                            $notalogro = $notamax;}
			if ($notalogro < $notamin) {
                            $notalogro = $notamin;}
			$pendiente = 0;
			$inactivar = 0;
		}
		grabar_notalogro($ano, $id_alumno, $id_asignatura, $id_logro, $periodo, $notalogro, $pendiente, $inactivar);
	} //fin logros
	if ($_POST["estado"] == "CERRADA")
	{
		$notatot = promediar_asignatura($ano, $id_alumno, $id_asignatura, $periodo, $tipoedu);
	}
	disconnect();
	echo"<script>window.location.href='../notas_alumno.php?numero=$numerosig&id_asignatura=$id_asignatura&curso=".$_POST["curso"]."&id_director=".$_POST["id_director"]."&periodo=$periodo&estado=".$_POST["estado"]."&alumtot=$alumtot';</script>";
}

/*********************************************************************************/
/* proceso que graba planilla de notas logros y actividades por alumno y periodo */
/* recibe variables POST de la captura de la planilla de un alumno               */ 
/*********************************************************************************/
if ($_POST["op"] === "planilla_alumnoact")
{
	$ano = $_POST["ano"];
	$id_asignatura = $_POST["id_asignatura"];
	$id_alumno = $_POST["id_alumno"];
	$periodo = $_POST["periodo"];
	$tipoedu = $_POST["tipoedu"];
	$alumtot = intval($_POST["alumtot"]);
	$pertipo = "P";
	$numerosig = intval($_POST["numero"]) + 1; //siguiente alumno
	if ($numerosig > $alumtot)
		$numerosig = $alumtot;
	//notas limite a�o lectivo
	$sql = "select notamin, notamax, notalim from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{
		$notamin = intval($reganol->notamin);
		$notamax = intval($reganol->notamax);
		$notalim = intval($reganol->notalim);
	}
	//logros asignatura, periodo y tipoedu
	$file = "y".$ano."logros";
	$sql = "SELECT id_logro FROM $file WHERE id_asignatura = '$id_asignatura' AND periodos LIKE '% $periodo %' ";
	$sql.="AND tipoedu = '$tipoedu' ORDER BY id_logro";
	$datlogr=viewsql($sql);
	$numlogr = count($datlogr);
	foreach($datlogr as $reglogr)
	{
		$id_logro = $reglogr->id_logro;
		$campolog = "logro".$id_logro;
		$campopen = "chkpen".$id_logro;
		$campoina = "chkina".$id_logro;
		$notalogro = intval($_POST[$campolog]);
		$chkpen = isset($_POST[$campopen]) ? $_POST[$campopen] : null ;
		$chkina = isset($_POST[$campoina]) ? $_POST[$campoina] : null ;
		$pendiente = 0;
		$inactivar = 0;
		if ($chkina=="on")
		{
			$inactivar = 1;
			$pendiente = 0;
		}
		if ($chkpen=="on" and $inactivar == 0)
			$pendiente = 1;
		//actividades del logro y periodo
                $id_evidencia = 0;
		$notalogro = 0;
		$file = "y".$ano."activ";
		$sql = "SELECT id_actividad, porcentaje FROM $file WHERE id_logro = '$id_logro' AND periodo = '$periodo' ORDER BY numero";
		$datacti = viewsql($sql);
		if (count($datacti) > 0)
		{
			$numacti = 0;
			foreach($datacti as $regacti)
			{
				$numacti = $numacti + 1;
				$id_actividad = $regacti->id_actividad;
				$porcentaje = $regacti->porcentaje;
				$campoact = "logro".$id_logro."act".$id_actividad;
				$nota = abs(intval($_POST[$campoact])); //quitar signos y decimales
				if ($nota > 0)
				{
					if ($nota > $notamax)
						$nota = $notamax;
					if ($nota < $notamin)
						$nota = $notamin;
					$notapar = $nota;
					if ($_POST["activprom"] <= 0 and $porcentaje > 0)
						$notapar = round($notapar*($porcentaje/100),2);
					$notalogro = $notalogro + $notapar;		
				}
				grabar_notaactiv($ano, $id_alumno, $id_asignatura, $id_logro, $id_evidencia, $id_actividad, $periodo, $nota);
			}
			if ($_POST["activprom"] > 0) //actividades por promedio
				$notalogro = $notalogro/$numacti;	
		}
		if ($notalogro > 0)
		{
			$notalogro = intval(round($notalogro,1)); //parte entera del redondeo a 1 decimal
			if ($notalogro > $notamax)
				$notalogro = $notamax;
			if ($notalogro < $notamin)
				$notalogro = $notamin;
			$pendiente = 0;
			$inactivar = 0;
		}
		grabar_notalogro($ano, $id_alumno, $id_asignatura, $id_logro, $periodo, $notalogro, $pendiente, $inactivar);
	} //fin logros
	if ($_POST["estado"] == "CERRADA")
	{
		$notatot = promediar_asignatura($ano, $id_alumno, $id_asignatura, $periodo, $tipoedu);
	}
	disconnect();
	echo"<script>window.location.href='../notas_alumno.php?numero=$numerosig&id_asignatura=$id_asignatura&curso=".$_POST["curso"]."&id_director=".$_POST["id_director"]."&periodo=$periodo&estado=".$_POST["estado"]."&alumtot=$alumtot';</script>";
}

/************************************************************************************/
/* proceso que graba planilla de notas actividades de un logro por alumno y periodo */
/* recibe variables POST de la captura de la planilla de un alumno y un logro       */ 
/************************************************************************************/
if ($_POST["op"] == "planilla_logro")
{
	$ano = $_POST["ano"];
	$grado = $_POST["grado"];
	$curso = $_POST["curso"];
	$id_asignatura = $_POST["id_asignatura"];
	$id_logro = $_POST["id_logro"];
	$id_alumno = $_POST["id_alumno"];
	$periodo = $_POST["periodo"];
	$tipoedu = $_POST["tipoedu"];
	$chkpen = isset($_POST['chkpen']) ? $_POST['chkpen'] : null ;
	$chkina = isset($_POST['chkina']) ? $_POST['chkina'] : null ;
	$chklogro = isset($_POST['chklogro']) ? $_POST['chklogro'] : null ;
	$pendiente = 0;
	$inactivar = 0;
	$logroinac = 0;
	if ($chkina=="on")
		$inactivar = 1;
	if ($chkpen=="on" and $inativar == 0)
		$pendiente = 1;
	if ($chklogro=="on")
		$logroinac = 1;
	$pertipo = "P";
	//notas limite a�o lectivo
	$sql = "select notamin, notamax, notalim from anoslect where ano = '$ano' limit 1";
	$datanol = viewsql($sql);
	foreach($datanol as $reganol)
	{
		$notamin = intval($reganol->notamin);
		$notamax = intval($reganol->notamax);
		$notalim = intval($reganol->notalim);
	}
	//actividades del logro
        $id_evidencia = 0;
	$notalogro = 0;
	$numacti = 0;
	$file = "y".$ano."activ";
	$sql = "SELECT id_actividad, porcentaje FROM $file WHERE id_logro = '$id_logro' AND periodo = '$periodo' ORDER BY numero";
	$datacti=viewsql($sql);
	foreach($datacti as $regacti)
	{
		$numacti = $numacti + 1;
		$id_actividad = $regacti->id_actividad;
		$campoact = "alu".$id_alumno."act".$id_actividad;
		$porcentaje = $regacti->porcentaje;
		$nota = abs(intval($_POST[$campoact]));
		if ($nota > 0)
		{
			if ($nota > $notamax)
				$nota = $notamax;
			if ($nota < $notamin)
				$nota = $notamin;
			$notapar = $nota;
			if ($_POST["activprom"] <= 0 and $porcentaje > 0)
				$notapar = round($notapar*($porcentaje/100),2);
			$notalogro = $notalogro + $notapar;		
		}
		grabar_notaactiv($ano, $id_alumno, $id_asignatura, $id_logro, $id_evidencia, $id_actividad, $periodo, $nota);
	}
	if ($_POST["activprom"] > 0 and $numacti > 0) //actividades por promedio
		$notalogro = $notalogro/$numacti;	
	if ($notalogro > 0)
	{
		$notalogro = intval(round($notalogro,1)); //parte entera de redondear a 1 decimal
		if ($notalogro > $notamax)
			$notalogro = $notamax;
		if ($notalogro < $notamin)
			$notalogro = $notamin;
		$pendiente = 0;
		$inactivar = 0;
	}
	grabar_notalogro($ano, $id_alumno, $id_asignatura, $id_logro, $periodo, $notalogro, $pendiente, $inactivar);
	if ($_POST["estado"] == "CERRADA")
	{
		$notatot = promediar_asignatura($ano, $id_alumno, $id_asignatura, $periodo, $tipoedu);
	}
	if (intval($_POST["noactivar"]) != $logroinac) //inactivar o activar nuevamente logro para el curso
	{
		$file = "y".$ano."logros";
		$cursos = "";
		$sql = "select cursos from $file where id_logro = '$id_logro' limit 1";
		$datlogr = viewsql($sql);
		foreach($datlogr as $reglogr)
		{$cursos = $reglogr->cursos;}
		if ($logroinac == 1) //inactivar logro para el curso
		{
			if (strpos($cursos,$curso) <= 0)
				$cursos = $cursos." ".$curso;
		}
		else
		{
			$bcurso = ' '.$curso;
			$cursos = str_replace($bcurso,'',$cursos);
		}
		$sql = "UPDATE $file set cursos = '$cursos' WHERE id_logro = '$id_logro' LIMIT 1";
		execsql($sql);
		//actualizar notas ya registradas
		$file = "y".$ano."cursos";
		$notasdet = "y".$ano."notasp".$periodo;
		$sql="SELECT id_alumno FROM $file WHERE grado = '$grado' AND curso = '$curso' AND tipoedu = '$tipoedu' order by id_alumno";
		$datalum=viewsql($sql);
		foreach($datalum as $regalum)
		{
			$id_alumno = $regalum->id_alumno;
			if ($logroinac == 1) //grabar logro nota 0 propiedad inactivo
				grabar_notalogro($ano, $id_alumno, $id_asignatura, $id_logro, $periodo, '0', '0', '1');
			else //grabar logro nota actual propiedad activo
				grabar_notalogro($ano, $id_alumno, $id_asignatura, $id_logro, $periodo, 'X', '0', '0');
		}
	}
	disconnect();
	echo"<script>window.close();</script>";
}

/************************************************************************************/
/* proceso que graba planilla de notas actividades de un logro por alumno y periodo */
/* recibe variables POST de la captura de la planilla de un alumno y un logro       */ 
/************************************************************************************/
if ($_POST["op"] == "planilla_observaciones")
{
	$ano = $_POST["ano"];
	$id_asignatura = $_POST["id_asignatura"];
	$pertipo = $_POST["pertipo"];
	$periodo = $_POST["periodo"];
	$estado = $_POST["estado"];
	$id_alumno = $_POST["id_alumno"];
	$id_director = $_POST["id_director"];
	$alumno = $_POST["alumno"];
	$id_observacion = $_POST["id_observacion"];
	$id_libre = $_POST["id_libre"];
	$observacion = strtoupper($_POST["observacion"]);
	$chkpen = isset($_POST['chkpen']) ? $_POST['chkpen'] : null ;
	$chkina = isset($_POST['chkina']) ? $_POST['chkina'] : null ;
	$pendiente = 0;
	$inactivar = 0;
	if ($chkpen=="on")
		$pendiente = 1;
	if ($chkina=="on")
	{
		$pendiente = 0;
		$inactivar = 1;
	}
	//grabar observacion libre
	$file = "y".$ano."observa";
	if ($observacion != "")
	{
		if ($id_libre > 0)
			$sql = "UPDATE $file SET observacion = '$observacion' WHERE id_obseasig = '$id_libre' LIMIT 1";
		else
			$sql= "INSERT INTO $file (id_alumno, id_asignatura, pertipo, periodo, observacion) ".
				"VALUES ('$id_alumno', '$id_asignatura', '$pertipo', '$periodo', '$observacion')";
		execsql($sql);
	}
	//grabar observacion de lista si no existe para la asignatura y periodo
	if ($id_observacion > 0)
	{
		$id_obseasig = 0;
		$sql="select id_obseasig from $file where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' ".
			"and pertipo = '$pertipo' and periodo = '$periodo' and id_observacion = '$id_observacion' limit 1";
		$datasig=viewsql($sql);
		foreach($datasig as $regasig)
		{$id_obseasig = intval($regasig->id_obseasig);}
		if ($id_obseasig == 0)
		{
			$observacion = "";
			$sql = "select observacion from observaciones where id_observacion = '$id_observacion' limit 1";
			$datobse = viewsql($sql);
			foreach($datobse as $regobse)
			{$observacion = $regobse->observacion;}
			$sql= "INSERT INTO $file (id_alumno, id_asignatura, pertipo, periodo, id_observacion, observacion) ".
				"VALUES ('$id_alumno', '$id_asignatura', '$pertipo', '$periodo', '$id_observacion', '$observacion')";
			execsql($sql);
		}
	}
	if ($pendiente != intval($_POST["oldpendiente"]) or $inactivar != intval($_POST["oldinactivar"]))	
		grabar_notaasig($ano, $id_alumno, $id_asignatura, $pertipo, $periodo, $_POST["notatot"], $pendiente, $inactivar);
	disconnect();
	echo"<script>window.location.href='../notas_observa.php?numero=".$_POST["numero"]."&id_asignatura=$id_asignatura&curso=".$_POST["curso"]."&id_director=$id_director&pertipo=$pertipo&periodo=$periodo&estado=$estado&alumtot=".$_POST["alumtot"]."';</script>";
}

/**************************************************************************/
/* proceso que graba peri�dos acad�micos del a�o escolar                  */
/**************************************************************************/
if ($_POST["op"] == "grabar_periodos")
{
	$error = "";
	$ano = $_POST["ano"];
	$anterior = "ABIERTO";
	$fchfallasi = "";
	$fchfallasf = "";
	$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
	$datperi=viewsql($sql);
	foreach($datperi as $regperi)
	{
		$id_periodo = $regperi->id_periodo;
		$oldestado = $regperi->estado; //estado anterior
		$pertipo = $regperi->pertipo;
		$periodo = $regperi->periodo;
		$est = "est".$id_periodo;
		$ant = "ant".$id_periodo;
		$ini = "ini".$id_periodo;
		$fin = "fin".$id_periodo;
		$cor = "cor".$id_periodo;
		$pla = "pla".$id_periodo;
		$ent = "ent".$id_periodo;
		$estado = $_POST[$est]; //nuevo estado
		$fchinicio = fec_amd($_POST[$ini]);
		$fchfinal = fec_amd($_POST[$fin]);
		if ($periodo==1)
			$fchfallasi = $fchinicio;
		else
		{
			//inicial fallas periodo anterior + 1 dia
			$fchfallasi = $fchfallasf; 
			$fchnum = mktime(0,0,0,intval(substr($fchfallasi,5,2)) ,intval(substr($fchfallasi,8,2))+1, intval(substr($fchfallasi,0,4)));
			$fchfallasi = date('Y-m-d',$fchnum);
		}
		if ($pertipo == "P")
			$fchfallasf = fec_amd($_POST[$cor]);
		$fchplazo = fec_amd($_POST[$pla]);
		$fchentrega = fec_amd($_POST[$ent]);
		if ($anterior != "CERRADO" and $regperi->notanum > 1) //si no es primer periodo
			$estado = "";
		//actualizar y verificar estado de planillas profesores, cerrarlas para entrega de notas oficiales
		$planillas = "y".$_POST["ano"]."planillas";
		if ($estado == "CERRADO" and $oldestado != "CERRADO")
		{
			//verificar que todos los cursos est�n CERRADOS en el periodo
			$numreg = 0;
			$detalle = "";
			$sql = "SELECT * FROM cursos where ano = '$ano' order by sign(grado), cast(SUBSTRING_INDEX(grado, '-', 1) as decimal), curso";
			$datcurs = viewsql($sql);
			foreach($datcurs as $regcurs)
			{
				$id_curso = $regcurs->id_curso;
				$grado = $regcurs->grado;
				$curso = $regcurs->curso;
				if ($periodo == 1)
				{
					$estado = "";
					$sql = "select estado from $planillas where id_asignatura = '0' AND grado = '$grado' AND curso = '$curso' AND pertipo = '$pertipo' AND periodo = '$periodo' LIMIT 1";
					$datplan=viewsql($sql);
					foreach($datplan as $regplan)
					{$estado = $regplan->estado;}
					if ($estado != 'CERRADO')
					{
						$numreg = $numreg + 1;
						$detalle = $detalle.$grado." (".$curso.") ";
					}
				}
			}
			//error si existen cursos ABIERTOS				
			if ($numreg > 0)
			{
				$estado = $oldestado;
				$error = "Imposible cerrar $regperi->pernomb, existen $numreg CURSOS SIN CERRAR en los grados $detalle, cerrar primero los cursos!";
			}
		}
		if ($pertipo == "P")
			$sql = "UPDATE periodos SET estado = '$estado', fchinicio = '$fchinicio', fchfinal = '$fchfinal', fchfallasi = '$fchfallasi', fchfallasf = '$fchfallasf', fchplazo = '$fchplazo', fchentrega = '$fchentrega' WHERE id_periodo = '$id_periodo' LIMIT 1";
		else
			$sql = "UPDATE periodos SET estado = '$estado', fchinicio = '$fchinicio', fchfinal = '$fchfinal', fchplazo = '$fchplazo' WHERE id_periodo = '$id_periodo' LIMIT 1";
		execsql($sql);
		//actualizar estado de planillas profesores, abrirlas para registro de notas		
		if ($estado == "ABIERTO")
		{
			$sql = "UPDATE $planillas SET estado = 'ABIERTA' WHERE id_asignatura > '0' and estado = '' and pertipo = '$pertipo' and periodo = '$periodo'";
			execsql($sql);
		}
		$anterior = $oldestado;
	}
	disconnect();
	if ($error != "")
		echo"<script> alert('Error! $error'); window.location.href='../notas_periodos.php?ano=$ano';</script>";
	else
		echo"<script>alert('Periodos grabados...'); window.location.href='../notas_periodos.php?ano=$ano';</script>";
}

/*************************************************************************************************/
/* proceso que graba nota examenes semestrales o superacion extraordinaria despues de fin de a�o */
/* recibe variables POST de la captura de la planilla de examenes recuperacion o superaci�n      */ 
/*************************************************************************************************/
if ($_POST["op"] == "planilla_examen")
{
	$ano = $_POST["ano"];
	$grado = $_POST["grado"];
	$pertipo = $_POST["pertipo"];
	$periodo = $_POST["periodo"];
	$id_alumno = $_POST["id_alumno"];
	$tipoedu = $_POST["tipoedu"];
	$id_asignatura = $_POST["id_asignatura"];
	$id_area = $_POST["id_area"];
	$nota = $_POST["notaex"];
	$notalim = $_POST["notalim"];
	$notamin = $_POST["notamin"];
	$notamax = $_POST["notamax"];
	if ($nota > $notamax)
		$nota = $notamax;
	if ($nota < $notamin and $nota > 0)
		$nota = $notamin;
	//ajustar nota del examen
	grabar_notaasig($ano, $id_alumno, $id_asignatura, $pertipo, $periodo, $nota, '0', '0');
	//ajustar logros perdidos en los periodos segun nota examen
	ajustar_logros($ano, $periodo, $id_asignatura, $id_alumno, $tipoedu, $nota);
	//promediar asignaturas periodos que cubre examen
	$pini = 1;
	$pfin = 4;
	if (intval($periodo)==1) //semestral 1
		$pfin = 2;
	if (intval($periodo)==2) //semestral 2
		$pini = 3;
	for($i=$pini; $i<=$pfin ;$i++) //periodos de notas
	{
		$notanw = promediar_asignatura($ano, $id_alumno, $id_asignatura, $i, $tipoedu);
	}
	//ajustar notas de area
	if ($periodo!=2) // si examen semestral 1, final o superacion
	{
		grabar_notaarea($ano, $grado, $id_area, $id_alumno, 1);
		grabar_notaarea($ano, $grado, $id_area, $id_alumno, 2);
	}
	if ($periodo!=1) //si examen semestral 2, final o superacion
	{
		grabar_notaarea($ano, $grado, $id_area, $id_alumno, 3);
		grabar_notaarea($ano, $grado, $id_area, $id_alumno, 4);
	}
	echo"<script>alert('Nota de Examen grabada.'); window.opener.document.location.reload();window.close();</script>";
}
?>