<?php
/**************************************************************************************************
- Configurar PHP, editar php.ini (MAMP/conf/phpversion/php.ini):
	smtp = idic.com (para envio de correos)
	max execution time = 0 (para que los scripts no tengan limite de tiempo)
	upload_max_filesize = 128M (tama�o m�ximo de archivos a cargar)
	memory_limit = 128M
	post_max_size = 128M (tama�o m�ximo archivos formulario)
	date.timezone = "America/Bogota"
	Verificar en php.ini que la linea extension = php_gd2.dll est� descomentareada
- Configurar PHPMYADMIN, editar config.inc.php (MAMP/bin/phpmyadmin/config.inc.php)
	$cfg['ExecTimeLimit'] = 0; (agregar al final, scripts no tengan limite de tiempo, backups... etc)
	$cfg['MemoryLimit'] = 0; (agregar al final, archivos a importar no tengan limite de memoria)
- Configurar PHPMYADMIN para que solicite contrase�a al iniciar:
	$cfg['auth_type'] = 'cookie'; (comentarear aut_type = config, user = root, password del root)
	$cfg['allowNoPassword'] = false;
	$cfg['controluser'] = 'pma';
	$cfg['controlpass'] = 'Idic5C31C16'; (Contrase�a del root)
***************************************************************************************************/
//$rootdir = $_SERVER['DOCUMENT_ROOT']."/idic";
$rootdir = dirname(__FILE__); 
$exportdir = $rootdir."/export/";
$tmpdir = sys_get_temp_dir();
define("ROOT_DIR", $rootdir); //carpeta aplicativo
define("FOLDER_TMP", $tmpdir); //carpeta archivos temporales php
define("EXPORT_DIR", $exportdir); //carpeta archivos descargas cliente
date_default_timezone_set('America/Bogota');
//error_reporting(0); //E_ALL o 0
//ini_set("display_errors", 0); //1 o 0
error_reporting(E_ERROR | E_PARSE | E_WARNING);
ini_set("display_errors", 1); //1 o 0
?>