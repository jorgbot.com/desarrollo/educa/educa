<?php
/***********************************************************************************************
* @file        : profesores_asignat.php                                                        *
* @brief       : Lista las asignaturas del profesor seg�n el a�o                               *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 25-mar-2012                                                    *
* @Modificado  : Nora Rodriguez 25-mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$op = isset($_GET['op']) ? $_GET['op'] : null ;
$id_profesor = isset($_GET['id_profesor']) ? $_GET['id_profesor'] : null ;
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
connect();
//profesor
$sql="SELECT apellidos, nombres FROM profesores where id_profesor = '$id_profesor' limit 1";
$datprof=viewsql($sql);
foreach($datprof as $regprof)
{$profesor = $regprof->nombres." ".$regprof->apellidos;}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	var ano = document.form0.ano.value;
	var id_profesor = document.form0.id_profesor.value;
	var theURL = 'profesores_asignat.php?ano='+ano+'&id_profesor='+id_profesor;
	window.location.href = theURL;
}
function validar()
{
	if(document.form1.nw_profesor.value == '')
	{alert('Falta seleccionar Docente...');document.form1.nw_profesor.focus(); return false;}
	if(document.form1.nw_profesor.value == document.form1.id_profasig.value)
	{alert('Docente no v�lido...');document.form1.nw_profesor.focus(); return false;}
	return true;
}
</script>
</head>
<body>
<?php if ($op=="") {
//asignaturas
$file = "y".$ano."profasig";
$sql="SELECT $file.*, asignaturas.asignatura, asignaturas.intensidad, asignaturas.gradocolor, asignaturas.gradofont ".
"FROM $file, asignaturas WHERE $file.id_profesor = '$id_profesor' and $file.id_asignatura = asignaturas.id_asignatura order by  sign($file.grado), cast(SUBSTRING_INDEX($file.grado, '-', 1) as decimal), $file.curso, asignaturas.asignatura";
$datasig=viewsql($sql);
?>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
<form action="" method="post" name="form0">
<input type="hidden" name="id_profesor" value="<?php echo $id_profesor;?>">
	<tr height="50">
		<td class="titulo">
		<img src='images/asignaturas.jpg' width="40" height="40" border="0" align="absmiddle"> ASIGNATURAS <?php echo $profesor;?>
		&nbsp;
		<select name="ano" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
		<?php
		$sql="SELECT ano FROM anoslect ORDER BY ano desc";
		$datanol=viewsql($sql);
		foreach($datanol as $reganol)
		{
			$selano = "";
			if ($reganol->ano==$ano)
				$selano = "selected";
			echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
		}
		?>
		</select>
		</td>
		<td class="titulo_campos" align="right"></td>
	</tr>
</form>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="250" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="30">Grado</th>
					<th width="30">Curso</th>
					<th>Asignatura</th>
					<th width="60">Intensidad</th>
				</tr>
				<?php
				foreach($datasig as $regasig)
				{
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center" bgcolor="<?php echo $regasig->gradocolor;?>"><font color="<?php echo $regasig->gradofont;?>"><b><?php echo $regasig->grado;?>&deg;</b></font></td>
					<td align="center"><?php echo $regasig->curso;?></td>
					<td><?php echo $regasig->asignatura;?></td>
					<td align="center"><?php echo $regasig->intensidad;?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="35">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 4 and $ano==$_SESSION["anoaca"] and count($datasig) > 0) { ?>
			<a class="link_menu" href="profesores_asignat.php?op=trasladar&id_profesor=<?php echo $id_profesor;?>"><img src="images/ok28.png" border="0" width="28" height="28" alt="" hspace="0" align="absmiddle"> <u>Trasladar Carga Acad�mica</u></a>
			&nbsp;&nbsp;&nbsp;&nbsp;
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</table>
<?php } else {
//traer profesores asignados a cursos
$sql="SELECT profesorescont.id_profesor, profesores.apellidos, profesores.nombres FROM profesores, profesorescont WHERE profesorescont.fchinicio like '$ano%' AND profesores.estado='ACTIVO' ";
$sql.="AND profesorescont.id_profesor = profesores.id_profesor ORDER BY profesores.apellidos, profesores.nombres";
$datprof = viewsql($sql);
?>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarcarga'>
<input type="hidden" name="id_profasig" value="<?php echo $id_profesor;?>">
<input type="hidden" name="ano" value="<?php echo $ano;?>">
	<tr height="50">
		<td class="titulo">
		<img src='images/asignaturas.jpg' width="40" height="40" border="0" align="absmiddle"> ASIGNATURAS <?php echo $profesor;?>
	</tr>
	<tr>
		<td>
			<table width="95%" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
				<tr height="250" valign="middle">
					<td width="30"></td>
					<td>Trasladar carga al Docente</td>
					<td>
						<select name="nw_profesor" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
						<option value="" selected>Seleccionar...</option>
						<?php
						foreach($datprof as $regprof)
						{
							$file = "y".$ano."profasig";
							$sql = "SELECT id_profasig FROM $file WHERE id_profesor = '".$regprof->id_profesor."' ";
							$datasig = viewsql($sql);
							if (count($datasig)==0)
								echo "<option value='$regprof->id_profesor-$regprof->apellidos $regprof->nombres'>$regprof->apellidos $regprof->nombres</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr height="35">
					<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
						<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar" align="absmiddle">
						<a href="profesores_asignat.php?id_profesor=<?php echo $id_profesor;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Regresar" hspace="10" align="absmiddle"></a>
						<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</form>
</table>
<?php } ?>
<br/><br/>
</body>
</html>
<?php disconnect();?>
