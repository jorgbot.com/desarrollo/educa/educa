<?php
/***********************************************************************************************
* @file        : asignaturas_meto.php                                                          *
* @brief       : Lista y edita metodologia por logro de asignatura, PARSELACION                *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
$id_asignatura = $_GET["id_asignatura"];
$id_logro = $_GET["id_logro"];
connect();
$datasignatura = datos_asignatura($id_asignatura);
$ano = intval($datasignatura["ano"]);
$datlogro = datos_logro($ano,$id_logro); //datos logro
//traer metodologia
$file = "y".$ano."metod";
$sql="SELECT * FROM $file WHERE id_asignatura = '$id_asignatura' and id_logro = '$id_logro' order by fecha";
$datmeto=viewsql($sql);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function checkDia(obj,registro)
{
	var valcampo = obj.value;
	var intcampo = parseInt(valcampo,10);
	if(intcampo > 31)
	{
		obj.value='';
		alert("D�a no v�lido!");
		obj.focus();
	}
	var nmes = eval ('document.form1.mes'+registro+'.value');
	if (intcampo == 31 && (nmes == '04' || nmes == '06' || nmes == '09' || nmes == '11'))
	{
		obj.value='';
		alert("D�a no v�lido para ese mes!");
		obj.focus();
	}
	if (intcampo >= 30 && nmes == '02')
	{
		obj.value='';
		alert("D�a no v�lido para ese mes!");
		obj.focus();
	}
}
function imprimir()
{
	ok=confirm('Desea Imprimir Metodolog�as?');
	if(ok)
	{
		var ano = document.form1.ano.value;
		var grado = document.form1.grado.value;
		var id_asignatura = document.form1.id_asignatura.value;
		var id_logro = document.form1.id_logro.value;
		var implogo = 'N';
		var papel = 'LETTER';
		var periodo = '';
		var implogros = 'S';
		var impactiv = 'N';
		var impmetod = 'S';
		var theURL= 'reports/rep_logrosmet.php?ano='+ano+'&periodo='+periodo+'&grado='+grado+'&id_asignatura='+id_asignatura+'&id_logro='+id_logro+'&implogros='+implogros+'&impactiv='+impactiv+'&impmetod='+impmetod+'&implogo='+implogo+'&papel='+papel;
		var winName='';
		var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
		window.open(theURL,winName,features);
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="5" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/academico.jpg' width="40" height="40" border="0" align="absmiddle"> METODOLOGIA LOGRO <?php echo $datlogro["logrocod"];?></td>
		<td class="subtitulo" align="right"><?php echo $datasignatura["asignatura"];?><br>GRADO <?php echo $datasignatura["grado"];?> A�O <?php echo $datasignatura["ano"];?></td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr><td colspan="2" height="40" class="tabla_color"><p align="justify"><b>LOGRO <?php echo $datlogro["logrocod"];?>: <?php echo $datlogro["logro"];?></b></p></td></tr>
	<tr><td colspan="2" height="10"></td></tr>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1">
<input type='hidden' name='op' value='grabarmetodologias'>
<input type='hidden' name='ano' value='<?php echo $datasignatura["ano"];?>'>
<input type='hidden' name='grado' value='<?php echo $datasignatura["grado"];?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='id_logro' value='<?php echo $id_logro;?>'>
	<tr height="380" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="80">Mes y D�a</th>
					<th width="40">Clases</th>
					<th>Metodolog�a</th>
					<th>Recursos</th>
					<th width="20"></th>
				</tr>
				<?php
				$num = 0;
				$id_metodologia = 0;
				foreach($datmeto as $regmeto)
				{
				$num = $num + 1;
				$nameid = "id".$num;
				$namedia = "dia".$num;
				$namemes = "mes".$num;
				$namecla = "clases".$num;
				$namemet = "metodologia".$num;
				$namerec = "recursos".$num;
				$id_metodologia = $regmeto->id_metodologia;
				?>		
				<tr height="25" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center">
					<?php select_mes($regmeto->fecha,$namemes);?>
					<input type="text" name="<?php echo $namedia;?>" id="dia" style="width: 20px;" maxlength="2" value="<?php echo substr($regmeto->fecha,8);?>" onBlur="javascript:checkDia(this,'<?php echo $num;?>');" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
					<td align="center"><input type="text" name="<?php echo $namecla;?>" id="metodologia" style="width: 25px;" maxlength="3" value="<?php echo $regmeto->numclases;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td>
						<textarea name="<?php echo $namemet;?>" rows="3" cols="100" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $regmeto->metodologia;?></textarea>
						<input type="hidden" name="<?php echo $nameid;?>" value="<?php echo $id_metodologia;?>">
					</td>
					<td><textarea name="<?php echo $namerec;?>" rows="3" cols="35" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $regmeto->recursos;?></textarea></td>
					<td align="center"><?php if ($_SESSION["nivasi"] >= 3) { ?> <a href="database/grabar_asignaturas.php?op=borrarmetodologia&id_metodologia=<?php echo $id_metodologia;?>&ano=<?php echo $ano;?>&id_asignatura=<?php echo $id_asignatura;?>&id_logro=<?php echo $id_logro;?>" title="Eliminar..."><img src="images/b_drop.png" width="16" height="16" border="0" hspace="0" align="absmiddle"></a><?php } ?></td>
				</tr>
				<?php
				}
				$ini = $num + 1;
				$max = $ini + 3;
				for($i=$ini; $i <= $max ; $i++)
				{
				$nameid = "id".$i;
				$namedia = "dia".$i;
				$namemes = "mes".$i;
				$namecla = "clases".$i;
				$namemet = "metodologia".$i;
				$namerec = "recursos".$i;
				?>
				<tr height="25" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td align="center">
						<?php select_mes("",$namemes);?>
						<input type="text" name="<?php echo $namedia;?>" id="dia" style="width: 20px;" maxlength="2" onBlur="javascript:checkDia(this,'<?php echo $i;?>');" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
					<td align="center"><input type="text" name="<?php echo $namecla;?>" value="" style="width: 25px;" maxlength="3" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td>
						<textarea name="<?php echo $namemet;?>" id="metodologia" rows="3" cols="100" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></textarea>
						<input type="hidden" name="<?php echo $nameid;?>" value="0">
					</td>
					<td><textarea name="<?php echo $namerec;?>" rows="3" cols="35" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></textarea></td>
					<td></td>
				</tr>
				<?php } ?>
				<input type='hidden' name='numero' value='<?php echo $max;?>'>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 2 and $ano == $_SESSION["anoaca"]) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar" align="absmiddle">
			<?php } ?>
			<a href="javascript:;" onClick="imprimir();" title="Imprimir"><img src="images/btn_imprimir.png" border="0" width="75" height="24" alt="Imprimir" hspace="10" align="absmiddle"></a>
			<a href="asignaturas_logros.php?id_asignatura=<?php echo $id_asignatura;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Regresar lista Logros" hspace="10" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
