<?php
/***********************************************************************************************
* @file        : asignaturas_horarios.php                                                          *
* @brief       : Crear y actualizar horarios por curso, grado y a�o acad�dmico                 *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 24-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 24-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$cursosel = isset($_GET['cursosel']) ? $_GET['cursosel'] : null ;
$numdia = isset($_GET['numdia']) ? $_GET['numdia'] : null ;
$ano = $_SESSION["anoaca"];
if ($numdia <= 0)
	$numdia = 1;
$titdia = nombre_dia($numdia);
//dia anterior
$antdia = $numdia - 1;
if ($antdia <= 0)
	$antdia = 5;
//dia siguiente
$sigdia = $numdia + 1;
if ($sigdia >= 6)
	$sigdia = 1;
connect();
$sql="SELECT grado, gradonom, nivel FROM grados order by id_grado";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{
	if ($reggrad->grado == $grado)
	{
		$nivel = $reggrad->nivel;
		break;
	}
}
//intervalos de horario
$sql = "SELECT * FROM horariosint where nivel = '$nivel' order by intervalo";
$datinte = viewsql($sql);
$numinte = count($datinte);
//cursos del grado
$sql = "select curso from cursos where ano = '$ano' and grado = '$grado' order by curso"; 
$datcurs = viewsql($sql);
$numcurs = count($datcurs);
//asignaturas del grado
$sql = "select id_asignatura, asignatura from asignaturas where ano = '$ano' and grado = '$grado' order by asignaturas.asignatura";
$datasig=viewsql($sql);
//profesores asignados al grado por asignaturas
$profasig = "y".$ano."profasig";
$sql = "select * from $profasig where grado = '$grado' order by id_profesor, curso";
$datprof = viewsql($sql);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	var grado = document.form0.grado.value;
	var numdia = document.form0.numdia.value;
	var cursosel = document.form0.cursosel.value;
	var theURL = 'horarios_dias.php?grado='+grado+'&numdia='+numdia+'&cursosel='+cursosel;
	window.location.href = theURL;
}
function checkProf(obj,intact,curact)
{
	var valobj = obj.value;
	var pos = valobj.indexOf('-');
	var idProf = valobj.substr(0,pos);
	idProf = parseInt(idProf,10);
	if (idProf > 0)
	{
		var profocu = eval ('document.form1.P'+idProf+'I'+intact+'.value');
		if (profocu != '' && profocu!=curact)
		{
			alert('Docente asignado ya asignado al curso '+profocu+'!');
			obj.value = '';
		}
		else
		{
			eval ("document.form1.P"+idProf+"I"+intact+".value='"+curact+"'");
		}
	}
}
</script>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr>
		<td width="20"></td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0">
			<form action="" method="post" name="form0">
			<input type='hidden' name='numdia' value='<?php echo $numdia;?>'>
			<input type='hidden' name='cursosel' value='<?php echo $cursosel;?>'>
				<tr height="50">
					<td class="titulo">
						HORARIO 
						&nbsp;&nbsp;
						<select name="grado" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
						<?php
						foreach($datgrad as $reggrad)
						{
							if ($grado == $reggrad->grado)
							{
								echo "<option value='$reggrad->grado' selected>GRADO $reggrad->gradonom</option>";
								$gradocolor = $reggrad->gradocolor;
							}
							else
								echo "<option value='$reggrad->grado'>GRADO $reggrad->gradonom</option>";
						}
						?>																
						</select>
						&nbsp;&nbsp;
						<a href="horarios_dias.php?ano=<?php echo $ano;?>&grado=<?php echo $grado;?>&numdia=<?php echo $antdia;?>&cursosel=<?php echo $cursosel;?>"><img src="images/arrback.png" border="0" width="24" height="24" hspace="2" title="Dia Anterior..." align="absmiddle"></a>
						<?php echo strtoupper($titdia);?>
						<a href="horarios_dias.php?ano=<?php echo $ano;?>&grado=<?php echo $grado;?>&numdia=<?php echo $sigdia;?>&cursosel=<?php echo $cursosel;?>"><img src="images/arrforwardt.png" border="0" width="24" height="24" hspace="2" title="Dia Siguiente..." align="absmiddle"></a>
					</td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
	<tr>
		<td></td>
		<td>
			<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0">
			<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
			<input type='hidden' name='op' value='grabarhorario'>
			<input type='hidden' name='ano' value='<?php echo $ano;?>'>
			<input type='hidden' name='grado' value='<?php echo $grado;?>'>
			<input type='hidden' name='nivel' value='<?php echo $nivel;?>'>
			<input type='hidden' name='numdia' value='<?php echo $numdia;?>'>
			<input type='hidden' name='cursosel' value='<?php echo $cursosel;?>'>
			<?php
			//cargar ocupacion profesores en todos los grados para el dia
			$idprof = 0;
			foreach($datprof as $regprof)
			{
				if ($regprof->id_profesor != $idprof)
				{
					$idprof = $regprof->id_profesor;
					for($i=1; $i<=$numinte ;$i++)
					{
						$name = "P".$idprof."I".$i;
						$valor = "";
						//horario asignado al profesor
						$sql = "select curso from horarios where id_profesor='$idprof' and ano='$ano' and dia='$numdia' and intervalo='$i' limit 1";
						$dathora=viewsql($sql);
						foreach($dathora as $reghora)
						{$valor = $reghora->curso;}
						?>
						<input type='hidden' name='<?php echo $name;?>' value='<?php echo $valor;?>'>
						<?php
					}
				}
			}
			?>
				<tr height="20"><td colspan="<?php echo $numcurs+1;?>" background="fondos/rollo.jpg"></td></tr>
				<tr valign="top" bgcolor="#FFFFFF">
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr><th height="26" width="90" background="images/backsilver2.gif"><?php echo $titdia;?></th></tr>
							<tr><td height="1"></td></tr>
							<?php
							$nint = 0;
							foreach($datinte as $reginte)
							{
								if ($reginte->tipo=="")
								{
									$nint = $nint +1;
									echo "<tr height='30'><td class='titulo_campos' bgcolor='#FFFFFF'>&nbsp;<font color='#E50000'>$nint.</font> $reginte->horas</td></tr><tr><td height='1' bgcolor='#C0C0C0'></td></tr>";
								}
								else
									echo "<tr height='15'><td class='titulo_campos' bgcolor='#C0C0C0' align='center'><font color='#000000'>$reginte->tipo</font></td></tr><tr><td height='1' bgcolor='#C0C0C0'></td></tr>";
							}
							?>
						</table>
					</td>
					<?php 
					foreach($datcurs as $regcurs)
					{
					$curso = $regcurs->curso;
					?>
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr><th height="26" background="images/backsilver2.gif"><?php echo $curso;?></th></tr>
						<tr><td height="1" bgcolor="#C0C0C0"></td></tr>
						<?php
						//traer horarios asignados al curso
						$sql = "select intervalo, id_asignatura, id_profesor from horarios where ano='$ano' and dia='$numdia' and grado='$grado' and curso='$curso' order by abs(intervalo)";
						$dathora=viewsql($sql);
						//recorrer intervalos del horario
						foreach($datinte as $reginte)
						{
							$intervalo = $reginte->intervalo;
							$name = "CURSO".$curso."I".$intervalo;
							$idprofhor = 0;
							$idasighor = 0;
							if (trim($reginte->tipo)=="") //no descanso ni menu
							{
								foreach($dathora as $reghora)
								{
									if ($reghora->intervalo == $intervalo)
									{
										$idprofhor = $reghora->id_profesor; 
										$idasighor = $reghora->id_asignatura; 
										break;
									}
								}
							?>
							<tr height="30">
								<td>
								<select name="<?php echo $name;?>" style="width: 250px;" class='txt_peq' onChange="javascript:checkProf(this,'<?php echo $intervalo;?>','<?php echo $curso;?>');" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								<option value="" selected></option>
								<?php
								//mostrar asignaturas del curso cargadas a profesores
								foreach($datasig as $regasig)
								{
									$asignatura = substr($regasig->asignatura,0,23);
									$numprof = 0;
									foreach($datprof as $regprof)
									{
										if ($regasig->id_asignatura == $regprof->id_asignatura and $regprof->curso == $curso)
										{
											$numprof = $numprof + 1;
											$profesor = substr($regprof->profesor,0,22);
											if ($regprof->id_profesor == $idprofhor and $regprof->id_asignatura == $idasighor)
												echo "<option value='$regprof->id_profesor-$regasig->id_asignatura' selected>$asignatura - $profesor</option>";
											else
												echo "<option value='$regprof->id_profesor-$regasig->id_asignatura'>$asignatura - $profesor</option>";
										}
									}
									if ($numprof <= 0 and $regasig->id_asignatura == $idasighor) //si asignatura seleccionada sin docente
										echo "<option value='0-$regasig->id_asignatura' selected>$asignatura</option>";
								}
								?>																
								</select>
								</td>
							</tr>
							<?php } else { ?>
							<tr height="15"><td bgcolor="#C0C0C0"></td></tr>
							<?php } ?>
							<tr><td height="1" bgcolor="#C0C0C0"></td></tr>
							<?php 
						}
						?>
						</table>
					</td>
					<?php
					}
					?>
				</tr>
				<tr height="34">
					<td colspan="<?php echo $numcurs+1;?>" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver402.gif">
						<?php if ($ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 3) { ?>
						<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
						<?php } ?>
						<a href="horarios.php?grado=<?php echo $grado;?>&ano=<?php echo $ano;?>&curso=<?php echo $cursosel;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
						<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
					</td>
				</tr>
			</form>
			</table>
		</td>
	</tr>
</table>
</body>
</html>
<?php disconnect();?>
