<?php
/***********************************************************************************************
* @file        : notas_periodos.php                                                            *
* @brief       : Actualizar periodos academicos por a�o                                        *
* @version     : 2.0                                                                           *
* @company     : NUBE                                                                          *
* @author      : Nora Rodriguez 24-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 24-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
$ano = $_GET["ano"];
connect();
$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
$datperi=viewsql($sql);
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />    
<meta name="robots" content="noindex" />
<meta name="googlebot" content="noindex" />
<title>Educa</title>
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" href="estilos.css" />
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/periodos.png' width="40" height="31" border="0" align="absmiddle"> PERIODOS ACADEMICOS A&Nacute;O <?php echo $ano;?></td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_planillas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabar_periodos'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
	<tr height="10">
		<td width="15"></td>
		<td></td>
		<td width="15"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th></th>
					<th>Estado</th>
					<th>Inicio</th>
					<th>Final</th>
					<th>Fallas</th>
					<th>Plazo Notas</th>
					<th>Boletines</th>
					<th width="20"></th>
				</tr>
				<?php
				$anterior = "";
				foreach($datperi as $regperi)
				{
				$id_periodo = $regperi->id_periodo;
				$ultimo = $regperi->estado;
				$est = "est".$id_periodo;
				$ant = "ant".$id_periodo;
				$ini = "ini".$id_periodo;
				$fin = "fin".$id_periodo;
				$cor = "cor".$id_periodo;
				$pla = "pla".$id_periodo;
				$ent = "ent".$id_periodo;
				?>		
				<tr height="25" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><?php echo $regperi->pernomb;?></td>
					<td align="center">
						<select name="<?php echo $est;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						$sql="SELECT * FROM listas WHERE listip = 'PERIODOS' order by id";
						$lista=viewsql($sql);
						foreach($lista as $reglis)
						{
							if ($regperi->estado == $reglis->lisnom)
								echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
							else
								echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
						}
						?>																
						</select>
						<input type="hidden" name="<?php echo $ant;?>" value="<?php echo $anterior;?>">
					</td>
					<td align="center"><input type="text" name="<?php echo $ini;?>" id="<?php echo $ini;?>" value="<?php echo fec_dma($regperi->fchinicio);?>" style="width: 90px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, <?php echo "form1.".$ini;?>, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center"><input type="text" name="<?php echo $fin;?>" id="<?php echo $fin;?>" value="<?php echo fec_dma($regperi->fchfinal);?>" style="width: 90px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, <?php echo "form1.".$fin;?>, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center"><input type="text" name="<?php echo $cor;?>" id="<?php echo $cor;?>" value="<?php echo fec_dma($regperi->fchfallasf);?>" style="width: 90px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, <?php echo "form1.".$cor;?>, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center"><input type="text" name="<?php echo $pla;?>" id="<?php echo $pla;?>" value="<?php echo fec_dma($regperi->fchplazo);?>" style="width: 90px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, <?php echo "form1.".$pla;?>, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td align="center"><input type="text" name="<?php echo $ent;?>" id="<?php echo $ent;?>" value="<?php echo fec_dma($regperi->fchentrega);?>" style="width: 90px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, <?php echo "form1.".$ent;?>, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
					<td><?php if ($regperi->estado == "ABIERTO") { ?><a class="link_list" href="reports/rep_planillaspen.php?ano=<?php echo $ano;?>&pertipo=<?php echo $regperi->pertipo;?>&periodo=<?php echo $regperi->periodo;?>&pernomb=<?php echo $regperi->pernomb;?>" title="Planillas Pendientes..." onclick="newWindow(this.href, '', 1024, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src="images/ico_alerta.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"></a><?php } ?></td>
				</tr>
				<?php
				$anterior = $regperi->estado;
				} ?>
			</table>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($ano >= $_SESSION["anoaca"] and $_SESSION["nivnot"] >= 4) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
