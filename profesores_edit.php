<?php
/***********************************************************************************************
* @file        : profesores_edit.php                                                           *
* @brief       : Edita y actualiza un registro de profesor                                     *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 15-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 15-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_profesores.php");
include("libs/fechas.php");
include("libs/image_resize.php");
$id_profesor = isset($_GET['id_profesor']) ? $_GET['id_profesor'] : null ;
$opcion="basicos";
connect();
$datprofesor = datos_profesor($id_profesor);
$id_contrato = 0;
if ($id_profesor > 0)
{
	$sql = "select id_contrato from profesorescont where id_profesor='$id_profesor' order by ano desc limit 1";
	$dat = viewsql($sql);
	foreach($dat as $reg)
	{$id_contrato = $reg->id_contrato;}
}
$datcontrato = datos_contrato($id_contrato, $id_profesor);
$titulo = "Nuevo Docente";
if ($id_profesor > 0)
	$titulo = "DOCENTE: ".$datprofesor["apellidos"]." ".$datprofesor["nombres"];
//lista de documentos
$sql="SELECT * FROM listas WHERE listip = 'IDENTIFICACION' order by id";
$listide=viewsql($sql);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/validInput.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.apellidos.value == '')
	{alert('Falta asignar Apellidos...');document.form1.apellidos.focus(); return false;}
	if(document.form1.nombres.value == '')
	{alert('Falta asignar Nombres...');document.form1.nombres.focus(); return false;}
	if(document.form1.docutip.value == '')
	{alert('Falta asignar tipo de documento...');document.form1.docutip.focus(); return false;}
	if(document.form1.docunum.value <= 0)
	{alert('Falta asignar Identificaci�n...');document.form1.docunum.focus(); return false;}
	if(document.form1.docuexp.value == '')
	{alert('Falta asignar lugar de expedici�n del documento...');document.form1.docuexp.focus(); return false;}
	if(document.form1.genero.value == '')
	{alert('Falta asignar Genero...');document.form1.genero.focus(); return false;}
	if(document.form1.tipo.value == '')
	{alert('Falta asignar Perfil...');document.form1.tipo.focus(); return false;}
	if(document.form1.correo.value == '')
	{alert('Falta asignar correo...');document.form1.correo.focus(); return false;}
	if(document.form1.telefono.value == '')
	{alert('Falta asignar tel�fono...');document.form1.telefono.focus(); return false;}
	if(document.form1.dianac.value == ''  || document.form1.mesnac.value == '' || document.form1.anonac.value == '')
	{alert('Falta asignar fecha de nacimiento...');document.form1.dianac.focus(); return false;}
	if(document.form1.lugnace.value == '')
	{alert('Falta asignar ciudad de Nacimiento...');document.form1.lugnace.focus(); return false;}
	if(document.form1.estado == 'RETIRADO')
	{
		if(document.form1.diaret.value == '' || document.form1.mesret.value == '' || document.form1.anoret.value == '')
		{alert("Favor asignar Fecha de Retiro...");document.form1.diaret.focus(); return false;}
	}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar este Docente?");
	if(ok)
	{
		document.form1.op.value='borrarprofesor';
		document.form1.submit();
	}
}
</script>
</head>
<body>
<?php include("includes/menu_profesores.php");?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_profesores.php" method="post" name="form1" onSubmit="return validar();" enctype="multipart/form-data">
<input type='hidden' name='op' value='grabarprofesor'>
<input type='hidden' name='id_profesor' value='<?php echo $id_profesor;?>'>
<input type="hidden" name="oldfoto" value="<?php echo $datprofesor["foto"];?>">
<input type="hidden" name="oldnombres" value="<?php echo $datprofesor["nombres"];?>">
<input type="hidden" name="oldapellidos" value="<?php echo $datprofesor["apellidos"];?>">
	<tr height="10">
		<td width="10"></td>
		<td></td>
		<td width="10"></td>
		<td></td>
		<td width="10"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Identificaci�n - ID <?php echo $id_profesor;?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td colspan="3" height="4"></td></tr>
				<tr>
					<td width="160" height="160" bgcolor="#E0E7E9" align="center" valign="middle">
						<?php if ($datprofesor["foto"] == "") { ?>
						<img src="images/foto.gif" width="80" height="80" border="0" align="absmiddle">
						<?php } else {  $height = 150; $width = resize_width($datprofesor["foto"],150); ?>
						<img src="<?php echo $datprofesor["foto"];?>" width="<?php echo $width;?>" height="<?php echo $height;?>" border="0" align="absmiddle">
						<?php } ?>
					</td>
					<td width="10"></td>
					<td>
						<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<tr>
								<td width="60">Estado</td>
								<td>
									<select name="estado" style="width: 150px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									$sql="SELECT * FROM listas WHERE listip = 'PROFESORES' order by id";
									$lista=viewsql($sql);
									foreach($lista as $reglis)
									{
										if ($datprofesor["estado"] == $reglis->lisnom)
											echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
										else
											echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Apellidos <font color="red">*</font></td>
								<td><input type="text" name="apellidos" size="25" onKeyUp="javascript:checkText(form1.apellidos);" value="<?php echo $datprofesor["apellidos"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							<tr>
							<tr>
								<td>Nombres <font color="red">*</font></td>				
								<td><input type="text" name="nombres" size="25" onKeyUp="javascript:checkText(form1.nombres);" value="<?php echo $datprofesor["nombres"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							<tr>
							<tr>
								<td>Identificaci�n <font color="red">*</font></td>
								<td>
									<select name="docutip" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<option value="" selected></option>
									<?php
									foreach($listide as $reglis)
									{
										if ($datprofesor["docutip"] == $reglis->lisval)
											echo "<option value='$reglis->lisval' title='$reglis->lisnom' selected>$reglis->lisval</option>";
										else
											echo "<option value='$reglis->lisval' title='$reglis->lisnom'>$reglis->lisval</option>";
									}
									?>																
									</select>
									<input type="text" name="docunum"  style="width: 100px;" maxlength="15" value="<?php echo $datprofesor["docunum"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr>
								<td>Expedido en <font color="red">*</font></td>
								<td>
									<input type="text" name="docuexp" size="25" value="<?php echo $datprofesor["docuexp"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr>
								<td>G�nero <font color="red">*</font></td>
								<td>
									<select name="genero" style="width: 110px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<option value="" selected></option>
									<?php
									$sql="SELECT * FROM listas WHERE listip = 'GENERO' order by id";
									$lista=viewsql($sql);
									foreach($lista as $reglis)
									{
										if ($datprofesor["genero"] == $reglis->lisval)
											echo "<option value='$reglis->lisval' selected>$reglis->lisnom</option>";
										else
											echo "<option value='$reglis->lisval'>$reglis->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
							<tr>
								<td>Perfil <font color="red">*</font></td>
								<td>
									<select name="tipo" style="width: 110px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<option value="" selected></option>
									<?php
									$sql="SELECT * FROM listas WHERE listip = 'PERSONAL' order by id";
									$lista=viewsql($sql);
									foreach($lista as $reglis)
									{
										if ($datprofesor["tipo"] == $reglis->lisnom)
											echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
										else
											echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
									}
									?>																
									</select>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</fieldset>
			<fieldset>
			<legend class="titulo_campos">Escalaf�n y Perfil</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr>
					<td>Resoluci�n</td>
					<td>
					<input type="text" name="escresol" size="17" value="<?php echo $datprofesor["escresol"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					Grado
					<input type="text" name="escgrado" size="5" value="<?php echo $datprofesor["escgrado"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr>
					<td>Especilidad</td>
					<td><input type="text" name="especialidad" size="67" value="<?php echo $datprofesor["especialidad"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td>Cargo</td>
					<td><input type="text" name="cargo" size="67" value="<?php echo $datprofesor["cargo"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
			</table>
			</fieldset>
			<fieldset>
			<legend class="titulo_campos">Cargar Foto JPG/GIF/PNG</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr height="35">
					<td>
						<?php if ($datprofesor["foto"]!="") { ?>
						<a href="database/grabar_profesores.php?op=borrarfoto&id_profesor=<?php echo $id_profesor;?>&foto=<?php echo $datprofesor["foto"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><img src="images/b_drop.png" width="16" height="16" border="0" align="absmiddle" title="Borrar foto actual"></a>
						<?php } ?>
					</td>
					<td>
						<input type="file" name="nwfoto" size="55" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<img src="images/ico_info.png" border="0" width="16" height="16" align="absmiddle" title="Tama�o 150 X 150 pixels. Si es superior el sistema la reducir� y puede deteriorarse la calidad.">
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Domicilio y Ubicaci�n</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr height="2"><td ></td><td width="60"></td><td></td></tr>
				<tr>
					<td></td>
					<td>Direcci�n</td>
					<td><input type="text" name="direccion" size="45" value="<?php echo $datprofesor["direccion"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Barrio</td>
					<td>
						<input type="text" name="barrio" size="45" value="<?php echo $datprofesor["barrio"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>Correo <font color="red">*</font></td>
					<td><input type="text" name="correo" id="correo" size="45" value="<?php echo $datprofesor["correo"];?>" onBlur="javascript:checkMail(form1.famicor);" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
				</tr>
				<tr>
					<td></td>
					<td>Tel�fonos <font color="red">*</font></td>
					<td>
						<input type="text" name="telefono" size="45" value="<?php echo $datprofesor["telefono"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr>
					<td></td>
					<td>Celulares </td>
					<td>
						<input type="text" name="celular" size="45" value="<?php echo $datprofesor["celular"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">						
					</td>
				</tr>
			</table>
			</fieldset>
			<fieldset>
			<legend class="titulo_campos">Fechas</legend>
			<table border="0" width="100%">
				<tr>
					<td width="60">Ingreso</td>
					<td>
					<?php 
					select_dia($datprofesor["fchingreso"],'diaing');
					select_mes($datprofesor["fchingreso"],'mesing');
					select_ano($datprofesor["fchingreso"],'anoing');
					?>
					</td>
				</tr>
				<tr>
					<td>Retiro</td>
					<td>
					<?php 
					select_dia($datprofesor["fchretiro"],'diaret');
					select_mes($datprofesor["fchretiro"],'mesret');
					select_ano($datprofesor["fchretiro"],'anoret');
					?>
					</td>
				</tr>
				<tr>
					<td>Nacimiento <font color="red">*</font></td>
					<td>
					<?php 
					select_dia($datprofesor["fchnace"],'dianac');
					select_mes($datprofesor["fchnace"],'mesnac');
					select_ano($datprofesor["fchnace"],'anonac');
					?>
					</td>
				</tr>
				<tr>
					<td>Ciudad <font color="red">*</font></td>
					<td>
					<input type="text" name="lugnace" style="width: 160px; text-transform:uppercase;" value="<?php echo $datprofesor["lugnace"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
			</table>
			</fieldset>
			<fieldset>
			<legend class="titulo_campos">Observaciones Generales</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr>
					<td>
					<textarea name='detalles' rows="4" cols="60" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datprofesor["detalles"];?></textarea>
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr>
		<td></td>
		<td colspan="3">
			<fieldset>
			<legend class="titulo_campos">Ultimo Contrato - Grados A�o <?php echo $datcontrato["ano"];?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
				<tr><td height="5"></td></tr>
				<tr height="25">
				<?php
				$gradoscont = "*".$datcontrato["grados"];
				$sql="SELECT grado FROM grados order by id_grado";
				$lista=viewsql($sql);
				foreach($lista as $reglis)
				{
					$grado = " ".$reglis->grado." ";
					$chk = "";
					if (strpos($gradoscont,$grado) > 0)
						$chk = "checked";
					if ($reglis->grado > 0)
						$grado.="&deg;";
					?>
					<td><b><?php echo $grado;?></b> <input type='checkbox' name='grado' <?php echo $chk;?>></td>
					<?php
				}
				?>																
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr height="36">
		<td colspan="5" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivpro"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_profesor!="" and $_SESSION["nivpro"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
