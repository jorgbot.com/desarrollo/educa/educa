<?php
/***********************************************************************************************
* @file        : alumnos_notas.php                                                             *
* @brief       : Lista notas a�o activo del alumno                                             *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 10-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 10-Mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("database/datos_notas.php");
include("libs/fechas.php");
$id_alumno = $_GET["id_alumno"];
$ano = isset($_GET['ano']) ? $_GET['ano'] : $_SESSION["anoaca"];
$op = isset($_GET['op']) ? $_GET['op'] : null ;
$periodo = isset($_GET['periodo']) ? $_GET['periodo'] : null ;
$pertipo = substr($periodo,0,1);
$periodo = substr($periodo,1,1);
$opcion = "notas";
connect();
include("includes/menu_titulo.php");
//promediar notas periodo o examen seleccionado
if ($op=="promediar_alumno")
{
	include_once("database/grabar_notas.php");
	$titulo = "PERIODO";
	if ($pertipo == "P") //periodo de notas
	{
		if ($periodo <= 4)
			promediar_notas($ano, $periodo, $grado, $curso, $id_alumno);
		else
			promediar_anolect($ano, $grado, $curso, $id_alumno);
	}
	else //examenes
	{
		$titulo = "EXAMEN";
		promediar_examen($ano, $periodo, $grado, $curso, $id_alumno);
	}
	echo"<script>alert('Alumno retotalizado, A�O $ano $titulo $periodo');</script>";
}
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = $reganol->notalim;
	$areasper = $reganol->areasper;
	$estado = $reganol->estado;
}
//puntajes
$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
$datpunt = viewsql($sql);
//periodos academicos
if ($periodo=="")
{
	$periodo = 99;
	$pertipo = "P";
}
$cierre="";
$sql="SELECT notanum, pertipo, periodo, perplan, pernomb, estado FROM periodos WHERE ano = '$ano' order by notanum";
$datperi=viewsql($sql);
$numperi = count($datperi);
foreach($datperi as $regperi)
{
	if ($regperi->pertipo=="P") //estado real del ultimo periodo
	{
		$cierre=$regperi->estado;
		if ($periodo==$regperi->periodo and $regperi->estado=="ABIERTO")
			$periodo = 99;
	}
	if ($regperi->pertipo=="E" and $regperi->estado=="ABIERTO") //cierra todos los periodos para que traiga todas las notas a la fecha
		$regperi->estado = "CERRADO";
}
//areas del grado
$sql="SELECT sum(intensidad) as suma, id_area, area, promedio, perdida FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' group by id_area ORDER BY perdida desc, suma desc, codigo";
$datarea = viewsql($sql);
//asignaturas del grado
$sql="SELECT id_area, id_asignatura, asignatura, porcentaje, intensidad, notas, notaQ FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' and (notas = '1' or boletin = '1') ORDER BY id_area, intensidad desc, codigo";
$datasig = viewsql($sql);
//tabla notas
$notas = "y".$ano."notas";
$notasfa = "y".$ano."notasfa";
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function imprimir()
{
	var id_alumno = document.form1.id_alumno.value;
	var ano = document.form1.ano.value;
	var grado = document.form1.grado.value;
	var curso = document.form1.curso.value;
	var tipoedu = document.form1.tipoedu.value;
	var implogo = 'S';
	var papel = 'LEGAL';
	var periodo = '';
	if (document.form1.papel[1].checked==true)
		papel = 'LETTER';
	if (document.form1.chklogo.checked == false)
		implogo = '';
	var theURL = 'reports/rep_boletincon.php?id_alumno='+id_alumno+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&notanum=9'+'&tipoedu='+tipoedu+'&papel='+papel+'&implogo='+implogo;;
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
function totalizar()
{
	var ano = document.form1.ano.value;
	var periodo = document.form1.periodo.value;
	if (periodo!="")
	{
		var tipo = periodo.substr(0,1);
		var nume = periodo.substr(1,1);
		if (tipo=='P')
			tipo = 'PERIODO';
		else
			tipo = 'EXAMEN';
		if (nume==5)
			nume = '5 (CIERRE)';
		ok=confirm('Esta opci�n totaliza notas, promedios y puesto del Alumno en el A�O '+ano+' '+tipo+' '+nume+', desea continuar?');
		if(ok)
		{
			var id_alumno = document.form1.id_alumno.value;
			var grado = document.form1.grado.value;
			var curso = document.form1.curso.value;
			window.location.href='alumnos_notas.php?op=promediar_alumno&id_alumno='+id_alumno+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&periodo='+periodo;
		}
	}
	else
	{
		alert('Debe seleccionar per�odo a totalizar.');
		return false;
	}
}
function seleccionar()
{
	var ano = document.form0.ano.value;
	var id_alumno = document.form0.id_alumno.value;
	var theURL = 'alumnos_notas.php?id_alumno='+id_alumno+'&ano='+ano;
	window.location.href = theURL;
}
function seleccionarPeriodo()
{
	var ano = document.form1.ano.value;
	var id_alumno = document.form1.id_alumno.value;
	var periodo = document.form1.periodo.value;
	var theURL = 'alumnos_notas.php?id_alumno='+id_alumno+'&ano='+ano+'&periodo='+periodo;
	window.location.href = theURL;
}
</script>
</head>
<body>
<?php include("includes/menu_alumnos.php");?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="javascript:imprimir();" method="post" name="form1">
<input type="hidden" name="id_alumno" value="<?php echo $id_alumno;?>">
<input type="hidden" name="ano" value="<?php echo $ano;?>">
<input type="hidden" name="grado" value="<?php echo $grado;?>">
<input type="hidden" name="curso" value="<?php echo $curso;?>">
<input type="hidden" name="tipoedu" value="<?php echo $tipoedu;?>">
	<tr height="450" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="1" bgcolor="#006600" width="100%">
					<tr height="25" bgcolor='#FFFFFF'>
					<?php
					foreach($datpunt as $regpunt)
					{echo "<td width='25' bgcolor='#006600' class='titulo_campos'>&nbsp;<font color='#FFFFFF'>$regpunt->notaQ</font></td><td width='130'>&nbsp;$regpunt->notadet&nbsp;&nbsp;$regpunt->notamin - $regpunt->notamax&nbsp;</td>";}
					?>
					</tr>
					</table>
				</td>
			</tr>
			<tr valign="top">
			<td>
				<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
					<tr height="20">
						<th align="left">GRADO <?php echo $grado;?> - CURSO <?php echo $curso;?></th>
						<th width="20" title="Intensidad Horaria">IH</th>
						<th width="20" title="Porcentaje Area">%</th>
						<?php
						foreach($datperi as $regperi)
						{echo "<th width='25' title='$regperi->pernomb'>$regperi->perplan</th>";}
						if ($superacion > 0) {
						?>
						<th width="25" title="EXAMEN SUPERACION EXTRAORDINARIA">ExS</th>
						<?php } ?>
						<th width="50" title="DEFINITIVA">Final</th>
					</tr>
					<?php
					$perdidas = 0; //areas perdidas alumno
					$sumnotas = 0; //suma de areas promedio alumno
					$numnotas = 0; //numero de areas promedio alumno
					$generales = 0; //numero de asignaturas generales, no afectan perdida
					$nota = array();
					$pend = array();
					$inac = array();
					//areas
					foreach($datarea as $regarea)
					{
						$id_area = $regarea->id_area;
						$area = $regarea->area;
						$bgcolor="#FFFFFF";
						if ($regarea->perdida > 0)
							$area = "AREA ".$area;
						else
							$generales = $generales + 1;
						$nota = notas_area($ano,$id_alumno,$id_area,$periodo,$datperi);
						if ($nota[9] > 0 and $nota[9] < $notalim and $regarea->perdida > 0) //area afecta perdida del a�o
							$perdidas = $perdidas + 1;
						if ($regarea->promedio > 0) //area afecta promedio del alumno
						{
							$sumnotas = $sumnotas + $nota[9];
							$numnotas = $numnotas + 1;
							if ($generales > 0)
								$bgcolor="#FFFFD5";								
						}
						if ($generales == 0) { ?>
						<tr height="20" bgcolor="#FFFFD5">
							<td align="left">&nbsp;<b><?php echo $area;?></b></td>
							<td align="center"><b><?php echo $regarea->suma;?></b></td>
							<td align="center"></td>
							<?php
							foreach($datperi as $regperi)
							{
								?>
								<td align="center" title="<?php echo $regperi->pernomb;?>"><b><?php echo $nota[$regperi->notanum];?></b></td>
								<?php
							}
							$color = "#000000";
							if ($nota[9] < $notalim)
								$color = "#E50000";
							$notaQ = cualitativa($datpunt,$nota[9]);
							if ($superacion > 0) {
							?>
							<td align="center" style="font-weight: bold; background-color: #FFFFD5;"></td>
							<?php } ?>
							<th align="center" style="font-weight: bold; background-color: #FFFFD5;"><font color="<?php echo $color;?>"><?php echo $nota[9]." ".$notaQ;?></font></th>
						</tr>
						<?php
						} if ($generales == 1) { ?>
						<tr height="20">
							<th align="left">ASIGNATURAS GENERALES</th>
							<th></th>
							<th></th>
							<?php
							foreach($datperi as $regperi)
							{
								?>
								<th></th>
								<?php
							}
							if ($superacion > 0) {
							?>
							<th></th>
							<?php } ?>
							<th></th>
						</tr>
						<?php }
						//asignaturas
						foreach($datasig as $regasig)
						{
							if ($regasig->id_area == $id_area)
							{
								$id_asignatura = $regasig->id_asignatura;
								$nota = notas_asignatura($ano,$id_alumno,$id_asignatura,$periodo,$datperi);
								$estadoper = "";
								?>
								<tr height="20" bgcolor="<?php echo $bgcolor;?>" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='<?php echo $bgcolor;?>'" align="left">
									<td>&nbsp;&nbsp;&nbsp;<?php echo $regasig->asignatura;?></td>
									<td align="center" title="Intensidad Horaria" bgcolor="#FFFFD5"><?php echo $regasig->intensidad;?></td>
									<td align="center" title="Porcenteje Area" bgcolor="#FFFFD5"><?php echo $regasig->porcentaje;?></td>
									<?php
									foreach($datperi as $regperi)
									{
										
										$estadoper = "CONSULTA";
										if ($regperi->notanum==7 and $estado=="ABIERTO") //a�o abierto para cambios 
											$estadoper = $regperi->estado;
										?>
										<td align="center" title="<?php echo $regperi->pernomb;?>">
										<?php if ($regperi->pertipo == "P"){ ?>
										<a class="link_list" href="notas_alumno.php?id_alumno=<?php echo $id_alumno;?>&id_asignatura=<?php echo $id_asignatura;?>&periodo=<?php echo $regperi->periodo;?>&estado=<?php echo $estadoper;?>" onclick="newWindow(this.href, '', 950, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="<?php echo $regperi->pernomb;?>"><font color="#000000"><u><?php echo $nota[$regperi->notanum];?></u></font></a>
										<?php } else { ?>
										<a class="link_list" href="notas_examen.php?id_alumno=<?php echo $id_alumno;?>&id_asignatura=<?php echo $id_asignatura;?>&pertipo=E&periodo=<?php echo $regperi->periodo;?>&estado=<?php echo $estado;?>&examen=<?php echo $regperi->pernomb;?>" onclick="newWindow(this.href, '', 500, 300, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="<?php echo $regperi->pernomb;?>"><font color="#000000"><u><?php echo $nota[$regperi->notanum];?></u></font></a>
										<?php }?>
										</td>
										<?php
									}
									$notasup = "";
									$notaQ = cualitativa($datpunt,$nota[9]);
									$color = "#000000";
									if ($nota[9] < $notalim)
										$color = "#E50000";
									//examen superacion para grados con opcion activa $superacion==1
									//ultimo examen cerrado, asignatura requiere nota y activo examen superacion
									if ($superacion > '0' and $regasig->notas > '0')
									{
										$notasup = $nota[8];
										if ($notasup =="" and $nota[9] < $notalim and $estado=="ABIERTO" and $estadoper == "CERRADO")//pierde asignatura
											$notasup = "S";
									}
									if ($superacion > 0) {
									?>
									<td align="center" title="EXAMEN SUPERACION"><a class="link_list" href="notas_examen.php?id_alumno=<?php echo $id_alumno;?>&id_asignatura=<?php echo $id_asignatura;?>&pertipo=E&periodo=4&estado=<?php echo $estado;?>&examen=EXAMEN SUPERACION EXTRAORDINARIA" onclick="newWindow(this.href, '', 500, 300, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="EXAMEN SUPERACION"><font color="#000000"><u><?php echo $notasup;?></u></font></a></td>
									<?php } ?>
									<th align="center" title="DEFINITIVA"><font color="<?php echo $color;?>"><b><?php echo $nota[9]." ".$notaQ;?></b></font></th>
								</tr>
								<?php
							} //si area
						} //for asignaturas
					} //for areas 
					$promedio = 0;
					if ($numnotas > 0)
						$promedio = round($sumnotas/$numnotas,2);
					 ?>
					<tr height="20">
						<th colspan="1" align="left">&nbsp;AREAS PERDIDAS: <?php echo $perdidas;?> &nbsp;&nbsp; (M�XIMO <?php echo $areasper;?>)</th>
						<th colspan="11" align="right">&nbsp;PROMEDIO AREAS: <?php echo $promedio;?></th>
					</tr>
				</table>
			</td>
			</tr>
			</table>
		</td>
	</tr>
	<tr height="35">
		<th align="right" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<input type="radio" name="papel" value="LEGAL" checked> Papel Oficio &nbsp;&nbsp; <input type="radio" name="papel" value="LETTER"> Papel Carta
			&nbsp;&nbsp;
			<input type="checkbox" name="chklogo"> Imprimir Logo IDIC
			<input type="image" class="botones_img" src="images/btn_imprimir.png" border="0" name="imprimir"  width="70" height="24" hspace="10" alt="Imprimir" align="absmiddle">
			<?php if ($estado!="CERRADO") { ?>
			&nbsp;&nbsp;
			<select name="periodo" style="width: 140px;" onChange="javascript:seleccionarPeriodo();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			<option value="" selected>Seleccionar per�odo...</option>
			<?php
			$numper = 0;
			foreach($datperi as $regperi)
			{
				if ($regperi->estado=="CERRADO" or $regperi->estado=="ABIERTO")
				{
					$numper = $numper + 1;
					$sel = "";
					if ($regperi->periodo==$periodo or $periodo==99)
						$sel = "selected";
					echo "<option value='$regperi->pertipo$regperi->periodo' $sel>$regperi->pernomb - $regperi->estado</option>";
				}
			}
			if ($superacion > 0) {?>
			<option value="E4">EXAMEN SUPERACION</option>
			<?php
			}
			if ($cierre=="CERRADO")
			{
			?>
			<option value="P5">DEFINITIVAS Y CIERRE</option>
			<?php } ?>
			</select>
			<?php if ($numper > 0) { ?>
			<a href="javascript:;" onClick="totalizar();" title="Totalizar Areas"><img src="images/btn_totalizar.png" border="0" width="75" height="24" alt="Totalizar Areas" hspace="10" align="absmiddle"></a>
			<?php } } ?>
			&nbsp;&nbsp;
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</th>
	</tr>
</form>
</table>
</BR></BR></BR>
</body>
</html>
<?php disconnect();?>
