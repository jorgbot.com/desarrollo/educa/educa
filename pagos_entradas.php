<?php
/***********************************************************************************************
* @file        : pagos_entradas.php                                                            *
* @brief       : Lista y crear hojas de entrada para pagos de pensiones                        *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-Ago-2015                                                    *
* @Modificado  : Nora Rodriguez 21-Ago-2015                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
connect();
$ano = isset($_GET['ano']) ? $_GET['ano'] :  $_SESSION["anoaca"] ;
$mes = isset($_GET['mes']) ? $_GET['mes'] : 0 ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$id_entrada = isset($_GET['id_entrada']) ? $_GET['id_entrada'] : 0 ;
$fchentrada = isset($_GET['fchentrada']) ? $_GET['fchentrada'] : null ;
$buscar = isset($_GET['buscar']) ? $_GET['buscar'] : null ;
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : "listar" ;
if (isset($_POST['opcion'])=="buscar")
{
	$ano = $_POST['ano'];
	$tipo = $_POST['tipo'];
	$buscar = $_POST['buscar'];
	$fchentrada = $_POST['fchentrada'];
}
if ($tipo=='T')
{
	$file = "y".$ano."pagostra";
	$titulo = "Transporte";
}
else
{
	$file = "y".$ano."pagosent";
	$titulo = "Pensi�n";
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.mesent.value == '')
	{alert('Mes de entrada no v�lido...'); document.form1.mesent.focus(); return false;}
	if(document.form1.diaent.value == '')
	{alert('Dia de entrada no v�lido...'); document.form1.diaent.focus(); return false;}
        var hoy = document.form1.fchhoy.value;
	var ok=checkFechas(hoy,hoy);
	if(ok)
	{alert('Fecha superior a hoy es incorrecta...'); document.form1.diaent.focus(); return false;}
	return true;
}

function checkValMes(obj)
{
	var valcampo=obj.value;
	var intMonth = parseInt(valcampo,10);
	var mError = '';
	if (intMonth > 12 || intMonth <= 0)
		{mError='N�mero de mes no v�lido ';}
	if (mError != '')
	{
		obj.value="";
		alert(mError);
		obj.focus();
	}
}

function checkValDia(obj)
{
	var valcampo=obj.value;
	var intDay = parseInt(valcampo,10);
	var intMonth = parseInt(document.form1.mesent.value,10);
	var mError = '';
	if (intDay > 0 && mError == '')
	{
		if (intDay > 31 && (intMonth==1 || intMonth==3 || intMonth==5 || intMonth==7 || intMonth==9 || intMonth==11))
			{mError='N�mero de d�a no v�lido para ese mes';}
		if (intDay > 30 && (intMonth==4 || intMonth==6 || intMonth==8 || intMonth==10 || intMonth==12))
			{mError='N�mero de d�a no v�lido para ese mes';}
		if (intDay > 29 && intMonth==2)
			{mError='N�mero de d�a no v�lido para ese mes';}
	}
	if (mError != '')
	{
		obj.value="";
		alert(mError);
		obj.focus();
	}
}
//Chequea que fecha de entrada sea inferior a hoy
       function checkFechas(fechaInicial,fechaFinal)
        {
           var intday = parseInt(document.form1.diaent.value,10); 
           var intMonth = parseInt(document.form1.mesent.value,10);
           var intyear = parseInt(document.form1.anoent.value,10);
           if (intday < 10)
           {
               intday = "0"+intday;
           }
           if (intMonth<10)
           {
               intMonth = "0"+intMonth;
           }
           fechaInicial = intday+"/"+intMonth+"/"+intyear;

           valuesStart=fechaInicial.split("/");
           valuesEnd=fechaFinal.split("/");
 
            // Verificamos que la fecha inicial no sea posterior a la final
            var dateStart=new Date(valuesStart[2],(valuesStart[1]-1),valuesStart[0]);
            var dateEnd=new Date(valuesEnd[2],(valuesEnd[1]-1),valuesEnd[0]);
            if(dateStart<=dateEnd)
            {
                return 0;
            }
            return 1;
        }

function seleccionar()
{
	var ano = document.form0.ano.value;
	var tipo = document.form0.tipo.value;
	var theURL = 'pagos_entradas.php?ano='+ano+'&tipo='+tipo;
	window.location.href = theURL;
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="40">
		<td class="titulo"><img src='images/entradas.png' width="32" height="32" border="0" align="absmiddle"> Hojas de Entrada</td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<?php
if ($opcion=="listar")
{
$sql="";
if ($fchentrada!="")
{
	$fecha = fec_amd($fchentrada);
	$sql="select * from $file where fchentrada = '$fecha' ORDER BY id_entrada desc";
}
if ($buscar > 0)
	$sql="select * from $file where id_entrada = '$buscar' ORDER BY id_entrada desc";
if ($sql=="")
{
	$sql="select fchentrada from $file order by id_entrada desc limit 1";
	$dat=viewsql($sql);
	foreach($dat as $reg)
	{$fchentrada = $reg->fchentrada;}
	if ($fchentrada=="")
		$fchentrada = date('Y-m-d');
	$fecha = substr($fchentrada,0,8);
	$sql="select * from $file ORDER BY id_entrada desc";
}
$dat=viewsql($sql);
//echo "$sql<br>";
?>
<table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
<form action="" method="post" name="form0">
<input type='hidden' name='opcion' value='<?php echo $buscar;?>'>
<input type='hidden' name='tipo' value='<?php echo $tipo;?>'>
	<tr><td colspan="3" height="15"></td></tr>
	<tr height="32">
		<td width="6" background="images/backsilver401.gif"></td>
		<td background="images/backsilver402.gif" class="subtitulo">
			<select name="ano" style="font-size: 14px; font-weight: bold;" onChange="javascript:seleccionar();">
			<?php
			$sql="SELECT ano FROM anoslect ORDER BY ano desc";
			$datanol=viewsql($sql);
			foreach($datanol as $reganol)
			{
				$selano = "";
				if ($reganol->ano==$ano)
					$selano = "selected";
				echo "<option value='$reganol->ano' $selano>A�O $reganol->ano</option>";
			}
			?>
			</select>
		</td>
		<td align="right" background="images/backsilver402.gif">
			BUSCAR N�
			<input type="text" name="buscar" value="<?php echo $buscar;?>" style="width: 40px;">
			&nbsp;
			FECHA
			<input type="text" name="fchentrada" id="fchentrada" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form0.fchentrada, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			&nbsp;
			<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
			&nbsp;
			<a href="pagos_entradas.php?opcion=editar&tipo=<?php echo $tipo;?>&ano=<?php echo $ano;?>"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Nueva Entrada"></a>
			&nbsp;
		</td>
		<td width="6" background="images/backsilver403.gif"></td>
	</tr>
</form>
</table>
<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="95%" align="center">
	<tr height="20">
		<th width="100">Entrada</th>
		<th width="100">Fecha</th>
		<th width="100">Estado</th>
		<th></th>
	</tr>
	<?php
	foreach($dat as $reg)
	{
	$id_entrada = $reg->id_entrada;
	?>		
	<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
		<td align="center"><a class="link_list" href="pagos_entradas.php?opcion=editar&tipo=<?php echo $tipo;?>&id_entrada=<?php echo $id_entrada;?>&ano=<?php echo $ano;?>" title="ID Entrada"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"><?php echo $id_entrada;?></a></td>
		<td><?php echo fecha_larga($reg->fchentrada);?></td>
		<td><?php echo $reg->estado;?></td>
		<td></td>
	</tr>
	<?php } ?>
</table>
<br><br>
<?php } else {
$estado = "ABIERTA";
$titulo = "NUEVA ENTRADA";
$anoent=substr(date('Y-m-d'),0,4);
$mesent=substr(date('Y-m-d'),5,2);
$diaent="";
if ($id_entrada > 0)
{
	$titulo = "ENTRADA N� ".$id_entrada;
	$sql="select * from $file where id_entrada = '$id_entrada' LIMIT 1";
	$dat=viewsql($sql);
	foreach($dat as $reg)
	{
		$fchentrada = $reg->fchentrada;
		$anoent = substr($reg->fchentrada,0,4);
		$mesent = substr($reg->fchentrada,5,2);
		$diaent = substr($reg->fchentrada,8,2);
		$estado = $reg->estado;
	}
}
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_pagos.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabar_entrada'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='tipo' value='<?php echo $tipo;?>'>
<input type='hidden' name='id_entrada' value='<?php echo $id_entrada;?>'>
<input type='hidden' name='oldfchentrada' value='<?php echo $fchentrada;?>'>
<input type='hidden' name='fchhoy' value='<?php echo date('d/m/Y');?>'>
	<tr height="20">
		<th width="20"></th>
		<th class="subtitulo"><?php echo $titulo;?></th>
		<th width="20"></th>
	</tr>
	<tr><td colspan="3" height="30"></td></tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<table border="0" cellpadding="1" cellspacing="1">
				<tr height="30">
					<td>Entrada A�o</td>
					<td>
						<?php if ($_SESSION["nivpag"] >= 4 or $_SESSION["nivtra"] >= 4) { ?>
						<input type="text" name="anoent" value="<?php echo $anoent;?>" style="width: 32px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php } else { ?>
						<input type="text" name="anoent" value="<?php echo $anoent;?>" readonly style="width: 32px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php } ?>
						Mes
						<input type="text" name="mesent" value="<?php echo $mesent;?>" onBlur="javascript:checkValMes(this);" style="width: 25px;" maxlength="2" title="mes" onKeyDown="if(event.keyCode==13) event.keyCode=9;" autofocus>
						D�a
						<input type="text" name="diaent" value="<?php echo $diaent;?>" onBlur="javascript:checkValDia(this);" style="width: 24px;" maxlength="2" title="dia" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr height="30">
					<td>Estado &nbsp;</td>
					<td>
						<select name="estado" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php
						$sql="SELECT lisnom FROM listas WHERE listip = 'ENTRADAS' ORDER BY id";
                                                if ($estado == "DEFINITIVA" and (($tipo=="P" and $_SESSION["nivpag"] != 4) or ($tipo=="T" and $_SESSION["nivtra"] != 4)))
        						$sql="SELECT lisnom FROM listas WHERE listip = 'ENTRADAS' and lisnom = 'DEFINITIVA' ORDER BY id";
						$lista=viewsql($sql);
						foreach($lista as $reglis)
						{
							if ($estado == $reglis->lisnom)
								echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
							else
								echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
						}
						?>																
						</select>
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="30"></td></tr>
	<tr height="36" bgcolor="#CCCCCC" background="images/backsilver.gif">
		<td></td>
		<td align="right" valign="middle">
			<?php if ($_SESSION["nivpag"] >= 3 or $_SESSION["nivtra"] >= 3) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="5" alt="Grabar" align="absmiddle">
			<?php } if ($id_entrada > 0) { ?>
			<a href="reports/rep_pagosent.php?id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&ano=<?php echo $ano;?>&tipo=<?php echo $tipo;?>" title="Reporte..." onclick="newWindow(this.href, '', 1024, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_imprimir.png' border="0" width="75" height="24" alt="Imprimir" align="absmiddle" title="Imprimir Entrada"></a>
			<?php } ?>
			<a href="pagos_entradas.php?mes=<?php echo $mes;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
		</td>
		<td></td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
