<?php
/***********************************************************************************************
* @file        : alumnos_pagosliq.php                                                          *
* @brief       : Liquidacion de saldos del alumno                                              *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 20-jul-2015                                                    *
* @Modificado  : Nora Rodriguez 06-nov-2018                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("database/datos_pagos.php");
include("libs/fechas.php");
$id_alumno = $_GET["id_alumno"];
$ano = isset($_GET['ano']) ? $_GET['ano'] : $_SESSION["anoaca"] ;
$opcion = "liquidar";
$fchpago = date('Y-m-d');
$anopago = intval(substr($fchpago,0,4));
$mespago = substr($fchpago,5,2);
$diapago = substr($fchpago,8,2);
connect();
include("includes/menu_titulo.php");
//porcentaje de interes en matricula
$porcen = 0;
$sql="SELECT interes FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{
    $porcen = $reganol->interes; 
}
//cargar valores del grado
$metodo = "I"; //Intereses o Rangos
$pensionbas = 0;
$pensionfin = 0;
$pensionret = 0;
$sql="SELECT pension, pensionfin, pensionret FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{$pensionbas = $reggrad->pension; $pensionfin = $reggrad->pensionfin; $pensionret = $reggrad->pensionret;}
if ($pensionfin > 0 and $pensionret > 0) {
    $metodo = "R";
    $porcen = 0;
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/pagos.js'></script>
<script language="javascript" type='text/JavaScript'>
//Funcion que liquida todos los Saldos
function Liquidar()
{
	var anopag = parseInt(document.form1.anopago.value);
	var mespag = parseInt(document.form1.mespago.value);
	var diapag = parseInt(document.form1.diapago.value);
	if (anopag > 2010 && mespag > 0 && diapag > 0)
	{
		Saldos_Pension();
		Saldos_Transporte();
		Saldos_Total();
	}
}
function seleccionar()
{
	var ano = document.form0.ano.value;
	var id_alumno = document.form0.id_alumno.value;
	var theURL = 'alumnos_pagos.php?id_alumno='+id_alumno+'&ano='+ano;
	window.location.href = theURL;
}
function imprimirSaldos()
{
	var id_alumno = document.form1.id_alumno.value;
	var ano = document.form1.ano.value;
	var anopag = parseInt(document.form1.anopago.value);
	var mespag = parseInt(document.form1.mespago.value);
	var diapag = parseInt(document.form1.diapago.value);
	if (mespag <= 9)
		var mes = '0'+mespag;
	else
		var mes = mespag;
	if (diapag <= 9)
		var dia = '0'+diapag;
	else
		var dia = diapag;
	var fchcorte = anopag+'-'+mes+'-'+dia;
	var theURL = 'reports/rep_pagoscue.php?id_alumno='+id_alumno+'&ano='+ano+'&fchcorte='+fchcorte;
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
</script>
</head>
<body>
<?php include("includes/menu_alumnos.php");?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="javascript:imprimir();" method="post" name="form1">
<input type="hidden" name="id_alumno" value="<?php echo $id_alumno;?>">
<input type="hidden" name="ano" value="<?php echo $ano;?>">
<input type='hidden' name='porcentaje' value='<?php echo $porcen;?>'>
<input type="hidden" name="tipo" value="PT">
<input type="hidden" name="id_pago" value="0">
<input type="hidden" name="valor" value="0">
<input type="hidden" name="valor_con" value="0">
<input type="hidden" name="valor_efe" value="0">
<input type="hidden" name="neto" value="0">
<input type="hidden" name="interes" value="0">
<input type="hidden" name="liquida" value="0">
<input type='hidden' name='metodo' value='<?php echo $metodo;?>'>
<input type='hidden' name='pensionbas' value='<?php echo $pensionbas;?>'>
<input type='hidden' name='pensionfin' value='<?php echo $pensionfin;?>'>
<input type='hidden' name='pensionret' value='<?php echo $pensionret;?>'>
	<tr height="450" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<br><br>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr>
			<td width="30"></td>
			<td align="center">
				<table border="0" cellpadding="2" cellspacing="1" bgcolor="#C0C0C0" align="center">
					<tr>
						<th rowspan="2" width="90" class="subtitulo"><?php echo $ano;?></th>
						<th colspan="7" class="titulo">
                                                    <img src='images/pagos.png' width="40" height="40" border="0" align="absmiddle">
                                                    <?php if ($metodo=="I") {
                                                        echo "Pensiones -  Interes por mora $porcen%";
                                                    } else { ?>
                                                       Pensiones -  Rangos <?php echo number_format($pensionbas,0)." - ".number_format($pensionfin,0)." - ".number_format($pensionret,0);
                                                    }?>
                                                </th>
						<th colspan="3" class="titulo"><img src='images/rutas.png' width="40" height="40" border="0" align="absmiddle">Transportes</th>
						<th rowspan="2" width="70" class="subtitulo">Total Mes</th>
					</tr>
					<tr>
						<th width="60" class="titulo_campos">Pensi�n</th>
						<th width="60" class="titulo_campos">Pagos<br>Pensi�n</th>
						<th width="60" class="titulo_campos">Pagos<br>Interes</th>
						<th width="60" class="titulo_campos">Debe<br>Interes</th>
						<th width="60" class="titulo_campos">Debe<br>Pensi�n</th>
						<th width="60" class="titulo_campos">Interes</th>
						<th width="60" class="titulo_campos">Total</th>
						<th width="60" class="titulo_campos">Transporte</th>
						<th width="60" class="titulo_campos">Pagos</th>
						<th width="60" class="titulo_campos">Debe</th>
					</tr>
					<?php
					$valores = valores_pension($id_alumno,$ano); //arreglo pensiones del alumno
					$pagos = pagos_pension($id_alumno,$ano); //arreglo pagos del alumno
					$valorestra = valores_transporte($id_alumno,$ano); //arreglo transportes del alumno
					$pagostra = pagos_transporte($id_alumno,$ano); //arreglo pagos del alumno
					$saldos = array(); //arreglo saldos pension
					$saldostra = array(); //arreglo saldos transporte
                                        $ajupen = array(); //ajustes sobre pension
					$sumvalpen = 0;
                                        $sumvaltra = 0;
                                        $sumpagpen = 0;
					$sumpagint = 0;
                                        $sumsalint = 0;
					$sumpagtra = 0;
					$totalval = 0;
					$totalint = 0;
					$totalpen = 0;
					$totaltra = 0;
					$totalacu = 0;
                                        $pensionmit = round($pensionbas/2,0);
                                        //iniciar arreglos
                                        for($i=1; $i<=11 ;$i++)
                                        {
                                                if ($i <= 9)
                                                        $mes = "0".$i;
                                                else
                                                        $mes = $i;
                                                $saldos[$mes] = 0;
                                                $saldostra[$mes] = 0;
                                                $ajupen[$mes] = 0;
                                        }
                                        //cargar ajustes pensiones
                                        $pagosaju = "y".$ano."pagosaju";
                                        $sql="SELECT mes, valor FROM $pagosaju WHERE id_alumno = '$id_alumno' AND tipo = 'P'";
                                        $dataju = viewsql($sql);
                                        foreach($dataju as $regaju)
                                        {
                                            if (intval($regaju->mes) <= 9)
                                                $mes = "0".$regaju->mes;
                                            else
                                                $mes = $regaju->mes;
                                            $ajupen[$mes] = $ajupen[$mes] + 1;
                                            $valores[$mes] = intval($regaju->valor);
                                        }
                                        //mostrar meses
					for($i=1; $i<=11 ;$i++)
					{
						//nombres campos
						if ($i <= 9)
							$mes = "0".$i;
						else
							$mes = $i;
						$campoval = "valorpen".$mes;
						$campopag = "pagospen".$mes;
						$camposal = "saldopen".$mes;
                                                $campoaju = "ajupen".$mes;
						$campoint = "interes".$mes;
						$campointliq = "inteliq".$mes;
						$campointsal = "intesal".$mes;
						$campovaltra = "valortra".$mes;
						$camposaltra = "saldotra".$mes;
						$campopen = "pension".$mes;
						$campoacu = "acumu".$mes;
						if ($i > intval($mespago))
						{
							$valorpag = 0;
							$valorint = 0;
						}
						//saldo pension
						$saldo = 0;
						$interes = 0;
						$total = 0;
                                                //si pension rangos, no ajusts manuales y pagos inferiores a la mitad
                                                if ($metodo=='R' and $i > 1 and $pagos[$mes]<=$pensionmit and $ajupen[$mes] <= 0 and $anopago >= $ano)
                                                {
                                                     if ($i == $mespago and $diapago >= 16) {
                                                         $valores[$mes] = $pensionfin;  
                                                     }
                                                     if ($mespago > $i or $anopago > $ano) {
                                                         $valores[$mes] = $pensionret;  
                                                     }
                                                }
						$saldo = $valores[$mes] - $pagos[$mes];
						$saldos[$mes] = $saldo;
						if ($i > intval($mespago))
							$saldo = 0;
						$saldoint = $pagos[$campointsal];
						if ($metodo == "I" and $saldo > 0 and $i >= 2)
							$interes = interes_pension($ano,$mes,$saldo,$porcen,$fchpago);
						//saldo transporte	
						$saldotra = 0;
						$saldotra = $valorestra[$mes] - $pagostra[$mes];
						$saldostra[$mes] = $saldotra;
						if ($i > intval($mespago))
							$saldotra = 0;
						//sumas totales
						$pensionsum = $saldo + $interes;
						$acummes = $pensionsum + $saldotra;
						$sumvalpen = $sumvalpen + $valores[$mes];
						$sumpagpen = $sumpagpen + $pagos[$mes];
						$sumpagint = $sumpagint + $pagos[$campoint];
						$sumsalint = $sumsalint + $pagos[$campointsal];
						$sumvaltra = $sumvaltra + $valorestra[$mes];
						$sumpagtra = $sumpagtra + $pagostra[$mes];
						$totalval = $totalval + $saldo;
						$totalint = $totalint + $interes;
						$totalpen = $totalpen + $pensionsum;
						$totaltra = $totaltra + $saldotra;
						$totalacu = $totalacu + $acummes;
						$nommes = nombre_mes($mes);
						if ($mes=="01")
							$nommes = "Matricula";
						?>
						<tr bgcolor="#FFFFFF" align="center" height="22">
							<th align="left" class="subtitulo"><?php echo $nommes;?></th>
							<td align="right">
                                                            <input type="text" name="<?php echo $campoval;?>" value="<?php echo $valores[$mes];?>" style="width: 40px;" class="disabled_white" disabled>
                                                            <input type="hidden" name="<?php echo $campoaju;?>" value="<?php echo $ajupen[$mes];?>">
                                                        </td>
							<td align="right">
                                                            <input type="text" name="<?php echo $campopag;?>" value="<?php echo $pagos[$mes];?>" style="width: 40px;" class="disabled_white" disabled>
                                                        </td>
							<td align="right"><?php echo number_format($pagos[$campoint],0);?></td>
							<td align="right">
                                                            <input type="text" name="<?php echo $campointsal;?>" value="<?php echo $pagos[$campointsal];?>" style="width: 40px;" class="disabled_white" disabled>
                                                        </td>
							<td align="right">
								<input type="text" name="<?php echo $camposal;?>" value="<?php echo $saldos[$mes];?>" style="width: 40px;" class="disabled_white" disabled>
							</td>
							<td align="right">
								<input type="hidden" name="<?php echo $campoint;?>" value="<?php echo $valorint;?>">
								<input type="text" name="<?php echo $campointliq;?>" value="<?php echo $interes;?>" style="width: 40px;" class="disabled_white" disabled>
							</td>
							<td align="right"><input type="text" name="<?php echo $campopen;?>" value="<?php echo $pensionsum;?>" style="width: 55px;" class="disabled_yellow" disabled></td>
							<td align="right"><?php echo number_format($valorestra[$mes],0);?></td>
							<td align="right"><?php echo number_format($pagostra[$mes],0);?></td>
							<td align="right">
								<input type="hidden" name="<?php echo $camposaltra;?>" value="<?php echo $saldostra[$mes];?>">
								<input type="text" name="<?php echo $campovaltra;?>" value="<?php echo $saldotra;?>" style="width: 55px;" class="disabled_yellow" disabled>
							</td>
							<td align="right"><input type="text" name="<?php echo $campoacu;?>" value="<?php echo $acummes;?>" style="width: 55px;" class="disabled_yellow" disabled></td>
						</tr>
						<?php
					}
                                        //acumular pagos totales
                                        $sumpagpen = 0;
					$sumpagint = 0;
                                        $sumpagtot = 0;
					$sumpagtra = 0;
                                        $filepag = "y".$ano."pagos";
                                        $sql="SELECT * FROM $filepag WHERE id_alumno = '$id_alumno' ORDER BY fchpago, id_pago";
                                        $datpag = viewsql($sql);
                                        foreach($datpag as $regpag)
                                        {
                                                if ($regpag->tipo=='P')
                                                {
                                                    $sumpagtot = $sumpagtot + $regpag->valor;
                                                    $sumpagpen = $sumpagpen + $regpag->neto;
                                                    $sumpagint = $sumpagint + $regpag->interes;
                                                }
                                                if ($regpag->tipo=='T')
                                                {
                                                    $sumpagtra = $sumpagtra + $regpag->valor;
                                                }
                                        }
                                        $saldopen = 0;
                                        $saldotra = 0;
                                        if ($sumpagpen > $sumvalpen)
                                        {
                                            $saldopen = $sumpagpen - $sumvalpen;
                                            $sumsalint = 0;
                                        }
                                        if ($sumpagtra > $sumvalpen)
                                            $saldotra = $sumpagtra - $sumvaltra;
                                        ?>
					<tr bgcolor="#FFFFFF" align="center" height="22">
						<th align="left" class="subtitulo">Saldo +</th>
						<td align="right"></td>
						<td align="right"><?php echo number_format($saldopen,0);?></td>
						<td align="right"></td>
						<td align="right"></td>
						<td align="right"></td>
						<td align="right"><input type="hidden" name="saldoPen" value=""></td>
						<td align="right"><input type="hidden" name="saldoPenInt" value=""></td>
						<td align="right"></td>
						<td align="right"><?php echo number_format($saldotra,0);?></td>
						<td align="right"></td>
						<td align="right"><input type="hidden" name="saldoTra" value=""></td>
					</tr>
					<tr height="22">
						<th></th>
						<th align="right"><?php echo number_format($sumvalpen,0);?></th>
						<th align="right"><?php echo number_format($sumpagpen,0);?></th>
						<th align="right"><?php echo number_format($sumpagint,0);?></th>
						<th align="right"><?php echo number_format($sumsalint,0);?></th>
						<th align="right"><input type="text" name="totalval" value="<?php echo $totalval;?>" style="width: 50px;" class="disabled_total" disabled></th>
						<th align="right"><input type="text" name="totalint" value="<?php echo $totalint;?>" style="width: 50px;" class="disabled_total" disabled></th>
						<th align="right"><input type="text" name="totalpen" value="<?php echo $totalpen;?>" style="width: 55px;" class="disabled_yellow" disabled></th>
						<th align="right"><?php echo number_format($sumvaltra,0);?></th>
						<th align="right"><?php echo number_format($sumpagtra,0);?></th>
						<th align="right"><input type="text" name="totaltra" value="<?php echo $totaltra;?>" style="width: 55px;" class="disabled_yellow" disabled></th>
						<th align="right"><input type="text" name="totalacu" value="<?php echo $totalacu;?>" style="width: 55px;" class="disabled_yellow" disabled></th>
					</tr>
				</table>
			</td>
			<td width="30"></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr height="35">
		<th align="right" bgcolor="#CCCCCC" background="images/backsilver.gif">
			Pensi�n e Inter�s<input type="radio" name="destino" value="0" checked onClick="javascript:Saldos_Alumno();">&nbsp;&nbsp;Solo Pensi�n<input type="radio" name="destino" value="1" onClick="javascript:Saldos_Alumno();">&nbsp;&nbsp;Solo Inter�s<input type="radio" name="destino" value="2" onClick="javascript:Saldos_Alumno();">
			&nbsp;&nbsp;&nbsp;
			LIQUIDAR AL A�O
			<input type="text" name="anopago" value="<?php echo $ano;?>" style="width: 40px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			MES
			<input type="text" name="mespago" value="<?php echo $mespago;?>" style="width: 25px;" onBlur="javascript:checkValMes(this);" maxlength="2" title="mes" onKeyDown="if(event.keyCode==13) event.keyCode=9;" autofocus>
			DIA
			<input type="text" name="diapago" value="<?php echo $diapago;?>" style="width: 24px;" onBlur="javascript:checkValDia(this);" maxlength="2" title="dia" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			<a href="javascript:;" onClick="Liquidar();" title="Liquidar Saldos"><img src="images/btn_liquidar.png" border="0" width="70" height="24" alt="Liquidar Saldos" hspace="10" align="absmiddle"></a>
			&nbsp;&nbsp;
			<a href="javascript:;" onClick="imprimirSaldos();"><img src="images/btn_imprimir.png" border="0" width="70" height="24" alt="Imprimir" hspace="5" align="absmiddle"></a>
			&nbsp;&nbsp;
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</th>
	</tr>
</form>
</table>
</BR></BR></BR>
</body>
</html>
<?php disconnect();?>
