<?php
/***********************************************************************************************
* @file        : transporte_rutas.php                                                          *
* @brief       : Lista y crear rutas de transporte                                             *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-Ago-2015                                                    *
* @Modificado  : Nora Rodriguez 21-Ago-2015                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_transporte.php");
include("libs/fechas.php");
connect();
$id_ruta = isset($_GET['id_ruta']) ? $_GET['id_ruta'] : 0 ;
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : "listar" ;
$buscar = isset($_POST['buscar']) ? $_POST['buscar'] : null ;
$order = isset($_POST['order']) ? $_POST['order'] : "CODIGO" ;
$ano = $_SESSION["anoaca"];
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="40">
		<td class="titulo"><img src='images/rutas.png' width="40" height="40" border="0" align="absmiddle"> Rutas Transporte <?php echo $ano;?></td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<?php
if ($opcion=="listar")
{
$sql="SELECT rutas.id_ruta, rutas.ruta, rutas.recorrido, conductores.apellidos, conductores.nombres, conductores.celular, conductores.placa, conductores.marca, conductores.capacidad FROM rutas, conductores WHERE rutas.ano = '$ano' AND rutas.id_conductor = conductores.id_conductor ";
if ($buscar > 0)
	$sql.="AND rutas.ruta = '$buscar' ";
$sql.= "ORDER BY rutas.ruta";
$dat=viewsql($sql);
?>
<table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
<form action="" method="post" name="form0">
	<tr><td colspan="3" height="15"></td></tr>
	<tr height="32">
		<td width="6" background="images/backsilver401.gif"></td>
		<td align="right" background="images/backsilver402.gif">
			BUSCAR N�
			<input type="text" name="buscar" value="<?php echo $buscar;?>" size="8">
			&nbsp;
			<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
			&nbsp;
			<a href="transporte_rutas.php?opcion=editar&tipo=<?php echo $tipo;?>"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Nueva Entrada"></a>
			&nbsp;
		</td>
		<td width="6" background="images/backsilver403.gif"></td>
	</tr>
</form>
</table>
<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="95%" align="center">
	<tr height="20">
		<th width="30">Ruta</th>
		<th>Recorrido</th>
		<th width="145">Conductor</th>
		<th width="65">Celular</th>
		<th width="60">Veh�culo</th>
		<th width="55">Placas</th>
		<th width="30">Cap</th>
		<th width="35">Alumnos</th>
	</tr>
	<?php
	$numalu = 0;
	foreach($dat as $reg)
	{
	$id_ruta = $reg->id_ruta;
	$sql = "select id_alumno from transporte where id_ruta = '$id_ruta' and estado = 'ACTIVO'";
	$datalu=viewsql($sql);
	$alumnos = count($datalu);
	$numalu = $numalu + $alumnos;
	?>		
	<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
		<td align="center"><a class="link_list" href="transporte_rutas.php?opcion=editar&id_ruta=<?php echo $id_ruta;?>" title="ID Ruta"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"><?php echo $reg->ruta;?></a></td>
		<td><?php echo $reg->recorrido;?></td>
		<td><?php echo substr($reg->nombres." ".$reg->apellidos,0,30);?></td>
		<td><?php echo $reg->celular;?></td>
		<td><?php echo $reg->marca;?></td>
		<td><?php echo $reg->placa;?></td>
		<td align="center"><?php echo $reg->capacidad;?></td>
		<td align="center"><?php echo $alumnos;?></td>
	</tr>
	<?php } ?>
	<tr height="20">
		<th colspan="7" align="right">Total</th>
		<th align="center"><?php echo number_format($numalu,0);?></th>
	</tr>
</table>
<br><br>
<?php } else {
$titulo = "NUEVA RUTA";
$datruta = datos_ruta($id_ruta);
if ($id_ruta > 0)
	$titulo = "MODIFICAR RUTA";
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_transporte.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabar_ruta'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='id_ruta' value='<?php echo $id_ruta;?>'>
	<tr height="20">
		<th width="10"></th>
		<th colspan="3" class="subtitulo"><?php echo $titulo;?></th>
		<th width="10"></th>
	</tr>
	<tr><td colspan="3" height="10"></td></tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<table border="0" cellpadding="1" cellspacing="1">
				<tr height="20">
					<td>Ruta N�</td>
					<td>
						<input type="text" name="ruta" value="<?php echo $datruta["ruta"];?>" style="width: 40px;" maxlength="2" onKeyDown="if(event.keyCode==13) event.keyCode=9;" autofocus>
					</td>
				</tr>
				<tr height="20">
					<td>Recorrido</td>
					<td>
						<input type="text" name="recorrido" value="<?php echo $datruta["recorrido"];?>" style="width: 520px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr height="20">
					<td>Conductor</td>
					<td>
						<select name="id_conductor" style="width: 520px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						$sql="select id_conductor, apellidos, nombres, marca, placa, capacidad from conductores WHERE estado = 'ACTIVO' order by apellidos, nombres";
						$datcon=viewsql($sql);
						foreach($datcon as $regcon)
						{
							$selected = "";
							if (intval($datruta["id_conductor"]) == intval($regcon->id_conductor))
								$selected = "selected";
							echo "<option value='$regcon->id_conductor' $selected>$regcon->apellidos $regcon->nombres - $regcon->marca PLACA $regcon->placa CAPACIDAD $regcon->capacidad</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr height="20">
					<td>Monitor</td>
					<td>
						<select name="id_monitor" style="width: 520px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<option value="" selected></option>
						<?php
						$sql="select id_monitor, apellidos, nombres from monitores WHERE estado = 'ACTIVO' order by apellidos, nombres";
						$datmon=viewsql($sql);
						foreach($datmon as $regmon)
						{
							$selected = "";
							if (intval($datruta["id_monitor"]) == intval($regmon->id_monitor))
								$selected = "selected";
							echo "<option value='$regmon->id_monitor' $selected>$regmon->apellidos $regmon->nombres</option>";
						}
						?>																
						</select>
					</td>
				</tr>
			</table>
		</td>
		<td width="5"></td>
		<td>
			<fieldset>
			<legend class="titulo_campos">Comentarios</legend>
			<table border="0" cellpadding="1" cellspacing="1" width="100%">
				<tr>
					<td>
					<textarea name='detalles' style="width: 200px; height: 50px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datruta["detalles"];?></textarea>
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="10"></td></tr>
	<tr height="36" bgcolor="#CCCCCC" background="images/backsilver.gif">
		<td></td>
		<td colspan="3" align="right" valign="middle">
			<?php if ($_SESSION["nivtra"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="5" alt="Grabar" align="absmiddle">
			<?php } ?>
			<a href="transporte_rutas.php"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
		</td>
		<td></td>
	</tr>
</form>
</table>
<br>
<?php
$sql="SELECT transporte.id_transporte,transporte.id_alumno,transporte.grado,transporte.curso,transporte.codigo,transporte.id_ruta,transporte.id_familiar,transporte.jornada,transporte.paradero,transporte.id_rutaop,transporte.id_familiarop,transporte.paraderoop,".
"alumnos.apellidos,alumnos.nombres,alumnos.docutip,alumnos.docunum FROM transporte, alumnos ".
"WHERE (transporte.id_ruta = '$id_ruta' OR transporte.id_rutaop = '$id_ruta') AND transporte.estado = 'ACTIVO' AND transporte.id_alumno = alumnos.id_alumno ";
if ($order=="CODIGO")
	$sql.= "ORDER BY transporte.codigo";
if ($order=="CURSO")
	$sql.= "ORDER BY CAST(transporte.grado AS SIGNED), transporte.curso, alumnos.apellidos, alumnos.nombres";
if ($order=="NOMBRE")
	$sql.= "ORDER BY alumnos.apellidos, alumnos.nombres";
$datalu=viewsql($sql);
?>
<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="95%" align="center">
	<tr height="20">
		<th width="20">#</th>
		<th>Alumno</th>
		<th width="38">ID</th>
		<th colspan="2">Curso</th>
		<th>Barrio</th>
		<th>Direcci�n</th>
		<th>Paradero</th>
		<th width="20"></th>
	</tr>
	<?php
	$numalu = 0;
	foreach($datalu as $reg)
	{
	$numalu = $numalu + 1;
	$id_alumno = $reg->id_alumno;
	$id_transporte = $reg->id_transporte;
	if ($reg->id_ruta==$id_ruta)
	{
		$id_familiar = $reg->id_familiar;
		$paradero = $reg->paradero;
		$jornada = "R D";
		if ($reg->jornada==1)
			$jornada = "R";
		if ($reg->jornada==2)
			$jornada = "D";
	}
	else
	{
		$id_familiar = $reg->id_familiarop;
		$paradero = $reg->paraderoop;
		$jornada = "D";
	}
	$tipo = "";
	$apellidos = "";
	$nombres = "";
	$direccion = "";
	$barrio = "";
	$telefono = "";
	$telefofi = "";
	$celular = "";
	$sql="select tipo, apellidos, nombres, direccion, barrio, telefono, telefofi, celular from familiares where id_alumno = '$id_alumno' and id_familiar = '$id_familiar' LIMIT 1";
	$datfam=viewsql($sql);
	foreach($datfam as $regfam)
	{
		$tipo = $regfam->tipo;
		$apellidos = $regfam->apellidos;
		$nombres = $regfam->nombres;
		$direccion = $regfam->direccion;
		$barrio = $regfam->barrio;
		$telefono = $regfam->telefono;
		$telefofi = $regfam->telefofi;
		$celular = $regfam->celular;
	}
	?>		
	<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
		<td align="center"><?php echo $reg->codigo;?></td>
		<td><?php echo substr($reg->apellidos." ".$reg->nombres,0,35);?></td>
		<td><a class="link_list" href="alumnos_transporte.php?op=editar&id_alumno=<?php echo $id_alumno;?>&id_transporte=<?php echo $id_transporte;?>" onclick="newWindow(this.href, '', 980, 630, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Editar"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $id_alumno;?></a></td>
		<td width="15" align="center"><?php echo $reg->grado;?>&deg;</td>
		<td width="25" align="center"><?php echo $reg->curso;?></td>
		<td><?php echo $barrio;?></td>
		<td><?php echo $direccion;?></td>
		<td><?php echo $paradero;?></td>
		<td align="center" title="Recoger/Dejar"><?php echo $jornada;?></td>
	</tr>
	<?php } ?>
	<tr height="20">
		<th colspan="9" align="left">&nbsp;&nbsp;Total <?php echo number_format($numalu,0);?></th>
	</tr>
</table>
<br>
<?php } ?>
</body>
</html>
<?php disconnect();?>
