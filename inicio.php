<?php
/***********************************************************************************************
* @file        : inicio.php                                                                    *
* @brief       : P�gina de incio, informaci�n general, autorizada a todos los usuarios         *
* @version     : 2.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 01-Jul-2019                                                    *
* @Modificado  : Nora Rodriguez 13-Jul-2019                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
include("libs/image_resize.php");
connect();
include("includes/registros_cumple.php");
?>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="UTF-8" />    
<meta name="robots" content="noindex" />
<meta name="googlebot" content="noindex" />
<title>Educa</title>
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" href="estilos.css" />
<link rel="stylesheet" href="estilosmenu.css" />
<link rel="stylesheet" href="estilosfoto.css" />
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fotos.js'></script>
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr height="1">
        <td></td>
        <td width="1000"></td>
        <td></td>
    </tr>
    <tr height="120">
        <td background="fondos/fondolat1.jpg">&nbsp;</td>
	<td width="1000" align="center"><?php include("includes/encabezado.php");?></td>
	<td background="fondos/fondolat2.jpg">&nbsp;</td>
    </tr>
    <tr height="12">
        <td background="fondos/marco_top.gif"></td>
        <td width="1000" background="fondos/marco_top.gif"></td>
        <td  background="fondos/marco_top.gif"></td>
    </tr>
    <tr>
        <td></td>
	<td align = "center" bgcolor="#FFFFFF"><?php include("includes/menu_principal.php");?></td>
	<td></td>
    </tr>
    <tr height="535">
        <td></td>
	<td valign="top"  bgcolor="#FFFFFF">
            <table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="80">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10"></td>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr valign="top">
							<td>
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr height="32">
									<td width="6" background="images/backsilver401.gif"></td>
									<td align="left" background="images/backsilver402.gif" class="titulo"> 
                                                                            Peri&oacute;dos Acad&eacute;micos <?php echo $ano;?>
									</td>
									<td width="6" background="images/backsilver403.gif"></td>
								</tr>
							</table>
							<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
								<tr height="20">
									<th></th>
									<th>Estado</th>
									<th>Inicio</th>
									<th>Final</th>
									<th>Fallas</th>
									<th>Plazo Notas</th>
									<th>Boletines</th>
								</tr>
								<?php
								$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
								$datperi=viewsql($sql);
								foreach($datperi as $regperi)
								{
								$id_periodo = $regperi->id_periodo;
								$color = "#000000";
								if ($regperi->estado == "ABIERTO")
									$color = "#E50000";
								?>		
								<tr height="25" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
									<td><?php echo $regperi->pernomb;?></td>
									<td align="center"><font color="<?php echo $color;?>"><?php echo $regperi->estado;?></font></td>
									<td align="center"><?php echo fecha_texto($regperi->fchinicio);?></td>
									<td align="center"><?php echo fecha_texto($regperi->fchfinal);?></td>
									<td align="center"><?php echo fecha_texto($regperi->fchfallasf);?></td>
									<td align="center"><?php echo fecha_texto($regperi->fchplazo);?></td>
									<td align="center"><?php echo fecha_texto($regperi->fchentrega);?></td>
								</tr>
								<?php
								} ?>
							</table>
							</td>
							<td width="10"></td>
							<td width="300">
                                                            <table width ="100%" border="0" cellpadding="0" cellspacing="0" width="100%">
                                                                <tr>
                                                                    <td align="center" class="subtitulo">
                                                                        <img src="images/cumple1.gif" border="0" width="100" height="64" align="absmiddle" hspace="0"> Cumplea&nacute;os del Mes
                                                                    </td>
                                                                </tr>
                                                                                    
										<?php
										foreach($cumple as $registros)
										{
											$nombre = substr($registros->campo2,0,30);
											$foto = trim($registros->campo3);
											if (!file_exists($foto))
												$foto = "images/foto.gif";
											$curso = $registros->campo4;
											if ($registros->campo5 == "A")
												$img = "images/alumnosm.gif";
											else
												$img = "images/profesorsm.gif";
											?>
											<tr height="20">
												<td>
												<img src="<?php echo $img;?>" width="20" height="20" border="0" hspace="0" align="absmiddle" title="Ver Foto..." onMouseOver="javascript:abrirVentana('<?php echo $foto;?>','<?php echo $nombre;?>','<?php echo $curso;?>',event);">
												<?php
												if ($diasel==$registros->campo1)
												{ $letra = "images/letra_".substr($nombre,0,1).".gif"; ?>
												<font style="font-family: Arial Narrow, Arial; font-size: 12px; color: #E50000;"><?php echo "$registros->campo1 ";?><img src="<?php echo $letra;?>" width="23" height="23" border="0" hspace="0" align="absmiddle"><?php echo substr($nombre,1)." ".$curso;?></font>
												<?php } else { ?>
												<font style="font-family: Arial Narrow, Arial; font-size: 12px; color: #000000;"><?php echo "$registros->campo1 $nombre $curso";?> </font>
												<?php } ?>
												</td>
											</tr>
											<tr><td height='1' bgcolor='#B7C4DB'></td></tr>
											<?php
										} ?>
										</table>
                                                            

                                                        
                                                        
                                                        </td>
						</tr>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<div id="capaVentana" onmouseout="this.style.display='none'"  onclick="this.style.display='none'" onmouseover="this.style.display='block'">
		<table border="0" cellpadding="1" cellspacing="0" width="100%">
		<form action="" method="post" name="formulario">
			<tr><th><span id="data" style="background-color: #E0E7E9; font-family: Arial, Verdana, Helvetica, sans-serif; font-size: 10px; color: #000000; font-weight: bold; padding-top: 0px; padding-bottom: 0px;"></span></th></tr>
			<tr>
				<td height="160" align="center">
					<?php
					if ($foto == "")
					{$foto = "images/foto.gif"; $height = 80; $width = 80;}
					else
					{$height = 150; $width = 150;}
					?>
					<img name="foto" src="<?php echo $foto;?>" width="<?php echo $width;?>" height="<?php echo $height;?>" border="0" align="absmiddle">
				</td>
			</tr>
		</form>
		</table>
		</div>
        </td>
        <td></td>
    </tr>
    <tr>
        <td colspan="3" valign="top">
            <div id="fondo_pie">
            <?php include("includes/menu_bottom.php");?>
            </div>
        </td>
    </tr>
</table>
</body>
</html>
<?php disconnect();?>