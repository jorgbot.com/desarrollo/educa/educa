<?php
/***********************************************************************************************
* @file        : pagos_pension.php                                                             *
* @brief       : Formulario para editar o crear pagos de pensiones                             *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 17-Jul-2015                                                    *
* @Modificado  : Nora Rodriguez 17-Jul-2015                                                    *
***********************************************************************************************/
?>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
	<tr valign="top">
		<td>
			<fieldset>
			<legend class="titulo_campos">
			<?php if ($datpago["destino"] <= 0) { ?>
			Pensi�n e Inter�s<input type="radio" name="destino" value="0" checked onClick="javascript:Saldos_Pension();">&nbsp;&nbsp;Solo Pensi�n<input type="radio" name="destino" value="1" onClick="javascript:Saldos_Pension();">&nbsp;&nbsp;Solo Inter�s<input type="radio" name="destino" value="2" onClick="javascript:Saldos_Pension();">
			<?php } if ($datpago["destino"] == 1) { ?>
			Pensi�n e Inter�s<input type="radio" name="destino" value="0" onClick="javascript:Saldos_Pension();">&nbsp;&nbsp;Solo Pensi�n<input type="radio" name="destino" value="1" checked onClick="javascript:Saldos_Pension();">&nbsp;&nbsp;Solo Inter�s<input type="radio" name="destino" value="2" onClick="javascript:Saldos_Pension();">
			<?php } if ($datpago["destino"] >= 2) { ?> 
			Pensi�n e Inter�s<input type="radio" name="destino" value="0" onClick="javascript:Saldos_Pension();">&nbsp;&nbsp;Solo Pensi�n<input type="radio" name="destino" value="1" onClick="javascript:Saldos_Pension();">&nbsp;&nbsp;Solo Inter�s<input type="radio" name="destino" value="2" checked onClick="javascript:Saldos_Pension();">
			<?php } ?>
			</legend>
			<table border="0" cellpadding="0" cellspacing="0">
				<tr valign="top">
					<td>
						<table border="0" cellpadding="1" cellspacing="1">
							<tr height="22">
								<td>Entrada <?php echo $datpago["id_entrada"];?></td>
								<td><?php echo fecha_texto($datpago["fchentrada"]);?></td>
							</tr>
							<tr height="22">
								<td>A�o Lectivo&nbsp;</td>
								<td><input type="text" name="ano" value="<?php echo $datpago["ano"];?>" style="width: 35px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
							<tr height="22">
								<td>Interes Mora%&nbsp;</td>
								<td>
									<?php if ($_SESSION["nivpag"] >= 4) { ?>
									<input type="text" name="porcentaje" value="<?php echo $datpago["porcentaje"];?>" style="width: 20px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php } else { ?>
									<input type="text" name="porcentaje" value="<?php echo $datpago["porcentaje"];?>" style="width: 20px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php } ?>
								</td>
							</tr>
						</table>
					</td>
					<td width="10"></td>
					<td>
						<table border="0" cellpadding="1" cellspacing="1">
							<tr height="22">
								<td colspan="2">
									Banco
									<select name="id_banco" style="width: 135px; border: 1px solid #A7CD2C;" onChange="javascript:SeleccionarBanco();" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<?php
									$sql="SELECT id_banco, banco, numero FROM bancos ORDER BY numero";
									$dat=viewsql($sql);
									foreach($dat as $reg)
									{
										$sel = "";
										if ($reg->id_banco==$datpago["id_banco"])
											$sel = "selected";
										echo "<option value='$reg->id_banco' $sel>$reg->banco</option>";
									}
									?>
									</select>
								</td>
							</tr>
							<tr height="22">
								<td><div id="titulofecha">Consignaci�n</div></td>
								<td>
									<input type="text" name="anopago" value="<?php echo $anopago;?>" style="width: 34px; height: 18px; text-align: right; font-size: 12px; border: 1px solid #A7CD2C;" onBlur="javascript:checkValAno(this);" maxlength="4" title="A�o" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<input type="text" name="mespago" value="<?php echo substr($datpago["fchpago"],5,2);?>" style="width: 22px; height: 18px; text-align: right; font-size: 12px; border: 1px solid #A7CD2C;" onBlur="javascript:checkValMes(this);" maxlength="2" title="Mes" onKeyDown="if(event.keyCode==13) event.keyCode=9;" autofocus>
									<input type="text" name="diapago" value="<?php echo substr($datpago["fchpago"],8,2);?>" style="width: 22px; height: 18px; text-align: right; font-size: 12px; border: 1px solid #A7CD2C;" onBlur="javascript:checkValDia(this);" maxlength="2" title="Dia" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr height="22">
								<td>Valor Pagado $&nbsp;&nbsp;</td>
								<td><input type="text" name="valor" value="<?php echo $datpago["valor"];?>" style="width: 85px; height: 18px; text-align: right; font-size: 12px; border: 1px solid #A7CD2C;" onBlur="javascript:Pagos_Pension();" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			</fieldset>
			<br>
			<fieldset>
			<legend class="titulo_campos">Historial Pagos Pensiones</legend>
			<table border="0" cellpadding="2" cellspacing="1" bgcolor="#C0C0C0">
			<tr>
				<th>FchConsigna</th>
				<th colspan="2">Hoja Entrada</th>
				<th>IDPago</th>
				<th>Consigna</th>
				<th>Efectivo</th>
				<th>Total</th>
			</tr>
			<?php
			$numpag = 0;
                        $netotot = 0;
                        $intetot = 0;
                        $pagotot = 0;
			$filepag = "y".$ano."pagos";
			$sql="SELECT * FROM $filepag WHERE id_alumno = '$id_alumno' AND tipo = 'P' ORDER BY fchpago, id_pago";
			$datpag = viewsql($sql);
			foreach($datpag as $regpag)
			{
				$numpag = $numpag + 1;
				$pagotot = $pagotot + $regpag->valor;
				$netotot = $netotot + $regpag->neto;
				$intetot = $intetot + $regpag->interes;
				?>
				<tr bgcolor="#FFFFFF" height="20">
					<th align="left"><?php echo fecha_texto($regpag->fchpago);?></th>
					<td width="20"><?php echo $regpag->id_entrada;?></td>
					<td width="40"><?php echo fecha_texto($regpag->fchentrada)?></td>
					<td><?php echo $regpag->id_pago;?></td>
					<td align="right"><?php echo number_format($regpag->valor_con,0);?></td>
					<td align="right"><?php echo number_format($regpag->valor_efe,0);?></td>
					<td align="right"><?php echo number_format($regpag->valor,0);?></td>
				</tr>
			<?php
			} ?>
                                <tr> 
                                    <th colspan="7" align="right">Intereses $ <?php echo number_format($intetot,0);?> &nbsp; &nbsp;  Pensiones $ <?php echo number_format($netotot,0);?> &nbsp; &nbsp;  Total $ <?php echo number_format($pagotot,0);?></th>
                                </tr>        
			</table>			
			</fieldset>
		</td>
		<td width="10"></td>
		<td>
			<fieldset>
                        <?php if ($metodo=="I") { ?>
			<legend class="titulo_campos">Liquidaci�n Pensiones - Inter�s por Mora <?php echo $porcentaje;?>%</legend>
                        <?php } else { ?>
			<legend class="titulo_campos">Liquidaci�n Pensiones -  Rangos <?php echo number_format($pensionbas,0)." - ".number_format($pensionfin,0)." - ".number_format($pensionret,0);?></legend>
                        <?php } ?>
			<table border="0" cellpadding="2" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr>
					<th colspan="2">Pensi�n</th>
					<th>Pagos a<br>Hoy</th>
					<th>Pago<br>Interes</th>
					<th title="Debe saldo en pensiones">Debe<br>Pensi�n</th>
					<th title="Debe en intereses pagos anteriores">Debe<br>Interes</th>
					<th title="Interes que liquida a la fecha de pago">Interes<br>Liquida</th>
					<?php if ($id_pago > 0) { ?>
					<th>IdPago <?php echo $id_pago;?><br>Pensi�n</th>
					<th>IdPago <?php echo $id_pago;?><br>Interes</th>
					<?php } else { ?>
					<th>Entrada<br>Pensi�n</th>
					<th>Entrada<br>Interes</th>
					<?php } ?>
				</tr>
				<?php
				$pagosdet = "y".$ano."pagosdet";
				$sql="SELECT id_item, id_pago, mes, valor, interes, interesliq, interesaju, fchpago FROM $pagosdet WHERE id_alumno = '$id_alumno' AND tipo='P'";
				$datite = viewsql($sql);
                                $pagosaju = "y".$ano."pagosaju";
                                $sql="SELECT mes, valor FROM $pagosaju WHERE id_alumno = '$id_alumno' AND tipo = 'P' order by mes";
                                $dataju = viewsql($sql);
                                $valortot = 0;
				$pagostot = 0;
				$saldotot = 0;
				$pagostotint = 0;
				$saldototint = 0;
				$liquitot = 0;
				$netopago = 0;
				$intepago = 0;
				for($i=1; $i<=11 ;$i++)
				{
					if ($i <= 9)
						$mes = "0".$i;
					else
						$mes = $i;
					$campoval = "valorpen".$mes;
					$campopag = "pagospen".$mes;
					$camposal = "saldopen".$mes;
                                        $campoaju = "ajupen".$mes;
					$campoint = "interes".$mes;
					$campointliq = "inteliq".$mes;
					$campointsal = "intesal".$mes;
                                        $campomes = "valor".$mes;
					$ajuste = 0;
					$valorpag = 0;
                                        $ajustepen = 0;
					$valorint = 0;
                                        $swajuste = 0;
                                        //verificar si tiene ajuste en valor matricula
                                        foreach($dataju as $regaju)
                                        {
                                            if (intval($regaju->mes) == $i)
                                            {
                                              $swajuste = 1;
                                              $ajustepen = intval($regaju->valor);
                                            }
                                        }                                        
					//recorrer pagos detallados meses
                                        $pension = $valores[$mes];
					foreach($datite as $regite)
					{
                                            if (intval($regite->mes)==$i)
                                            {
                                                $saldo = $pension - $pagos[$mes];  
                                                $fchpago = $regite->fchpago;
                                                $anopag = intval(substr($fchpago,0,4));
                                                $mespag = intval(substr($fchpago,5,2));
                                                $diapag = intval(substr($fchpago,8,2));
                                                //validar pension metodo Rangos si no tiene ajuste manual
                                                if ($metodo==='R' and $i > 1 and $swajuste == 0)
                                                {
                                                    if ($diapag >= 16 and $mespag == $i) {
                                                        $pension = $pensionfin;  
                                                    }
                                                    if (($mespag > $i and $anopag==$ano) or $anopag > $ano) {
                                                        $pension = $pensionret;  
                                                    }
                                                }
                                                //Activar meses pagados en el ID Pago
                                                if ($regite->id_pago == $id_pago)
                                                {
                                                    $valorpag = $regite->valor;
                                                    $valorint = $regite->interes;
                                                    $netopago = $netopago + $regite->valor;
                                                    $intepago = $intepago + $regite->interes;
                                                    if ($regite->interesaju > 0)
                                                        $ajuste = $regite->interesaju;
                                                }
                                            }
					}
                                        $saldo = $pension - $pagos[$mes];
                                        $saldos[$mes] = $saldo;
					$saldopag = $saldos[$mes];
					$saldoint = $interes[$mes];
					if ($i > $meshoy or ($anopago < $ano and $i > 1))
					{
						$saldopag = 0;
						$saldoint = 0;
					}
					$valortot = $valortot + $valores[$mes];
					$pagostot = $pagostot + $pagos[$mes];
					$saldotot = $saldotot + $saldopag;
					$pagostotint = $pagostotint + $pagos[$campoint];
					$saldototint = $saldototint + $pagos[$campointsal];
					$liquitot = $liquitot + $interesliq[$mes];
					$nombremes = strtoupper(substr(nombre_mes($mes),0,3));
					if ($mes == "01")
						$nombremes = "MAT";
					?>
					<tr bgcolor="#FFFFFF" align="center" height="22">
						<th width="25" align="left"><?php echo $nombremes;?></th>
						<td align="right">
                                                    <input type="text" name="<?php echo $campoval;?>" value="<?php echo $pension;?>" style="width: 40px;" class="disabled_white" disabled>
                                                    <input type="hidden" name="<?php echo $campoaju;?>" value="<?php echo $ajustepen;?>">
                                                </td>
						<td align="right">
                                                    <input type="text" name="<?php echo $campopag;?>" value="<?php echo $pagos[$mes];?>" style="width: 40px;" class="disabled_white" disabled>
                                                </td>
						<td align="right">
                                                    <?php echo $pagos[$campoint];?>
                                                </td>
						<td align="right">
                                                    <input type="text" name="<?php echo $camposal;?>" value="<?php echo $saldos[$mes];?>" style="width: 40px;" class="disabled_white" disabled>
                                                </td>
						<td align="right">
                                                    <input type="hidden" name="<?php echo $campointsal;?>" value="<?php echo $pagos[$campointsal];?>">
                                                    <?php echo $pagos[$campointsal];?>
                                                </td>
						<td align="right">
                                                    <input type="text" name="<?php echo $campointliq;?>" value="<?php echo $interesliq[$mes];?>" style="width: 40px;" class="disabled_white" disabled>
                                                </td>
						<td align="right">
                                                    <input type="text" name="<?php echo $campomes;?>" value="<?php echo $valorpag;?>" style="width: 55px;" class="disabled_yellow" disabled>
                                                </td>
						<td align="right">
							<?php if ($ajuste > 0) { ?>
							<span class="titulo_error" title="Ajuste$ <?php echo $ajuste;?>">A</span>
							<?php } ?>
							<input type="text" name="<?php echo $campoint;?>" value="<?php echo $valorint;?>" style="width: 55px;" class="disabled_yellow" disabled>
						</td>
					</tr>
					<?php
				} //fin for meses
                                //remanentes del pago editado
				$remanpen="";
				$remanint="";
				if ($netopago < $datpago["neto"] and $id_pago > 0)
				{
					$remanpen = $datpago["neto"] - $netopago;
					$pagostot = $pagostot + $remanpen;
				}
                                //saldos finales a favor del alumno
                                $saldoFinPen = $netotot - $valortot;
                                $saldoFinInt = 0;
                                //if ($saldoFinPen > 0 and ($saldototint + $liquitot) > 0)
                                //    $saldoFinInt = ($intetot + $saldototint + $liquitot) - $intetot;
				?>
				<tr bgcolor="#FFFFFF" align="center" height="22">
					<th title="Saldo a Favor">+</th>
					<td align="right"></td>
					<td align="right"><?php if ($saldoFinPen > 0) echo $saldoFinPen;?></td>
					<td align="right"><?php if ($saldoFinInt > 0) echo $saldoFinInt;?></td>
					<td align="right"></td>
					<td align="right"></td>
					<td align="right"></td>
					<td align="right"><input type="text" name="remanpen" value="<?php echo $remanpen;?>" style="width: 55px;" class="disabled_yellow" disabled></td>
					<td align="right"><input type="text" name="remanint" value="<?php echo $remanint;?>" style="width: 55px;" class="disabled_yellow" disabled></td>
				</tr>
				<tr height="22">
					<th>TOT</th>
					<th align="right"><?php echo $valortot;?></th>
					<th align="right"><?php echo $netotot;?></th>
					<th align="right"><?php echo $intetot;?></th>
					<th align="right"><?php echo $saldotot;?></th>
					<th align="right"><?php echo $saldototint;?></th>
					<th align="right"><input type="text" name="liquida" value="<?php echo $liquitot;?>" style="width: 40px;" class="disabled_total" disabled></th>
					<th align="right"><input type="text" name="neto" value="<?php echo $datpago["neto"];?>" style="width: 55px;" class="disabled_yellow" readonly></th>
					<th align="right"><input type="text" name="interes" value="<?php echo $datpago["interes"];?>" style="width: 55px;" class="disabled_yellow" readonly></th>
				</tr>
			</table>
			</fieldset>
		</td>
	</tr>
</table>			
