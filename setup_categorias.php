<?php
/***********************************************************************************************
* @file        : setup_categorias.php                                                          *
* @brief       : Lista y edita tabla de categotias seg�n tipo de categoria                     *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 28-mar-2012                                                    *
* @Modificado  : Nora Rodriguez 28-mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : null ;
$id_categoria = isset($_GET['id_categoria']) ? $_GET['id_categoria'] : null ;
$tip_categoria = isset($_GET['tip_categoria']) ? $_GET['tip_categoria'] : null ;
if ($opcion == "")
	$opcion = "listar";
connect();
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.categoria.value == '')
	{alert('Falta asignar descripci�n de la categoria...');document.form1.categoria.focus(); return false;}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar esta categoria?");
	if(ok)
	{
		document.form1.op.value='borrarcategoria';
		document.form1.submit();
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/idicestrella.png' width="24" height="24" border="0" align="absmiddle"> CATEGORIAS - <?php echo $tip_categoria;?></td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<?php if ($opcion == "listar") { ?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="350" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
			<form action="" method="post" name="form0">
				<tr height="30">
					<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
						<?php if ($_SESSION["nivpar"] >= 3) { ?>
						<a href="setup_categorias.php?opcion=editar&tip_categoria=<?php echo $tip_categoria;?>"><img src="images/btn_nuevo.png" border="0" width="75" height="24" alt="Nuevo Registro" hspace="10"></a>
						<?php } ?>
					</td>
				</tr>
				<?php
				$sql="SELECT * FROM categorias where tip_categoria = '$tip_categoria' order by categoria";
				$datcategoria=viewsql($sql);
				foreach($datcategoria as $regcategoria)
				{
				$id_categoria = $regcategoria->id_categoria;
				$categoria = $regcategoria->categoria;
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><a class="link_list" href="setup_categorias.php?opcion=editar&id_categoria=<?php echo $id_categoria;?>&tip_categoria=<?php echo $tip_categoria;?>" title="ID categoria"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $categoria;?></a></td>
				</tr>
				<?php } ?>
			</form>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
<br><br>
<?php } if ($opcion == "editar") {
$titulo = "Nueva Categoria";
$categoria = "";
if ($id_categoria > 0)
{
	$sql="select * from categorias where id_categoria = '$id_categoria' limit 1";
	$datcategoria = viewsql($sql);
	foreach($datcategoria as $regcategoria)
	{
		$categoria = $regcategoria->categoria;
	}
	$titulo = "Editar Categoria";
}
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_setup.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarcategoria'>
<input type='hidden' name='id_categoria' value='<?php echo $id_categoria;?>'>
<input type='hidden' name='tip_categoria' value='<?php echo $tip_categoria;?>'>
	<tr height="10">
		<td width="30"></td>
		<td></td>
		<td width="30"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos"><?php echo $titulo;?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td height="30"></td></tr>
				<tr height="30">
					<td>
					<input type="text" name="categoria" size="50" value="<?php echo $categoria;?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr><td height="30"></td></tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivpar"] >= 3) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_categoria!="" and $_SESSION["nivpar"] >= 3) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10"></a>
			<?php } ?>
			<a href="setup_categorias.php?tip_categoria=<?php echo $tip_categoria;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
