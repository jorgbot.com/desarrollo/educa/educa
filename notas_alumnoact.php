<?php
/***********************************************************************************************
* @file        : notas_alumno.php                                                              *
* @brief       : planilla de notas por alumno de logros y actividades del periodo              *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_notas.php");
include("libs/fechas.php");
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$numero = isset($_GET['numero']) ? $_GET['numero'] : null ;
$alumtot = isset($_GET['alumtot']) ? $_GET['alumtot'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_director = isset($_GET['id_director']) ? $_GET['id_director'] : null ;
$periodo = $_GET["periodo"];
$estado = $_GET["estado"];
$pertipo = "P";
connect();
//datos asignatura
$sql="select ano, grado, asignatura, activprom from asignaturas where id_asignatura = '$id_asignatura' limit 1";
$datasig = viewsql($sql);
foreach($datasig as $regasig)
{
	$ano = $regasig->ano;
	$grado = $regasig->grado;
	$asignatura = $regasig->asignatura;
	$activprom = $regasig->activprom;
}
//traer id_alumno del curso
if ($numero > 0)
{
	$cursos = "y".$ano."cursos";
	$sql="SELECT id_alumno, apellidos, nombres, tipoedu, fchretiro FROM $cursos WHERE grado = '$grado' and curso = '$curso' and numero = '$numero' limit 1";
	$datcurs=viewsql($sql);
	foreach($datcurs as $regcurs)
	{
		$id_alumno = $regcurs->id_alumno;
		$alumno = $regcurs->apellidos." ".$regcurs->nombres;
		$tipoedu = $regcurs->tipoedu;
		$fchretiro = $regcurs->fchretiro;
	}
	$numero = intval($numero);
	$alumtot = intval($alumtot);
	$numeroant = $numero - 1;
	$numerosig = $numero + 1;
	if ($numeroant <= 0)
		$numeroant = 1;
	if ($numerosig > $alumtot)
		$numerosig = $alumtot;
}
else
{
	$sql="SELECT apellidos, nombres, tipoedu, fchretiro FROM alumnos WHERE id_alumno = '$id_alumno' limit 1";
	$datalum=viewsql($sql);
	foreach($datalum as $regalum)
	{
		$alumno = $regalum->apellidos." ".$regalum->nombres;
		$tipoedu = $regalum->tipoedu;
		$fchretiro = $regalum->fchretiro;
	}
}
//datos alumno
if ($tipoedu == "E")
	$alumno = $alumno." (ESPECIAL)";
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$areasper = $reganol->areasper;
	$notalim = $reganol->notalim;
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
}
//puntajes
$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
$datpunt = viewsql($sql);
$numpunt = count($datpunt);
//logros de la asignatura y periodo
$file = "y".$ano."logros";
$sql = "SELECT id_logro, logrocod, logro, activmin, cursos FROM $file WHERE id_asignatura = '$id_asignatura' AND periodos LIKE '%$periodo%' ";
$sql.="AND tipoedu = '$tipoedu' ORDER BY logrocod";
$datlogr=viewsql($sql);
$numlogr = count($datlogr);
//actividades de los logros de la asignatura y periodo
$fileact = "y".$ano."activ";
$sql = "SELECT * FROM $fileact WHERE id_asignatura = '$id_asignatura' AND periodo = '$periodo' ORDER BY id_logro, numero";
$datacti = viewsql($sql);
$numacti = count($datacti);
$notas = "y".$ano."notas";
$notasdet = "y".$ano."notasp".$periodo;
$readonly="readonly";
$action="";
if (($_SESSION["nivnot"] >= 2 and $estado == "ABIERTA") or $_SESSION["nivnot"] >= 4)
{
	$readonly="";
	$action="database/grabar_planillas.php";
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript'>
function checkNota(obj,idalumno,numlogro,idlogro,regact)
{
	var valobj = obj.value;
	var total = 0;
	var totalQ = '';
	var notamin = '<?php echo $notamin;?>';
	var notamax = '<?php echo $notamax;?>';
	var numpunt = '<?php echo $numpunt;?>';
	var numlogr = '<?php echo $numlogr;?>';
	var numacti = '<?php echo $numacti;?>';
	var activprom = document.form1.activprom.value;
	notamin = parseInt(notamin,10);
	notamax = parseInt(notamax,10);
	numpunt = parseInt(numpunt,10);
	numlogr = parseInt(numlogr,10);
	numacti = parseInt(numacti,10);
	if (valobj=='' || valobj=='NaN')
		var intNota = 0;
	else
		var intNota = parseInt(valobj,10);
	if (intNota > 0)
	{
		//validar minimo y maximo de la nota
		if (intNota < notamin)
		{
			obj.value = '';
			obj.focus();
			alert('Nota inv�lida, m�nima '+notamin);
			return false;
		}
		if (intNota > notamax)
		{
			obj.value = '';
			obj.focus();
			alert('Nota inv�lida, m�xima '+notamax);
			return false;
		}
	}
	else
	{
		obj.value = '';
	}
	var logro = 0;
	logro = intNota;
	//promediar nota total logro
	if (regact > 0)
	{
		var notact = 0;
		var poract = 0;
		logro = 0;
		for(var i=1;i<=regact;i++){
			notact = eval ('document.form1.logro'+idlogro+'act'+i+'.value');
			poract = eval ('document.form1.logro'+idlogro+'por'+i+'.value');
			if (notact=='' || notact=='NaN')
				notact = 0;
			else
				notact = parseInt(notact,10);
			if (poract=='' || poract=='NaN')
				poract = 0;
			else
				poract = parseInt(poract,10);
			if (activprom <= 0) //actividad por porcentaje
			{
				notact = notact*(poract/100);
				notact = Math.round(notact*100)/100;
			}
			logro = logro + notact;
		}
		if (activprom > 0 && regact > 0) //actividad por promedio
			logro = logro/regact;
		if (logro > 0)
		{
			logro = Math.round(logro * 10) / 10; //redondear a 1 decimal			
			logro = parseInt(logro,10); //tomar parte entera
		}
	}
	//ajustar topes nota total logro
	if (logro < notamin && logro > 0)
		logro = notamin; 
	if (logro > notamax)
		logro = notamax;
	//retornar nota total logro
	eval ("document.form1.logro"+idlogro+".value='"+logro+"'");
	//retornar nota total logro a la planilla padre
	eval ("opener.document.form1.alu"+idalumno+"log"+numlogro+".value='"+logro+"'");
	//promediar logros para nota total
	total = logro;
	var numdiv = 0;
	if (numlogr > 0)
	{
		var notalogro = 0;
		var totlogros = 0;
		var chkpen = '';
		var chkina = '';
		for(var i=1;i<=numlogr;i++){
			notalogro = eval ('document.form1.logro'+i+'.value');
			if (notalogro=='' || notalogro=='NaN')
				notalogro = 0;
			else
				notalogro = parseFloat(notalogro);
			chkpen = eval ('document.form1.chkpen'+i+'.checked');
			chkina = eval ('document.form1.chkina'+i+'.checked');
			if (notalogro > 0 || (chkpen == false && chkina == false))
			{
				totlogros = totlogros + notalogro;
				numdiv = numdiv + 1;
			}
		}
		if (numdiv > 0)
		{
			total = totlogros/numdiv;
			total = Math.round(total*100)/100;
			total = total.toFixed(1);
		}
	}
	//ajustar topes nota total asignatura
	if (total < notamin && total > 0)
		total = notamin; 
	if (total > notamax)
		total = notamax;
	//retornar nota total asignatura
	eval ("document.form1.notatot.value='"+total+"'");
	//retornar nota total asignatura a la planilla padre
	eval ("opener.document.form1.nota"+idalumno+".value='"+total+"'");
	//nota cualitativa
	var puntaje = '';
	var max1 = 0;
	var max2 = 0;
	var cali = '';
	for(var i=1;i<=numpunt;i++){
		puntaje = eval ('document.form1.puntaje'+i+'.value');
		max2 = parseInt(puntaje.substr(0,3),10);
		cali = puntaje.substr(3);
		if (i==1)
			max1 = -1;
		else
		{
			max1 = i - 1;
			puntaje = eval ('document.form1.puntaje'+max1+'.value');
			max1 = parseInt(puntaje.substr(0,3),10);
		}
		if (total > max1 && total <= max2)
			totalQ = cali;
	}
	eval ("document.form1.notatotQ.value='"+totalQ+"'");
	//retornar nota total asignatura a la planilla padre
	eval ("opener.document.form1.cali"+idalumno+".value='"+totalQ+"'");
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/logro.jpg' width="40" height="40" border="0" align="absmiddle"> LOGROS Y ACTIVIDADES PERIODO <?php echo $periodo;?> - <?php echo $asignatura;?></td>
		<td class="subtitulo" align="right">GRADO <?php echo $grado;?> A�O <?php echo $ano;?></td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
	<tr>
		<td colspan="2" height="30" class="tabla_color">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td>
					&nbsp;&nbsp;
					<?php if ($numero > 0) { ?>
					<a href="notas_alumno.php?numero=<?php echo $numeroant;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>"><img src="images/arrback.png" border="0" width="24" height="24" hspace="2" title="Alumno Anterior..." align="absmiddle"></a>
					<?php } ?>
					<img src="images/alumnosm.gif" width="20" height="20" border="0" hspace="0" align="absmiddle"> <b><?php echo $alumno;?></b>
					<?php if ($numero > 0) { ?>
					<a href="notas_alumno.php?numero=<?php echo $numerosig;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>"><img src="images/arrforwardt.png" border="0" width="24" height="24" hspace="2" title="Alumno Siguiente..." align="absmiddle"></a>
					<?php } ?>
					</td>
					<td class="titulo_error" align="left">
					&nbsp;
					<?php if (substr($fchretiro,0,1) > 0) { ?>
					(RETIRADO EL <?php echo fecha_texto($fchretiro);?>)
					<?php } ?>
					</td>
					<td align="right">
					<a class="link_black" href="notas_observa.php?numero=<?php echo $numero;?>&id_asignatura=<?php echo $id_asignatura;?>&curso=<?php echo $curso;?>&id_director=<?php echo $id_director;?>&pertipo=<?php echo $pertipo;?>&periodo=<?php echo $periodo;?>&estado=<?php echo $estado;?>&alumtot=<?php echo $alumtot;?>" onclick="newWindow(this.href, '', 900, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Observaciones"><img src="images/comment.png" width="16" height="16" border="0" hspace="0" align="absmiddle"> Observaciones</a>					
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="<?php echo $action;?>" method="post" name="form1">
<input type='hidden' name='op' value='planilla_alumnoact'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='curso' value='<?php echo $curso;?>'>
<input type='hidden' name='id_director' value='<?php echo $id_director;?>'>
<input type="hidden" name="periodo" value="<?php echo $periodo;?>">
<input type="hidden" name="estado" value="<?php echo $estado;?>">
<input type='hidden' name='numero' value='<?php echo $numero;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type="hidden" name="tipoedu" value="<?php echo $tipoedu;?>">
<input type="hidden" name="notamin" value="<?php echo $notamin;?>">
<input type='hidden' name='activprom' value='<?php echo $activprom;?>'>
<input type='hidden' name='alumtot' value='<?php echo $alumtot;?>'>
<?php
//cargar puntajes de evaluacion
$numpunt = 0;
foreach($datpunt as $regpunt)
{
	$numpunt = $numpunt + 1;
	$punt = trim($regpunt->notamax);
	if (strlen($punt)==1)
		$punt = "00".$punt;
	if (strlen($punt)==2)
		$punt = "0".$punt;
	$name = "puntaje".$numpunt;
	$puntaje = $punt.$regpunt->notaQ;
	?>
	<input type="hidden" name="<?php echo $name;?>" id="<?php echo $name;?>" value="<?php echo $puntaje;?>">
	<?php
}
?>
	<tr height="20">
		<th width="30">Logro</th>
		<th>Descripci�n</th>
		<th width="35" title="Pendiente Nota">Pendte</th>
		<th width="35" title="Inactivar Promedios">Inacti</th>
		<?php if ($activprom > 0) { ?>
		<th width="30"></th>
		<?php } else { ?>
		<th width="30" title="Porcentaje">%Act</th>
		<?php } ?>
		<th width="35">Nota</th>
		<th width="35">Total</th>
		<th width="35">Ajuste<br>Examen</th>
	</tr>
	<?php
	$inactivar = 0; //inactivar asignatura
	$notatot = 0;
	$notatotQ = "";
	$notatotEx = "";
	$notatotExQ = "";
	$sql="SELECT nota, notaEx, notaExF, inactivar FROM $notas WHERE id_alumno = '$id_alumno' AND id_asignatura = '$id_asignatura' AND pertipo = '$pertipo' AND periodo = '$periodo' LIMIT 1";
	$dattot=viewsql($sql);
	foreach($dattot as $regtot)
	{
		$notatot = $regtot->nota;
		if ($regtot->notaEx > 0)
			$notatotEx = $regtot->notaEx;
		if ($regtot->notaExF > 0)
			$notatotEx = $regtot->notaExF;
		$inactivar = $regtot->inactivar;
	}
	$notatotQ = cualitativa($datpunt,$notatot);
	$notatotExQ = cualitativa($datpunt,$notatotEx);
	$reg = 0; //consecutivo logros
	foreach($datlogr as $reglogr)
	{
		$reg = $reg + 1;
		$id_logro = $reglogr->id_logro;
		$activmin = intval($reglogr->activmin);
		$campolog = "logro".$id_logro;
		$campopen = "chkpen".$id_logro;
		$campoina = "chkina".$id_logro;
		$campologid = "logro".$reg;
		$campopenid = "chkpen".$reg;
		$campoinaid = "chkina".$reg;
		$titleact = "";
		if ($activmin > 0)
			$titleact = "Actividades m�nimas ".$activmin;
		//inactivar logro segun el curso
		$inactivarlog = 0;
		$chkina = "";
		$chkpen = "";
		$notalog = "";
		$notalogEx = "";
		if (strpos($reglogr->cursos,$curso) > 0)
		{
			$chkina = "checked";
			$inactivarlog = 1;
		}
                //traer notas alumno
		$sql="select nota, notaEx, numEx, pendiente, inactivar from $notasdet ".
			"where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
			"and id_actividad = '0' limit 1";
		$datnota=viewsql($sql);
		foreach($datnota as $regnota)
		{
			$notalog = intval($regnota->nota);
			if ($regnota->notaEx > 0)
				$notalogEx = $regnota->notaEx." E".$regnota->numEx;
			$inactivarlog = intval($regnota->inactivar);
			if ($inactivarlog > 0 or $inactivar > 0) //inactivar logro alumno o inactivar asignatura
				$chkina = "checked";
			if ($regnota->pendiente > 0 and $inactivarlog==0)
				$chkpen = "checked";
		}
		$regact = 0; //actividades por logro
		foreach($datacti as $regacti)
		{
			if ($regacti->id_logro == $id_logro)
				$regact = $regact + 1;
		}
		//editar notas alumno
		$editar = 1;
		if (substr($fchretiro,0,1) > 0 or $inactivar > 0 or $inactivarlog > 0) //alumno retirado o asignatura inactiva o logro inactivo
			$editar = 0;
		?>
		<tr height="20" bgcolor="#FFFFFF">
			<td><font color="#006600"><?php echo $reglogr->logrocod;?></font></td>
			<td><font color="#006600"><?php echo $reglogr->logro;?></font></td>
			<td align="center" title="Pendiente Nota"><input type="checkbox" name="<?php echo $campopen;?>" id="<?php echo $campopenid;?>" <?php echo $chkpen;?> tabindex="1" /></td>
			<td align="center" title="Inactivar Promedios">
				<?php if ($_SESSION["nivnot"] >= 4) { ?>
				<input type="checkbox" name="<?php echo $campoina;?>" id="<?php echo $campoinaid;?>" <?php echo $chkina;?> <?php echo $readonly;?> onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
				<?php } else { ?>
				<input type="checkbox" name="<?php echo $campoina;?>" id="<?php echo $campoinaid;?>" <?php echo $chkina;?> readonly onclick="javascript: return false;"onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
				<?php } ?>
			</td>
			<td></td>
			<td></td>
			<td align="center">
				<?php if ($regact <= 0 and $activmin <= 0 and $editar == 1) { ?>
				<input type="text" name="<?php echo $campolog;?>" id="<?php echo $campologid;?>" value="<?php echo $notalog;?>" <?php echo $readonly;?> style="width: 25px; border: 1px solid #009900;" onBlur="javascript:checkNota(this,'<?php echo $id_alumno;?>','<?php echo $id_logro;?>','<?php echo $reg;?>','<?php echo $regact;?>');" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } else { ?>
				<input type="text" name="<?php echo $campolog;?>" id="<?php echo $campologid;?>" value="<?php echo $notalog;?>" readonly style="width: 25px;" title="<?php echo $titleact;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php } ?>
			</td>
			<td align="center"><?php echo $notalogEx;?></td>
		</tr>
		<?php
		$num = 0;
		foreach($datacti as $regacti)
		{
			if ($regacti->id_logro == $id_logro)
			{
				$num = $num + 1;
				$id_actividad = $regacti->id_actividad;
				$porcentaje = "";
				if ($activprom <= 0)
					$porcentaje = "% ".$regacti->porcentaje;
				$campoact = "logro".$id_logro."act".$id_actividad;
				$campoactid = "logro".$reg."act".$num;
				$campopor = "logro".$reg."por".$num;
				$notaact = "";
				$sql="select nota, notaEx, numEx from $notasdet ".
					"where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
					"and id_actividad = '$id_actividad' limit 1";
				$datnota=viewsql($sql);
				foreach($datnota as $regnota)
				{$notaact = intval($regnota->nota);}
				?>		
				<tr height="20" bgcolor="#FFFFFF">
					<td></td>
					<td><?php echo $regacti->numero." - ".$regacti->actividad;?></td>
					<td></td>
					<td></td>
					<td align="center"><?php echo $porcentaje;?></td>
					<td align="center">
						<?php if ($editar==1) { ?>
						<input type="text" name="<?php echo $campoact;?>" id="<?php echo $campoactid;?>" value="<?php echo $notaact;?>" <?php echo $readonly;?> maxlength="3" style="width: 25px;  border: 1px solid #009900;" onBlur="javascript:checkNota(this,'<?php echo $id_alumno;?>','<?php echo $id_logro;?>','<?php echo $reg;?>','<?php echo $regact;?>');" onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
						<?php } else { ?>
						<input type="text" name="<?php echo $campoact;?>" id="<?php echo $campoactid;?>" value="<?php echo $notaact;?>" readonly style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"/>
						<?php } ?>
						<input type="hidden" name="<?php echo $campopor;?>" id="<?php echo $campopor;?>" value="<?php echo intval($regacti->porcentaje);?>">
					</td>
					<td></td>
					<td></td>
				</tr>
			<?php 
			}
		} 
	} ?>
	<tr height="22">
		<th colspan="5" align="right">NOTA ASIGNATURA&nbsp;</th>
		<th><input type="text" name="notatotQ" id="notatotQ" value="<?php echo $notatotQ;?>" readonly style="width: 25px; border: 0px; background-color: #E0E7E9; font-weight: bold;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></th>
		<th><input type="text" name="notatot" id="notatot" value="<?php echo $notatot;?>" readonly style="width: 30px; border: 0px; background-color: #E0E7E9; font-weight: bold;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></th>
		<th><?php echo $notatotEx." ".$notatotExQ;?></th>
	</tr>
	<tr height="36">
		<td colspan="8" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($action!="") { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
