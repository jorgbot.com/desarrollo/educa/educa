<?php
/***********************************************************************************************
* @file        : profesores_docum.php                                                             *
* @brief       : Lista y edita documentos adjuntos de un profesor                                *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 23-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 23-Mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_profesores.php");
include("libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$id_profesor = $_GET["id_profesor"];
$id_documento = isset($_GET['id_documeno']) ? $_GET['id_documento'] : null ;
$opcion = "documentos";
connect();
$sql="SELECT apellidos, nombres FROM profesores WHERE id_profesor = '$id_profesor' limit 1";
$datprof=viewsql($sql);
foreach($datprof as $regprof)
{$titulo = "DOCENTE: ".$regprof->apellidos." ".$regprof->nombres;}
//documentos existentes
$sql = "select * from documentos where id_profesor = '$id_profesor' order by documento";
$datdocu=viewsql($sql);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.documento.value=="")
	{alert("Favor digitar nombre del documento..."); document.form1.documento.focus(); return false;}
	if(document.form1.archivo.value=="")
	{alert("Favor seleccionar el archivo..."); return false;}
	else
	{
		pase=valid_file(document.form1.archivo);
		if(pase==0)
			return false;
	}
	return true;
}
function valid_file(obj)
{
 ext=obj.value.split('.');
 var j, i = ext.length - 1;
 var ok;
 for (j = 0; i >=0 ; i--, j++)
 {
 	if(ext[j]=='xls' || ext[j]=='doc' || ext[j]=='docx' || ext[j]=='pdf' || ext[j]=='ppt' || ext[j]=='zip'  || ext[j]=='jpg' || ext[j]=='gif' || ext[j]=='png')
	ok=1;   
 }
 if(ok!=1)
 {alert('Los documentos validos son: \n'+ "- XLS \n"+"- DOC \n"+"- PPT \n"+"- PDF \n"+"- JPG \n"+"- PNG \n"+"- GIF");
 return (0);
 }
}
</script>
</head>
<body>
<?php include("includes/menu_profesores.php");?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_profesores.php" method="post" name="form1" onSubmit="return validar();" enctype="multipart/form-data">
<input type='hidden' name='op' value='grabardocumento'>
<input type='hidden' name='id_documento' value='<?php echo $id_documento;?>'>
<input type='hidden' name='id_profesor' value='<?php echo $id_profesor;?>'>
	<tr><td colspan="3" height="15"></td></tr>
	<tr height="320">
		<td valign="top">
		<table border="0" cellpadding="0" cellspacing="0" width="100%">
			<tr height="15"><td colspan="3"></td></tr>
			<tr>
				<td width="30"></td>
				<td>
					<?php if (count($datdocu) > 0) { ?>
					<fieldset>
					<legend class="titulo_campos">Documentos Existentes</legend>
					<br>
					<table border="0" cellpadding="0" cellspacing="1" width="100%" bgcolor="#B7C4DB">
						<tr>
							<th>Nombre Documento</th>
							<th width="180">Ubicaci�n Archivo</th>
							<th width="60">Abrir</th>
							<th width="60">Borrar</th>
						</tr>
						<?php
						foreach($datdocu as $registros)
						{
						$id_documento=$registros->id_documento;
						$archivo=$registros->archivo;
						?>
						<tr bgcolor="#FFFFFF" height="20" ONMOUSEOVER="this.bgColor='#FFFBD6'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
							<td><?php echo $registros->documento;?></td>
							<td><?php echo $archivo;?></td>							
							<td align="center"><a href="<?php echo $archivo;?>" target="_blank"><img src='images/periodos.png' width='20' height='15' border="0" hspace="2" align="absmiddle" title="Abrir"></a></td>
							<td align="center">
								<?php if ($_SESSION["nivpro"] >= 2) { ?>
								<a href="database/grabar_profesores.php?op=borrardocumento&id_documento=<?php echo $id_documento;?>&id_profesor=<?php echo $id_profesor;?>&archivo=<?php echo $archivo;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><img src="images/b_drop.png" width="16" height="16" border="0" align="absmiddle" title="Borrar"></a>
								<?php } ?>
							</td>
						</tr>
						<?php } ?>
					</table>
					<br>
					</fieldset>
					<?php } ?>
				</td>
				<td width="30"></td>
			</tr>
			<tr height="15"><td colspan="3"></td></tr>
			<tr>
				<td width="30"></td>
				<td>
					<fieldset>
					<legend class="titulo_campos">Nuevo Documento</legend>
					<table border="0" cellpadding="1" cellspacing="1">
						<tr height="1"><td colspan="2"></td></tr>
						<tr>
							<td>documento</td>
							<td><input type="text" name="documento" size="90" maxlength="50" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
						</tr>
						<tr>
							<td>Archivo</td>
							<td><input type="file" name="archivo" style="width: 480px;"/></td>
						</tr>
						<tr height="1"><td colspan="2"></td></tr>
					</table>
					</fieldset>
				</td>
				<td width="30"></td>
			</tr>
			<tr height="15"><td colspan="3"></td></tr>
		</table>
		</td>
	</tr>
	<tr><td colspan="3" height="40"></td></tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivpro"] >= 2) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
