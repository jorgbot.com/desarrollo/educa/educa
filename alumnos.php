<?php
/***********************************************************************************************
* @file        : alumnos.php                                                                   *
* @brief       : Muestra la lista general de alumnos para selecci�n o creaci�n                 *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 15-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 15-feb-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivalu"] <= 0)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</script>";}
include("database/database.php");
include("libs/fechas.php");
connect();
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$op = isset($_POST['op']) ? $_POST['op'] : null ;
$buscar = isset($_POST['buscar']) ? $_POST['buscar'] : null ;
if ($op == "buscar")
{
	$ano = $_POST['ano'];
	$gradocurso = $_POST['gradocurso'];
	$pos = strpos($gradocurso,"*");
	$grado = substr($gradocurso,0,$pos);
	$curso = substr($gradocurso,$pos+1);
}
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
//grado
$gradocolor = "#FFFFFF";
$sql="select grado, gradocolor, gradofont from grados where grado='$grado' limit 1";
if ($grado == "")
	$sql="SELECT grado, gradocolor, gradofont FROM grados order by id_grado limit 1";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{
	$grado = $reggrad->grado;
	$gradocolor = $reggrad->gradocolor;
	$gradofont = $reggrad->gradofont;
}
//LISTA DE ALUMNOS
$sql="SELECT alumnos.id_alumno, alumnos.apellidos, alumnos.nombres, alumnos.estado, alumnos.docunum, alumnos.docutip, alumnos.fchnace, matriculas.ano, matriculas.grado, matriculas.curso, matriculas.estado as matricula FROM alumnos, matriculas ".
    "WHERE alumnos.id_alumno = matriculas.id_alumno ";
if ($buscar != "")
{
	if ($buscar > 0)
		$sql.="AND (alumnos.id_alumno = '$buscar' or alumnos.docunum = '$buscar') ";
	else
	{
		$buscar = strtoupper($buscar);
		$sql.="AND (alumnos.apellidos like '$buscar%' or alumnos.nombres like '$buscar%') ";
	}
	$sql.= "ORDER BY alumnos.apellidos, alumnos.nombres, matriculas.ano DESC";
}
else
{
	$sql.="AND matriculas.ano = '$ano' AND matriculas.grado = '$grado' ";
	if ($curso != "")
		$sql.="AND matriculas.curso = '$curso' ";
	$sql.= "ORDER BY alumnos.apellidos, alumnos.nombres";
}
$datalum=viewsql($sql);
//echo "$sql<br>";
?>
<!DOCTYPE html>
<html lang="es"><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>Educa</title>
<!-- InstanceEndEditable -->
<meta charset="utf-8" />
<meta name="robots" content="noindex" />
<meta name="googlebot" content="noindex" />
<!-- InstanceBeginEditable name="head" -->
<link rel="shortcut icon" href="images/favicon.ico" />
<link rel="stylesheet" href="estilos.css" />
<link rel="stylesheet" href="estilosmenu.css" />
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript'>
function seleccionar()
{
	var ano = document.form0.ano.value;
	var pos = document.form0.gradocurso.value.indexOf('*');
	var grado = document.form0.gradocurso.value.substr(0,pos);
	var curso = document.form0.gradocurso.value.substr(pos+1);
	var theURL = 'alumnos.php?ano='+ano+'&grado='+grado+'&curso='+curso;
	window.location.href = theURL;
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#E2DDDA">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="120" bgcolor="#D1CCC9">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr>
		<td></td>
		<td align = "center" bgcolor="#FFFFFF">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_principal.php");?><!-- InstanceEndEditable -->
		</td>
		<td></td>
	</tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="150">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10">
				</td>
				<td>
					<div class="div_marco">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr height="40">
							<td width="50" align="center"><img src='images/alumno.png' width="40" height="40" border="0" align="absmiddle"></td>
							<td class="titulo">Alumnos</td>
							<td align="right">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>&nbsp;&nbsp;<a class="link_black" href="horarios.php?ano=<?php echo $ano;?>&grado=<?php echo $grado;?>" onclick="newWindow(this.href, '', 1024, 570, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Horarios..."><img src="images/horario.gif" width="28" height="28" border="0" hspace="0" align="absmiddle">Horarios</a>&nbsp;&nbsp;</td>
									<td width="1" bgcolor="#FFCC00"></td>
									<td>&nbsp;&nbsp;<a class="link_black" href="alumnos_cursos.php?ano=<?php echo $ano;?>&grado=<?php echo $grado;?>" onclick="newWindow(this.href, '', 930, 500, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Cursos..."><img src="images/cursos.png" width="28" height="28" border="0" hspace="0" align="absmiddle">Cursos</a>&nbsp;&nbsp;</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</div>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<form action="" method="post" name="form0">
					<input type="hidden" name="op" value="buscar">
						<tr><td colspan="3" height="15"></td></tr>
						<tr height="32">
							<td width="6" background="images/backsilver401.gif"></td>
							<td align="right" background="images/backsilver402.gif">
								BUSCAR...
								<input type="text" name="buscar" value="<?php echo $buscar;?>" size="25" style="text-transform:uppercase;">
								&nbsp;&nbsp;
								<select name="gradocurso" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
								<option value="" selected>Grado y Curso...</option>
								<?php
								$sql = "SELECT grado, gradonom FROM grados order by id_grado";
								$datgrad = viewsql($sql);
								foreach($datgrad as $reggrad)
								{
									$lgrado = $reggrad->grado;
									$lcurso = "";
									if ($lgrado==$grado and $lcurso==$curso)
										echo "<option value='$lgrado*$lcurso' selected>GRADO $reggrad->gradonom</option>";
									else
										echo "<option value='$lgrado*$lcurso'>GRADO $reggrad->gradonom</option>";
									$sql = "SELECT grado, curso FROM cursos WHERE ano = '$ano' AND grado = '$lgrado' order by curso";
									$datcurs = viewsql($sql);
									foreach($datcurs as $regcurs)
									{
										if ($regcurs->grado==$grado and $regcurs->curso==$curso)
										echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
										else
										echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
									}
								}
								?>																
								</select>
								&nbsp;
								<select name="ano" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
								<?php
								$sql="SELECT ano FROM anoslect ORDER BY ano desc";
								$datanol=viewsql($sql);
								foreach($datanol as $reganol)
								{
									$selano = "";
									if ($reganol->ano==$ano)
										$selano = "selected";
									echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
								}
								?>
								</select>
								&nbsp;
								<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
								<?php if ($_SESSION["nivalu"] >= 3) { ?>
								&nbsp;
								<a href="alumnos_edit.php" onclick="newWindow(this.href, '', 980, 630, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
								<?php } ?>
							</td>
							<td width="6" background="images/backsilver403.gif"></td>
						</tr>
					</form>
					</table>
					<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="100%">
						<tr height="20">
							<th width="20"></th>
							<th>Apellidos y Nombres</th>
							<th>ID</th>
							<th width="80">Estado</th>
							<th>Identificaci�n</th>
							<th>Tipo</th>
							<th>Edad</th>
							<th>A�o</th>
							<th>Matr�cula</th>
							<th>Curso</th>
							<th width="25" title="Fallas">Fallas</th>
						</tr>
						<?php
                                                $idalu = 0;
						foreach($datalum as $regalum)
						{
						$id_alumno = $regalum->id_alumno;
						$edad = calculo_edad($regalum->fchnace);
						$seccion = "seccion".$id_alumno;
                                                if ($idalu!=$id_alumno)
                                                {
                                                 $idalu = $regalum->id_alumno;
						?>		
						<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
							<td align="center" bgcolor="<?php echo $gradocolor;?>" class="titulo_campos"><font color="<?php echo $gradofont;?>"><?php echo $regalum->grado;?>&deg;</font></td>
							<td><?php echo $regalum->apellidos." ".$regalum->nombres;?></td>
							<td><a name="<?php echo $seccion;?>" class="link_list" href="alumnos_edit.php?id_alumno=<?php echo $id_alumno;?>" onclick="newWindow(this.href, '', 980, 630, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Editar"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $id_alumno;?></a></td>
							<td><?php echo $regalum->estado;?></td>
							<td><?php echo $regalum->docunum;?></td>
							<td><?php echo $regalum->docutip;?></td>
							<td align="center"><?php echo $edad;?></td>
							<td align="center"><?php echo $regalum->ano;?></td>
							<td><?php echo $regalum->matricula;?></td>
							<td align="center"><b><?php echo $regalum->curso;?></b></td>
							<td align="center"><a href="alumnos_fallas.php?id_alumno=<?php echo $id_alumno;?>" onclick="newWindow(this.href, '', 900, 630, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Fallas..."><img src="images/fallasm.png" width="16" height="16" border="0" hspace="0" align="absmiddle" title="Fallas..."></a></td>
						</tr>
                                                <?php }} ?>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr>
		<td colspan="3" valign="top">
		<div id="fondo_pie">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</div>
		</td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>