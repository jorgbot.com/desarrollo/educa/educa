<?php
/***********************************************************************************************
* @file        : notas_imprimir.php                                                            *
* @brief       : p�gina que permite seleccionar formato de impresi�n de planilla de notas      *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
$id_asignatura = $_GET["id_asignatura"];
$id_profasig = $_GET["id_profasig"];
$pertipo = $_GET["pertipo"];
$periodo = $_GET["periodo"];
$notanum = $_GET["notanum"];
$tipoedu = $_GET["tipoedu"];
$grado = $_GET["grado"];
$curso = $_GET["curso"];
$ano = $_SESSION["anoaca"];
include("database/database.php");
connect();
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<link rel="stylesheet" type="text/css" href="estilos.css">
<script language='JavaScript'>
function imprimir()
{
	var ano = document.form1.ano.value;
	var id_asignatura = document.form1.id_asignatura.value;
	var id_profasig = document.form1.id_profasig.value;
	var pertipo = document.form1.periodo.value.substr(0,1);
	var periodo = document.form1.periodo.value.substr(1,1);
	var notanum = document.form1.periodo.value.substr(2,1);
	var tipoedu = document.form1.tipoedu.value;
	var grado = document.form1.grado.value;
	var curso = document.form1.curso.value;
	var implogo = 'S';
	var impnotas = 'S';
	var papel = 'LEGAL';
	if (document.form1.papel[1].checked==true)
		papel = 'LETTER';
	if (document.form1.chklogo.checked == false)
		implogo = '';
	var theURL= 'reports/rep_planilla.php';
	if (document.form1.formato[1].checked==true)
		theURL= 'reports/rep_planillagen.php';
	if (document.form1.formato[2].checked==true)
		impnotas = '';
	theURL = theURL+'?id_asignatura='+id_asignatura+'&id_profasig='+id_profasig+'&pertipo='+pertipo+'&periodo='+periodo+'&tipoedu='+tipoedu+'&impnotas='+impnotas+'&papel='+papel+'&implogo='+implogo;
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/printer.jpg' width="40" height="40" border="0" align="absmiddle">IMPRIMIR PLANILLA</td>
		<td class="subtitulo" align="right"></td>
	</tr>
	<tr><td colspan="2" height="10"></td></tr>
</table>
<table width="95%" border="0" cellpadding="0" cellspacing="0" class="tabla_marco" align="center">
<form action="javascript:imprimir();" method="post" name="form1">
<input type="hidden" name="id_asignatura" value="<?php echo $id_asignatura;?>">
<input type="hidden" name="id_profasig" value="<?php echo $id_profasig;?>">
<input type="hidden" name="ano" value="<?php echo $ano;?>">
<input type="hidden" name="tipoedu" value="<?php echo $tipoedu;?>">
<input type="hidden" name="grado" value="<?php echo $grado;?>">
<input type="hidden" name="curso" value="<?php echo $curso;?>">
	<tr>
		<td width="20"></td>
		<td align="center">
			<fieldset>
			<legend class="titulo_campos">Formatos</legend>
			<table border="0">
				<tr><td height="5"></td></tr>
				<tr><td height="25"><input type="radio" name="formato" value="0" checked> Planilla Notas del Periodo/Examen</td></tr>
				<tr><td height="25"><input type="radio" name="formato" value="1"> Planilla Notas Generales</td></tr>
				<tr><td height="25"><input type="radio" name="formato" value="2"> Formato de Planilla 
				<select name="periodo" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				<?php
				$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
				$datperi=viewsql($sql);
				foreach($datperi as $regperi)
				{
					if ($regperi->notanum == $notanum)
						echo "<option value='$regperi->pertipo$regperi->periodo$regperi->notanum' selected>$regperi->pernomb</option>";
					else
						echo "<option value='$regperi->pertipo$regperi->periodo$regperi->notanum'>$regperi->pernomb</option>";
				}
				?>
				</select>
				</td></tr>
				<tr><td height="5"></td></tr>
			</table>
			</fieldset>
		</td>
		<td width="20"></td>
		<td align="center">
			<fieldset>
			<legend class="titulo_campos">Opciones</legend>
			<table border="0">
				<tr><td height="10"></td></tr>
				<tr><td height="30"><input type="radio" name="papel" value="LEGAL" checked> Papel Oficio &nbsp;&nbsp; <input type="radio" name="papel" value="LETTER"> Papel Carta</td></tr>
				<tr><td height="30"><input type="checkbox" name="chklogo" checked> Imprimir Logo IDIC</td></tr>
				<tr><td height="10"></td></tr>
			</table>
			</fieldset>
		</td>
		<td width="20"></td>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr height="36">
		<td colspan="5" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<input type="image" class="botones_img" src="images/btn_imprimir.png" border="0" name="imprimir"  width="70" height="24" hspace="10" alt="Imprimir">
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>