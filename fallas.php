<?php
/***********************************************************************************************
* @file        : fallas.php                                                                    *
* @brief       : Permite registrar inasistencias y retardos por curso                          *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 08-ago-2018                                                    *
* @Modificado  : Nora Rodriguez 10-sep-2018                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivalu"] <= 0)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</s cript>";}
include("database/database.php");
include("libs/fechas.php");
connect();
$ano = isset($_GET['ano']) ? $_GET['ano'] : 0;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$buscar = isset($_GET['buscar']) ? $_GET['buscar'] : null ;
$fecha = isset($_GET['fecha']) ? $_GET['fecha'] : "";
if ($ano <= 0) {
    $ano = $_SESSION["anoaca"];
}
if ($fecha == "") {
    $fecha = date('Y-m-d');
    //$fecha = mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2))-1, intval(substr($fecha,0,4)));
    //$fecha = date('Y-m-d',$fecha);
}
$fallas = "y".$ano."fallas";
$cursos = "y".$ano."cursos";
//fechas anterior y siguiente
if (substr($fecha,2,1)=="-")
	$fecha = fec_amd($fecha);
$fchant = mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2))-1, intval(substr($fecha,0,4)));
$fchant = date('Y-m-d',$fchant);
$fchsig = mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2))+1, intval(substr($fecha,0,4)));
$fchsig = date('Y-m-d',$fchsig);
$sfecha = $fecha;
if (substr($sfecha,4,1)=="-")
	$sfecha = fec_dma($sfecha);
$numdia = date("w",mktime(0,0,0,intval(substr($fecha,5,2)) ,intval(substr($fecha,8,2)), intval(substr($fecha,0,4))));
//cargar periodo de la fecha
$periodo = 0;
$fecha_num = strtotime(fec_dma($fecha)."00:00:00");
$sql = "SELECT periodo, fchinicio, fchfinal FROM periodos WHERE ano = '$ano' and pertipo = 'P' order by notanum";
$datperi = viewsql($sql);
foreach ($datperi as $regperi) {
    $fecha_ini = $regperi->fchinicio;
    $fecha_fin = $regperi->fchfinal;
    $fecha_ini = strtotime(fec_dma($fecha_ini)."00:00:00");
    $fecha_fin = strtotime(fec_dma($fecha_fin)."00:00:00");
    if ($fecha_num >= $fecha_ini and $fecha_num <= $fecha_fin) {
        $periodo = $regperi->periodo;
    }
}
//traer lista de cursos
$gradocolor = "#FFFFFF";
$gradofont = "#000000";
$sql="SELECT cursos.grado, cursos.curso, grados.gradonom, grados.gradocolor, grados.gradofont FROM cursos, grados where cursos.ano = '$ano' and cursos.grado = grados.grado order by grados.id_grado, cursos.curso";
$datcurs=viewsql($sql);
foreach($datcurs as $regcurs)
{
    if ($grado == "")
    {
        $grado = $regcurs->grado;
        $curso = $regcurs->curso;
    }
    if ($regcurs->grado == $grado)
    {
	$gradocolor = $regcurs->gradocolor;
	$gradofont = $regcurs->gradofont;
        break;
    }
}
//traer alumnos del curso seleccionado
if ($buscar > 0)
{
    $sql="SELECT * FROM $cursos WHERE id_alumno = '$buscar' ORDER BY apellidos, nombres";
    $datalum=viewsql($sql);
    foreach($datalum as $regalum)
    {
	$grado = $regalum->grado;
	$curso = $regalum->curso;
    }
} else {
    $sql="SELECT * FROM $cursos WHERE grado = '$grado' AND curso = '$curso' ORDER BY apellidos, nombres";
    $datalum=viewsql($sql);
}
//traer fallas del curso seleccionado
$sql="SELECT $fallas.id_alumno, $fallas.id_falla, $fallas.fecha, $fallas.fchjust, $fallas.id_categoria, $fallas.tipo, $cursos.curso FROM $fallas, $cursos WHERE $fallas.fecha = '$fecha' and $fallas.id_asignatura = '0' AND $fallas.id_alumno = $cursos.id_alumno ";
$datfall=viewsql($sql);
?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>EDUCA</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="robots" content="noindex"/>
<meta name="googlebot" content="noindex"/>
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript' type='text/JavaScript'>
function seleccionar()
{
	var ano = document.form0.ano.value;
	var fecha = document.form0.fecha.value;
	var pos = document.form0.gradocurso.value.indexOf('*');
	var grado = document.form0.gradocurso.value.substr(0,pos);
	var curso = document.form0.gradocurso.value.substr(pos+1);
	var theURL = 'fallas.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&fecha='+fecha;
	window.location.href = theURL;
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#FFFF66">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="150" bgcolor="#006600">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr height="40" bgcolor="#006600">
		<td background="fondos/top.jpg"></td>
		<td background="fondos/top.jpg" class="titulo_color">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_top.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/top.jpg"></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_top.jpg"></td></tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="150">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10">
				</td>
				<td>
					<div class="div_marco">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr height="40">
							<td width="50" align="center"><img src='images/fallas.png' width="40" height="40" border="0" align="absmiddle"></td>
							<td class="titulo">Inasistencias</td>
							<td align="right">
							<table border="0" cellpadding="0" cellspacing="0">
								<tr>
									<td>&nbsp;&nbsp;<a class="link_black" href="fallas_perm.php?tipo=A" onclick="newWindow(this.href, '', 850, 450, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Permisos..."><img src="images/permiso28.png" width="28" height="28" border="0" hspace="0" align="absmiddle">Autorizaciones</a>&nbsp;&nbsp;</td>
									<td width="1" bgcolor="#FFCC00"></td>
									<td>&nbsp;&nbsp;<a class="link_black" href="fallas_perm.php?tipo=C" onclick="newWindow(this.href, '', 850, 450, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Citaciones..."><img src="images/calendar28.png" width="28" height="28" border="0" hspace="0" align="absmiddle">Citaciones</a>&nbsp;&nbsp;</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
					</div>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <form action="" method="get" name="form0" >
                                        <input type='hidden' name='ano' value='<?php echo $ano;?>'>
                                        <input type='hidden' name='grado' value='<?php echo $grado;?>'>
                                        <input type='hidden' name='curso' value='<?php echo $curso;?>'>
					<input type="hidden" name="op" value="buscar">
						<tr><td colspan="3" height="15"></td></tr>
						<tr height="32">
							<td width="6" background="images/backsilver401.gif"></td>
                                                        <td align="left" class="titulo" background="images/backsilver402.gif">
                                                            <a href="fallas.php?fecha=<?php echo $fchant;?>&ano=<?php echo $ano;?>&grado=<?php echo $grado;?>&curso=<?php echo $curso;?>"><img src="images/arrback.png" border="0" width="24" height="24" hspace="2" title="Dia Anterior..." align="absmiddle"></a>
                                                            <?php echo fecha_completa($fecha);?>
                                                            <a href="fallas.php?fecha=<?php echo $fchsig;?>&ano=<?php echo $ano;?>&grado=<?php echo $grado;?>&curso=<?php echo $curso;?>"><img src="images/arrforwardt.png" border="0" width="24" height="24" hspace="2" title="Dia Siguiente..." align="absmiddle"></a>
                                                        </td>
							<td align="right" background="images/backsilver402.gif">
                                                        ALUMNO
                                                        <input type="text" name="buscar" value="<?php echo $buscar;?>" size="5" style="text-transform:uppercase;">
                                                        &nbsp;
                                                        <select name="gradocurso" id="gradocurso" onChange="javascript:seleccionar();">
                                                        <?php
                                                        foreach($datcurs as $regcurs)
                                                        {
                                                                if ($regcurs->grado==$grado and $regcurs->curso==$curso)
                                                                echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $regcurs->gradonom - CURSO $regcurs->curso</option>";
                                                                else
                                                                echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $regcurs->gradonom - CURSO $regcurs->curso</option>";
                                                        }
                                                        ?>																
                                                        </select>
							FECHA <input type="text" name="fecha" id="fecha" value="<?php echo fec_dma($fecha);?>" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form0.fecha, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
							<input type="image" class="botones_img" name="btnbuscaralu" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
							</td>
							<td width="6" background="images/backsilver403.gif"></td>
						</tr>
					</form>
					</table>
					<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="100%">
                                        <form action="database/grabar_fallas.php" method="post" name="form1">
                                        <input type='hidden' name='op' value='grabarfallascurso'>
                                        <input type='hidden' name='ano' value='<?php echo $ano;?>'>
                                        <input type='hidden' name='grado' value='<?php echo $grado;?>'>
                                        <input type='hidden' name='curso' value='<?php echo $curso;?>'>
                                        <input type='hidden' name='fecha' value='<?php echo $fecha;?>'>
                                        <input type='hidden' name='numdia' value='<?php echo $numdia;?>'>
                                        <input type='hidden' name='periodo' value='<?php echo $periodo;?>'>
                                        <input type='hidden' name='page' value='fallascurso'>
						<tr height="20">
                                                        <th colspan="2">Curso</th>
							<th>Apellidos y Nombres</th>
							<th>ID</th>
                                                        <th>Falla</th>
                                                        <th>Retardo</th>
                                                        <th>Permiso</th>
                                                        <th colspan="2">Excusa y Fecha</th>
                                                        <th colspan="3">Acciones</th>
						</tr>
						<?php
						$sql="select id_categoria, categoria from categorias where tip_categoria = 'INASISTENCIAS' order by categoria";
						$datcateg=viewsql($sql);
						foreach($datalum as $regalum)
						{
						$id_alumno = $regalum->id_alumno;
						$seccion = "seccion".$id_alumno;
                                                $nameop = "op".$id_alumno;
                                                $namect = "ct".$id_alumno;
                                                $id_falla = 0;
                                                $id_categoria = 0;
                                                $tipo = "";
                                                $fchjust = "";
                                                $checkina = "";
                                                $checkret = "";
                                                $checkper = "";
                                                
                                                foreach($datfall as $regfall)
                                                {
                                                    if ($regfall->id_alumno == $id_alumno)
                                                    {  
                                                        $id_falla = $regfall->id_falla;
                                                        $id_categoria = intval($regfall->id_categoria);
                                                        $tipo = $regfall->tipo;
                                                        $fchjust = fecha_texto($regfall->fchjust);
                                                        if ($regfall->tipo == "I") {
                                                          $checkina = "checked";
                                                        }
                                                        if ($regfall->tipo == "R") {
                                                          $checkret = "checked";  
                                                        }
                                                        if ($regfall->tipo == "P") {
                                                          $checkper = "checked";
                                                        }
                                                        break;
                                                    }
                                                }
						?>		
						<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
							<td align="center" bgcolor="<?php echo $gradocolor;?>" class="titulo_campos"><font color="<?php echo $gradofont;?>"><?php echo $regalum->grado;?>&deg;</font></td>
                                                        <td align="center"><?php echo $regalum->curso;?></td>
							<td><?php echo $regalum->apellidos." ".$regalum->nombres;?></td>
							<td align="center"><b><?php echo $regalum->id_alumno;?></b></td>
                                                        <td align="center"><input type="radio" name="<?php echo $nameop;?>" value="I" <?php echo $checkina;?> ></td>
							<td align="center"><input type="radio" name="<?php echo $nameop;?>" value="R" <?php echo $checkret;?> ></td>
							<td align="center"><input type="radio" name="<?php echo $nameop;?>" value="P" <?php echo $checkper;?> ></td>
                                                        <td align="center">
                                                            <select name="<?php echo $namect;?>" style="width: 200px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                                            <?php
                                                            echo "<option value='0' selected></option>";
                                                            foreach($datcateg as $regcateg)
                                                            {
                                                                    if ($id_categoria == intval($regcateg->id_categoria))
                                                                            echo "<option value='$regcateg->id_categoria' selected>$regcateg->categoria</option>";
                                                                    else
                                                                            echo "<option value='$regcateg->id_categoria'>$regcateg->categoria</option>";
                                                            }
                                                            ?>																
                                                            </select>
                                                        </td>
                                                        <td width="70"><?php echo $fchjust;?></td>
                                                        <td width="45" align="center"><?php if ($id_falla > 0) {?><a name="<?php echo $seccion;?>" class="link_list" href="alumnos_fallas.php?op=editar&id_alumno=<?php echo $id_alumno;?>&id_falla=<?php echo $id_falla;?>&page=fallascurso" onclick="newWindow(this.href, '', 950, 450, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Editar"><u>Editar</u></a><?php }?></td>
                                                        <td width="50" align="center">
                                                            <?php if ($tipo != "P" and $id_falla > 0 and $id_categoria > 0) {?><a name="imprimir" class="link_list" href="reports/rep_fallaspap.php?ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>" onclick="newWindow(this.href, '', 1000, 500, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Editar"><u>Imprimir</u></a><?php }?>
                                                            <?php if ($tipo == "P" and $id_falla > 0 and $id_categoria > 0) {?><a name="imprimir" class="link_list" href="reports/rep_fallaspapper.php?ano=<?php echo $ano;?>&id_falla=<?php echo $id_falla;?>" onclick="newWindow(this.href, '', 1000, 500, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Editar"><u>Imprimir</u></a><?php }?>
                                                        </td>
                                                        <td width="40" align="center"><a name="listar" class="link_list" href="fallas_perm.php?opcion=inasistencias&id_alumno=<?php echo $id_alumno;?>&tipo=I" onclick="newWindow(this.href, '', 800, 500, 1, 1, 1, 0, 0, 0, 0); return false;" target="_blank" title="Editar"><u>Listar</u></a></td>
						</tr>
                                                <?php } ?>
                                                <tr height="30">
                                                    <td colspan="12" background="images/backsilver402.gif" align="right">
                                                        <input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="5" alt="Grabar" align="absmiddle">
                                                    </td>
                                                </tr>
                                        </form>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_bot.jpg"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#D3A919"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FFDB4B"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FCF463"></td></tr>
	<tr>
		<td colspan="3" valign="top">
		<div id="fondo_pie">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</div>
		</td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>