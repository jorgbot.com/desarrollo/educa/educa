<?php
/***********************************************************************************************
* @file        : est_alumnos.php (ancho hoja 196)                                              *
* @brief       : estadisticas alumnos por grados                                               *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 14-may-2012                                                    *
* @Modificado  : Nora Rodriguez 14-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set("America/Bogota");
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$nivel = isset($_GET['nivel']) ? $_GET['nivel'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$antano = $ano - 1;
$titulo = "ESTADISTICAS DE ALUMNOS SEG�N RANGOS DE EDAD Y G�NERO";
$subtitulo = "A�O LECTIVO ".$ano;
connect();
//niveles
$sql= "select nivel, nombre from niveles order by id_nivel";
$datnive=viewsql($sql);
//alumnos
$sql="SELECT matriculas.grado, matriculas.curso, matriculas.id_alumno, grados.nivel, grados.gradonom, alumnos.fchnace, alumnos.genero FROM matriculas, grados, alumnos WHERE matriculas.ano = '$ano' AND matriculas.estado = 'CERRADA' AND matriculas.curso != '' AND matriculas.fchretiro like '0000%' AND matriculas.grado = grados.grado AND matriculas.id_alumno = alumnos.id_alumno ";
$condi = "";
if ($nivel != "")
{
	$nomnivel = substr($nivel,1);
	$nivel = substr($nivel,0,1);
	$condi.="AND matriculas.nivel = '$nivel' ";
	$subtitulo.=" - NIVEL ".$nomnivel;
}
if ($grado != "")
{
	$condi.="AND matriculas.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
$sql = $sql.$condi."ORDER BY grados.id_grado, matriculas.curso";
$datalum=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(3);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(7);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(1);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(4);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//titulos
	
	
   	
	

	//lista de Alumnos
	$alumest = array();
	$gradonom = "";
	$curso = "";
	
	$pdf->SetFont('Arial','',8);
	foreach($datalum as $regalum)
	{
		if ($regalum->gradonom != $gradonom)
		{
			if ($gradonom != "")
			{
				$pdf->SetFont('Arial','B',8);
				$pdf->Cell(20,5,$gradonom,1,1,'C',1);
				$pdf->Cell(20,5,"Edad",1,0,'C',1);
				$pdf->Cell(10,5,'3 - 5',1,0,'C',1);
				$pdf->Cell(10,5,'6 - 8',1,0,'C',1);
				$pdf->Cell(10,5,'9 - 12',1,0,'C',1);
				$pdf->Cell(10,5,'13 - 15',1,0,'C',1);
				$pdf->Cell(10,5,'16 o +',1,1,'C',1);
				
	
	
				$pdf->SetFont('Arial','',8);
	            $pdf->Cell(20,5,'Ni�os',1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango1,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($mrango2,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($mrango3,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango4,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango5,0),1,1,'C',1);
				
				
				
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(20,5,'Ni�as',1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango1,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($frango2,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($frango3,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango4,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango5,0),1,1,'C',1);
				
				
				
				$pdf->Cell(20,7,"TOTAL",1,0,'C');
				$pdf->Cell(10,7,number_format($mrango1+$frango1),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango2+$frango2),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango3+$frango3),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango4+$frango4),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango5+$frango5),1,1,'C');
				
				
				$pdf->Cell(182,5,"",0,1,0);
				
			}
			$gradonom = $regalum->gradonom;
			$granum = 0;
			$curnum = 0;
			$curnue = 0;
			$curant = 0;
	
	$mrango1 = 0;
	$mrango2 = 0; 
	$mrango3 = 0;
	$mrango4 = 0;
	$mrango5 = 0;

	
	$frango1 = 0;
	$frango2 = 0;
	$frango3 = 0;
	$frango4 = 0;
	$frango5 = 0;
	
		}
		
		if ($grado!="" and $gradonom != "" and $regalum->curso!=$curso)
		{
			if ($curso!="")
			{
				$pdf->SetFont('Arial','B',8);
				$pdf->Cell(20,5,$curso,1,0,'C',1);
				$pdf->Cell(20,5,"Edad",1,0,'C',1);
				$pdf->Cell(10,5,'3 - 5',1,0,'C',1);
				$pdf->Cell(10,5,'6 - 8',1,0,'C',1);
				$pdf->Cell(10,5,'9 - 12',1,0,'C',1);
				$pdf->Cell(10,5,'13 - 15',1,0,'C',1);
				$pdf->Cell(10,5,'16 o +',1,1,'C',1);
	
	
				$pdf->SetFont('Arial','',8);
	            $pdf->Cell(20,5,'Ni�os',1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango1,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($mrango2,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($mrango3,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango4,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango5,0),1,1,'C',1);
				
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(20,5,'Ni�as',1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango1,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($frango2,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($frango3,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango4,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango5,0),1,1,'C',1);
				
				
				$pdf->Cell(20,7,"TOTAL",1,0,'C');
				$pdf->Cell(10,7,number_format($mrango1+$frango1),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango2+$frango2),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango3+$frango3),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango4+$frango4),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango5+$frango5),1,1,'C');
			}
			$curso = $regalum->curso;
			$curnum = 0;
			$curnue = 0;
			$curant = 0;
			$curmas = 0;
		    $curfem = 0;
			
	$mrango1 = 0;
	$mrango2 = 0; 
	$mrango3 = 0;
	$mrango4 = 0;
	$mrango5 = 0;

	
	$frango1 = 0;
	$frango2 = 0;
	$frango3 = 0;
	$frango4 = 0;
	$frango5 = 0;
		}
		$granum = $granum + 1;
		$curnum = $curnum + 1;
		//$totnum = $totnum + 1;
		$id_alumno = $regalum->id_alumno;
	
	$sql = "SELECT id_matricula FROM matriculas WHERE id_alumno = '$id_alumno' AND matriculas.ano = '$antano' LIMIT 1";
		$datmatr = viewsql($sql);
		
		$edad = calculo_edad($regalum->fchnace);
		
		
		if($edad >= 3 and $edad <= 5){
			if($regalum->genero == "M"){
			$mrango1=$mrango1+1;			
			}
			else {
			$frango1=$frango1+1;	
			}
			
		}
		
		if($edad >= 6 and $edad <= 8){
			if($regalum->genero == "M"){
			$mrango2=$mrango2+1;			
			}
			else {
			$frango2=$frango2+1;	
			}
			
		}
		
		if($edad >= 9 and $edad <= 12){
			if($regalum->genero == "M"){
			$mrango3=$mrango3+1;			
			}
			else {
			$frango3=$frango3+1;	
			}
			
		}
		
		if($edad >= 13 and $edad <= 15){
			if($regalum->genero == "M"){
			$mrango4=$mrango4+1;			
			}
			else {
			$frango4=$frango4+1;	
			}
			
		}
		
		if($edad >= 16){
			if($regalum->genero == "M"){
			$mrango5=$mrango5+1;			
			}
			else {
			$frango5=$frango5+1;	
			}
			
		}
		
	}
	
	
	if ($grado!="")
	{
		$pdf->SetFont('Arial','B',8);
				$pdf->Cell(20,5,$curso,1,0,'C',1);
				$pdf->Cell(20,5,"Edad",1,0,'C',1);
				$pdf->Cell(10,5,'3 - 5',1,0,'C',1);
				$pdf->Cell(10,5,'6 - 8',1,0,'C',1);
				$pdf->Cell(10,5,'9 - 12',1,0,'C',1);
				$pdf->Cell(10,5,'13 - 15',1,0,'C',1);
				$pdf->Cell(10,5,'16 o +',1,1,'C',1);
	
	
				$pdf->SetFont('Arial','',8);
	            $pdf->Cell(20,5,'Ni�os',1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango1,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($mrango2,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($mrango3,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango4,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango5,0),1,1,'C',1);
				
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(20,5,'Ni�as',1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango1,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($frango2,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($frango3,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango4,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango5,0),1,1,'C',1);
				
				
				$pdf->Cell(20,7,"TOTAL",1,0,'C');
				$pdf->Cell(10,7,number_format($mrango1+$frango1),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango2+$frango2),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango3+$frango3),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango4+$frango4),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango5+$frango5),1,1,'C');
			
		
	}
	else
	{
		       $pdf->SetFont('Arial','B',8);
			   $pdf->Cell(20,5,$gradonom,1,1,'C',1);
				$pdf->Cell(20,5,"Edad",1,0,'C',1);
				$pdf->Cell(10,5,'3 - 5',1,0,'C',1);
				$pdf->Cell(10,5,'6 - 8',1,0,'C',1);
				$pdf->Cell(10,5,'9 - 12',1,0,'C',1);
				$pdf->Cell(10,5,'13 - 15',1,0,'C',1);
				$pdf->Cell(10,5,'16 o +',1,1,'C',1);
	
	
				$pdf->SetFont('Arial','',8);
	            $pdf->Cell(20,5,'Ni�os',1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango1,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($mrango2,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($mrango3,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango4,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($mrango5,0),1,1,'C',1);
				
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(20,5,'Ni�as',1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango1,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($frango2,0),1,0,'C',1);
	            $pdf->Cell(10,5,number_format($frango3,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango4,0),1,0,'C',1);
				$pdf->Cell(10,5,number_format($frango5,0),1,1,'C',1);
				
				
				$pdf->Cell(20,7,"TOTAL",1,0,'C');
				$pdf->Cell(10,7,number_format($mrango1+$frango1),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango2+$frango2),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango3+$frango3),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango4+$frango4),1,0,'C');
				$pdf->Cell(10,7,number_format($mrango5+$frango5),1,1,'C');
				
	}
	

	$pdf->Output();
	disconnect();

	
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $granumrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
