<?php
/***********************************************************************************************
* @file        : rep_alumnos.php (ancho hoja 196)                                              *
* @brief       : lista general de alumnos por grado y/o curso                                  *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 11-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 11-abr-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$matricula = isset($_GET['matricula']) ? $_GET['matricula'] : null ;
$estado = isset($_GET['estado']) ? $_GET['estado'] : null ;
$genero = isset($_GET['genero']) ? $_GET['genero'] : null ;
$tipoedu = isset($_GET['tipoedu']) ? $_GET['tipoedu'] : null ;
$swnuevos = isset($_GET['swnuevos']) ? $_GET['swnuevos'] : 0 ; //solo alumnos nuevos
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 1 ; //saltar pagina en cambio de curso
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$titulo = "LISTADO DE ENCARGADOS DE PAGO POR ALUMNO";
$subtitulo = "A�O ".$ano;
$antano = $ano - 1;
connect();
$sql="SELECT alumnos.id_alumno,alumnos.apellidos,alumnos.nombres,matriculas.ano,matriculas.grado,matriculas.curso,matriculas.id_resppagos FROM alumnos, matriculas ".
"WHERE alumnos.id_alumno = matriculas.id_alumno AND matriculas.ano = '$ano' AND matriculas.id_resppagos!='0' AND matriculas.estado = 'CERRADA' ";
if ($grado != "")
{
	$sql.="AND matriculas.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
if ($curso != "")
{
	$sql.="AND matriculas.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
if ($estado != "")
{
	$sql.="AND alumnos.estado = '$estado' ";
	$subtitulo.=" ESTADO ".$estado;
}
if ($tipoedu != "")
{
	$subtitulo.=" EDUCACION ".$tipoedu;
	$tipoedu = substr($tipoedu,0,1);
	$sql.="AND alumnos.tipoedu = '$tipoedu' ";
}
if ($genero != "")
{
	$subtitulo.=" GENERO ".$genero;
	$genero = substr($genero,0,1);
	$sql.="AND alumnos.genero = '$genero' ";
}
$sql.= "ORDER BY CAST(matriculas.grado AS SIGNED), matriculas.curso ASC,alumnos.apellidos";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$estado = $GLOBALS["estado"] ;
			$matricula = $GLOBALS["matricula"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(4);
			$this->SetFont('Arial','B',6);
			$this->Cell(8,5,'Curso',1,0,'C',1);
			$this->Cell(55,5,'Nombre del Alumno',1,0,'C',1);
			$this->Cell(55,5,'Encargado de Pago',1,0,'C',1);
			$this->Cell(8,5,'Tipo',1,0,'C',1);
			$this->Cell(15,5,'Documento',1,0,'C',1);
			$this->Cell(40,5,'Lugar Exp',1,0,'C',1);
			$this->Cell(0,5,'Pa�s',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IIMPRESO ".$fecha,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//lista de Alumnos
	$num = 0;
	$curso = "";
	foreach($datalum as $regalum)
	{
		if ($swsalto > 0 and $curso!=$regalum->curso) //saltar hoja por curso
		{
			if ($curso!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',6);
				$pdf->Cell(0,5,"TOTAL ALUMNOS: ".$num,1,1,'L',1);
				$pdf->AddPage('P',$papel);			
			}
			$curso = $regalum->curso;
			$num = 0;
		}
		$num = $num +1;
		$id_alumno = $regalum->id_alumno;
		$id_resppagos = $regalum->id_resppagos;
		$pdf->SetFont('Arial','',6);
		$pdf->Cell(8,5,$regalum->curso,'L',0,'C');
		$pdf->Cell(55,5,$regalum->apellidos." ".$regalum->nombres,'LR',0,'L');
		$fam = 0;
		$sql = "SELECT id_familiar, apellidos, nombres, docuexp, docunum, docutip, pais FROM familiares WHERE id_alumno = '$id_alumno'";
		$datmatr = viewsql($sql);
		foreach($datmatr as $regmatr)
		{
			if ($id_resppagos==$regmatr->id_familiar)
			{
				if ($fam > 0)
				{
					$pdf->Cell(50,5,"",'LR',0,'C');
					$pdf->Cell(10,5,"",'LR',0,'C');
				} 
				$pdf->Cell(55,5,$regmatr->apellidos." ".$regmatr->nombres,'LR',0,'L');
				$pdf->Cell(8,5,$regmatr->docutip,'LR',0,'L');
				$pdf->Cell(15,5,$regmatr->docunum,'LR',0,'L');
				$pdf->Cell(40,5,$regmatr->docuexp,'LR',0,'L');
				$pdf->Cell(0,5,$regmatr->pais,'LR',1,'L');
				$fam = $fam + 1;
			}
		}
	}
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(0,5,"TOTAL ALUMNOS: ".$num,1,1,'L',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
