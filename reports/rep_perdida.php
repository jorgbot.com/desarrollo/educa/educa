<?php
/***********************************************************************************************
* @file        : rep_perdida.php (ancho hoja 196)                                              *
* @brief       : Reporte de perdidad de a�o a fecha de corte                                   *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 10-may-2012                                                    *
* @Modificado  : Nora Rodriguez 21-nov-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$corte = isset($_GET['notanum']) ? $_GET['notanum'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($papel == "")
	$papel = "Letter";
if ($corte <= 0)
	$corte = 9;
connect();
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = intval($reganol->notalim);
	$areasper = intval($reganol->areasper);
	$estado = $reganol->estado;
}
//todos los periodos
$titcorte = "";
$sql="SELECT notanum, pertipo, periodo, perplan, pernomb, estado FROM periodos WHERE ano = '$ano' order by notanum";
$datperi = viewsql($sql);
foreach($datperi as $regperi)
{
	if ($regperi->notanum > $corte)
		break;
	if ($regperi->estado=="CERRADO")
		$titcorte = " - ".$regperi->pernomb;
}
$notasfa = "y".$ano."notasfa";
$notas = "y".$ano."notas";
$cursos = "y".$ano."cursos";
$titulo = "PERDIDA A�O LECTIVO ".$ano;

class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("PERDIDA");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->SetFont('Arial','',7);
if ($grado=="")
{$sql="SELECT grados.*, niveles.nombre FROM grados, niveles WHERE grados.nivel = niveles.nivel and grados.perdida = '1' order by grados.id_grado";}
else
{$sql="SELECT grados.*, niveles.nombre FROM grados, niveles WHERE grados.grado = '$grado' and grados.nivel = niveles.nivel order by grados.id_grado";}
$datgrado=viewsql($sql);
foreach($datgrado as $reggrado)
{
    $grado = $reggrado->grado;
    $subtitulo = " GRADO ".$grado." - ".$reggrado->gradonom;
    $subtitulo = $subtitulo.$titcorte;
    $nalu = 0;
    $pdf->AddPage('P',$papel);
    //areas del grado que inciden en perdida de a�o
    $sql="SELECT sum(intensidad) as suma, id_area, area, perdida FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' GROUP BY id_area ORDER BY suma desc, area";
    $datarea = viewsql($sql);
    $numareas = count($datarea);
    //alumnos sin retirar
    $sql="SELECT $cursos.id_alumno, $cursos.curso, $cursos.id_areadeb, $cursos.id_areaper, $cursos.pierde, alumnos.apellidos, alumnos.nombres, alumnos.tipoedu FROM $cursos, alumnos WHERE $cursos.grado = '$grado' AND $cursos.fchretiro LIKE '0000%' ";
    if ($curso != "")
    {
            $sql.="AND $cursos.curso = '$curso' ";
            $subtitulo.=" CURSO ".$curso;
    }
    $sql.="AND $cursos.id_alumno = alumnos.id_alumno ORDER BY alumnos.apellidos, alumnos.nombres";
    $datalum=viewsql($sql);
    foreach($datalum as $regalum)
    {
            $id_alumno = $regalum->id_alumno;
            $alumno = $regalum->apellidos." ".$regalum->nombres;
            $acurso = $regalum->curso;
            $tipoedu = $regalum->tipoedu;
            $pierde = $regalum->pierde;
            $id_areadeb = $regalum->id_areadeb; //area debe a�o anterior
            $areadeb = "";
            if ($id_areadeb > 0) {
                $sql = "select area from asignaturas where id_area = '$id_areadeb' LIMIT 1";
                $datdeb = viewsql($sql);
                foreach($datdeb as $regdeb)
                {$areadeb = $regdeb->area;}
            }
            
            //puntajes seg�n tipo educaci�n
            $sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
            $datpunt = viewsql($sql);
            //areas del grado que inciden en perdida de a�o
            $nare = 0;
            $areas = array();
            foreach($datarea as $regarea)
            {
                    $id_area = $regarea->id_area;
                    $areas[$id_area] = 0;
                    $areas[$id_area."P1"] = 0;
                    $areas[$id_area."P2"] = 0;
                    $areas[$id_area."P3"] = 0;
                    $areas[$id_area."P4"] = 0;
                    $areas[$id_area."P5"] = 0;
                    $areas[$id_area."P6"] = 0;
                    $areas[$id_area."P7"] = 0;
                    $areas[$id_area."P8"] = 0;
                    $areas[$id_area."P9"] = 0;
            }	
            foreach($datarea as $regarea)
            {
                    $id_area = $regarea->id_area;
                    $nota = notas_area($ano,$id_alumno,$id_area,$corte,$datperi);                    
                    if ($nota[9] < $notalim)
                    {
                            $areas[$id_area] = intval($id_area); //cargar area perdida
                            foreach($datperi as $regperi)
                            {
                                $areaperi = $id_area."P".$regperi->notanum;
                                $areas[$areaperi] = $nota[$regperi->notanum];
                            }			
                            $areaperi = $id_area."P9";
                            $areas[$areaperi] = $nota[9];
                            if (trim($regarea->area)===trim($areadeb)) //pierde misma area a�o anterior
                            {
                                $pierde = 1;
                            }
                            if ($regarea->perdida > 0)
                            {
                                $nare = $nare + 1;
                            }
                    }
            } //fin for areas
            //si alumno pierde a�o
            if (($nare > 0 and $nare >= $areasper) or $pierde > 0)
            {
                    $nalu = $nalu+1;
                    $pdf->SetFont('Arial','B',7);
                    $pdf->Cell(68,6,substr($alumno,0,35)." - ".$id_alumno,1,0,'L',1);
                    $pdf->Cell(6,6,$acurso,1,0,'L',1);
                    $pdf->Cell(50,6,"FIRMA:",1,0,'L',1);
                    foreach($datperi as $regperi)
                    {$pdf->Cell(8,6,$regperi->perplan,1,0,'C',1);}
                    $pdf->Cell(12,6,"DEF",1,0,'C',1);
                    $pdf->SetFont('Arial','B',7);
                    $pdf->Cell(0,6,$nare,1,1,'C',1);
                    foreach($datarea as $regarea)
                    {
                            $id_area = $regarea->id_area;
                            if ($areas[$id_area] > 0)
                            {
                                    $pdf->SetFont('Arial','',7);
                                    $pdf->Cell(5,5,'','TBL');
                                    $pdf->Cell(119,5,$regarea->area,'TBR',0,'L');
                                    foreach($datperi as $regperi)
                                    {
                                        $areaperi = $id_area."P".$regperi->notanum;
                                        $pdf->Cell(8,5,$areas[$areaperi],1,0,'C');
                                    }			
                                    $areaperi = $id_area."P9";
                                    $notaQ = cualitativa($datpunt,$areas[$areaperi]);
                                    $pdf->Cell(12,5,$areas[$areaperi]." ".$notaQ,1,0,'C');
                                    $pdf->Cell(0,5,"",1,1);
                            }
                    }
            }
    } //for alumnos
    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(0,6,"TOTAL ALUMNOS QUE PIERDEN A�O: ".$nalu,1,1,'L',1);
    $datarea="";
    $datalumn="";
} //for grados
$pdf->Output();
disconnect();
?>
