<?php
/***********************************************************************************************
* @file        : rep_planillagen.php (ancho hoja 193)                                          *
* @brief       : planilla general de notas asignatura de todos los alumnos en PDF              *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 11-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 11-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$id_asignatura = $_GET["id_asignatura"];
$id_profasig = $_GET["id_profasig"];
$pertipo = $_GET["pertipo"];
$periodo = $_GET["periodo"];
$tipoedu = $_GET["tipoedu"];
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
connect();
$general = "N";
include("../database/datos_planilla.php");
$subtitulo = $pernomb." - ".$estadoper;
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$asignatura = $GLOBALS["asignatura"] ;
			$profesor = $GLOBALS["profesor"] ;
			$implogo = $GLOBALS["implogo"] ;
			$datpunt = $GLOBALS["datpunt"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(1);
				$this->Cell(18);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->SetFont('Arial','B',10);
				$this->Cell(18,6,"");
				$this->Cell(100,6,$titulo,0,1);
				$this->Cell(18,6,"");
				$this->Cell(100,6,$GLOBALS["subtitulo"],0,1);
			}
			else
			{
				$this->Ln(10);
				$this->SetFont('Arial','B',10);
				$this->Cell(100,6,$titulo,0,1);
				$this->Cell(100,6,$GLOBALS["subtitulo"],0,1);
			}
			//tabla de evaluacion		
			$this->SetY(19);
			$this->SetDrawColor(0,128,0);
			$this->SetFillColor(0,128,0);
			$this->SetTextColor(255,255,255);
			$this->SetFont('Arial','B',7);
			$this->Cell(146);
			$this->Cell(50,5,"Evaluaci�n",1,1,'C',1);
			$this->SetFillColor(224,231,233);
			$this->SetTextColor(0,0,0);
			foreach($datpunt as $regpunt)
			{
				$this->Cell(146);
				$this->Cell(8,5,$regpunt->notaQ,1,0);
				$this->Cell(30,5,$regpunt->notadet,1,0);
				$this->Cell(0,5,$regpunt->notamin." - ".$regpunt->notamax,1,1);
			}
			$this->SetDrawColor(128,128,128);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',9);
			$this->SetY(32);
			$this->Cell(25,6,"ASIGNATURA",1,0);
			$this->Cell(118,6,$asignatura,1,1);
			$this->Cell(25,6,"DOCENTE",1,0);
			$this->Cell(118,6,$profesor,1,1);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','',7);
			$this->Ln(4);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("PLANILLA");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$titulo = "PLANILLA GENERAL ".$ano." - CURSO ".$curso;
	$asignatura = $asignatura." - IH ".$intensidad;
	$pdf->AddPage('P',$papel);
	//titulos columnas
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(5,5,'N�',1,0,'C',1);
	$pdf->Cell(44,5,'APELLIDOS Y NOMBRES',1,0,'C',1);
	foreach($datperi as $regperi)
	{$pdf->Cell(12,5,$regperi->perplan,1,0,'C',1);}			
	$pdf->Cell(12,5,"DEF",1,0,'C',1);
	$pdf->Cell(0,5,"Observaciones",1,1,'C',1);
	//lista de Alumnos
	$nota = array();
	$pend = array();
	$num = 0;
	foreach($datalum as $regalum)
	{
		$num = $num +1;
		$retiro = $regalum->fchretiro;
		$obsv = " ";
			if ($retiro != ('0000-00-00')){
				$obsv = "RETIRADO EL ".$retiro;
			}
			else {
				$obsv = " ";
			}
		$id_alumno = $regalum->id_alumno;
		$pdf->SetFont('Arial','',6);
		$pdf->Cell(5,5,$num,1,'R');
		$pdf->Cell(44,5,substr($regalum->apellidos." ".$regalum->nombres,0,32),1,0,'L');
		//traer notas total periodos
		$nota = notas_asignatura($ano,$id_alumno,$id_asignatura,$corte,$datperi);
		$numnot = 0;
		$sumnot = 0; 
		foreach($datperi as $regperi)
		{
			$notaper = $nota[$regperi->notanum];
			if ($estadoper=="ABIERTO" and $regperi->pertipo=="P" and $regperi->notanum <= $corte)
			{
				if ($regperi->notanum==$corte)
				{
					$notaper = 0;
					$sql="select nota, pendiente, inactivar from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = 'P' and periodo = '$periodo' LIMIT 1";
					$datnot=viewsql($sql);
					foreach($datnot as $regnot)
					{
						$notaper = $regnot->nota;
					}
					if ($notaper!="INAC" and $notaper!="PEND")
					{
						$numnot = $numnot + 1;
						$sumnot = $sumnot + $notaper;
					}
				}
				else
				{
					$numnot = $numnot + 1;
					$sumnot = $sumnot + $nota[$regperi->notanum];
				}
			}
			$notaQ = cualitativa($datpunt,$notaper);			
			$pdf->Cell(12,5,$notaper." ".$notaQ,1,0,'C');
		}
		//nota final
		if ($estadoper=="ABIERTO" and $numnot > 0) //recalcular definitiva a�adiendo periodo actual
		{
			$sumnot = round($sumnot/$numnot,1);
			$nota[9] = $sumnot;
		}
		$notaQ = cualitativa($datpunt,$nota[9]);
		$pdf->SetFont('Arial','B',6);
		if ($nota[9] <= $notalim)
			$pdf->SetTextColor(164,0,0);
		$pdf->Cell(12,5,$nota[9]." ".$notaQ,1,0,'C',1);
		$pdf->SetTextColor(0,0,0);
		$pdf->Cell(0,5,$obsv,1,1,'C');
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
