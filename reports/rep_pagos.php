<?php
/***********************************************************************************************
* @file        : rep_pagos.php (ancho hoja 196)                                                *
* @brief       : reporte conciliacion de pagos totales por rango de fechas                     *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-sep-2015                                                    *
* @Modificado  : Nora Rodriguez 23-sep-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchtip = isset($_GET['fchtip']) ? $_GET['fchtip'] : "ENTRADAS" ;
$fchini = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$fchfin = isset($_GET['fchfin']) ? $_GET['fchfin'] : date('d-m-Y') ;
$id_entrada = isset($_GET['id_entrada']) ? $_GET['id_entrada'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$exportar = isset($_GET['exportar']) ? $_GET['exportar'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$fchini = fec_amd($fchini);
$fchfin = fec_amd($fchfin);
connect();
$titulo = "A�O ".$ano."  PAGOS PENSIONES";
if ($tipo=="T")
	$titulo = "A�O ".$ano."  PAGOS TRANSPORTES";
if ($tipo=="M")
	$titulo = "A�O ".$ano."  PAGOS MATRICULAS";
if ($id_entrada > 0)
	$subtitulo = "HOJA DE ENTRADA N� ".$id_entrada;
else
	$subtitulo = $fchtip." DEL ".fecha_texto($fchini)." AL ".fecha_texto($fchfin);
//alumnos
$filepag = "y".$ano."pagos";
$filedet = "y".$ano."pagosdet";
$sql="SELECT $filepag.*, alumnos.apellidos, alumnos.nombres FROM $filepag, alumnos ";
if ($id_entrada > 0)
	$sql.="WHERE id_entrada = '$id_entrada' ";
else
{
	if ($fchtip=="ENTRADAS")
		$sql.="WHERE date_format($filepag.fchentrada,'%Y-%m-%d') BETWEEN '$fchini' AND '$fchfin' ";
	else
		$sql.="WHERE date_format($filepag.fchpago,'%Y-%m-%d') BETWEEN '$fchini' AND '$fchfin' ";
}
if ($tipo!="")
{
	if ($tipo=="T")
		$sql.="AND tipo = 'T' ";
	else
		$sql.="AND tipo = 'P' ";
}
if ($grado != "")
	$sql.="AND grado = '$grado' ";
if ($curso != "")
	$sql.="AND curso = '$curso' ";
$sql.="AND $filepag.id_alumno = alumnos.id_alumno ORDER BY $filepag.id_entrada, alumnos.apellidos, alumnos.nombres, $filepag.id_pago";
$datpag=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$tipo = $GLOBALS["tipo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(8,5,"IDEnt",1,0,'C',1);
			$this->Cell(18,5,"FchEntrada",1,0,'C',1);
			$this->Cell(50,5,"Alumno",1,0,'C',1);
			$this->Cell(12,5,"Curso",1,0,'C',1);
			$this->Cell(10,5,"IDPago",1,0,'C',1);
			$this->Cell(18,5,"FchConsig",1,0,'C',1);
			if ($tipo=="T")
				$this->Cell(16,5,"Transporte",1,0,'C',1);
			else
				$this->Cell(16,5,"Pensi�n",1,0,'C',1);
			$this->Cell(15,5,"Interes",1,0,'C',1);
			$this->Cell(16,5,"Consignado",1,0,'C',1);
			$this->Cell(15,5,"Efectivo",1,0,'C',1);
			$this->Cell(16,5,"TOTAL",1,1,'C',1);
			$this->SetFont('Arial','',7);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("SALDOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$num = 0;
	$totcon = 0;
	$totefe = 0;
	$totnet = 0;
	$totint = 0;
	$totval = 0;
	$netocon = 0;
	$netoefe = 0;
	$intecon = 0;
	$inteefe = 0;
	foreach($datpag as $regpag)
	{
		$num = $num + 1;
		$pdf->Cell(8,5,$regpag->id_entrada,'LR',0,'L');
		$pdf->Cell(18,5,fecha_texto($regpag->fchentrada),'LR',0,'L');
		$pdf->Cell(50,3,substr($regpag->apellidos." ".$regpag->nombres,0,30),'LR',0,'L');
		$pdf->Cell(4,5,$regpag->grado,'LR',0,'L');
		$pdf->Cell(8,5,$regpag->curso,'LR',0,'L');
		$pdf->Cell(10,5,$regpag->id_pago,'LR',0,'L');
		$pdf->Cell(18,5,fecha_texto($regpag->fchpago),'LR',0,'L');
		$pdf->Cell(16,5,number_format($regpag->neto,0),'LR',0,'R');
		$pdf->Cell(15,5,number_format($regpag->interes,0),'LR',0,'R');
		$pdf->Cell(16,5,number_format($regpag->valor_con,0),'LR',0,'R');
		$pdf->Cell(15,5,number_format($regpag->valor_efe,0),'LR',0,'R');
		$pdf->Cell(16,5,number_format($regpag->valor,0),'LR',1,'R');
		$totcon = $totcon + intval($regpag->valor_con);
		$totefe = $totefe + intval($regpag->valor_efe);
		$totnet = $totnet + intval($regpag->neto);
		$totint = $totint + intval($regpag->interes);
		$totval = $totval + intval($regpag->valor);
		if ($regpag->id_banco <= 0)
		{
			$netoefe = $netoefe + $regpag->neto;
			$inteefe = $inteefe + $regpag->interes;
		}
		else
		{
			$netocon = $netocon + $regpag->neto;
			$intecon = $intecon + $regpag->interes;
		}
	}
	$pdf->SetFont('Arial','B',6);
	if ($tipo=="T")
	{
		$pdf->Cell(116,5,"PAGOS: ".$num,1,0,'L',1);
		$pdf->Cell(16,5,number_format($totnet,0),1,0,'R',1);
		$pdf->Cell(15,5,number_format($totint,0),1,0,'R',1);
		$pdf->Cell(16,5,number_format($totcon,0),1,0,'R',1);
		$pdf->Cell(15,5,number_format($totefe,0),1,0,'R',1);
		$pdf->Cell(16,5,number_format($totval,0),1,1,'R',1);
	}
	else
	{
		$pdf->Cell(26,4,"PAGOS: ".$num,'LT',0,'L',1);
		$pdf->Cell(90,4,"TOTALES:",'TR',0,'R',1);
		$pdf->Cell(16,4,number_format($totnet,0),'LTR',0,'R',1);
		$pdf->Cell(15,4,number_format($totint,0),'LTR',0,'R',1);
		$pdf->Cell(16,4,number_format($totcon,0),'LTR',0,'R',1);
		$pdf->Cell(15,4,number_format($totefe,0),'LTR',0,'R',1);
		$pdf->Cell(16,4,number_format($totval,0),'LTR',1,'R',1);
		$pdf->Cell(116,4,"CONSIGNADO:",'LR',0,'R',1);
		$pdf->Cell(16,4,number_format($netocon,0),'LR',0,'R',1);
		$pdf->Cell(15,4,number_format($intecon,0),'LR',0,'R',1);
		$pdf->Cell(16,4,"",'LR',0,'R',1);
		$pdf->Cell(15,4,"",'LR',0,'R',1);
		$pdf->Cell(16,4,"",'LR',1,'R',1);
		$pdf->Cell(116,4,"EFECTIVO:",'LRB',0,'R',1);
		$pdf->Cell(16,4,number_format($netoefe,0),'LRB',0,'R',1);
		$pdf->Cell(15,4,number_format($inteefe,0),'LRB',0,'R',1);
		$pdf->Cell(16,4,"",'LRB',0,'R',1);
		$pdf->Cell(15,4,"",'LRB',0,'R',1);
		$pdf->Cell(16,4,"",'LRB',1,'R',1);
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
