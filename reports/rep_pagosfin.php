<?php
/***********************************************************************************************
* @file        : rep_pagosdian.php (ancho hoja 196)                                            *
* @brief       : reporte pagos y saldo pendiente a fecha de corte                              *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-sep-2015                                                    *
* @Modificado  : Nora Rodriguez 23-sep-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$exportar = isset($_GET['exportar']) ? $_GET['exportar'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$swdeben = isset($_GET['swdeben']) ? $_GET['swdeben'] : 0 ; //1 = solo alumnos que deben
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 0 ; //1 = saltar pagina en cambio de curso
$fchcorte = fec_amd($fchcorte);
$mescorte = substr($fchcorte,5,2);
connect();
$porcen = 0;
$sql="SELECT ano, interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
$titulo = "A�O ".$ano." PAGOS MATRICULAS, PENSIONES, TRANSPORTE Y SALDO FINAL ";
if ($tipo=="P")
	$titulo = "A�O ".$ano." PAGOS PENSIONES Y SALDO FINAL ";
if ($tipo=="T")
	$titulo = "A�O ".$ano." PAGOS TRANSPORTES Y SALDO FINAL ";
if ($tipo=="M")
	$titulo = "A�O ".$ano." PAGOS MATRICULAS Y SALDO FINAL ";
if ($tipo=="MP")
	$titulo = "A�O ".$ano." PAGOS MATRICULAS, PENSIONES Y SALDO FINAL ";
$subtitulo = "CORTE A ".fecha_texto($fchcorte);
//alumnos
$filepag = "y".$ano."pagos";
$filedet = "y".$ano."pagosdet";
$cursos = "y".$ano."cursos";
$sql="SELECT grado, curso, apellidos, nombres, id_alumno FROM $cursos ";
if ($grado != "")
	$sql.="WHERE grado = '$grado' ";
if ($curso != "")
	$sql.="AND curso = '$curso' ";
$sql.="ORDER BY CAST(grado AS SIGNED), curso, apellidos, nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$tipo = $GLOBALS["tipo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(8,5,'Curso',1,0,'C',1);
			if ($tipo=="MP")
				$this->Cell(31,5,'Apellidos y Nombres',1,0,'L',1);			
			else
				$this->Cell(44,5,'Apellidos y Nombres',1,0,'L',1);
			if ($tipo=="M" or $tipo=="MP")
				$this->Cell(13,5,'Matricula',1,0,'L',1);
			if ($tipo!="M")
			{			
				for($i=2; $i<=11 ;$i++)
				{
					if ($i <= 9)
						$mes = "0".$i;
					else
						$mes = $i;
					$this->Cell(13,5,substr(strtoupper(nombre_mes($mes)),0,3),1,0,'C',1);
				}
			}
			$this->Cell(14,5,"SALDO",1,1,'C',1);
			$this->SetFont('Arial','',7);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("SALDOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$totcur = array();
	$totfin = array();
	for($i=1; $i<=12 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$totcur[$mes] = 0;
		$totfin[$mes] = 0;
	}			
	//Lista de Alumnos
	$numalu = 0;
	$curso = "";
	foreach($datalum as $regalum)
	{
		if ($swsalto > 0 and $curso!=$regalum->curso) //saltar hoja por curso
		{
			if ($curso!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',6);
				if ($tipo=="MP")
					$pdf->Cell(39,5,"ALUMNOS ".$numalu,1,0,'L',1);
				else
					$pdf->Cell(52,5,"ALUMNOS ".$numalu,1,0,'L',1);
				if ($tipo=="M" or $tipo=="MP")
					$pdf->Cell(13,5,number_format($totcur["01"],0),0,0,'R');
				if ($tipo!="M")
				{
					for($i=2; $i<=11 ;$i++)
					{
						if ($i <= 9)
							$mes = "0".$i;
						else
							$mes = $i;
						$pdf->Cell(13,5,number_format($totcur[$mes],0),1,0,'R',1);					
						$totcur[$mes] = 0;
					}
				}
				$pdf->Cell(14,5,number_format($totcur["12"],0),1,1,'R',1);
				$pdf->AddPage('P',$papel);			
			}
			$curso = $regalum->curso;
			$totcur["01"] = 0;
			$totcur["12"] = 0;
			$numalu = 0;
		}
		//cargar valores alumno
		$id_alumno = $regalum->id_alumno;
		if ($tipo=="T")
			$valores = valores_transporte($id_alumno,$ano);
		else
			$valores = valores_pension($id_alumno,$ano);
		$pagos = array();
		$saldos = array();
		for($i=1; $i<=12 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			$pagos[$mes] = 0;
			$saldos[$mes] = 0;
		}
		if ($tipo=="T")
			$sql="SELECT id_pago, mes, valor, interes, interesliq, tipo FROM $filedet WHERE id_alumno = '$id_alumno' AND ano = '$ano' AND tipo = 'T'";
		else
			$sql="SELECT id_pago, mes, valor, interes, interesliq, tipo FROM $filedet WHERE id_alumno = '$id_alumno' AND ano = '$ano' AND tipo != 'T'";
		$datpag = viewsql($sql);
		foreach($datpag as $regpag)
		{
			if (intval($regpag->mes) <= 9)
				$mes = "0".$regpag->mes;
			else
				$mes = $regpag->mes;
			$pagos[$mes] = $pagos[$mes] + intval($regpag->valor);
			if ($regpag->tipo=="M")
				$pagos["01"] = $pagos["01"] + intval($regpag->valor);
		}
		//calcular saldos
		$saldotot = 0;
		if ($tipo=="M" or $tipo=="MP")
		{
			$saldo = $valores["matricula"] - $pagos["01"];
			$saldos["01"] = $saldo;
			$saldotot = $saldo;
		}
		for($i=2; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			$saldo = $valores[$mes] - $pagos[$mes];
			$interes = 0;
			if ($i > $mescorte)
				$saldo = 0;
			$saldos[$mes] = $saldo;
			$saldotot = $saldotot + $saldo;			
		}
		if ($swdeben == 0 or ($swdeben==1 and $saldotot > 0))
		{
			$numalu = $numalu + 1;
			$pdf->SetFont('Arial','',7);
			if ($tipo=="MP")
			{
				$pdf->Cell(8,3,$regalum->curso,0,0,'L');
				$pdf->Cell(44,3,substr($regalum->apellidos." ".$regalum->nombres,0,28),0,1,'L');
				$pdf->Cell(39,5,"",0,0);
			}
			else
			{
				$pdf->Cell(8,5,$regalum->curso,0,0,'L');
				$pdf->Cell(44,5,substr($regalum->apellidos." ".$regalum->nombres,0,28),0,0,'L');
			}
			if ($tipo=="M" or $tipo=="MP")
				$pdf->Cell(13,5,number_format($pagos["01"],0),0,0,'R');
			if ($tipo!="M")
			{
				for($i=2; $i<=11 ;$i++)
				{
					if ($i <= 9)
						$mes = "0".$i;
					else
						$mes = $i;
					$pdf->Cell(13,5,number_format($pagos[$mes],0),0,0,'R');
					$totcur[$mes] = $totcur[$mes] + $saldos[$mes];
					$totfin[$mes] = $totfin[$mes] + $saldos[$mes];
					$totcur["12"] = $totcur["12"] + $saldos[$mes];
					$totfin["12"] = $totfin["12"] + $saldos[$mes];
				}
			}
			$totcur["01"] = $totcur["01"] + $saldos["01"];
			$totfin["01"] = $totfin["01"] + $saldos["01"];
			$pdf->Cell(14,5,number_format($saldotot,0),0,1,'R');
		}
	}
	$pdf->SetFont('Arial','B',6);
	if ($tipo=="M" or $tipo=="MP")
	{
		$pdf->Cell(47,5,"ALUMNOS ".$numalu,1,0,'L',1);
		if ($swsalto > 0)
			$pdf->Cell(13,5,number_format($totcur["01"],0),0,0,'R');
		else
			$pdf->Cell(13,5,number_format($totfin["01"],0),0,0,'R');
	}
	else
		$pdf->Cell(52,5,"ALUMNOS ".$numalu,1,0,'L',1);
	if ($tipo!="M")
	{
		for($i=2; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			if ($swsalto > 0)
				$pdf->Cell(13,5,number_format($totcur[$mes],0),1,0,'R',1);
			else
				$pdf->Cell(13,5,number_format($totfin[$mes],0),1,0,'R',1);
		}
	}
	if ($swsalto > 0)
		$pdf->Cell(14,5,number_format($totcur["12"],0),1,1,'R',1);
	else
		$pdf->Cell(14,5,number_format($totfin["12"],0),1,1,'R',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
