<?php
/***********************************************************************************************
* @file        : rep_fallaspapper.php (ancho hoja 196)                                         *
* @brief       : papeletas de permisos y citaciones                                            *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-ago-2018                                                    *
* @Modificado  : Nora Rodriguez 13-ago-2018                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : $_SESSION["anoaca"];
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_falla = isset($_GET['id_falla']) ? $_GET['id_falla'] : 0 ;
$fchcorte = isset($_GET['fchcorte']) ? $_GET['fchcorte'] : date('d-m-Y') ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
$fchcorte = fec_amd($fchcorte);
connect();
$fallas = "y".$ano."fallas";
$cursos = "y".$ano."cursos";
if ($id_falla > 0) {
    $sql="SELECT $fallas.id_alumno, $fallas.tipo, $fallas.concepto, $fallas.fecha, $fallas.fchfin, $fallas.fchjust, $fallas.hcita, $fallas.id_categoria, $fallas.detalles, $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres FROM $fallas, $cursos WHERE $fallas.id_falla = '$id_falla' and $fallas.id_alumno = $cursos.id_alumno LIMIT 1";
} else {
    $sql="SELECT $fallas.id_alumno, $fallas.tipo, $fallas.concepto, $fallas.fecha, $fallas.fchfin, $fallas.fchjust, $fallas.hcita, $fallas.id_categoria, $fallas.detalles, $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres FROM $fallas, $cursos WHERE $fallas.fecha = '$fchcorte' AND ($fallas.tipo='P' OR $fallas.tipo='C') ";
    if ($grado!="") {
        $sql.="AND $cursos.grado = '$grado' ";
    }
    if ($curso!="") {
        $sql.="AND $cursos.curso = '$curso' ";
    }
    $sql.="AND $fallas.id_alumno = $cursos.id_alumno ORDER BY $cursos.grado, $cursos.curso, $cursos.apellidos";
}
//echo "$sql<br>";
$datalu=viewsql($sql);
class PDF extends FPDF
{
		//Page header
		function Header()
		{
		}
		function Footer()
		{
		}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("PAPELETAS");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
//lista de Alumnos
$numalu = 0;
$gradonom = "";
$pdf->AddPage('P',$papel);
foreach($datalu as $regalu)
{
    $id_alumno = $regalu->id_alumno;
    $tipo = $regalu->tipo;
    $fecha = $regalu->fecha;
    $fchfin = $regalu->fchfin;
    $fchjust = $regalu->fchjust;
    $hcita = $regalu->hcita;
    $id_categoria = $regalu->id_categoria;
    if ($regalu->grado!=$gradonom)
    {
	$ngrado = $regalu->grado;
	$sql = "SELECT gradonom FROM grados WHERE grado = '$ngrado' LIMIT 1";
	$datgra = viewsql($sql);
	foreach($datgra as $reggra)
	{$gradonom = $reggra->gradonom;}
    }
    $categoria = "";
    $sql = "SELECT categoria FROM categorias WHERE id_categoria = '$id_categoria' LIMIT 1";
    $datcat = viewsql($sql);
    foreach($datcat as $regcat)
    {$categoria = $regcat->categoria;}
    //contar alumnos por hoja
    $numalu = $numalu + 1;
    if ($numalu > 1)
        $pdf->Cell(0,9,"",0,1);
    $pdf->SetFont('Arial','B',12);
    $pdf->SetTextColor(0,80,0);
    $pdf->Cell(0,6,$_SESSION["instinom"],0,1,'C');
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(0,5,"",'',1); 
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('Arial','B',9);
    $pdf->Cell(0,10,"ESTUDIANTE: ".$regalu->nombres." ".$regalu->apellidos.", ID ".$id_alumno." - ".$gradonom." CURSO ".$regalu->curso,'TLR',1,'C');
    $pdf->SetTextColor(600,0,0);
    $pdf->SetFont('Arial','B',10);
    if ($tipo=="P") {
        $pdf->Cell(190,10,"PERMISO ESTUDIANTIL",'LR',1,'C');
    }
   if ($tipo=="A") {
        $pdf->Cell(190,10,"AUTORIZACION ESTUDIANTIL",'LR',1,'C');
    }
   if ($tipo=="C") {
        $pdf->Cell(190,10,"SOLICITUD DE ENTREVISTA",'LR',1,'C');
    }
    $pdf->SetTextColor(0,0,0);
    $pdf->SetFont('Arial','',8);
    $vigencia = "EL DIA ".fecha_completa($fecha);
    if ($fecha!=$fchfin and substr($fchfin,0,2)!="00") {
        $vigencia = "DESDE EL ".fecha_texto($fecha)." HASTA EL ".fecha_texto($fchfin);  
    }
    if ($tipo=="A") {
        $pdf->Cell(0,5,"NOS PERMITIMOS COMUNICAR QUE EL (LA) PRESENTE ESTUDIANTE TIENE AUTORIZACION ESPECIAL POR COORDINACI�N",'LR',1,'L');
        $pdf->Cell(0,5,"DE CONVIVENCIA ESCOLAR PARA ".$regalu->concepto." ".$vigencia,'LR',1,'L');
    }
    if ($tipo=="P") {
        $pdf->Cell(0,5,"NOS PERMITIMOS COMUNICAR QUE EL (LA) PRESENTE ESTUDIANTE TIENE PERMISO AUTORIZADO POR COORDINACI�N DE",'LR',1,'L');
        $pdf->Cell(0,5,"CONVIVENCIA ESCOLAR PARA ".$regalu->concepto." POR ".$categoria." ".$vigencia,'LR',1,'L');
    }
    if ($tipo=="C") {
        $pdf->Cell(0,5,"FECHA DE ENTREVISTA: ".fecha_completa($fecha)." - HORA ".$hcita,'LR',1,'L');
        $pdf->Cell(0,5,"FECHA DE SOLICITUD: ".fecha_texto($fchjust),'LR',1,'L');
        $pdf->Cell(0,5,"Se�or Padre de Familia o Acudiente: Con el fin de tratar temas de VITAL IMPORTANCIA, consideramos necesaria una entrevista con usted para",'LR',1,'L');
        $pdf->Cell(0,5,"dialogar respecto a: ".$regalu->concepto,'LR',1,'L');
    }
    $pdf->Cell(0,1,"",'LR',1); 
    $pdf->MultiCell(0,5,"OBSERVACIONES: ".$regalu->detalles,'LR','J');
    $pdf->Cell(0,8,"",'LR',1); 
    $pdf->Cell(70,6,"FIRMA DEL PADRE O ACUDIENTE",'BTL',0,'C'); 
    $pdf->Cell(50,6,"","BT",0);
    $pdf->Cell(70,6,"COORDINACION",'BTR',1,'C'); 
    if ($numalu == 3) //Hasta 3 papeletas por hoja
    {
	$pdf->AddPage('P',$papel);
	$numalu = 0;
    }
}
$pdf->Output();
disconnect();
?>
