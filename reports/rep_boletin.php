<?php
/***********************************************************************************************
* @file        : rep_boletin.php (hoja carta, vertical, ancho hoja 196)                        *
* @brief       : Genera reporte de planilla en PDF                                             *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 15-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 15-dic-2013                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$grado = $_GET["grado"];
$periodo = $_GET["periodo"];
$tipoedu = $_GET["tipoedu"];
$papel = $_GET["papel"];
$implogo = $_GET["implogo"];
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$impdoble = isset($_GET['impdoble']) ? $_GET['impdoble'] : null ;
$exportar = "";
$anoant = $ano - 1;
$notanum = $periodo;
if ($notanum >= 3)
	$notanum = $notanum + 1;
global $hoja;
$hoja = 0;
global $blanco;
$blanco = 0;
connect();
//institucion
$sql="select * from institucion limit 1";
$datinst=viewsql($sql);
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = $reganol->notalim;
	$paraevi = $reganol->notaevi;
	$printevi = $reganol->printevi;
	$areasper = $reganol->areasper;
}
//puntajes
$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
$datpunt = viewsql($sql);
//periodos del a�o
$estado = "";
$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
$datperi = viewsql($sql);
foreach($datperi as $regperi)
{
	if ($regperi->pertipo == "P" and $regperi->periodo == $periodo)
	{
		$pernomb = $regperi->pernomb;
		$fchinicio = $regperi->fchinicio;
		$fchfinal = $regperi->fchfinal;
		$fchplazo = $regperi->fchplazo;
		$estado = $regperi->estado;
	}
}
//nombre grado
$sql="SELECT gradonom FROM grados WHERE grado = '$grado' limit 1";
$datgrad = viewsql($sql);
foreach($datgrad as $reggrad)
{$gradonom = $reggrad->gradonom;}
//areas del grado
//$sql="SELECT sum(intensidad) as suma, id_area, area, areacod, promedio, perdida FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' group by id_area ORDER BY perdida desc, suma desc, codigo";
$sql="SELECT sum(asignaturas.intensidad) as suma, asignaturas.id_area, asignaturas.promedio, asignaturas.perdida, areas.area, areas.areacod FROM asignaturas, areas WHERE asignaturas.ano = '$ano' AND asignaturas.grado = '$grado' and asignaturas.id_area = areas.id_area group by asignaturas.id_area ORDER BY asignaturas.perdida desc, suma desc, asignaturas.codigo";
$datarea = viewsql($sql);
//asignaturas del grado
$sql="SELECT id_area, id_asignatura, asignatura, porcentaje, intensidad, notas, notaQ FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' and (notas = '1' or boletin = '1') ORDER BY id_area, intensidad desc, codigo";
$datasig = viewsql($sql);
//logros del grado y periodo
$logros = "y".$ano."logros";
$sql = "SELECT id_logro, logrocod, logro, logrosi, logrono, id_asignatura FROM $logros WHERE grado = '$grado' AND periodos LIKE '% $periodo %' AND tipoedu = '$tipoedu' ORDER BY id_asignatura, logrocod";
$datlogr = viewsql($sql);
//evidencias
$fileevi = "y".$ano."eviden";
$sql = "SELECT $logros.id_asignatura, $fileevi.id_logro, $fileevi.id_evidencia, $fileevi.evidencia, $fileevi.numero FROM $logros, $fileevi WHERE $logros.id_logro = $fileevi.id_logro AND $logros.grado = '$grado' AND $fileevi.periodo = '$periodo' ORDER BY $fileevi.id_logro, $fileevi.numero";
$datevid = viewsql($sql);
//alumnos
$cursos = "y".$ano."cursos";
$sql="SELECT * FROM $cursos ";
if ($id_alumno > 0)
	$sql.="WHERE id_alumno = '$id_alumno' LIMIT 1";
else
{
	$sql.="WHERE grado = '$grado' ";
	if ($curso != "")
		$sql.="AND curso = '$curso' ";
	$sql.="AND tipoedu = 'F' AND fchretiro LIKE '0000%' ORDER BY grado, curso, apellidos, nombres";
}
$datalum=viewsql($sql);
//tablas notas anuales
$notas = "y".$ano."notas";
$notasfa = "y".$ano."notasfa";
$notasdet = "y".$ano."notasp".$periodo;
$fallas = "y".$ano."fallas";
$observ = "y".$ano."observa";
$planillas = "y".$ano."planillas";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		global $blanco;
		if ($blanco == 0)
		{
			$titulo = $GLOBALS["titulo"];
			$subtitulo = $GLOBALS["subtitulo"];
			$pernomb = $GLOBALS["pernomb"];
			$id_alumno = $GLOBALS["id_alumno"];
			$alumno = $GLOBALS["alumno"];
			$fchretiro = $GLOBALS["fchretiro"];
			$gradonom = $GLOBALS["gradonom"];
			$curso = $GLOBALS["curso"];
			$estcurso = $GLOBALS["estcurso"];
			$implogo = $GLOBALS["implogo"];
			$datpunt = $GLOBALS["datpunt"];
			global $hoja;
			$hoja = $hoja + 1;
			$this->SetDrawColor(128,128,128);
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,0,0);
			if ($implogo=="S")
			{
				if ($hoja==1)
				{
					$datinst = $GLOBALS["datinst"];
					foreach($datinst as $reginst)
					{
						$insnom = $reginst->instinom;
						$inssig = $reginst->instisig;
						$insciu = $reginst->ciudad;
						$insdir = $reginst->direccion;
						$instel = $reginst->telefono;
						$insre1 = $reginst->resolkin;
						$insre2 = $reginst->resolpri;
						$insre3 = $reginst->resolsec;
						$inssec = $reginst->resolseed;
						$insdan = $reginst->resoldane;
						$insnot = $reginst->notaria;
					}
					$this->Image('../images/logo.jpg',12,10,20,0,'JPG');
					$this->Ln(5);
					$this->Cell(0,6,$insnom,0,1,'C');
					$this->Cell(0,6,$inssig,0,1,'C');
					$this->SetFont('Arial','',6);
					$this->SetY(30);
					$this->Cell(21,3,"");
					$this->MultiCell(55,3,$insre1,0,'C');
					$this->SetY(30);
					$this->Cell(80,3,"");
					$this->MultiCell(55,3,$insre2,0,'C');
					$this->SetY(30);
					$this->Cell(141,3,"");
					$this->MultiCell(55,3,$insre3,0,'C');
				}
				else
					$this->Ln(1);
			}
			else
			{
				$this->Ln(26);
			}
			$this->Ln(3);
			$this->SetTextColor(0,80,0);
			$this->SetFont('Arial','B',12);
			if ($estcurso != "CERRADO")
				$this->Cell(0,6,"INFORME ACAD�MICO (NOTAS PARCIALES) ".$titulo,0,1);
			else
				$this->Cell(0,6,"INFORME ACAD�MICO ".$titulo,0,1);
			$this->Ln(3);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(20,6,"GRADO",1,0);
			$this->Cell(90,6,$gradonom." - CURSO ".$curso,1,0);
			$this->SetFont('Arial','B',8);
			$this->Cell(25,6,"HOJA: ".$hoja,1,1);
			$this->Cell(20,6,"ALUMNO ",1,0);
			$this->Cell(90,6,$alumno,1,0);
			$this->Cell(25,6,"CODIGO: ".$id_alumno,1,1);
			$this->Cell(20,6,$pernomb,1,0);
			$this->Cell(45,6,$subtitulo,1,0);
			$detalle = "PERIODO ".$estcurso;
			if ($estcurso != "CERRADO")
			{
				$this->SetTextColor(164,0,0);
				$detalle = "(PENDIENTE CIERRE DEL PERIODO)";
			}
			if ($fchretiro != "")
				$detalle = "(ALUMNO RETIRADO - ".$fchretiro." )";
			$this->Cell(70,6,$detalle,1,1);
			$this->SetTextColor(0,0,0);
			$pos=$this->GetY();
			$pos=$pos-25;
			$this->SetY($pos);
			$this->SetDrawColor(0,128,0);
			$this->SetFillColor(0,128,0);
			$this->SetTextColor(255,255,255);
			$this->SetFont('Arial','B',7);
			$this->Cell(146);
			$this->Cell(50,5,"Evaluaci�n",1,1,'C',1);
			$this->SetDrawColor(0,80,0);
			$this->SetFillColor(224,231,233);
			$this->SetTextColor(0,0,0);
			foreach($datpunt as $regpunt)
			{
				$this->Cell(146);
				$this->Cell(8,5,$regpunt->notaQ,1,0);
				$this->Cell(30,5,$regpunt->notadet,1,0);
				$this->Cell(0,5,$regpunt->notamin." - ".$regpunt->notamax,1,1);
			}
			$this->SetDrawColor(128,128,128);
			$this->SetTextColor(0,80,0);
			$this->SetFont('Times','B',7);
			$this->Cell(180,10,'PERIODOS(I,II,III,IV)     EXAMENES SEMESTRALES(ExI, ExII)     EXAMEN FINAL(ExF)     DEFINITIVA(DEF)',0,1);
			$this->SetFont('Arial','',7);
			$this->SetTextColor(0,0,0);
		}
	}
	function Footer()
	{
		$this->Cell(0,0,'',1,1);
		$fecha=fecha_texto(date('Y-m-d'));
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->SetTextColor(0,0,0);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO ".$fecha,0,0,'C');
	}
}
//iniciar documento PDF
$titulo = "A�O ".$ano." - ".$pernomb;
$subtitulo = fecha_texto($fchinicio)." A ".fecha_texto($fchfinal);
$pdf=new PDF();
$pdf->SetTitle("BOLETIN");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$numalu = 0;
$curso = "";
foreach($datalum as $regalum)
{
	$numalu = $numalu + 1;
	$id_alumno = $regalum->id_alumno;
	$alumno = substr($regalum->apellidos." ".$regalum->nombres,0,40);
	$areadeb = 0;
	if ($regalum->id_areadeb > 0)
	{
		$sql = "select areacod from areas where id_area = '".$regalum->id_areadeb."' limit 1";
		$datreg=viewsql($sql);
		foreach($datreg as $reg)
		{$areadeb = intval($reg->areacod);}
	}
	$inactperi = "";
	if ($regalum->inactivar!="") //periodos inactivos
		$inactperi = " ".$regalum->inactivar." ";
	if ($regalum->curso != $curso)
	{
		$curso = $regalum->curso;
		$estcurso = $estado;
		if ($estado != "CERRADO") //si periodo no cerrado, verificar estado del curso
		{
			$sql = "select estado from $planillas where id_asignatura = '0' and grado='$grado' and curso='$curso' and pertipo = 'P' and periodo = '$periodo' limit 1";
			$datreg=viewsql($sql);
			foreach($datreg as $reg)
			{$estcurso = $reg->estado;}
		}
	}
	$puesto = $regalum->puesto1;
	$promedio = $regalum->promedio1;
	if ($periodo == 2)
	{
		$puesto = $regalum->puesto2;
		$promedio = $regalum->promedio2;
	}
	if ($periodo == 3)
	{
		$puesto = $regalum->puesto3;
		$promedio = $regalum->promedio3;
	}
	if ($periodo == 4)
	{
		$puesto = $regalum->puesto4;
		$promedio = $regalum->promedio4;
	}
	$fchretiro = "";
	if (substr($regalum->fchretiro,0,1) > 0)
	{
		if (strtotime($regalum->fchretiro) >= strtotime($fchinicio) and strtotime($regalum->fchretiro) <= strtotime($fchfinal))
			$fchretiro =  fecha_texto($regalum->fchretiro);
	}
	if ($impdoble=="S" and $hoja%2 > 0)
	{
		$blanco = 1;
		$pdf->AddPage('P',$papel);
	}
	$blanco = 0;
	$hoja = 0;
	$pdf->AddPage('P',$papel);
	$pdf->SetDrawColor(128,128,128);
	$perdidas = 0; //areas perdidas alumno
	$sumnotas = 0; //suma de areas promedio alumno
	$numnotas = 0; //numero de areas promedio alumno
	$generales = 0; //numero de asignaturas generales, no afectan perdida
	$nota = array();
	$pend = array();
	foreach($datarea as $regarea)
	{
		$id_area = $regarea->id_area;
		$area = $regarea->area;
		$areacod = intval($regarea->areacod);
		if ($regarea->perdida > 0)
			$area = "AREA ".$area;
		else
			$generales = $generales + 1;
		//imprimir titulo area
		if ($generales == 0) //asignaturas de areas
		{
			$pdf->SetFont('Arial','B',7);
			$oldposY = $pdf->GetY(); 			
			$pdf->Cell(114,7,substr($area,0,79),'LRT',0,'L',1);
			if ($areadeb > 0 and $areacod==$areadeb) //area que debe a�o anterior
			{
				$pdf->Cell(0,4,"",0,1);
				$pdf->SetFont('Arial','B',5);
				$pdf->SetTextColor(164,0,0);
				$pdf->Cell(114,4,"(REPROBADA A�O ".$anoant.")",0,1);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetY($oldposY);
				$pdf->Cell(114,7,"",0,0);
			}
			//titulos periodos
			$pdf->SetFont('Times','B',6);
			foreach($datperi as $regperi)
			{
				if ($regperi->pertipo == "P")
					$pdf->Cell(10,3,$regperi->perplan,1,0,'C',1);
				else
					$pdf->Cell(10,3,"",1,0,'C',1);
			}			
			$pdf->Cell(0,3,"DEF",'LRT',1,'C',1);
			//notas area
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(114,4,"",0);
			$numfin = 0;
			$notasum = 0;
			$notafin = "";
			$notafinQ = "";
			foreach($datperi as $regperi)
			{
				$notaN = "";
				$notaEx = 0;
				$notaQ = "";
				$pdf->SetTextColor(0,0,0);
				if ($regperi->pertipo == "P" and $regperi->periodo <= $periodo)
				{
					$nper = $regperi->periodo;
					$sql="select nota, notaEx from $notasfa where id_alumno = '$id_alumno' and id_area = '$id_area' and periodo = '$nper' limit 1";
					$datnot=viewsql($sql);
					foreach($datnot as $regnot)
					{
						$notaN = $regnot->nota;
						$notaEx = $regnot->notaEx;
					}
					if ($notaEx > 0 and $periodo >= 3 and $regperi->periodo <= 2)
						$notasum = $notasum + $notaEx;
					else
						$notasum = $notasum + $notaN;
					if ($notaN > 0)
					{
						$decimal = ($notaN-intval($notaN))*100;
						if ($decimal <= 0)
							$notaN = intval($notaN);
						$notaQ = cualitativa($datpunt,$notaN);
						if ($notaN < $notalim)
							$pdf->SetTextColor(164,0,0);
						$numfin = $numfin + 1;
					}
				}
				if ($regperi->pertipo == "P")
					$pdf->Cell(10,4,$notaN." ".$notaQ,1,0,'C',1);
				else
					$pdf->Cell(10,4,$notaN." ".$notaQ,1,0,'C',1);
			}
			$pdf->SetTextColor(0,0,0);
			if ($notasum > 0 and $numfin > 0)
			{
				$notafin = round($notasum/$numfin,1);
				$decimal = ($notafin-intval($notafin))*100;
				if ($decimal <= 0)
					$notafin = intval($notafin);
				$notafinQ = cualitativa($datpunt,$notafin);
				if ($notafin < $notalim and $regarea->perdida > 0) //area afecta perdida del a�o
					$perdidas = $perdidas + 1;
				if ($regarea->promedio > 0) //area afecta promedio del alumno
				{
					$sumnotas = $sumnotas + $notafin;
					$numnotas = $numnotas + 1;
				}
				if ($notafin < $notalim)
					$pdf->SetTextColor(164,0,0);
			}
			$pdf->Cell(0,4,$notafin." ".$notafinQ,1,1,'R',1);
			$pdf->SetTextColor(0,0,0);
		}
		if ($generales == 1) //asignaturas generales (no areas)
		{
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(184,5,"ASIGNATURAS GENERALES",1,0,'L',1);
			$pdf->Cell(0,5,"",1,1,'R',1);
		}			
		//lista de asignaturas
		foreach($datasig as $regasig)
		{
		if ($regasig->id_area == $id_area)
		{
			$id_asignatura = $regasig->id_asignatura;
			$nota = notas_asignatura($ano,$id_alumno,$id_asignatura,$notanum,$datperi);
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(2,7,"","TLB",0);
			$pdf->Cell(102,7,substr($regasig->asignatura,0,70),"TB",0,'L');
			//traer fallas asignatura del periodo
			$numfallas = 0;
			$sql="SELECT horas FROM $fallas WHERE id_alumno = '$id_alumno' and id_asignatura='$id_asignatura' and periodo='$periodo' order by id_falla";
			$datfall = viewsql($sql);
			foreach($datfall as $regfall)
			{$numfallas = $numfallas + intval($regfall->horas);}
			//titulos periodos asignatura
			$pdf->SetFont('Times','B',6);
			$pdf->Cell(10,3,"IH ".$regasig->intensidad,1,0,'C');
			foreach($datperi as $regperi)
			{$pdf->Cell(10,3,$regperi->perplan,1,0,'C');}			
			$pdf->Cell(0,3,"DEF",1,1,'C');
			//imprimit notas periodos asignatura
			$pdf->Cell(104,4,"",0);
			$pdf->SetFont('Arial','B',6);
			$pdf->Cell(10,4,"Fallas ".$numfallas,1,0,'C');
			$pdf->SetFont('Arial','B',7);
			$numfin = 0;
			$notasum = 0;
			$notafin = "";
			$notafinQ = "";
			foreach($datperi as $regperi)
			{
				$notaN = "";
				$notaEx = 0;
				$notaQ = "";
				$pdf->SetTextColor(0,0,0);
				if ($regperi->notanum <= $notanum)
				{
					$nper = $regperi->periodo;
					$tper = $regperi->pertipo;
					$nnum = $regperi->notanum;
					$sql="select pertipo, periodo, nota, notaEx, pendiente, inactivar from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = '$tper' and periodo = '$nper' LIMIT 1";
					$datnot=viewsql($sql);
					foreach($datnot as $regnot)
					{
						$notaN = $regnot->nota;
						$notaEx = $regnot->notaEx;
						if ($regnot->pendiente > 0)
							$notaQ = "PEND";
						if ($regnot->inactivar > 0)
							$notaQ = "INAC";
						if ($notaQ!="")
							$notaN = "";
					}
					if ($regperi->pertipo == "P" and $regperi->periodo <= $periodo and $notaQ=="")
					{
						$numfin = $numfin + 1;
						if ($notaEx > 0 and intval($regperi->periodo) <= 2 and $periodo >= 3)
						{
							$examen = 0;
							//verificar si nota recuperada en examen semestral 1
							$sql="select nota from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = 'E' and periodo = '1' LIMIT 1";
							$datexa=viewsql($sql);
							foreach($datexa as $regexa)
							{$examen = $regexa->nota;}
							if ($examen >= $notalim)
								$notasum = $notasum + $notaEx;
							else
								$notasum = $notasum + $notaN;
						}
						else
							$notasum = $notasum + $notaN;
					}
					if ($notaN > 0)
					{
						$decimal = ($notaN-intval($notaN))*100;
						if ($decimal <= 0)
							$notaN = intval($notaN);
						$notaQ = cualitativa($datpunt,$notaN);
						if ($notaN < $notalim)
							$pdf->SetTextColor(164,0,0);
					}
				}
				$pdf->Cell(10,4,$notaN." ".$notaQ,1,0,'C');
			}
			$pdf->SetTextColor(0,0,0);
			if ($regasig->notaQ != "") //nota default de la asignatura
			{
				$pdf->SetFont('Arial','',5);
				$pdf->Cell(0,4,$regasig->notaQ,1,1,'R');
			}
			else
			{
				if ($notasum > 0 and $numfin > 0)
				{
					$notafin = round($notasum/$numfin,1);
					$decimal = ($notafin-intval($notafin))*100;
					if ($decimal <= 0)
						$notafin = intval($notafin);
					$notaQ = cualitativa($datpunt,$notafin);
					if ($notafin < $notalim)
						$pdf->SetTextColor(164,0,0);
				}
				$pdf->Cell(0,4,$notafin." ".$notaQ,1,1,'R');
				$pdf->SetTextColor(0,0,0);
			}
			//logros perdidos o pendientes periodos anteriores si asignatura final perdida
			if ($notafin < $notalim and $periodo > 1)
			{
				for($i=1; $i<$periodo ;$i++)
				{
					$seekper = $i." ";
					$pos = strpos($inactperi,$seekper); //verificar periodo no inactivo
					if ($pos <= 0) //periodo no inactivo
					{
						$nper = "y".$ano."notasp".$i;
						$sql="select $nper.nota, $nper.notaEx, $nper.pendiente, $logros.id_logro, $logros.logrocod, $logros.logrono from $nper, $logros where $nper.id_alumno='$id_alumno' and $nper.id_asignatura='$id_asignatura' and $nper.nota < '$notalim' and $nper.id_actividad='0' and $nper.inactivar='0' and $nper.id_logro = $logros.id_logro order by $logros.logrocod";
						$datpend = viewsql($sql);
						foreach($datpend as $regpend)
						{
							$swmostrar = 1;
							//verificar si logros periodos 1 y 2 recuperados en examen 1 para boletines 3 en adelante
							if ($periodo >= 3 and $i <= 2 and $regpend->notaEx > 0)
							{
								$examen = 0;
								$sql="select nota from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = 'E' and periodo = '1' LIMIT 1";
								$datexa=viewsql($sql);
								foreach($datexa as $regexa)
								{$examen = $regexa->nota;}
								if ($examen >= $notalim) //si recuperado en examen 1 no lo muestra
									$swmostrar = 0;
							}
							if ($swmostrar==1)
							{
								$notalog = $regpend->nota;
								$pdf->SetTextColor(164,0,0);
								if ($regpend->pendiente > 0)
								{
									$notaQ = "(PEND)";
									$notalog = "";
									$pdf->SetTextColor(0,128,0);
								}
								else
									$notaQ = cualitativa($datpunt,$notalog);
								$pdf->Cell(8,6,"","L",0);
								$pdf->Cell(176,6,"");
								$oldpos = $pdf->GetY(); 
								$pdf->SetFont('Arial','B',7);
								$pdf->Cell(0,6,$notalog." ".$notaQ,1,0,'R');
								$pdf->SetTextColor(0,0,0);
								$pdf->SetFont('Arial','',7);
								$pdf->SetX(10);
								$pdf->SetTextColor(164,0,0);
								$pdf->Cell(5,3,"(P".$i.")",0,0,'C');
								$pdf->SetTextColor(0,0,0);
								$pdf->MultiCell(180,3,$regpend->logrocod.". ".$regpend->logrono,0,'J');
								$pos = $pdf->GetY();
								$dif = $pos - $oldpos;
								if ($dif < 6)
									$pdf->Cell(5,3,"",0,1);
							} //if mostrar
						} //for logros
					} //if inactivo
				} //for periodos  
			}
			//logros asignatura del periodo
			foreach($datlogr as $reglogr)
			{
				if ($reglogr->id_asignatura == $id_asignatura)
				{
					$id_logro = $reglogr->id_logro;
					$notalog = "";
					$notaQ = "";
					$inactivar = 0;
					$pendiente = 0;
					$sql="select nota, pendiente, inactivar from $notasdet ".
						"where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
						"and id_evidencia = '0' and id_actividad = '0' limit 1";
					$datnota=viewsql($sql);
					foreach($datnota as $regnota)
					{
						$notalog = intval($regnota->nota);
						$inactivar = intval($regnota->inactivar);
						$pendiente = intval($regnota->pendiente);
					}
					if ($inactivar == 0) //logro no inactivo
					{
						if ($notalog < $notalim)
							$pdf->SetTextColor(164,0,0);
						if ($pendiente > 0)
						{
							$notalog = "";
							$notaQ = "(PEND)";
							$pdf->SetTextColor(0,128,0);
						}
						else
							$notaQ = cualitativa($datpunt,$notalog);
						$logro = $reglogr->logro;
						if ($notalog < $notalim)
							$logro = $reglogr->logrono;
						if ($notalog == 100)
							$logro = $reglogr->logrosi;
						$pdf->Cell(8,6,"","L",0);
						$pdf->Cell(176,6,"");
						$oldpos = $pdf->GetY(); 
						$pdf->SetFont('Arial','B',7);
						$pdf->Cell(0,6,$notalog." ".$notaQ,1,0,'R');
						$pdf->SetTextColor(0,0,0);
						$pdf->SetX(12);
						$pdf->SetFont('Arial','',7);
						$pdf->MultiCell(180,3,"L".$reglogr->logrocod.". ".$logro,0,'J');
						$pos = $pdf->GetY();
						$dif = $pos - $oldpos;
						if ($dif < 6)
							$pdf->Cell(5,3,"",0,1);
                                                if ($paraevi > 0 and $printevi > 0) //si se manejan evidencias y se imprimen
                                                {
                                                    foreach($datevid as $regevi)
                                                    {
                                                        if ($regevi->id_logro == $id_logro)
                                                        {
                                                            $id_evidencia = $regevi->id_evidencia;
                                                            $notaevi = 0;
                                                            $sql="select nota from $notasdet ".
                                                                    "where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
                                                                    "and id_evidencia = '$id_evidencia' and id_actividad = '0' limit 1";
                                                            $datnota=viewsql($sql);
                                                            foreach($datnota as $regnota)
                                                            {$notaevi = $regnota->nota;}
                                                            $notaQ = "";
                                                            if ($notaevi < $notalim)
                                                                    $pdf->SetTextColor(164,0,0);
                                                            $pdf->Cell(8,6,"","L",0);
                                                            $pdf->Cell(176,6,"");
                                                            $oldpos = $pdf->GetY(); 
                                                            $pdf->SetFont('Arial','B',7);
                                                            $pdf->Cell(0,6,intval($notaevi)." ".$notaQ,1,0,'R');
                                                            $pdf->SetTextColor(0,0,0);
                                                            $pdf->SetX(18);
                                                            $pdf->SetFont('Arial','',7);
                                                            $pdf->MultiCell(175,3,"E".$regevi->numero.". ".$regevi->evidencia,0,'J');
                                                            $pos = $pdf->GetY();
                                                            $dif = $pos - $oldpos;
                                                            if ($dif < 6)
                                                                    $pdf->Cell(5,3,"",0,1);
                                                        }
                                                    }
                                                }
					}
				} //if id_asignatura
			} //for logro
		} //si area
		} //for asignatura
	} //for area
	//observaciones de lista del alumno
	$sql="select $observ.observacion, asignaturas.asignatura from $observ, asignaturas where $observ.id_alumno = '$id_alumno' and $observ.id_observacion > '0' and $observ.pertipo = 'P' and $observ.periodo = '$periodo' and $observ.id_asignatura = asignaturas.id_asignatura order by asignaturas.asignatura";
	$datobse = viewsql($sql);
	if (count($datobse)>0)
	{
		$pdf->SetFont('Arial','B',7);
		$pdf->Cell(0,5,"OBSERVACIONES",1,1,'L',1);
		$pdf->SetFont('Arial','',6);
		$asignatura = "";
		foreach($datobse as $regobse)
		{
			if ($regobse->asignatura!=$asignatura)
			{
				$pdf->Cell(0,0,'',1,1);
				$pdf->SetFont('Arial','B',7);
				$pdf->Cell(0,4,$regobse->asignatura,"LR",1);
				$pdf->SetFont('Arial','',7);
				$asignatura = $regobse->asignatura;
			}
			$pdf->MultiCell(0,4,$regobse->observacion,"LR",'J');
		}
	} //if observaciones
	//promedios
	if ($promedio <= 0 and $numnotas > 0)
		$promedio = round($sumnotas/$numnotas,2);
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(144,6,'AREAS PERDIDAS '.$perdidas,1,0,'L',1);
	$pdf->Cell(30,6,'PROMEDIO '.$promedio,'LTB',0,'C',1);
	$pdf->Cell(0,6,'PUESTO '.$puesto,'RTB',1,'C',1);
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(20,5,'',0,1);
	$pdf->Cell(196,6,'NOVEDADES:','B',1,'L');
	$pdf->Cell(30,15,'  ',0,1);
	$pdf->Cell(30,6,'  ',0,0);
	$pdf->Cell(60,6,'DIRECTOR DE GRUPO','T',0,'C');
	$pdf->Cell(20,6,'  ',0,0);
	$pdf->Cell(60,6,'PADRE DE FAMILIA O ACUDIENTE','T',1,'C');
	$pdf->SetDrawColor(255,255,255);
} //for alumno
$pdf->Output();
disconnect();
?>
