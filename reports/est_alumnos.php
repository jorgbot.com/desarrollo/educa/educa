<?php
/***********************************************************************************************
* @file        : est_alumnos.php (ancho hoja 196)                                              *
* @brief       : estadisticas alumnos por grados                                               *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 14-may-2012                                                    *
* @Modificado  : Nora Rodriguez 14-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$nivel = isset($_GET['nivel']) ? $_GET['nivel'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = ""; 
$antano = $ano - 1;
$titulo = "ESTADISTICAS DE ALUMNOS";
$subtitulo = "A�O LECTIVO ".$ano;
connect();
//niveles
$sql= "select nivel, nombre from niveles order by id_nivel";
$datnive=viewsql($sql);
//alumnos
$sql="SELECT matriculas.grado, matriculas.curso, matriculas.id_alumno, grados.nivel, grados.gradonom, alumnos.genero, matriculas.nivel, matriculas.estado FROM matriculas, grados, alumnos WHERE matriculas.ano = '$ano' AND matriculas.estado = 'CERRADA' AND matriculas.fchretiro like '0000%' AND matriculas.grado = grados.grado AND matriculas.id_alumno = alumnos.id_alumno ";
$condi = "";
if ($nivel != "")
{
	$nomnivel = substr($nivel,1);
	$nivel = substr($nivel,0,1);
	$condi.="AND matriculas.nivel = '$nivel' ";
	$subtitulo.=" - NIVEL ".$nomnivel;
}
if ($grado != "")
{
	$condi.="AND matriculas.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
$sql = $sql.$condi."ORDER BY grados.id_grado, matriculas.curso";




$datalum=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(4);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
				$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//titulos
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(25,5,'Grado',1,0,'C',1);
	$pdf->Cell(15,5,'Curso',1,0,'C',1);
	$pdf->Cell(15,5,'Alumnos',1,0,'C',1);
	$pdf->Cell(15,5,'Nuevos',1,0,'C',1);
	$pdf->Cell(15,5,'Antiguos',1,0,'C',1);
	$pdf->Cell(15,5,'Ni�as',1,0,'C',1);
	$pdf->Cell(15,5,'Ni�os',1,0,'C',1);

	//NUEVO CAMPO

	$pdf->Cell(25,5,'Matriculados',1,1,'C',1);






	//lista de Alumnos
	$alumest = array();
	$totnum = 0;
	$totnue = 0;
	$totant = 0;
	$totmas = 0;
	$totfem = 0;
	$gradonom = "";
	$curso = "";
	//total de matriculados
    $totalmatri = 0;



	$pdf->SetFont('Arial','',8);
	foreach($datalum as $regalum)
	{

		$Var_alumno = 0; 
		$Var_alumno = $regalum->id_alumno;

		if ($regalum->gradonom != $gradonom)
		{
			if ($gradonom != "")
			{
				$pdf->Cell(25,7,$gradonom,1,0,'L');
				$pdf->Cell(15,7,"",1,0,'C');
				$pdf->Cell(15,7,$granum,1,0,'C');
				$pdf->Cell(15,7,$granue,1,0,'C');
				$pdf->Cell(15,7,$graant,1,0,'C');
				$pdf->Cell(15,7,$grafem,1,0,'C');
				$pdf->Cell(15,7,$gramas,1,0,'C');
				//matriculados 
				$pdf->Cell(25,7,$matr,1,1,'C');
			

			}
			$gradonom = $regalum->gradonom;
			$granum = 0;
			$granue = 0;
			$graant = 0;
			$gramas = 0;
			$grafem = 0;
			$curnum = 0;
			$curnue = 0;
			$curant = 0;
			$curmas = 0;
			$curfem = 0;
			$matr = 0;
			$pend = 0;


//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
//en caso de que tengan parametros
		}
		if ($grado!="" and $gradonom != "" and $regalum->curso!=$curso)
		{
			if ($curso!="")
			{
				$pdf->Cell(25,7,$gradonom,1,0,'L');
				$pdf->Cell(15,7,$curso,1,0,'L');
				$pdf->Cell(15,7,$curnum,1,0,'C');
				$pdf->Cell(15,7,$curnue,1,0,'C');
				$pdf->Cell(15,7,$curant,1,0,'C');
				$pdf->Cell(15,7,$curfem,1,0,'C');
				$pdf->Cell(15,7,$curmas,1,0,'C');
				$pdf->Cell(25,7,$matr,1,1,'C');
				

			}


			$curso = $regalum->curso;
			$curnum = 0;
			$curnue = 0;
			$curant = 0;
			$curmas = 0;
			$curfem = 0;
			$matr = 0;
			$pend = 0;



		}
		$granum = $granum + 1;
		$curnum = $curnum + 1;
		$totnum = $totnum + 1;
		$id_alumno = $regalum->id_alumno;
		$estado = "NUE";

		


        //matriculas si es cerraa $sqlarray1 AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
		
        	$sqlarray = ""; 
        	$valoralumno = 0;  
          
          $sqlarray =  "SELECT COUNT( matriculas.id_alumno ) AS alumno FROM matriculas, y".$ano."pagos WHERE y".$ano."pagos.tipo =  'p' 
          AND matriculas.id_alumno ='".$Var_alumno."' AND y".$ano."pagos.id_alumno ='".$Var_alumno."' AND y".$ano."pagos.fchpago!='' AND matriculas.ano ='".$ano."' AND matriculas.estado =  'CERRADA'";

 		  $resul_pagado_pension = mysql_query($sqlarray);  
          $arreglo_pagado_pension = mysql_fetch_array($resul_pagado_pension);
          $valoralumno = $arreglo_pagado_pension['alumno'];

		
          if ($valoralumno >= 1)
          {    
      	 	$matr = $matr + 1;       
          }
        	
			//matriculas si es pendiente $sqlarray1 AQUIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
		
        	$sqlarray1 = ""; 
        	$valoralumno1 = 0;  
          
          $sqlarray1 =  "SELECT COUNT( matriculas.id_alumno ) AS alumno FROM matriculas, y".$ano."pagos WHERE y".$ano."pagos.tipo =  'p' 
          AND matriculas.id_alumno ='".$Var_alumno."' AND y".$ano."pagos.id_alumno ='".$Var_alumno."' AND y".$ano."pagos.fchpago!='' AND matriculas.ano ='".$ano."' AND matriculas.estado =  'PENDIENTE'";

 		  $resul_pagado_pension1 = mysql_query($sqlarray1);  
          $arreglo_pagado_pension1 = mysql_fetch_array($resul_pagado_pension1);
          $valoralumno1 = $arreglo_pagado_pension1['alumno'];

 
          if ($valoralumno1 >= 1)
          {    
      	 	$pend = $pend + 1;       
          }
         

    



		$sql = "SELECT id_matricula FROM matriculas WHERE id_alumno = '$id_alumno' AND matriculas.ano = '$antano' LIMIT 1";
		
		$datmatr = viewsql($sql);
		if (count($datmatr) > 0)
			$estado = "ANT";
		$alumest[$id_alumno] = $estado;
		if ($estado == "NUE")
		{
			$granue = $granue + 1;
			$curnue = $curnue + 1;
			$totnue = $totnue + 1;
		}
		else
		{
			$graant = $graant + 1;
			$curant = $curant + 1;
			$totant = $totant + 1;
		}
		if ($regalum->genero == "M")
		{
			$gramas = $gramas + 1;
			$curmas = $curmas + 1;
			$totmas = $totmas + 1;
		}
		else
		{
			$grafem = $grafem + 1;
			$curfem = $curfem + 1;
			$totfem = $totfem + 1;
		}
	}
	if ($grado!="")
	{
		$pdf->Cell(25,7,$gradonom,1,0,'L');
		$pdf->Cell(15,7,$curso,1,0,'L');
		$pdf->Cell(15,7,$curnum,1,0,'C');
		$pdf->Cell(15,7,$curnue,1,0,'C');
		$pdf->Cell(15,7,$curant,1,0,'C');
		$pdf->Cell(15,7,$curfem,1,0,'C');
		$pdf->Cell(15,7,$curmas,1,0,'C');
		$pdf->Cell(25,7,$matr,1,1,'C');
		

	}
	else
	{
		$pdf->Cell(25,7,$gradonom,1,0,'L');
		$pdf->Cell(15,7,"",1,0,'C');
		$pdf->Cell(15,7,$granum,1,0,'C');
		$pdf->Cell(15,7,$granue,1,0,'C');
		$pdf->Cell(15,7,$graant,1,0,'C');
		$pdf->Cell(15,7,$grafem,1,0,'C');
		$pdf->Cell(15,7,$gramas,1,0,'C');
		$pdf->Cell(25,7,$matr,1,1,'C');
	

	}
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(25,5,'TOTAL',1,0,'C',1);
	$pdf->Cell(15,5,"",1,0,'C',1);


//HACER CURSOR CON CONSUKLTA NUEVA CUMNPLIENDO LA CONDICION DE MATRICULA Y YPAGOS


$sql22="SELECT COUNT( DISTINCT matriculas.id_alumno ) AS 'matriculados' FROM matriculas, y".$ano."pagos WHERE y".$ano."pagos.tipo =  'p'
AND y".$ano."pagos.id_alumno = matriculas.id_alumno AND y".$ano."pagos.fchpago != '' AND matriculas.ano ='".$ano."' AND matriculas.estado ='CERRADA'";


$sql22_resul = mysql_query($sql22);
$sql22_fetch = mysql_fetch_array($sql22_resul);

$total_marriculados = $sql22_fetch['matriculados'];


$sqlp="SELECT COUNT( DISTINCT matriculas.id_alumno ) AS 'pendientes' FROM matriculas, y".$ano."pagos WHERE y".$ano."pagos.tipo =  'p'
AND y".$ano."pagos.id_alumno = matriculas.id_alumno AND y".$ano."pagos.fchpago != '' AND matriculas.ano ='".$ano."' AND matriculas.estado ='PENDIENTE'";


$sqlp_resul = mysql_query($sqlp);
$sqlp_fetch = mysql_fetch_array($sqlp_resul);

$total_pendientes = $sqlp_fetch['pendientes'];


	$pdf->Cell(15,5,number_format($totnum,0),1,0,'C',1);
	$pdf->Cell(15,5,number_format($totnue,0),1,0,'C',1);
	$pdf->Cell(15,5,number_format($totant,0),1,0,'C',1);
	$pdf->Cell(15,5,number_format($totfem,0),1,0,'C',1);
	$pdf->Cell(15,5,number_format($totmas,0),1,0,'C',1);
    $pdf->Cell(25,5,number_format($total_marriculados,0),1,0,'C',1);
	
	


	//totales por niveles
	if ($nivel=="" and $grado=="")
	{
		$pdf->Ln(10);
		   $pdf->Cell(25,5,'Nivel',1,0,'C',1);
		 $pdf->Cell(15,5,'Alumnos',1,0,'C',1);
		  $pdf->Cell(15,5,'Nuevos',1,0,'C',1);
		$pdf->Cell(15,5,'Antiguos',1,0,'C',1);
		   $pdf->Cell(15,5,'Ni�as',1,0,'C',1);
		   $pdf->Cell(15,5,'Ni�os',1,1,'C',1);
		//nuevo

		//$pdf->Cell(25,5,'Matriculados',1,1,'C',1);

 
		$pdf->SetFont('Arial','',8);
		$totnum = 0;
		$totnue = 0;
		$totant = 0;
		$totmas = 0;
		$totfem = 0;
		$nivel = "";

		$totmtr = 0;

		foreach($datnive as $regnive)
		{
			$nivel = $regnive->nivel;
			$pdf->Cell(25,7,$regnive->nombre,1,0,'L');
			$totnum = 0;
			$totnue = 0;
			$totant = 0;
			$totmas = 0;
			$totfem = 0;
			$totmtr = 0;

			$total_marriculados_nvl = 0;
		


			
			foreach($datalum as $regalum)
			{
				if ($regalum->nivel == $nivel)
				{
					
					$totnum = $totnum +1;
					

					$id_alumno = $regalum->id_alumno;
					if ($alumest[$id_alumno] == "NUE")
						$totnue = $totnue +1;
					else
						$totant = $totant +1;
					if ($regalum->genero == "M")
						$totmas = $totmas +1;
					else
						$totfem = $totfem +1;

					
				}


			}

			
		


			$pdf->Cell(15,7,$totnum,1,0,'C');
			$pdf->Cell(15,7,$totnue,1,0,'C');
			$pdf->Cell(15,7,$totant,1,0,'C');
			$pdf->Cell(15,7,$totfem,1,0,'C');
			$pdf->Cell(15,7,$totmas,1,1,'C');
		    //$pdf->Cell(25,7,$totmtr,1,1,'C');  
			
			
	}
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $granumrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
