<?php
/***********************************************************************************************
* @file        : rep_profesores.php (ancho hoja 196)                                           *
* @brief       : Listado General de Docentes                                                   *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($implogo == "")
	$implogo = "";
if ($papel == "")
	$papel = "Letter";
$titulo = "INFORMACI�N GENERAL DE DOCENTES";
$subtitulo = "";
if ($ano > 0)
	$subtitulo = "A�O ".$ano;
if ($grado != "")
	$subtitulo.=" GRADO ".$grado;
connect();
//profesores
$sql = "select * from profesores order by apellidos, nombres";
$datprof=viewsql($sql);
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',8);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
		$this->SetFont('Arial','B',6);
		$this->Cell(45,6,'Apellidos y Nombres',1,0,'L',1);
		$this->Cell(15,6,'C�dula',1,0,'C',1);
		$this->Cell(50,6,'Direcci�n',1,0,'C',1);
		$this->Cell(18,6,'Tel�fonos',1,0,'C',1);
		$this->Cell(53,6,'Cargo y Especialidad',1,0,'C',1);
		$this->Cell(0,6,'Ing-Ret',1,1,'C',1);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("PROFESORES");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
$pdf->SetFont('Arial','',6);
foreach($datprof as $regprof)
{
	//linea 1
	$pdf->Cell(45,6,substr($regprof->apellidos." ".$regprof->nombres,0,32),0,0,'L');
	$pdf->Cell(15,6,$regprof->docunum,0,0,'L');
	$pdf->Cell(50,3,substr($regprof->direccion,0,43),0,0,'L');
	$pdf->Cell(18,3,substr($regprof->telefono,0,60),0,0,'L');
	$pdf->Cell(53,3,substr($regprof->cargo,0,40),0,0,'L');
	$pdf->Cell(0,3,fecha_texto($regprof->fchingreso),0,1,'L');
	//linea 2
	$pdf->Cell(60,3,"",0,0);
	$pdf->Cell(50,3,$regprof->barrio,0,0,'L');
	$pdf->Cell(18,3,$regprof->celular,0,0,'L');
	$pdf->Cell(53,3,substr($regprof->especialidad,0,40),0,0,'L');
	$pdf->Cell(0,3,fecha_texto($regprof->fchretiro),0,1,'L');
	$pdf->Cell(0,0,'',1,1);
}
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,5," TOTAL DOCENTES: ".count($datprof),1,1,'L',1);
$pdf->Output();
disconnect();
?>
