<?php
/***********************************************************************************************
* @file        : rep_fallascon.php (ancho hoja 196)                                            *
* @brief       : fallas consolidadas por alumno y asignaturas en el periodo                    *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 17-may-2012                                                    *
* @Modificado  : Nora Rodriguez 17-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ; 
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$fchini = isset($_GET['fchini']) ? $_GET['fchini'] : null ;
$fchfin = isset($_GET['fchfin']) ? $_GET['fchfin'] : null ;
$id_categoria = isset($_GET['id_categoria']) ? $_GET['id_categoria'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$fchini = fec_amd($fchini);
$fchfin = fec_amd($fchfin);
$titulo = "INASISTENCIAS ASIGNATURAS CONSOLIDADAS POR ALUMNO ";
$subtitulo = fecha_larga($fchini)." AL ".fecha_larga($fchfin);
connect();
//alumnos
$fallas = "y".$ano."fallas";
$cursos = "y".$ano."cursos";
$sql="SELECT $fallas.horas, $fallas.id_alumno, asignaturas.codigo, asignaturas.asignatura, $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres ".
	"FROM $fallas, asignaturas, $cursos WHERE date_format($fallas.fecha,'%Y-%m-%d') between '$fchini' AND '$fchfin' AND $fallas.id_asignatura > 0 ";
if ($id_categoria > 0)	
	$sql.="AND $fallas.id_categoria = '$id_categoria' ";
if ($id_alumno > 0)
	$sql.="AND $fallas.id_alumno = '$id_alumno' ";
else
{
	if ($grado != "")
	{
		$sql.="AND $cursos.grado = '$grado' ";
		$subtitulo.=" - GRADO ".$grado;
	}
	if ($curso != "")
	{
		$sql.="AND $cursos.curso = '$curso' ";
		$subtitulo.=" CURSO ".$curso;
	}
}
$sql.="AND $fallas.id_asignatura = asignaturas.id_asignatura AND $fallas.id_alumno = $cursos.id_alumno ".
	"ORDER BY abs($cursos.grado), $cursos.curso, $cursos.apellidos, $cursos.nombres, asignaturas.asignatura";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',6);
			$this->Cell(7,5,'Grado',1,0,'C',1);
			$this->Cell(7,5,'Curso',1,0,'C',1);
			$this->Cell(55,5,'Apellidos y Nombres',1,0,'L',1);
                        $this->Cell(10,5,'ID',1,0,'C',1);
			$this->Cell(85,5,'Asignatura',1,0,'C',1);
			$this->Cell(7,5,'C�d',1,0,'C',1);
			$this->Cell(10,5,'Horas',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$pdf->SetFont('Arial','',7);
	//lista de Alumnos
	$id_alumno = "";
	$asignatura = "";
	$suma = 0;
	foreach($datalum as $regalum)
	{
		if ($regalum->id_alumno != $id_alumno)
		{
			if ($id_alumno != "")
			{
				$pdf->Cell(10,5,$suma,'LR',1,'C');
				$pdf->Cell(181,0,'',1,1);
			}
			$id_alumno = $regalum->id_alumno;
			$pdf->Cell(7,5,$regalum->grado,'LR',0,'C');
			$pdf->Cell(7,5,$regalum->curso,'LR',0,'C');
			$pdf->Cell(55,5,substr($regalum->apellidos." ".$regalum->nombres,0,32),'LR',0,'L');
                        $pdf->Cell(10,5,$regalum->id_alumno,'LR',0,'C');
			$pdf->Cell(85,5,$regalum->asignatura,'LR',0,'L');
			$pdf->Cell(7,5,$regalum->codigo,'LR',0,'L');
			$asignatura = $regalum->asignatura;
			$suma = 0;
		}
		else
		{
			if ($regalum->asignatura != $asignatura)
			{
				$pdf->Cell(10,5,$suma,'LR',1,'C');
				$pdf->Cell(7,5,"",'LR',0,'C');
				$pdf->Cell(7,5,"",'LR',0,'C');
				$pdf->Cell(55,5,"",'LR',0,'L');
				$pdf->Cell(10,5,"",'LR',0,'L');
				$pdf->Cell(85,5,$regalum->asignatura,'LR',0,'L');
				$pdf->Cell(7,5,$regalum->codigo,'LR',0,'L');
				$asignatura = $regalum->asignatura;
				$suma = 0;
			}
		}
		$suma = $suma + $regalum->horas;
	}
	$pdf->Cell(10,5,$suma,'LR',1,'C');
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(0,5," TOTAL REGISTROS: ".count($datalum),1,1,'L',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
