<?php
/***********************************************************************************************
* @file        : rep_pagosacu.php (ancho hoja 196)                                             *
* @brief       : reporte acumulados de totdos los pagos por mes                                *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-sep-2015                                                    *
* @Modificado  : Nora Rodriguez 23-sep-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$swinteres = isset($_GET['swinteres']) ? $_GET['swinteres'] : 0 ; //liquidar interes
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$exportar = isset($_GET['exportar']) ? $_GET['exportar'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$fchcorte = fec_amd($fchcorte);
$mescorte = intval(substr($fchcorte,5,2));
//porcentaje de interes
connect();
$porcen = 0;
$sql="SELECT interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
$filedet = "y".$ano."pagosdet";
$cursos = "y".$ano."cursos";
$sql="SELECT id_alumno FROM $cursos ";
if ($grado != "")
	$sql.="WHERE grado = '$grado' ";
if ($curso != "")
	$sql.="AND curso = '$curso' ";
$sql.="ORDER BY id_alumno";
$datalum=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ACUMULADO");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$pdf->ln();
	$pdf->ln();
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(0,6,$_SESSION["instinom"],0,1);
	$pdf->ln();	
	$pdf->Cell(0,6,"ACUMULADOS PAGOS MENSUALES Y SALDOS A�O ".$ano,0,1);
	$pdf->ln();	
	$pdf->SetFont('Arial','B',9);
	$pdf->SetTextColor(0,80,0);
	$pdf->Cell(20,5, "CORTE AL ".fecha_texto($fchcorte),0,1,'L');
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','',7);
	$pdf->ln();	
	//titulos linea 1
	$pdf->Cell(10,6,"",'LTB',0,'L',1);
	$pdf->Cell(16,6,"Pensiones",'BTR',0,'C',1);
	$pdf->Cell(16,3,"Pagos",'LTR',0,'C',1);
	$pdf->Cell(13,3,"Pagos",'LTR',0,'C',1);
	$pdf->Cell(16,3,"Pagos",'LTR',0,'C',1);
	$pdf->Cell(16,3,"Deben",'LTR',0,'C',1);
	$pdf->Cell(13,3,"Deben",'LTR',0,'C',1);
	$pdf->Cell(16,3,"Deben",'LTR',0,'C',1);
	$pdf->Cell(15,6,"Transporte",'LTR',0,'C',1);
	$pdf->Cell(15,3,"Pagos",'LTR',0,'C',1);
	$pdf->Cell(15,3,"Deben",'LTR',0,'C',1);
	$pdf->Cell(16,3,"TOTAL",'LTR',0,'C',1);
	$pdf->Cell(16,3,"TOTAL",'LTR',1,'C',1);
	//titulos linea 2
	$pdf->Cell(26);
	$pdf->Cell(16,3,"Pensi�n",'LRB',0,'C',1);
	$pdf->Cell(13,3,"Interes",'LRB',0,'C',1);
	$pdf->Cell(16,3,"Pens + Inter",'LRB',0,'C',1);
	$pdf->Cell(16,3,"Pensi�n",'LRB',0,'C',1);
	$pdf->Cell(13,3,"Interes",'LRB',0,'C',1);
	$pdf->Cell(16,3,"Pens + Inter",'LRB',0,'C',1);
	$pdf->Cell(15);
	$pdf->Cell(15,3,"Transporte",'LRB',0,'C',1);
	$pdf->Cell(15,3,"Transporte",'LRB',0,'C',1);
	$pdf->Cell(16,3,"PAGOS",'LRB',0,'C',1);
	$pdf->Cell(16,3,"DEBEN",'LRB',1,'C',1);
	//arreglos para acumulados
	$valpen = array();
	$pagpen = array();
	$pagint = array();
	$pagmes = array();
	$deupen = array();
	$deuint = array();
	$salpen = array();
	$valtra = array();
	$pagtra = array();
	$saltra = array();
	$pagtot = array();
	$saltot = array();
	for($i=1; $i<=11 ;$i++)
	{
		$mes = $i;
		$valpen[$mes] = 0; //valor pensiones
		$pagpen[$mes] = 0; //pago pensiones
		$pagint[$mes] = 0; //pago intereses
		$pagmes[$mes] = 0; //pago pension + interes
		$deupen[$mes] = 0; //deuda pension
		$deuint[$mes] = 0; //deuda interes
		$salpen[$mes] = 0; //saldo pension + interes
		$valtra[$mes] = 0; //valor transporte
		$pagtra[$mes] = 0; //pagos transporte
		$saltra[$mes] = 0; //saldo transporte
		$pagtot[$mes] = 0; //pago pension + interes + transporte
		$saltot[$mes] = 0; // saldo pension + interes + transporte
	}
	$numtot = 0;
	$numpen = 0;
	$numtra = 0;
	//cargar valores alumno por alumno
	foreach($datalum as $regalum)
	{
		$id_alumno = $regalum->id_alumno;
		$aluvalpen = valores_pension($id_alumno,$ano);
		$aluvaltra = valores_transporte($id_alumno,$ano);
		$alupagpen = array();
		$alupagint = array();
		$alupagtra = array();
		$alusalpen = array();
		$alusalint = array();
		$alusaltra = array();
		for($i=1; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			$alupagpen[$mes] = 0;
			$alupagint[$mes] = 0;
			$alupagtra[$mes] = 0;
			$alusalpen[$mes] = 0;
			$alusalint[$mes] = 0;
			$alusaltra[$mes] = 0;
		}
		$alutotpen = 0;
		$alutottra = 0;
		$sql="SELECT id_pago, tipo, mes, valor, interes, interesliq, interesaju FROM $filedet WHERE id_alumno = '$id_alumno'";
		$datpag = viewsql($sql);
		foreach($datpag as $regpag)
		{
			if (intval($regpag->mes) <= 9)
				$mes = "0".$regpag->mes;
			else
				$mes = $regpag->mes;
			if ($regpag->tipo=="P")
			{
				$alupagpen[$mes] = $alupagpen[$mes] + intval($regpag->valor);
				$alupagint[$mes] = $alupagint[$mes] + intval($regpag->interes);
				if ($swinteres > 0)
				{
					$sint = intval($regpag->interesliq) - intval($regpag->interes) - intval($regpag->interesaju);
					$alusalint[$mes] = $alusalint[$mes] + $sint;
				}
			}
			else
				$alupagtra[$mes] = $alupagtra[$mes] + intval($regpag->valor);
		}
		//calcular saldos
		for($i=1; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			$saldopen = $aluvalpen[$mes] - $alupagpen[$mes];
			$saldotra = $aluvaltra[$mes] - $alupagtra[$mes];
			$interes = 0;
			if ($i > $mescorte)
			{
				$saldopen = 0;
				$saldotra = 0;
			}
			if ($swinteres > 0)
			{
				if ($saldopen > 0  and $porcen > 0 and $i > 1)
					$interes = interes_pension($ano,$mes,$saldopen,$porcen,$fchcorte);
				$interes = $interes + $alusalint[$mes];
			}
			//acumula pensiones
			$valpen[$i] = $valpen[$i] + $aluvalpen[$mes];
			$pagpen[$i] = $pagpen[$i] + $alupagpen[$mes];
			$pagint[$i] = $pagint[$i] + $alupagint[$mes];
			$pagmes[$i] = $pagmes[$i] + $alupagpen[$mes] + $alupagint[$mes];
			$deupen[$i] = $deupen[$i] + $saldopen;
			$deuint[$i] = $deuint[$i] + $interes;
			$salpen[$i] = $salpen[$i] + $saldopen + $interes;
			//acumula transportes
			$valtra[$i] = $valtra[$i] + $aluvaltra[$mes];
			$pagtra[$i] = $pagtra[$i] + $alupagtra[$mes];
			$saltra[$i] = $saltra[$i] + $saldotra;
			//acumula totales
			$pagtot[$i] = $pagtot[$i] + $alupagpen[$mes] + $alupagint[$mes] + $alupagtra[$mes];
			$saltot[$i] = $saltot[$i] + $saldopen + $interes + $saldotra;
			$alutotpen = $alutotpen + $saldopen;
			$alutottra = $alutottra + $saldotra;
		}
		$numtot = $numtot + 1;
		if ($alutotpen > 0)
			$numpen = $numpen + 1;
		if ($alutottra > 0)
			$numtra = $numtra + 1;
	}
	//mostrar arreglos	
	for($i=1; $i<=11 ;$i++)
	{
		$mes = $i;
		if ($mes > 1)
			$nommes = strtoupper(nombre_mes($mes));
		else
			$nommes = "MATRICULA";
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(10,10,substr($nommes,0,3),1,0,'C');
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(16,10,number_format($valpen[$mes],0),1,0,'R');
		$pdf->Cell(16,10,number_format($pagpen[$mes],0),1,0,'R');
		$pdf->Cell(13,10,number_format($pagint[$mes],0),1,0,'R');
		$pdf->Cell(16,10,number_format($pagmes[$mes],0),1,0,'R');
		$pdf->Cell(16,10,number_format($deupen[$mes],0),1,0,'R');
		$pdf->Cell(13,10,number_format($deuint[$mes],0),1,0,'R');
		$pdf->Cell(16,10,number_format($salpen[$mes],0),1,0,'R');
		$pdf->Cell(15,10,number_format($valtra[$mes],0),1,0,'R');
		$pdf->Cell(15,10,number_format($pagtra[$mes],0),1,0,'R');
		$pdf->Cell(15,10,number_format($saltra[$mes],0),1,0,'R');
		$pdf->Cell(16,10,number_format($pagtot[$mes],0),1,0,'R');
		$pdf->Cell(16,10,number_format($saltot[$mes],0),1,1,'R');
	}	
	$totvalpen = 0; 
	$totpagpen = 0;
	$totpagint = 0;
	$totpagmes = 0;
	$totdeupen = 0;
	$totdeuint = 0;
	$totsalpen = 0;
	$totvaltra = 0;
	$totpagtra = 0;
	$totsaltra = 0;
	$totpagtot = 0;
	$totsaltot = 0;
	for($i=1; $i<=11 ;$i++)
	{
		$mes = $i;
		$totvalpen = $totvalpen + $valpen[$mes]; 
		$totpagpen = $totpagpen + $pagpen[$mes];
		$totpagint = $totpagint + $pagint[$mes];
		$totpagmes = $totpagmes + $pagmes[$mes];
		$totdeupen = $totdeupen + $deupen[$mes];
		$totdeuint = $totdeuint + $deuint[$mes];
		$totsalpen = $totsalpen + $salpen[$mes];
		$totvaltra = $totvaltra + $valtra[$mes];
		$totpagtra = $totpagtra + $pagtra[$mes];
		$totsaltra = $totsaltra + $saltra[$mes];
		$totpagtot = $totpagtot + $pagtot[$mes];
		$totsaltot = $totsaltot + $saltot[$mes];
	}
	$pdf->SetFont('Arial','B',6);	
	$pdf->Cell(26,8,number_format($totvalpen,0),1,0,'R',1);
	$pdf->Cell(16,8,number_format($totpagpen,0),1,0,'R',1);
	$pdf->Cell(13,8,number_format($totpagint,0),1,0,'R',1);
	$pdf->Cell(16,8,number_format($totpagmes,0),1,0,'R',1);
	$pdf->Cell(16,8,number_format($totdeupen,0),1,0,'R',1);
	$pdf->Cell(13,8,number_format($totdeuint,0),1,0,'R',1);
	$pdf->Cell(16,8,number_format($totsalpen,0),1,0,'R',1);
	$pdf->Cell(15,8,number_format($totvaltra,0),1,0,'R',1);
	$pdf->Cell(15,8,number_format($totpagtra,0),1,0,'R',1);
	$pdf->Cell(15,8,number_format($totsaltra,0),1,0,'R',1);
	$pdf->Cell(16,8,number_format($totpagtot,0),1,0,'R',1);
	$pdf->Cell(16,8,number_format($totsaltot,0),1,1,'R',1);
	$pdf->Cell(71,8,"ALUMNOS: ".number_format($numtot,0),1,0,'L',1);
	$pdf->Cell(16,8,number_format($numpen,0),1,0,'R',1);
	$pdf->Cell(59,8,"",1,0,'R',1);
	$pdf->Cell(15,8,number_format($numtra,0),1,0,'R',1);
	$pdf->Cell(32,8,"",1,1,'R',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
