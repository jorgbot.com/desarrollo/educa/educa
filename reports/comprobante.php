<?php
/***********************************************************************************************
* @file        : comprobante.php (ancho hoja 196)                                              *
* @brief       : Imprime comprobante de pago en PDF                                            *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
$id_pago = isset($_GET['id_pago']) ? $_GET['id_pago'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$tope = 3;
if ($papel!="LETTER")
	$tope = 4;
connect();
//institucion
$sql="select * from institucion limit 1";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$insnom = $reginst->instinom." ".$reginst->instisig;
	$nit = $reginst->nit;
	$ciudad = $reginst->ciudad;
	$direccion = $reginst->direccion;
	$telefono = $reginst->telefono;
}
//pagos
$file = "y".$ano."pagos";
$sql="SELECT $file.*, alumnos.apellidos, alumnos.nombres FROM $file, alumnos ";
if ($id_pago > 0)
	$sql.="WHERE $file.id_pago = '$id_pago' AND $file.id_alumno = alumnos.id_alumno LIMIT 1";
else
{
	$sql.="WHERE $file.grado = '$grado' ";
	if ($curso != "")
		$sql.="AND $file.curso = '$curso' ";
}
$datalum=viewsql($sql);
$filedet = "y".$ano."pagosdet";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
	}
	function Footer()
	{
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("CARNET");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
//$pdf->SetMargins(5,5,5);
$inipos = $pdf->GetY();
$num = 0;
foreach($datalum as $regalum)
{
	$num = $num + 1;
	//marco negro
	$pdf->Cell(185,78,"",1,1);
	//titulo institucion
	$pos = $inipos + 2;
	$pdf->SetY($pos);
	$pdf->SetFont('Arial','B',10);
	$pdf->SetTextColor(0,80,0);
	$pdf->Cell(5,5,"",0,0);
	$pdf->Cell(90,5,$insnom,0,0);
	$pdf->SetTextColor(0,0,0);
	if ($regalum->tipo=="P")
		$pdf->Cell(83,4,"PENSIONES PAGO N� ".$id_pago,0,1,'R');
	else
		$pdf->Cell(83,4,"TRANSPORTE PAGO N� ".$id_pago,0,1,'R');
	$pdf->SetTextColor(0,80,0);
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(5,4,"",0,0);
	$pdf->Cell(90,4,"NIT ".$nit,0,1,'L');
	$pdf->Cell(5,4,"",0,0);
	$pdf->Cell(100,4,$ciudad." ".$direccion,0,0,'L');
	$pdf->SetFont('Arial','B',10);
	$pdf->ln();
	//datos pago
	$tipo = $regalum->tipo;
	$pdf->ln();
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(5,6,"",0,0);
	$pdf->Cell(60,6,"ALUMNO: ".$regalum->apellidos." ".$regalum->nombres,0,1);
	$pdf->Cell(5,6,"",0,0);
	$pdf->Cell(60,6,"GRADO ".$regalum->grado." CURSO ".$regalum->curso,0,1);
	$pdf->SetFont('Arial','',8);
	$pdf->ln();
	$pdf->Cell(5,6,"",0,0);
	$pdf->Cell(30,6,"HOJA DE ENTRADA: ",0,0);
	$pdf->Cell(30,6,$regalum->id_entrada." - ".fecha_texto($regalum->fchentrada),0,1,'R');
	$pdf->Cell(5,6,"",0,0);
	$pdf->Cell(30,6,"CONSIGNACION/PAGO: ",0,0);
	$pdf->Cell(30,6,fecha_texto($regalum->fchpago),0,1,'R');
	$pdf->Cell(5,6,"",0,0);
	$pdf->Cell(30,6,"VALOR CONSIGNADO: ",0,0);
	$pdf->Cell(30,6,number_format($regalum->valor_con,0),0,1,'R');
	$pdf->Cell(5,6,"",0,0);
	$pdf->Cell(30,6,"VALOR EFECTIVO: ",0,0);
	$pdf->Cell(30,6,number_format($regalum->valor_efe,0),0,1,'R');
	$pdf->Cell(5,6,"",0,0);
	$pdf->Cell(30,6,"VALOR TOTAL: ",0,0);
	$pdf->Cell(30,6,number_format($regalum->valor,0),0,1,'R');
	$pdf->ln();
	//cargar valores
	$sql="SELECT tipo, mes, valor, interes, interesliq, interesaju FROM $filedet WHERE id_pago = '$id_pago' ";
	$datpag=viewsql($sql);
	$pagnet = array();
	$pagint = array();
	$pagmes = array();
	for($i=1; $i<=12 ;$i++)
	{
		$mes = $i;
		$pagnet[$mes] = 0; //pago intereses
		$pagint[$mes] = 0; //pago intereses
		$pagmes[$mes] = 0; //pago mes
	}
	foreach($datpag as $regpag)
	{
		$mes = $regpag->mes;
		$pagnet[$mes] = $regpag->valor;
		$pagint[$mes] = $regpag->interes;
		$pagmes[$mes] = $regpag->valor + $regpag->interes;
	}
	//titulos cuadro
	if ($tipo=="P")
	{
		$pos = $inipos + 8;
		$pdf->SetY($pos);
		$pdf->Cell(120,5,"",0,0);
		$pdf->Cell(18,5,"",1,0,'L',1);
		$pdf->Cell(13,5,"Pensi�n",1,0,'C',1);
		$pdf->Cell(13,5,"Inter�s",1,0,'R',1);
		$pdf->Cell(13,5,"Total",1,1,'R',1);
		for($i=1; $i<=12 ;$i++)
		{
			$mes = $i;
			if ($i > 1)
				$nommes = nombre_mes($mes);
			if ($i==1)
				$nommes = "Matr�cula";
			if ($i==12)
				$nommes = "Saldo a favor";
			$pdf->Cell(120,5,"",0,0);
			$pdf->Cell(18,5,$nommes,1,0,'L',1);
			$pdf->Cell(13,5,number_format($pagnet[$i],0),1,0,'R');
			$pdf->Cell(13,5,number_format($pagint[$i],0),1,0,'R');
			$pdf->Cell(13,5,number_format($pagmes[$i],0),1,1,'R');
		}
	}
	else
	{
		$pos = $inipos + 12;
		$pdf->SetY($pos);
		$pdf->Cell(142,5,"",0,0);
		$pdf->Cell(18,5,"",1,0,'L',1);
		$pdf->Cell(17,5,"Valor Pago",1,1,'C',1);
		for($i=2; $i<=11 ;$i++)
		{
			$mes = $i;
			$nommes = nombre_mes($mes);
			$pdf->Cell(142,5,"",0,0);
			$pdf->Cell(18,5,$nommes,1,0,'L',1);
			$pdf->Cell(17,5,number_format($pagmes[$i],0),1,1,'R');
		}
	}
	if ($num >= $tope)
	{
		$pdf->AddPage('P',$papel);
		$inipos = 10;
		$num = 0;
	}
} //for alumnos
$pdf->Output();
disconnect();
?>
