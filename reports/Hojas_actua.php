<?php
/***********************************************************************************************
* @file        : matricula.php (ancho hoja 196)                                                *
* @brief       : Imprime hoja de matricula en PDF                                              *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 17-sep-2015                                                    *
* @Modificado  : Nora Rodriguez 17-sep-2015                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : 0 ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] :  $_SESSION["anoaca"] ;
$papel = "A4";
$titulo = "HOJA DE DATOS POR ALUMNO";
$exportar = "";
connect();
//parametros institucion
$sql="select instinom, instisig, nit, ciudad, direccion, telefono from institucion limit 1";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$insnom = $reginst->instinom;
	$inssig = $reginst->instisig;
	$insnit = $reginst->nit;
	$insciu = $reginst->ciudad;
	$insdir = $reginst->direccion;
	$instel = $reginst->telefono;
}
if ($id_alumno <= 0) //lote de alumnos por cursos del a�o anterior
{
	$anoant = $ano;
	
	$cursos = "y".$ano."cursos";
	$sql="SELECT id_alumno FROM $cursos ";
	if ($grado != "")
		$sql.="WHERE grado = '$grado' ";
	if ($curso != "")
		$sql.="AND curso = '$curso' ";
	$sql.="ORDER BY CAST(grado AS SIGNED), curso, apellidos, nombres";
}
else //un solo alumno por la matricula del a�o
{
	$anoant = $ano ;	
	$sql="SELECT id_alumno FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
}
$datcur=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		function Header()
		{
		}
		function Footer()
		{
		}
	}
	//iniciar documento PDF
	$gradonom = "";
	$pdf=new PDF();
	$pdf->SetTitle("HOJA DE ACTUALIZACI�N DE DATOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->SetAutoPageBreak(true,5); 
	foreach($datcur as $regcur)
	{
		$id_alumno = $regcur->id_alumno;
		//datos matricula alumno
		$sql="SELECT matriculas.id_matricula, matriculas.grado, matriculas.curso, matriculas.fecha, matriculas.folio, matriculas.id_respmatri, matriculas.id_resppagos, alumnos.id_familiar, alumnos.nombres, alumnos.id_saludenti, alumnos.apellidos, alumnos.docunum, alumnos.docutip, alumnos.docuexp, alumnos.fchnace, alumnos.lugnace, alumnos.genero, alumnos.procegrado, alumnos.proceinsti FROM matriculas, alumnos WHERE ";
		$sql.="matriculas.id_alumno = '$id_alumno' AND matriculas.ano = '$ano' AND matriculas.estado = 'CERRADA' AND matriculas.id_alumno = alumnos.id_alumno LIMIT 1";
		$datalum=viewsql($sql);
		foreach($datalum as $regalum)
		{
			$sisben = $regalum->id_saludenti;
			$id_familiar = $regalum->id_familiar;
			$id_respmatri =  $regalum->id_respmatri;
			$id_resppagos =  $regalum->id_resppagos;
			if (intval($regalum->grado)!=intval($grado))
			{
				$grado = $regalum->grado;
				$sql="select gradonom from grados where grado='$grado' limit 1";
				$dat=viewsql($sql);
				foreach($dat as $reg)
				{$gradonom = $reg->gradonom;}
			}
			$anoact=$ano+1;
			$pdf->AddPage('P',$papel);
			$pdf->SetFont('Arial','B',11);
			$pdf->SetTextColor(0,80,0);
			$pdf->Cell(0,6,$_SESSION["instinom"],0,1,'C');
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','B',11);
			$pdf->Cell(0,6,"HOJA DE ACTUALIZACI�N DE DATOS PARA EL A�O ".$anoact,0,1,'C');
			$pdf->Cell(0,6,"",0,1);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(0,6,"POR FAVOR VERIFIQUE LOS DATOS QUE A CONTINUACI�N SE PRESENTAN, HACER LAS MODIFICACIONES ",0,1,'L');
			$pdf->Cell(0,6,"EN TINTA ROJA Y NO OLVIDE FIRMAR INDEPENDIENTEMENTE DE LAS MODIFICACIONES. ",0,1,'L');
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(18,4,"",0,1);
			$pdf->Cell(18,2,"",0,0);
			$pdf->Cell(16,6,"ALUMNO:",0,0);
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(85,6,$regalum->apellidos. "  ". $regalum->nombres,0,0);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(40,6,"     C�DIGO:  ". $id_alumno."         CURSO:  ". $regalum->curso,0,1);
			$pdf->Cell(0,2,"",0,1);
			
			
			// Tabla matr�cula
			
			$pos=$pdf->GetY();
			$pos=$pos-10;
			$pdf->SetY($pos);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','B',10);
			$pdf->Ln(10);
			
		$dire = " ";
		$tele = " ";
				$sql="select direccion, telefono from familiares where familiares.id_familiar = $id_familiar";
                $fam=viewsql($sql);
				foreach($fam as $fami)
				{
					$dire = $fami->direccion;
					$tele = $fami->telefono;
				}
			// Alumno
			
			$pdf->SetFont('Arial','B',8);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(120,5,"DATOS ESTUDIANTE",'LTR',0,1);
			$pdf->Cell(70,5,"CORRECCI�N (SI LA HAY)",1,1,'C'); 
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','',8);
			$edad = calculo_edad($regalum->fchnace);
			$pdf->Cell(40,5,"FECHA DE NACIMIENTO",1,0,'LTR',1);
			$pdf->Cell(25,5,fecha_texto($regalum->fchnace),'LTR',0,1); 
			$pdf->Cell(10,5,"EDAD",1,0,'LTR',1);
			$pdf->Cell(45,5,$edad,1,0,'LTR',1); 
			$pdf->Cell(70,5," ",'LTR',1,'L',1); 
			$pdf->Cell(40,5,"LUGAR DE NACIMIENTO",1,0,1);
			$pdf->Cell(80,5,$regalum->lugnace,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"TIPO DE ID",1,0,'LTR',1);
			$pdf->Cell(10,5,$regalum->docutip,'LTR',0,1); 
			$pdf->Cell(25,5,"N�MERO DE ID",1,0,'LTR',1);
			$pdf->Cell(45,5,$regalum->docunum,1,0,'LTR',1);			
			$pdf->Cell(70,5," ",'LTR',1,'L',1); 
			$pdf->Cell(40,5,"LUGAR DE EXPEDICI�N",1,0,'LTR',1);
			$pdf->Cell(80,5,$regalum->docuexp,1,0,'LTR',1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"DIRECCION",1,0,'LTR',1);
			$pdf->Cell(80,5,$dire,1,0,'LTR',1);  
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"TEL�FONO",1,0,'LTR',1); 
			$pdf->Cell(80,5,$tele,1,0,'LTR',1); 
			$pdf->Cell(70,5," ",1,'LTR',0,1); 
			$pdf->Cell(0,2,"",0,1);
			
				for($i=1; $i<=3 ;$i++)
			{
				$tipo = "MADRE";
				if ($i==2)
					$tipo = "PADRE";
				$sql="select apellidos, nombres, docunum, docutip, docuexp, direccion, barrio, estrato, telefono, telefofi, celular, correo, profesion from familiares where id_alumno = '$id_alumno' and tipo = '$tipo' limit 1";
				if ($i==3)
				{
					$tipo = "ACUDIENTE";
					$sql="select apellidos, nombres, docunum, docutip, docuexp, direccion, barrio, estrato, telefono, telefofi, celular, correo, profesion from familiares where id_alumno = '$id_alumno' and id_familiar = '$id_respmatri' LIMIT 1";
				}
 
			$pdf->Cell(36);
			$pdf->Cell(50,4," ",0,1);
			$pdf->SetFont('Arial','B',8);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(120,5,"DATOS ".$tipo,'LTR',0,1);
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','',8);
			
			$apellidos = "";
				$nombres = "";
				$docunum = "";
				$docutip = "";
				$docuexp = "";
				$direccion = "";
				$barrio = "";
				$estrato = "";
				$telefono = "";
				$telefofi = "";
				$celular = "";
				$profesion = "";
				$correo = "";
				$dat=viewsql($sql);
				foreach($dat as $reg)
				{
					$apellidos = $reg->apellidos;
					$nombres = $reg->nombres;
					$docunum = $reg->docunum;
					$docutip = $reg->docutip;
					$docuexp = $reg->docuexp;
					$direccion = $reg->direccion;
					$barrio = $reg->barrio;
					$estrato = $reg->estrato;
					$telefono = $reg->telefono;
					$telefofi = $reg->telefofi;
					$celular = $reg->celular;
					$profesion = $reg->profesion;
					$correo = $reg->correo;
				}
				
			$pdf->Cell(40,5,"APELLIDOS Y NOMBRES" ,1,0,'LTR',1);
			$pdf->Cell(80,5,$apellidos." ".$nombres,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"TIPO DE ID" ,1,0,'LTR',1);
			$pdf->Cell(10,5,$docutip,'LTR',0,1); 
			$pdf->Cell(25,5,"N�MERO DE ID" ,1,0,'LTR',1);
			$pdf->Cell(45,5,$docunum,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"LUGAR DE EXPEDICI�N" ,1,0,'LTR',1);
			$pdf->Cell(80,5,$docuexp,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"DIRECCI�N" ,1,0,'LTR',1);
			$pdf->Cell(80,5,$direccion,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"BARRIO" ,1,0,'LTR',1);
			$pdf->Cell(80,5,$barrio,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"TEL�FONO CASA" ,1,0,'LTR',1);
			$pdf->Cell(80,5,$telefono,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"TEL�FONO OFICINA" ,1,0,'LTR',1);
			$pdf->Cell(80,5,$telefofi,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"CELULAR" ,1,0,'LTR',1);
			$pdf->Cell(80,5,$celular,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
			$pdf->Cell(40,5,"PROFESI�N" ,1,0,'LTR',1);
			$pdf->Cell(80,5,$profesion,'LTR',0,1); 
			$pdf->Cell(70,5," ",'LTR',1,1); 
		    $pdf->Cell(40,5,"CORREO",1,0,'LTR',1);
			$pdf->Cell(80,5,$correo,1,0,'LTR',1); 
			$pdf->Cell(70,5," ",1,'LTR',0,1); 
			$pdf->Cell(0,2,"",0,1);
			}
		
	
			//DATOS FAMILIARES
			for($i=1; $i<=4 ;$i++)
			{
				$tipo = "MADRE";
				if ($i==2)
					$tipo = "PADRE";
				$sql="select apellidos, nombres, docunum, docutip, docuexp, direccion, barrio, estrato, telefono, telefofi, celular, correo, profesion from familiares where id_alumno = '$id_alumno' and tipo = '$tipo' limit 1";
				if ($i==3)
				{
					$tipo = "ACUDIENTE";
					$sql="select apellidos, nombres, docunum, docutip, docuexp, direccion, barrio, estrato, telefono, telefofi, celular, correo, profesion from familiares where id_alumno = '$id_alumno' and id_familiar = '$id_respmatri' LIMIT 1";
				}
					if ($i==4)
				{
					$tipo = "ENCARGADO PAGOS";
					$sql="select apellidos, nombres, docunum, docutip, docuexp, direccion, barrio, estrato, telefono, telefofi, celular, correo, profesion from familiares where id_alumno = '$id_alumno' and id_familiar = '$id_resppagos' LIMIT 1";
				}
		
				$apellidos = "";
				$nombres = "";
				$docunum = "";
				$docutip = "";
				$docuexp = "";
				$direccion = "";
				$barrio = "";
				$estrato = "";
				$telefono = "";
				$telefofi = "";
				$celular = "";
				$profesion = "";
				$correo = "";
				$dat=viewsql($sql);
				foreach($dat as $reg)
				{
					$apellidos = $reg->apellidos;
					$nombres = $reg->nombres;
					$docunum = $reg->docunum;
					$docutip = $reg->docutip;
					$docuexp = $reg->docuexp;
					$direccion = $reg->direccion;
					$barrio = $reg->barrio;
					$estrato = $reg->estrato;
					$telefono = $reg->telefono;
					$telefofi = $reg->telefofi;
					$celular = $reg->celular;
					$profesion = $reg->profesion;
					$correo = $reg->correo;
					if ($tipo = "ACUDIENTE"){ $estratoacu=$reg->estrato;}
				}
								
			}
			
			//ACEPTACION
		
		}//fin Alumno	
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(0,1,"",0,1);
		if($estratoacu == 0){
			$pdf->Cell(0,13,"ESTRATO:   1 [  ]   2 [  ]   3 [  ]   4 [  ]   5 [  ]   6 [  ] ",0,1);
		}
		if($estratoacu == 1){
			$pdf->Cell(0,13,"ESTRATO:   1 [ X ]   2 [  ]   3 [  ]   4 [  ]   5 [  ]   6 [  ] ",0,1);
		}
		if($estratoacu == 2){
			$pdf->Cell(0,13,"ESTRATO:   1 [  ]   2 [ X ]   3 [  ]   4 [  ]   5 [  ]   6 [  ] ",0,1);
		}
		if($estratoacu == 3){
			$pdf->Cell(0,13,"ESTRATO:   1 [  ]   2 [  ]   3 [ X ]   4 [  ]   5 [  ]   6 [  ] ",0,1);
		}
		if($estratoacu == 4){
			$pdf->Cell(0,13,"ESTRATO:   1 [  ]   2 [  ]   3 [  ]   4 [ X ]   5 [  ]   6 [  ] ",0,1);
		}
		if($estratoacu == 5){
			$pdf->Cell(0,13,"ESTRATO:   1 [  ]   2 [  ]   3 [  ]   4 [  ]   5 [ X ]   6 [  ] ",0,1);
		}
		if($estratoacu == 6){
			$pdf->Cell(0,13,"ESTRATO:   1 [  ]   2 [  ]   3 [  ]   4 [  ]   5 [  ]   6 [ X ] ",0,1);
		}
		if ($sisben == 1){
		$pdf->Cell(0,4,"PERTENECE A SISB�N:   SI  [ X ]    NO  [   ]      NIVEL 1 [   ]      NIVEL 2 [   ]      NIVEL 3 [   ]",0,1);}
		if ($sisben == 0){
		$pdf->Cell(0,4,"PERTENECE A SISB�N:   SI  [   ]    NO  [ X ]      NIVEL 1 [   ]      NIVEL 2 [   ]      NIVEL 3 [   ] ",0,1);}
		else {$pdf->Cell(0,13,"PERTENECE A SISB�N: NO",0,1);}
		//$pdf->Cell(0,13,"",0,1);
		$pdf->Cell(0,3,"",0,1);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(0,5,"__________________________________",0,1,'R');
		$pdf->Cell(0,5,"   FIRMA DEL PADRE O ACUDIENTE    ",0,1,'R');
		
		
	}//fin cursos

	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
