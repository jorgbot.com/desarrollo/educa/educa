<?php
/***********************************************************************************************
* @file        : rep_pagoscon.php (ancho hoja 196)                                              *
* @brief       : reporte consolidado de pagos por alumno                                        *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-sep-2015                                                    *
* @Modificado  : Nora Rodriguez 23-sep-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$formato = isset($_GET['formato']) ? $_GET['formato'] : "" ;
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$exportar = isset($_GET['exportar']) ? $_GET['exportar'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$swinteres = isset($_GET['swinteres']) ? $_GET['swinteres'] : 0 ; //liquidar interes
$swdeben = isset($_GET['swdeben']) ? $_GET['swdeben'] : 1 ; //solo alumnos que deben
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 1 ; //saltar pagina en cambio de curso
$fchcorte = fec_amd($fchcorte);
$mescorte = substr($fchcorte,5,2);
connect();
$porcentaje = 0;
$sql="SELECT interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcentaje = $reganol->interes;}
if ($tipo=="T")
{
	$titulo = "A�O ".$ano." CONSOLIDADO PAGOS TRANSPORTES POR ALUMNO";
	$swinteres = 0;
}
else
{
	$tipo = "P";
	$titulo = "A�O ".$ano." CONSOLIDADO PAGOS PENSIONES POR ALUMNO";
}
$subtitulo = "SALDOS CORTE A ".fecha_texto($fchcorte);
//alumnos
$filepag = "y".$ano."pagos";
$filedet = "y".$ano."pagosdet";
$fileaju = "y".$ano."pagosaju";
$cursos = "y".$ano."cursos";
$sql="SELECT grado, curso, apellidos, nombres, id_alumno FROM $cursos ";
if ($grado != "")
	$sql.="WHERE grado = '$grado' ";
if ($curso != "")
	$sql.="AND curso = '$curso' ";
$sql.="ORDER BY CAST(grado AS SIGNED), curso, apellidos, nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$formato = $GLOBALS["formato"] ;
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(8,5,'Curso',1,0,'C',1);
			$this->Cell(40,5,'Apellidos y Nombres',1,0,'L',1);
			if ($formato=="DET")
			{
				$this->Cell(6,5,"Ent",'LTB',0,'C',1);
				$this->Cell(15,5,"FchEntrada",'RTB',0,'C',1);
				$this->Cell(8,5,"Pago",'LTB',0,'C',1);
				$this->Cell(15,5,"FchConsig",'RTB',0,'C',1);
			}
			$this->Cell(16,5,"Pago Neto",1,0,'C',1);
			$this->Cell(13,5,"Interes",1,0,'C',1);
			$this->Cell(16,5,"Pago Total",1,0,'C',1);
			$this->Cell(16,5,"Total a Pagar",1,0,'C',1);
			$this->Cell(15,5,"SaldoNeto",1,0,'C',1);
			$this->Cell(13,5,"SaldoInte",1,0,'C',1);
			$this->Cell(15,5,"SaldoTotal",1,1,'C',1);
			$this->SetFont('Arial','',7);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("SALDOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//Lista de Alumnos
	$numalu = 0;
	$totnet = 0;
	$totint = 0;
	$totval = 0;
	$totpagosnet = 0;
	$totsaldonet = 0;
	$totsaldoint = 0;
	$totsaldotot = 0;
	$curso = "";
	foreach($datalum as $regalum)
	{
		if ($swsalto > 0 and $curso!=$regalum->curso) //saltar hoja por curso
		{
			if ($curso!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',6);
				if ($formato=="DET")
					$pdf->Cell(92,5,"ALUMNOS ".$numalu,1,0,'L',1);
				else
					$pdf->Cell(48,5,"ALUMNOS ".$numalu,1,0,'L',1);
				$pdf->Cell(16,5,number_format($totnet,0),1,0,'R',1);
				$pdf->Cell(13,5,number_format($totint,0),1,0,'R',1);
				$pdf->Cell(16,5,number_format($totval,0),1,0,'R',1);
				$pdf->Cell(16,5,number_format($totpagosnet,0),1,0,'R',1);
				$pdf->Cell(15,5,number_format($totsaldonet,0),1,0,'R',1);
				$pdf->Cell(13,5,number_format($totsaldoint,0),1,0,'R',1);
				$pdf->Cell(15,5,number_format($totsaldotot,0),1,1,'R',1);
				$pdf->AddPage('P',$papel);			
			}
			$curso = $regalum->curso;
			$numalu = 0;
			$totnet = 0;
			$totint = 0;
			$totval = 0;
			$totpagosnet = 0;
			$totsaldonet = 0;
			$totsaldoint = 0;
			$totsaldotot = 0;
		}
		//cargar valores alumno
		$id_alumno = $regalum->id_alumno;
		if ($tipo=="T")
			$valores = valores_transporte($id_alumno,$ano);
		else
			$valores = valores_pension($id_alumno,$ano);
		$pagos = array();
		$saldos = array();
		if ($swinteres > 0)
			$salint = array();
		$pagosnet = 0;
		$saldonet = 0;
		$saldoint = 0;
		$saldotot = 0;
		for($i=1; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			$pagos[$mes] = 0;
			$saldos[$mes] = 0;
			if ($swinteres > 0)
				$salint[$mes] = "";
		}
		//$sql="SELECT id_pago, mes, valor, interes, interesliq FROM $filedet WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' AND ano = '$ano' ";
		$sql="SELECT id_pago, mes, valor, interes, interesliq FROM $filedet WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' AND fchpago <= '$fchcorte' ";
		$datpag = viewsql($sql);
		if ($swinteres > 0)
		{
			$sql="SELECT mes, valor FROM $fileaju WHERE id_alumno = '$id_alumno' AND tipo = 'I' AND ano = '$ano' ";
			$dataju = viewsql($sql);
		}
		foreach($datpag as $regpag)
		{
			if (intval($regpag->mes) <= 9)
				$mes = "0".$regpag->mes;
			else
				$mes = $regpag->mes;
			$pagos[$mes] = $pagos[$mes] + intval($regpag->valor);
			if ($swinteres > 0)
			{
				$sint = intval($regpag->interesliq) - intval($regpag->interes);
				$salint[$mes] = $salint[$mes] + $sint;
			}
		}
		//calcular saldos
		for($i=1; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			$saldo = $valores[$mes] - $pagos[$mes];
			$saldos[$mes] = $saldo;
			$interes = 0;
			if ($i > $mescorte)
				$saldo = 0;
			else
				$pagosnet = $pagosnet + $valores[$mes];
			if ($tipo=="P" and $porcentaje > 0 and $swinteres > 0)
			{
				$interes = 0;
				if ($saldo > 0)
					$interes = interes_pension($ano,$mes,$saldo,$porcentaje,$fchcorte);
				foreach($dataju as $regaju)
				{
					if (intval($regaju->mes)==$i)
						$salint[$mes] = $salint[$mes] - intval($regaju->valor);
				}
				if ($salint[$mes] < 0)
					$salint[$mes] = 0;
				$saldos[$mes] = $saldos[$mes] + $salint[$mes] + $interes;
			}
			$saldonet = $saldonet + $saldo;			
			$saldoint = $saldoint + $interes;			
			$saldotot = $saldotot + $saldo + $interes;			
		}
		if ($swdeben == 0 or ($swdeben==1 and $saldotot > 0))
		{
			$numalu = $numalu + 1;
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(8,5,$regalum->curso,'LR',0,'L');
			$pdf->SetFont('Arial','',6);
			$pdf->Cell(40,5,substr($regalum->apellidos." ".$regalum->nombres,0,28),'R',0,'L');
			$pdf->SetFont('Arial','',7);
			$alunet = 0;
			$aluint = 0;
			$aluval = 0;
			$numpag = 0;
//			$sql="SELECT * FROM $filepag WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' AND ano = '$ano' ";
			$sql="SELECT * FROM $filepag WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' AND fchpago <= '$fchcorte' ";
			$datpag = viewsql($sql);
			foreach($datpag as $regpag)
			{
				$numpag = $numpag + 1;
				if ($formato=="DET")
				{
					if ($numpag > 1)
					{
						$pdf->Cell(8,5,"",'LR',0,'L');
						$pdf->Cell(40,5,"",'R',0,'L');
					}
					$pdf->SetFont('Arial','',6);
					$pdf->Cell(6,5,$regpag->id_entrada,0,0,'L');
					$pdf->Cell(15,5,fecha_texto($regpag->fchentrada),'R',0,'L');
					$pdf->Cell(8,5,$regpag->id_pago,0,0,'L');
					$pdf->Cell(15,5,fecha_texto($regpag->fchpago),'R',0,'L');
					$pdf->SetFont('Arial','',7);
					$pdf->Cell(16,5,number_format($regpag->neto,0),'LR',0,'R');
					$pdf->Cell(13,5,number_format($regpag->interes,0),'LR',0,'R');
					$pdf->Cell(16,5,number_format($regpag->valor,0),'LR',0,'R');
					$pdf->Cell(16,5,"",'LR',0,'R');
					$pdf->Cell(15,5,"",'LR',0,'R');
					$pdf->Cell(13,5,"",'LR',0,'R');
					$pdf->Cell(15,5,"",'LR',1,'R');
				}
				$alunet = $alunet + intval($regpag->neto);
				$aluint = $aluint + intval($regpag->interes);
				$aluval = $aluval + intval($regpag->valor);
				$totnet = $totnet + intval($regpag->neto);
				$totint = $totint + intval($regpag->interes);
				$totval = $totval + intval($regpag->valor);
			}
			if ($formato=="DET")
			{
				$pdf->Cell(8,5,"",'LR',0,'L');
				$pdf->Cell(40,5,"",'R',0,'L');
				$pdf->Cell(6,5,"",0,0,'L');
				$pdf->Cell(15,5,"",'R',0,'L');
				$pdf->Cell(8,5,"",0,0,'L');
				$pdf->Cell(15,5,"",'R',0,'L');
			}
			$pdf->Cell(16,5,number_format($alunet,0),'LR',0,'R');
			$pdf->Cell(13,5,number_format($aluint,0),'LR',0,'R');
			$pdf->Cell(16,5,number_format($aluval,0),'LR',0,'R');
			$pdf->Cell(16,5,number_format($pagosnet,0),'LR',0,'R');
			$pdf->Cell(15,5,number_format($saldonet,0),'LR',0,'R');
			$pdf->Cell(13,5,number_format($saldoint,0),'LR',0,'R');
			$pdf->Cell(15,5,number_format($saldotot,0),'LR',1,'R');
			$totpagosnet = $totpagosnet + $pagosnet;
			$totsaldonet = $totsaldonet + $saldonet;
			$totsaldoint = $totsaldoint + $saldoint;
			$totsaldotot = $totsaldotot + $saldotot;
		}
	}
	$pdf->SetFont('Arial','B',6);
	if ($formato=="DET")
		$pdf->Cell(92,5,"ALUMNOS ".$numalu,1,0,'L',1);
	else
		$pdf->Cell(48,5,"ALUMNOS ".$numalu,1,0,'L',1);
	$pdf->Cell(16,5,number_format($totnet,0),1,0,'R',1);
	$pdf->Cell(13,5,number_format($totint,0),1,0,'R',1);
	$pdf->Cell(16,5,number_format($totval,0),1,0,'R',1);
	$pdf->Cell(16,5,number_format($totpagosnet,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($totsaldonet,0),1,0,'R',1);
	$pdf->Cell(13,5,number_format($totsaldoint,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($totsaldotot,0),1,1,'R',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
