<?php
/***********************************************************************************************
* @file        : rep_logros.php (ancho hoja 196)                                               *
* @brief       : Reporte de logros perdidos al periodo de corte                                *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 23-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$corte = isset($_GET['notanum']) ? $_GET['notanum'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($papel == "")
	$papel = "Letter";
if ($corte <= 0)
	$corte = 9;
connect();
if ($id_asignatura > 0)
{
	$sql="select ano, grado from asignaturas where id_asignatura = '$id_asignatura' limit 1";
	$datasig = viewsql($sql);
	foreach($datasig as $regasig)
	{
		$ano = $regasig->ano;
		$grado = $regasig->grado;
	}
}
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = $reganol->notalim;
	$areasper = $reganol->areasper;
}
//todos los periodos
$titcorte = "";
$sql="SELECT notanum, pertipo, periodo, perplan, pernomb, estado FROM periodos WHERE ano = '$ano' order by notanum";
$datperi = viewsql($sql);
foreach($datperi as $regperi)
{
	if ($regperi->notanum > $corte)
		break;
	else
	{
		if ($regperi->notanum == $corte)
		{
			$titcorte = " - ".$regperi->pernomb." (".$regperi->estado.")";
		}
	}
}
$sql="SELECT notanum, pertipo, periodo, perplan, estado FROM periodos WHERE ano = '$ano' and pertipo='P' and notanum <= '$corte' order by notanum";
$peri = viewsql($sql);
//alumnos
$titulo = "LOGROS PERDIDOS A�O ".$ano.$titcorte;
$subtitulo = "GRADO ".$grado;
$cursos = "y".$ano."cursos";
$sql="SELECT $cursos.id_alumno, alumnos.apellidos, alumnos.nombres, alumnos.tipoedu FROM $cursos, alumnos ".
	"WHERE $cursos.grado = '$grado' ";
if ($curso != "")
{
	$sql.="AND $cursos.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
$sql.="AND $cursos.id_alumno = alumnos.id_alumno ORDER BY alumnos.apellidos, alumnos.nombres";
$datalum=viewsql($sql);
//asignaturas
$sql = "select id_area, area, id_asignatura, asignatura from asignaturas ";
if ($id_asignatura!="")
	$sql.= "where id_asignatura = '$id_asignatura' LIMIT 1";
else
	$sql.= "where ano = '$ano' and grado = '$grado' and notas = '1' order by area, asignatura";
$datasig=viewsql($sql);
$logros = "y".$ano."logros";
$notasfa = "y".$ano."notasfa";
$notas = "y".$ano."notas";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("LOGROS");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
$nalu = 0;
$pdf->SetFont('Arial','',7);
foreach($datalum as $regalum)
{
	$id_alumno = $regalum->id_alumno;
	$alumno = $regalum->apellidos." ".$regalum->nombres;
	$tipoedu = $regalum->tipoedu;
	$nasi = 0;
	//puntajes seg�n tipo educaci�n
	$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
	$datpunt = viewsql($sql);
	foreach($peri as $reg)
	{
		$periodo = $reg->periodo;
		$nper = "y".$ano."notasp".$periodo;
		$id_area = "";
		foreach($datasig as $regasig)
		{
			$id_asignatura = $regasig->id_asignatura;
			$nlog = 0;
			//traer logros perdidos no recuperados en examen a la fecha
			$sql="select $nper.nota, $logros.id_logro, $logros.logrocod, $logros.logrono from $nper, $logros where $nper.id_alumno='$id_alumno' and $nper.id_asignatura='$id_asignatura' and $nper.nota < '$notalim' and $nper.notaEx < '$notalim' and $nper.id_actividad='0' and $nper.inactivar='0' and $nper.id_logro = $logros.id_logro order by $logros.logrocod";
			$datpend = viewsql($sql);
			foreach($datpend as $regpend)
			{
				$nasi = $nasi + 1;
				$nlog = $nlog + 1;
				if ($nasi == 1)
				{
					$nalu = $nalu + 1;
					$pdf->SetFont('Arial','B',7);
					$pdf->Cell(60,5,substr($alumno,0,35),1,0,'L',1);
					$pdf->Cell(68,5,"FIRMA:",1,0,'L',1);
					foreach($datperi as $regperi)
					{
						$pdf->Cell(8,5,$regperi->perplan,1,0,'C',1);
					}			
					$pdf->Cell(0,5,"DEF",1,1,'C',1);
				}
				if ($regasig->id_area != $id_area)
				{
					$id_area = $regasig->id_area;
					$nota = notas_area($ano,$id_alumno,$id_area,$corte,$datperi);
					$pdf->SetFont('Arial','U',7);
					$pdf->Cell(5,5,"");
					$pdf->Cell(123,5,$regasig->area,0,0,'L');
					$pdf->SetFont('Arial','',7);
					foreach($datperi as $regperi)
					{
						$notaN = "";
						if ($regperi->notanum <= $corte)
							$notaN = $nota[$regperi->notanum];
						$pdf->Cell(8,5,$notaN,1,0,'C');
					}			
					$notaQ = cualitativa($datpunt,$nota[9]);
					$pdf->Cell(0,5,$nota[9]." ".$notaQ,1,1,'C');
				}
				if ($nlog == 1)
				{
					$notaDef = 0;
					$nota = notas_asignatura($ano,$id_alumno,$id_asignatura,$corte,$datperi);
					$pdf->SetFont('Arial','B',7);
					$pdf->Cell(10,5,"");
					$pdf->Cell(118,5,$regasig->asignatura,0,0,'L');
					$pdf->SetFont('Arial','',7);
					$notaQ = "";
					foreach($datperi as $regperi)
					{
						$pdf->Cell(8,5,$nota[$regperi->notanum],1,0,'C');
					}
					$notaQ = cualitativa($datpunt,$nota[9]);
					$pdf->Cell(0,5,$nota[9]." ".$notaQ,1,1,'C');
				}
				$oldpos = $pdf->GetY();
				$pdf->Cell(184,6,"",0,0);
				$pdf->Cell(0,6,$regpend->nota,1,0,'C');				
				$pdf->SetX(20);
				$pdf->MultiCell(175,3,"P".$periodo." - ".$regpend->logrocod.". ".$regpend->logrono,0,'J');
				$pos = $pdf->GetY();
				$dif = $pos - $oldpos;
				if ($dif == 3)
					$pdf->Cell(5,3,"",0,1);
			} //for notss
		} //for asignaturas	
	} //for periodos
}	
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,5," TOTAL ALUMNOS: ".$nalu,1,1,'L',1);
$pdf->Output();
disconnect();
?>
