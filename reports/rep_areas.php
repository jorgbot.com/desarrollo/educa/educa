<?php
/***********************************************************************************************
* @file        : rep_areas.php (ancho hoja 193)                                                *
* @brief       : Reporte de areas por grado                                                    *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 23-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($papel == "")
	$papel = "Letter"; 
$titulo = "LISTA DE AREAS";
$subtitulo = "A�O ".$ano;
if ($grado != "")
	$subtitulo.=" - GRADO ".$grado;
connect();
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
		$this->SetFont('Arial','B',6);
		$this->Cell(0,5,'AREA',1,1,'L',1);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("AREAS");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
//asignaturas
$sql = "select * from areas where grado = '$grado' ORDER BY area";
$datarea=viewsql($sql);
$id_area = "";
$pdf->SetFont('Arial','',8);
foreach($datarea as $regarea)
{
	$pdf->Cell(0,5,$regarea->area,0,1,'L');
}	
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,5," TOTAL AREAS: ".count($datarea),1,1,'L',1);
$pdf->Output();
disconnect();
?>
