<?php
/***********************************************************************************************
* @file        : rep_pagosmat.php (ancho hoja 196)                                             *
* @brief       : reporte pagos de matriculas                                                   *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-sep-2015                                                    *
* @Modificado  : Nora Rodriguez 23-sep-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$exportar = isset($_GET['exportar']) ? $_GET['exportar'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$swretirados = isset($_GET['swretirados']) ? $_GET['swretirados'] : 0 ; //0=todos, 1=alumnos retirados
$swcampo = isset($_GET['swcampo']) ? $_GET['swcampo'] : 0 ; //imprimir 0=valores, 1=idpago, 2=identrada
$swdeben = isset($_GET['swdeben']) ? $_GET['swdeben'] : 1 ; //solo alumnos que deben
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 1 ; //saltar pagina en cambio de curso
$fchcorte = fec_amd($fchcorte);
$mescorte = substr($fchcorte,5,2);
connect();
$porcen = 0;
$sql="SELECT ano, interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
$tipo = "M";
$titulo = "A�O ".$ano." MATRICULAS PAGOS Y DEUDA ";
$subtitulo = "CORTE A ".fecha_texto($fchcorte);
if ($swcampo==1)
	$subtitulo = $subtitulo." (NUMEROS DE PAGO)"; 
if ($swcampo==2)
	$subtitulo = $subtitulo." (HOJAS DE ENTRADA)"; 
if ($swretirados > 0)
	$subtitulo = $subtitulo." ALUMNOS RETIRADOS"; 
//alumnos
$filepag = "y".$ano."pagos";
$filedet = "y".$ano."pagosdet";
$cursos = "y".$ano."cursos";
$sql="SELECT matriculas.id_alumno, matriculas.grado, matriculas.curso, matriculas.matricula, matriculas.fchretiro, alumnos.apellidos, alumnos.nombres FROM matriculas, alumnos WHERE matriculas.ano = '$ano' AND matriculas.estado='CERRADA' ";
if ($swretirados > 0)
	$sql.="AND matriculas.fchretiro!='0000-00-00'";
if ($grado != "")
	$sql.="AND matriculas.grado = '$grado' ";
if ($curso != "")
	$sql.="AND matriculas.curso = '$curso' ";
$sql.="AND matriculas.id_alumno = alumnos.id_alumno ORDER BY CAST(matriculas.grado AS SIGNED), matriculas.curso, alumnos.apellidos, alumnos.nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(8,5,'Grado',1,0,'C',1);
			$this->Cell(8,5,'Curso',1,0,'C',1);
			$this->Cell(8,5,'ID',1,0,'C',1);
			$this->Cell(50,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(15,5,"Matricula",1,0,'C',1);
			$this->Cell(15,5,"Pagos",1,0,'C',1);
			$this->Cell(15,5,"Entrada",1,0,'C',1);
			$this->Cell(15,5,"IDpago",1,0,'C',1);
			$this->Cell(15,5,"Debe",1,1,'C',1);
			$this->SetFont('Arial','',7);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("MATRICULAS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$curso = "";
	$numalu = 0;
	$totmat = 0;
	$totpag = 0;
	$totsal = 0;
	foreach($datalum as $regalum)
	{
		if ($swsalto > 0 and $curso!=$regalum->curso) //saltar hoja por curso
		{
			if ($curso!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',6);
				$pdf->Cell(66,5,"ALUMNOS ".$numalu,1,0,'L',1);
				$pdf->Cell(15,5,number_format($totmat,0),1,0,'R',1);					
				$pdf->Cell(15,5,number_format($totpag,0),1,0,'R',1);					
				$pdf->Cell(15,5,"",1,0,'R',1);					
				$pdf->Cell(15,5,"",1,0,'R',1);					
				$pdf->Cell(15,5,number_format($totsal,0),1,1,'R',1);					
				$pdf->AddPage('P',$papel);			
			}
			$curso = $regalum->curso;
			$numalu = 0;
			$totmat = 0;
			$totpag = 0;
			$totsal = 0;
		}
		//cargar valores alumno
		$matricula = 0;
		$pagostot = 0;
		$saldotot = 0;
		$idpago = "";
		$identrada = "";
		$id_alumno = $regalum->id_alumno;
		$matricula = intval($regalum->matricula);
		$sql="SELECT $filedet.id_pago, $filedet.valor, $filepag.id_entrada, $filepag.fchentrada FROM $filedet, $filepag WHERE $filedet.id_alumno = '$id_alumno' AND $filedet.ano = '$ano' AND $filedet.mes = '1' AND $filedet.tipo = 'P' AND $filedet.id_pago = $filepag.id_pago";
		$datpag = viewsql($sql);
		foreach($datpag as $regpag)
		{
			$pagostot = $pagostot + intval($regpag->valor);
			$identrada = $regpag->id_entrada." ".substr(fecha_texto($regpag->fchentrada),0,6);
			$idpago = $regpag->id_pago;
		}
		$saldotot = $matricula - $pagostot;
		if ($swdeben == 0 or ($swdeben==1 and $saldotot > 500))
		{
			$numalu = $numalu + 1;
			$totmat = $totmat + $matricula;
			$totpag = $totpag + $pagostot;
			$totsal = $totsal + $saldotot;
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(8,5,$regalum->grado,0,0,'L');
			$pdf->Cell(8,5,$regalum->curso,0,0,'L');
			$pdf->Cell(8,5,$regalum->id_alumno,0,0,'L');
			$pdf->Cell(50,5,substr($regalum->apellidos." ".$regalum->nombres,0,28),0,0,'L');
			$pdf->Cell(15,5,number_format($matricula,0),0,0,'R');
			$pdf->Cell(15,5,number_format($pagostot,0),0,0,'R');
			$pdf->Cell(15,5,$identrada,0,0,'R');
			$pdf->Cell(15,5,$idpago,0,0,'R');
			$pdf->Cell(15,5,number_format($saldotot,0),0,1,'R');
		}
	}
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(75,5,"ALUMNOS ".$numalu,1,0,'L',1);
	$pdf->Cell(15,5,number_format($totmat,0),1,0,'R',1);					
	$pdf->Cell(15,5,number_format($totpag,0),1,0,'R',1);					
	$pdf->Cell(15,5,"",1,0,'R',1);					
	$pdf->Cell(15,5,"",1,0,'R',1);					
	$pdf->Cell(15,5,number_format($totsal,0),1,1,'R',1);					
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
