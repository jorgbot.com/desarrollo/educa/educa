<?php
/***********************************************************************************************
* @file        : rep_alumnoslib.php (ancho hoja 196)                                           *
* @brief       : lista general de alumnos matriculados por grado y/o curso                     *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 17-abr-2014                                                    *
* @Modificado  : Nora Rodriguez 17-abr-2014                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$matricula = isset($_GET['matricula']) ? $_GET['matricula'] : null ;
$estado = isset($_GET['estado']) ? $_GET['estado'] : null ;
$genero = isset($_GET['genero']) ? $_GET['genero'] : null ;
$tipoedu = isset($_GET['tipoedu']) ? $_GET['tipoedu'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$titulo = "LISTADO ALUMNOS LIBRO DE CALIFICACIONES - REGISTRO ESCOLAR";
$subtitulo = "A�O ".$ano;
$antano = $ano - 1;
connect();
$sql="SELECT alumnos.id_alumno, alumnos.apellidos, alumnos.nombres, alumnos.docutip, alumnos.docunum, alumnos.codigo, matriculas.id_matricula, matriculas.ano, matriculas.grado, matriculas.curso, matriculas.folio FROM alumnos, matriculas ".
"WHERE alumnos.id_alumno = matriculas.id_alumno AND matriculas.ano = '$ano' ";
if ($grado != "")
{
	$sql.="AND matriculas.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
if ($curso != "")
{
	$sql.="AND matriculas.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
if ($matricula != "")
{
	$sql.="AND matriculas.estado = '$matricula' ";
	$subtitulo.=" - ESTADO ".$matricula;
}
if ($estado != "")
{
	$sql.="AND alumnos.estado = '$estado' ";
	$subtitulo.=" ESTADO ACTUAL ".$estado;
}
if ($tipoedu != "")
{
	$subtitulo.=" EDUCACION ".$tipoedu;
	$tipoedu = substr($tipoedu,0,1);
	$sql.="AND alumnos.tipoedu = '$tipoedu' ";
}
if ($genero != "")
{
	$subtitulo.=" GENERO ".$genero;
	$genero = substr($genero,0,1);
	$sql.="AND alumnos.genero = '$genero' ";
}
$sql.= "ORDER BY matriculas.grado, alumnos.apellidos, alumnos.nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(15,3,"",0,0);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(15,3,"",0,0);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(15,3,"",0,0);
			$this->Cell(10,5,'N�',1,0,'C',1);
			$this->Cell(10,5,'Grado',1,0,'C',1);
			$this->Cell(10,5,'Curso',1,0,'C',1);
			$this->Cell(75,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(15,5,'Alumno',1,0,'C',1);
			$this->Cell(15,5,'Matricula',1,0,'C',1);
			$this->Cell(15,5,'Folio',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IIMPRESO ".$fecha,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//lista de Alumnos
	$num = 0;
	foreach($datalum as $regalum)
	{
		$num = $num +1;
		$pdf->SetFont('Arial','',8);
		$pdf->Cell(15,3,"",0,0);
		$pdf->Cell(10,5,$num,1,0,'L');
		$pdf->Cell(10,5,$regalum->grado,1,0,'C');
		$pdf->Cell(10,5,$regalum->curso,1,0,'C');
		$pdf->Cell(75,5,$regalum->apellidos." ".$regalum->nombres,1,0,'L');
		$pdf->Cell(15,5,$regalum->id_alumno,1,0,'L');
		$pdf->Cell(15,5,$regalum->id_matricula,1,0,'L');
		$pdf->Cell(15,5,$regalum->folio,1,1,'L');
	}
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(15,3,"",0,0);
	$pdf->Cell(150,5," TOTAL ALUMNOS: ".count($datalum),1,1,'L',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
