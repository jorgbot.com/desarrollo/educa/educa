<?php
/***********************************************************************************************
* @file        : rep_pagosing.php (ancho hoja 196)                                             *
* @brief       : ingresos totalizados por entrada y periodo                                    *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 8-nov-2017                                                     *
* @Modificado  : Nora Rodriguez 8-nov-2017                                                     *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchtip = isset($_GET['fchtip']) ? $_GET['fchtip'] : "ENTRADAS" ;
$fchini = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$fchfin = isset($_GET['fchfin']) ? $_GET['fchfin'] : date('d-m-Y') ;
$id_entrada = isset($_GET['id_entrada']) ? $_GET['id_entrada'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$exportar = isset($_GET['exportar']) ? $_GET['exportar'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$fchini = fec_amd($fchini);
$fchfin = fec_amd($fchfin);
connect();
$titulo = "A�O ".$ano."  INGRESOS TOTALIZADOS POR ENTRADA";
$subtitulo = $fchtip." DEL ".fecha_texto($fchini)." AL ".fecha_texto($fchfin);
//alumnos
$filepag = "y".$ano."pagos";
$filedet = "y".$ano."pagosdet";
$sql="SELECT $filepag.*, alumnos.apellidos, alumnos.nombres FROM $filepag, alumnos ";
if ($id_entrada > 0)
	$sql.="WHERE id_entrada = '$id_entrada' ";
else
{
	if ($fchtip=="ENTRADAS")
		$sql.="WHERE date_format($filepag.fchentrada,'%Y-%m-%d') BETWEEN '$fchini' AND '$fchfin' ";
	else
		$sql.="WHERE date_format($filepag.fchpago,'%Y-%m-%d') BETWEEN '$fchini' AND '$fchfin' ";
}
if ($grado != "")
	$sql.="AND grado = '$grado' ";
if ($curso != "")
	$sql.="AND curso = '$curso' ";
$sql.="AND $filepag.id_alumno = alumnos.id_alumno ORDER BY $filepag.id_entrada";
$datpag=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$tipo = $GLOBALS["tipo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(10,5,"Entrada",1,0,'C',1);
			$this->Cell(18,5,"Fecha",1,0,'C',1);
			$this->Cell(18,5,"Transporte",1,0,'C',1);
			$this->Cell(18,5,"Pensi�n",1,0,'C',1);
			$this->Cell(18,5,"Interes",1,0,'C',1);
			$this->Cell(18,5,"Consignado",1,0,'C',1);
			$this->Cell(18,5,"Efectivo",1,0,'C',1);
			$this->Cell(18,5,"TOTAL",1,1,'C',1);
			$this->SetFont('Arial','',7);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("SALDOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$num = 0;
        $id_entrada = 0;

        $tottra = 0;
        $totpen = 0;
	$totint = 0;
        $totcon = 0;
	$totefe = 0;
	$totval = 0;

        $acutra = 0;
        $acupen = 0;
	$acuint = 0;
        $acucon = 0;
	$acuefe = 0;
	$acuval = 0;

        $tranefe = 0;
	$netoefe = 0;
	$inteefe = 0;
        $trancon = 0;
	$netocon = 0;
	$intecon = 0;
        
        
        foreach($datpag as $regpag)
	{
           if ($regpag->id_entrada != $id_entrada) 
           {
               if ($id_entrada > 0)
               {
                    $pdf->Cell(10,5,$id_entrada,'LR',0,'L');
                    $pdf->Cell(18,5,fecha_texto($fchentrada),'LR',0,'L');
                    $pdf->Cell(18,5,number_format($tottra,0),'LR',0,'R');
                    $pdf->Cell(18,5,number_format($totpen,0),'LR',0,'R');
                    $pdf->Cell(18,5,number_format($totint,0),'LR',0,'R');
                    $pdf->Cell(18,5,number_format($totcon,0),'LR',0,'R');
                    $pdf->Cell(18,5,number_format($totefe,0),'LR',0,'R');
                    $pdf->Cell(18,5,number_format($totval,0),'LR',1,'R');
               }
                $num = $num + 1;
                $id_entrada = $regpag->id_entrada;
                $fchentrada = $regpag->fchentrada;
                $tottra = 0;
                $totpen = 0;
                $totint = 0;
                $totcon = 0;
                $totefe = 0;
                $totval = 0;
            } 
            if ($regpag->tipo=="T") {
                $tottra = $tottra + intval($regpag->neto);
                $acutra = $acutra + intval($regpag->neto);
            }
            else {
                $totpen = $totpen + intval($regpag->neto);
                $acupen = $acupen + intval($regpag->neto);
            }
            $totint = $totint + intval($regpag->interes);
            $totcon = $totcon + intval($regpag->valor_con);
            $totefe = $totefe + intval($regpag->valor_efe);
            $totval = $totval + intval($regpag->valor);
            $acuint = $acuint + intval($regpag->interes);
            $acucon = $acucon + intval($regpag->valor_con);
            $acuefe = $acuefe + intval($regpag->valor_efe);
            $acuval = $acuval + intval($regpag->valor);
               if ($regpag->id_banco <= 0)
		{
                   if ($regpag->tipo=="T")
                        $tranefe = $tranefe + intval($regpag->neto);
                   else
			$netoefe = $netoefe + $regpag->neto;
                   $inteefe = $inteefe + $regpag->interes;
		}
		else
		{
                   if ($regpag->tipo=="T")
                        $trancon = $trancon + intval($regpag->neto);
                   else
			$netocon = $netocon + $regpag->neto;
		   $intecon = $intecon + $regpag->interes;
		}
	}
        //ULTIMA ENTRADA
        $pdf->Cell(10,5,$id_entrada,'LR',0,'L');
        $pdf->Cell(18,5,fecha_texto($fchentrada),'LR',0,'L');
        $pdf->Cell(18,5,number_format($tottra,0),'LR',0,'R');
        $pdf->Cell(18,5,number_format($totpen,0),'LR',0,'R');
        $pdf->Cell(18,5,number_format($totint,0),'LR',0,'R');
        $pdf->Cell(18,5,number_format($totcon,0),'LR',0,'R');
        $pdf->Cell(18,5,number_format($totefe,0),'LR',0,'R');
        $pdf->Cell(18,5,number_format($totval,0),'LR',1,'R');
        //TOTALES FINALES
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(28,4,"TOTAL ".$num,'LTR',0,'R',1);
	$pdf->Cell(18,4,number_format($acutra,0),'LTR',0,'R',1);
	$pdf->Cell(18,4,number_format($acupen,0),'LTR',0,'R',1);
	$pdf->Cell(18,4,number_format($acuint,0),'LTR',0,'R',1);
	$pdf->Cell(18,4,number_format($acucon,0),'LTR',0,'R',1);
	$pdf->Cell(18,4,number_format($acuefe,0),'LTR',0,'R',1);
	$pdf->Cell(18,4,number_format($acuval,0),'LTR',1,'R',1);
	$pdf->Cell(28,4,"CONSIGNADO",'LR',0,'R',1);
	$pdf->Cell(18,4,number_format($trancon,0),'LR',0,'R',1);
	$pdf->Cell(18,4,number_format($netocon,0),'LR',0,'R',1);
	$pdf->Cell(18,4,number_format($intecon,0),'LR',0,'R',1);
	$pdf->Cell(18,4,"",'LR',0,'R',1);
	$pdf->Cell(18,4,"",'LR',0,'R',1);
	$pdf->Cell(18,4,"",'LR',1,'R',1);
	$pdf->Cell(28,4,"EFECTIVO",'LRB',0,'R',1);
	$pdf->Cell(18,4,number_format($tranefe,0),'LRB',0,'R',1);
	$pdf->Cell(18,4,number_format($netoefe,0),'LRB',0,'R',1);
	$pdf->Cell(18,4,number_format($inteefe,0),'LRB',0,'R',1);
	$pdf->Cell(18,4,"",'LRB',0,'R',1);
	$pdf->Cell(18,4,"",'LRB',0,'R',1);
	$pdf->Cell(18,4,"",'LRB',1,'R',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
