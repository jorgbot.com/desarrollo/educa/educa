<?php
/***********************************************************************************************
* @file        : certificados_histlabo.php (ancho hoja 196)                                    *
* @brief       : Genera reporte de certificado de historia laboral por docente                 *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$id_profesor = $_GET["id_profesor"];
$ano = $_GET["ano"];
$grado = $_GET["grado"];
$consecutivo = isset($_GET['consecutivo']) ? $_GET['consecutivo'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
connect();
//institucion
$sql="select * from institucion limit 1";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$rector = $reginst->rector;
	$secretaria = $reginst->secretaria;
	$rectordoc = $reginst->rectordoc;
	$secretariadoc = $reginst->secretariadoc;
	$ciudad = $reginst->ciudad;
}
//a�os lectivos
$sql = "select ano, protocolo from anoslect order by ano";
$datanol = viewsql($sql);
//profesores
$sql="SELECT profesorescont.*, profesores.apellidos, profesores.nombres, profesores.docunum, profesores.docutip, profesores.docuexp, profesores.cargo FROM profesorescont, profesores ";
if ($id_profesor > 0)
	$sql.="WHERE profesores.id_profesor = '$id_profesor' ";
else
{
	$sql.="WHERE profesorescont.ano = '$ano' ";
	if ($grado != "")
		$sql.="AND profesorescont.grados LIKE '% $grado %' ";
}
$sql.="AND profesorescont.id_profesor = profesores.id_profesor GROUP BY profesorescont.id_profesor order by profesores.apellidos, profesores.nombres";
$datprof=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,0,0);
			if ($implogo=="S")
			{
				$datinst = $GLOBALS["datinst"] ;
				foreach($datinst as $reginst)
				{
					$insnom = $reginst->instinom;
					$inssig = $reginst->instisig;
					$insciu = $reginst->ciudad;
					$insdir = $reginst->direccion;
					$instel = $reginst->telefono;
					$insre1 = $reginst->resolkin;
					$insre2 = $reginst->resolpri;
					$insre3 = $reginst->resolsec;
					$inssec = $reginst->resolseed;
					$insdan = $reginst->resoldane;
					$insnot = $reginst->notaria;
				}
				$this->Image('../images/logo.jpg',12,10,20,0,'JPG');
				$this->Ln(5);
				$this->Cell(0,6,$insnom,0,1,'C');
				$this->Cell(0,6,$inssig,0,1,'C');
				$this->SetFont('Arial','',6);
				$this->SetY(30);
				$this->Cell(21,3,"");
				$this->MultiCell(55,3,$insre1,0,'C');
				$this->SetY(30);
				$this->Cell(80,3,"");
				$this->MultiCell(55,3,$insre2,0,'C');
				$this->SetY(30);
				$this->Cell(141,3,"");
				$this->MultiCell(55,3,$insre3,0,'C');
			}
			else
			{
				$this->Ln(26);
			}
			$this->Ln(5);
		}
		function Footer()
		{
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("CERTIFICADO");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	foreach($datprof as $regprof)
	{
	$pdf->AddPage('P',$papel);
	$id_profesor = $regprof->id_profesor;
	$apellidos = $regprof->apellidos;
	$nombres = $regprof->nombres;
	$docutip = $regprof->docutip;
	$docunum = $regprof->docunum;
	$docuexp = $regprof->docuexp;
	$cargo = $regprof->cargo;
	$sql="SELECT lisnom FROM listas WHERE listip = 'IDENTIFICACION' and lisval = '$docutip' LIMIT 1";
	$listide=viewsql($sql);
	foreach($listide as $regiden)
	{$docutip = $regiden->lisnom;}
	//texto del certificado
	$texto = "";
	$sql="SELECT temtex FROM temas WHERE temnom = 'CERTIFICADO DE HISTORIA LABORAL' LIMIT 1";
	$dattem = viewsql($sql);
	foreach($dattem as $regtem)
	{$texto = $regtem->temtex;}
	$texto = str_replace('$apellidos',$apellidos,$texto);
	$texto = str_replace('$nombres',$nombres,$texto);
	$texto = str_replace('$docutip',$docutip,$texto);
	$texto = str_replace('$docunum',$docunum,$texto);
	$texto = str_replace('$docuexp',$docuexp,$texto);
	$texto = str_replace('$cargo',$cargo,$texto);
	if ($ano < substr(date('Y-m-d'),0,4))
		$texto = str_replace('labora','labor�',$texto);
	$pdf->SetFont('Arial','B',12);
	if ($consecutivo != "")
	{
		$pdf->Cell(0,10,'No. '.$consecutivo,0,1,'L');
		$consecutivo = $consecutivo + 1;
	}
	else
		$pdf->Cell(0,10,'',0,1,'L');
	$pdf->Cell(0,8,'LOS SUSCRITOS RECTOR Y SECRETARIA',0,1,'C');
	$pdf->Cell(0,8,'C E R T I F I C A N',0,1,'C');
	$pdf->Ln(5);
	$pdf->SetFont('Arial','',10);
	$pdf->MultiCell(0,5,$texto,0,'J');
	$pdf->Ln(3);
	//historial contratos
	$sql="SELECT * FROM profesorescont WHERE id_profesor = '$id_profesor' order by ano";
	$datcont=viewsql($sql);
	foreach($datcont as $regcont)
	{
		$conano = $regcont->ano;
		$protocolo = "";
		foreach($datanol as $reganol)
		{
			if ($reganol->ano == $conano)
			{
				$protocolo = $reganol->protocolo;
				break;
			}
		}
		$tiempo = strtotime($regcont->fchfinal) - strtotime($regcont->fchinicio);
		$dias = round($tiempo/(60 * 60 * 24),0);
		$meses = round($dias / 30,0);
		$dias = abs($dias - ($meses*30));
		$horas = "";
		$asignaturas = "";
		$asig = "";
		$profasig = "y".$conano."profasig";
		$sql="SELECT asignaturas.asignatura, asignaturas.intensidad, $profasig.curso FROM asignaturas, $profasig WHERE $profasig.id_profesor = '$id_profesor' AND $profasig.id_asignatura = asignaturas.id_asignatura ORDER BY asignaturas.asignatura";
		$datasig = viewsql($sql);
		foreach($datasig as $regasig)
		{
			if ($regasig->asignatura != $asig)
			{
				$asig = $regasig->asignatura;
				if ($asignaturas == "")
					$asignaturas = $regasig->asignatura;
				else
					$asignaturas = $asignaturas.", ".$regasig->asignatura;
			}
			$horas = $horas + intval($regasig->intensidad);
		}
		$grados = trim($regcont->grados);
		$grados = str_replace(' ','�, ',$grados);
		$grados = $grados."�";
		$pdf->SetFont('Arial','',9);
		$pdf->Cell(0,5,"A�O ".$conano.": ".$protocolo,0,1,'L');
		$pdf->Cell(50,5,"TIEMPO TRABAJADO:",0,0,'L');
		$pdf->Cell(30,5,$meses." MESES ".$dias." DIAS.",0,1,'L');
		$pdf->Cell(50,5,"CALIDAD DE PROFESOR:",0,0,'L');
		$pdf->Cell(30,5,"TIEMPO ".$regcont->tiempo,0,1,'L');
		$pdf->Cell(50,5,"HORAS SEMANALES:",0,0,'L');
		$pdf->Cell(30,5,$horas." H",0,1,'L');
		$pdf->Cell(50,5,"GRADOS DICTADOS:",0,0,'L');
		$pdf->Cell(30,5,$grados,0,1,'L');
		$pdf->Cell(50,5,"ASIGNATURAS DICTADAS:",0,0,'L');
		$pdf->MultiCell(145,5,$asignaturas.".",0,'J');
		$pdf->Ln(3);
	}
	$pdf->ln(3);
	$fecha = fecha_carta(date('Y-m-d'));
	$pdf->SetFont('Arial','',10);
	$pdf->Cell(0,5,"El presente certificado se expide en la ciudad de ".$ciudad.", el ".$fecha.".",0,1);
	$pdf->ln(15);
	$pdf->Cell(98,8,"_______________________________",0,0,'C');
	$pdf->Cell(98,8,"_______________________________",0,1,'C');
	$pdf->Cell(98,5,$rector,0,0,'C');
	$pdf->Cell(98,5,$secretaria,0,1,'C');
	$pdf->Cell(98,5,"CC ".$rectordoc,0,0,'C');
	$pdf->Cell(98,5,"CC ".$secretariadoc,0,1,'C');
	$pdf->Cell(98,5,"RECTOR",0,0,'C');
	$pdf->Cell(98,5,"SECRETARIA",0,1,'C');
	} //for alumnos
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
