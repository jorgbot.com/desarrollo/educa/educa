<?php
/***********************************************************************************************
* @file        : carnet.php (ancho hoja 196)                                                   *
* @brief       : Imprime carnet estudiantil por alumno en PDF                                  *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$concepto = isset($_GET['concepto']) ? $_GET['concepto'] : null ;
$papel = $_GET["papel"];
$tope = 4;
if ($papel!="LETTER")
	$tope = 5;
connect();
//institucion
$sql="select * from institucion limit 1";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$insnom = $reginst->instinom;
	$inssig = $reginst->instisig;
	$ciudad = $reginst->ciudad;
	$direccion = $reginst->direccion;
	$telefono = $reginst->telefono;
}
//periodos academicos
$sql="SELECT * FROM periodos WHERE ano = '$ano' and pertipo = 'P' order by notanum";
$datperi=viewsql($sql);
$numperi = count($datperi);
//alumnos
$cursos = "y".$ano."cursos";
$sql="SELECT $cursos.*, grados.gradonom, alumnos.foto FROM $cursos, grados, alumnos ";
if ($id_alumno > 0)
	$sql.="WHERE $cursos.id_alumno = '$id_alumno' AND $cursos.grado = grados.grado AND $cursos.id_alumno = alumnos.id_alumno LIMIT 1";
else
{
	$sql.="WHERE $cursos.grado = '$grado' ";
	if ($curso != "")
		$sql.="AND $cursos.curso = '$curso' ";
	$sql.="AND $cursos.grado = grados.grado AND $cursos.id_alumno = alumnos.id_alumno ORDER BY $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres";
}
$datalum=viewsql($sql);
//echo "$sql<br>";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
	}
	function Footer()
	{
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("CARNET");
$pdf->AliasNbPages();
$pdf->SetDrawColor(0,0,0);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
//$pdf->SetMargins(5,5,5);
$inipos = $pdf->GetY();
$num = 0;
foreach($datalum as $regalum)
{
	$num = $num + 1;
	$foto = trim($regalum->foto);
	if ($foto != "")
		$foto = "../".$foto;
	//marco negro
	$pdf->Cell(90,50,"",1,1);
	//logo 
	$pos = $inipos + 2;
	$pdf->Image('../images/logo.jpg',12,$pos,13,0,'JPG'); //izq, top, ancho
	//titulo institucion
	$pos = $inipos + 2;
	$pdf->SetY($pos);
	$pdf->SetTextColor(0,80,0);
	$pdf->SetFont('Arial','B',10);
	$pdf->Cell(16,5,"",0,0);
	$pdf->Cell(0,5,$insnom,0,1);
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(16,4,"",0,0);
	$pdf->Cell(75,4,$ciudad." ".$direccion,0,1,'L');
	$pdf->Cell(16,4,"",0,0);
	$pdf->Cell(75,4,"Tel: ".$telefono,0,1,'L');
	//foto alumno
	$pos = $inipos + 12;
	$pdf->SetY($pos);
	$pdf->SetX(70);
	$pdf->Cell(27,35,"",0,1,'C',1); //fondo foto
	if (file_exists($foto))
	{
		$pos = $inipos + 14;
		$pdf->Image($foto,72,$pos,23,0,'JPG'); //izq, top, ancho
	}
	//datos carnet
	$pos = $inipos + 20;
	$pdf->SetY($pos);
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(60,8,substr($regalum->apellidos." ".$regalum->nombres,0,32),0,1,'C');
	$pdf->Cell(60,8,"GRADO ".$regalum->gradonom." - ".$regalum->curso,0,1,'C');
	$pdf->ln(5);
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(60,8,"VALIDO HASTA DIC/".$ano,0,1,'C');
	$pdf->Cell(0,10,"",0,1);
	$inipos = $pdf->GetY();
	if ($num >= $tope)
	{
		$pdf->AddPage('P',$papel);
		$inipos = 10;
		$num = 0;
	}
} //for alumnos
$pdf->Output();
disconnect();
?>
