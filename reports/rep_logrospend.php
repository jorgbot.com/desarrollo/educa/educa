<?php
/***********************************************************************************************
* @file        : rep_logrospend.php (ancho hoja 196)                                           *
* @brief       : Reporte de logros pendientes de nota                                          *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 23-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$pertipo = isset($_GET['pertipo']) ? $_GET['pertipo'] : null ;
$periodo = isset($_GET['periodo']) ? $_GET['periodo'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
$id_profasig = isset($_GET['id_profasig']) ? $_GET['id_profasig'] : null ;
$implogros = isset($_GET['implogros']) ? $_GET['implogros'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($papel == "")
	$papel = "Letter";
$tipoedu = "F";
if ($pertipo=="E")
{
	if ($periodo=="1")
		$periodo=2;
	else
		$periodo=4;
}
connect();
if ($id_asignatura > 0)
{
	$sql="select ano, grado from asignaturas where id_asignatura = '$id_asignatura' limit 1";
	$datasig = viewsql($sql);
	foreach($datasig as $regasig)
	{
		$ano = $regasig->ano;
		$grado = $regasig->grado;
	}
}
if ($id_profasig > 0)
{
	$profasig = "y".$ano."profasig";
	$sql="select curso, id_profesor from $profasig where id_profasig = '$id_profasig' limit 1";
	$datasig=viewsql($sql);
	foreach($datasig as $regasig)
	{
		$curso = $regasig->curso;
		$id_profesor = $regasig->id_profesor;
	}
}
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = $reganol->notalim;
	$areasper = $reganol->areasper;
}
$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
$datpunt = viewsql($sql);
//alumnos
$logros = "y".$ano."logros";
$notas = "y".$ano."notas";
$notasp = "y".$ano."notasp".$periodo;
$cursos = "y".$ano."cursos";
$titulo = "LOGROS PENDIENTES A�O ".$ano." - PERIODO ".$periodo;
$subtitulo = "";
$sql="SELECT * FROM $cursos WHERE grado = '$grado' ";
if ($curso != "")
{
	$sql.="AND curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
$sql.="ORDER BY apellidos, nombres";
$datalum=viewsql($sql);
//asignaturas
$sql = "select id_area, area, id_asignatura, asignatura, intensidad from asignaturas ";
if ($id_asignatura!="")
	$sql.= "where id_asignatura = '$id_asignatura' LIMIT 1";
else
	$sql.= "where ano = '$ano' and grado = '$grado' and notas = '1' order by area, asignatura";
$datasig=viewsql($sql);
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(16);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("LOGROS");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$nalu = 0;
$pdf->SetFont('Arial','',7);
foreach($datasig as $regasig)
{
	$id_asignatura = $regasig->id_asignatura;
	$subtitulo = " GRADO ".$grado." CURSO ".$curso." - ".$regasig->asignatura." - IH ".$regasig->intensidad;
	//traer logros de la asignatura para el periodo
	$sql = "SELECT id_logro, logrocod, logro FROM $logros WHERE id_asignatura = '$id_asignatura' AND periodos LIKE '% $periodo %' ";
	$sql.="AND tipoedu = '$tipoedu' ORDER BY logrocod";
	$datlogr=viewsql($sql);
	$numlogr = count($datlogr);
	$nalu = 0;
	foreach($datalum as $regalum)
	{
		$id_alumno = $regalum->id_alumno;
		$alumno = $regalum->apellidos." ".$regalum->nombres;
		//traer logros PENDIENTES
		$sql="select id_logro, nota from $notasp where id_alumno='$id_alumno' and id_asignatura='$id_asignatura' and pendiente='1' and id_actividad='0' order by id_logro";
		$datpend = viewsql($sql);
		//alumno con logros perdidos
		if (count($datpend) > 0)
		{
			$nalu = $nalu + 1;
			if ($nalu == 1)
			{
				$pdf->AddPage('P',$papel);
				if ($implogros == "S")
				{
					foreach($datlogr as $reglogr)
					{
						$pdf->MultiCell(0,4,"L".$reglogr->logrocod.". ".$reglogr->logro,"LTR",'J');
					}
					$pdf->Cell(0,0,'',1,1);
					$pdf->Ln(4);
				}
				$pdf->SetFont('Arial','B',7);
				$pdf->Cell(50,5,"Apellidos y Nombres",1,0,'L',1);
				foreach($datlogr as $reglogr)
				{$pdf->Cell(10,5,"L".$reglogr->logrocod,1,0,'C',1);}	
				$pdf->Cell(12,5,"Total",1,1,'C',1);
				$pdf->SetFont('Arial','',7);
			}
			$pdf->Cell(50,5,substr($alumno,0,32),1,0,'L');
			foreach($datlogr as $reglogr)
			{
				$pdf->Cell(10,5,$nota[$reglogr->id_logro],1,0,'C');
			}
			//traer nota final asignatura del periodo
			$notatot = 0;
			$sql="select nota from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = 'P' and periodo = '$periodo' limit 1";
			$datnota=viewsql($sql);
			foreach($datnota as $regnota)
			{$notatot = $regnota->nota;}
			$notaQ = cualitativa($datpunt,$notatot);
			$pdf->Cell(12,5,$notatot." ".$notaQ,1,1,'C');
		}
	} //for alumnos
	if ($nalu > 0)
	{	
		$len = 50 + ($numlogr * 10) + 12;	
		$pdf->SetFont('Arial','B',6);
		$pdf->Cell($len,5," TOTAL ALUMNOS LOGROS PENDIENTES : ".$nalu,1,1,'L',1);
	}
} //for asignaturas
$pdf->Output();
disconnect();
?>
