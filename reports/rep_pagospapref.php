<?php
/***********************************************************************************************
* @file        : rep_pagospap.php (ancho hoja 196)                                             *
* @brief       : papeletas de alumnos con deuda en pension o matriculas                        *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-oct-2015                                                    *
* @Modificado  : Nora Rodriguez 13-oct-2015                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$id_ruta = isset($_GET['id_ruta']) ? $_GET['id_ruta'] : null ;
$swvalores = isset($_GET['swvalores']) ? $_GET['swvalores'] : 1 ; //Imprimir valores en deuda
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
$exportar = "";
$fchcorte = fec_amd($fchcorte);
$mescorte = substr($fchcorte,5,2);
$filepag = "y".$ano."pagosdet";
connect();
$banco = "";
$sql="SELECT banco FROM institucion limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$banco = $reganol->banco;}
$porcen = 0;
$sql="SELECT ano, interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
$filepag = "y".$ano."pagosdet";

$sql="SELECT matriculas.id_alumno, matriculas.grado, matriculas.curso, alumnos.apellidos, alumnos.nombres, alumnos.genero FROM matriculas, alumnos WHERE  matriculas.ano = '$ano'  AND matriculas.id_alumno = alumnos.id_alumno ";
if ($id_alumno > 0)
{
	$sql.="AND matriculas.id_alumno = '$id_alumno' LIMIT 1";
}
else
{	
	if ($grado != "")
		$sql.="AND grado = '$grado' ";
	if ($curso != "")
		$sql.="AND curso = '$curso' ";
	    $sql.="ORDER BY CAST(grado AS SIGNED), curso, apellidos, nombres";
}
$datalu=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
		}
		function Footer()
		{
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("PAPELETAS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	//lista de Alumnos
	$numalu = 0;
	$gradonom = "";
	$pdf->AddPage('P',$papel);
	foreach($datalu as $regalu)
	{
		$id_alumno = $regalu->id_alumno;
			
		
			$numalu = $numalu + 1;
			if ($numalu > 1)
			$pdf->Cell(0,9,"",0,1);
			
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,80,0);
			$pdf->Cell(0,6,$_SESSION["instinom"],0,1,'C');
			$pdf->SetFont('Arial','B',10);
			
			$pdf->Cell(0,6,"INFORMACI�N IMPORTANTE ",0,1,'C');
			
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','B',9);
			$alumno = "ALUMNO";
			if ($regalu->genero=="F")
				$alumno = "ALUMNA";
			$pdf->Cell(100,5,$alumno.": ".$regalu->nombres." ".$regalu->apellidos,'TL',0);
						
			$pdf->Cell(0,5,"CURSO: ".$regalu->curso,'TR',1,'R');
			$pdf->SetFont('Arial','',7.8);
			$pdf->Cell(0,2,"",'LR',1);
			$pdf->Cell(0,10,"INFORMAMOS EL C�DIGO DE REFERENCIA PARA REALIZAR LOS PAGOS DE MATR�CULA, PENSIONES Y/O SERVICIO DE TRANSPORTE",'LR',1,'C');
			$pdf->Cell(0,1," ESCOLAR",'LR',1,'C');
			
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(0,5,"",'LR',1);
			$pdf->SetFont('Arial','B',8.2);
			$pdf->Cell(74,8," ",'L',0,'');
			$pdf->SetTextColor(180,0,0);
			$pdf->Cell(31,8,"C�DIGO DE REFERENCIA:  ",'',0,'L');
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(0,8,"        ".$id_alumno,'R',1,'L');
			$pdf->SetFont('Arial','',7.8);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(0,2,"",'LR',1);
                        $pdf->SetFont('Arial','B',8.2);
			$pdf->Cell(190,12,$banco." A NOMBRE DEL INSTITUTO DE INTEGRACI�N CULTURAL SAS",'BLR',1,'C');
		
		
			
			
		
		if ($numalu == 4) //Hasta 3 papeletas por hoja
		{
			$pdf->AddPage('P',$papel);
			$numalu = 0;
		}
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
