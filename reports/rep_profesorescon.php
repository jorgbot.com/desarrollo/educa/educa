<?php
/***********************************************************************************************
* @file        : rep_profesorescon.php (ancho hoja 196)                                        *
* @brief       : Listado CONTRATOS  DE Docentes                                                *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($implogo == "")
	$implogo = "";
if ($papel == "")
	$papel = "Letter";
$titulo = "INFORMACI�N CONTRATOS DE DOCENTES";
$subtitulo = "";
if ($ano > 0)
	$subtitulo = "A�O ".$ano;
if ($grado != "")
	$subtitulo.=" GRADO ".$grado;
connect();
//profesores
$sql="SELECT profesorescont.*, profesores.apellidos, profesores.nombres, profesores.docunum, profesores.cargo, profesores.escgrado, profesores.escresol, profesores.especialidad FROM profesorescont, profesores ".
"WHERE profesorescont.ano = '$ano' ";
if ($grado != "")
	$sql.="AND profesorescont.grados LIKE '% $grado %' ";
$sql.="AND profesorescont.id_profesor = profesores.id_profesor order by profesores.apellidos, profesores.nombres";
$datprof=viewsql($sql);
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',10);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
		$this->SetFont('Arial','B',6);
		$this->Cell(45,5,'Apellidos y Nombres',1,0,'L',1);
		$this->Cell(15,5,'C�dula',1,0,'C',1);
		$this->Cell(53,5,'Cargo y Especialidad',1,0,'C',1);
		$this->Cell(25,5,'Escalaf�n',1,0,'C',1);
		$this->Cell(18,5,'Contrato',1,0,'C',1);
		$this->Cell(20,5,'Grados',1,0,'C',1);
		$this->Cell(15,5,'Ini-Fin',1,0,'C',1);
		$this->Cell(0,5,'Est',1,1,'C',1);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("PROFESORES");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
$pdf->SetFont('Arial','',6);
foreach($datprof as $regprof)
{
	$estado = "ACT";
	if ($regprof->estado == "INACTIVO")
		$estado = "INA";
	//linea 1
	$pdf->Cell(45,6,substr($regprof->apellidos." ".$regprof->nombres,0,32),0,0,'L');
	$pdf->Cell(15,6,$regprof->docunum,0,0,'L');
	$pdf->Cell(53,3,substr($regprof->cargo,0,40),0,0,'L');
	$pdf->Cell(25,3,$regprof->escresol,0,0,'L');
	$pdf->Cell(18,3,$regprof->tipocont,0,0,'L');
	$pdf->Cell(20,6,$regprof->grados,0,0,'L');
	$pdf->Cell(15,3,fecha_texto($regprof->fchinicio),0,0,'L');
	$pdf->Cell(0,6,$estado,0,1,'R');
	//linea 2
	$pos=$pdf->GetY();
	$pos=$pos-3;
	$pdf->SetY($pos);
	$pdf->Cell(60,3,"",0,0);
	$pdf->Cell(53,3,substr($regprof->especialidad,0,40),0,0,'L');
	$pdf->Cell(25,3,"GRADO ".$regprof->escgrado,0,0,'L');
	$pdf->Cell(18,3,$regprof->tiempo,0,0,'L');
	$pdf->Cell(20,3,"",0,0);
	$pdf->Cell(15,3,fecha_texto($regprof->fchfinal),0,1,'L');
	$pdf->Cell(0,0,'',1,1);
}
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,5," TOTAL DOCENTES: ".count($datprof),1,1,'L',1);
$pdf->Output();
disconnect();
?>
