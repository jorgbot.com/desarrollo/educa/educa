<?php
/***********************************************************************************************
* @file        : rep_transp.php (ancho hoja 196)                                           *
* @brief       : lista general de alumnos en transporte por grado y/o curso                     *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-oct-2015                                                    *
* @Modificado  : Nora Rodriguez 13-oct-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$estado = isset($_GET['estado']) ? $_GET['estado'] : null ;
$id_ruta = isset($_GET['id_ruta']) ? $_GET['id_ruta'] : null ;
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 1 ; //saltar pagina en cambio de curso
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$fchcorte = fec_amd($fchcorte);
$mescorte = substr($fchcorte,5,2);
$titulo = "A�O ".$ano." ALUMNOS CON TRANSPORTE";
$subtitulo = "CORTE A ".fecha_texto($fchcorte);
$filepag = "y".$ano."pagosdet";
connect();
$sql="SELECT transporte.grado,transporte.curso,transporte.estado,transporte.fchingreso,transporte.fchretiro,transporte.id_ruta,transporte.jornada,transporte.transp02,transporte.transp03,transporte.transp04,transporte.transp05,transporte.transp06,".
"transporte.transp07,transporte.transp08,transporte.transp09,transporte.transp10,transporte.transp11,alumnos.id_alumno,alumnos.apellidos,alumnos.nombres,alumnos.docutip,alumnos.docunum FROM transporte, alumnos ".
"WHERE transporte.ano = '$ano' AND transporte.id_alumno = alumnos.id_alumno ";
if ($grado != "")
{
	$sql.="AND transporte.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
if ($curso != "")
{
	$sql.="AND transporte.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
if ($id_ruta > 0)
{
	$sql.="AND transporte.id_ruta = '$id_ruta' ";
	$subtitulo.=" CURSO ".$curso;
}
if ($estado != "")
{
	$sql.="AND transporte.estado = '$estado' ";
}
$sql.= "ORDER BY ABS(transporte.grado), transporte.curso, alumnos.apellidos, alumnos.nombres";
$datalu=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(11,5,'Curso',1,0,'C',1);
			$this->Cell(44,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(7,5,'ID',1,0,'C',1);
			$this->Cell(16,5,'Ingreso',1,0,'C',1);
			$this->Cell(16,5,'Retiro',1,0,'C',1);
			$this->Cell(45,5,'Ruta y Recorrido',1,0,'C',1);
			$this->Cell(50,5,'Meses Pendientes',1,0,'C',1);
			$this->Cell(12,5,'Deben',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//lista de Alumnos
	$numalu = 0;
	$numdeb = 0;
	$saldotot = 0;
	$curso = "";
	foreach($datalu as $regalu)
	{
		if ($swsalto > 0 and $curso!=$regalu->curso) //saltar hoja por curso
		{
			if ($curso!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',7);
				$pdf->Cell(141,5,"TOTAL ALUMNOS: ".$numalu,1,0,'L',1);
				$pdf->Cell(40,5,"DEBEN: ".$numdeb,'LTB',0,'L',1);
				$pdf->Cell(20,5,number_format($saldotot,0),'RTB',1,'R',1);
				$pdf->AddPage('P',$papel);			
			}
			$curso = $regalu->curso;
			$numalu = 0;
			$numdeb = 0;
			$saldotot = 0;
		}
		$id_alumno = $regalu->id_alumno;
		$numalu = $numalu +1;		
		$valores = array();
		$pagos = array();
		$saldos = array();
		$saldoalu = 0;
		for($i=2; $i<=11 ;$i++)
		{
			$mes = $i;
			$valores[$mes] = 0;
			$pagos[$mes] = 0;
			$saldos[$mes] = 0;
		}
		$valores[2] = intval($regalu->transp02);
		$valores[3] = intval($regalu->transp03);
		$valores[4] = intval($regalu->transp04);
		$valores[5] = intval($regalu->transp05);
		$valores[6] = intval($regalu->transp06);
		$valores[7] = intval($regalu->transp07);
		$valores[8] = intval($regalu->transp08);
		$valores[9] = intval($regalu->transp09);
		$valores[10] = intval($regalu->transp10);
		$valores[11] = intval($regalu->transp11);
		$sql="SELECT id_pago, mes, valor FROM $filepag WHERE id_alumno = '$id_alumno' AND tipo = 'T' AND ano = '$ano' ";
		$datpag = viewsql($sql);
		foreach($datpag as $regpag)
		{
			$mes = $regpag->mes;
			$pagos[$mes] = $pagos[$mes] + intval($regpag->valor);
		}
		$mesespend = "";
		for($i=2; $i<=11 ;$i++)
		{
			$mes = $i;
			$saldo = $valores[$mes] - $pagos[$mes];
			if ($i > $mescorte)
				$saldo = 0;
			if ($saldo > 0)
			{
				$nommes = strtoupper(substr(nombre_mes($mes),0,3));
				$mesespend = $mesespend.$nommes." ";
			}
			$saldos[$mes] = $saldo;
			$saldoalu = $saldoalu + $saldo;			
			$saldotot = $saldotot + $saldo;			
		}
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(4,5,$regalu->grado,1,0,'C');
		$pdf->Cell(7,5,$regalu->curso,1,0,'C');
		$pdf->Cell(44,5,substr($regalu->apellidos." ".$regalu->nombres,0,26),1,0,'L');
		$pdf->Cell(7,5,$regalu->id_alumno,1,0,'L');
		$pdf->Cell(16,5,fecha_texto($regalu->fchingreso),1,0,'L');
		$pdf->Cell(16,5,fecha_texto($regalu->fchretiro),1,0,'L');
		$id_ruta = $regalu->id_ruta;
		$ruta = "";
		$recorrido="";
		$sql = "select ruta, recorrido from rutas where id_ruta = '$id_ruta' LIMIT 1";
		$datrut=viewsql($sql);
		foreach($datrut as $regrut)
		{
			$ruta = $regrut->ruta;
			$recorrido = $regrut->recorrido;
		}
		$pdf->Cell(5,5,$ruta,1,0,'C');
		$pdf->SetFont('Arial','',6);
		$pdf->Cell(40,5,substr($recorrido,0,29),1,0,'L');
		$pdf->Cell(50,5,$mesespend,1,0,'L');
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(12,5,number_format($saldoalu,0),1,1,'R');
		if ($saldoalu > 0)
			$numdeb = $numdeb +1;		
	}
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(141,5,"TOTAL ALUMNOS: ".$numalu,1,0,'L',1);
	$pdf->Cell(40,5,"DEBEN: ".$numdeb,'LTB',0,'L',1);
	$pdf->Cell(20,5,number_format($saldotot,0),'RTB',1,'R',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
