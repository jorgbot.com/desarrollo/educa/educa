<?php
/***********************************************************************************************
* @file        : rep_planillacon.php (ancho hoja 193)                                          *
* @brief       : Genera reporte de planilla consolidada de notas por alumno en PDF             *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 11-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 11-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$formato = isset($_GET['formato']) ? $_GET['formato'] : null ;
$corte = isset($_GET['notanum']) ? $_GET['notanum'] : null ;
$id_alumno = $_GET["id_alumno"];
$ano = $_GET["ano"];
$grado = $_GET["grado"];
$curso = $_GET["curso"];
$tipoedu = $_GET["tipoedu"];
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$formato = trim($formato);
$exportar = "";
$anoant = $ano - 1;
global $hoja;
$hoja = 0;
global $hojafin;
$hojafin = 0;
connect();
//institucion
$sql="select * from institucion limit 1";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$rector = $reginst->rector;
	$secretaria = $reginst->secretaria;
}
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = $reganol->notalim;
	$areasper = $reganol->areasper;
}
//puntajes
$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
$datpunt = viewsql($sql);
//periodos academicos
$cierre="";
$sql="SELECT notanum, pertipo, periodo, perplan, estado FROM periodos WHERE ano = '$ano' order by notanum";
$datperi=viewsql($sql);
$numperi = count($datperi);
foreach($datperi as $regperi)
{
	if ($regperi->pertipo=="P") //estado del ultimo periodo
		$cierre=$regperi->estado;
}
//nombre grado
$superacion = 0;
$sql="SELECT gradonom, superacion FROM grados WHERE grado = '$grado' limit 1";
$datgrad = viewsql($sql);
foreach($datgrad as $reggrad)
{$gradonom = $reggrad->gradonom; $superacion = $reggrad->superacion;}
//areas del grado
$sql="SELECT sum(intensidad) as suma, id_area, area, promedio, perdida FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' group by id_area ORDER BY perdida desc, suma desc, codigo";
$datarea = viewsql($sql);
//asignaturas del grado
$sql="SELECT id_area, id_asignatura, asignatura, porcentaje, intensidad, notas, notaQ FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' and (notas = '1' or boletin = '1') ORDER BY id_area, intensidad desc, codigo";
$datasig = viewsql($sql);
//tablas notas y alumnos
$fallas = "y".$ano."fallas";
$notas = "y".$ano."notas";
$notasfa = "y".$ano."notasfa";
$cursos = "y".$ano."cursos";
$sql="SELECT * FROM $cursos ";
if ($id_alumno > 0)
	$sql.="WHERE id_alumno = '$id_alumno' LIMIT 1";
else
{
	$sql.="WHERE grado = '$grado' ";
	if ($curso != "")
		$sql.="AND curso = '$curso' ";
	$sql.="AND tipoedu = 'F' ORDER BY apellidos, nombres";
}
$datalum=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$id_alumno = $GLOBALS["id_alumno"] ;
			$alumno = $GLOBALS["alumno"] ;
			$gradonom = $GLOBALS["gradonom"] ;
			$curso = $GLOBALS["curso"] ;
			$folio = $GLOBALS["folio"] ;
			$implogo = $GLOBALS["implogo"] ;
			$datpunt = $GLOBALS["datpunt"] ;
			global $hoja;
			global $hojafin;
			$hoja = $hoja + 1;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,0,0);
			if ($implogo=="S")
			{
				if ($hoja==1)
				{
					$datinst = $GLOBALS["datinst"] ;
					foreach($datinst as $reginst)
					{
						$insnom = $reginst->instinom;
						$inssig = $reginst->instisig;
						$insciu = $reginst->ciudad;
						$insdir = $reginst->direccion;
						$instel = $reginst->telefono;
						$insre1 = $reginst->resolkin;
						$insre2 = $reginst->resolpri;
						$insre3 = $reginst->resolsec;
						$inssec = $reginst->resolseed;
						$insdan = $reginst->resoldane;
						$insnot = $reginst->notaria;
					}
					$this->Image('../images/logo.jpg',12,10,20,0,'JPG');
					$this->Ln(5);
					$this->Cell(0,6,$insnom,0,1,'C');
					$this->Cell(0,6,$inssig,0,1,'C');
					$this->SetFont('Arial','',6);
					$this->SetY(30);
					$this->Cell(21,3,"");
					$this->MultiCell(55,3,$insre1,0,'C');
					$this->SetY(30);
					$this->Cell(80,3,"");
					$this->MultiCell(55,3,$insre2,0,'C');
					$this->SetY(30);
					$this->Cell(141,3,"");
					$this->MultiCell(55,3,$insre3,0,'C');
				}
				else
					$this->Ln(1);
			}
			else
			{
				$this->Ln(26);
			}
			$this->Ln(5);
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			$this->Cell(100,6,$titulo,0,1);
			$this->Ln(3);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(20,6,"GRADO ",1,0);
			$this->Cell(85,6,$gradonom." - CURSO ".$curso,1,0);
			$this->SetFont('Arial','B',8);
			$this->Cell(35,6,$folio,1,1);
			$this->Cell(20,6,"ALUMNO ",1,0);
			$this->Cell(85,6,$alumno,1,0);
			$this->Cell(35,6,"MATRICULA N� ".$id_alumno,1,1);
			$pos=$this->GetY();
			$pos=$pos-25;
			$this->SetY($pos);
			//tabla de evaluacion		
			$this->SetDrawColor(0,128,0);
			$this->SetFillColor(0,128,0);
			$this->SetTextColor(255,255,255);
			$this->SetFont('Arial','B',7);
			$this->Cell(146);
			$this->Cell(50,5,"Evaluaci�n",1,1,'C',1);
			$this->SetFillColor(224,231,233);
			$this->SetTextColor(0,0,0);
			foreach($datpunt as $regpunt)
			{
				$this->Cell(146);
				$this->Cell(8,5,$regpunt->notaQ,1,0);
				$this->Cell(30,5,$regpunt->notadet,1,0);
				$this->Cell(0,5,$regpunt->notamin." - ".$regpunt->notamax,1,1);
			}
			$this->SetDrawColor(128,128,128);
			$this->SetTextColor(0,80,0);
			$this->SetFont('Times','B',7);
			$this->Cell(180,10,$subtitulo,0,1);
			$this->SetFont('Arial','',7); 
			$this->SetTextColor(0,0,0);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("NOTAS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$titulo = "INFORME CONSOLIDADO DE NOTAS ".$ano;
	$subtitulo = 'PERIODOS(I,II,III,IV)     EXAMENES SEMESTRALES(ExI, ExII)     EXAMEN FINAL(ExF)     DEFINITIVA(DEF)';
	if ($formato=="FINAL")
		$titulo = "INFORME ACAD�MICO DEFINITIVO ".$ano;
	if ($formato=="LIBRO")
		$titulo = "LIBRO DE CALIFICACIONES ".$ano;
	if ($superacion > 0)
		$subtitulo = 'PERIODOS(I,II,III,IV)     EXAMENES SEMESTRALES(ExI, ExII)     EXAMEN FINAL(ExF)     EXAMEN SUPERACION EXTRAORDINARIA(ExS)      DEFINITIVA(DEF)';
	$numalu = 0;
	foreach($datalum as $regalum)
	{
		$numalu = $numalu + 1;
		$id_alumno = $regalum->id_alumno;
		$alumno = $regalum->apellidos." ".$regalum->nombres;
		$curso = $regalum->curso;
		$promedio = $regalum->promedio;
		$puesto = $regalum->puesto;
		$fchretiro = $regalum->fchretiro;
		$pierde = intval($regalum->pierde);
		$folio = "";
		$sql = "select folio from matriculas where id_alumno = '$id_alumno' and ano = '$ano' limit 1";
		$datmatr=viewsql($sql);
		foreach($datmatr as $regmatr)
		{$folio = "FOLIO N� ".$regmatr->folio;}
		$hoja = 0;
		if ($numalu == 2)
			$hojafin = $pdf->PageNo();
		$pdf->AddPage('P',$papel);
		//titulos columnas
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Arial','B',6);
		$pdf->Cell(100,5,'',1,0,'L',1);
		$pdf->Cell(4,5,'I.H',1,0,'C',1);
		$pdf->Cell(4,5,'%',1,0,'C',1);
		$pdf->Cell(7,5,'Fallas',1,0,'C',1);
		foreach($datperi as $regperi)
		{$pdf->Cell(9,5,$regperi->perplan,1,0,'C',1);}
		if ($superacion > 0)
			$pdf->Cell(8,5,"ExS",1,0,'C',1);
		$pdf->SetDrawColor(0,128,0);
		$pdf->Cell(0,5,"DEF",1,1,'C',1);
		$pdf->SetDrawColor(128,128,128);
		//lista de areas
		$perdidas = 0; //areas perdidas alumno
		$sumnotas = 0; //suma de areas promedio alumno
		$numnotas = 0; //numero de areas promedio alumno
		$generales = 0; //numero de asignaturas generales, no afectan perdida
		$nota = array();
		$pend = array();
		foreach($datarea as $regarea)
		{
			$id_area = $regarea->id_area;
			$area = $regarea->area;
			if ($regarea->perdida > 0)
				$area = "AREA ".$area;
			else
				$generales = $generales + 1;
			$nota = notas_area($ano,$id_alumno,$id_area,$corte,$datperi);
			if ($formato!="") // Boletin Final o Libro Calificaciones, traer notas periodos sin ajustar
			{
				$sql="select periodo, nota from $notasfa where id_alumno = '$id_alumno' and id_area = '$id_area' order by periodo";
				$dattot=viewsql($sql);
				foreach($dattot as $regtot)
				{
					$valnota = $regtot->nota;
					if ($regtot->periodo==1)
						$nota[1] = $regtot->nota;
					if ($regtot->periodo==2)
						$nota[2] = $regtot->nota;
					if ($regtot->periodo==3)
						$nota[4] = $regtot->nota;
					if ($regtot->periodo==4)
						$nota[5] = $regtot->nota;
				}
			}			
			if ($nota[9] > 0)
			{
				if ($nota[9] < $notalim and $regarea->perdida > 0) //area afecta perdida del a�o
					$perdidas = $perdidas + 1;
				if ($regarea->promedio > 0) //area afecta promedio del alumno
				{
					$sumnotas = $sumnotas + $nota[9];
					$numnotas = $numnotas + 1;
				}
			}
			$pdf->SetFillColor(243,247,248);
			if ($generales == 0)
			{
				$pdf->SetFont('Arial','B',6);
				if ($regalum->id_areadeb == $id_area) //area que debe a�o anterior
					$pdf->Cell(100,5,substr($area." (REPROBADA A�O ".$anoant.")",0,79),1,0,'L',1);
				else
					$pdf->Cell(100,5,substr($area,0,79),1,0,'L',1);
				$pdf->Cell(4,5,$regarea->suma,1,0,'C',1);
				$pdf->Cell(4,5,"",1,0,'C',1);
				$pdf->Cell(7,5,"",1,0,'C',1);
				foreach($datperi as $regperi)
				{
					$notaQ = cualitativa($datpunt,$nota[$regperi->notanum]);
					$pdf->Cell(9,5,$nota[$regperi->notanum]." ".$notaQ,1,0,'C',1);
				}
				if ($superacion > 0)
					$pdf->Cell(8,5,"",1,0,'C',1);
				$pdf->SetFillColor(224,231,233);
				$notadef = $nota[9];
				if ($formato=="FINAL")
					$notadef = intval($nota[9]);
				$notaQ = cualitativa($datpunt,$nota[9]);
				if ($regarea->promedio > 0)
					$pdf->SetFillColor(224,231,233);
				if ($nota[9] < $notalim)
					$pdf->SetTextColor(164,0,0);
				$pdf->SetDrawColor(0,128,0);
				$pdf->Cell(0,5,$notadef." ".$notaQ,1,1,'C',1);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetDrawColor(128,128,128);
			}
			if ($generales == 1)
			{
				$pdf->SetFillColor(224,231,233);
				$pdf->SetFont('Arial','B',6);
				$pdf->Cell(100,5,"ASIGNATURAS GENERALES",1,0,'L',1);
				$pdf->Cell(4,5,"",1,0,'C',1);
				$pdf->Cell(4,5,"",1,0,'C',1);
				$pdf->Cell(7,5,"",1,0,'C',1);
				foreach($datperi as $regperi)
				{$pdf->Cell(9,5,"",1,0,'C',1);}
				if ($superacion > 0)
					$pdf->Cell(8,5,"",1,0,'C',1);
				$pdf->SetDrawColor(0,128,0);
				$pdf->Cell(0,5,"",1,1,'C',1);
				$pdf->SetDrawColor(128,128,128);
			}
			//lista de asignaturas
			$pdf->SetFillColor(243,247,248);
			foreach($datasig as $regasig)
			{
				if ($regasig->id_area == $id_area)
				{
				$id_asignatura = $regasig->id_asignatura;
				$porcentaje = $regasig->porcentaje;
				if ($porcentaje <= 0)
					$porcentaje = "";
				//traer fallas asignatura acumuladas a corte
				$numfallas = 0;
				foreach($datperi as $regperi)
				{
					if ($regperi->pertipo=="P" and $regperi->estado=="CERRADO" and $regperi->notanum <= $corte)
					{
						$sql="SELECT horas FROM $fallas WHERE id_alumno = '$id_alumno' and id_asignatura='$id_asignatura' and periodo='".$regperi->periodo."' order by id_falla";
						$datfall = viewsql($sql);
						foreach($datfall as $regfall)
						{$numfallas = $numfallas + intval($regfall->horas);}
					}
				}
				$nota = notas_asignatura($ano,$id_alumno,$id_asignatura,$corte,$datperi);
				if ($formato!="") // Boletin Final o Libro Calificaciones, traer notas periodos sin ajustar
				{
					$sql="select periodo, nota from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = 'P' order by periodo";
					$dattot=viewsql($sql);
					foreach($dattot as $regtot)
					{
						if ($regtot->periodo==1 and $nota[1]!="PEND" and $nota[1]!="INAC")
							$nota[1] = $regtot->nota;
						if ($regtot->periodo==2 and $nota[2]!="PEND" and $nota[2]!="INAC")
							$nota[2] = $regtot->nota;
						if ($regtot->periodo==3 and $nota[4]!="PEND" and $nota[4]!="INAC")
							$nota[4] = $regtot->nota;
						if ($regtot->periodo==4 and $nota[5]!="PEND" and $nota[5]!="INAC")
							$nota[5] = $regtot->nota;
					}
				}			
				$pdf->SetFont('Arial','',6);
				$pdf->Cell(3,5,"","LTB");
				$pdf->Cell(97,5,substr($regasig->asignatura,0,75),"TBR",0,'L');
				$pdf->Cell(4,5,$regasig->intensidad,1,0,'C');
				$pdf->Cell(4,5,$porcentaje,1,0,'C');
				$pdf->Cell(7,5,$numfallas,1,0,'C');
				foreach($datperi as $regperi)
				{
					$notaQ = cualitativa($datpunt,$nota[$regperi->notanum]);
					$pdf->Cell(9,5,$nota[$regperi->notanum]." ".$notaQ,1,0,'C');
				}
				//nota final asignatura
				if ($regasig->notaQ != "") //nota default de la asignatura
				{
					$pdf->SetDrawColor(0,128,0);
					$pdf->SetFont('Arial','',5);
					$pdf->Cell(0,4,$regasig->notaQ,1,1,'R');
				}
				else
				{
					if ($superacion > 0)
						$pdf->Cell(8,5,$nota[8],1,0,'C');
					$pdf->SetDrawColor(0,128,0);
					if ($regarea->promedio > 0 and $generales > 0)
						$pdf->SetFillColor(224,231,233);
					else
						$pdf->SetFillColor(255,255,255);
					$pdf->SetFont('Arial','B',6);
					$notadef = $nota[9];
					if ($formato=="FINAL")
						$notadef = intval($nota[9]);
					$notaQ = cualitativa($datpunt,$nota[9]);
					if ($regasig->notas <= 0 and $notadef <= 0)
						$notadef = "";
					if ($nota[9] < $notalim)
						$pdf->SetTextColor(164,0,0);
					$pdf->Cell(0,5,$notadef." ".$notaQ,1,1,'C',1);
					$pdf->SetTextColor(0,0,0);
				}
				$pdf->SetDrawColor(128,128,128);
				} //si area
			} //for asignaturas
		} //for areas
		if ($numnotas > 0)
			$promedio = round($sumnotas/$numnotas,1);
		$promueve="";
		$puestofin="";
		$promediofin='PROMEDIO AREAS: '.$promedio;				
		if (substr($fchretiro,0,4) > 0)
			$promueve ="*** ALUMNO RETIRADO EL ".fecha_texto($fchretiro)." ***";
		if ($cierre=="CERRADO" and $promueve=="")
		{
			if ($pierde > 0) {
                            if ($grado < 11) {
                             $promueve="*** EL ALUMNO NO ES PROMOVIDO AL GRADO SIGUIENTE ***";   
                            } else {
                              $promueve="*** EL ALUMNO NO ES PROMOVIDO ***";  
                            }
                        } else {
                            if ($grado < 11) {
                             $promueve="*** EL ALUMNO ES PROMOVIDO AL GRADO SIGUIENTE ***";  
                            } else {
                             $promueve="*** EL ALUMNO ES PROMOVIDO ***";
                            }
                            
                        }
			$puestofin='PUESTO: '.$puesto;
		}
		$pdf->SetFont('Arial','B',8);
		$pdf->SetFillColor(224,231,233);
		$pdf->Cell(50,6,'AREAS PERDIDAS: '.$perdidas,'TBL',0,'L',1);
		$pdf->Cell(91,6,$promueve,'TB',0,'L',1);
		$pdf->Cell(0,6,$promediofin."  ".$puestofin,'TBR',1,'R',1);
		$pdf->SetFont('Arial','',7);
		if ($formato!="")
		{
			if ($formato=="LIBRO")
			{
				$nombre1 = $rector;
				$nombre2 = $secretaria;
				$firma1 = "RECTOR";
				$firma2 = "SECRETARIA";
			}
			else
			{
				$nombre1 = "";
				$nombre2 = "";
				$firma1 = "DIRECTOR DE GRUPO";
				$firma2 = "PADRE DE FAMILIA O ACUDIENTE";
			}
			$pdf->ln(15);
			$pdf->Cell(98,6,"_______________________________",0,0,'C');
			$pdf->Cell(98,6,"_______________________________",0,1,'C');
			$pdf->Cell(98,6,$nombre1,0,0,'C');
			$pdf->Cell(98,6,$nombre2,0,1,'C');
			$pdf->Cell(98,6,$firma1,0,0,'C');
			$pdf->Cell(98,6,$firma2,0,1,'C');
		}
	} //for alumnos
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
