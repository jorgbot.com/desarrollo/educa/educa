<?php
/***********************************************************************************************
* @file        : rep_perdidaare.php (ancho hoja 196)                                           *
* @brief       : Reporte de perdidad de areas a fecha de corte                                 *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 15-jul-2013                                                    *
* @Modificado  : Nora Rodriguez 15-jul-2013                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$corte = isset($_GET['notanum']) ? $_GET['notanum'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($papel == "")
	$papel = "Letter";
if ($corte <= 0)
	$corte = 9;
connect();
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = intval($reganol->notalim);
	$areasper = intval($reganol->areasper);
	$estado = $reganol->estado;
}
//todos los periodos tipo nota
$titcorte = "";
$sql="SELECT notanum, pertipo, periodo, perplan, pernomb, estado FROM periodos WHERE ano = '$ano' order by notanum";
$datperi = viewsql($sql);
foreach($datperi as $regperi)
{
	if ($regperi->notanum > $corte)
		break;
	if ($regperi->estado=="CERRADO")
		$titcorte = " - ".$regperi->pernomb;
}
//areas del grado que inciden en perdida de a�o
$sql="SELECT sum(intensidad) as suma, id_area, area, perdida FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' AND notas = '1' GROUP BY id_area ORDER BY suma desc, area";
$datarea = viewsql($sql);
//alumnos
$titulo = "PERDIDA DE AREAS A�O ".$ano;
$subtitulo = " GRADO ".$grado;
$cursos = "y".$ano."cursos";
$sql="SELECT $cursos.id_alumno, $cursos.curso,  $cursos.id_areadeb, $cursos.id_areaper, $cursos.pierde, alumnos.apellidos, alumnos.nombres, alumnos.tipoedu FROM $cursos, alumnos WHERE $cursos.grado = '$grado' AND $cursos.fchretiro LIKE '0000%' ";
if ($curso != "")
{
	$sql.="AND $cursos.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
$subtitulo = $subtitulo.$titcorte;
$sql.="AND $cursos.id_alumno = alumnos.id_alumno ORDER BY alumnos.apellidos, alumnos.nombres";
$datalum=viewsql($sql);
$notasfa = "y".$ano."notasfa";
$notas = "y".$ano."notas";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("PERDIDA");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
$nalu = 0;
$nper = 0;
$pdf->SetFont('Arial','',7);
foreach($datalum as $regalum)
{
	$id_alumno = $regalum->id_alumno;
	$alumno = $regalum->apellidos." ".$regalum->nombres;
	$curso = $regalum->curso;
	$tipoedu = $regalum->tipoedu;
	$pierde = $regalum->pierde; //pierde a�o
        $id_areadeb = $regalum->id_areadeb; //area debe a�o anterior
        $areadeb = "";
        if ($id_areadeb > 0) {
            $sql = "select area from asignaturas where id_area = '$id_areadeb' LIMIT 1";
            $datdeb = viewsql($sql);
            foreach($datdeb as $regdeb)
            {$areadeb = $regdeb->area;}
        }
        
	//puntajes seg�n tipo educaci�n
	$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
	$datpunt = viewsql($sql);
	//areas del grado que inciden en perdida de a�o
	$nreg = 0;
	$nare = 0;
        $aper = 0;
	foreach($datarea as $regarea)
	{
		$id_area = $regarea->id_area;
		$nota = notas_area($ano,$id_alumno,$id_area,$corte,$datperi);
		$perdida = 0; //perdida de area
		if (round($nota[9],1) < $notalim)
		{
			if ($regarea->perdida > 0) //si area incide en perdida de a�o
				$nare = $nare + 1;
			$perdida = 1;
                        if (trim($regarea->area)==trim($areadeb)) //si pierde misma area dos a�os
                            $aper = 1;
		}
		if ($perdida===1)
		{
			$nreg = $nreg + 1;
			if ($nreg===1)
			{
				$nalu = $nalu+1;
				$pdf->SetFont('Arial','B',7);
				$pdf->Cell(60,6,substr($alumno,0,35),1,0,'L',1);
				$pdf->Cell(8,6,$curso,1,0,'L',1);
				$pdf->Cell(56,6,"FIRMA:",1,0,'L',1);
				foreach($datperi as $regperi)
				{$pdf->Cell(8,6,$regperi->perplan,1,0,'C',1);}
				$alupos = $pdf->GetY();
				$pdf->Cell(12,6,"DEF",1,0,'C',1);
				$pdf->Cell(0,6,"",1,1,'C',1);
			}
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(5,5,'','TBL');
			$pdf->Cell(119,5,$regarea->area,'TBR',0,'L');
			foreach($datperi as $regperi)
			{
				$pdf->Cell(8,5,$nota[$regperi->notanum],1,0,'C');
			}			
			$notaQ = cualitativa($datpunt,$nota[9]);
			$pdf->Cell(12,5,$nota[9]." ".$notaQ,1,0,'C');
			$pdf->Cell(0,5,"",1,1);
			$oldpos = $pdf->GetY();
		}
	} //for areas
	//si alumno perdi� areas o alguna area perdida es la misma del a�o anterior
	if ($nare > 0 and ($nare >= $areasper or $aper > 0))
	{
		$nper = $nper+1;
		$pdf->SetY($alupos);
		$pdf->SetFont('Arial','B',7);
		$pdf->Cell(0,6,"P",0,1,'R');
		$pdf->SetY($oldpos);
	}
} //for alumnos	
$pdf->SetFont('Arial','B',7);
$pdf->Cell(0,6," TOTAL ALUMNOS QUE PIERDEN AREAS: ".$nalu." - PIERDEN A�O: ".$nper,1,1,'L',1);
$pdf->Output();
disconnect();
?>
