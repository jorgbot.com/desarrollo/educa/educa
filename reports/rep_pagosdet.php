<?php
/***********************************************************************************************
* @file        : rep_pagosdet.php (ancho hoja 196)                                             *
* @brief       : reporte conciliacion de pagos detallados por rango de fechas                  *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-sep-2015                                                    *
* @Modificado  : Nora Rodriguez 23-sep-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchtip = isset($_GET['fchtip']) ? $_GET['fchtip'] : "ENTRADAS" ;
$fchini = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$fchfin = isset($_GET['fchfin']) ? $_GET['fchfin'] : date('d-m-Y') ;
$id_entrada = isset($_GET['id_entrada']) ? $_GET['id_entrada'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$exportar = isset($_GET['exportar']) ? $_GET['exportar'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$fchini = fec_amd($fchini);
$fchfin = fec_amd($fchfin);
connect();

$titulo = "A�O ".$ano." PAGOS PENSIONES CAUSACION MENSUAL";
if ($tipo=="T")
	$titulo = "A�O ".$ano."  PAGOS TRANSPORTES CAUSACION MENSUAL";
if ($tipo=="M")
	$titulo = "A�O ".$ano."  PAGOS MATRICULAS";
if ($id_entrada > 0){
	$filepag = "y".$ano."pagos";
    $sql="SELECT $filepag.id_entrada, $filepag.fchentrada FROM $filepag WHERE $filepag.id_entrada = '$id_entrada'";
	$ent=viewsql($sql);
	foreach($ent as $fchent){
		$subtitulo = "HOJA DE ENTRADA N� ".$id_entrada." - ".fecha_texto($fchent->fchentrada);
	}
}
else
	$subtitulo = $fchtip." DEL ".fecha_texto($fchini)." AL ".fecha_texto($fchfin);
//Pagos detallados
$filepag = "y".$ano."pagos";
$filedet = "y".$ano."pagosdet";
$sql="SELECT $filepag.id_pago, $filepag.id_banco, $filepag.id_entrada, $filepag.fchentrada, $filepag.tipo, $filepag.grado, $filepag.curso, $filepag.fchpago, $filepag.valor_con, $filepag.valor_efe,".
	"$filedet.mes, $filedet.valor, $filedet.interes, $filedet.interesliq, matriculas.saldofin, alumnos.apellidos, alumnos.nombres, alumnos.id_alumno FROM $filepag, $filedet, alumnos, matriculas ";
if ($id_entrada > 0)
{
	$sql.="WHERE $filepag.id_entrada = '$id_entrada' ";
}
else
{
	if ($fchtip=="ENTRADAS")
		$sql.="WHERE date_format($filepag.fchentrada,'%Y-%m-%d') BETWEEN '$fchini' AND '$fchfin' ";
	else
		$sql.="WHERE date_format($filepag.fchpago,'%Y-%m-%d') BETWEEN '$fchini' AND '$fchfin' ";
}
if ($tipo!="")
{
	if ($tipo=="T")
		$sql.="AND $filepag.tipo = 'T' ";
	else
		$sql.="AND $filepag.tipo = 'P' ";
}
if ($grado != "")
	$sql.="AND $filepag.grado = '$grado' ";
if ($curso != "")
	$sql.="AND $filepag.curso = '$curso' ";
if ($tipo=="M")
	$sql.="AND $filedet.mes = '1' ";
$sql.="AND $filepag.id_pago = $filedet.id_pago AND $filepag.id_alumno = alumnos.id_alumno AND $filepag.id_alumno = matriculas.id_alumno AND matriculas.id_alumno = alumnos.id_alumno AND matriculas.ano='$ano' ORDER BY $filepag.id_entrada, alumnos.apellidos, alumnos.nombres, $filedet.id_pago, $filedet.mes";
$datpag=viewsql($sql);

	

if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$tipo = $GLOBALS["tipo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(7,5,"Hoja",1,0,'C',1);
			$this->Cell(8,5,"IDAlu",1,0,'C',1);
			$this->Cell(41,5,"Alumno",1,0,'C',1);
			$this->Cell(12,5,"Curso",1,0,'C',1);
			$this->Cell(10,5,"IDPago",1,0,'C',1);
			$this->Cell(17,5,"FchConsigna",1,0,'C',1);
			$this->Cell(14,5,"Consignado",1,0,'C',1);
			$this->Cell(14,5,"Efectivo",1,0,'C',1);
			$this->Cell(14,5,"Total",1,0,'C',1);
			$this->Cell(8,5,"Mes",1,0,'C',1);
			$this->Cell(14,5,"Neto",1,0,'C',1);
			$this->Cell(14,5,"Interes",1,0,'C',1);
			$this->Cell(14,5,"TotalMes",1,1,'C',1);
			$this->SetFont('Arial','',7);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("SALDOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$num = 0;
	$totcon = 0;
	$totefe = 0;
	$totnet = 0;
	$totint = 0;
	$totval = 0;
	$totmes = 0;
	$netocon = 0;
	$netoefe = 0;
	$intecon = 0;
	$inteefe = 0;
	$id_pago = 0;
	$saldof = 0;
	
	foreach($datpag as $regpag){
	
		if ($regpag->id_pago != $id_pago)
		{
			$num = $num + 1;
			$id_pago = $regpag->id_pago;
			$id_banco = $regpag->id_banco;
			$pdf->Cell(7,5,$regpag->id_entrada,'LR',0,'L');
			$pdf->Cell(8,5,$regpag->id_alumno,'LR',0,'R');
			$pdf->Cell(41,5,substr($regpag->apellidos." ".$regpag->nombres,0,25),'LR',0,'L');
			$pdf->Cell(4,5,$regpag->grado,'LR',0,'L');
			$pdf->Cell(8,5,$regpag->curso,'LR',0,'L');
			$pdf->Cell(10,5,$regpag->id_pago,'LR',0,'L');
			$pdf->Cell(17,5,fecha_texto($regpag->fchpago),'LR',0,'L');
			$pdf->Cell(14,5,number_format($regpag->valor_con,0),'LR',0,'R');
			$pdf->Cell(14,5,number_format($regpag->valor_efe,0),'LR',0,'R');
			$pdf->Cell(14,5,number_format(intval($regpag->valor_con)+intval($regpag->valor_efe),0),'LR',0,'R');
			$totcon = $totcon + intval($regpag->valor_con);
			$totefe = $totefe + intval($regpag->valor_efe);
			$totval = $totval + intval($regpag->valor_con) + intval($regpag->valor_efe);
		}
		else
		{
			$pdf->Cell(7,5,"",'LR',0,'L');
			$pdf->Cell(8,5,"",'LR',0,'L');
			$pdf->Cell(41,3,"",'LR',0,'L');
			$pdf->Cell(4,5,"",'LR',0,'L');
			$pdf->Cell(8,5,"",'LR',0,'L');
			$pdf->Cell(10,5,"",'LR',0,'L');
			$pdf->Cell(17,5,"",'LR',0,'L');
			$pdf->Cell(14,5,"",'LR',0,'R');
			$pdf->Cell(14,5,"",'LR',0,'R');
			$pdf->Cell(14,5,"",'LR',0,'R');
		}
		$nommes = substr(strtoupper(nombre_mes($regpag->mes)),0,3);
		if (intval($regpag->mes)==1)
			$nommes = "MAT";
		$pdf->Cell(8,5,$nommes,'LR',0,'L');
		$pdf->Cell(14,5,number_format($regpag->valor,0),'LR',0,'R');
		$pdf->Cell(14,5,number_format($regpag->interes,0),'LR',0,'R');
		$pdf->Cell(14,5,number_format(intval($regpag->valor)+intval($regpag->interes),0),'LR',1,'R');
		$totnet = $totnet + intval($regpag->valor);
		$totint = $totint + intval($regpag->interes);
		$totmes = $totmes + intval($regpag->valor) + intval($regpag->interes);
		if ($id_banco <= 0)
		{
			$netoefe = $netoefe + $regpag->valor;
			$inteefe = $inteefe + $regpag->interes;
		}
		else
		{
			$netocon = $netocon + $regpag->valor;
			$intecon = $intecon + $regpag->interes;
		}
			
		if (intval($regpag->saldofin)!= 0 and intval($regpag->mes) >= 11) {
			
		  	
			$id_pago = $regpag->id_pago;
			$id_banco = $regpag->id_banco;
			$pdf->Cell(7,5," ",'LR',0,'L');
			$pdf->Cell(8,5," ",'LR',0,'L');
			$pdf->Cell(41,5," ",'LR',0,'L');
			$pdf->Cell(4,5," ",'LR',0,'L');
			$pdf->Cell(8,5," ",'LR',0,'L');
			$pdf->Cell(10,5," ",'LR',0,'L');
			$pdf->Cell(17,5," ",'LR',0,'L');
			$pdf->Cell(14,5," ",'LR',0,'R');
			$pdf->Cell(14,5," ",'LR',0,'R');
			$pdf->Cell(14,5," ",'LR',0,'R');
			$pdf->Cell(8,5,"SAL",'LR',0,'L');
                        $pdf->Cell(14,5,number_format(intval($regpag->saldofin)),'LR',0,'R');
                        $pdf->Cell(14,5," ",'LR',0,'R');
                        $pdf->Cell(14,5,number_format(intval($regpag->saldofin)),'LR',1,'R');
			$saldof = $saldof + (intval($regpag->saldofin));
			 }
		}
	

	$pdf->SetFont('Arial','B',6);
	if ($tipo=="T")
	{
		$pdf->Cell(104,5,"TOTAL PAGOS: ".$num,1,0,'L',1);
		$pdf->Cell(14,5,number_format($totcon,0),1,0,'R',1);
		$pdf->Cell(14,5,number_format($totefe,0),1,0,'R',1);
		$pdf->Cell(14,5,number_format($totval,0),1,0,'R',1);
		$pdf->Cell(8,5,"",1,0,'R',1);
		$pdf->Cell(14,5,number_format($totnet,0),1,0,'R',1);
		$pdf->Cell(14,5,number_format($totint,0),1,0,'R',1);
		$pdf->Cell(14,5,number_format($totmes,0),1,1,'R',1);
		if ($totmes < $totval)
		{
			$totadi = $totval - $totmes ;
			$pdf->Cell(182,5,"REMANENTE: ",1,0,'R',1);
			$pdf->Cell(14,5,number_format($totadi,0),1,0,'R',1);
		}
	}
	else
	{
		$totadi = $totval - $totmes ;
		$pdf->Cell(95,5,"TOTAL PAGOS: ".$num,1,0,'L',1);
		$pdf->Cell(14,5,number_format($totcon,0),1,0,'R',1);
		$pdf->Cell(14,5,number_format($totefe,0),1,0,'R',1);
		$pdf->Cell(14,5,number_format($totval,0),1,0,'R',1);
		$pdf->Cell(8,5,"",1,0,'R',1);
		$pdf->Cell(14,5,number_format($totnet,0),1,0,'R',1);
		$pdf->Cell(14,5,number_format($totint,0),1,0,'R',1);
		$pdf->Cell(14,5,number_format($totmes,0),1,1,'R',1);
		$pdf->Cell(145,4,"CONSIGNADO:",'LR',0,'R',1);
		$pdf->Cell(14,4,number_format($netocon,0),'LR',0,'R',1);
		$pdf->Cell(14,4,number_format($intecon,0),'LR',0,'R',1);
		$pdf->Cell(14,4,"",'LR',1,'R',1);
		$pdf->Cell(54,4,"* REMANENTE",'LB',0,'L',1);
		$pdf->Cell(91,4,"EFECTIVO:",'RB',0,'R',1);
		$pdf->Cell(14,4,number_format($netoefe,0),'LRB',0,'R',1);
		$pdf->Cell(14,4,number_format($inteefe,0),'LRB',0,'R',1);
		$pdf->Cell(14,4,"* ".number_format($totadi,0),'LRB',0,'R',1);
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
