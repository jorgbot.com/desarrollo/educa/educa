<?php
/***********************************************************************************************
* @file        : est_perdida.php (ancho hoja 196)                                              *
* @brief       : estadisticas alumnos que pierden a�o                                          *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 14-may-2012                                                    *
* @Modificado  : Nora Rodriguez 14-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$nivel = isset($_GET['nivel']) ? $_GET['nivel'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$antano = $ano - 1;
$titulo = "ESTADISTICAS PERDIDA DE A�O";
$subtitulo = "A�O LECTIVO ".$ano;
connect();
//niveles
$sql= "select nivel, nombre from niveles order by id_nivel";
$datnive=viewsql($sql);
//alumnos no retirados
$cursos = "y".$ano."cursos";
$sql="SELECT $cursos.grado, $cursos.curso, $cursos.id_alumno, $cursos.pierde, grados.nivel, grados.gradonom, alumnos.genero FROM $cursos, grados, alumnos WHERE $cursos.grado = grados.grado AND $cursos.fchretiro like '0000%' AND $cursos.id_alumno = alumnos.id_alumno ";
$condi = "";
if ($nivel != "")
{
	$subtitulo.=" - NIVEL ".$nivel;
	$nomnivel = substr($nivel,1);
	$nivel = substr($nivel,0,1);
	$condi.="AND $cursos.nivel = '$nivel' ";
	$subtitulo.=" - NIVEL ".$nomnivel;
}
if ($grado != "")
{
	$condi.="AND $cursos.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
$sql = $sql.$condi."ORDER BY grados.id_grado, $cursos.curso";
$datalum=viewsql($sql);
//echo "$sql<br>";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(2);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',10);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(4);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("ALUMNOS");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
//titulos
$pdf->SetFont('Arial','B',8);
$pdf->Cell(25,6,'Grado',1,0,'C',1);
$pdf->Cell(15,6,'Alumnos',1,0,'C',1);
$pdf->Cell(15,3,'Ni�as','LRT',0,'C',1);
$pdf->Cell(15,3,'Ni�os','LRT',0,'C',1);
$pdf->Cell(15,3,'Ni�as','LRT',0,'C',1);
$pdf->Cell(15,3,'Ni�os','LRT',1,'C',1);
$pdf->Cell(40,3,'',0,0);
$pdf->Cell(15,3,'Ganaron','LRB',0,'C',1);
$pdf->Cell(15,3,'Ganaron','LRB',0,'C',1);
$pdf->Cell(15,3,'Perdieron','LRB',0,'C',1);
$pdf->Cell(15,3,'Perdieron','LRB',1,'C',1);
//lista de Alumnos
$totnum = 0;
$totmasg = 0;
$totfemg = 0;
$totmasp = 0;
$totfemp = 0;
$gradonom = "";
$femp = 0;
$masp = 0;
$pdf->SetFont('Arial','',8);
foreach($datalum as $regalum)
{
	if ($regalum->gradonom != $gradonom)
	{
		if ($gradonom != "" and ($femp+$masp > 0))
		{
			$pdf->Cell(25,7,$gradonom,1,0,'L');
			$pdf->Cell(15,7,$num,1,0,'C');
			$pdf->Cell(15,7,$femg,1,0,'C');
			$pdf->Cell(15,7,$masg,1,0,'C');
			$pdf->Cell(15,7,$femp,1,0,'C');
			$pdf->Cell(15,7,$masp,1,1,'C');
		}
		$gradonom = $regalum->gradonom;
		$num = 0;
		$femg = 0;
		$masg = 0;
		$masp = 0;
		$femp = 0;
	}
	$num = $num +1;
	$totnum = $totnum +1;
	if ($regalum->pierde == 0)
	{
		if ($regalum->genero == "M")
		{
			$masg = $masg +1;
			$totmasg = $totmasg +1;
		}
		else
		{
			$femg = $femg +1;
			$totfemg = $totfemg +1;
		}
	}
	else
	{
		if ($regalum->genero == "M")
		{
			$masp = $masp +1;
			$totmasp = $totmasp +1;
		}
		else
		{
			$femp = $femp +1;
			$totfemp = $totfemp +1;
		}
	}
}
if ($femp+$masp > 0)
{
	$pdf->Cell(25,7,$gradonom,1,0,'L');
	$pdf->Cell(15,7,$num,1,0,'C');
	$pdf->Cell(15,7,$femg,1,0,'C');
	$pdf->Cell(15,7,$masg,1,0,'C');
	$pdf->Cell(15,7,$femp,1,0,'C');
	$pdf->Cell(15,7,$masp,1,1,'C');
}
//totales
$pdf->SetFont('Arial','B',8);
$pdf->Cell(25,5,'TOTAL',1,0,'C',1);
$pdf->Cell(15,5,number_format($totnum,0),1,0,'C',1);
$pdf->Cell(15,5,number_format($totfemg,0),1,0,'C',1);
$pdf->Cell(15,5,number_format($totmasg,0),1,0,'C',1);
$pdf->Cell(15,5,number_format($totfemp,0),1,0,'C',1);
$pdf->Cell(15,5,number_format($totmasp,0),1,1,'C',1);
$pdf->Output();
disconnect();
?>
