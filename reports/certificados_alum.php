<?php
/***********************************************************************************************
* @file        : certificados_alum.php                                                         *
* @brief       : Imprime certificado de alumnos seg�n plantilla de texto en tabla              *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 13-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../database/database.php");
include("../database/datos_setup.php");
include("../libs/fechas.php");
$id_plantilla = $_GET["id_plantilla"];
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$consecutivo = isset($_GET['consecutivo']) ? $_GET['consecutivo'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$imprimir = isset($_GET['imprimir']) ? $_GET['imprimir'] : null ;
$fecha = isset($_GET['fecha']) ? $_GET['fecha'] : null ;
if ($fecha=="")
	$fecha = date('Y-m-d');
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
connect();
//traer plantilla
$datplantilla = datos_plantilla($id_plantilla);
$texto = $datplantilla["texto"];
$fecha = fecha_carta($fecha);
//traer alumno
$sql="SELECT apellidos, nombres, docunum, docutip, docuexp, tipoedu FROM alumnos WHERE id_alumno = '$id_alumno' limit 1";
$datalum=viewsql($sql);
foreach($datalum as $regalum)
{
	$apellidos = $regalum->apellidos;
	$nombres = $regalum->nombres;
	$docutip = $regalum->docutip;
	$docunum = $regalum->docunum;
	$docuexp = $regalum->docuexp;
	$tipoedu = $regalum->tipoedu;
}
$sql="SELECT matriculas.curso, grados.gradonom, grados.educacion FROM matriculas, grados WHERE matriculas.id_alumno = '$id_alumno' and matriculas.ano = '$ano' and matriculas.grado=grados.grado limit 1";
$datmatr = viewsql($sql);
foreach($datmatr as $regmatr)
{
	$grado = $regmatr->gradonom;
	$curso = $regmatr->curso;
	$educacion = $regmatr->educacion;
}
$gradoanterior = "";
$sfind = "matricula_anterior";
$pos = strpos($texto,$sfind);
if ($pos > 0)
{
	$anoant = $ano - 1;
	$sql="SELECT matriculas.curso, grados.gradonom FROM matriculas, grados WHERE matriculas.id_alumno = '$id_alumno' and matriculas.ano = '$anoant' and matriculas.grado=grados.grado limit 1";
	$datmatr = viewsql($sql);
	foreach($datmatr as $regmatr)
	{
		$gradoanterior = $regmatr->gradonom." durante el a�o ".$anoant;
		$curso = $regmatr->curso;
	}
}
$sql="SELECT lisnom FROM listas WHERE listip = 'IDENTIFICACION' and lisval = '$docutip' LIMIT 1";
$listide=viewsql($sql);
foreach($listide as $regiden)
{$docutip = $regiden->lisnom;}
$texto = str_replace('$consecutivo',$consecutivo,$texto);
$texto = str_replace('$apellidos',$apellidos,$texto);
$texto = str_replace('$nombres',$nombres,$texto);
$texto = str_replace('$docutip',$docutip,$texto);
$texto = str_replace('$docunum',$docunum,$texto);
$texto = str_replace('$docuexp',$docuexp,$texto);
$texto = str_replace('$grado',$grado,$texto);
$texto = str_replace('$educacion',$educacion,$texto);
if ($gradoanterior != "")
	$texto = str_replace('$matricula_anterior',$gradoanterior,$texto);
$texto = str_replace('$fecha',$fecha,$texto);
?>
<html>
<head>
<title>CERTICICADO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script language='JavaScript' type='text/JavaScript'>
<!--
	function imprimir ()
	{ 
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
	}
-->
</script>
</head>
<body style="margin: 0px;">
<table cellspacing="0" cellpadding="0" border="0" width="700" align="left">
<tr valign="top">
	<td colspan="3" height="180">
		<?php if ($implogo=="S") include("../includes/banner_doc.php");?>
	</td>
</tr>
<tr>
	<td width="30"></td>
	<td>
		<table cellspacing="0" cellpadding="0" border="0" width="100%">
			<tr><td height="50"></td></tr>
			<tr>
				<td>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr><td style="font-family: Arial,Helvetica,Verdana; font-size: 15px; text-align: justify;"><?php echo $texto;?></td></tr>
					</table>
				</td>
			</tr>
		</table>
	</td>
	<td width="10"></td>
</tr>
</table>
<?php if ($imprimir!="") { ?>
<script type='text/javascript' language='javascript'>
<!--
// Do print the page
if (typeof(window.print) != 'undefined')
{
	window.print();
}
//-->
</script>
<?php } ?>
</body>
</html>