<?php
/***********************************************************************************************
* @file        : rep_asignaturas.php (ancho hoja 196)                                          *
* @brief       : lista de asignaturas, logros, actividades por grado                           *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 23-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$periodo = isset($_GET['periodo']) ? $_GET['periodo'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
$id_logro = isset($_GET['id_logro']) ? $_GET['id_logro'] : null ;
$implogros = isset($_GET['implogros']) ? $_GET['implogros'] : null ;
$impactiv = isset($_GET['impactiv']) ? $_GET['impactiv'] : null ; 
$impmetod = isset($_GET['impmetod']) ? $_GET['impmetod'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($implogo == "")
	$implogo = "";
if ($papel == "")
	$papel = "Letter";
connect();
$semanas = semanas_academicas($ano);
//porcentaje perdida de fallas
$sql = "select fallasper from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{$fallasper = $reganol->fallasper;}
$titulo = "LISTA DE ASIGNATURAS";
if ($implogros == "S")
	$titulo = "LISTA DE ASIGNATURAS Y LOGROS";
if ($impactiv == "S")
	$titulo = "LISTA DE ASIGNATURAS, LOGROS, EVIDENCIAS Y ACTIVIDADES";
if ($impmetod == "S")
	$titulo = "LISTA DE ASIGNATURAS, LOGROS Y METODOLOGIAS";
$subtitulo = "A�O ".$ano;
if ($grado != "")
	$subtitulo.=" - GRADO ".$grado;
if ($periodo > 0)
	$subtitulo.=" - PERIODO ".$periodo;
$logros = "y".$ano."logros";
$evidencias = "y".$ano."eviden";
$actividades = "y".$ano."activ";
$metodologias = "y".$ano."metod";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogros = $GLOBALS["implogros"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
		$this->SetFont('Arial','B',6);
		if ($implogros == "S")
			$this->Cell(0,5,'AREA Y ASIGNATURA',1,1,'L',1);
		else
		{
			$this->Cell(10,5,'AREA',1,0,'L',1);
			$this->Cell(115,5,'ASIGNATURA',1,0,'L',1);
			$this->Cell(8,5,'COD',1,0,'C',1);
			$this->Cell(6,5,'I.H',1,0,'C',1);
			$this->Cell(6,5,'H.T',1,0,'C',1);
			$this->Cell(6,5,'H.P',1,0,'C',1);
			$this->Cell(5,5,'%A',1,0,'C',1);
			$this->Cell(10,5,'ACTIV',1,0,'C',1);
			$this->Cell(6,5,'Bole',1,0,'C',1);
			$this->Cell(6,5,'Nota',1,0,'C',1);
			$this->Cell(6,5,'Obse',1,0,'C',1);
			$this->Cell(6,5,'Prom',1,0,'C',1);
			$this->Cell(6,5,'Perd',1,1,'C',1);
		}
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("ASIGNATURAS");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
//grados
$sql = "SELECT grado, gradonom FROM grados order by id_grado";
if ($grado!="") {
    $sql = "SELECT grado, gradonom FROM grados WHERE grado='$grado' order by id_grado";
}
$datgrad = viewsql($sql);
foreach($datgrad as $reggrad)
{
$grado = $reggrad->grado;
$gradonom = $reggrad->gradonom;
$subtitulo = "GRADO ".$gradonom;
$pdf->AddPage('P',$papel);
$pdf->SetFont('Arial','',7);
//asignaturas del grado
$sql = "select * from asignaturas where ano = '$ano' and grado = '$grado' ORDER BY area, asignatura";
if ($id_asignatura > 0)
	$sql = "select * from asignaturas where id_asignatura = '$id_asignatura' LIMIT 1";
$datasig=viewsql($sql);
$totin = 0;
$total = 0;
$id_area = "";
foreach($datasig as $regasig)
{
	if ($implogros == "S")
	{
		if ($regasig->id_area != $id_area)
		{
			$id_area = $regasig->id_area;
			$pdf->SetFont('Arial','BU',7);
			$pdf->Cell(0,5,$regasig->area,0,1,'L');
		}
		$idasignatura = $regasig->id_asignatura;
		$pdf->SetFont('Arial','B',7);
		$pdf->Cell(170,5,$regasig->asignatura,0,0,'L');
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(10,5,"COD ".$regasig->codigo,0,0,'L');
		$pdf->Cell(10,5,"I.H ".$regasig->intensidad,0,0,'C');
		$pdf->Cell(0,5,"%".$regasig->porcentaje,0,1,'C');
		$sql = "SELECT id_logro, logrocod, logro, periodos FROM $logros WHERE grado = '$grado' and id_asignatura = '$idasignatura' ";
		if ($periodo > 0)
			$sql.="AND periodos LIKE '% $periodo %' ";
		$sql.="ORDER BY logrocod";
		if ($id_logro > 0)
			$sql = "select id_logro, logrocod, logro, periodos FROM $logros WHERE id_asignatura = '$idasignatura' and id_logro = '$id_logro' LIMIT 1";
		$datlogr = viewsql($sql);
		foreach($datlogr as $reglogr)
		{
			$idlogro = $reglogr->id_logro;
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(5,3,"",0,0);
			$pdf->Cell(8,3,"L".$reglogr->logrocod,0,0);
			$pdf->Cell(12,3,"P".$reglogr->periodos,0,0);
			$pdf->MultiCell(170,3,$reglogr->logro,0,'J');
			if ($impactiv == "S")
			{
				$sql = "SELECT id_evidencia, evidencia, porcentaje, numero FROM $evidencias WHERE id_asignatura = '$idasignatura' AND id_logro = '$idlogro' ";
				if ($periodo > 0)
					$sql.="AND periodo = '$periodo' ";
				$sql.="ORDER BY numero";
				$datevid = viewsql($sql);
				foreach($datevid as $regevid)
				{
                                    $id_evidencia = $regevid->id_evidencia;
                                    $pdf->Cell(20,3,"",0,0);
                                    $pdf->Cell(8,3,"E".$regevid->numero,0,0);
                                    $pdf->Cell(8,3,"%".$regevid->porcentaje,0,0);
                                    $pdf->MultiCell(145,3,$regevid->evidencia,0,'J');
                                    $sql = "SELECT id_actividad, actividad, porcentaje, numero FROM $actividades WHERE id_asignatura = '$idasignatura' AND id_logro = '$idlogro' AND id_evidencia = '$id_evidencia' ";
                                    if ($periodo > 0)
                                            $sql.="AND periodo = '$periodo' ";
                                    $sql.="ORDER BY numero";
                                    $datacti = viewsql($sql);
                                    foreach($datacti as $regacti)
                                    {
                                            $pdf->Cell(30,3,"",0,0);
                                            $pdf->Cell(8,3,"A".$regacti->numero,0,0);
                                            if ($regacti->porcentaje > 0)
                                                    $pdf->Cell(8,3,"%".$regacti->porcentaje,0,0);
                                            $pdf->MultiCell(145,3,$regacti->actividad,0,'J');
                                    }
                                }
			}
			if ($impmetod == "S")
			{
				$sql = "SELECT * FROM $metodologias WHERE id_logro = '$idlogro' ORDER BY fecha";
				$datmeto = viewsql($sql);
				foreach($datmeto as $regmeto)
				{
					$pdf->Cell(25,3,"",0,0);
					$pdf->Cell(10,3,substr(fecha_texto($regmeto->fecha),0,6),0,0);
					$pdf->Cell(6,3,"CL".$regmeto->numclases,0,0);
					$inipos = $pdf->GetY(); 
					$pdf->MultiCell(100,3,$regmeto->metodologia,0,'J');
					$finpos = $pdf->GetY();
					$pdf->SetY($inipos);
					$pdf->Cell(151,3,"",0,0);
					$pdf->MultiCell(45,3,$regmeto->recursos,0,'J');
					$pdf->SetY($finpos);
				}
			}
		} //fin logros
	}
	else
	{
		if ($regasig->id_area != $id_area)
		{
			$id_area = $regasig->id_area;
			$pdf->SetFont('Arial','B',6);
			$pdf->Cell(125,5,$regasig->area,1,0,'L');
			$pdf->Cell(8,5,"",1,0,'L');
			$pdf->Cell(6,5,"",1,0,'C');
			$pdf->Cell(6,5,"",1,0,'C');
			$pdf->Cell(6,5,"",1,0,'C');
			$pdf->Cell(5,5,"",1,0,'C');
			$pdf->Cell(10,5,"",1,0,'C');
			$pdf->Cell(6,5,"",1,0,'C');
			$pdf->Cell(6,5,"",1,0,'C');
			$pdf->Cell(6,5,"",1,0,'C');
			$pdf->Cell(6,5,"",1,0,'C');
			$pdf->Cell(6,5,"",1,1,'C');
		}
		$idasignatura = $regasig->id_asignatura;
		$totin = $totin + $regasig->intensidad;
		$horas = $regasig->intensidad * $semanas;
		$total = $total + $horas;
		$porce = round($horas*($fallasper/100),0);
		$prom = "%";
		if ($regasig->activprom > 0)
			$prom = "P";
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(10,5,"","LTB",0);
		$pdf->Cell(115,5,$regasig->asignatura,"RTB",0,'L');
		$pdf->Cell(8,5,$regasig->codigo,1,0,'L');
		$pdf->Cell(6,5,$regasig->intensidad,1,0,'C');
		$pdf->Cell(6,5,$horas,1,0,'C');
		$pdf->Cell(6,5,$porce,1,0,'C');
		$pdf->Cell(5,5,$regasig->porcentaje,1,0,'C');
		$pdf->Cell(10,5,$prom." ".$regasig->activmin,1,0,'C');
		$pdf->Cell(6,5,$regasig->boletin,1,0,'C');
		$pdf->Cell(6,5,$regasig->notas,1,0,'C');
		$pdf->Cell(6,5,$regasig->observacion,1,0,'C');
		$pdf->Cell(6,5,$regasig->promedio,1,0,'C');
		$pdf->Cell(6,5,$regasig->perdida,1,1,'C');
	}
} //fin asignaturas
} //fin grados
$pdf->SetFont('Arial','B',6);
$pdf->Cell(133,5," TOTAL ASIGNATURAS: ".count($datasig),1,0,'L',1);
$pdf->Cell(6,5,$totin,1,0,'L',1);
$pdf->Cell(0,5,$total,1,1,'L',1);
$pdf->Output();
disconnect();
?>
