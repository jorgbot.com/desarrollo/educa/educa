<?php
/***********************************************************************************************
* @file        : rep_planilla.php (ancho hoja 193)                                             *
* @brief       : Genera reporte de planilla en PDF                                             *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 15-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 20-ene-2018                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$id_asignatura = $_GET["id_asignatura"];
$id_profasig = $_GET["id_profasig"];
$pertipo = $_GET["pertipo"];
$periodo = $_GET["periodo"];
$tipoedu = $_GET["tipoedu"];
$implogo = $_GET["implogo"];
$impnotas = $_GET["impnotas"];
$papel = $_GET["papel"];
$implogros = "N";
$exportar = "";
$general = "N";
if ($impnotas != "S")
	$general = "";
connect();
include("../database/datos_planilla.php");
$numcols = $numlogr + $numacti;
$pages = 1;
if ($numcols > 16)
{
	$pages = intval($numcols / 16); //16 columnas de notas por hoja de planilla
	$resto = ($numcols % 16);
	if ($resto > 0)
		$pages = $pages + 1;
}
$logpage = array(); //nro de logros por pagina
for($i=0; $i<=5 ;$i++)
{
	$logpage[$i] = 0;
}
//contar nro de logros por pagina
$ntot = 0;
if ($pertipo=="P")
{
	$implogros = "S";
	foreach($datlogr as $reglogr)
	{
		$nact = 1;
		$id_logro = $reglogr->id_logro;
		foreach($datacti as $regacti)
		{
			if ($regacti->id_logro == $id_logro)
				$nact = $nact + 1;
		}
		$ntot = $ntot + $nact;
		if ($ntot <= 16)
			$logpage[1] = $logpage[1] + 1; 	
		if ($ntot > 16 and $ntot <= 32)
			$logpage[2] = $logpage[2] + 1; 	
		if ($ntot > 32 and $ntot <= 48)
			$logpage[3] = $logpage[3] + 1; 	
		if ($ntot > 48 and $ntot <= 64)
			$logpage[4] = $logpage[4] + 1; 	
		if ($ntot > 64 and $ntot <= 80)
			$logpage[4] = $logpage[4] + 1; 	
	}
}
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$asignatura = $GLOBALS["asignatura"] ;
			$profesor = $GLOBALS["profesor"] ;
			$implogo = $GLOBALS["implogo"] ;
			$datpunt = $GLOBALS["datpunt"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(16);
				$this->Cell(100,6,$titulo,0,1);
			}
			//tabla de evaluacion		
			$this->SetY(19);
			$this->SetDrawColor(0,128,0);
			$this->SetFillColor(0,128,0);
			$this->SetTextColor(255,255,255);
			$this->SetFont('Arial','B',7);
			$this->Cell(146);
			$this->Cell(50,5,"Evaluaci�n",1,1,'C',1);
			$this->SetFillColor(224,231,233);
			$this->SetTextColor(0,0,0);
			foreach($datpunt as $regpunt)
			{
				$this->Cell(146);
				$this->Cell(8,5,$regpunt->notaQ,1,0);
				$this->Cell(30,5,$regpunt->notadet,1,0);
				$this->Cell(0,5,$regpunt->notamin." - ".$regpunt->notamax,1,1);
			}
			$this->SetDrawColor(128,128,128);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',9);
			$this->SetY(32);
			$this->Cell(25,6,"ASIGNATURA",1,0);
			$this->Cell(118,6,$asignatura,1,1);
			$this->Cell(25,6,"DOCENTE",1,0);
			$this->Cell(118,6,$profesor,1,1);
			$this->SetTextColor(0,0,0);
			$this->SetFont('Arial','',7);
			$this->Ln(4);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("PLANILLA");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$titulo = "PLANILLA ".$ano." - ".$pernomb." CURSO ".$curso;
	$asignatura = $asignatura." - IH ".$intensidad;
	for($i=1; $i <= $pages ; $i++)
	{
		$pdf->AddPage('P',$papel);
		//si imprimir logros y actividades
		if ($implogros == "S")
		{
			$nlog = 0;
			foreach($datlogr as $reglogr)
			{
				$nlog = $nlog + 1;
				if ($nlog <= $logpage[$i])
				{
					$pdf->MultiCell(0,4,"L".$reglogr->logrocod.". ".$reglogr->logro,"LTR",'J');
					$id_logro = $reglogr->id_logro;
                                        $nevi = 0;
					foreach($datevid as $regevid)
					{
						if ($regevid->id_logro == $id_logro)
						{
                                                        $nevi = $nevi + 1;
							$pdf->Cell(5,4,"","L");
							$pdf->MultiCell(0,4,"E".$nevi." ".$regevid->codigo.". ".$regevid->evidencia,"R",'J');
                                                        $id_evidencia = $regevid->id_evidencia;
                                                        foreach($datacti as $regacti)
                                                        {
                                                                if ($regacti->id_evidencia == $id_evidencia)
                                                                {
                                                                        $pdf->Cell(10,4,"","L");
                                                                        $pdf->MultiCell(0,4,"ACT".$regacti->codigo.". ".$regacti->actividad,"R",'J');
                                                                }
                                                        }
                                                }
					}
				}
			}
			$pdf->Cell(0,0,'',1,1);
			$pdf->Ln(4);
		}
		//titulos columnas
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Arial','B',6);
		$pdf->Cell(5,6,'N�',1,0,'C',1);
		$pdf->Cell(44,6,'APELLIDOS Y NOMBRES',1,0,'C',1);
		//titulos actividades
		$nlog = 0;
		if ($implogros == "S")
		{
			foreach($datlogr as $reglogr)
			{
				$nlog = $nlog + 1;
				if ($nlog <= $logpage[$i])
				{
					$colsact = 0;
                                        $colsevi = 0;
					$id_logro = $reglogr->id_logro;
					foreach($datevid as $regevid)
					{
						if ($regevid->id_logro == $id_logro)
						{
                                                        $id_evidencia = $regevid->id_evidencia;
                                                        foreach($datacti as $regacti)
                                                        {
                                                                if ($regacti->id_evidencia == $id_evidencia)
                                                                {
                                                                        $pdf->Cell(6,6,"A".$regacti->numero,1,0,'C',1);
                                                                }
                                                        }
                                                        $pdf->Cell(6,6,"E".$regevid->numero,1,0,'C',1);
						}
					}

					$pdf->Cell(6,6,"L ".$nlog,1,0,'C',1);
				}
			}
		}
		$wobs = 8;
		if ($nlog < 4)
			$wobs = 40;
		$pdf->Cell(11,6,'ASIGN',1,0,'C',1);
		$pdf->Cell($wobs,6,'Observaci�n',1,1,'C',1);
                /*
		$pos=$pdf->GetY();
		$pos=$pos-4;
		$pdf->SetY($pos);
		$pdf->Cell(49,4,'');
                
		$pdf->Cell(11,4,'','BLR',1,'C');
		$pdf->SetTextColor(0,0,0);
                 * 
                 */
		$num = 0;
		foreach($datalum as $regalum)
		{
			$num = $num +1;
			$retiro = $regalum->fchretiro;
			$obsv = " ";
			if ($retiro != ('0000-00-00')){
				$obsv = "RETIRADO EL ".$retiro;
			}
			else {
				$obsv = " ";
			}
			$id_alumno = $regalum->id_alumno;
			$pdf->SetFont('Arial','',6);
			$pdf->Cell(5,5,$num,1,'R');
			$pdf->Cell(44,5,substr($regalum->apellidos." ".$regalum->nombres,0,32),1,0,'L');
			$nlog = 0;
			if ($implogros == "S")
			{
				foreach($datlogr as $reglogr)
				{    
					$nlog = $nlog + 1;
					if ($nlog <= $logpage[$i])
					{
						$colsact = 0;
						$id_logro = $reglogr->id_logro;
						foreach($datevid as $regevid)
						{
							if ($regevid->id_logro == $id_logro)
							{
                                                            $id_evidencia = $regevid->id_evidencia;
                                                            foreach($datacti as $regacti)
                                                            {
                                                                    if ($regacti->id_evidencia == $id_evidencia)
                                                                    {
                                                                            $id_actividad = $regacti->id_actividad;
                                                                            $notaact = "";
                                                                            if ($impnotas == "S")
                                                                            {
                                                                                    $sql="select nota from $notasdet ".
                                                                                            "where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
                                                                                            "and id_evidencia = '$id_evidencia' and id_actividad = '$id_actividad' limit 1";
                                                                                    $datnota=viewsql($sql);
                                                                                    foreach($datnota as $regnota)
                                                                                    {$notaact = intval($regnota->nota);}
                                                                            }
                                                                            $pdf->Cell(6,5,$notaact,1,0,'C');
                                                                    }
                                                            }
                                                            $notaevi = "";
                                                            if ($impnotas == "S")
                                                            {
                                                                $sql="select nota from $notasdet ".
                                                                "where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
                                                                "and id_evidencia = '$id_evidencia' and id_actividad = '0' limit 1";
                                                                $datnota=viewsql($sql);
                                                                foreach($datnota as $regnota)
                                                                {$notaevi = intval($regnota->nota);}
                                                            } 
                                                            $pdf->SetFillColor(243,247,248);
                                                            $pdf->Cell(6,5,$notaevi,1,0,'C',1);
                                                        }
                                                }
						$notalog = "";
						if ($impnotas == "S")
						{
							$sql="select nota, pendiente, inactivar from $notasdet ".
								"where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and id_logro = '$id_logro' ".
								"and id_evidencia = '0' and id_actividad = '0' limit 1";
							$datnota=viewsql($sql);
							foreach($datnota as $regnota)
							{
								$notalog = $regnota->nota;
								if ($notalog <= 0)
								{
									if ($regnota->pendiente==1)
										$notalog = "PEND"; 
									if ($regnota->inactivar==1)
										$notalog = "INAC"; 
								}
							}
						}
						$pdf->SetFillColor(224,231,233);
						$pdf->Cell(6,5,$notalog,1,0,'C',1);
					}
				}
			}
			$notatot = "";
			if ($impnotas == "S")
			{
				$sql="select nota, pendiente, inactivar from $notas where id_alumno = '$id_alumno' and id_asignatura = '$id_asignatura' and pertipo = '$pertipo' and periodo = '$periodo' LIMIT 1";
				$dattot=viewsql($sql);
				foreach($dattot as $regtot)
				{
					$notatot = $regtot->nota;
					$decimal = ($notatot-intval($notatot))*100;
					if ($decimal <= 0)
						$notatot = intval($notatot);
					if ($notatot < $notalim)
						$pdf->SetTextColor(164,0,0);
					if ($notatot > 0)
					{
						foreach($datpunt as $regpunt)
						{
							if (intval($notatot) >= intval($regpunt->notamin) and intval($notatot) <= intval($regpunt->notamax))
							{
								$notatot = $notatot." ".$regpunt->notaQ;
								break;
							}
						}  
					}
					else
					{
						if ($regtot->pendiente == 1)
							$notatot = "PEND";
						if ($regtot->inactivar == 1)
							$notatot = "INAC";
					}
				}
			}
			$pdf->SetFont('Arial','B',6);
			$pdf->Cell(11,5,$notatot,1,0,'C',1);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell($wobs,5, $obsv,1,1,'C');
		}
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
