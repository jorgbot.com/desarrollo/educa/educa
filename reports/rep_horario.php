<?php
/***********************************************************************************************
* @file        : rep_horario.php (ancho hoja 196)                                              *
* @brief       : Imprime horario por grado y curso o profesor en formato PDF                   *
*                Formatos en tama�o peque�o, normal o grande                                   *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 30-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 30-abr-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$grado = $_GET["grado"];
$curso = $_GET["curso"];
$id_profesor = $_GET["id_profesor"];
$id_director = $_GET["id_director"];
$formato = $_GET["formato"];
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
connect();
//nivel del grado para intervalos
$nivel = "";
$gradonom = $grado;
$sql="SELECT nivel, gradonom FROM grados WHERE grado = '$grado' limit 1";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{$nivel = $reggrad->nivel; $gradonom = $reggrad->gradonom;}
//nombre director
$titulo = "HORARIO ".$ano;
$subtitulo = "";
if ($id_profesor > 0)
{
	$sql="SELECT apellidos, nombres FROM profesores where id_profesor = '$id_profesor' limit 1";
	$datprof=viewsql($sql);
	foreach($datprof as $regprof)
	{$subtitulo = "DOCENTE: ".$regprof->nombres." ".$regprof->apellidos;}
}
else
{
	$titulo.= " - GRADO ".$gradonom." CURSO ".$curso;
	$sql="SELECT apellidos, nombres FROM profesores where id_profesor = '$id_director' limit 1";
	$datprof=viewsql($sql);
	foreach($datprof as $regprof)
	{$subtitulo = "DIRECTOR: ".$regprof->nombres." ".$regprof->apellidos;}
}
//intervalos de horario
$sql="SELECT * FROM horariosint where nivel = '$nivel' order by intervalo";
$datinte=viewsql($sql);
//tama�o defualt normal
$sizetit = 8;
$sizelet = 6;
$colnum = 3;
$colint = 15;
$coldia = 35;
$alto = 5;
$orient = 'P'; //portrait
if ($formato == "P") //peque�o
{
	$sizetit = 5;
	$sizelet = 3;
	$colnum = 2;
	$colint = 10;
	$coldia = 18;
	$alto = 2;
}
if ($formato == "G") //grande
{
	$sizetit = 12;
	$sizelet = 10;
	$colnum = 6;
	$colint = 25;
	$coldia = 58;
	$alto = 6;
	$orient = 'L'; //landscape
}
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$implogo = $GLOBALS["implogo"];
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$sizetit = $GLOBALS["sizetit"] ;
			$alto = $GLOBALS["alto"] ;
			$this->SetFont('Arial','B',$sizetit);
			$this->SetTextColor(0,0,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,$alto,$_SESSION["instinom"],0,1);
				$this->Cell(17,$alto,"");
				$this->Cell(80,$alto,$titulo,0,1);
				$this->Cell(17,$alto,"");
				$this->Cell(80,$alto,$subtitulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(80,$alto,$titulo,0,1);
				$this->Cell(80,$alto,$subtitulo,0,1);
			}
			$this->Ln(2);
		}
		function Footer()
		{
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("HORARIO");
	$pdf->AliasNbPages();
	$pdf->AddPage($orient,$papel);
	//titulos columnas
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(0,128,0); //verde
	$pdf->SetTextColor(255,255,255); //blanco
	$pdf->SetFont('Arial','B',$sizelet);
	$pdf->Cell($colnum,$alto,'N�',1,0,'C',1);
	$pdf->Cell($colint,$alto,'INTERVALO',1,1,'C',1);
	$pdf->SetFillColor(224,231,233); //gris
	$pdf->SetTextColor(0,0,0); //negro
	//columa intervalos
	$nint = 0;
	$posY = $alto;
	foreach($datinte as $reginte)
	{
		if ($reginte->tipo=="")
		{
			$nint = $nint + 1;
			$pdf->Cell($colnum,$alto*2,$nint,1,0,'C',1);
			$pdf->Cell($colint,$alto*2,$reginte->horas,1,1,'C',1);
			$posY = $posY + ($alto*2);
		}
		else
		{
			$pdf->Cell($colnum+$colint,$alto,$reginte->tipo,1,1,'C',1);
			$posY = $posY + $alto;
		}
	}
	$pos = $pdf->GetY();
	$pos = $pos - $posY;
	$pdf->SetY($pos);
	//columnas por dia
	for($i=1; $i<6 ;$i++)
	{
		$dia = $i;
		$dianom = "";
		$sql="SELECT lisnom FROM listas WHERE listip = 'HORARIO' and lisval='$dia' limit 1";
		$lista=viewsql($sql);
		foreach($lista as $reglis)
		{$dianom = $reglis->lisnom;}
		$pdf->SetFillColor(0,128,0); //verde
		$pdf->SetTextColor(255,255,255); //blanco
		$pdf->Cell($colnum+$colint,$alto,"",0,0);
		for($j=1; $j<$dia ;$j++)
		{$pdf->Cell($coldia,$alto,"",0,0);}
		$pdf->Cell($coldia,$alto,$dianom,1,1,'C',1);
		$pdf->SetFillColor(224,231,233); //gris
		$pdf->SetTextColor(0,0,0); //negro
		///horario del dia
		if ($id_profesor > 0)
			$sql="SELECT * FROM horarios where ano='$ano' and dia='$dia' and id_profesor='$id_profesor' order by intervalo";
		else
			$sql="SELECT * FROM horarios where ano='$ano' and dia='$dia' and grado='$grado' and curso='$curso' order by intervalo";
		$dathora=viewsql($sql);
		foreach($datinte as $reginte)
		{
			if ($reginte->tipo=="")
			{
				$intervalo = $reginte->intervalo;
				$asignatura = "";
				$detalle = "";
				$pdf->SetFillColor(255,255,200); //amarillo
				foreach($dathora as $reghora)
				{
					if ($reghora->intervalo == $intervalo)
					{
						$pdf->SetFillColor(255,255,255); //blanco
						$sql="SELECT asignatura FROM asignaturas where id_asignatura = '".$reghora->id_asignatura."' limit 1";
						$datasig=viewsql($sql);
						foreach($datasig as $regasig)
						{$asignatura = $regasig->asignatura;}
						if ($id_profesor > 0)
							$detalle = $reghora->grado."� CURSO ".$reghora->curso;
						else
						{
							$sql="SELECT apellidos, nombres FROM profesores where id_profesor = '".$reghora->id_profesor."' limit 1";
							$datprof=viewsql($sql);
							foreach($datprof as $regprof)
							{$detalle = $regprof->nombres." ".$regprof->apellidos;}
						}
						break;
					}
				}
				//asignatura
				$pdf->Cell($colnum+$colint,$alto,"",0,0);
				for($j=1; $j<$dia ;$j++)
				{$pdf->Cell($coldia,$alto,"",0,0);}
				$pdf->Cell($coldia,$alto,substr($asignatura,0,25),'TLR',1,'C',1);
				//profesor
				$pdf->Cell($colnum+$colint,$alto,"",0,0);
				for($j=1; $j<$dia ;$j++)
				{$pdf->Cell($coldia,$alto,"",0,0);}
				$pdf->Cell($coldia,$alto,substr($detalle,0,25),'BLR',1,'C',1);
			}
			else
			{
				$pdf->SetFillColor(224,231,233); //gris
				$pdf->Cell($colnum+$colint,$alto,$reginte->tipo,1,0,'C',1);
				for($j=1; $j<$dia ;$j++)
				{$pdf->Cell($coldia,$alto,"",1,0,'C',1);}
				$pdf->Cell($coldia,$alto,"",1,1,'C',1);
			}
		}
		$pos = $pdf->GetY();
		$pos = $pos - $posY;
		$pdf->SetY($pos);
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
