<?php
/***********************************************************************************************
* @file        : rep_perdidaasi.php (ancho hoja 196)                                           *
* @brief       : Reporte de asignaturas perdidas por alumno                                    *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 23-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$notanum = $_GET["notanum"];
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
$id_profesor = isset($_GET['id_profesor']) ? $_GET['id_profesor'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
$examen = "N"; //no tener en cuenta examenes
if ($notanum <= 0)
	$notanum = 9;
if ($papel == "")
	$papel = "Letter";
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
if ($id_profesor <= 0 and $_SESSION["usutip"]=="DOCENTE")
	$id_profesor = $_SESSION["usunro"];
connect();
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = $reganol->notalim;
	$areasper = $reganol->areasper;
}
//todos los periodos
$titcorte = "";
$sql="SELECT notanum, pertipo, periodo, perplan, pernomb, estado FROM periodos WHERE ano = '$ano' order by notanum";
$datperi = viewsql($sql);
foreach($datperi as $regperi)
{
	if ($regperi->notanum > $notanum)
		break;
	if ($regperi->estado=="CERRADO")
	{
		$titcorte = " - ".$regperi->pernomb;
		$periodo = $regperi->periodo;
		$pertipo = $regperi->pertipo;
	}
}
//asignaturas del grado
$profasig = "y".$ano."profasig";
$sql="SELECT area, id_area, id_asignatura, asignatura, porcentaje, intensidad, notas, notaQ FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' and notas = '1' ORDER BY area, intensidad desc, codigo";
$datasig = viewsql($sql);
//alumnos
$titulo = "ASIGNATURAS PERDIDAS A�O ".$ano;
$subtitulo = "GRADO ".$grado;
$cursos = "y".$ano."cursos";
$sql="SELECT $cursos.id_alumno, alumnos.apellidos, alumnos.nombres, alumnos.tipoedu FROM $cursos, alumnos WHERE $cursos.grado = '$grado' ";
if ($curso != "")
{
	$sql.="AND $cursos.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
$subtitulo = $subtitulo.$titcorte;
$sql.="AND $cursos.id_alumno = alumnos.id_alumno ORDER BY alumnos.apellidos, alumnos.nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
$notasfa = "y".$ano."notasfa";
$notas = "y".$ano."notas";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("ASIGNATURAS");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
$nalu = 0; //numero de alumnos que pierden area
$pdf->SetFont('Arial','',7);
foreach($datalum as $regalum)
{
	$id_alumno = $regalum->id_alumno;
	$alumno = $regalum->apellidos." ".$regalum->nombres;
	$tipoedu = $regalum->tipoedu;
	//puntajes seg�n tipo educaci�n
	$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
	$datpunt = viewsql($sql);
	$nasi = 0; //n�mero de asignaturas perdidas o con examen
	$id_area = "";
	foreach($datasig as $regasig)
	{
		$id_asignatura = $regasig->id_asignatura;
		$mostrar=0; //indicador mostrar asignatura
		$notas = notas_asignatura($ano,$id_alumno,$id_asignatura,$notanum,$datperi);
		foreach($datperi as $regperi)
		{
			//pierde asignatura nota definitiva en corte de periodo
			if ($notas[9] < $notalim)
				$mostrar=1;
			 //corte en examen y presenta examen
			if ($regperi->pertipo=="E" and $regperi->notanum==$notanum and $notas[$regperi->notanum] > '0')
				$mostrar=1;
		}
                /*
                if ($mostrar==1 and $id_profesor > 0)
                {
                    $sql="select id_profesor from $profasig where id_profesor = '$id_profesor' and id_asignatura = '$id_asignatura' limit 1";
                    $datprof = viewsql($sql);
                    if (count($datprof) > 0)
                        $mostrar = 1;
                    else
                        $mostrar = 0;
                            
                }
                 * 
                 */
		if ($mostrar==1) //si pierde la asignatura
		{
			$nasi = $nasi + 1;
			//imprimir nombre del alumno
			if ($nasi == 1)
			{
				$nalu = $nalu + 1;
				$pdf->SetFont('Arial','B',7);
				$pdf->SetFillColor(224,231,233);				
				$pdf->Cell(60,5,substr($alumno,0,35),1,0,'L',1);
				$pdf->Cell(68,5,"FIRMA:",1,0,'L',1);
				foreach($datperi as $regperi)
				{
					$pdf->Cell(8,5,$regperi->perplan,1,0,'C',1);
				}			
				$pdf->Cell(0,5,"DEF",1,1,'C',1);
			}
			//imprimir area y notas del area
			if ($regasig->id_area != $id_area)
			{
				$id_area = $regasig->id_area;
				$notare = notas_area($ano,$id_alumno,$id_area,$notanum,$datperi);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFillColor(243,247,248);				
				$pdf->Cell(5,5,'','TBL',0,'L',1);
				$pdf->SetFont('Arial','B',7);
				$pdf->Cell(123,5,$regasig->area,'TBR',0,'L',1);
				foreach($datperi as $regperi)
				{
					$notaN = "";
					if ($regperi->notanum <= $notanum)
						$notaN = $notare[$regperi->notanum];
					$pdf->Cell(8,5,$notaN,1,0,'C',1);
				}
				$notafin=$notare[9];			
				$notafinQ = cualitativa($datpunt,$notafin);
				if ($notafin < $notalim)
					$pdf->SetTextColor(164,0,0);
				$pdf->Cell(0,5,$notafin." ".$notafinQ,1,1,'C',1);
				$pdf->SetTextColor(0,0,0);
			}
			//imprimir asignatura
			$id_asignatura = $regasig->id_asignatura;
			$asignatura = $regasig->asignatura;
			$pdf->SetFont('Arial','',7);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(10,5,'','TBL');
			$pdf->Cell(118,5,$asignatura,'TBR',0,'L');
			foreach($datperi as $regperi)
			{
				$notaN = "";
				if ($regperi->notanum <= $notanum)
					$notaN = $notas[$regperi->notanum];
				$pdf->Cell(8,5,$notaN,1,0,'C');
			}
			$notafin=$notas[9];
			$notaQ = cualitativa($datpunt,$notafin);
			$pdf->Cell(0,5,$notafin." ".$notaQ,1,1,'C');
		} //si asignatura perdida
	} //for asignaturas
} //for alumnos	
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,5," TOTAL ALUMNOS: ".$nalu,1,1,'L',1);
$pdf->Output();
disconnect();
?>
