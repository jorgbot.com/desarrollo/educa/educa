<?php
/***********************************************************************************************
* @file        : rep_planillaspen.php (ancho hoja 193)                                         *
* @brief       : Reporte de planillas pendientes por entregar del periodo abierto              *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 23-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
//include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$pertipo = $_GET["pertipo"];
$periodo = $_GET["periodo"];
$pernomb = $_GET["pernomb"];
$selgrado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$selcurso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($implogo == "")
	$implogo = "";
if ($papel == "")
	$papel = "Letter";
connect();
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,"PLANILLAS PENDIENTES DE ENTREGA",0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,"PLANILLAS PENDIENTES DE ENTREGA",0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$titulo,0,1);
		$this->Ln(3);
		$this->SetFont('Arial','B',6);
		$this->Cell(8,5,'Grado',1,0,'L',1);
		$this->Cell(8,5,'Curso',1,0,'L',1);
		$this->Cell(120,5,'Asignatura',1,0,'L',1);
		$this->Cell(0,5,'Docente',1,1,'L',1);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("REPORTE");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$planillas = "y".$ano."planillas";
$profasig = "y".$ano."profasig";
$numreg = 0;
$sql = "select grado, gradonom from grados ";
if ($selgrado != "")
	$sql.= "where grado = '$selgrado' ";
$sql.="order by id_grado";
$datgrad=viewsql($sql);
foreach($datgrad as $reggrad)
{
	$grado = $reggrad->grado;
	$gradonom = $reggrad->gradonom;
	$titulo = "A�O ".$ano." - ".$pernomb;
	if ($grado != "")
		$titulo.=" GRADO ".$gradonom;
	if ($selcurso != "")
		$titulo.=" - CURSO ".$selcurso;
	$sql = "select id_asignatura, asignatura from asignaturas where ano = '$ano' and grado = '$grado' and notas = '1' order by asignatura";
	$datasig=viewsql($sql);
	$pdf->AddPage('P',$papel);
	$sql = "select curso from cursos where ano = '$ano' and grado = '$grado' ";
	if ($selcurso != "")
		$sql.= "and curso = '$selcurso' ";
	$sql.="order by curso";
	$datcurs = viewsql($sql);
	$pdf->SetFont('Arial','',7);
	foreach($datcurs as $regcurs)
	{
		$curso = $regcurs->curso;
		foreach($datasig as $regasig)
		{
			$id_asignatura = $regasig->id_asignatura;
			$estado = "ABIERTA";
			$sql="SELECT estado FROM $planillas WHERE pertipo = '$pertipo' AND periodo = '$periodo' AND grado = '$grado' AND curso = '$curso' AND id_asignatura = '$id_asignatura' LIMIT 1";
			$datplan=viewsql($sql);
			foreach($datplan as $regplan)
			{$estado = $regplan->estado;}
			if ($estado != 'ENTREGADA')
			{
				$numreg = $numreg + 1;
				$profesor = "";
				$sql="SELECT profesor FROM $profasig WHERE id_asignatura = '$id_asignatura' AND grado = '$grado' AND curso = '$curso' LIMIT 1";
				$datprof=viewsql($sql);
				foreach($datprof as $regprof)
				{$profesor = $regprof->profesor;}
				$pdf->Cell(8,5,$grado);
				$pdf->Cell(8,5,$curso);
				$pdf->Cell(120,5,$regasig->asignatura,0,0,'L');
				$pdf->Cell(0,5,$profesor,0,1,'L');
			}
		}
	}
}
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,5," TOTAL ".$numreg,1,1,'L',1);
$pdf->Output();
disconnect();
?>
