<?php
/***********************************************************************************************
* @file        : rep_transp.php (ancho hoja 196)                                           *
* @brief       : lista general de alumnos en transporte por grado y/o curso                     *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-oct-2015                                                    *
* @Modificado  : Nora Rodriguez 13-oct-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_alumnos.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$estado = isset($_GET['estado']) ? $_GET['estado'] : null ;
$id_ruta = isset($_GET['id_ruta']) ? $_GET['id_ruta'] : null ;
$swruta = isset($_GET['swruta']) ? $_GET['swruta'] : 1 ; //saltar pagina en cambio de ruta
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 1 ; //saltar pagina en cambio de curso
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$fchcorte = fec_amd($fchcorte);
$mescorte = substr($fchcorte,5,2);
$titulo = "A�O ".$ano." ALUMNOS POR RUTAS";
$subtitulo = "CORTE A ".fecha_texto($fchcorte);
$filepag = "y".$ano."pagosdet";
connect();
$sql="SELECT transporte.id_alumno,transporte.grado,transporte.curso,transporte.codigo,transporte.id_ruta,transporte.id_familiar,transporte.jornada,transporte.id_rutaop,transporte.id_familiarop,rutas.ruta, rutas.recorrido,".
"alumnos.apellidos,alumnos.nombres,alumnos.docutip,alumnos.docunum FROM transporte, rutas, alumnos ".
"WHERE transporte.ano = '$ano' AND transporte.id_ruta = rutas.id_ruta AND transporte.id_alumno = alumnos.id_alumno ";
if ($grado != "")
{
	$sql.="AND transporte.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
if ($curso != "")
{
	$sql.="AND transporte.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
if ($id_ruta > 0)
{
	$sql.="AND transporte.id_ruta = '$id_ruta' ";
	$subtitulo.=" CURSO ".$curso;
}
if ($estado != "")
{
	$sql.="AND transporte.estado = '$estado' ";
}
$sql.= "ORDER BY rutas.ruta, alumnos.apellidos, alumnos.nombres";
$datalu=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(48,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(11,5,'Curso',1,0,'C',1);
			$this->Cell(7,5,'C�d',1,0,'C',1);
			$this->Cell(16,5,'Celular',1,0,'C',1);
			$this->Cell(12,5,'Tel�fono',1,0,'C',1);
			$this->Cell(25,5,'Recoger/Dejar','LTB',0,'L',1);
			$this->Cell(50,5,'Direcci�n','TBR',0,'L',1);
			$this->Cell(27,5,'Barrio',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//lista de Alumnos
	$numalu = 0;
	$ruta = "";
	foreach($datalu as $regalu)
	{
		if ($ruta!=$regalu->ruta) //saltar hoja por curso
		{
			if ($ruta!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',6);
				$pdf->Cell(0,5,"TOTAL ALUMNOS: ".$numalu,1,1,'L',1);
				if ($swruta > 0)
					$pdf->AddPage('P',$papel);			
			}
			$ruta = $regalu->ruta;
			$numalu = 0;
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(0,8,"RUTA ".$ruta." - ".$regalu->recorrido,1,1,'L');
		}
		$numalu = $numalu +1;		
		$id_alumno = $regalu->id_alumno;
		$id_familiar = $regalu->id_familiar;
		$datfamiru = basicos_familiar($id_familiar);
		$jornada = "RD";
		if ($regalu->jornada==1)
			$jornada = "R";
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(48,5,substr($regalu->apellidos." ".$regalu->nombres,0,30),'LR',0,'L');
		$pdf->Cell(4,5,$regalu->grado,0,0,'C');
		$pdf->Cell(7,5,$regalu->curso,'R',0,'C');
		$pdf->Cell(7,5,$regalu->codigo,'R',0,'C');
		$pdf->Cell(16,5,substr($datfamiru["celular"],0,10),'R',0,'L');
		$pdf->Cell(12,5,substr($datfamiru["telefono"],0,8),'R',0,'L');
		$pdf->Cell(5,5,$jornada,'R',0,'C');
		$pdf->Cell(70,5,$datfamiru["direccion"],'R',0,'L');
		$pdf->Cell(27,5,substr($datfamiru["barrio"],0,20),'R',1,'L');
		if ($regalu->id_rutaop > 0)
		{
			$id_rutaop = $regalu->id_rutaop;
			$rutaop = "";
			$sql = "select ruta from rutas where id_ruta = '$id_rutaop' LIMIT 1";
			$datrut=viewsql($sql);
			foreach($datrut as $regrut)
			{$rutaop = "RUTA ".$regrut->ruta;}
			$id_familiar = $regalu->id_familiarop;
			$datfamiru = basicos_familiar($id_familiar);
			$jornada = "D";
			$pdf->Cell(48,5,"",'LR',0,'L');
			$pdf->Cell(11,5,$rutaop,'R',0,'C');
			$pdf->Cell(7,5,"",'R',0,'L');
			$pdf->Cell(16,5,substr($datfamiru["celular"],0,10),'R',0,'L');
			$pdf->Cell(12,5,substr($datfamiru["telefono"],0,8),'R',0,'L');
			$pdf->Cell(5,5,$jornada,'R',0,'C');
			$pdf->Cell(70,5,$datfamiru["direccion"],'R',0,'L');
			$pdf->Cell(27,5,substr($datfamiru["barrio"],0,20),'R',1,'L');
		}		
	}
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(0,5,"TOTAL ALUMNOS: ".$numalu,1,0,'L',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
