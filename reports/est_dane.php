<?php
/***********************************************************************************************
* @file        : est_dane.php (ancho hoja 196)                                                 *
* @brief       : estadisticas dane total alumnos por a�o                                       *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 18-may-2012                                                    *
* @Modificado  : Nora Rodriguez 18-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$implogo = $_GET["implogo"];
$exportar = $_GET["exportar"];
$papel = $_GET["papel"];
$antano = $ano - 1;
$titulo = "ESTADISTICAS DANE";
$subtitulo = "ALUMNOS A�O LECTIVO ".$ano;
$resultado = "";
connect();
//niveles
$sql= "select nivel, nombre from niveles order by id_nivel";
$datnive=viewsql($sql);
//alumnos no retirados
$cursos = "y".$ano."cursos";
$sql="SELECT $cursos.grado, $cursos.id_alumno, alumnos.* FROM $cursos, alumnos WHERE $cursos.id_alumno = alumnos.id_alumno AND $cursos.fchretiro like '0000%' ".
"ORDER BY sign($cursos.grado), abs($cursos.grado), $cursos.apellidos, $cursos.nombres";
//echo "$sql<br>";
$datalum=viewsql($sql);
$i = 0;
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(4);
			$this->SetFont('Arial','B',8);
			$this->Cell(5,5,'G',1,0,'C',1);
			$this->Cell(16,5,'Apellido1',1,0,'C',1);
			$this->Cell(16,5,'Apellido2',1,0,'C',1);
			$this->Cell(15,5,'Nombre1',1,0,'C',1);
			$this->Cell(15,5,'Nombre2',1,0,'C',1);
			$this->Cell(7,5,'Tipo',1,0,'C',1);
			$this->Cell(15,5,'NoDocum',1,0,'C',1);
			$this->Cell(17,5,'DepExp',1,0,'C',1);
			$this->Cell(16,5,'CiuExp',1,0,'C',1);
			$this->Cell(5,5,'Est',1,0,'C',1);
			$this->Cell(5,5,'Sis',1,0,'C',1);
			$this->Cell(5,5,'Gen',1,0,'C',1);
			$this->Cell(15,5,'FecNace',1,0,'C',1);
			$this->Cell(17,5,'DepNace',1,0,'C',1);
			$this->Cell(16,5,'CiuNace',1,0,'C',1);
			$this->Cell(5,5,'Rep',1,0,'C',1);
			$this->Cell(0,5,'Nue',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
				$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$pdf->SetFont('Arial','',8);
}
else
{
	require_once '../../phpexcel/Classes/PHPExcel/IOFactory.php';
	require_once dirname(__FILE__) . '/../../phpexcel/Classes/PHPExcel.php';
	// Create new PHPExcel object
	$objPHPExcel = new PHPExcel();
	// Set properties
	$objPHPExcel->getProperties()->setTitle("ALUMNOS");
	$i=$i+1;
	$Acelda="'A".$i."'";
	$Bcelda="'B".$i."'";
	$Ccelda="'C".$i."'";
	$Dcelda="'D".$i."'";
	$Ecelda="'E".$i."'";
	$Fcelda="'F".$i."'";
	$Gcelda="'G".$i."'";
	$Hcelda="'H".$i."'";
	$Icelda="'I".$i."'";
	$Jcelda="'J".$i."'";
	$Jcelda="'K".$i."'";
	$Jcelda="'L".$i."'";
	$objPHPExcel->setActiveSheetIndex(0);
	$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, 'GRADO');
	$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, 'DOCTIP');
	$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, 'DOCNUM');
	$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, 'DOCDEP');
	$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, 'DOCCIU');
	$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, 'APELLIDO1');
	$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, 'APELLIDO2');
	$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, 'NOMBRE1');
	$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, 'NOMBRE2');
	$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, 'DEPRESIDE');
	$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, 'CIURESIDE');
	$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, 'FCHNACE');
	$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, 'DEPNACE');
	$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, 'CIUNACE');
	$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, 'GENERO');
	$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, 'REPIT');
	$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, 'NUEVO');
}
//lista de Alumnos
$numreg = 0;
foreach($datalum as $regalum)
{
	$numreg = $numreg +1;
	$id_alumno = $regalum->id_alumno;
	$id_familiar = $regalum->id_familiar;
	$grado = $regalum->grado;
	$docutip = $regalum->docutip;
	$docunum = $regalum->docunum;
	$docuexp = $regalum->docuexp;
	$docudep = $regalum->docudep;
	$genero = $regalum->genero;
	$fchnace = fec_dma($regalum->fchnace);
	$fchnace=strtr($fchnace,'-',"/");
	$depnace = $regalum->depnace;
	$lugnace = $regalum->lugnace;
	$repit = "N";
	if ($regalum->grado == $regalum->procegrado)
			$repit = "S";
	$nuevo = "S";
	$sql = "SELECT id_matricula FROM matriculas WHERE id_alumno = '$id_alumno' AND matriculas.ano = '$antano' LIMIT 1";
	$datmatr = viewsql($sql);
	if (count($datmatr) > 0)
		$nuevo = "N";
	$pos = strpos($regalum->apellidos," ");
	if ($pos <= 0)
		$pos = strlen($regalum->apellidos);
	$apellido1 = trim(substr($regalum->apellidos,0,$pos));
	$apellido2 = trim(substr($regalum->apellidos,$pos+1));
	$pos = strpos($regalum->nombres," ");
	if ($pos <= 0)
		$pos = strlen($regalum->nombres);
	$nombre1 = trim(substr($regalum->nombres,0,$pos));
	$nombre2 = trim(substr($regalum->nombres,$pos+1));
	$sisben = 0;
	if ($regalum->id_saludenti == 1)
		$sisben = 1;
	$estrato = 0;
	$sql="select * from familiares where id_familiar = '$id_familiar' limit 1";
	$datfami = viewsql($sql);
	foreach($datfami as $regfami)
	{$estrato = $regfami->estrato;}
	if ($exportar=="")//PDF
	{
		$pdf->SetFont('Arial','',6);
		$pdf->Cell(5,5,$grado,'LR',0,'C');
		$pdf->Cell(16,5,$apellido1,'LR',0,'L');
		$pdf->Cell(16,5,$apellido2,'LR',0,'L');
		$pdf->Cell(15,5,$nombre1,'LR',0,'L');
		$pdf->Cell(15,5,$nombre2,'LR',0,'L');
		$pdf->Cell(7,5,$docutip,'LR',0,'L');
		$pdf->Cell(15,5,$docunum,'LR',0,'L');
		$pdf->Cell(17,5,substr($docudep,0,12),'LR',0,'L');
		$pdf->Cell(16,5,substr($docuexp,0,12),'LR',0,'L');
		$pdf->Cell(5,5,$estrato,'LR',0,'C');
		$pdf->Cell(5,5,$sisben,'LR',0,'C');
		$pdf->Cell(5,5,$genero,'LR',0,'C');
		$pdf->Cell(15,5,$fchnace,'LR',0,'L');
		$pdf->Cell(17,5,substr($depnace,0,12),'LR',0,'L');
		$pdf->Cell(16,5,substr($lugnace,0,12),'LR',0,'L');
		$pdf->Cell(5,5,$repit,'LR',0,'C');
		$pdf->Cell(0,5,$nuevo,'LR',1,'C');
	}
	else
	{
		$Acelda="'A".$i."'";
		$Bcelda="'B".$i."'";
		$Ccelda="'C".$i."'";
		$Dcelda="'D".$i."'";
		$Ecelda="'E".$i."'";
		$Fcelda="'F".$i."'";
		$Gcelda="'G".$i."'";
		$Hcelda="'H".$i."'";
		$Icelda="'I".$i."'";
		$Jcelda="'J".$i."'";
		$Jcelda="'K".$i."'";
		$Jcelda="'L".$i."'";
		$objPHPExcel->setActiveSheetIndex(0);
		$objPHPExcel->getActiveSheet()->setCellValue('A'.$i, $grado);
		$objPHPExcel->getActiveSheet()->setCellValue('B'.$i, $docutip);
		$objPHPExcel->getActiveSheet()->setCellValue('C'.$i, $docunum);
		$objPHPExcel->getActiveSheet()->setCellValue('D'.$i, $docudep);
		$objPHPExcel->getActiveSheet()->setCellValue('E'.$i, $docuexp);
		$objPHPExcel->getActiveSheet()->setCellValue('F'.$i, $apellido1);
		$objPHPExcel->getActiveSheet()->setCellValue('G'.$i, $apellido2);
		$objPHPExcel->getActiveSheet()->setCellValue('H'.$i, $nombre1);
		$objPHPExcel->getActiveSheet()->setCellValue('I'.$i, $nombre2);
		$objPHPExcel->getActiveSheet()->setCellValue('J'.$i, 'BOGOTA');
		$objPHPExcel->getActiveSheet()->setCellValue('K'.$i, 'BOGOTA');
		$objPHPExcel->getActiveSheet()->setCellValue('L'.$i, $fchnace);
		$objPHPExcel->getActiveSheet()->setCellValue('M'.$i, $depnace);
		$objPHPExcel->getActiveSheet()->setCellValue('N'.$i, $lugnace);
		$objPHPExcel->getActiveSheet()->setCellValue('O'.$i, $genero);
		$objPHPExcel->getActiveSheet()->setCellValue('P'.$i, $repit);
		$objPHPExcel->getActiveSheet()->setCellValue('Q'.$i, $nuevo);
	}
}
disconnect();
if ($exportar=="")//PDF
{	
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(0,5," TOTAL ALUMNOS: ".$numreg,1,1,'L',1);
	$pdf->Output();
}
else
{
	$objPHPExcel->setActiveSheetIndex(0);
	//$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
	$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('D')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('E')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('F')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('G')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('H')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('I')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('J')->setAutoSize(true);						
	$objPHPExcel->getActiveSheet()->getColumnDimension('K')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('L')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('M')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('N')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('O')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('P')->setAutoSize(true);
	$objPHPExcel->getActiveSheet()->getColumnDimension('Q')->setAutoSize(true);
	$report_dir = ROOT_DIR."/reports/";
	$filename = "est_dane.XLS";						
	$archivo = ROOT_DIR."/reports/".$filename; //ruta completa del archivo a generar 
	$exist = file_exists($archivo);
	if ($exist=="TRUE")
		{@unlink ($archivo);}
	//$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	$objWriter = new PHPExcel_Writer_Excel5($objPHPExcel);
	$objWriter->save(str_replace('dane.php', 'dane.XLS', __FILE__));
	?>
	<html>
	<head>
	<title>Exportar</title>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link href="../estilos.css" rel="stylesheet" type="text/css">
	</head>
	<body>
	<table border="0" cellpadding="0" cellspacing="0" width="90%" align="center">
		<tr><td height="20"></td></tr>
		<tr>
			<td>
				<table width="100%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0">
					<tr><th colspan="3" height="25">EXPORTAR LISTA DE ALUMNOS</th></tr>
					<tr height="30"><td colspan="3"></td></tr>
					<tr valign="top">
						<td width="20"></td>
						<td>
							<fieldset>
							<legend class="titulo_campos">Resultado</legend>
							<table border="0">
								<tr>
									<td height="50"><?php echo $resultado;?></td>
								</tr>
								<tr>
									<td height="40">
									<?php if ($archivo!="") { ?>
									<a class="link_menu" href='../setup_export.php?exportdir=<?php echo $report_dir;?>&filename=<?php echo $filename;?>'><img src="../images/ico_download.png" border="0" width="16" height="16" alt="Descargar" hspace="2">DESCARGUE AQUI EL ARCHIVO: <?php echo $filename;?></a>
									<?php } ?>
									</td>
								</tr>
							</table>
							</fieldset>
						</td>
						<td width="20"></td>
					</tr>
					<tr height="30"><td colspan="3"></td></tr>
					<tr valign="middle" height="35">
						<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="../images/backsilver.gif">
							<a href="javascript:;" onClick="window.close();"><img src="../images/btn_cerrar.PNG" border="0" width="70" height="24" alt="Cerrar" hspace="20" align="absmiddle"></a>					
						</td>
					</tr>
				</table>
				<br><br>
			</td>
		</tr>
	</table>
	</body>
	</html>
	<?php
}
?>
