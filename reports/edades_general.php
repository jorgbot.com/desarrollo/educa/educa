<?php
/***********************************************************************************************
* @file        : est_alumnos.php (ancho hoja 196)                                              *
* @brief       : estadisticas alumnos por grados                                               *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 14-may-2012                                                    *
* @Modificado  : Nora Rodriguez 14-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set("America/Bogota");
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$nivel = isset($_GET['nivel']) ? $_GET['nivel'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$antano = $ano - 1;
$titulo = "ESTADISTICAS DE ALUMNOS SEG�N EDAD Y G�NERO";
$subtitulo = "A�O LECTIVO ".$ano;
connect();
//niveles
$sql= "select nivel, nombre from niveles order by id_nivel";
$datnive=viewsql($sql);
//alumnos
$sql="SELECT matriculas.grado, matriculas.curso, matriculas.id_alumno, grados.nivel, grados.gradonom, alumnos.fchnace, alumnos.genero FROM matriculas, grados, alumnos WHERE matriculas.ano = '$ano' AND matriculas.estado = 'CERRADA' AND matriculas.curso != '' AND matriculas.fchretiro like '0000%' AND matriculas.grado = grados.grado AND matriculas.id_alumno = alumnos.id_alumno ";
$condi = "";
if ($nivel != "")
{
	$nomnivel = substr($nivel,1);
	$nivel = substr($nivel,0,1);
	$condi.="AND matriculas.nivel = '$nivel' ";
	$subtitulo.=" - NIVEL ".$nomnivel;
}
if ($grado != "")
{
	$condi.="AND matriculas.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
$sql = $sql.$condi."ORDER BY grados.id_grado, matriculas.curso";
$datalum=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(4);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//titulos
	
	
   	
	

	//lista de Alumnos
	$alumest = array();
	$tottres = 0;
	$totcuatro = 0;
	$totcinco = 0;
	$totseis = 0;
	$totsiete = 0;
	$totocho = 0;
	$totnueve = 0;
	$totdiez = 0;
	$totonce = 0;
	$totdoce = 0;
	$tottrece = 0;
	$totcatorce = 0;
	$totquince = 0;
	$totdieciseis = 0;
	$totdiecisiete = 0;
	$totdieciocho = 0;
	$totdiecinueve = 0;
	$totveinte = 0;
	$gradonom = "";
	$curso = "";
	
	$pdf->SetFont('Arial','',8);
	foreach($datalum as $regalum)
	{
		if ($regalum->gradonom != $gradonom)
		{
			if ($gradonom != "")
			{
				$pdf->SetFont('Arial','B',8);
				$pdf->Cell(20,5,$gradonom,1,0,'C',1);
				$pdf->Cell(9,5,'3',1,0,'C',1);
				$pdf->Cell(9,5,'4',1,0,'C',1);
				$pdf->Cell(9,5,'5',1,0,'C',1);
				$pdf->Cell(9,5,'6',1,0,'C',1);
				$pdf->Cell(9,5,'7',1,0,'C',1);
				$pdf->Cell(9,5,'8',1,0,'C',1);
				$pdf->Cell(9,5,'9',1,0,'C',1);
				$pdf->Cell(9,5,'10',1,0,'C',1);
				$pdf->Cell(9,5,'11',1,0,'C',1);
				$pdf->Cell(9,5,'12',1,0,'C',1);
				$pdf->Cell(9,5,'13',1,0,'C',1);
				$pdf->Cell(9,5,'14',1,0,'C',1);
				$pdf->Cell(9,5,'15',1,0,'C',1);
				$pdf->Cell(9,5,'16',1,0,'C',1);
				$pdf->Cell(9,5,'17',1,0,'C',1);
				$pdf->Cell(9,5,'18',1,0,'C',1);
				$pdf->Cell(9,5,'19',1,0,'C',1); 
				$pdf->Cell(9,5,'20',1,1,'C',1); 
	
	
				$pdf->SetFont('Arial','',8);
	            $pdf->Cell(20,5,'Ni�os',1,0,'C',1);
				$pdf->Cell(9,5,number_format($mtres,0),1,0,'C',1);
	            $pdf->Cell(9,5,number_format($mcuatro,0),1,0,'C',1);
	            $pdf->Cell(9,5,number_format($mcinco,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($msiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mnueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiez,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($monce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdoce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mtrece,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mcatorce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mquince,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdieciseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiecisiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdieciocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiecinueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mveinte,0),1,1,'C',1);
				
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(20,5,'Ni�as',1,0,'C',1);
				$pdf->Cell(9,5,number_format($ftres,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcuatro,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcinco,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fsiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($focho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fnueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiez,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fonce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdoce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($ftrece,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcatorce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fquince,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdieciseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiecisiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdieciocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiecinueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fveinte,0),1,1,'C',1);
				
				
				$pdf->Cell(20,7,"TOTAL",1,0,'C');
				$pdf->Cell(9,7,number_format($mtres+$ftres),1,0,'C');
				$pdf->Cell(9,7,number_format($mcuatro+$fcuatro),1,0,'C');
				$pdf->Cell(9,7,number_format($mcinco+$fcinco),1,0,'C');
				$pdf->Cell(9,7,number_format($mseis+$fseis),1,0,'C');
				$pdf->Cell(9,7,number_format($msiete+$fsiete),1,0,'C');
				$pdf->Cell(9,7,number_format($mocho+$focho),1,0,'C');
				$pdf->Cell(9,7,number_format($mnueve+$fnueve),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiez+$fdiez),1,0,'C');
				$pdf->Cell(9,7,number_format($monce+$fonce),1,0,'C');
				$pdf->Cell(9,7,number_format($mdoce+$fdoce),1,0,'C');
				$pdf->Cell(9,7,number_format($mtrece+$ftrece),1,0,'C');
				$pdf->Cell(9,7,number_format($mcatorce+$fcatorce),1,0,'C');
				$pdf->Cell(9,7,number_format($mquince+$fquince),1,0,'C');
				$pdf->Cell(9,7,number_format($mdieciseis+$fdieciseis),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiecisiete+$fdiecisiete),1,0,'C');
				$pdf->Cell(9,7,number_format($mdieciocho+$fdieciocho),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiecinueve+$fdiecinueve),1,0,'C');
				$pdf->Cell(9,7,number_format($mveinte+$fveinte),1,1,'C');
				
					  
				$pdf->Cell(182,5,' ',1,1,'C',0);
				
			}
			$gradonom = $regalum->gradonom;
			$granum = 0;
			$curnum = 0;
			$curnue = 0;
			$curant = 0;
	
	$mtres = 0;
	$mcuatro = 0;
	$mcinco = 0;
	$mseis = 0;
	$msiete = 0;
	$mocho = 0;
	$mnueve = 0;
	$mdiez = 0;
	$monce = 0;
	$mdoce = 0;
	$mtrece = 0;
	$mcatorce = 0;
	$mquince = 0;
	$mdieciseis = 0;
	$mdiecisiete = 0;
	$mdieciocho = 0;
	$mdiecinueve = 0;
	$mveinte = 0;
	
	$ftres = 0;
	$fcuatro = 0;
	$fcinco = 0;
	$fseis = 0;
	$fsiete = 0;
	$focho = 0;
	$fnueve = 0;
	$fdiez = 0;
	$fonce = 0;
	$fdoce = 0;
	$ftrece = 0;
	$fcatorce = 0;
	$fquince = 0;
	$fdieciseis = 0;
	$fdiecisiete = 0;
	$fdieciocho = 0;
	$fdiecinueve = 0;
	$fveinte = 0;
		}
		
		if ($grado!="" and $gradonom != "" and $regalum->curso!=$curso)
		{
			if ($curso!="")
			{
				$pdf->SetFont('Arial','B',8);
				$pdf->Cell(20,5,$curso,1,0,'C',1);
				$pdf->Cell(9,5,'3',1,0,'C',1);
				$pdf->Cell(9,5,'4',1,0,'C',1);
				$pdf->Cell(9,5,'5',1,0,'C',1);
				$pdf->Cell(9,5,'6',1,0,'C',1);
				$pdf->Cell(9,5,'7',1,0,'C',1);
				$pdf->Cell(9,5,'8',1,0,'C',1);
				$pdf->Cell(9,5,'9',1,0,'C',1);
				$pdf->Cell(9,5,'10',1,0,'C',1);
				$pdf->Cell(9,5,'11',1,0,'C',1);
				$pdf->Cell(9,5,'12',1,0,'C',1);
				$pdf->Cell(9,5,'13',1,0,'C',1);
				$pdf->Cell(9,5,'14',1,0,'C',1);
				$pdf->Cell(9,5,'15',1,0,'C',1);
				$pdf->Cell(9,5,'16',1,0,'C',1);
				$pdf->Cell(9,5,'17',1,0,'C',1);
				$pdf->Cell(9,5,'18',1,0,'C',1);
				$pdf->Cell(9,5,'19',1,0,'C',1); 
				$pdf->Cell(9,5,'20',1,1,'C',1); 
	
	
				$pdf->SetFont('Arial','',8);
	            $pdf->Cell(20,5,'Ni�os',1,0,'C',1);
				$pdf->Cell(9,5,number_format($mtres,0),1,0,'C',1);
	            $pdf->Cell(9,5,number_format($mcuatro,0),1,0,'C',1);
	            $pdf->Cell(9,5,number_format($mcinco,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($msiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mnueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiez,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($monce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdoce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mtrece,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mcatorce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mquince,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdieciseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiecisiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdieciocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiecinueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mveinte,0),1,1,'C',1);
				
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(20,5,'Ni�as',1,0,'C',1);
				$pdf->Cell(9,5,number_format($ftres,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcuatro,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcinco,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fsiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($focho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fnueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiez,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fonce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdoce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($ftrece,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcatorce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fquince,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdieciseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiecisiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdieciocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiecinueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fveinte,0),1,1,'C',1);
				
				
				$pdf->Cell(20,7,"TOTAL",1,0,'C');
				$pdf->Cell(9,7,number_format($mtres+$ftres),1,0,'C');
				$pdf->Cell(9,7,number_format($mcuatro+$fcuatro),1,0,'C');
				$pdf->Cell(9,7,number_format($mcinco+$fcinco),1,0,'C');
				$pdf->Cell(9,7,number_format($mseis+$fseis),1,0,'C');
				$pdf->Cell(9,7,number_format($msiete+$fsiete),1,0,'C');
				$pdf->Cell(9,7,number_format($mocho+$focho),1,0,'C');
				$pdf->Cell(9,7,number_format($mnueve+$fnueve),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiez+$fdiez),1,0,'C');
				$pdf->Cell(9,7,number_format($monce+$fonce),1,0,'C');
				$pdf->Cell(9,7,number_format($mdoce+$fdoce),1,0,'C');
				$pdf->Cell(9,7,number_format($mtrece+$ftrece),1,0,'C');
				$pdf->Cell(9,7,number_format($mcatorce+$fcatorce),1,0,'C');
				$pdf->Cell(9,7,number_format($mquince+$fquince),1,0,'C');
				$pdf->Cell(9,7,number_format($mdieciseis+$fdieciseis),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiecisiete+$fdiecisiete),1,0,'C');
				$pdf->Cell(9,7,number_format($mdieciocho+$fdieciocho),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiecinueve+$fdiecinueve),1,0,'C');
				$pdf->Cell(9,7,number_format($mveinte+$fveinte),1,1,'C');
			}
			$curso = $regalum->curso;
			$curnum = 0;
			$curnue = 0;
			$curant = 0;
			$curmas = 0;
		    $curfem = 0;
			
			$mtres = 0;
			$mcuatro = 0;
			$mcinco = 0;
			$mseis = 0;
			$msiete = 0;
			$mocho = 0;
			$mnueve = 0;
			$mdiez = 0;
			$monce = 0;
			$mdoce = 0;
			$mtrece = 0;
			$mcatorce = 0;
			$mquince = 0;
			$mdieciseis = 0;
			$mdiecisiete = 0;
			$mdieciocho = 0;
			$mdiecinueve = 0;
			$mveinte = 0;
			
			$ftres = 0;
			$fcuatro = 0;
			$fcinco = 0;
			$fseis = 0;
			$fsiete = 0;
			$focho = 0;
			$fnueve = 0;
			$fdiez = 0;
			$fonce = 0;
			$fdoce = 0;
			$ftrece = 0;
			$fcatorce = 0;
			$fquince = 0;
			$fdieciseis = 0;
			$fdiecisiete = 0;
			$fdieciocho = 0;
			$fdiecinueve = 0;
			$fveinte = 0;
		}
		$granum = $granum + 1;
		$curnum = $curnum + 1;
		//$totnum = $totnum + 1;
		$id_alumno = $regalum->id_alumno;
	
	$sql = "SELECT id_matricula FROM matriculas WHERE id_alumno = '$id_alumno' AND matriculas.ano = '$antano' LIMIT 1";
		$datmatr = viewsql($sql);
		
		$edad = calculo_edad($regalum->fchnace);
		
		
		if($edad==3){
			if($regalum->genero == "M"){
			$mtres=$mtres+1;			
			}
			else {
			$ftres=$ftres+1;	
			}
			
		}
		if($edad==4){
			if($regalum->genero == "M"){
			$mcuatro=$mcuatro+1;			
			}
			else {
			$fcuatro=$fcuatro+1;	
			}
			
		}
		if($edad==5){
			if($regalum->genero == "M"){
			$mcinco=$mcinco+1;			
			}
			else {
			$fcinco=$fcinco+1;	
			}
			
		}
		if($edad==6){
			if($regalum->genero == "M"){
			$mseis=$mseis+1;			
			}
			else {
			$fseis=$fseis+1;	
			}
			
		}
		if($edad==7){
			if($regalum->genero == "M"){
			$msiete=$msiete+1;			
			}
			else {
			$fsiete=$fsiete+1;	
			}
			
		}
		if($edad==8){
			if($regalum->genero == "M"){
			$mocho=$mocho+1;			
			}
			else {
			$focho=$focho+1;	
			}
			
		}
		if($edad==9){
			if($regalum->genero == "M"){
			$mnueve=$mnueve+1;			
			}
			else {
			$fnueve=$fnueve+1;	
			}
			
		}
		if($edad==10){
			if($regalum->genero == "M"){
			$mdiez=$mdiez+1;			
			}
			else {
			$fdiez=$fdiez+1;	
			}
			
		}
		if($edad==11){
			if($regalum->genero == "M"){
			$monce=$monce+1;			
			}
			else {
			$fonce=$fonce+1;	
			}
		
		}
		if($edad==12){
			if($regalum->genero == "M"){
			$mdoce=$mdoce+1;			
			}
			else {
			$fdoce=$fdoce+1;	
			}
			
		}
		if($edad==13){
		    if($regalum->genero == "M"){
			$mtrece=$mtrece+1;			
			}
			else {
			$ftrece=$ftrece+1;	
			}
			
		}
		if($edad==14){
			if($regalum->genero == "M"){
			$mcatorce=$mcatorce+1;			
			}
			else {
			$fcatorce=$fcatorce+1;	
			}
			
		}
		if($edad==15){
			if($regalum->genero == "M"){
			$mquince=$mquince+1;			
			}
			else {
			$fquince=$fquince+1;	
			}
			
		}
		if($edad==16){
			if($regalum->genero == "M"){
			$mdieciseis=$mdieciseis+1;			
			}
			else {
			$fdieciseis=$fdieciseis+1;	
			}
			
		}
		if($edad==17){
			if($regalum->genero == "M"){
			$mdiecisiete=$mdiecisiete+1;			
			}
			else {
			$fdiecisiete=$fdiecisiete+1;	
			}
			
		}
		if($edad==18){
			if($regalum->genero == "M"){
			$mdieciocho=$mdieciocho+1;			
			}
			else {
			$fdieciocho=$fdieciocho+1;	
			}
			
		} 
		if($edad==19){
			if($regalum->genero == "M"){
			$mdiecinueve=$mdiecinueve+1;			
			}
			else {
			$fdiecinueve=$fdiecinueve+1;	
			}
			
		}
		if($edad==20){
			if($regalum->genero == "M"){
			$mveinte=$mveinte+1;			
			}
			else {
			$fveinte=$fveinte+1;	
			}
			
		}
	}
	
	
	if ($grado!="")
	{
		$pdf->SetFont('Arial','B',8);
				$pdf->Cell(20,5,$curso,1,0,'C',1);
					$pdf->Cell(9,5,'3',1,0,'C',1);
				$pdf->Cell(9,5,'4',1,0,'C',1);
				$pdf->Cell(9,5,'5',1,0,'C',1);
				$pdf->Cell(9,5,'6',1,0,'C',1);
				$pdf->Cell(9,5,'7',1,0,'C',1);
				$pdf->Cell(9,5,'8',1,0,'C',1);
				$pdf->Cell(9,5,'9',1,0,'C',1);
				$pdf->Cell(9,5,'10',1,0,'C',1);
				$pdf->Cell(9,5,'11',1,0,'C',1);
				$pdf->Cell(9,5,'12',1,0,'C',1);
				$pdf->Cell(9,5,'13',1,0,'C',1);
				$pdf->Cell(9,5,'14',1,0,'C',1);
				$pdf->Cell(9,5,'15',1,0,'C',1);
				$pdf->Cell(9,5,'16',1,0,'C',1);
				$pdf->Cell(9,5,'17',1,0,'C',1);
				$pdf->Cell(9,5,'18',1,0,'C',1);
				$pdf->Cell(9,5,'19',1,0,'C',1); 
				$pdf->Cell(9,5,'20',1,1,'C',1); 
	
	
				$pdf->SetFont('Arial','',8);
	            $pdf->Cell(20,5,'Ni�os',1,0,'C',1);
				$pdf->Cell(9,5,number_format($mtres,0),1,0,'C',1);
	            $pdf->Cell(9,5,number_format($mcuatro,0),1,0,'C',1);
	            $pdf->Cell(9,5,number_format($mcinco,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($msiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mnueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiez,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($monce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdoce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mtrece,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mcatorce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mquince,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdieciseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiecisiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdieciocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiecinueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mveinte,0),1,1,'C',1);
				
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(20,5,'Ni�as',1,0,'C',1);
				$pdf->Cell(9,5,number_format($ftres,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcuatro,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcinco,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fsiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($focho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fnueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiez,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fonce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdoce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($ftrece,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcatorce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fquince,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdieciseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiecisiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdieciocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiecinueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fveinte,0),1,1,'C',1);
				
				
				$pdf->Cell(20,7,"TOTAL",1,0,'C');
				$pdf->Cell(9,7,number_format($mtres+$ftres),1,0,'C');
				$pdf->Cell(9,7,number_format($mcuatro+$fcuatro),1,0,'C');
				$pdf->Cell(9,7,number_format($mcinco+$fcinco),1,0,'C');
				$pdf->Cell(9,7,number_format($mseis+$fseis),1,0,'C');
				$pdf->Cell(9,7,number_format($msiete+$fsiete),1,0,'C');
				$pdf->Cell(9,7,number_format($mocho+$focho),1,0,'C');
				$pdf->Cell(9,7,number_format($mnueve+$fnueve),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiez+$fdiez),1,0,'C');
				$pdf->Cell(9,7,number_format($monce+$fonce),1,0,'C');
				$pdf->Cell(9,7,number_format($mdoce+$fdoce),1,0,'C');
				$pdf->Cell(9,7,number_format($mtrece+$ftrece),1,0,'C');
				$pdf->Cell(9,7,number_format($mcatorce+$fcatorce),1,0,'C');
				$pdf->Cell(9,7,number_format($mquince+$fquince),1,0,'C');
				$pdf->Cell(9,7,number_format($mdieciseis+$fdieciseis),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiecisiete+$fdiecisiete),1,0,'C');
				$pdf->Cell(9,7,number_format($mdieciocho+$fdieciocho),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiecinueve+$fdiecinueve),1,0,'C');
				$pdf->Cell(9,7,number_format($mveinte+$fveinte),1,1,'C');
		
	}
	else
	{
		       $pdf->SetFont('Arial','B',8);
				$pdf->Cell(20,5,$gradonom,1,0,'C',1);
					$pdf->Cell(9,5,'3',1,0,'C',1);
				$pdf->Cell(9,5,'4',1,0,'C',1);
				$pdf->Cell(9,5,'5',1,0,'C',1);
				$pdf->Cell(9,5,'6',1,0,'C',1);
				$pdf->Cell(9,5,'7',1,0,'C',1);
				$pdf->Cell(9,5,'8',1,0,'C',1);
				$pdf->Cell(9,5,'9',1,0,'C',1);
				$pdf->Cell(9,5,'10',1,0,'C',1);
				$pdf->Cell(9,5,'11',1,0,'C',1);
				$pdf->Cell(9,5,'12',1,0,'C',1);
				$pdf->Cell(9,5,'13',1,0,'C',1);
				$pdf->Cell(9,5,'14',1,0,'C',1);
				$pdf->Cell(9,5,'15',1,0,'C',1);
				$pdf->Cell(9,5,'16',1,0,'C',1);
				$pdf->Cell(9,5,'17',1,0,'C',1);
				$pdf->Cell(9,5,'18',1,0,'C',1);
				$pdf->Cell(9,5,'19',1,0,'C',1); 
				$pdf->Cell(9,5,'20',1,1,'C',1); 
	
	
				$pdf->SetFont('Arial','',8);
	            $pdf->Cell(20,5,'Ni�os',1,0,'C',1);
				$pdf->Cell(9,5,number_format($mtres,0),1,0,'C',1);
	            $pdf->Cell(9,5,number_format($mcuatro,0),1,0,'C',1);
	            $pdf->Cell(9,5,number_format($mcinco,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($msiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mnueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiez,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($monce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdoce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mtrece,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mcatorce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mquince,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdieciseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiecisiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdieciocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mdiecinueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($mveinte,0),1,1,'C',1);
				
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(20,5,'Ni�as',1,0,'C',1);
				$pdf->Cell(9,5,number_format($ftres,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcuatro,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcinco,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fsiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($focho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fnueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiez,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fonce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdoce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($ftrece,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fcatorce,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fquince,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdieciseis,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiecisiete,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdieciocho,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fdiecinueve,0),1,0,'C',1);
				$pdf->Cell(9,5,number_format($fveinte,0),1,1,'C',1);
				
				
				$pdf->Cell(20,7,"TOTAL",1,0,'C');
				$pdf->Cell(9,7,number_format($mtres+$ftres),1,0,'C');
				$pdf->Cell(9,7,number_format($mcuatro+$fcuatro),1,0,'C');
				$pdf->Cell(9,7,number_format($mcinco+$fcinco),1,0,'C');
				$pdf->Cell(9,7,number_format($mseis+$fseis),1,0,'C');
				$pdf->Cell(9,7,number_format($msiete+$fsiete),1,0,'C');
				$pdf->Cell(9,7,number_format($mocho+$focho),1,0,'C');
				$pdf->Cell(9,7,number_format($mnueve+$fnueve),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiez+$fdiez),1,0,'C');
				$pdf->Cell(9,7,number_format($monce+$fonce),1,0,'C');
				$pdf->Cell(9,7,number_format($mdoce+$fdoce),1,0,'C');
				$pdf->Cell(9,7,number_format($mtrece+$ftrece),1,0,'C');
				$pdf->Cell(9,7,number_format($mcatorce+$fcatorce),1,0,'C');
				$pdf->Cell(9,7,number_format($mquince+$fquince),1,0,'C');
				$pdf->Cell(9,7,number_format($mdieciseis+$fdieciseis),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiecisiete+$fdiecisiete),1,0,'C');
				$pdf->Cell(9,7,number_format($mdieciocho+$fdieciocho),1,0,'C');
				$pdf->Cell(9,7,number_format($mdiecinueve+$fdiecinueve),1,0,'C');
				$pdf->Cell(9,7,number_format($mveinte+$fveinte),1,1,'C');
				
	}
	
	
	
//	$pdf->SetFont('Arial','B',8);
//	$pdf->Cell(20,5,'TOTAL',1,0,'C',1);
//	$pdf->Cell(10,5,number_format($tottres,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totcuatro,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totcinco,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totseis,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totsiete,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totocho,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totnueve,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totdiez,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totonce,0),1,0,'C',1);
//  $pdf->Cell(10,5,number_format($totdoce,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($tottrece,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totcatorce,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totquince,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totdieciseis,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totdiecisiete,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totdieciocho,0),1,0,'C',1);
//	$pdf->Cell(10,5,number_format($totdiecinueve,0),1,1,'C',1);
	
//	$grantotal=$tottres+$totcuatro+$totcinco+$totseis+$totsiete+$totocho+$totnueve+$totdiez+$totonce+$totdoce+$tottrece+$totcatorce+$totquince+$totdieciseis+$totdiecisiete+$totdieciocho+$totdiecinueve;
	
//	$pdf->SetFont('Arial','B',8);
//	$pdf->Cell(20,5,'TOTAL GLOBAL',1,0,'C',1);
//	$pdf->Cell(10,5,number_format($grantotal,0),1,0,'C',1);

	$pdf->Output();
	disconnect();

	
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $granumrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
