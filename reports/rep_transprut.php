<?php
/***********************************************************************************************
* @file        : rep_transprut.php (ancho hoja 196)                                            *
* @brief       : lista de rutas y conductores                                                  *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-oct-2015                                                    *
* @Modificado  : Nora Rodriguez 13-oct-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_alumnos.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$titulo = "A�O ".$ano." RUTAS Y CONDUCTORES";
connect();
$sql="SELECT rutas.id_ruta, rutas.ruta, rutas.recorrido, conductores.* FROM rutas, conductores ".
"WHERE rutas.ano = '$ano' AND rutas.id_conductor = conductores.id_conductor ORDER BY rutas.ruta";
$datrut=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("RUTAS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,80,0);
	$pdf->Cell(100,6,$titulo,0,1);
	$pdf->SetTextColor(0,0,0);
	$pdf->Ln();
	$pdf->Ln();
	//lista de rutas
	$numrut = 0;
	$numalu = 0;
	$ruta = "";
	foreach($datrut as $regrut)
	{
		if ($ruta!=$regrut->ruta) //saltar hoja por curso
		{
			$numrut = $numrut +1;
			$id_ruta = $regrut->id_ruta;
			$ruta = $regrut->ruta;
			$numalu = 0;
			$sql = "SELECT id_alumno FROM transporte WHERE (id_ruta = '$id_ruta' OR id_rutaop = '$id_ruta') AND estado = 'ACTIVO'";
			$datalu=viewsql($sql);
			$numalu = count($datalu);
			if ($numrut==8 or $numrut==16) 
				$pdf->AddPage('P',$papel);
			$pdf->SetFont('Arial','B',8); 
			$pdf->Cell(150,5,"RUTA ".$ruta." - ".$regrut->recorrido,'LTB',0,'L',1);
			$pdf->Cell(30,5,"ALUMNOS: ".$numalu,'TBR',1,'R',1);
		}
		$numalu = $numalu +1;		
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(130,5,"CONDUCTOR: ".substr($regrut->apellidos." ".$regrut->nombres,0,30). "                   RH: ".$regrut->rh. "           CATEGOR�A: ".$regrut->categoria,'L',0,'L');
		$pdf->Cell(50,5,"PLACA: ".$regrut->placa,'LR',1,'L');
		$pdf->Cell(130,5,"CEDULA: ".$regrut->docunum." ".$regrut->docuexp." INGRESO: ".fecha_texto($regrut->fchingreso),'L',0,'L');
		$pdf->Cell(50,5,"MARCA: ".$regrut->marca,'LR',1,'L');
		$pdf->Cell(130,5,"CELULAR: ".$regrut->celular." TELEFONO: ".$regrut->telefono,'L',0,'L');
		$pdf->Cell(50,5,"COLOR: ".$regrut->color,'LR',1,'L');
		$pdf->Cell(130,5,"DIRECCION: ".$regrut->direccion. "           LOCALIDAD: ". $regrut->foto,'L',0,'L');
		$pdf->Cell(50,5,"MODELO: ".$regrut->modelo,'LR',1,'L');
		$pdf->Cell(130,5,"BARRIO: ".$regrut->barrio,'BL',0,'L');
		$pdf->Cell(50,5,"CAPACIDAD: ".$regrut->capacidad,'BLR',1,'L');
		
		
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
