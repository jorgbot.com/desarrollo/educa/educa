<?php
/***********************************************************************************************
* @file        : rep_profesoresanu.php (ancho hoja 196)                                        *
* @brief       : Informe completo de protocolizaci�n de docentes                               *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
if ($_GET["exportar"]=="XLS")
{echo"<script>window.close();window.opener.document.location.href='../excel/com_excel/profesores.php?ano=".$_GET["ano"]."';</script>";}
   
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
include("../libs/docentes.php");
$ano = $_GET["ano"];
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
connect();
//institucion
$sql="select * from institucion limit 1";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$rector = $reginst->rector;
	$secretaria = $reginst->secretaria;
}
//profesores
$sql="SELECT profesorescont.*, profesores.apellidos, profesores.nombres, profesores.docunum, profesores.docutip, profesores.docuexp, profesores.cargo, profesores.direccion, profesores.telefono, profesores.escgrado, profesores.escresol, profesores.especialidad ".
"FROM profesorescont, profesores WHERE profesorescont.ano = '$ano' and profesorescont.id_profesor = profesores.id_profesor order by profesores.apellidos, profesores.nombres";
$datprof=viewsql($sql);
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"];
			$implogo = $GLOBALS["implogo"] ;
			if ($implogo=="S")
			{
				$datinst = $GLOBALS["datinst"] ;
				foreach($datinst as $reginst)
				{
					$insnom = $reginst->instinom;
					$inssig = $reginst->instisig;
					$insciu = $reginst->ciudad;
					$insdir = $reginst->direccion;
					$instel = $reginst->telefono;
					$insre1 = $reginst->resolkin;
					$insre2 = $reginst->resolpri;
					$insre3 = $reginst->resolsec;
					$inssec = $reginst->resolseed;
					$insdan = $reginst->resoldane;
					$insnot = $reginst->notaria;
				}
				$this->Image('../images/logo.jpg',12,10,20,0,'JPG');
				$this->Ln(5);
				$this->SetFont('Arial','B',12);
				$this->SetTextColor(0,0,0);
				$this->Cell(25,4,"");
				$this->Cell(0,8,$insnom." ".$inssig,0,1,'L');
				$this->SetFont('Arial','',8);
				$this->Cell(25,4,"");
				$this->Cell(0,8,$insdir." ".$insciu." Tels ".$instel,0,1,'L');
				$this->SetFont('Arial','',6);
				$this->Cell(25,4,"");
				$this->Cell(90,4,$insre1,0,0,'L');
				$this->Cell(90,4,$insre2,0,0,'L');
				$this->Cell(90,4,$insre3,0,1,'L');
			}
			else
			{
				$this->Ln(20);
			}
			$this->Ln(2);
			$this->SetFont('Arial','B',10);
			$this->SetTextColor(0,80,0);
			$this->Cell(100,8,$titulo,0,1);
			$this->Ln(2);
			//linea 1
			$this->SetFont('Arial','B',7);
			$this->SetTextColor(0,0,0);
			$this->Cell(5,8,'N�',1,0,'L',1);
			$this->Cell(50,8,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(16,8,'C�dula',1,0,'C',1);
			$this->Cell(60,8,'Direcci�n y Tel�fono',1,0,'C',1);
			$this->Cell(30,4,'Resoluci�n',"TLR",0,'C',1);
			$this->Cell(78,8,'Cargo y Especialidad',1,0,'C',1);
			$this->Cell(20,4,'Tiempo Trab',"TLR",0,'C',1);
			$this->Cell(35,8,'Grados',1,0,'C',1);
			$this->Cell(13,4,"Horas","TLR",1,'C',1);
			//linea 2
			$this->Cell(131,4,'',0,0);
			$this->Cell(30,4,'Escalaf�n',"BLR",0,'C',1);
			$this->Cell(78,4,'',0,0);
			$this->Cell(20,4,'Calidad',"BLR",0,'C',1);
			$this->Cell(35,4,'',0,0);
			$this->Cell(13,4,"40 Min","BLR",1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$profasig = "y".$ano."profasig";
	$pdf=new PDF();
	$pdf->SetTitle("NOTAS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$titulo = "RELACION PERSONAL DIRECTIVO, DOCENTE Y ADMINISTRATIVO A�O ".$ano;
	$pdf->AddPage('L',$papel);
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','',7);
	$num = 0;
	foreach($datprof as $regprof)
	{
		$num = $num + 1;
		$id_profesor = $regprof->id_profesor;
                $pgrados = trim($regprof->grados);
                $pgrados = str_replace("-2", "PK", $pgrados);
                $pgrados = str_replace("-1", "K", $pgrados);
                $pgrados = str_replace(" 0", " T", $pgrados);
                $pgrados = str_replace(" ", ",", $pgrados);
                $escalafon = $regprof->escgrado;
                if ($escalafon=="")
                    $escalafon="SIN ESCALAF�N";
                else 
                    $escalafon="GRADO ".$escalafon;
                $fchinicio = $regprof->fchinicio;
                $fchfinal = $regprof->fchfinal;
                $tiempotra = tiempo_trabajado($fchinicio,$fchfinal);
                $tiempocar = tiempo_carga($ano,$id_profesor);
                $tiempocar = $tiempocar." H.SEM";
		//linea 1
		$pdf->Cell(5,8,$num,1,0,'L');
		$pdf->Cell(50,8,substr($regprof->apellidos." ".$regprof->nombres,0,32),1,0,'L');
		$pdf->Cell(16,8,$regprof->docunum,1,0,'L');
		$pdf->Cell(60,4,substr($regprof->direccion,0,43),'TLR',0,'L');
		$pdf->Cell(30,4,$regprof->escresol,'TLR',0,'L');
		$pdf->SetFont('Arial','',6);
		$pdf->Cell(78,4,substr($regprof->cargo,0,63),'TLR',0,'L');
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(20,4,$tiempotra,'TLR',0,'L');
		$pdf->Cell(35,8,$pgrados,1,0,'L');
		$pdf->Cell(13,8,$tiempocar,1,1,'C');
		//linea 2
		$pos=$pdf->GetY();
		$pos=$pos-4;
		$pdf->SetY($pos);
		$pdf->Cell(71,4,"",0,0);
		$pdf->Cell(60,4,$regprof->telefono,'BLR',0,'L');
		$pdf->Cell(30,4,$escalafon,'BLR',0,'L');
		$pdf->SetFont('Arial','',6);
		$pdf->Cell(78,4,substr($regprof->especialidad,0,60),'BLR',0,'L');
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(20,4,$regprof->tiempo,'BLR',1,'L');
	}
	$pdf->ln(10);
	$pdf->Cell(98,5,"_______________________________",0,0,'C');
	$pdf->Cell(98,5,"_______________________________",0,1,'C');
	$pdf->Cell(98,4,$rector,0,0,'C');
	$pdf->Cell(98,4,$secretaria,0,1,'C');
	$pdf->Cell(98,4,"RECTOR",0,0,'C');
	$pdf->Cell(98,4,"SECRETARIA",0,1,'C');
	$pdf->Output();
	disconnect();
?>
