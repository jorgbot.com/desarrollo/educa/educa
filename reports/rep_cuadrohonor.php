<?php
/***********************************************************************************************
* @file        : rep_alumnosch.php (ancho hoja 196)                                            *
* @brief       : cuadro de honor promedio alumnos por nivel, grago o curso                     *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 14-may-2012                                                    *
* @Modificado  : Nora Rodriguez 14-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$periodo = isset($_GET['periodo']) ? $_GET['periodo'] : null ;
$nivel = isset($_GET['nivel']) ? $_GET['nivel'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$implogo = $_GET["implogo"]; 
$papel = $_GET["papel"];
$exportar = "";
$titulo = "INFORME CUADRO DE HONOR";
$subtitulo = "A�O ".$ano." PERIODO ".$periodo;
connect();
$cursos = "y".$ano."cursos";
$sql="SELECT * FROM $cursos ";
$condi = "";
if ($nivel != "")
{ 
	$subtitulo.=" - NIVEL ".$nivel;
	$nomnivel = substr($nivel,1);
	$nivel = substr($nivel,0,1);
	$condi.="AND nivel = '$nivel' ";
	$subtitulo.=" - NIVEL ".$nomnivel;
}
if ($grado != "")
{
	$condi.="AND grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
if ($curso != "")
{
	$condi.="AND curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
if ($condi != "")
	$condi = "WHERE ".substr($condi,4);
$sql = $sql.$condi;
if ($periodo == 1)
	$sql.= "ORDER BY promedio1 desc, apellidos, nombres";
if ($periodo == 2)
	$sql.= "ORDER BY promedio2 desc, apellidos, nombres";
if ($periodo == 3)
	$sql.= "ORDER BY promedio3 desc, apellidos, nombres";
if ($periodo == 4)
	$sql.= "ORDER BY promedio4 desc, apellidos, nombres";
if ($periodo == 5)
	$sql.= "ORDER BY promedio desc, apellidos, nombres";
$datalum=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',6);
			$this->Cell(45,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(7,5,'ID',1,0,'C',1);
			$this->Cell(7,5,'Nivel',1,0,'C',1);
			$this->Cell(7,5,'Grado',1,0,'C',1);
			$this->Cell(7,5,'Curso',1,0,'C',1);
			$this->Cell(12,5,'Promedio',1,0,'C',1);
			$this->Cell(10,5,'Puesto',1,0,'C',1);
			$this->Cell(0,5,'Observaci�n',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$pdf->SetFont('Arial','',6);
	//lista de Alumnos
	$num = 0;
	$prom = 0;
	foreach($datalum as $regalum)
	{
		
		$num = $num +1;
		$id_alumno = $regalum->id_alumno;
		$promedio = $regalum->promedio;
		if ($periodo == 1)
			$promedio = $regalum->promedio1;
		if ($periodo == 2)
			$promedio = $regalum->promedio2;
		if ($periodo == 3)
			$promedio = $regalum->promedio3;
		if ($periodo == 4)
			$promedio = $regalum->promedio4;
		$pdf->Cell(45,5,substr($regalum->apellidos." ".$regalum->nombres,0,32),'LR',0,'L');
		$pdf->Cell(7,5,$regalum->id_alumno,'LR',0,'C');
		$pdf->Cell(7,5,$regalum->nivel,'LR',0,'C');
		$pdf->Cell(7,5,$regalum->grado,'LR',0,'C');
		$pdf->Cell(7,5,$regalum->curso,'LR',0,'C');
		$pdf->Cell(12,5,$promedio,'LR',0,'C');
		$pdf->Cell(10,5,$num,'LR',0,'C');
		$pdf->Cell(0,5,"",1,1);
		$prom = $prom+$promedio;
	}
	$prom=$prom/count($datalum); 
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(150,5," TOTAL ALUMNOS: ".count($datalum),1,0,'L',1);
	$pdf->Cell(0,5," PROMEDIO CURSO: ".round($prom,2 ),1,0,'L',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
