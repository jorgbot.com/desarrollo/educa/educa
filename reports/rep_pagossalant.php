<?php
/***********************************************************************************************
* @file        : rep_pagossalant.php (ancho hoja 196)                                          *
* @brief       : reporte saldos pendientes a�o anterior                                        *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 8-nov-2017                                                     *
* @Modificado  : Nora Rodriguez 8-nov-2017                                                     *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$exportar = isset($_GET['exportar']) ? $_GET['exportar'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$swretirados = isset($_GET['swretirados']) ? $_GET['swretirados'] : 0 ; //0=todos, 1=alumnos retirados
$swinteres = isset($_GET['swinteres']) ? $_GET['swinteres'] : 0 ; //liquidar interes
$swdeben = isset($_GET['swdeben']) ? $_GET['swdeben'] : 1 ; //solo alumnos que deben
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 0 ; //saltar pagina en cambio de curso
$fchcorte = fec_amd($fchcorte);
$mescorte = substr($fchcorte,5,2);
$swsalto = 0;
$anoant = $ano - 1;
connect();
$porcen = 0;
$sql="SELECT ano, interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
if ($tipo=="T")
{
	$titulo = "ALUMNOS A�O ".$ano."  - TRANSPORTES PENDIENTES A�O ".$anoant;
	$swinteres = 0;
}
else
{
	$tipo = "P";
	$titulo = "ALUMNOS A�O ".$ano." - PENSIONES PENDIENTES A�O ".$anoant;
}
$subtitulo = "CORTE A ".fecha_texto($fchcorte);
if ($swinteres > 0)
	$subtitulo = $subtitulo." (INCLUYE INTERES POR MORA)"; 
if ($swretirados > 0)
	$subtitulo = $subtitulo." ALUMNOS RETIRADOS"; 
//alumnos
$filepag = "y".$anoant."pagos";
$filedet = "y".$anoant."pagosdet";
$cursos = "y".$ano."cursos";
$sql="SELECT matriculas.id_alumno, matriculas.grado, matriculas.curso, matriculas.matricula, matriculas.estado, matriculas.fchretiro, alumnos.apellidos, alumnos.nombres FROM matriculas, alumnos WHERE matriculas.ano = '$ano' ";
if ($swretirados > 0)
	$sql.="AND matriculas.fchretiro!='0000-00-00'";
if ($grado != "")
	$sql.="AND matriculas.grado = '$grado' ";
if ($curso != "")
	$sql.="AND matriculas.curso = '$curso' ";
$sql.="AND matriculas.id_alumno = alumnos.id_alumno ORDER BY CAST(matriculas.grado AS SIGNED), matriculas.curso, alumnos.apellidos, alumnos.nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(10,5,'Curso',1,0,'C',1);
			$this->Cell(44,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(8,5,'ID',1,0,'C',1);
			for($i=2; $i<=11 ;$i++)
			{
				if ($i <= 9)
					$mes = "0".$i;
				else
					$mes = $i;
				$this->Cell(12,5,substr(strtoupper(nombre_mes($mes)),0,3),1,0,'C',1);
			}			
			$this->Cell(14,5,"TOTAL",1,1,'C',1);
			$this->SetFont('Arial','',7);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("SALDOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$totcur = array();
	$totfin = array();
	for($i=2; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$totcur[$mes] = 0;
		$totfin[$mes] = 0;
	}			
	//Lista de Alumnos
	$numalu = 0;
	$acumcur = 0;			
	$acumfin = 0;			
	$curso = "";
	foreach($datalum as $regalum)
	{
		if ($swsalto > 0 and $curso!=$regalum->curso) //saltar hoja por curso
		{
			if ($curso!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',7);
				$pdf->Cell(62,5,"ALUMNOS ".$numalu,1,0,'L',1);
				for($i=2; $i<=11 ;$i++)
				{
					if ($i <= 9)
						$mes = "0".$i;
					else
						$mes = $i;
					$pdf->Cell(12,5,number_format($totcur[$mes],0),1,0,'R',1);					
					$totcur[$mes] = 0;
				}
				$pdf->Cell(14,5,number_format($acumcur,0),1,1,'R',1);
				$pdf->AddPage('P',$papel);			
			}
			$curso = $regalum->curso;
			$acumcur = 0;
			$numalu = 0;
		}
		//cargar valores alumno
		$estado= $regalum->estado;
		$id_alumno = $regalum->id_alumno;
                $mostrar = 0;
                $sql = "SELECT id_matricula FROM matriculas where id_alumno='$id_alumno' AND ano = '$anoant' LIMIT 1";
                $datmatr=viewsql($sql);
                if (count($datmatr) > 0)
                    $mostrar = 1;
                if ($mostrar==1 and $estado!='PENDIENTE')
                {
                    if ($tipo=="T")
                            $valores = valores_transporte($id_alumno,$anoant);
                    else
                            $valores = valores_pension($id_alumno,$anoant);
                    $pagos = array();
                    $saldos = array();
                    if ($swinteres > 0)
                            $salint = array();
                    $saldotot = 0;
                    for($i=1; $i<=11 ;$i++)
                    {
                            if ($i <= 9)
                                    $mes = "0".$i;
                            else
                                    $mes = $i;
                            $pagos[$mes] = 0;
                            $saldos[$mes] = 0;
                            if ($swinteres > 0)
                                    $salint[$mes] = "";
                    }
                    $sql="SELECT id_pago, mes, valor, interes, interesliq, interesaju FROM $filedet WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' AND ano = '$anoant' ";
                    $datpag = viewsql($sql);
                    foreach($datpag as $regpag)
                    {
                            if (intval($regpag->mes) <= 9)
                                    $mes = "0".$regpag->mes;
                            else
                                    $mes = $regpag->mes;
                            $pagos[$mes] = $pagos[$mes] + intval($regpag->valor);
                            if ($swinteres > 0)
                            {
                                    $sint = intval($regpag->interesliq) - intval($regpag->interes) - intval($regpag->interesaju);
                                    $salint[$mes] = $salint[$mes] + $sint;
                            }
                    }
                    //calcular saldos
                    for($i=1; $i<=11 ;$i++)
                    {
                            if ($i <= 9)
                                    $mes = "0".$i;
                            else
                                    $mes = $i;
                            $saldo = $valores[$mes] - $pagos[$mes];
                            $interes = 0;
                            if ($i > $mescorte)
                                    $saldo = 0;
                            if ($tipo=="P" and $swinteres > 0 and $porcen > 0)
                            {
                                    $interes = 0;
                                    if ($saldo > 0)
                                            $interes = interes_pension($ano,$mes,$saldo,$porcen,$fchcorte);
                                    $saldo = $saldo + $salint[$mes] + $interes;
                            }
                            $saldos[$mes] = $saldo;
                            $saldotot = $saldotot + $saldo;			
                    }
                    if ($swdeben == 0 or ($swdeben==1 and $saldotot > 500))
                    {
                            $numalu = $numalu + 1;
                            $pdf->SetFont('Arial','',7);
                            $pdf->Cell(10,5,$regalum->grado." ".$regalum->curso,0,0,'L');
                            $pdf->Cell(44,5,substr($regalum->apellidos." ".$regalum->nombres,0,28),0,0,'L');
                            $pdf->Cell(8,5,$regalum->id_alumno,0,0,'L');
                            for($i=2; $i<=11 ;$i++)
                            {
                                    if ($i <= 9)
                                            $mes = "0".$i;
                                    else
                                            $mes = $i;
                                      $pdf->Cell(12,5,number_format($saldos[$mes],0),0,0,'R');
                                      $acumcur = $acumcur + $saldos[$mes];			
                                      $acumfin = $acumfin + $saldos[$mes];			
                                      $totcur[$mes] = $totcur[$mes] + $saldos[$mes];
                                      $totfin[$mes] = $totfin[$mes] + $saldos[$mes];
                            }			
                            $pdf->Cell(14,5,number_format($saldotot,0),0,1,'R');
                            if ($swretirados > 0)
                                    $pdf->Cell(52,4,fecha_texto($regalum->fchretiro),0,1,'R');
                    }
                }
	}
	$pdf->SetFont('Arial','B',7);
		$pdf->Cell(62,5,"ALUMNOS ".$numalu,1,0,'L',1);
		for($i=2; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			if ($swsalto > 0)
				$pdf->Cell(12,5,number_format($totcur[$mes],0),1,0,'R',1);
			else
				$pdf->Cell(12,5,number_format($totfin[$mes],0),1,0,'R',1);
		}
		if ($swsalto > 0)
			$pdf->Cell(14,5,number_format($acumcur,0),1,1,'R',1);
		else
			$pdf->Cell(14,5,number_format($acumfin,0),1,1,'R',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
