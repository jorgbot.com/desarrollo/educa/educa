<?php
/***********************************************************************************************
* @file        : rep_ENTRADA.php (ancho hoja 196)                                              *
* @brief       : reporte entrada de pagos                                                      *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 17-abr-2014                                                    *
* @Modificado  : Nora Rodriguez 17-abr-2014                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : null ;
$id_entrada = isset($_GET['id_entrada']) ? $_GET['id_entrada'] : null ;
$fchentrada = isset($_GET['fchentrada']) ? $_GET['fchentrada'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
$exportar = "";
$titulo = "HOJA DE ENTRADA N� ".$id_entrada." - ".fecha_texto($fchentrada);
if ($tipo=="T")
	$subtitulo = "PAGOS TRANSPORTE A�O ".$ano;
else
	$subtitulo = "PAGOS PENSIONES A�O ".$ano;
connect();
$filedet = "y".$ano."pagosdet";
$filepag = "y".$ano."pagos";
$campos="SELECT $filepag.*, alumnos.apellidos, alumnos.nombres FROM $filepag, alumnos ";
$sql="WHERE $filepag.id_entrada = '$id_entrada' AND $filepag.id_alumno = alumnos.id_alumno ";
if ($tipo=="T")
	$sql.="AND $filepag.tipo = 'T' ";
else
	$sql.="AND $filepag.tipo = 'P' ";
$sql=$campos.$sql."ORDER BY alumnos.apellidos, alumnos.nombres ";
$datpag=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$tipo = $GLOBALS["tipo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(50,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(10,5,'IdAlum',1,0,'C',1);
			$this->Cell(12,5,'Curso',1,0,'C',1);
			$this->Cell(10,5,'IdPago',1,0,'C',1);
			$this->Cell(18,5,'FchConsig',1,0,'C',1);
			if ($tipo=="P")
				$this->Cell(18,5,'Pensi�n',1,0,'C',1);
			else
				$this->Cell(18,5,'Transporte',1,0,'C',1);
			$this->Cell(18,5,'Interes',1,0,'C',1);
			$this->Cell(18,5,'Consignado',1,0,'C',1);
			$this->Cell(16,5,'Efectivo',1,0,'C',1);
			$this->Cell(18,5,'Total',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("PAGOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//lista de Alumnos
	$num = 0;
	$totcon = 0;
	$totefe = 0;
	$totnet = 0;
	$totint = 0;
	$totval = 0;
	$netocon = 0;
	$netoefe = 0;
	$intecon = 0;
	$inteefe = 0;
	foreach($datpag as $regpag)
	{
		$num = $num +1;
		$totcon = $totcon + $regpag->valor_con;
		$totefe = $totefe + $regpag->valor_efe;
		$totnet = $totnet + $regpag->neto;
		$totint = $totint + $regpag->interes;
		$totval = $totval + $regpag->valor;
		if ($regpag->id_banco <= 0)
		{
			$netoefe = $netoefe + $regpag->neto;
			$inteefe = $inteefe + $regpag->interes;
		}
		else
		{
			$netocon = $netocon + $regpag->neto;
			$intecon = $intecon + $regpag->interes;
		}
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(50,5,substr($regpag->apellidos." ".$regpag->nombres,0,32),'LR',0,'L');
		$pdf->Cell(10,5,$regpag->id_alumno,'LR',0);
		$pdf->Cell(4,5,$regpag->grado,'LR',0,'C');
		$pdf->Cell(8,5,$regpag->curso,'LR',0,'C');
		$pdf->Cell(10,5,$regpag->id_pago,'LR',0,'L');
		$pdf->Cell(18,5,fecha_texto($regpag->fchpago),'LR',0,'L');
		$pdf->Cell(18,5,number_format($regpag->neto,0),'LR',0,'R');
		$pdf->Cell(18,5,number_format($regpag->interes,0),'LR',0,'R');
		$pdf->Cell(18,5,number_format($regpag->valor_con,0),'LR',0,'R');
		$pdf->Cell(16,5,number_format($regpag->valor_efe,0),'LR',0,'R');
		$pdf->Cell(18,5,number_format($regpag->valor,0),'LR',1,'R');
	}
	$pdf->SetFont('Arial','B',7);
	if ($tipo=="P")
	{
		$pdf->Cell(20,4,"PAGOS: ".$num,'LT',0,'L',1);
		$pdf->Cell(80,4,"TOTALES:",'TR',0,'R',1);
		$pdf->Cell(18,4,number_format($totnet,0),'LTR',0,'R',1);
		$pdf->Cell(18,4,number_format($totint,0),'LTR',0,'R',1);
		$pdf->Cell(18,4,number_format($totcon,0),'LTR',0,'R',1);
		$pdf->Cell(16,4,number_format($totefe,0),'LTR',0,'R',1);
		$pdf->Cell(18,4,number_format($totval,0),'LTR',1,'R',1);
		$pdf->Cell(100,4,"CONSIGNADO:",'LR',0,'R',1);
		$pdf->Cell(18,4,number_format($netocon,0),'LR',0,'R',1);
		$pdf->Cell(18,4,number_format($intecon,0),'LR',0,'R',1);
		$pdf->Cell(18,4,"",'LR',0,'R',1);
		$pdf->Cell(16,4,"",'LR',0,'R',1);
		$pdf->Cell(18,4,"",'LR',1,'R',1);
		$pdf->Cell(100,4,"EFECTIVO:",'LRB',0,'R',1);
		$pdf->Cell(18,4,number_format($netoefe,0),'LRB',0,'R',1);
		$pdf->Cell(18,4,number_format($inteefe,0),'LRB',0,'R',1);
		$pdf->Cell(18,4,"",'LRB',0,'R',1);
		$pdf->Cell(16,4,"",'LRB',0,'R',1);
		$pdf->Cell(18,4,"",'LRB',1,'R',1);
	}
	else
	{
		$pdf->Cell(100,5,"PAGOS: ".$num,1,0,'L',1);
		$pdf->Cell(18,5,number_format($totnet,0),1,0,'R',1);
		$pdf->Cell(18,5,number_format($totint,0),1,0,'R',1);
		$pdf->Cell(18,5,number_format($totcon,0),1,0,'R',1);
		$pdf->Cell(16,5,number_format($totefe,0),1,0,'R',1);
		$pdf->Cell(18,5,number_format($totval,0),1,1,'R',1);
	}
	//relaciion de ajustes intereses
	if ($tipo=="P")
	{
	
		$sql="SELECT $filepag.id_pago, $filepag.id_alumno, $filepag.grado, $filepag.curso, $filedet.mes, $filedet.interesaju, alumnos.apellidos, alumnos.nombres FROM $filepag, $filedet, alumnos WHERE $filepag.id_entrada = '$id_entrada' AND $filedet.interesaju > '0' ";
		$sql.="AND $filepag.id_pago = $filedet.id_pago AND $filepag.id_alumno = alumnos.id_alumno ORDER BY $filepag.id_entrada, alumnos.apellidos, alumnos.nombres";
		$datpag=viewsql($sql);
		if (count($datpag) > 0)
		{
			$pdf->ln();
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(20,5,"AJUSTES INTERESES",0,1,'L');
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(50,5,'Apellidos y Nombres',1,0,'L',1);
			$pdf->Cell(10,5,'IdAlum',1,0,'C',1);
			$pdf->Cell(12,5,'Curso',1,0,'C',1);
			$pdf->Cell(10,5,'IdPago',1,0,'C',1);
			$pdf->Cell(8,5,"Mes",1,0,'C',1);
			$pdf->Cell(18,5,'Ajustes',1,1,'C',1);
			$num = 0;
			$totval = 0;
			foreach($datpag as $regpag)
			{
				$num = $num +1;
				$totval = $totval + $regpag->interesaju;
				$pdf->Cell(50,5,substr($regpag->apellidos." ".$regpag->nombres,0,32),'LR',0,'L');
				$pdf->Cell(10,5,$regpag->id_alumno,'LR',0);
				$pdf->Cell(4,5,$regpag->grado,'LR',0,'C');
				$pdf->Cell(8,5,$regpag->curso,'LR',0,'C');
				$pdf->Cell(10,5,$regpag->id_pago,'LR',0,'L');
				$pdf->Cell(8,5,substr(strtoupper(nombre_mes($regpag->mes)),0,3),'LR',0,'L');
				$pdf->Cell(18,5,number_format($regpag->interesaju,0),'LR',1,'R');
			}
			$pdf->Cell(90,5,"AJUSTES: ".$num,1,0,'L',1);
			$pdf->Cell(18,5,number_format($totval,0),1,1,'R',1);
		}
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
