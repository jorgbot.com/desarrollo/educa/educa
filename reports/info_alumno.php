<?php
/***********************************************************************************************
* @file        : matricula.php (ancho hoja 196)                                                *
* @brief       : Imprime hoja de matricula en PDF                                              *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 17-sep-2015                                                    *
* @Modificado  : Nora Rodriguez 17-sep-2015                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : 0 ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] :  $_SESSION["anoaca"] ;
$papel = "LETTER";
$titulo = "HOJA DE DATOS POR ALUMNO";
$exportar = "";
connect();
//parametros institucion
$sql="select instinom, instisig, nit, ciudad, direccion, telefono from institucion limit 1";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$insnom = $reginst->instinom;
	$inssig = $reginst->instisig;
	$insnit = $reginst->nit;
	$insciu = $reginst->ciudad;
	$insdir = $reginst->direccion;
	$instel = $reginst->telefono;
}
if ($id_alumno <= 0) //lote de alumnos por cursos del a�o anterior
{
	$anoant = $ano;
	$ano = $ano;
	$cursos = "y".$anoant."cursos";
	$sql="SELECT id_alumno FROM $cursos ";
	if ($grado != "")
		$sql.="WHERE grado = '$grado' ";
	if ($curso != "")
		$sql.="AND curso = '$curso' ";
	$sql.="ORDER BY abs(grado), curso, apellidos, nombres";
}
else //un solo alumno por la matricula del a�o
{
	$anoant = $ano - 1;	
	$sql="SELECT id_alumno FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
}
$datcur=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		function Header()
		{
		}
		function Footer()
		{
		}
	}
	//iniciar documento PDF
	$gradonom = "";
	$pdf=new PDF();
	$pdf->SetTitle("MATRICULA");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	foreach($datcur as $regcur)
	{
		$id_alumno = $regcur->id_alumno;
		//datos matricula alumno
		$sql="SELECT matriculas.id_matricula, matriculas.grado, matriculas.curso, matriculas.fecha, matriculas.folio, matriculas.id_respmatri, matriculas.id_resppagos, alumnos.nombres, alumnos.apellidos, alumnos.docunum, alumnos.docutip, alumnos.docuexp, alumnos.fchnace, alumnos.lugnace, alumnos.genero, alumnos.procegrado, alumnos.proceinsti FROM matriculas, alumnos WHERE ";
		$sql.="matriculas.id_alumno = '$id_alumno' AND matriculas.ano = '$ano' AND matriculas.id_alumno = alumnos.id_alumno LIMIT 1";
		$datalum=viewsql($sql);
		foreach($datalum as $regalum)
		{
			$id_respmatri =  $regalum->id_respmatri;
			$id_resppagos =  $regalum->id_resppagos;
			if (intval($regalum->grado)!=intval($grado))
			{
				$grado = $regalum->grado;
				$sql="select gradonom from grados where grado='$grado' limit 1";
				$dat=viewsql($sql);
				foreach($dat as $reg)
				{$gradonom = $reg->gradonom;}
			}
			$pdf->AddPage('P',$papel);
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(100,6,$titulo,0,1);
			$pdf->Cell(36);
			$pdf->Cell(50,6," ",0,1);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(36);
			$pdf->Cell(50,6," ",0,1);
			$pdf->Cell(36);
			$pdf->Cell(50,6);
			//tabla matricula
			$pos=$pdf->GetY();
			$pos=$pos-20;
			$pdf->SetY($pos);
			$pdf->SetDrawColor(0,128,0);
			$pdf->SetFillColor(0,128,0);
			$pdf->SetTextColor(255,255,255);
			$pdf->SetFont('Arial','B',12);
			$pdf->Cell(146);
			$pdf->Ln(10);
			//ALUMNO
			$pdf->SetFont('Arial','B',8);
			$pdf->SetFillColor(255,255,255);
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(15,3,"",0,0);
			$pdf->Cell(40,4,"DATOS ESTUDIANTE",1,1,'C',1);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFillColor(224,231,233);
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(15,3,"",0,0);
			$pdf->Cell(80,3,"Apellidos y Nombres",'LTR',0,'L',1);
			$pdf->Cell(24,3,"Fecha Nacimiento",'LTR',0,'C',1);
			$pdf->Cell(44,3,"Lugar de Nacimiento",'LTR',0,'C',1);
			$pdf->Cell(11,3,"Edad",'LTR',0,'C',1);
			$pdf->Cell(11,3,"G�nero",'LTR',0,'C',1);
			$pdf->Cell(11,3,"C�digo",'LTR',1,'C',1);
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(15,3,"",0,0);
			$pdf->Cell(80,6,$regalum->apellidos." ".$regalum->nombres,'LRB',0);
			$pdf->Cell(24,6,fecha_texto($regalum->fchnace),'LRB',0,'C');
			$pdf->Cell(44,6,$regalum->lugnace,'LRB',0);
			$edad = calculo_edad($regalum->fchnace);
			$pdf->Cell(11,6,$edad,'LRB',0,'C');
			$pdf->Cell(11,6,$regalum->genero,'LRB',0,'C');
			$pdf->Cell(11,6,$id_alumno,'LRB',1,'C');
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(15,3,"",0,0);
			$pdf->Cell(30,3,"Identificaci�n N�mero",'LTR',0,'L',1);
			$pdf->Cell(15,3,"Tipo",'LTR',0,'C',1);
			$pdf->Cell(50,3,"Lugar de Expedici�n",'LTR',0,'C',1);
			$pdf->Cell(86,3,"Matriculado en el Grado",'LTR',1,'C',1);
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(15,3,"",0,0);
			$pdf->Cell(30,6,$regalum->docunum,'LRB',0);
			$pdf->Cell(15,6,$regalum->docutip,'LRB',0,'C');
			$pdf->Cell(50,6,$regalum->docuexp,'LRB',0);
			$pdf->Cell(86,6,$gradonom." (".$regalum->grado.")",'LRB',1,'C');
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(15,3,"",0,0);
			$pdf->Cell(151,3,"Colegio de Procedencia",'LTR',0,'L',1);
			$pdf->Cell(30,3,"Grado Procedencia",'LTR',1,'C',1);
			$pdf->SetFont('Arial','',8);
			$proceinsti = $regalum->proceinsti;
			$procegrado = $regalum->procegrado;
			$procegradonom = "";
			$sql="select grado from matriculas where id_alumno = '$id_alumno' and ano = '$anoant' limit 1";
			$dat=viewsql($sql);
			foreach($dat as $reg)
			{
				$procegrado = $reg->grado;
				$proceinsti = $insnom." ".$inssig;
			}
			$sql="select gradonom from grados where grado='$procegrado' limit 1";
			$dat=viewsql($sql);
			foreach($dat as $reg)
			{$procegradonom = $reg->gradonom;}
			$pdf->Cell(15,3,"",0,0);
			$pdf->Cell(151,6,$proceinsti,'LRB',0);
			$pdf->Cell(30,6,$procegradonom." (".$procegrado.")",'LRB',1,'C');
			//DATOS FAMILIARES
			for($i=1; $i<=4 ;$i++)
			{
				$tipo = "MADRE";
				if ($i==2)
					$tipo = "PADRE";
				$sql="select apellidos, nombres, docunum, docutip, docuexp, direccion, barrio, estrato, telefono, telefofi, celular, correo, profesion from familiares where id_alumno = '$id_alumno' and tipo = '$tipo' limit 1";
				if ($i==3)
				{
					$tipo = "ACUDIENTE";
					$sql="select apellidos, nombres, docunum, docutip, docuexp, direccion, barrio, estrato, telefono, telefofi, celular, correo, profesion from familiares where id_alumno = '$id_alumno' and id_familiar = '$id_respmatri' LIMIT 1";
				}
					if ($i==4)
				{
					$tipo = "ENCARGADO PAGOS";
					$sql="select apellidos, nombres, docunum, docutip, docuexp, direccion, barrio, estrato, telefono, telefofi, celular, correo, profesion from familiares where id_alumno = '$id_alumno' and id_familiar = '$id_resppagos' LIMIT 1";
				}
				$pdf->Cell(3,3,"",0,1);
				$pdf->SetFont('Arial','B',8);
				$pdf->SetFillColor(250,250,250);
				$pdf->SetTextColor(0,0,0);
				$pdf->Cell(15);
				$pdf->Cell(50,4,"DATOS ".$tipo,1,1,'C',1);
				$pdf->SetTextColor(0,0,0);
				$pdf->SetFillColor(224,231,233);
				$pdf->SetFont('Arial','',7);
				$pdf->Cell(15);
				$pdf->Cell(76,3,"Nombres y Apellidos",'LTR',0,'L',1);
				$pdf->Cell(50,3,"Identificaci�n",'LTR',0,'C',1);
				$pdf->Cell(55,3,"Profesi�n",'LTR',1,'C',1);
				$apellidos = "";
				$nombres = "";
				$docunum = "";
				$docutip = "";
				$docuexp = "";
				$direccion = "";
				$barrio = "";
				$estrato = "";
				$telefono = "";
				$telefofi = "";
				$celular = "";
				$profesion = "";
				$correo = "";
				$dat=viewsql($sql);
				foreach($dat as $reg)
				{
					$apellidos = $reg->apellidos;
					$nombres = $reg->nombres;
					$docunum = $reg->docunum;
					$docutip = $reg->docutip;
					$docuexp = $reg->docuexp;
					$direccion = $reg->direccion;
					$barrio = $reg->barrio;
					$estrato = $reg->estrato;
					$telefono = $reg->telefono;
					$telefofi = $reg->telefofi;
					$celular = $reg->celular;
					$profesion = $reg->profesion;
					$correo = $reg->correo;
				}
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(15);
				$pdf->Cell(76,6,$apellidos." ".$nombres,'LRB',0);
				$pdf->Cell(50,6,$docutip." ".$docunum." ".substr($docuexp,0,16),'LRB',0);
				$pdf->Cell(55,6,$profesion,'LRB',1);
				$pdf->SetFont('Arial','',7);
				$pdf->Cell(15);
				$pdf->Cell(115,3,"Direcci�n",'LTR',0,'C',1);
				$pdf->Cell(56,3,"Barrio",'LTR',0,'C',1);
				$pdf->Cell(10,3,"Estrato",'LTR',1,'C',1);
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(15);
				$pdf->Cell(115,6,$direccion,'LRB',0);
				$pdf->Cell(56,6,$barrio,'LRB',0);
				$pdf->Cell(10,6,$estrato,'LRB',1,'C');
				$pdf->SetFont('Arial','',7);
				$pdf->Cell(15);
				$pdf->Cell(41,3,"Celular",'LTR',0,'C',1);
				$pdf->Cell(20,3,"Tel�fono Casa",'LTR',0,'C',1);
				$pdf->Cell(35,3,"Tel�fono Oficina",'LTR',0,'C',1);
				$pdf->Cell(85,3,"Correo",'LTR',1,'C',1);
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(15);
				$pdf->Cell(41,6,$celular,'LRB',0);
				$pdf->Cell(20,6,$telefono,'LRB',0);
				$pdf->Cell(35,6,$telefofi,'LRB',0);
				$pdf->Cell(85,6,$correo,'LRB',1);
			}
			//ACEPTACION
			
		}//fin Alumno
	}//fin cursos
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
