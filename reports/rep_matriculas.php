<?php
/***********************************************************************************************
* @file        : rep_matriculas.php (ancho hoja 196)                                           *
* @brief       : lista general de alumnos matriculados por grado y/o curso                     *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 17-abr-2014                                                    *
* @Modificado  : Nora Rodriguez 17-abr-2014                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$matricula = isset($_GET['matricula']) ? $_GET['matricula'] : null ;
$estado = isset($_GET['estado']) ? $_GET['estado'] : null ;
$genero = isset($_GET['genero']) ? $_GET['genero'] : null ;
$tipoedu = isset($_GET['tipoedu']) ? $_GET['tipoedu'] : null ;
$swnuevos = isset($_GET['swnuevos']) ? $_GET['swnuevos'] : 0 ; //solo alumnos nuevos
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 1 ; //saltar pagina en cambio de curso
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$titulo = "ALUMNOS MATRICULADOS";
$subtitulo = "A�O ".$ano;
$antano = $ano - 1;
connect();
$sql="SELECT alumnos.id_alumno,alumnos.apellidos,alumnos.nombres,alumnos.docutip,alumnos.docunum,alumnos.docuexp,alumnos.fchnace,alumnos.fchingreso,alumnos.fchretiro,alumnos.tipoedu,alumnos.genero,matriculas.ano,matriculas.grado,matriculas.curso,matriculas.estado,matriculas.fecha,matriculas.folio FROM alumnos, matriculas ".
"WHERE alumnos.id_alumno = matriculas.id_alumno AND matriculas.ano = '$ano' ";
if ($grado != "")
{
	$sql.="AND matriculas.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
if ($curso != "")
{
	$sql.="AND matriculas.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
if ($matricula != "")
{
	$sql.="AND matriculas.estado = '$matricula' ";
	$subtitulo.=" - MATRICULA ".$matricula;
}
if ($estado != "")
{
	$sql.="AND alumnos.estado = '$estado' ";
	$subtitulo.=" ESTADO ".$estado;
}
if ($tipoedu != "")
{
	$subtitulo.=" EDUCACION ".$tipoedu;
	$tipoedu = substr($tipoedu,0,1);
	$sql.="AND alumnos.tipoedu = '$tipoedu' ";
}
if ($genero != "")
{
	$subtitulo.=" GENERO ".$genero;
	$genero = substr($genero,0,1);
	$sql.="AND alumnos.genero = '$genero' ";
}
if ($swnuevos > 0)
	$subtitulo.=" NUEVOS ALUMNOS";
$sql.= "ORDER BY CAST(matriculas.grado AS SIGNED), matriculas.curso, alumnos.apellidos, alumnos.nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(10,5,'Curso',1,0,'C',1);
			$this->Cell(55,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(8,5,'ID',1,0,'C',1);
			$this->Cell(40,5,'Identifcaci�n',1,0,'C',1);
			$this->Cell(4,5,'G',1,0,'C',1);
			$this->Cell(7,5,'Edad',1,0,'C',1);
			$this->Cell(21,5,'Ingreso',1,0,'C',1);
			$this->Cell(28,5,'Matricula',1,0,'C',1);
			$this->Cell(15,5,'Retiro',1,0,'C',1);
			$this->Cell(8,5,'Folio',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//lista de Alumnos
	$num = 0;
	foreach($datalum as $regalum)
	{
		if ($swsalto > 0 and $curso!=$regalum->curso) //saltar hoja por curso
		{
			if ($curso!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',6);
				$pdf->Cell(0,5,"TOTAL ALUMNOS: ".$num,1,1,'L',1);
				$pdf->AddPage('P',$papel);			
			}
			$curso = $regalum->curso;
			$num = 0;
		}
		
		$id_alumno = $regalum->id_alumno;
		$nuevo = "NUE";
		$sql = "SELECT id_matricula FROM matriculas WHERE id_alumno = '$id_alumno' AND matriculas.ano = '$antano' LIMIT 1";
		$datmatr = viewsql($sql);
		if (count($datmatr) > 0)
			$nuevo = "";
		if ($swnuevos <= 0 or ($swnuevos==1 and $nuevo=="NUE"))
		{
			$num = $num + 1;
			$edad = calculo_edad($regalum->fchnace);
			$pdf->SetFont('Arial','',6);
			$pdf->Cell(4,5,$regalum->grado,'L',0,'C');
			$pdf->Cell(6,5,$regalum->curso,'R',0,'C');
			$pdf->Cell(55,5,$regalum->apellidos." ".$regalum->nombres,'R',0,'L');
			$pdf->Cell(8,5,$regalum->id_alumno,'R',0,'L');
			$pdf->Cell(7,5,$regalum->docutip,'R',0,'L');
			$pdf->Cell(15,5,$regalum->docunum,'R',0,'L');
			$pdf->Cell(18,5,substr($regalum->docuexp,0,14),'R',0,'L');
			$pdf->Cell(4,5,$regalum->genero,'.R',0,'C');
			$pdf->Cell(7,5,$edad,'R',0,'C');
			$pdf->Cell(15,5,fecha_texto($regalum->fchingreso),'R',0,'C');
			$pdf->Cell(6,5,$nuevo,'R',0,'C');
			$pdf->Cell(13,5,$regalum->estado,'R',0,'L');
			$pdf->Cell(15,5,fecha_texto($regalum->fecha),'R',0,'C');
			$pdf->Cell(15,5,fecha_texto($regalum->fchretiro),'R',0,'C');
			$pdf->Cell(8,5,$regalum->folio,'R',1,'L');
		}
	}
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(0,5,"TOTAL ALUMNOS: ".$num,1,1,'L',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
