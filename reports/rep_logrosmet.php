<?php
/***********************************************************************************************
* @file        : rep_logros.php (ancho hoja 196)                                               *
* @brief       : lista de asignaturas, logros y metodologia de logros                          *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 23-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$periodo = isset($_GET['periodo']) ? $_GET['periodo'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
$id_logro = isset($_GET['id_logro']) ? $_GET['id_logro'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($implogo == "")
	$implogo = "";
if ($papel == "")
	$papel = "Letter";
connect();
$semanas = semanas_academicas($ano);
//porcentaje perdida de fallas
$sql = "select fallasper from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{$fallasper = $reganol->fallasper;}
$titulo = "LOGROS METODOLOGIA Y ACTIVIDADES";
$subtitulo = "A�O ".$ano;
if ($grado != "")
	$subtitulo.=" - GRADO ".$grado;
if ($periodo > 0)
	$subtitulo.=" - PERIODO ".$periodo;
$logros = "y".$ano."logros";
$actividades = "y".$ano."activ";
$metodologias = "y".$ano."metod";
$profasig = "y".$ano."profasig";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
		$this->SetFont('Arial','B',6);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("ASIGNATURAS");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
//asignaturas
$sql = "select * from asignaturas where ano = '$ano' and grado = '$grado' ORDER BY area, asignatura";
if ($id_asignatura > 0)
	$sql = "select * from asignaturas where id_asignatura = '$id_asignatura' LIMIT 1";
$datasig=viewsql($sql);
$pdf->SetFont('Arial','',7);
$id_area = "";
foreach($datasig as $regasig)
{
	$id_asignatura = $regasig->id_asignatura;
	$docentes = "";
	$sql="SELECT profesor FROM $profasig WHERE id_asignatura = '$id_asignatura' GROUP BY id_profesor order by PROFESOR";
	$datprof=viewsql($sql);
	foreach($datprof as $regprof)
	{$docentes = $docentes.$regprof->profesor." ";}
	$sql = "SELECT id_logro, logrocod, logro, periodos FROM $logros WHERE grado = '$grado' and id_asignatura = '$id_asignatura' ";
	if ($periodo > 0)
		$sql.="AND periodos LIKE '% $periodo %' ";
	$sql.="ORDER BY logrocod";
	if ($id_logro > 0)
		$sql = "select id_logro, logrocod, logro, periodos FROM $logros WHERE id_asignatura = '$id_asignatura' and id_logro = '$id_logro' LIMIT 1";
	$datlogr = viewsql($sql);
	foreach($datlogr as $reglogr)
	{
		$id_logro = $reglogr->id_logro;
		$pdf->SetFont('Arial','B',7);
		$pdf->Cell(20,6,"ASIGNATURA",1,0,'L',1);
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(86,6,$regasig->asignatura,1,0,'L');
		$pdf->Cell(70,6,"AREA ".$regasig->area,1,0,'L');
		$pdf->Cell(10,6,"%".$regasig->porcentaje,1,0,'C');
		$pdf->Cell(0,6,"I.H ".$regasig->intensidad,1,1,'C');
		$pdf->SetFont('Arial','B',7);
		$pdf->Cell(20,6,"DOCENTE(S)",1,0,'L',1);
		$pdf->SetFont('Arial','',7);
		$pdf->MultiCell(176,6,$docentes,1,'J');
		$pdf->SetFont('Arial','B',7);
		$inipos = $pdf->GetY(); 
		$pdf->Cell(20,8,"LOGRO ".$reglogr->logrocod,1,0,'L',1);
		$pdf->Cell(0,8,"",1,0,'L');
		$pdf->SetFont('Arial','',7);
		$pdf->SetX(30);
		$pdf->MultiCell(0,4,$reglogr->logro,0,'J');
		$pdf->SetY($inipos+8);
		//METODOLOGIA
		$pdf->ln();
		$pdf->SetFont('Arial','B',7);
		$pdf->Cell(12,5,"FECHA",1,0,'C',1);
		$pdf->Cell(12,5,"CLASES",1,0,'C',1);
		$pdf->Cell(125,5,"METODOLOGIA",1,0,'C',1);
		$pdf->Cell(0,5,"RECURSOS",1,1,'C',1);
		$pdf->SetFont('Arial','',7);
		$sql = "SELECT * FROM $metodologias WHERE id_logro = '$id_logro' ORDER BY fecha";
		$datmeto = viewsql($sql);
		foreach($datmeto as $regmeto)
		{
			$pdf->Cell(24,4,"",0,0);
			$inipos = $pdf->GetY(); 
			$pdf->MultiCell(125,4,$regmeto->metodologia,1,'J');
			$finpos = $pdf->GetY();
			$dif = $finpos - $inipos;
			$pdf->SetY($inipos);
			$pdf->Cell(12,$dif,"",1,0);
			$pdf->Cell(12,$dif,"",1,0);
			$pdf->Cell(125,$dif,"",0,0);
			$pdf->Cell(0,$dif,"",1,0);
			$pdf->SetX(10);
			$pdf->Cell(12,4,substr(fecha_texto($regmeto->fecha),0,6),0,0);
			$pdf->Cell(12,4,$regmeto->numclases,0,0,'C');
			$pdf->Cell(125,4,"",0,0);
			$pdf->MultiCell(0,4,$regmeto->recursos,0,'J');
			$pdf->SetY($finpos);
		}
		//ACTIVIDADES
		$pdf->ln();
		$pdf->SetFont('Arial','B',7);
		$pdf->Cell(20,5,"CODIGO",1,0,'C',1);
		$pdf->Cell(166,5,"ACTIVIDAD EVALUADA",1,0,'C',1);
		$pdf->Cell(0,5,"%",1,1,'C',1);
		$pdf->SetFont('Arial','',7);
		$sql = "SELECT id_actividad, actividad, porcentaje, codigo, numero FROM $actividades WHERE id_logro = '$id_logro' ";
		if ($periodo > 0)
			$sql.="AND periodo = '$periodo' ";
		$sql.="ORDER BY numero";
		$datacti = viewsql($sql);
		foreach($datacti as $regacti)
		{
			$pdf->Cell(20,4,"",0,0);
			$inipos = $pdf->GetY(); 
			$pdf->MultiCell(166,4,$regacti->actividad,1,'J');
			$finpos = $pdf->GetY();
			$dif = $finpos - $inipos;
			$pdf->SetY($inipos);
			$pdf->Cell(20,$dif,"",1,0);
			$pdf->Cell(166,$dif,"",0,0);
			$pdf->Cell(0,$dif,"",1,0);
			$pdf->SetX(10);
			$pdf->Cell(20,4,$regacti->codigo,0,0);
			$pdf->Cell(166,4,"",0,0);
			$pdf->Cell(0,4,$regacti->porcentaje,0,0,'C');
			$pdf->SetY($finpos);
		}
	} //fin logros
}	
$pdf->Output();
disconnect();
?>
