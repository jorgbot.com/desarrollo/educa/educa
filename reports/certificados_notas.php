<?php
/***********************************************************************************************
* @file        : certificados_notas.php (ancho hoja 196)                                       *
* @brief       : Genera reporte de certificado de notas por alumno en PDF                      *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$tipoedu = isset($_GET['tipoedu']) ? $_GET['tipoedu'] : null ;
$corte = isset($_GET['notanum']) ? $_GET['notanum'] : null ;
$consecutivo = isset($_GET['consecutivo']) ? $_GET['consecutivo'] : null ;
$formato = isset($_GET['formato']) ? $_GET['formato'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
if ($tipoedu=="")
	$tipoedu = "F";
connect();
//verificar que no deba pension ni transporte
$saldopen = 0;
$saldotra = 0;
if ($ano >= 2015)
{
    $saldopen = saldo_pensiones($ano,$id_alumno);
    $saldotra = saldo_transporte($ano,$id_alumno);
}
if ($saldopen > 0 or $saldotra > 0)
{
    $msg = "No se puede expedir certificado, el Alumno tiene saldos pendientes en Pension: $".number_format($saldopen,0)." en Transporte: $".number_format($saldotra,0);
    echo"<script>alert('$msg');window.close();</script>";
}    
 //institucion
$sql="select * from institucion limit 1";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$rector = $reginst->rector;
	$secretaria = $reginst->secretaria;
	$rectordoc = $reginst->rectordoc;
	$secretariadoc = $reginst->secretariadoc;
	$ciudad = $reginst->ciudad;
}
//notas limite a�o lectivo
$sql = "select * from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{
	$notamin = $reganol->notamin;
	$notamax = $reganol->notamax;
	$notalim = $reganol->notalim;
	$areasper = $reganol->areasper;
}
//puntajes
$sql = "select * from puntajes where ano = '$ano' and tipoedu = '$tipoedu' order by notamax";
$datpunt = viewsql($sql);
//periodos academicos
if ($corte <= 0)
	$corte=9;
$cierre="";
$fperiodo="";
$sql="SELECT notanum, pertipo, periodo, perplan, estado FROM periodos WHERE ano = '$ano' order by notanum";
$datperi=viewsql($sql);
$numperi = count($datperi);
foreach($datperi as $regperi)
{
	if ($regperi->pertipo=="P") //estado del ultimo periodo
	{
		$cierre=$regperi->estado;
		if ($regperi->estado!="CERRADO")
		{
			$fperiodo=" (A�o lectivo sin finalizar - notas al PERIODO: ".$regperi->perplan." )";
		}
	}
}
//tablas notas y alumnos
$notas = "y".$ano."notas";
$cursos = "y".$ano."cursos";
$sql="SELECT $cursos.*, alumnos.docutip, alumnos.docunum, alumnos.docuexp FROM $cursos, alumnos ";
$sql.="WHERE $cursos.id_alumno = alumnos.id_alumno ";
if ($id_alumno > 0)
{
    $sql.="AND $cursos.id_alumno = '$id_alumno' LIMIT 1";
    $datalum = viewsql($sql);
    foreach($datalum as $regalum)
    {
	$grado = $regalum->grado;
    }
}
else
{
	$sql.="AND $cursos.grado = '$grado' ";
	if ($curso != "")
		$sql.="AND $cursos.curso = '$curso' ";
	$sql.="AND $cursos.tipoedu = 'F' ORDER BY $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres";
        $datalum = viewsql($sql);
}
//nombre grado
$sql="SELECT gradonom, educacion FROM grados WHERE grado = '$grado' limit 1";
$datgrad = viewsql($sql);
foreach($datgrad as $reggrad)
{
	$gradonom = $reggrad->gradonom;
	$educacion = ucwords($reggrad->educacion);
}
//areas del grado
$sql="SELECT sum(intensidad) as suma, id_area, area, promedio, perdida FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' group by id_area ORDER BY perdida desc, suma desc, codigo";
$datarea = viewsql($sql);
//asignaturas del grado
$sql="SELECT id_area, area, id_asignatura, asignatura, porcentaje, intensidad, notaQ FROM asignaturas WHERE ano = '$ano' AND grado = '$grado' and (notas = '1' or boletin = '1') ORDER BY id_area, intensidad desc, codigo";
$datasig = viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,0,0);
			if ($implogo=="S")
			{
				$datinst = $GLOBALS["datinst"] ;
				foreach($datinst as $reginst)
				{
					$insnom = $reginst->instinom;
					$inssig = $reginst->instisig;
					$insciu = $reginst->ciudad;
					$insdir = $reginst->direccion;
					$instel = $reginst->telefono;
					$insre1 = $reginst->resolkin;
					$insre2 = $reginst->resolpri;
					$insre3 = $reginst->resolsec;
					$inssec = $reginst->resolseed;
					$insdan = $reginst->resoldane;
					$insnot = $reginst->notaria;
				}
				$this->Image('../images/logo.jpg',12,10,20,0,'JPG');
				$this->Ln(5);
				$this->Cell(0,6,$insnom,0,1,'C');
				$this->Cell(0,6,$inssig,0,1,'C');
				$this->SetFont('Arial','',6);
				$this->SetY(30);
				$this->Cell(21,3,"");
				$this->MultiCell(55,3,$insre1,0,'C');
				$this->SetY(30);
				$this->Cell(80,3,"");
				$this->MultiCell(55,3,$insre2,0,'C');
				$this->SetY(30);
				$this->Cell(141,3,"");
				$this->MultiCell(55,3,$insre3,0,'C');
			}
			else
			{
				$this->Ln(26);
			}
			$this->Ln(5);
		}
		function Footer()
		{
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("CERTIFICADO");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$numalu = 0;
	//lista de Alumnos
	foreach($datalum as $regalum)
	{
	$numalu = $numalu + 1;
	$pdf->AddPage('P',$papel);
	$id_alumno = $regalum->id_alumno;
	$apellidos = $regalum->apellidos;
	$nombres = $regalum->nombres;
	$docutip = $regalum->docutip;
	$docunum = $regalum->docunum;
	$docuexp = $regalum->docuexp;
	$tipoedu = $regalum->tipoedu;
	$promedio = $regalum->promedio;
	$numpierde = $regalum->areasper;
	$fchretiro = $regalum->fchretiro;
	$pierde = intval($regalum->pierde);
	$folio = "";
	$sql = "select folio from matriculas where id_alumno = '$id_alumno' and ano = '$ano' limit 1";
	$datmatr=viewsql($sql);
	foreach($datmatr as $regmatr)
	{$folio = $regmatr->folio;}
	$sql="SELECT lisnom FROM listas WHERE listip = 'IDENTIFICACION' and lisval = '$docutip' LIMIT 1";
	$listide=viewsql($sql);
	foreach($listide as $regiden)
	{$docutip = $regiden->lisnom;}
	$aprobado = "";
	$promueve = $fperiodo;
	if ($cierre=="CERRADO")
	{
		if ($pierde > 0)
		{
			$aprobado = "(NO APROBO GRADO ".$gradonom.")";
			$promueve="*** EL ALUMNO NO ES PROMOVIDO AL GRADO SIGUIENTE ***";
                        if ($grado==11) {
        			$promueve="*** EL ALUMNO NO ES PROMOVIDO ***";
                        }
		}
		else
		{
			$aprobado = "(APROBO GRADO ".$gradonom.")";
			$promueve = "*** EL ALUMNO ES PROMOVIDO AL GRADO SIGUIENTE ***";
                        if ($grado==11) {
        			$promueve = "*** EL ALUMNO ES PROMOVIDO ***";
                        }
		}
	}
	if (substr($fchretiro,0,4) > 0)
		$promueve ="*** ALUMNO RETIRADO EL ".fecha_texto($fchretiro)." ***";
	$titulo = "CERTIFICADO DE NOTAS ".$ano;
	if ($formato=="LIBRO")
		$titulo = "LIBRO DE CALIFICACIONES ".$ano;
	//texto del certificado
	$texto = "";
	$sql="SELECT temtex FROM temas WHERE temnom = 'CERTIFICADO DE NOTAS' LIMIT 1";
	$dattem = viewsql($sql);
	foreach($dattem as $regtem)
	{$texto = $regtem->temtex;}
	$texto = str_replace('$apellidos',$apellidos,$texto);
	$texto = str_replace('$nombres',$nombres,$texto);
	$texto = str_replace('$docutip',$docutip,$texto);
	$texto = str_replace('$docunum',$docunum,$texto);
	$texto = str_replace('$docuexp',$docuexp,$texto);
	$texto = str_replace('$gradonom',$gradonom,$texto);
	$texto = str_replace('$grado',$grado,$texto);
	$texto = str_replace('$educacion',$educacion,$texto);
	$texto = str_replace('$ano',$ano.$fperiodo,$texto);
	if ($formato=="")
	{
		$pdf->SetFont('Arial','B',12);
		if ($consecutivo != "")
		{
			$pdf->Cell(0,10,'Certificado No. '.$consecutivo,0,0,'L');
			$consecutivo = $consecutivo + 1;
		}
		else
			$pdf->Cell(0,10,'',0,0,'L');
		$pdf->Cell(0,10,'Folio No. '.$folio,0,1,'R');
		$pdf->Cell(0,8,'LOS SUSCRITOS RECTOR Y SECRETARIA',0,1,'C');
		$pdf->Cell(0,8,'C E R T I F I C A N',0,1,'C');
		$pdf->Ln(5);
		$pdf->SetFont('Arial','',10);
		$pdf->MultiCell(0,5,$texto,0,'J');
		$pdf->Ln(3);
	}
	else
	{
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,80,0);
		$pdf->Cell(100,6,'LIBRO DE CALIFICACIONES',0,1);
		$pdf->Ln(3);
		$pdf->SetTextColor(0,0,0);			
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(20,6,"GRADO ",1,0);
		$pdf->Cell(90,6,$gradonom." - CURSO ".$curso,1,0);
		$pdf->SetFont('Arial','B',8);
		$pdf->Cell(25,6,"FOLIO ".$folio,1,1);
		$pdf->Cell(20,6,"ALUMNO ",1,0);
		$pdf->Cell(90,6,$apellidos." ".$nombres,1,0);
		$pdf->Cell(25,6,"CODIGO ".$id_alumno,1,1);
		$pdf->Ln(3);
	}
	//titulos columnas
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(110,5,'',1,0,'L',1);
	$pdf->Cell(6,5,'I.H',1,0,'C',1);
	$pdf->Cell(5,5,'%',1,0,'C',1);
	$pdf->Cell(6,5,'DEF',1,0,'C',1);
	$pdf->Cell(0,5,'CONCEPTO',1,1,'C',1);
	$pdf->SetFont('Arial','',8);
	$perdidas = 0; //areas perdidas alumno
	$sumnotas = 0; //suma de areas promedio alumno
	$numnotas = 0; //numero de areas promedio alumno
	$generales = 0; //numero de asignaturas generales, no afectan perdida
	//lista de areas
	foreach($datarea as $regarea)
	{
		$id_area = $regarea->id_area;
		$area = $regarea->area;
		if ($regarea->perdida > 0)
			$area = "AREA ".$area;
		else
			$generales = $generales + 1;
		$id_area = $regarea->id_area;
		$nota = notas_area($ano,$id_alumno,$id_area,$corte,$datperi);
		if ($nota[9] > 0)
		{
			if ($nota[9] < $notalim and $regarea->perdida > 0) //area afecta perdida del a�o
				$perdidas = $perdidas + 1;
			if ($regarea->promedio > 0) //area afecta promedio del alumno
			{
				$sumnotas = $sumnotas + $nota[9];
				$numnotas = $numnotas + 1;
			}
		}
		if ($generales == 0)
		{
			$pdf->SetFont('Arial','B',6);
			$pdf->Cell(110,5,substr($area,0,79),1,0,'L',1);
			$pdf->Cell(6,5,$regarea->suma,1,0,'C',1);
			$pdf->Cell(5,5,'',1,0,'L',1);
			$notaQ = cualitativa($datpunt,$nota[9]);
			$notaD = conceptual($datpunt,$nota[9]);
			if ($nota[9] <= $notalim)
				$pdf->SetTextColor(164,0,0);
			$pdf->Cell(6,5,$notaQ,1,0,'C',1);
			$pdf->Cell(0,5,strtoupper($notaD),1,1,'L',1);
			$pdf->SetTextColor(0,0,0);
		}
		if ($generales == 1)
		{
			$pdf->SetFont('Arial','B',6);
			$pdf->Cell(110,5,"ASIGNATURAS GENERALES",1,0,'L',1);
			$pdf->Cell(6,5,"",1,0,'C',1);
			$pdf->Cell(5,5,"",1,0,'C',1);
			$pdf->Cell(6,5,"",1,0,'C',1);
			$pdf->Cell(0,5,"",1,1,'C',1);
		}
		//lista de asignaturas
		foreach($datasig as $regasig)
		{
			if ($regasig->id_area == $id_area)
			{
				$id_asignatura = $regasig->id_asignatura;
				$id_asignatura = $regasig->id_asignatura;
				$nota = notas_asignatura($ano,$id_alumno,$id_asignatura,$corte,$datperi);
				$pdf->SetFont('Arial','',6);
				$pdf->Cell(5,5,"","LTB");
				$pdf->Cell(105,5,substr($regasig->asignatura,0,75),"TBR",0,'L');
				$pdf->Cell(6,5,$regasig->intensidad,1,0,'C');
				$pdf->Cell(5,5,$regasig->porcentaje,1,0,'C');
				if ($regasig->notaQ != "") //nota default de la asignatura
				{
					$pdf->Cell(6,5,substr($regasig->notaQ,0,1),1,0,'C',1);
					$pdf->Cell(0,5,$regasig->notaQ,1,0,'L');
				}
				else
				{
					$notaQ = cualitativa($datpunt,$nota[9]);
					$pdf->SetFont('Arial','B',6);
					$notafin = $nota[9];
					if ($nota[9] < $notalim)
						$pdf->SetTextColor(164,0,0);
					$pdf->Cell(6,5,$notaQ,1,0,'C',1);
					$pdf->SetTextColor(0,0,0);
				}
				$pdf->Cell(0,5,"",1,1,'L');
			} //si area
		} //for asignaturas
	} //for areas
	//si pierde areas en linea o en matricula o pierde a�o en matricula
	$pdf->SetFont('Arial','B',7);
	$pdf->Cell(70,8,'AREAS PERDIDAS: '.$perdidas."   ".ucwords($aprobado),1,0,'',1);
	$pdf->Cell(0,8,$promueve,1,1,'R',1);
	$pdf->SetFont('Arial','',10);
	$pdf->ln(3);
	$fecha = fecha_carta(date('Y-m-d'));
	if ($formato=="")
		$pdf->Cell(0,5,"El presente certificado se expide en la ciudad de ".$ciudad.", el ".$fecha.".",0,1);
	$pdf->ln(15);
	$pdf->Cell(98,8,"_______________________________",0,0,'C');
	$pdf->Cell(98,8,"_______________________________",0,1,'C');
	$pdf->Cell(98,5,$rector,0,0,'C');
	$pdf->Cell(98,5,$secretaria,0,1,'C');
	$pdf->Cell(98,5,"CC ".$rectordoc,0,0,'C');
	$pdf->Cell(98,5,"CC ".$secretariadoc,0,1,'C');
	$pdf->Cell(98,5,"RECTOR",0,0,'C');
	$pdf->Cell(98,5,"SECRETARIA",0,1,'C');
	} //for alumnos
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
