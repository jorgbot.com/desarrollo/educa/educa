<?php
/***********************************************************************************************
* @file        : rep_fallasper.php (ancho hoja 196)                                            *
* @brief       : fallas consolidadas para perdida de asignatura por alumno                     *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 17-may-2012                                                    *
* @Modificado  : Nora Rodriguez 13-nov-2018                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php"); 
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_categoria = isset($_GET['id_categoria']) ? $_GET['id_categoria'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
connect();
//porcentaje perdida de fallas
$sql = "select fallasper from anoslect where ano = '$ano' limit 1";
$datanol = viewsql($sql);
foreach($datanol as $reganol)
{$fallasper = $reganol->fallasper;}
//calcular n�mero de semanas del a�o seg�n peri�dos acad�micos
$semanas = semanas_academicas($ano);
$titulo = "ASIGNATURAS PERDIDAS POR FALLAS";
$subtitulo = "A�O ".$ano." - SEMANAS ACAD�MICAS ".$semanas;
//tabla categorias de inasistencias
$sql="select id_categoria, categoria, parametro from categorias where tip_categoria = 'INASISTENCIAS' order by id_categoria";
$datcateg=viewsql($sql);
// AND categorias.parametro != '1'
//alumnos con fallas acumuladas por asignatura con categoria perdida de a�o
$fallas = "y".$ano."fallas";
$cursos = "y".$ano."cursos";
$sql="SELECT $fallas.horas, $fallas.id_alumno, $fallas.id_categoria, $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres, asignaturas.codigo, asignaturas.asignatura, asignaturas.intensidad ".
	"FROM $fallas, $cursos, asignaturas WHERE $fallas.fecha LIKE '$ano%' AND $fallas.id_asignatura > 0 ";
if ($id_categoria > 0)	
	$sql.="AND $fallas.id_categoria = '$id_categoria' ";
if ($id_alumno > 0)
	$sql.="AND $fallas.id_alumno = '$id_alumno' ";
else
{
	if ($grado != "")
	{
		$sql.="AND $cursos.grado = '$grado' ";
		$subtitulo.=" - GRADO ".$grado;
	}
	if ($curso != "")
	{
		$sql.="AND $cursos.curso = '$curso' ";
		$subtitulo.=" CURSO ".$curso;
	}
}
$sql.="AND $fallas.id_alumno = $cursos.id_alumno AND $fallas.id_asignatura = asignaturas.id_asignatura ".
	"ORDER BY abs($cursos.grado), $cursos.curso, $cursos.apellidos, $cursos.nombres, asignaturas.asignatura";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',6);
			$this->Cell(7,5,'Grado',1,0,'C',1);
			$this->Cell(7,5,'Curso',1,0,'C',1);
			$this->Cell(55,5,'Apellidos y Nombres',1,0,'L',1);
                        $this->Cell(10,5,'ID',1,0,'C',1);
			$this->Cell(75,5,'Asignatura',1,0,'C',1);
			$this->Cell(8,5,'I.H.S',1,0,'C',1);
			$this->Cell(8,5,'H.TOT',1,0,'C',1);
			$this->Cell(8,5,'%20',1,0,'C',1);
			$this->Cell(8,5,'Fallas',1,0,'C',1);
			$this->Cell(0,5,'Pierde',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$pdf->SetFont('Arial','',7);
	//lista de Alumnos
	$nalu = 0;
	$id_alumno = "";
	$asignatura = "";
	$intensidad = 0;
        $numasi = 0;
	foreach($datalum as $regalum)
	{
		if ($regalum->id_alumno != $id_alumno)
		{
			if ($id_alumno != "")
			{
				//acumulado asignatura anterior
				$horas = $intensidad * $semanas;
				$porce = round($horas*($fallasper/100),0);
				$pierde = "";
				if ($suma >= $porce)
				{
					$pierde = "SI";
                                        $numasi = $numasi + 1;
                                        if ($numasi == 1) {
                                            $pdf->Cell(7,5,$grado,'LR',0,'C');
                                            $pdf->Cell(7,5,$curso,'LR',0,'C');
                                            $pdf->Cell(55,5,substr($alumno,0,32),'LR',0,'L');
                                            $pdf->Cell(10,5,$id_alumno,'LR',0,'C');
                                        } else {
                                            $pdf->Cell(7,5,"",'LR',0,'C');
                                            $pdf->Cell(7,5,"",'LR',0,'C');
                                            $pdf->Cell(55,5,"",'LR',0,'L');
                                            $pdf->Cell(10,5,"",'LR',0,'C');
                                        }
					$pdf->Cell(75,5,$asignatura,'LR',0,'L');
					$pdf->Cell(8,5,$intensidad,'LR',0,'C');
					$pdf->Cell(8,5,$horas,'LR',0,'C');
					$pdf->Cell(8,5,$porce,'LR',0,'C');
					$pdf->Cell(8,5,$suma,'LR',0,'C');
					$pdf->Cell(0,5,$pierde,'LR',1,'C');
				}
			}
			$nalu = $nalu + 1;
			$id_alumno = $regalum->id_alumno;
			$grado = $regalum->grado;
			$curso = $regalum->curso;
			$alumno = $regalum->apellidos." ".$regalum->nombres;
			$asignatura = $regalum->asignatura;
			$intensidad = $regalum->intensidad;
			$suma = 0;
                        $numasi = 0;
		}
		else
		{
			if ($regalum->asignatura != $asignatura)
			{
				//acumulado asignatura anterior
				$horas = $intensidad * $semanas;
				$porce = round($horas*($fallasper/100),0);
				$pierde = "";
				if ($suma >= $porce)
				{
					$pierde = "SI";
                                        $numasi = $numasi + 1;
                                        if ($numasi == 1) {
                                            $pdf->Cell(7,5,$grado,'LR',0,'C');
                                            $pdf->Cell(7,5,$curso,'LR',0,'C');
                                            $pdf->Cell(55,5,substr($alumno,0,32),'LR',0,'L');
                                            $pdf->Cell(10,5,$id_alumno,'LR',0,'C');
                                        } else {
                                            $pdf->Cell(7,5,"",'LR',0,'C');
                                            $pdf->Cell(7,5,"",'LR',0,'C');
                                            $pdf->Cell(55,5,"",'LR',0,'L');
                                            $pdf->Cell(10,5,"",'LR',0,'C');
                                        }
					$pdf->Cell(75,5,$asignatura,'LR',0,'L');
					$pdf->Cell(8,5,$intensidad,'LR',0,'C');
					$pdf->Cell(8,5,$horas,'LR',0,'C');
					$pdf->Cell(8,5,$porce,'LR',0,'C');
					$pdf->Cell(8,5,$suma,'LR',0,'C');
					$pdf->Cell(0,5,$pierde,'LR',1,'C');
				}
				//nueva asignatura
				$asignatura = $regalum->asignatura;
				$intensidad = $regalum->intensidad;
				$suma = 0;
			}
		}
                //suma si no se ha excusado o si la categoria de excusa acumula para fallas 
                $id_categoria = $regalum->id_categoria;
                if ($id_categoria > 0)
                {
                    foreach($datcateg as $regcateg) {
                        if ($id_categoria == $regcateg->id_categoria and $regcateg->parametro==1) {
                            $id_categoria = 0; 
                        }
                    }
                }
                if ($id_categoria <= 0) {
                    $suma = $suma + $regalum->horas;
                }
	}
	$horas = $intensidad * $semanas;
	$porce = round($horas*($fallasper/100),0);
	$pierde = "";
	if ($suma >= $porce)
	{
            $pierde = "SI";
            $numasi = $numasi + 1;
            if ($numasi == 1) {
                $pdf->Cell(7,5,$grado,'LR',0,'C');
                $pdf->Cell(7,5,$curso,'LR',0,'C');
                $pdf->Cell(55,5,substr($alumno,0,32),'LR',0,'L');
                $pdf->Cell(10,5,$id_alumno,'LR',0,'C');
            } else {
                $pdf->Cell(7,5,"",'LR',0,'C');
                $pdf->Cell(7,5,"",'LR',0,'C');
                $pdf->Cell(55,5,"",'LR',0,'L');
                $pdf->Cell(10,5,"",'LR',0,'C');
            }
            $pdf->Cell(75,5,$asignatura,'LR',0,'L');
            $pdf->Cell(8,5,$intensidad,'LR',0,'C');
            $pdf->Cell(8,5,$horas,'LR',0,'C');
            $pdf->Cell(8,5,$porce,'LR',0,'C');
            $pdf->Cell(8,5,$suma,'LR',0,'C');
            $pdf->Cell(0,5,$pierde,'LR',1,'C');
	}
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(0,5," TOTAL ALUMNOS: ".$nalu,1,1,'L',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
