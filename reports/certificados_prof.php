<?php
/***********************************************************************************************
* @file        : certificados_prof.php                                                         *
* @brief       : Imprime certificado de profesores seg�n plantilla de texto en tabla           *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 13-abr-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../database/database.php");
include("../database/datos_setup.php");
include("../libs/fechas.php");
$id_plantilla = $_GET["id_plantilla"];
$id_profesor = isset($_GET['id_profesor']) ? $_GET['id_profesor'] : null ;
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$consecutivo = isset($_GET['consecutivo']) ? $_GET['consecutivo'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$imprimir = isset($_GET['imprimir']) ? $_GET['imprimir'] : null ;
$fecha = isset($_GET['fecha']) ? $_GET['fecha'] : null ;
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
if ($fecha=="")
	$fecha = date('Y-m-d');
connect();
//traer plantilla
$datplantilla = datos_plantilla($id_plantilla);
$texto = $datplantilla["texto"];
$fecha = fecha_carta($fecha);
//traer profesor
if ($id_profesor=="PRUEBA")
{
	$implogo = "S";
	$sql="SELECT id_profesor FROM profesorescont WHERE ano = '$ano' limit 1";
	$datcont = viewsql($sql);
	foreach($datcont as $regcont)
	{$id_profesor = $regcont->id_profesor;}
}
$sql="SELECT apellidos, nombres, docunum, docutip, docuexp, cargo FROM profesores WHERE id_profesor = '$id_profesor' limit 1";
$datprof=viewsql($sql);
foreach($datprof as $regprof)
{
	$apellidos = $regprof->apellidos;
	$nombres = $regprof->nombres;
	$docutip = $regprof->docutip;
	$docunum = $regprof->docunum;
	$docuexp = $regprof->docuexp;
	$cargo = $regprof->cargo;
}
$sql="SELECT lisnom FROM listas WHERE listip = 'IDENTIFICACION' and lisval = '$docutip' LIMIT 1";
$listide=viewsql($sql);
foreach($listide as $regiden)
{$docutip = $regiden->lisnom;}
//ultimo contrato
$sql="SELECT * FROM profesorescont WHERE id_profesor = '$id_profesor' and ano = '$ano' limit 1";
$datcont = viewsql($sql);
foreach($datcont as $regcont)
{
	$tipocont = $regcont->tipocont;
	$tiempo = $regcont->tiempo;
	$grados = $regcont->grados;
	$fchinicio = fecha_texto($regcont->fchinicio);
	$fchfinal = fecha_texto($regcont->fchfinal);
}
$sql="SELECT lisnom FROM listas WHERE listip = 'IDENTIFICACION' and lisval = '$docutip' LIMIT 1";
$listide=viewsql($sql);
foreach($listide as $regiden)
{$docutip = $regiden->lisnom;}
$texto = str_replace('$consecutivo',$consecutivo,$texto);
$texto = str_replace('$apellidos',$apellidos,$texto);
$texto = str_replace('$nombres',$nombres,$texto);
$texto = str_replace('$docutip',$docutip,$texto);
$texto = str_replace('$docunum',$docunum,$texto);
$texto = str_replace('$docuexp',$docuexp,$texto);
$texto = str_replace('$cargo',$cargo,$texto);
$texto = str_replace('$tipocont',$tipocont,$texto);
$texto = str_replace('$tiempo',$tiempo,$texto);
$texto = str_replace('$fchinicio',$fchinicio,$texto);
$texto = str_replace('$fchfinal',$fchfinal,$texto);
$texto = str_replace('$fecha',$fecha,$texto);
?>
<html>
<head>
<title>CERTICICADO</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<script language='JavaScript' type='text/JavaScript'>
<!--
	function imprimir()
	{ 
		bV = parseInt(navigator.appVersion);
		if (bV >= 4) window.print();
	}
-->
</script>
</head>
<body style="margin: 0px;">
<table cellspacing="0" cellpadding="0" border="0" width="700" align="left">
<tr>
	<td width="20"></td>
	<td>
	<table cellspacing="0" cellpadding="0" border="0" width="100%">
		<tr valign="top">
			<td height="150">
				<?php if ($implogo=="S") include("../includes/banner_doc.php");?>
			</td>
		</tr>
		<tr><td height="50"></td></tr>
		<tr>
			<td>
				<table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-family:Arial,Verdana; font-size:13px;">
					<tr><td style="text-align: justify;"><?php echo $texto;?></td></tr>
				</table>
			</td>
		</tr>
	</table>
	</td>
</tr>
</table>
<?php if ($imprimir!="") { ?>
<script type='text/javascript' language='javascript'>
<!--
// Do print the page
if (typeof(window.print) != 'undefined')
{
	window.print();
}
//-->
</script>
<?php } ?>
</body>
</html>