<?php
/***********************************************************************************************
* @file        : rep_transppap.php (ancho hoja 196)                                           *
* @brief       : papeletas de alumnos en transporte por grado y/o curso                     *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-oct-2015                                                    *
* @Modificado  : Nora Rodriguez 13-oct-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_alumnos.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_ruta = isset($_GET['id_ruta']) ? $_GET['id_ruta'] : null ;
$codigo = isset($_GET['codigo']) ? $_GET['codigo'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
connect();
if ($id_ruta > 0)
	$sql="SELECT id_ruta, ruta FROM rutas WHERE id_ruta = '$id_ruta' LIMIT 1";
else
	$sql="SELECT id_ruta, ruta FROM rutas WHERE ano = '$ano' ORDER BY ruta";
$datrut=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
		}
		function Footer()
		{
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("PAPELETAS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	//lista de Alumnos
	$numalu = 0;
	$pdf->AddPage('P',$papel);
	foreach($datrut as $regrut)
	{
		$id_ruta = $regrut->id_ruta;
		$ruta = $regrut->ruta;
		$sql="SELECT transporte.id_alumno,transporte.grado,transporte.curso,transporte.codigo,transporte.id_ruta,transporte.id_familiar,transporte.jornada,transporte.paradero,transporte.id_rutaop,transporte.id_familiarop,transporte.paraderoop,".
		"alumnos.apellidos,alumnos.nombres,alumnos.fchnace,alumnos.genero FROM transporte, alumnos ".
		"WHERE (transporte.id_ruta = '$id_ruta' OR transporte.id_rutaop = '$id_ruta') AND transporte.estado = 'ACTIVO' AND transporte.id_alumno = alumnos.id_alumno ";
		if ($id_alumno > 0)
			$sql.="AND transporte.id_alumno = '$id_alumno' ";
		else
		{
			if ($grado != "")
				$sql.="AND transporte.grado = '$grado' ";
			if ($curso != "")
				$sql.="AND transporte.curso = '$curso' ";
			if ($codigo != "")
				$sql.="AND transporte.codigo = '$codigo' ";
		}
		$sql.= "ORDER BY transporte.codigo";
		$datalu=viewsql($sql);
		foreach($datalu as $regalu)
		{
			$numalu = $numalu + 1;		
			$idalum = $regalu->id_alumno;
			if ($regalu->id_ruta==$id_ruta)
			{
				$id_familiar = $regalu->id_familiar;
				$paradero = $regalu->paradero;
				$jornada = "RECOGER Y DEJAR";
				if ($regalu->jornada==1)
					$jornada = "SOLO RECOGER";
				if ($regalu->jornada==2)
					$jornada = "SOLO DEJAR";
			}
			else
			{
				$id_familiar = $regalu->id_familiarop;
				$paradero = $regalu->paraderoop;
				$jornada = "SOLO DEJAR";
			}
			$pdf->Cell(0,5,"",0,1);
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,80,0);
			$pdf->Cell(0,8,$_SESSION["instinom"],0,1,'C');
			$pdf->SetFont('Arial','B',9);
			$pdf->Cell(0,8,"ALUMNOS EN TRANSPORTE A�O ".$ano,0,1,'C');
			$pdf->SetTextColor(0,0,0);
			$pdf->Ln();
			$pdf->SetFont('Arial','B',8);
			$pdf->SetFillColor(0,128,0);
			$pdf->SetTextColor(255,255,255);
			$pdf->Cell(20,4," RUTA ".$ruta,1,1,'L',1);
			$pdf->SetTextColor(0,0,0);
			$pdf->SetFillColor(224,231,233);
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(12,4,"C�digo",'LTR',0,'C',1);
			$pdf->Cell(90,4,"Alumno Nombres y Apellidos",'LTR',0,'L',1);
			$pdf->Cell(20,4,"Grado y Curso",'LTR',0,'C',1);
			$pdf->Cell(12,4,"Edad",'LTR',0,'C',1);
			$pdf->Cell(12,4,"G�nero",'LTR',0,'C',1);
			$pdf->Cell(50,4,"Transporte",'LTR',1,'C',1);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(12,8,$regalu->codigo,'LRB',0,'C');
			$pdf->Cell(90,8,$regalu->nombres." ".$regalu->apellidos,'LRB',0);
			$pdf->Cell(20,8,$regalu->grado."  ".$regalu->curso,'LRB',0,'C');
			$edad = calculo_edad($regalu->fchnace);
			$pdf->Cell(12,8,$edad,'LRB',0,'C');
			$pdf->Cell(12,8,$regalu->genero,'LRB',0,'C');
			$pdf->Cell(50,8,$jornada,'LRB',1,'C');
			$tipo = "";
			$apellidos = "";
			$nombres = "";
			$direccion = "";
			$barrio = "";
			$telefono = "";
			$telefofi = "";
			$celular = "";
			$sql="select tipo, apellidos, nombres, direccion, barrio, telefono, telefofi, celular from familiares where id_alumno = '$idalum' and id_familiar = '$id_familiar' LIMIT 1";
			$dat=viewsql($sql);
			foreach($dat as $reg)
			{
				$tipo = $reg->tipo;
				$apellidos = $reg->apellidos;
				$nombres = $reg->nombres;
				$direccion = $reg->direccion;
				$barrio = $reg->barrio;
				$telefono = $reg->telefono;
				$telefofi = $reg->telefofi;
				$celular = $reg->celular;
			}
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(100,4,"Direcci�n",'LTR',0,'C',1);
			$pdf->Cell(46,4,"Barrio",'LTR',0,'C',1);
			$pdf->Cell(50,4,"Paradero",'LTR',1,'C',1);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(100,8,$direccion,'LRB',0);
			$pdf->Cell(46,8,$barrio,'LRB',0);
			$pdf->Cell(50,8,$paradero,'LRB',1,'C');
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(20,4,"Acudiente",'LTR',0,'L',1);
			$pdf->Cell(72,4,"Acudiente Nombres y Apellidos",'LTR',0,'C',1);
			$pdf->Cell(43,4,"Celular",'LTR',0,'C',1);
			$pdf->Cell(16,4,"Casa",'LTR',0,'C',1);
			$pdf->Cell(45,4,"Oficina",'LTR',1,'C',1);
			$pdf->SetFont('Arial','',9);
			$pdf->Cell(20,8,$tipo,'LRB',0);
			$pdf->Cell(72,8,$nombres." ".$apellidos,'LRB',0);
			$pdf->Cell(43,8,$celular,'LRB',0);
			$pdf->Cell(16,8,$telefono,'LRB',0);
			$pdf->Cell(45,8,$telefofi,'LRB',1);
			$pdf->Cell(0,10,"",0,1);
			if ($numalu == 3) //Hasta 3 papeletas por hoja
			{
				$pdf->AddPage('P',$papel);
				$numalu = 0;
			}
		}
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
