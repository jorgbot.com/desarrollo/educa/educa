<?php
/***********************************************************************************************
* @file        : rep_pagospap.php (ancho hoja 196)                                             *
* @brief       : papeletas de alumnos con deuda en pension o matriculas                        *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-oct-2015                                                    *
* @Modificado  : Nora Rodriguez 13-oct-2015                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$id_ruta = isset($_GET['id_ruta']) ? $_GET['id_ruta'] : null ;
$swvalores = isset($_GET['swvalores']) ? $_GET['swvalores'] : 1 ; //Imprimir valores en deuda
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$fchcorte = fec_amd($fchcorte);
$mescorte = substr($fchcorte,5,2);
$filepag = "y".$ano."pagosdet";
connect();
$porcen = 0;
$sql="SELECT ano, interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
$filepag = "y".$ano."pagosdet";
if ($tipo=="T")
	$sql="SELECT transporte.id_alumno, transporte.grado, transporte.curso, transporte.codigo, transporte.id_ruta, alumnos.apellidos, alumnos.nombres, alumnos.genero FROM transporte, alumnos WHERE transporte.ano = '$ano' AND transporte.estado = 'ACTIVO' AND transporte.id_alumno = alumnos.id_alumno ";
else
	$sql="SELECT matriculas.id_alumno, matriculas.grado, matriculas.curso, alumnos.apellidos, alumnos.nombres, alumnos.genero FROM matriculas, alumnos WHERE  matriculas.ano = '$ano' AND matriculas.estado = 'CERRADA' AND matriculas.id_alumno = alumnos.id_alumno ";
if ($id_alumno > 0)
{
	if ($tipo=="T")
		$sql.="AND transporte.id_alumno = '$id_alumno' LIMIT 1";
	else
		$sql.="AND matriculas.id_alumno = '$id_alumno' LIMIT 1";
}
else
{	
	if ($grado != "")
		$sql.="AND grado = '$grado' ";
	if ($curso != "")
		$sql.="AND curso = '$curso' ";
	if ($tipo=="P" and $id_ruta > 0)
		$sql.="AND id_ruta = '$id_ruta' ";
	$sql.="ORDER BY CAST(grado AS SIGNED), curso, apellidos, nombres";
}
$datalu=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
		}
		function Footer()
		{
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("PAPELETAS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	//lista de Alumnos
	$numalu = 0;
	$gradonom = "";
	$pdf->AddPage('P',$papel);
	foreach($datalu as $regalu)
	{
		$id_alumno = $regalu->id_alumno;
		if ($tipo=="T")
			$aluvalmes = valores_transporte($id_alumno,$ano);
		else
			$aluvalmes = valores_pension($id_alumno,$ano);
		$alupagmes = array();
		$alupagint = array();
		$alusalmes = array();
		$alusalint = array();
		for($i=1; $i<=11 ;$i++)
		{
			$alupagmes[$i] = 0;
			$alupagint[$i] = 0;
			$alusalmes[$i] = 0;
			$alusalint[$i] = 0;
		}
		$alutotpen = 0;
		$sql="SELECT id_pago, tipo, mes, valor, interes, interesliq, interesaju FROM $filepag WHERE id_alumno = '$id_alumno' AND tipo = '$tipo'";
		$datpag = viewsql($sql);
		foreach($datpag as $regpag)
		{
			$mes = $regpag->mes;
			$alupagmes[$mes] = $alupagmes[$mes] + intval($regpag->valor);
			$alupagint[$mes] = $alupagint[$mes] + intval($regpag->interes);
			$sint = intval($regpag->interesliq) - intval($regpag->interes) - intval($regpag->interesaju);
			$alusalint[$mes] = $alusalint[$mes] + $sint;
		}
		//calcular saldos
		for($i=2; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			$saldopen = $aluvalmes[$mes] - $alupagmes[$i];
			$interes = 0;
			if ($i > $mescorte)
				$saldopen = 0;
			if ($tipo=="P" and $saldopen > 0  and $porcen > 0 and $i > 1)
				$interes = interes_pension($ano,$mes,$saldopen,$porcen,$fchcorte);
			$interes = $interes + $alusalint[$i];
			$alusalmes[$i] = $saldopen;
			$alusalint[$i] = $interes;
			$alutotpen = $alutotpen + $saldopen;
		}
		if ($alutotpen > 0)
		{		
			$numalu = $numalu + 1;
			if ($numalu > 1)
				$pdf->Cell(0,9,"",0,1);
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,80,0);
			$pdf->Cell(0,6,$_SESSION["instinom"],0,1,'C');
			$pdf->SetFont('Arial','B',9);
			if ($tipo=="T")
			{
				$id_ruta = $regalu->id_ruta;
				$codigo = $regalu->codigo;
				$pdf->Cell(0,6,"SOLICITUD DE PAZ Y SALVO TRANSPORTES A�O ".$ano,0,1,'C');
				$ruta = "";
				$sql = "SELECT ruta FROM rutas WHERE id_ruta = '$id_ruta' LIMIT 1";
				$datrut = viewsql($sql);
				foreach($datrut as $regrut)
				{$ruta = $regrut->ruta;}
			}
			else
				$pdf->Cell(0,6,"SOLICITUD DE PAZ Y SALVO PENSIONES A�O ".$ano,0,1,'C');
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(0,8,"FECHA DE CORTE ".fecha_texto($fchcorte),0,1,'C');
			$pdf->SetFont('Arial','B',9);
			$alumno = "ALUMNO";
			if ($regalu->genero=="F")
				$alumno = "ALUMNA";
			$pdf->Cell(100,5,$alumno.": ".$regalu->nombres." ".$regalu->apellidos." ID ".$id_alumno,'TL',0);
			if ($regalu->grado!=$gradonom)
			{
				$ngrado = $regalu->grado;
				$sql = "SELECT gradonom FROM grados WHERE grado = '$ngrado' LIMIT 1";
				$datgra = viewsql($sql);
				foreach($datgra as $reggra)
				{$gradonom = $reggra->gradonom;}
			}
			if ($tipo=="T")
				$pdf->Cell(0,5,$gradonom." CURSO ".$regalu->curso." - RUTA ".$ruta." CODIGO ".$codigo,'TR',1,'R');
			else
				$pdf->Cell(0,5,$gradonom." CURSO ".$regalu->curso,'TR',1,'R');
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(0,10,"SE�OR PADRE DE FAMILIA:",'LR',1,'L');
			if ($tipo=="T")
			{
				$titmes = "Transporte";
				$pdf->Cell(0,5,"NOS PERMITIMOS RECORDARLE QUE EL PAGO DEL SERVICIO DE TRANSPORTE SE DEBE EFECTUAR DURANTE LOS PRIMEROS DIEZ DIAS",'LR',1,'L');
				$pdf->Cell(0,5,"DE CADA MES SEGUN LO ACORDADO EN EL MOMENTO DE LA MATRICULA. QUEREMOS INFORMARLE QUE SE DEBEN LOS MESES DE:",'LR',1,'L');
			}
			else
			{
				$titmes = "Pensi�n";
				$pdf->Cell(0,5,"NOS PERMITIMOS RECORDARLE EL PAGO DE LAS PENSIONES DE CONFORMIDAD CON LO ACORDADO EN EL CONTRATO FIRMADO POR",'LR',1,'L');
				$pdf->Cell(0,5,"USTED EN EL MOMENTO DE LA MATRICULA. QUEREMOS INFORMARLE QUE SE DEBEN LOS MESES DE:",'LR',1,'L');
			}
			if ($swvalores > 0)
			{
				$pdf->SetFont('Arial','U',8);
				$pdf->Cell(15,4,"",'L',0);
				for($i=2; $i<=11 ;$i++)
				{
					if ($alusalmes[$i] > 0)
					{
						$nommes = substr(strtoupper(nombre_mes($i)),0,3);
						$pdf->Cell(16,4,$nommes,0,0,'R');
					}
				}
				$pdf->Cell(16,4,'TOTAL',0,0,'R');
				$pdf->Cell(0,4,"",'R',1);
				$pdf->SetFont('Arial','',8);
				$totpen = 0;
				$totint = 0;
				$totfin = 0;
				$pdf->Cell(15,4,$titmes,'L',0);
				for($i=2; $i<=11 ;$i++)
				{
					if ($alusalmes[$i] > 0)
					{
						$pdf->Cell(16,4,number_format($alusalmes[$i],0),0,0,'R');
						$totpen = $totpen + $alusalmes[$i];
						$totfin = $totfin + $alusalmes[$i];
					}
				}
				$pdf->Cell(16,4,number_format($totpen,0),0,0,'R');
				$pdf->Cell(0,4,"",'R',1);
				if ($tipo=="P")
				{
					$pdf->Cell(15,4,"Inter�s",'L',0);
					for($i=2; $i<=11 ;$i++)
					{
						if ($alusalmes[$i] > 0)
						{
							$pdf->Cell(16,4,number_format($alusalint[$i],0),0,0,'R');
							$totint = $totint + $alusalint[$i];
							$totfin = $totfin + $alusalint[$i];
						}
					}
					$pdf->Cell(16,4,number_format($totint,0),0,0,'R');
					$pdf->Cell(0,4,"",'R',1);
					$pdf->Cell(15,4,"Total Mes",'L',0);
					for($i=2; $i<=11 ;$i++)
					{
						if ($alusalmes[$i] > 0)
							$pdf->Cell(16,4,number_format($alusalmes[$i]+$alusalint[$i],0),0,0,'R');
					}
					$pdf->SetFont('Arial','B',8);
					$pdf->Cell(16,4,number_format($totfin,0),0,0,'R');
					$pdf->Cell(0,4,"",'R',1);
				}
				else
				{
					$pdf->Cell(0,8,"",'LR',1,'L');
				}
			}
			else
			{
				$meses = "";
				for($i=2; $i<=11 ;$i++)
				{
					if ($alusalmes[$i] > 0)
					{
						$nommes = strtoupper(nombre_mes($i));
						if ($meses=="")
							$meses = $meses.$nommes;
						else
							$meses = $meses.", ".$nommes;
					}
				}
				$pdf->Cell(0,16,$meses,'LR',1,'L');
			}
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(0,4,"",'LR',1);
			$pdf->Cell(0,5,"POR FAVOR DEVUELVA AL COLEGIO ESTA CIRCULAR FIRMADA POR EL PADRE O ACUDIENTE.",'LR',1,'L');
			$pdf->Cell(148,5,"SI USTED YA REALIZO LA CONCILIACION DE SUS PAGOS HAGA CASO OMISO DE ESTE COMUNICADO.",'LB',0,'L');
			$pdf->SetFont('Arial','B',8);
			$pdf->Cell(48,5,"FIRMA DEL PADRE O ACUDIENTE",'BTR',1,'R');
		}
		if ($numalu == 3) //Hasta 3 papeletas por hoja
		{
			$pdf->AddPage('P',$papel);
			$numalu = 0;
		}
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
