<?php
/***********************************************************************************************
* @file        : diploma.php (ancho hoja 196)                                                  *
* @brief       : Genera reporte de certificado de notas por alumno en PDF                      *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_notas.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$concepto = isset($_GET['concepto']) ? $_GET['concepto'] : null ;
$papel = $_GET["papel"];
$concepto = strtoupper($concepto);
connect();
//institucion
$sql="select * from institucion limit 1";
$datinst=viewsql($sql);
foreach($datinst as $reginst)
{
	$insnom = $reginst->instinom;
	$inssig = $reginst->instisig;
	$rector = $reginst->rector;
	$secretaria = $reginst->secretaria;
	$rectordoc = $reginst->rectordoc;
	$secretariadoc = $reginst->secretariadoc;
	$ciudad = $reginst->ciudad;
}
//periodos academicos
$sql="SELECT * FROM periodos WHERE ano = '$ano' and pertipo = 'P' order by notanum";
$datperi=viewsql($sql);
$numperi = count($datperi);
//alumnos
$cursos = "y".$ano."cursos";
$sql="SELECT $cursos.*, grados.gradonom FROM $cursos, grados ";
if ($id_alumno > 0)
	$sql.="WHERE $cursos.id_alumno = '$id_alumno' AND $cursos.grado = grados.grado LIMIT 1";
else
{
	$sql.="WHERE $cursos.grado = '$grado' ";
	if ($curso != "")
		$sql.="AND $cursos.curso = '$curso' ";
	$sql.="AND $cursos.grado = grados.grado ORDER BY $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres";
}
$datalum=viewsql($sql);
//echo "$sql<br>";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
	}
	function Footer()
	{
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("DIPLOMA");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->SetMargins(20,20,20);
//$pdf->AddFont('Calligrapher','','calligra.php');
$pdf->AddFont('Frscript','','frscript.php');
$numalu = 0;
foreach($datalum as $regalum)
{
	$numalu = $numalu + 1;
	$pdf->AddPage('P','Letter');
	//logo centrado
	$pdf->Image('../images/logo.jpg',90,10,30,0,'JPG'); //izq, top, ancho
	//titulo institucion
	$pdf->SetFont('Arial','B',12);
	$pdf->SetTextColor(0,80,0);
	$pdf->SetFont('times','',20);
	$pdf->Cell(0,30,'',0,1);
	$pdf->Cell(0,8,$insnom,0,1,'C');
	$pdf->SetFont('times','',30);
	$pdf->Cell(0,8,$inssig,0,1,'C');
	//concepto
	$pdf->SetFont('Frscript','',50);
	$pdf->SetTextColor(0,0,0);
	$pdf->ln();
	$pdf->ln();
	$pdf->Cell(0,8,'Diploma de Honor',0,1,'C');
	$pdf->ln();
	$pdf->ln();
	$pdf->SetFont('Frscript','',25);
	$pdf->Cell(0,8,'Concedido a',0,1,'C');
	$pdf->ln();
	$pdf->SetFont('times','',12);
	$pdf->Cell(100,8,substr($regalum->apellidos." ".$regalum->nombres,0,35),'B',0);
	$pdf->SetFont('Frscript','',20);
	$pdf->Cell(18,8,'Grado',0,0);
	$pdf->SetFont('times','',12);
	$pdf->Cell(30,8,$regalum->gradonom,'B',0);
	$pdf->SetFont('Frscript','',20);
	$pdf->Cell(18,8,'Curso',0,0);
	$pdf->SetFont('times','',12);
	$pdf->Cell(10,8,$regalum->curso,'B',1);
	$pdf->ln();
	$pdf->SetFont('Frscript','',25);
	$pdf->Cell(15,8,'Por ',0,0,'C');
	$pdf->SetFont('times','',12);
	$pdf->MultiCell(0,6,$concepto,'B','J');
	$pdf->ln();
	$pdf->ln();
	$fecha = fecha_carta(date('Y-m-d'));
	$pdf->SetFont('Frscript','',20);
	$pdf->Cell(0,5,"Expedido en ".$ciudad.", el ".$fecha.".",0,1,'C');
	$pdf->ln(20);
	$pdf->SetFont('times','',12);
	$pdf->SetTextColor(128,128,128);
	$pdf->Cell(93,8,"_______________________________",0,0,'C');
	$pdf->Cell(93,8,"_______________________________",0,1,'C');
	$pdf->SetTextColor(0,0,0);
	$pdf->Cell(93,5,$rector,0,0,'C');
	$pdf->Cell(93,5,$secretaria,0,1,'C');
	$pdf->Cell(93,5,"RECTOR",0,0,'C');
	$pdf->Cell(93,5,"SECRETARIA",0,1,'C');
} //for alumnos
$pdf->Output();
disconnect();
?>
