<?php
/***********************************************************************************************
* @file        : rep_areaspend.php (ancho hoja 196)                                            *
* @brief       : lista de alumnos con areas perdidas a�o anterior                              *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 11-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 11-abr-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php"); 
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 0; //saltar pagina en cambio de curso
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$titulo = "ALUMNOS CON AREAS PENDIENTES A�O ANTERIOR";
$subtitulo = "A�O ".$ano;
$antano = $ano - 1;
connect();
//alumnos
$cursos = "y".$ano."cursos";
$sql="SELECT $cursos.id_alumno, $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres, areas.area FROM $cursos, areas WHERE $cursos.id_areadeb = areas.id_area AND $cursos.id_areadeb > '0' ";
if ($grado != "")
{
	$sql.="AND $cursos.grado = '$grado' ";
	$subtitulo.=" GRADO ".$grado;
}
if ($curso != "")
{
	$sql.="AND $cursos.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
$sql.="AND $cursos.fchretiro LIKE '0000%' ";
$sql.= "ORDER BY CAST($cursos.grado AS SIGNED), $cursos.curso, $cursos.apellidos, $cursos.nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',6);
			$this->Cell(10,5,'Grado',1,0,'C',1);
			$this->Cell(10,5,'Curso',1,0,'C',1);
			$this->Cell(10,5,'ID',1,0,'C',1);
			$this->Cell(60,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(70,5,'Area Pendiente',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//lista de Alumnos
	$num = 0;
	$grado = "";
	foreach($datalum as $regalum)
	{
		if ($swsalto > 0 and $grado!=$regalum->grado) //saltar hoja por curso
		{
			if ($grado!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',6);
				$pdf->Cell(160,5,"TOTAL ALUMNOS: ".$num,1,1,'L',1);
				$pdf->AddPage('P',$papel);			
			}
			$grado = $regalum->grado;
			$num = 0;
		}
		$num = $num +1;
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(10,5,$regalum->grado,'LR',0,'C');
		$pdf->Cell(10,5,$regalum->curso,'LR',0,'C');
		$pdf->Cell(10,5,$regalum->id_alumno,'LR',0,'L');
		$pdf->Cell(60,5,$regalum->apellidos." ".$regalum->nombres,'LR',0,'L');
		$pdf->Cell(70,5,$regalum->area,'LR',1,'L');
	}
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(160,5,"TOTAL ALUMNOS: ".$num,1,1,'L',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
