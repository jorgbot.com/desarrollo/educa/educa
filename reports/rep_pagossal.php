<?php
/***********************************************************************************************
* @file        : rep_cartera.php (ancho hoja 196)                                              *
* @brief       : reporte cartera pendiente a fecha de corte                                    *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 23-sep-2015                                                    *
* @Modificado  : Nora Rodriguez 23-sep-2015                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$exportar = isset($_GET['exportar']) ? $_GET['exportar'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$swretirados = isset($_GET['swretirados']) ? $_GET['swretirados'] : 0 ; //0=todos, 1=alumnos retirados
$swcobro = isset($_GET['swcobro']) ? $_GET['swcobro'] : 0 ; //Incluir datos de cobro
$swcampo = isset($_GET['swcampo']) ? $_GET['swcampo'] : 0 ; //imprimir 0=valores, 1=idpago, 2=identrada
$swinteres = isset($_GET['swinteres']) ? $_GET['swinteres'] : 0 ; //liquidar interes
$swdeben = isset($_GET['swdeben']) ? $_GET['swdeben'] : 1 ; //solo alumnos que deben
$swsalto = isset($_GET['swsalto']) ? $_GET['swsalto'] : 1 ; //saltar pagina en cambio de curso
$fchcorte = fec_amd($fchcorte);
$anocorte = substr($fchcorte,0,4);
$mescorte = substr($fchcorte,5,2);
$diacorte = substr($fchcorte,8,2);
connect();
$porcen = 0;
$sql="SELECT ano, interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
if ($tipo=="T")
{
	$titulo = "A�O ".$ano." TRANSPORTES PENDIENTES POR MES ";
	$swinteres = 0;
}
else
{
	$tipo = "P";
	$titulo = "A�O ".$ano." PENSIONES PENDIENTES POR MES ";
}
$subtitulo = "CORTE A ".fecha_texto($fchcorte);
if ($swinteres > 0)
	$subtitulo = $subtitulo." (INCLUYE INTERES POR MORA)"; 
if ($swcampo==1)
	$subtitulo = $subtitulo." (NUMEROS DE PAGO)"; 
if ($swcampo==2)
	$subtitulo = $subtitulo." (HOJAS DE ENTRADA)"; 
if ($swretirados > 0)
	$subtitulo = $subtitulo." ALUMNOS RETIRADOS"; 
//alumnos
$filepag = "y".$ano."pagos";
$filedet = "y".$ano."pagosdet";
$cursos = "y".$ano."cursos";
$sql="SELECT matriculas.id_alumno, matriculas.grado, matriculas.curso, matriculas.matricula, matriculas.estado, matriculas.fchretiro, alumnos.apellidos, matriculas.id_resppagos, alumnos.nombres FROM matriculas, alumnos WHERE matriculas.ano = '$ano' ";
if ($swretirados > 0) {
	$sql.="AND matriculas.estado='RETIRADA'";
} else {
	$sql.="AND matriculas.estado='CERRADA'";
}
if ($grado != "")
	$sql.="AND matriculas.grado = '$grado' ";
if ($curso != "")
	$sql.="AND matriculas.curso = '$curso' ";
$sql.="AND matriculas.id_alumno = alumnos.id_alumno ORDER BY CAST(matriculas.grado AS SIGNED), matriculas.curso, alumnos.apellidos, alumnos.nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',7);
			$this->Cell(8,5,'Curso',1,0,'C',1);
			$this->Cell(44,5,'Apellidos y Nombres',1,0,'L',1);
			for($i=2; $i<=11 ;$i++)
			{
				if ($i <= 9)
					$mes = "0".$i;
				else
					$mes = $i;
				$this->Cell(13,5,substr(strtoupper(nombre_mes($mes)),0,3),1,0,'C',1);
			}			
			$this->Cell(14,5,"TOTAL",1,1,'C',1);
			$this->SetFont('Arial','',7);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("SALDOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$totcur = array();
	$totfin = array();
	for($i=2; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$totcur[$mes] = 0;
		$totfin[$mes] = 0;
	}			
	//Lista de Alumnos
	$numalu = 0;
	$acumcur = 0;			
	$acumfin = 0;			
	$curso = "";
	foreach($datalum as $regalum)
	{
		if ($swsalto > 0 and $curso!=$regalum->curso) //saltar hoja por curso
		{
			if ($curso!="") //imprime totales
			{
				$pdf->SetFont('Arial','B',6);
				if ($swcampo > 0)
					$pdf->Cell(0,5,"ALUMNOS ".$numalu,1,1,'L',1);
				else
				{
					$pdf->Cell(52,5,"ALUMNOS ".$numalu,1,0,'L',1);
					for($i=2; $i<=11 ;$i++)
					{
						if ($i <= 9)
							$mes = "0".$i;
						else
							$mes = $i;
						$pdf->Cell(13,5,number_format($totcur[$mes],0),1,0,'R',1);					
						$totcur[$mes] = 0;
					}
					$pdf->Cell(14,5,number_format($acumcur,0),1,1,'R',1);
				}
				$pdf->AddPage('P',$papel);			
			}
			$curso = $regalum->curso;
			$acumcur = 0;
			$numalu = 0;
		}
		//cargar valores alumno
		$estado= $regalum->estado;
		$id_alumno = $regalum->id_alumno;
                $grado = $regalum->grado;
                //cargar valores del grado
                $metodo = "I"; //Intereses o Rangos
                $pensionbas = 0;
                $pensionfin = 0;
                $pensionret = 0;
                $pensionmit = 0;
                $sql="SELECT pension, pensionfin, pensionret FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
                $datgrad=viewsql($sql);
                foreach($datgrad as $reggrad)
                {
                    $pensionbas = $reggrad->pension;
                    $pensionfin = $reggrad->pensionfin;
                    $pensionret = $reggrad->pensionret;
                    $pensionmit = round($pensionbas/2,0);
                }
                if ($pensionfin > 0 and $pensionret > 0) {
                    $metodo = "R";
                }
		if ($tipo=="T")
			$valores = valores_transporte($id_alumno,$ano);
		else
			$valores = valores_pension($id_alumno,$ano);
		$pagos = array();
		$saldos = array();
                $ajupen = array();
		if ($swinteres > 0)
			$salint = array();
		if ($swcampo > 1)			
			$campoid = array();
		$saldotot = 0;
		for($i=1; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			$pagos[$mes] = 0;
			$saldos[$mes] = 0;
                        $ajupen[$mes] = 0;
			if ($swinteres > 0)
				$salint[$mes] = "";
			if ($swcampo > 0)			
				$campoid[$mes] = "";
		}
                //cargar ajustes pensiones
                $pagosaju = "y".$ano."pagosaju";
                $sql="SELECT mes, valor FROM $pagosaju WHERE id_alumno = '$id_alumno' AND tipo = 'P'";
                $dataju = viewsql($sql);
                foreach($dataju as $regaju)
                {
                    if (intval($regaju->mes) <= 9)
                        $mes = "0".$regaju->mes;
                    else
                        $mes = $regaju->mes;
                    $ajupen[$mes] = $ajupen[$mes] + 1;
                    $valores[$mes] = intval($regaju->valor);
                }
                //cargar pagos detallados
		if ($swcampo <= 0)
			$sql="SELECT id_pago, mes, valor, interes, interesliq, interesaju, fchpago FROM $filedet WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' AND ano = '$ano' ORDER BY mes, id_pago";
		else
			$sql="SELECT $filedet.id_pago, $filedet.mes, $filedet.valor, $filedet.interes, $filedet.interesliq, $filedet.interesaju, $filedet.fchpago, $filepag.id_entrada FROM $filedet, $filepag WHERE $filedet.id_alumno = '$id_alumno' AND $filedet.ano = '$ano' AND $filedet.tipo = '$tipo' AND $filedet.id_pago = $filepag.id_pago ORDER BY $filedet.mes, $filedet.id_pago";
		$datpag = viewsql($sql);
                //acumular valores pagados por mes e intereses liquidados 
                $nmes = 1;
                $fchpago = "";
		foreach($datpag as $regpag)
		{
                        $nmes = $regpag->mes;
                        $fchpago = $regpag->fchpago;
			if ($nmes <= 9)
				$mes = "0".$regpag->mes;
			else
				$mes = $regpag->mes;
                        //acumular pagos
			$pagos[$mes] = $pagos[$mes] + intval($regpag->valor);
                        $saldos[$mes] = $saldos[$mes] + intval($regpag->valor);
			if ($swinteres > 0)
			{
				$sint = intval($regpag->interesliq) - intval($regpag->interes) - intval($regpag->interesaju);
				$salint[$mes] = $salint[$mes] + $sint;
			}
                        //cargar campo a imprimir
			if ($swcampo==1)
				$campoid[$mes] = $regpag->id_pago;
			if ($swcampo==2)
				$campoid[$mes] = $regpag->id_entrada;
		}
		//calcular saldos pendientes restando pension menos pagos
		for($i=1; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
                        //pension rangos y no han habido pagos del mes
                        if ($metodo=="R" and $i > 1 and $pagos[$mes] <= $pensionmit and $ajupen[$mes] <= 0) {
                            if ($i == $mescorte and $diacorte >= 16) {
                                $valores[$mes] = $pensionfin;  
                            }
                            if ($i < $mescorte or $anocorte > $ano) {
                               $valores[$mes] = $pensionret;  
                            }
                        }
                        //calcular saldo pendiente
			$saldo = $valores[$mes] - $pagos[$mes];
			$interes = 0;
			if ($i > $mescorte)
				$saldo = 0;
			if ($metodo=="I" and $tipo=="P" and $swinteres > 0 and $porcen > 0)
			{
				$interes = 0;
				if ($saldo > 0)
					$interes = interes_pension($ano,$mes,$saldo,$porcen,$fchcorte);
				$saldo = $saldo + $salint[$mes] + $interes;
			}
			$saldos[$mes] = $saldo;
			$saldotot = $saldotot + $saldo;			
		}
		if ($swdeben == 0 or ($swdeben==1 and $saldotot > 500) and $estado!='PENDIENTE')
		{
			$numalu = $numalu + 1;
			$pdf->SetFont('Arial','',7);
			if ($regalum->curso!="")
				$pdf->Cell(8,5,$regalum->curso,0,0,'L');
			else
				$pdf->Cell(8,5,$regalum->grado,0,0,'L');
			$pdf->Cell(44,5,substr($regalum->apellidos." ".$regalum->nombres,0,28),0,0,'L');
			//$pdf->Cell(44,5,$fchpago." nmes ".$nmes,0,0,'L');
			if ($swcampo > 0)
				$saldotot = 0;			
			for($i=2; $i<=11 ;$i++)
			{
				if ($i <= 9)
					$mes = "0".$i;
				else
					$mes = $i;
				if ($swcampo <= 0)
				{
					$pdf->Cell(13,5,number_format($saldos[$mes],0),0,0,'R');
					$acumcur = $acumcur + $saldos[$mes];			
					$acumfin = $acumfin + $saldos[$mes];			
					$totcur[$mes] = $totcur[$mes] + $saldos[$mes];
					$totfin[$mes] = $totfin[$mes] + $saldos[$mes];
				}
				else
				{
					$pdf->Cell(13,5,$campoid[$mes],0,0,'R');
					if ($campoid[$mes]=="")
						$saldotot = $saldotot + 1;
				}
			}			
			if ($swcampo <= 0)
				$pdf->Cell(14,5,number_format($saldotot,0),0,1,'R');
			else
				$pdf->Cell(14,5,$saldotot." M",0,1,'R');
			if ($swretirados > 0)
				$pdf->Cell(52,5,fecha_texto($regalum->fchretiro),0,1,'R');
                        if ($swcobro > 0) {
                            $id_resppagos = $regalum->id_resppagos;
                            $sql = "SELECT tipo, apellidos, nombres, telefono, telefofi, celular, correo FROM familiares WHERE id_familiar = '$id_resppagos' LIMIT 1";
                            $datmatr = viewsql($sql);
                            foreach($datmatr as $regmatr)
                            {
				$pdf->Cell(8,5,"",0,0,'C');
				$pdf->Cell(80,5,substr($regmatr->tipo,0,6).", ".$regmatr->nombres." ".$regmatr->apellidos." CEL: ".$regmatr->celular." TEL: ".$regmatr->telefono." OFI: ".$regmatr->telefofi,0,1,'L');
                            }
                        }
		}
	}
	$pdf->SetFont('Arial','B',6);
	if ($swcampo > 0)
		$pdf->Cell(0,5,"ALUMNOS ".$numalu,1,1,'L',1);
	else
	{
		$pdf->Cell(53,5,"ALUMNOS ".$numalu,1,0,'L',1);
		for($i=2; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			if ($swsalto > 0)
				$pdf->Cell(13,5,number_format($totcur[$mes],0),1,0,'R',1);
			else
				$pdf->Cell(13,5,number_format($totfin[$mes],0),1,0,'R',1);
		}
		if ($swsalto > 0)
			$pdf->Cell(14,5,number_format($acumcur,0),1,1,'R',1);
		else
			$pdf->Cell(14,5,number_format($acumfin,0),1,1,'R',1);
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
