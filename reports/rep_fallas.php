<?php
/***********************************************************************************************
* @file        : rep_fallas.php (ancho hoja 196)                                               *
* @brief       : fallas detalladas por fecha, alum,no y asignaturas                            *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 17-may-2012                                                    *
* @Modificado  : Nora Rodriguez 17-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php"); 
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$fchini = isset($_GET['fchini']) ? $_GET['fchini'] : null ;
$fchfin = isset($_GET['fchfin']) ? $_GET['fchfin'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "TODAS" ;
$id_categoria = isset($_GET['id_categoria']) ? $_GET['id_categoria'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$sinjust = isset($_GET['sinjust']) ? $_GET['sinjust'] : 0 ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$fchini = fec_amd($fchini);
$fchfin = fec_amd($fchfin);
$titulo = $tipo." POR FECHA ";
$subtitulo = fecha_larga($fchini)." AL ".fecha_larga($fchfin);
connect();
//categorias
$sql="select id_categoria, tip_categoria, categoria from categorias where tip_categoria = 'INASISTENCIAS' order by id_categoria";
$datcateg=viewsql($sql);
$tipo = substr($tipo,0,1);
//novedades alumnos
$fallas = "y".$ano."fallas";
$cursos = "y".$ano."cursos";
$sql="SELECT $fallas.fecha, $fallas.fchfin, $fallas.fchjust, $fallas.hcita, $fallas.tipo, $fallas.id_categoria, $fallas.id_alumno, $cursos.grado, $cursos.curso, $cursos.apellidos, $cursos.nombres ".
	"FROM $fallas, $cursos WHERE date_format($fallas.fecha,'%Y-%m-%d') between '$fchini' and '$fchfin' AND $fallas.id_asignatura = 0 ";
if ($tipo != "T") {
    $sql.="AND $fallas.tipo = '$tipo' ";
} else {
    $sql.="AND ($fallas.tipo = 'I' OR $fallas.tipo = 'R' OR $fallas.tipo = 'P') "; 
}
if ($sinjust > 0) //Sin justificar
{
    $sql.="AND $fallas.id_categoria = '0' ";
    $titulo.=" (SIN JUSTIFICAR)"; 
}   
if ($id_categoria > 0)	
	$sql.="AND $fallas.id_categoria = '$id_categoria' ";
if ($id_alumno > 0)
	$sql.="AND $fallas.id_alumno = '$id_alumno' ";
else
{
	if ($grado != "")
	{
		$sql.="AND $cursos.grado = '$grado' ";
		$subtitulo.=" - GRADO ".$grado;
	}
	if ($curso != "")
	{
		$sql.="AND $cursos.curso = '$curso' ";
		$subtitulo.=" CURSO ".$curso;
	}
}
$sql.="AND $fallas.id_alumno = $cursos.id_alumno ".
	"ORDER BY $fallas.fecha, $cursos.grado,  $cursos.curso, $cursos.apellidos, $cursos.nombres";
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$tipo = $GLOBALS["tipo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(2);
			$this->SetFont('Arial','B',6);
			$this->Cell(18,5,'Fecha',1,0,'C',1);
			$this->Cell(7,5,'Grado',1,0,'C',1);
			$this->Cell(7,5,'Curso',1,0,'C',1);
			$this->Cell(55,5,'Apellidos y Nombres',1,0,'L',1);
			$this->Cell(10,5,'ID',1,0,'C',1);
                        $this->Cell(18,5,'Hasta el',1,0,'C',1);
                        $this->Cell(8,5,'Hora',1,0,'C',1);
			$this->Cell(55,5,'Concepto',1,0,'C',1);
			$this->Cell(18,5,'Justificada',1,1,'C',1);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("NOVEDADES");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	$pdf->SetFont('Arial','',7);
	//lista de novedades de alumnos
	$fecha = "";
	$id_alumno = "";
	foreach($datalum as $regalum)
	{
		
		if ($regalum->id_alumno != $id_alumno or $regalum->fecha != $fecha)
		{
                    if ($regalum->id_alumno != $id_alumno)
                        $id_alumno = $regalum->id_alumno;
                    if ($regalum->fecha != $fecha)
                        $fecha = $regalum->fecha;
                    $pdf->Cell(18,5,fecha_dia($regalum->fecha),'LR',0,'L');
                    $pdf->Cell(7,5,$regalum->grado,'LR',0,'C');
                    $pdf->Cell(7,5,$regalum->curso,'LR',0,'C');
                    $pdf->Cell(55,5,substr($regalum->apellidos." ".$regalum->nombres,0,32),'LR',0,'L');
		}
		else
		{
			$pdf->Cell(18,5,"",'LR',0,'L');
			$pdf->Cell(7,5,"",'LR',0,'C');
			$pdf->Cell(7,5,"",'LR',0,'C');
			$pdf->Cell(55,5,"",'LR',0,'L');
		}
                $pdf->Cell(10,5,$regalum->id_alumno,'LR',0,'C');
                if ($tipo=="P") {
                    $pdf->Cell(18,5,fecha_dia($regalum->fchfin),'LR',0,'L');
                }
                else {
                    $pdf->Cell(18,5,"",'LR',0,'L');
                }
                $pdf->Cell(8,5,$regalum->hcita,'LR',0,'C');
                $categoria = "";
		foreach($datcateg as $regcateg)
		{
                   if ($regalum->id_categoria == $regcateg->id_categoria) {
                        $categoria = $regcateg->categoria;
                        break;
                   }
                }
                if ($tipo=="T") {
                    $pdf->Cell(5,5,$regalum->tipo,'LR',0,'C');
                    $pdf->Cell(50,5,$categoria,'LR',0,'L');
                } else {
                    $pdf->Cell(55,5,$categoria,'LR',0,'L');
                }
                $pdf->Cell(18,5,fecha_dia($regalum->fchjust),'LR',1,'L');
	}
	$pdf->SetFont('Arial','B',6);
	$pdf->Cell(0,5," TOTAL REGISTROS: ".count($datalum),1,1,'L',1);
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
