<?php
/***********************************************************************************************
* @file        : rep_profesorescar.php (ancho hoja 196)                                        *
* @brief       : Listado carga acad�mica de Docentes por a�o y grado                           *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = $_GET["ano"];
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($implogo == "")
	$implogo = "";
if ($papel == "")
	$papel = "Letter";
$titulo = "CARGA ACAD�MICA DE DOCENTES";
$subtitulo = "A�O ".$ano;
connect();
//profesores asignaturas
$profasig = "y".$ano."profasig";
$sql = "SELECT asignaturas.asignatura, asignaturas.intensidad, $profasig.* FROM asignaturas, $profasig WHERE asignaturas.ano = '$ano' ";
if ($grado != "")
{
	$sql.="AND asignaturas.grado = '$grado' ";
	$subtitulo.=" GRADO ".$grado;
}
if ($curso != "")
{
	$sql.="AND $profasig.curso = '$curso' ";
	$subtitulo.=" CURSO ".$curso;
}
$sql.="AND asignaturas.id_asignatura = $profasig.id_asignatura ".
"order by $profasig.profesor, sign($profasig.grado), cast(SUBSTRING_INDEX($profasig.grado, '-', 1) as decimal), $profasig.curso, asignaturas.asignatura";
$datprof=viewsql($sql);
//echo "$sql<br>";
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',10);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
		$this->SetFont('Arial','B',6);
		$this->Cell(50,5,'Apellidos y Nombres',1,0,'L',1);
		$this->Cell(8,5,'Grado',1,0,'C',1);
		$this->Cell(8,5,'Curso',1,0,'C',1);
		$this->Cell(112,5,'Asignatura',1,0,'C',1);
		$this->Cell(8,5,'I.H',1,0,'C',1);
		$this->Cell(0,5,'TOTAL',1,1,'C',1);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("CARGA");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
$pdf->SetFont('Arial','',6);
$id_profesor = "";
foreach($datprof as $regprof)
{
	if ($regprof->id_profesor != $id_profesor)
	{
		if ($id_profesor != "")
			$pdf->Cell(0,0,'',1,1);
		$total = 0;
		$id_profesor = $regprof->id_profesor;
		$pdf->Cell(50,5,substr($regprof->profesor,0,32),'LR',0,'L');
	}
	else
	{
		$pdf->Cell(50,5,"",'LR',0,'L');
	}
	$total = $total + intval($regprof->intensidad); 	
	$pdf->Cell(8,5,$regprof->grado,'LR',0,'L');
	$pdf->Cell(8,5,$regprof->curso,'LR',0,'L');
	$pdf->Cell(112,5,substr($regprof->asignatura,0,40),'LR',0,'L');
	$pdf->Cell(8,5,$regprof->intensidad,'LR',0,'C');
	$pdf->Cell(0,5,$total,'LR',1,'C');
}
$pdf->SetFont('Arial','B',6);
$pdf->Cell(0,5," TOTAL DOCENTES: ".count($datprof),1,1,'L',1);
$pdf->Output();
disconnect();
?>
