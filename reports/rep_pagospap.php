<?php
/***********************************************************************************************
* @file        : rep_pagospap.php (ancho hoja 196)                                             *
* @brief       : papeletas de alumnos con deuda en pension o matriculas                        *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 13-oct-2015                                                    *
* @Modificado  : Nora Rodriguez 13-sep-2018                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$fchcorte = isset($_GET['fchini']) ? $_GET['fchini'] : date('d-m-Y') ;
$fchlimite = isset($_GET['fchlim']) ? $_GET['fchlim'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$id_ruta = isset($_GET['id_ruta']) ? $_GET['id_ruta'] : null ;
$swvalores = isset($_GET['swvalores']) ? $_GET['swvalores'] : 1 ; //Imprimir valores en deuda
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$fchcorte = fec_amd($fchcorte);
$anocorte = substr($fchcorte,0,4);
$mescorte = substr($fchcorte,5,2);
$diacorte = substr($fchcorte,8,2);
$filepag = "y".$ano."pagosdet";
connect();
$banco = "";
$sql="SELECT banco FROM institucion limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$banco = $reganol->banco;}
$porcen = 0;
$sql="SELECT ano, interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
$filepag = "y".$ano."pagosdet";
if ($tipo=="T")
	$sql="SELECT transporte.id_alumno, transporte.grado, transporte.curso, transporte.codigo, transporte.id_ruta, alumnos.apellidos, alumnos.nombres, alumnos.genero FROM transporte, alumnos WHERE transporte.ano = '$ano' AND transporte.estado = 'ACTIVO' AND transporte.id_alumno = alumnos.id_alumno ";
else
	$sql="SELECT matriculas.id_alumno, matriculas.grado, matriculas.curso, alumnos.apellidos, alumnos.nombres, alumnos.genero FROM matriculas, alumnos WHERE  matriculas.ano = '$ano' AND matriculas.estado = 'CERRADA' AND matriculas.id_alumno = alumnos.id_alumno ";
if ($id_alumno > 0)
{
	if ($tipo=="T")
		$sql.="AND transporte.id_alumno = '$id_alumno' LIMIT 1";
	else
		$sql.="AND matriculas.id_alumno = '$id_alumno' LIMIT 1";
}
else
{	
	if ($grado != "")
		$sql.="AND grado = '$grado' ";
	if ($curso != "")
		$sql.="AND curso = '$curso' ";
	if ($tipo=="P" and $id_ruta > 0)
		$sql.="AND id_ruta = '$id_ruta' ";
	$sql.="ORDER BY CAST(grado AS SIGNED), curso, apellidos, nombres";
}
$datalu=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
		}
		function Footer()
		{
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("PAPELETAS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	//lista de Alumnos
	$numalu = 0;
	$gradonom = "";
	$pdf->AddPage('P',$papel);
	foreach($datalu as $regalu)
	{
		$id_alumno = $regalu->id_alumno;
                $grado = $regalu->grado;
                //cargar valores del grado
                $metodo = "I"; //Intereses o Rangos
                $pensionbas = 0;
                $pensionfin = 0;
                $pensionret = 0;
                $pensionmit = 0;
                $sql="SELECT pension, pensionfin, pensionret FROM gradosval WHERE ano = '$ano' AND grado = '$grado' LIMIT 1";
                $datgrad=viewsql($sql);
                foreach($datgrad as $reggrad)
                {
                    $pensionbas = $reggrad->pension;
                    $pensionfin = $reggrad->pensionfin;
                    $pensionret = $reggrad->pensionret;
                    $pensionmit = round($pensionbas/2,0);
                }
                if ($pensionfin > 0 and $pensionret > 0) {
                    $metodo = "R";
                }
		if ($tipo=="T")
                {
                    $metodo="";
                    $valores = valores_transporte($id_alumno,$ano);
                } else {
                    $valores = valores_pension($id_alumno,$ano);
                }
		$pagos = array();
		$pagosint = array();
		$saldos = array();
		$saldosint = array();
                $ajupen = array();
		for($i=1; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
                        $ajupen[$mes] = 0;
			$pagos[$mes] = 0;
			$pagosint[$mes] = 0;
			$saldos[$mes] = 0;
			$saldosint[$mes] = 0;
		}
                //cargar ajustes pensiones
                if ($tipo=="P") {
                    $pagosaju = "y".$ano."pagosaju";
                    $sql="SELECT mes, valor FROM $pagosaju WHERE id_alumno = '$id_alumno' AND tipo = 'P'";
                    $dataju = viewsql($sql);
                    foreach($dataju as $regaju)
                    {
                        if (intval($regaju->mes) <= 9)
                            $mes = "0".$regaju->mes;
                        else
                            $mes = $regaju->mes;
                        $ajupen[$mes] = $ajupen[$mes] + 1;
                        $valores[$mes] = intval($regaju->valor);
                    }
                }
                //cargar pagos
		$alutotpen = 0;
		$sql="SELECT id_pago, tipo, mes, valor, interes, interesliq, interesaju FROM $filepag WHERE id_alumno = '$id_alumno' AND tipo = '$tipo'";
		$datpag = viewsql($sql);
		foreach($datpag as $regpag)
		{
			$mes = $regpag->mes;
			if ($mes <= 9)
				$mes = "0".$mes;
			else
				$mes = $mes;
			$pagos[$mes] = $pagos[$mes] + intval($regpag->valor);
			$pagosint[$mes] = $pagosint[$mes] + intval($regpag->interes);
			$sint = intval($regpag->interesliq) - intval($regpag->interes) - intval($regpag->interesaju);
			$saldosint[$mes] = $saldosint[$mes] + $sint;
		}
		//calcular saldos
		for($i=2; $i<=11 ;$i++)
		{
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
                        //pension rangos y no han habido pagos del mes
                        if ($metodo=="R" and $i > 1 and $pagos[$mes] <= $pensionmit and $ajupen[$mes] <= 0) {
                            if ($i == $mescorte and $diacorte >= 16) {
                                $valores[$mes] = $pensionfin;  
                            }
                            if ($i < $mescorte or $anocorte > $ano) {
                               $valores[$mes] = $pensionret;  
                            }
                        }
			$saldopen = $valores[$mes] - $pagos[$mes];
			$interes = 0;
			if ($i > $mescorte)
				$saldopen = 0;
			if ($tipo=="P" and $saldopen > 0  and $porcen > 0 and $i > 1)
				$interes = interes_pension($ano,$mes,$saldopen,$porcen,$fchcorte);
			$interes = $interes + $saldosint[$mes];
			$saldos[$mes] = $saldopen;
			$saldosint[$mes] = $interes;
			$alutotpen = $alutotpen + $saldopen;
		}
		if ($alutotpen > 500)
		{		
			$numalu = $numalu + 1;
			if ($numalu > 1)
				$pdf->Cell(0,9,"",0,1);
			$pdf->SetFont('Arial','B',12);
			$pdf->SetTextColor(0,80,0);
			$pdf->Cell(0,6,$_SESSION["instinom"],0,1,'C');
			$pdf->SetFont('Arial','B',9);
			if ($tipo=="T")
			{
				$id_ruta = $regalu->id_ruta;
				$codigo = $regalu->codigo;
				$pdf->Cell(0,6,"SOLICITUD DE PAZ Y SALVO TRANSPORTES A�O ".$ano,0,1,'C');
				$ruta = "";
				$sql = "SELECT ruta FROM rutas WHERE id_ruta = '$id_ruta' LIMIT 1";
				$datrut = viewsql($sql);
				foreach($datrut as $regrut)
				{$ruta = $regrut->ruta;}
			}
			else
                        {
                            $pdf->Cell(0,6,"SOLICITUD DE PAZ Y SALVO PENSIONES A�O ".$ano,0,1,'C');
                        }
			$pdf->SetTextColor(0,0,0);
			$pdf->Cell(0,8,"FECHA DE CORTE ".fecha_texto($fchcorte),0,1,'C');
			$pdf->SetFont('Arial','B',9);
			$alumno = "ALUMNO";
			if ($regalu->genero=="F")
				$alumno = "ALUMNA";
			$pdf->Cell(100,7,$alumno.": ".$regalu->nombres." ".$regalu->apellidos,'TL',0);
			$pdf->SetTextColor(0,0,0);
			if ($regalu->grado!=$gradonom)
			{
				$ngrado = $regalu->grado;
				$sql = "SELECT gradonom FROM grados WHERE grado = '$ngrado' LIMIT 1";
				$datgra = viewsql($sql);
				foreach($datgra as $reggra)
				{$gradonom = $reggra->gradonom;}
			}
			if ($tipo=="T")
				$pdf->Cell(0,6,$gradonom." CURSO ".$regalu->curso." - RUTA ".$ruta." CODIGO ".$codigo,'TR',1,'R');
			else
				$pdf->Cell(0,6,$gradonom." CURSO ".$regalu->curso,'TR',1,'R');
			$pdf->SetTextColor(600,0,0);
			$pdf->SetFont('Arial','B',10);
			$pdf->Cell(196,5,"REFERENCIA DE PAGO No. ".$id_alumno,'LR',1,'C');
                        $pdf->SetTextColor(0,0,0);
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(196,4,"SE�OR PADRE DE FAMILIA:",'LR',1,'L');
			if ($tipo=="T")
			{
				$titmes = "Transporte";
				$pdf->SetFont('Arial','',8);
				$pdf->Cell(0,5,"NOS PERMITIMOS RECORDARLE QUE EL PAGO DEL SERVICIO DE TRANSPORTE SE DEBE EFECTUAR DURANTE LOS PRIMEROS DIEZ DIAS",'LR',1,'L');
				$pdf->Cell(104,5,"DE CADA MES SEGUN LO ACORDADO EN EL MOMENTO DE LA MATRICULA.",'L',0,'L');
				$pdf->SetFont('Arial','B',8);
				$pdf->Cell(0,5,"QUEREMOS INFORMARLE QUE SE DEBEN LOS MESES DE: ",'R',1,'L');
			
			}
			else
			{
				$titmes = "Pensi�n";
				$pdf->Cell(0,5,"NOS PERMITIMOS RECORDARLE EL PAGO DE LAS PENSIONES DE CONFORMIDAD CON LO ACORDADO EN EL CONTRATO FIRMADO POR",'LR',1,'L');
				$pdf->Cell(62,5,"USTED EN EL MOMENTO DE LA MATRICULA. ",'L',0,'L');
				$pdf->SetFont('Arial','B',8);
				$pdf->Cell(0,5,"QUEREMOS INFORMARLE QUE SE DEBEN LOS MESES DE:",'R',1,'L');
			}
			if ($swvalores > 0)
			{
				$pdf->SetFont('Arial','U',7);
				$pdf->Cell(15,4,"",'L',0);
				for($i=2; $i<=11 ;$i++)
				{
                                        if ($i <= 9)
                                                $mes = "0".$i;
                                        else
                                                $mes = $i;
                                        if ($saldos[$mes] > 0)
					{
						$nommes = substr(strtoupper(nombre_mes($i)),0,3);
						$pdf->Cell(16,4,$nommes,0,0,'R');
					}
				}
				$pdf->Cell(16,4,'TOTAL',0,0,'R');
				$pdf->Cell(0,4,"",'R',1);
				$pdf->SetFont('Arial','',8);
				$totpen = 0;
				$totint = 0;
				$totfin = 0;
				$pdf->Cell(15,4,$titmes,'L',0);
				for($i=2; $i<=11 ;$i++)
				{
                                    if ($i <= 9)
                                            $mes = "0".$i;
                                    else
                                            $mes = $i;
                                    if ($saldos[$mes] > 0)
                                    {
					$pdf->Cell(16,4,number_format($saldos[$mes],0),0,0,'R');
					$totpen = $totpen + $saldos[$mes];
					$totfin = $totfin + $saldos[$mes];
                                    }
				}
				$pdf->Cell(16,4,number_format($totpen,0),0,0,'R');
				$pdf->Cell(0,4,"",'R',1);
			}
			else
			{
				$meses = "";
				for($i=2; $i<=11 ;$i++)
				{
                                        if ($i <= 9)
                                                $mes = "0".$i;
                                        else
                                                $mes = $i;
                                        if ($saldos[$mes] > 0)
					{
						$nommes = strtoupper(nombre_mes($i));
						if ($meses=="")
							$meses = $meses.$nommes;
						else
							$meses = $meses.", ".$nommes;
					}
				}
				$pdf->Cell(0,16,$meses,'LR',1,'L');
			}
			if ($tipo=="T") {
                        if ($fchlimite!="")
                        {
                            $fchlim=fecha_completa($fchlimite);
                            $fchlim=substr($fchlim,0,strlen($fchlim)-5);
                            $pdf->SetFont('Arial','B',7.4);
                            $pdf->Cell(0,2,"",'LR',1);
                            $pdf->Cell(0,4,"LE INFORMAMOS QUE SU HIJO NO SERA TRANSPORTADO DESDE EL ".$fchlim." HASTA NO ESTAR A PAZ Y SALVO POR ESTE CONCEPTO",'LR',1,'L');
                        }
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(0,4,"",'LR',1);
			$pdf->Cell(0,4,"POR FAVOR DEVUELVA AL COLEGIO ESTA CIRCULAR FIRMADA POR EL PADRE O ACUDIENTE.",'LR',1,'L');
                        $pdf->Cell(0,4,"",'LR',1);
                        $pdf->SetFont('Arial','B',8);
                        $pdf->SetTextColor(0,80,0);
                        $pdf->Cell(148,5,$banco,'LB',0,'L');
                        $pdf->SetTextColor(0,0,0);
                        $pdf->SetFont('Arial','',8);
			$pdf->Cell(48,5,"FIRMA DEL PADRE O ACUDIENTE",'BTR',1,'R'); 
		    }
			else {
			$pdf->SetFont('Arial','',8);
			$pdf->Cell(0,4,"",'LR',1);
			$pdf->Cell(0,5,"NOTA:  RECUERDE QUE EN EL TOTAL DE SU DEUDA NO SE INCLUYEN LOS INTERESES GENERADOS POR MORA EN EL PAGO. POR TAL",'LR',1,'L');
			$pdf->Cell(0,5,"MOTIVO LO INVITAMOS A QUE SE COMUNIQUE CON EL COLEGIO PARA LIQUIDAR SUS INTERESES Y AS� PUEDA CONSIGNAR EL VALOR REAL  ",'LR',1,'L');
                        $pdf->Cell(0,5,"DE LA DEUDA.",'LR',1,'L');
                        $pdf->SetFont('Arial','B',8);
                        $pdf->SetTextColor(0,80,0);
                        $pdf->Cell(148,5,$banco,'LB',0,'L');
                        $pdf->SetTextColor(0,0,0);
			$pdf->Cell(48,5,"FIRMA DEL PADRE O ACUDIENTE",'BTR',1,'R');
			}
		}
		if ($numalu == 3) //Hasta 3 papeletas por hoja
		{
			$pdf->AddPage('P',$papel);
			$numalu = 0;
		}
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
