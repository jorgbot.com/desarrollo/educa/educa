<?php
/***********************************************************************************************
* @file        : profe_rangos.php (ancho hoja 196)                                             *
* @brief       : estadisticas profesores por rango de edades                                   *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 7-nov-2017                                                    *
* @Modificado  : Nora Rodriguez 7-nov-2017                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set("America/Bogota");
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$antano = $ano - 1;
$titulo = "ESTADISTICAS DE PROFESORES SEG�N RANGOS DE EDAD Y G�NERO";
$subtitulo = "A�O LECTIVO ".$ano;
connect();
//PROFESORES
$sql="SELECT profesorescont.ano, profesores.fchnace, profesores.genero FROM profesorescont, profesores WHERE profesorescont.ano = '$ano' AND profesorescont.estado = 'ACTIVO' AND profesorescont.id_profesor = profesores.id_profesor ";
$data=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(3);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(7);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(1);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(4);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
        //totalizar
        $mrango1 = 0;
	$mrango2 = 0; 
	$mrango3 = 0;
	$mrango4 = 0;
	$mrango5 = 0;
	$frango1 = 0;
	$frango2 = 0;
	$frango3 = 0;
	$frango4 = 0;
	$frango5 = 0;
	foreach($data as $regprof)
	{
		$edad = calculo_edad($regprof->fchnace);
		if($edad <= 25){
			if($regprof->genero == "M"){
			$mrango1=$mrango1+1;			
			}
			else {
			$frango1=$frango1+1;	
			}
			
		}
		if($edad >= 26 and $edad <= 30){
			if($regprof->genero == "M"){
			$mrango2=$mrango2+1;			
			}
			else {
			$frango2=$frango2+1;	
			}
			
		}
		if($edad >= 31 and $edad <= 35){
			if($regprof->genero == "M"){
			$mrango3=$mrango3+1;			
			}
			else {
			$frango3=$frango3+1;	
			}
		}
		if($edad >= 36 and $edad <= 40){
			if($regprof->genero == "M"){
			$mrango4=$mrango4+1;			
			}
			else {
			$frango4=$frango4+1;	
			}
			
		}
		if($edad >= 41){
			if($regprof->genero == "M"){
			$mrango5=$mrango5+1;			
			}
			else {
			$frango5=$frango5+1;	
			}
			
		}
        }
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("PROFESORES");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//titulos
 	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(30,7,"EDAD RANGO",1,0,'C',1);
	$pdf->Cell(15,7,'20 - 25',1,0,'C',1);
	$pdf->Cell(15,7,'26 - 30',1,0,'C',1);
	$pdf->Cell(15,7,'31 - 35',1,0,'C',1);
	$pdf->Cell(15,7,'36 - 40',1,0,'C',1);
	$pdf->Cell(15,7,'41 o +',1,1,'C',1);
	//Masculino			
	$pdf->SetFont('Arial','',8);
        $pdf->Cell(30,7,'MASCULINO',1,0,'C');
	$pdf->Cell(15,7,number_format($mrango1,0),1,0,'C');
        $pdf->Cell(15,7,number_format($mrango2,0),1,0,'C');
        $pdf->Cell(15,7,number_format($mrango3,0),1,0,'C');
	$pdf->Cell(15,7,number_format($mrango4,0),1,0,'C');
	$pdf->Cell(15,7,number_format($mrango5,0),1,1,'C');
        //Femenino
	$pdf->SetFont('Arial','',8);
	$pdf->Cell(30,7,'FEMENINO',1,0,'C');
	$pdf->Cell(15,7,number_format($frango1,0),1,0,'C');
        $pdf->Cell(15,7,number_format($frango2,0),1,0,'C');
        $pdf->Cell(15,7,number_format($frango3,0),1,0,'C');
	$pdf->Cell(15,7,number_format($frango4,0),1,0,'C');
        $pdf->Cell(15,7,number_format($frango5,0),1,1,'C');
	//Totales			
	$pdf->Cell(30,7,"TOTAL",1,0,'C');
	$pdf->Cell(15,7,number_format($mrango1+$frango1),1,0,'C');
	$pdf->Cell(15,7,number_format($mrango2+$frango2),1,0,'C');
	$pdf->Cell(15,7,number_format($mrango3+$frango3),1,0,'C');
	$pdf->Cell(15,7,number_format($mrango4+$frango4),1,0,'C');
	$pdf->Cell(15,7,number_format($mrango5+$frango5),1,1,'C');
	$pdf->Cell(182,5,"",0,1,0);
       
	$pdf->Output();
	disconnect();

	
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $granumrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
