<?php
/***********************************************************************************************
* @file        : rep_observaciones.php (ancho hoja 196)                                        *
* @brief       : Reporte lista de observaciones para planillas de notas                        *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 02-may-2012                                                    *
* @Modificado  : Nora Rodriguez 02-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$id_categoria = isset($_GET['id_categoria']) ? $_GET['id_categoria'] : null ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : null ;
if ($papel == "")
	$papel = "Letter";
$titulo = "LISTA DE OBSERVACIONES PARA PLANILLA";
$subtitulo = "";
connect();
class PDF extends FPDF
{
	//Page header
	function Header()
	{
		$titulo = $GLOBALS["titulo"] ;
		$subtitulo = $GLOBALS["subtitulo"] ;
		$implogo = $GLOBALS["implogo"] ;
		$this->SetFont('Arial','B',12);
		$this->SetTextColor(0,80,0);
		if ($implogo == "S")
		{
			$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
			$this->Ln(5);
			$this->Cell(17);
			$this->Cell(60,6,$_SESSION["instinom"],0,1);
			$this->Cell(17,6,"");
			$this->Cell(100,6,$titulo,0,1);
		}
		else
		{
			$this->Ln(20);
			$this->Cell(100,6,$titulo,0,1);
		}
		$this->Ln(3);
		$this->SetTextColor(0,0,0);			
		$this->SetFont('Arial','B',9);
		$this->Cell(0,6,$subtitulo,0,1);
		$this->Ln(3);
	}
	function Footer()
	{
		$fecha=fecha_texto(date('Y-m-d'));
		$hora=date("g:i:s a");
		$this->SetY(-20);
		$this->SetFont('Arial','I',8);
		$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
	}
}
//iniciar documento PDF
$pdf=new PDF();
$pdf->SetTitle("OBSERVACIONES");
$pdf->AliasNbPages();
$pdf->SetDrawColor(128,128,128);
$pdf->SetFillColor(224,231,233);
$pdf->AddPage('P',$papel);
//observaciones
$sql="SELECT observaciones.*, categorias.categoria FROM observaciones, categorias where observaciones.id_categoria = categorias.id_categoria ";
if ($id_categoria > '0')
	$sql.="and observaciones.id_categoria = '$id_categoria' ";
$sql.="order by categorias.categoria, observaciones.observacion";
$datobservacion=viewsql($sql);
$id_categoria = "";
foreach($datobservacion as $regobservacion)
{
	if ($regobservacion->id_categoria != $id_categoria)
	{
		$id_categoria = $regobservacion->id_categoria;
		$pdf->SetFont('Arial','B',7);
		$pdf->Ln(1);
		$pdf->Cell(0,5,"CATEGORIA: ".$regobservacion->categoria,1,1,'L',1);
		$pdf->Ln(1);
	}
	$estado = "";
	if ($regobservacion->inactivar > 0)
		$estado = " (INACTIVA)";
	$pdf->SetFont('Arial','',7);
	$pdf->Cell(10,4,$regobservacion->niveles,0,0,'L');
	$pdf->MultiCell(0,4,$regobservacion->observacion.$estado,0,'J');
}
$pdf->SetFont('Arial','B',7);
$pdf->Cell(0,5," TOTAL OBSERVACIONES: ".count($datobservacion),1,1,'L',1);
$pdf->Output();
disconnect();
?>
