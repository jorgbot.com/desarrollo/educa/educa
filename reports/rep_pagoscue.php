<?php
/***********************************************************************************************
* @file        : rep_cuenta.php (ancho hoja 196)                                            *
* @brief       : estado de cuenta por alumno, fecha y asignaturas                             *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 17-may-2012                                                    *
* @Modificado  : Nora Rodriguez 17-may-2012                                                    *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../database/datos_pagos.php");
include("../libs/fechas.php");
//cargar variables reporte
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$curso = isset($_GET['curso']) ? $_GET['curso'] : null ;
$fchcorte = isset($_GET['fchcorte']) ? $_GET['fchcorte'] : date('Y-m-d') ;
$implogo = isset($_GET['implogo']) ? $_GET['implogo'] : null ;
$papel = isset($_GET['papel']) ? $_GET['papel'] : "LETTER" ;
$exportar = "";
$mescorte = substr($fchcorte,5,2);
connect();
$porcen = 0;
$sql="SELECT ano, interes FROM anoslect where ano = '$ano' limit 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
//alumnos
$file = "y".$ano."pagos";
$filedet = "y".$ano."pagosdet";
$cursos = "y".$ano."cursos";
$sql="SELECT grado, curso, apellidos, nombres, id_alumno FROM $cursos ";
if ($id_alumno > 0)
	$sql.="WHERE id_alumno = '$id_alumno' LIMIT 1";
else
{
	if ($grado != "")
	{
		$sql.="WHERE grado = '$grado' ";
		$subtitulo.=" - GRADO ".$grado;
	}
	if ($curso != "")
	{
		$sql.="AND curso = '$curso' ";
		$subtitulo.=" CURSO ".$curso;
	}
	$sql.="ORDER BY CAST(grado AS SIGNED), curso, apellidos, nombres";
}
$datalum=viewsql($sql);
//echo "$sql<br>";
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
			$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("CUENTA");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//lista de Alumnos
	foreach($datalum as $regalum)
	{
		$id_alumno = $regalum->id_alumno;
		$valores = valores_pension($id_alumno,$ano);
		$pagos = pagos_pension($id_alumno,$ano);
		$valorestra = valores_transporte($id_alumno,$ano); //arreglo transportes del alumno
		$pagostra = pagos_transporte($id_alumno,$ano); //arreglo pagos del alumno
                $sql="SELECT * FROM $file WHERE id_alumno = '$id_alumno' ORDER BY fchpago, id_pago";
                $datpag = viewsql($sql);
                $sql="SELECT * FROM $filedet WHERE id_alumno = '$id_alumno' ORDER BY mes";
                $datdet = viewsql($sql);
                //ESTADO DE CUENTA
		$pdf->ln();
		$pdf->SetFont('Arial','B',12);
		$pdf->SetTextColor(0,80,0);
		$pdf->Cell(20,5,"ESTADO DE CUENTA A�O ".$ano,0,1,'L');
		$pdf->SetTextColor(0,0,0);
		$pdf->ln();
		$pdf->SetFont('Arial','B',9);
		$pdf->Cell(20,5,$regalum->apellidos." ".$regalum->nombres." - GRADO ".$regalum->grado." CURSO ".$regalum->curso,0,0,'L');
		$pdf->ln();
		$pdf->ln();
		$pdf->SetFont('Arial','B',8);
		$pdf->SetTextColor(0,80,0);
		$pdf->Cell(20,5, "SALDOS CORTE AL ".fecha_texto($fchcorte),0,1,'L');
		$pdf->SetTextColor(0,0,0);
		$pdf->SetFont('Arial','',7);	
		//titulos
		$pdf->Cell(15,6,"",'LTR',0,'L',1);
		$pdf->Cell(15,6,"Pensi�n",1,0,'C',1);
		$pdf->Cell(15,3,"Pagos",'LTR',0,'C',1);
		$pdf->Cell(15,3,"Pagos",'LTR',0,'C',1);
		$pdf->Cell(15,3,"Debe",'LTR',0,'C',1);
		$pdf->Cell(15,3,"Debe",'LTR',0,'C',1);
		$pdf->Cell(15,3,"Interes",'LTR',0,'C',1);
		$pdf->Cell(15,6,"Total",'LTR',0,'C',1);
		$pdf->Cell(15,6,"Transporte",'LTR',0,'C',1);
		$pdf->Cell(15,3,"Pagos",'LTR',0,'C',1);
		$pdf->Cell(15,3,"Debe",'LTR',0,'C',1);
		$pdf->Cell(15,6,"TOTAL",'LTR',0,'C',1);
		$pdf->Cell(0,3,"",0,1);
		$pdf->Cell(30);
		$pdf->Cell(15,3,"Pensi�n",'LRB',0,'C',1);
		$pdf->Cell(15,3,"Interes",'LRB',0,'C',1);
		$pdf->Cell(15,3,"Interes",'LRB',0,'C',1);
		$pdf->Cell(15,3,"Pensi�n",'LRB',0,'C',1);
		$pdf->Cell(15,3,"Liquid",'LRB',0,'C',1);
		$pdf->Cell(15);
		$pdf->Cell(15);
		$pdf->Cell(15,3,"Transporte",'LRB',0,'C',1);
		$pdf->Cell(15,3,"Transporte",'LRB',0,'C',1);
		$pdf->Cell(0,3,"",0,1);
		$sumvalpen = 0;
		$sumpagpen = 0;
		$sumpagint = 0;
		$sumsalint = 0;
		$sumsalpen = 0;
		$sumintere = 0;
		$sumpensio = 0;
		$sumvaltra = 0;
		$sumpagtra = 0;
		$sumsaltra = 0;
		$sumtotal = 0;
		for($i=1; $i<=11 ;$i++)
		{
			//nombres campos
			if ($i <= 9)
				$mes = "0".$i;
			else
				$mes = $i;
			$campoint = "interes".$mes;
			$campointsal = "intesal".$mes;
			$saldo = $valores[$mes] - $pagos[$mes];
			$interes = 0;
			$pension = 0;
			if ($i > intval($mescorte))
				$saldo = 0;
			$pension = $saldo + $interes + $pagos[$campointsal];
			$saldotra = $valorestra[$mes] - $pagostra[$mes];
			$saldotot = $pension + $saldotra;
			//totales
			$sumvalpen = $sumvalpen + $valores[$mes];
			$sumpagpen = $sumpagpen + $pagos[$mes];
			$sumpagint = $sumpagint + $pagos[$campoint];
			$sumsalint = $sumsalint + $pagos[$campointsal];
			$sumsalpen = $sumsalpen + $saldo;
			$sumintere = $sumintere + $interes;
			$sumpensio = $sumpensio + $pension;
			$sumvaltra = $sumvaltra + $valorestra[$mes];
			$sumpagtra = $sumpagtra + $pagostra[$mes];
			$sumsaltra = $sumsaltra + $saldotra;
			$sumtotal = $sumtotal + $saldotot;
			$nombremes = nombre_mes($mes);
			if ($i==1)
				$nombremes = "Matricula";
			//imprimir mes			
			$pdf->Cell(15,5,$nombremes,1,0,'L',1);
			$pdf->Cell(15,5,number_format($valores[$mes],0),1,0,'R');
			$pdf->Cell(15,5,number_format($pagos[$mes],0),1,0,'R');
			$pdf->Cell(15,5,number_format($pagos[$campoint],0),1,0,'R');
			$pdf->Cell(15,5,number_format($pagos[$campointsal],0),1,0,'R');
			$pdf->Cell(15,5,number_format($saldo,0),1,0,'R');
			$pdf->Cell(15,5,number_format($interes,0),1,0,'R');
			$pdf->Cell(15,5,number_format($pension,0),1,0,'R');
			$pdf->Cell(15,5,number_format($valorestra[$mes],0),1,0,'R');
			$pdf->Cell(15,5,number_format($pagostra[$mes],0),1,0,'R');
			$pdf->Cell(15,5,number_format($saldotra,0),1,0,'R');
			$pdf->Cell(15,5,number_format($saldotot,0),1,1,'R');
		}
	}
        //totales pagos
        $sumpagpen = 0;
        $sumpagint = 0;
        $sumpagtot = 0;
	$sumpagtra = 0;
        foreach($datpag as $regpag)
        {
            if ($regpag->tipo=='P')
            {
                $sumpagtot = $sumpagtot + $regpag->valor;
                $sumpagpen = $sumpagpen + $regpag->neto;
                $sumpagint = $sumpagint + $regpag->interes;
            }
            if ($regpag->tipo=='T')
            {
               $sumpagtra = $sumpagtra + $regpag->valor;
            }
        }
        $saldopen = 0;
        $saldotra = 0;
        if ($sumpagpen > $sumvalpen)
           $saldopen = $sumpagpen - $sumvalpen;
        if ($sumpagtra > $sumvalpen)
           $saldotra = $sumpagtra - $sumvaltra;
        //imprimir saldos positivos
	$pdf->Cell(15,5,"Saldo +",1,0,'L',1);
	$pdf->Cell(15,5,"",1,0,'R');
	$pdf->Cell(15,5,number_format($saldopen,0),1,0,'R');
	$pdf->Cell(15,5,"",1,0,'R');
	$pdf->Cell(15,5,"",1,0,'R');
	$pdf->Cell(15,5,"",1,0,'R');
	$pdf->Cell(15,5,"",1,0,'R');
	$pdf->Cell(15,5,"",1,0,'R');
	$pdf->Cell(15,5,"",1,0,'R');
	$pdf->Cell(15,5,number_format($saldotra,0),1,0,'R');
	$pdf->Cell(15,5,"",1,0,'R');
	$pdf->Cell(15,5,"",1,1,'R');
        //Imprimir Totales
        $pdf->SetFont('Arial','B',7);
	$pdf->Cell(15,5," TOTALES ",1,0,'L',1);
	$pdf->Cell(15,5,number_format($sumvalpen,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumpagpen,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumpagint,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumsalint,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumsalpen,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumintere,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumpensio,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumvaltra,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumpagtra,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumsaltra,0),1,0,'R',1);
	$pdf->Cell(15,5,number_format($sumtotal,0),1,1,'R',1);
	//RELACION DE PAGOS PENSION
	$pdf->ln();
	$pdf->SetFont('Arial','B',8);
	$pdf->SetTextColor(0,80,0);
	$pdf->Cell(20,5,"RELACION PAGOS PENSIONES",0,1,'L');
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','',7);
	$pdf->Cell(17,5,"Fecha Pago",1,0,'L',1);
	$pdf->Cell(24,5,"Hoja Entrada",1,0,'C',1);
	$pdf->Cell(10,5,"PagoN�",1,0,'C',1);
	$pdf->Cell(13,5,"Consigna",1,0,'C',1);
	$pdf->Cell(13,5,"Efectivo",1,0,'C',1);
	$pdf->Cell(13,5,"Total",1,0,'C',1);
	$pdf->Cell(8,5,"Mes",1,0,'C',1);
	$pdf->Cell(13,5,"PagoPens",1,0,'C',1);
	$pdf->Cell(13,5,"InteresLiq",1,0,'C',1);
	$pdf->Cell(13,5,"InteresAju",1,0,'C',1);
	$pdf->Cell(13,5,"PagoInte",1,0,'C',1);
	$pdf->Cell(13,5,"PagoMes",1,0,'C',1);
	$pdf->Cell(13,5,"DebePens",1,0,'C',1);
	$pdf->Cell(13,5,"DebeInte",1,1,'C',1);
	$pdf->SetFont('Arial','',7);
	$numpag = 0;
	$sumtotal = 0;
	$maxmes = 0;
	$saldomes = array();
	$saldoint = array();
	for($i=1; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$saldomes[$mes] = $valores[$mes];
		$saldoint[$mes] = 0;
	}
	foreach($datpag as $regpag)
	{
		if ($regpag->tipo=="P")
		{
			$numpag = $numpag + 1;
			$pdf->Cell(17,5,fecha_texto($regpag->fchpago),'LR',0,'L');
			$pdf->Cell(7,5,$regpag->id_entrada,'LR',0,'L');
			$pdf->Cell(17,5,fecha_texto($regpag->fchentrada),'LR',0,'L');
			$pdf->Cell(10,5,$regpag->id_pago,'LR',0,'L');
			$pdf->Cell(13,5,number_format($regpag->valor_con,0),'LR',0,'R');
			$pdf->Cell(13,5,number_format($regpag->valor_efe,0),'LR',0,'R');
			$pdf->Cell(13,5,number_format($regpag->valor,0),'LR',0,'R');
			$sumtotal = $sumtotal + $regpag->valor;
			$id_pago = $regpag->id_pago;
			$num = 0;
			foreach($datdet as $regdet)
			{
				if ($regdet->id_pago==$id_pago)
				{
					$num = $num + 1;
					if ($num > 1)
					{
						$pdf->Cell(17,5,"",'LR',0);
						$pdf->Cell(7,5,"",'LR',0);
						$pdf->Cell(17,5,"",'LR',0);
						$pdf->Cell(10,5,"",'LR',0);
						$pdf->Cell(13,5,"",'LR',0);
						$pdf->Cell(13,5,"",'LR',0);
						$pdf->Cell(13,5,"",'LR',0);
					}
					$mes = $regdet->mes;
					if ($mes > $maxmes)
						$maxmes = $mes;
					if ($mes < 10)
						$mes = "0".$mes;
					$saldomes[$mes] = $saldomes[$mes] - $regdet->valor;
					$nommes = substr(strtoupper(nombre_mes($regdet->mes)),0,3);
					if (intval($regdet->mes)==1)
						$nommes = "MAT";
					$salint = $regdet->interesliq - $regdet->interes - $regdet->interesaju;
					$saldoint[$mes] = $saldoint[$mes] + $salint;
					if ($salint < 0)
						$salint = 0;
					$pdf->Cell(8,5,$nommes,'LR',0,'L');
					$pdf->Cell(13,5,number_format($regdet->valor,0),'LR',0,'R');
					$pdf->Cell(13,5,number_format($regdet->interesliq,0),'LR',0,'R');
					$pdf->Cell(13,5,number_format($regdet->interesaju,0),'LR',0,'R');
					$pdf->Cell(13,5,number_format($regdet->interes,0),'LR',0,'R');
					$pdf->Cell(13,5,number_format(intval($regdet->valor)+intval($regdet->interes),0),'LR',0,'R');
					$pdf->Cell(13,5,number_format($saldomes[$mes],0),'LR',0,'R');
					$pdf->Cell(13,5,number_format($salint,0),'LR',1,'R');
				}
			}
			if (count($num) <= 0)
			{
				$pdf->Cell(8,5,"",'LR',0);
				$pdf->Cell(13,5,"",'LR',0);
				$pdf->Cell(13,5,"",'LR',0);
				$pdf->Cell(13,5,"",'LR',0);
				$pdf->Cell(13,5,"",'LR',0);
				$pdf->Cell(13,5,"",'LR',0);
				$pdf->Cell(13,5,"",'LR',0);
				$pdf->Cell(13,5,"",'LR',1);
			}
		}
	}
	$totmes = 0;
	$totint = 0;
	for($i=1; $i<=$maxmes ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$totmes = $totmes + $saldomes[$mes];
		$totint = $totint + $saldoint[$mes];
	}
        if ($saldopen > 0)
            $totmes = $saldopen * -1;
	$nommes = strtoupper(nombre_mes($mes));
	if (intval($mes)=="01")
		$nommes = "MATRICULA";
        
	$pdf->Cell(98,5,"TOTAL PAGOS ".$numpag." (SALDOS A ".$nommes.")",1,0,'L',1);
	$pdf->Cell(13,5,number_format($sumpagpen,0),1,0,'R',1);
	$pdf->Cell(26,5,"",1,0,'R',1);
	$pdf->Cell(13,5,number_format($sumpagint,0),1,0,'R',1);
	$pdf->Cell(13,5,number_format($sumpagtot,0),1,0,'R',1);
	$pdf->Cell(13,5,number_format($totmes,0),1,0,'R',1);
	$pdf->Cell(13,5,number_format($totint,0),1,1,'R',1);
        
	//RELACION DE PAGOS TRANSPORTE
	$numpag = 0;
	$sumtotal = 0;
	$saldomes = array();
	for($i=1; $i<=11 ;$i++)
	{
		if ($i <= 9)
			$mes = "0".$i;
		else
			$mes = $i;
		$saldomes[$mes] = $valorestra[$mes];
		$sumtotal = $sumtotal + $valorestra[$mes];
	}
	if ($sumtotal > 0)
	{
	$sumtotal = 0;
        $sumsaldo = 0;
	$pdf->ln();
	$pdf->SetFont('Arial','B',8);
	$pdf->SetTextColor(0,80,0);
	$pdf->Cell(20,5,"RELACION PAGOS TRANSPORTE",0,1,'L');
	$pdf->SetTextColor(0,0,0);
	$pdf->SetFont('Arial','',7);
	$pdf->Cell(17,5,"Fecha Pago",1,0,'L',1);
	$pdf->Cell(24,5,"Hoja Entrada",1,0,'C',1);
	$pdf->Cell(10,5,"PagoN�",1,0,'C',1);
	$pdf->Cell(13,5,"Consigna",1,0,'C',1);
	$pdf->Cell(13,5,"Efectivo",1,0,'C',1);
	$pdf->Cell(13,5,"Total",1,0,'C',1);
	$pdf->Cell(8,5,"Mes",1,0,'C',1);
	$pdf->Cell(13,5,"Pago",1,0,'C',1);
	$pdf->Cell(13,5,"Debe",1,1,'C',1);
	$pdf->SetFont('Arial','',7);
	foreach($datpag as $regpag)
	{
		if ($regpag->tipo=="T")
		{
			$numpag = $numpag + 1;
			$pdf->Cell(17,5,fecha_texto($regpag->fchpago),'LR',0,'L');
			$pdf->Cell(7,5,$regpag->id_entrada,'LR',0,'L');
			$pdf->Cell(17,5,fecha_texto($regpag->fchentrada),'LR',0,'L');
			$pdf->Cell(10,5,$regpag->id_pago,'LR',0,'L');
			$pdf->Cell(13,5,number_format($regpag->valor_con,0),'LR',0,'R');
			$pdf->Cell(13,5,number_format($regpag->valor_efe,0),'LR',0,'R');
			$pdf->Cell(13,5,number_format($regpag->valor,0),'LR',0,'R');
			$sumtotal = $sumtotal + $regpag->valor;
			$id_pago = $regpag->id_pago;
			$num = 0;
			foreach($datdet as $regdet)
			{
			
				if ($regdet->id_pago==$id_pago)
				{
					$num = $num + 1;
					if ($num > 1)
					{
						$pdf->Cell(17,5,"",'LR',0);
						$pdf->Cell(7,5,"",'LR',0);
						$pdf->Cell(17,5,"",'LR',0);
						$pdf->Cell(10,5,"",'LR',0);
						$pdf->Cell(13,5,"",'LR',0);
						$pdf->Cell(13,5,"",'LR',0);
						$pdf->Cell(13,5,"",'LR',0);
					}
					$mes = $regdet->mes;
					if ($mes < 10)
						$mes = "0".$mes;
					$saldomes[$mes] = $saldomes[$mes] - $regdet->valor;
                                        $sumsaldo = $sumsaldo + $saldomes[$mes];
					$nommes = substr(strtoupper(nombre_mes($regdet->mes)),0,3);
					if (intval($regdet->mes)==1)
						$nommes = "MAT";
					$pdf->Cell(8,5,$nommes,'LR',0,'L');
					$pdf->Cell(13,5,number_format($regdet->valor,0),'LR',0,'R');
					$pdf->Cell(13,5,number_format($saldomes[$mes],0),'LR',1,'R');
				}
			}
			if (count($num) <= 0)
			{
				$pdf->Cell(8,5,"",'LR',0);
				$pdf->Cell(13,5,"",'LR',0);
				$pdf->Cell(13,5,"",'LR',1);
			}
		}
	}
        if ($saldotra > 0)
            $sumsaldo = $saldotra * -1;
        
	$pdf->Cell(77,5,"TOTAL PAGOS ".$numpag."  (SALDOS A ".$nommes.")",1,0,'L',1);
	$pdf->Cell(13,5,number_format($sumtotal,0),1,0,'R',1);
	$pdf->Cell(21,5,"",1,0,'R',1);
	$pdf->Cell(13,5,number_format($sumsaldo,0),1,1,'R',1);
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $numrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
