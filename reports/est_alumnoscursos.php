<?php
/***********************************************************************************************
* @file        : est_alumnos.php (ancho hoja 196)                                              *
* @brief       : estadisticas alumnos por grados                                               *
* @version     : 1.0                                                                           *
***********************************************************************************************/

session_start();
date_default_timezone_set('America/Bogota');
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='../libs/logout.php';</script>";}
require("../../fpdf/fpdf.php");
include("../webparam.php");
include("../database/database.php");
include("../libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$nivel = isset($_GET['nivel']) ? $_GET['nivel'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$implogo = $_GET["implogo"];
$papel = $_GET["papel"];
$exportar = "";
$antano = $ano - 1;
$titulo = "ESTADISTICAS DE ALUMNOS POR CURSO";
$subtitulo = "A�O LECTIVO ".$ano;
connect();
//niveles
$sql= "select nivel, nombre from niveles order by id_nivel";
$datnive=viewsql($sql);
//alumnos
$sql="SELECT matriculas.grado, matriculas.curso, matriculas.id_alumno, grados.nivel, grados.gradonom, alumnos.genero FROM matriculas, grados, alumnos WHERE matriculas.ano = '$ano' AND matriculas.estado = 'CERRADA' AND matriculas.curso != '' AND matriculas.fchretiro like '0000%' AND matriculas.grado = grados.grado AND matriculas.id_alumno = alumnos.id_alumno ";
$condi = "";
if ($nivel != "")
{
	$nomnivel = substr($nivel,1);
	$nivel = substr($nivel,0,1);
	$condi.="AND matriculas.nivel = '$nivel' ";
	$subtitulo.=" - NIVEL ".$nomnivel;
}
if ($grado != "")
{
	$condi.="AND matriculas.grado = '$grado' ";
	$subtitulo.=" - GRADO ".$grado;
}
$sql = $sql.$condi."ORDER BY grados.id_grado ASC, matriculas.curso ASC";
$datalum=viewsql($sql);
if ($exportar=="")//PDF
{
	class PDF extends FPDF
	{
		//Page header
		function Header()
		{
			$titulo = $GLOBALS["titulo"] ;
			$subtitulo = $GLOBALS["subtitulo"] ;
			$implogo = $GLOBALS["implogo"] ;
			$this->SetFont('Arial','B',12);
			$this->SetTextColor(0,80,0);
			if ($implogo == "S")
			{
				$this->Image('../images/logo.jpg',12,10,15,0,'JPG');
				$this->Ln(5);
				$this->Cell(17);
				$this->Cell(60,6,$_SESSION["instinom"],0,1);
				$this->Cell(17,6,"");
				$this->Cell(100,6,$titulo,0,1);
			}
			else
			{
				$this->Ln(20);
				$this->Cell(100,6,$titulo,0,1);
			}
			$this->Ln(2);
			$this->SetTextColor(0,0,0);			
			$this->SetFont('Arial','B',10);
			$this->Cell(0,6,$subtitulo,0,1);
			$this->Ln(4);
		}
		function Footer()
		{
			$fecha=fecha_texto(date('Y-m-d'));
			$hora=date("g:i:s a");
			$this->SetY(-20);
			$this->SetFont('Arial','I',8);
				$this->Cell(0,10,'PAGINA '.$this->PageNo().'/{nb}'." - IMPRESO EL ".$fecha." A LAS ".$hora,0,0,'C');
		}
	}
	//iniciar documento PDF
	$pdf=new PDF();
	$pdf->SetTitle("ALUMNOS");
	$pdf->AliasNbPages();
	$pdf->SetDrawColor(128,128,128);
	$pdf->SetFillColor(224,231,233);
	$pdf->AddPage('P',$papel);
	//titulos
	$pdf->SetFont('Arial','B',8);

	$pdf->Cell(15,5,'Curso',1,0,'C',1);
	$pdf->Cell(15,5,'Alumnos',1,0,'C',1);
	$pdf->Cell(15,5,'Nuevos',1,0,'C',1);
	$pdf->Cell(15,5,'Antiguos',1,0,'C',1);
	$pdf->Cell(15,5,'Ni�as',1,0,'C',1);
	$pdf->Cell(15,5,'Ni�os',1,1,'C',1);
	//lista de Alumnos
	$alumest = array();
	$totnum = 0;
	$totnue = 0;
	$totant = 0;
	$totmas = 0;
	$totfem = 0;
	$gradonom = "";
	$curso = "";
	$pdf->SetFont('Arial','',8);
	foreach($datalum as $regalum)
	{
		if ($regalum->curso != $curso)
		{
			if ($curso != "")
			{
				
				$pdf->Cell(15,6,$curso,1,0,'C');
				$pdf->Cell(15,6,$granum,1,0,'C');
				$pdf->Cell(15,6,$granue,1,0,'C');
				$pdf->Cell(15,6,$graant,1,0,'C');
				$pdf->Cell(15,6,$grafem,1,0,'C');
				$pdf->Cell(15,6,$gramas,1,1,'C');
			}
			$curso = $regalum->curso;
			$granum = 0;
			$granue = 0;
			$graant = 0;
			$gramas = 0;
			$grafem = 0;
			$curnum = 0;
			$curnue = 0;
			$curant = 0;
			$curmas = 0;
			$curfem = 0;
		}
		if ($grado!="" and $gradonom != "" and $regalum->curso!=$curso)
		{
			if ($curso!="")
			{
				
				$pdf->Cell(15,6,$curso,1,0,'L');
				$pdf->Cell(15,6,$curnum,1,0,'C');
				$pdf->Cell(15,6,$curnue,1,0,'C');
				$pdf->Cell(15,6,$curant,1,0,'C');
				$pdf->Cell(15,6,$curfem,1,0,'C');
				$pdf->Cell(15,6,$curmas,1,1,'C');
			}
			$curso = $regalum->curso;
			$curnum = 0;
			$curnue = 0;
			$curant = 0;
			$curmas = 0;
			$curfem = 0;
		}
		$granum = $granum + 1;
		$curnum = $curnum + 1;
		$totnum = $totnum + 1;
		$id_alumno = $regalum->id_alumno;
		$estado = "NUE";
		$sql = "SELECT id_matricula FROM matriculas WHERE id_alumno = '$id_alumno' AND matriculas.ano = '$antano' LIMIT 1";
		$datmatr = viewsql($sql);
		if (count($datmatr) > 0)
			$estado = "ANT";
		$alumest[$id_alumno] = $estado;
		if ($estado == "NUE")
		{
			$granue = $granue + 1;
			$curnue = $curnue + 1;
			$totnue = $totnue + 1;
		}
		else
		{
			$graant = $graant + 1;
			$curant = $curant + 1;
			$totant = $totant + 1;
		}
		if ($regalum->genero == "M")
		{
			$gramas = $gramas + 1;
			$curmas = $curmas + 1;
			$totmas = $totmas + 1;
		}
		else
		{
			$grafem = $grafem + 1;
			$curfem = $curfem + 1;
			$totfem = $totfem + 1;
		}
	}
	if ($grado!="")
	{
	
		$pdf->Cell(15,6,$curso,1,0,'L');
		$pdf->Cell(15,6,$curnum,1,0,'C');
		$pdf->Cell(15,6,$curnue,1,0,'C');
		$pdf->Cell(15,6,$curant,1,0,'C');
		$pdf->Cell(15,6,$curfem,1,0,'C');
		$pdf->Cell(15,6,$curmas,1,1,'C');
	}
	else
	{
		
		$pdf->Cell(15,6,$curso,1,0,'C');
		$pdf->Cell(15,6,$granum,1,0,'C');
		$pdf->Cell(15,6,$granue,1,0,'C');
		$pdf->Cell(15,6,$graant,1,0,'C');
		$pdf->Cell(15,6,$grafem,1,0,'C');
		$pdf->Cell(15,6,$gramas,1,1,'C');
	}
	$pdf->SetFont('Arial','B',8);
	$pdf->Cell(15,6,'TOTAL',1,0,'C',1);
	$pdf->Cell(15,6,number_format($totnum,0),1,0,'C',1);
	$pdf->Cell(15,6,number_format($totnue,0),1,0,'C',1);
	$pdf->Cell(15,6,number_format($totant,0),1,0,'C',1);
	$pdf->Cell(15,6,number_format($totfem,0),1,0,'C',1);
	$pdf->Cell(15,6,number_format($totmas,0),1,1,'C',1);
	//totales por niveles
	if ($nivel=="" and $grado=="")
	{
		$pdf->Ln(10);
		$pdf->Cell(25,6,'Nivel',1,0,'C',1);
		$pdf->Cell(15,6,'Alumnos',1,0,'C',1);
		$pdf->Cell(15,6,'Nuevos',1,0,'C',1);
		$pdf->Cell(15,6,'Antiguos',1,0,'C',1);
		$pdf->Cell(15,6,'Ni�as',1,0,'C',1);
		$pdf->Cell(15,6,'Ni�os',1,1,'C',1);
		$pdf->SetFont('Arial','',8);
		$totnum = 0;
		$totnue = 0;
		$totant = 0;
		$totmas = 0;
		$totfem = 0;
		$nivel = "";
		foreach($datnive as $regnive)
		{
			$nivel = $regnive->nivel;
			$pdf->Cell(25,6,$regnive->nombre,1,0,'L');
			$totnum = 0;
			$totnue = 0;
			$totant = 0;
			$totmas = 0;
			$totfem = 0;
			foreach($datalum as $regalum)
			{
				if ($regalum->nivel == $nivel)
				{
					$totnum = $totnum +1;
					$id_alumno = $regalum->id_alumno;
					if ($alumest[$id_alumno] == "NUE")
						$totnue = $totnue +1;
					else
						$totant = $totant +1;
					if ($regalum->genero == "M")
						$totmas = $totmas +1;
					else
						$totfem = $totfem +1;
				}
			}
			$pdf->Cell(15,6,$totnum,1,0,'C');
			$pdf->Cell(15,6,$totnue,1,0,'C');
			$pdf->Cell(15,6,$totant,1,0,'C');
			$pdf->Cell(15,6,$totfem,1,0,'C');
			$pdf->Cell(15,6,$totmas,1,1,'C');
		}
	}
	$pdf->Output();
	disconnect();
}
else //XLS o TXT
{
	disconnect();
	?>
	<script language="javascript" type='text/JavaScript'>
	var exportar='<?php echo $exportar;?>';
	var numrat='<?php echo $granumrat;?>';
	var formato='<?php echo $formato;?>';
	var theURL= '../export/liqexpo.php?exportar='+exportar+'&numrat='+numrat+'&formato='+formato+'&exparea='+exparea+'&expsubarea='+expsubarea;
	window.open(theURL,'','width=550,  height=400, top=0, left=0, toolbar=no, menubar=yes, location=no, directories=0, status=0, scrollbar=1, resizable=yes');
	window.close();
	</script>
	<?php
}
?>
