<?php
/***********************************************************************************************
* @file        : profesores_select.php                                                         *
* @brief       : Lista profesores para selecci�n                                               *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 24-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 24-abr-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
connect();
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$buscar = isset($_POST['buscar']) ? $_POST['buscar'] : null ;
$ano = $_SESSION["anoaca"];
//traer profesores
$numprof = 0;
if ($buscar!="" or $grado!="")
{
	$sql="SELECT profesores.id_profesor, profesores.apellidos, profesores.nombres, profesores.cargo, profesores.estado, profesorescont.grados FROM profesores, profesorescont ".
	"WHERE profesores.id_profesor = profesorescont.id_profesor ";
	if ($buscar != "")
	{
		if ($buscar > 0)
			$sql.="AND (profesores.id_profesor = '$buscar' or profesores.docunum = '$buscar') ";
		else
			$sql.="AND (profesores.apellidos like '%$buscar%' or profesores.nombres like '%$buscar%') ";
	}
	else
	{
		$sql.="AND profesorescont.ano = '$ano' ";
		if ($grado != "")
			$sql.="AND profesorescont.grados LIKE '% $grado %' ";
	}
	$sql.= "ORDER BY profesores.apellidos, profesores.nombres";
	$datprof=viewsql($sql);
	$numprof = count($datprof);
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	var grado = document.form0.grado.value;
	var theURL = 'profesores_select.php?grado='+grado;
	window.location.href = theURL;
}
function devuelve(campo1,campo2)
{
	eval ("opener.document.form1.id_profesor.value='" +  campo1 + "'")
	eval ("opener.document.form1.profesor.value='" +  campo2 + "'")
	window.close()
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="40">
		<td class="titulo"><img src='images/profesor.jpg' width="40" height="40" border="0" align="absmiddle"> Seleccionar profesor</td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
<form action="" method="post" name="form0">
<input type="hidden" name="op" value="buscar">
	<tr><td colspan="3" height="15"></td></tr>
	<tr height="32">
		<td width="6" background="images/backsilver401.gif"></td>
		<td align="right" background="images/backsilver402.gif">
			BUSCAR...
			<input type="text" name="buscar" value="<?php echo $buscar;?>" size="20" style="text-transform:uppercase;">
			&nbsp;
			<select name="grado" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
			<option value="" selected>Grado ...</option>
			<?php
			$sql = "SELECT grado, gradonom FROM grados order by id_grado";
			$datgrad = viewsql($sql);
			foreach($datgrad as $reggrad)
			{
				if ($grado==$reggrad->grado)
					echo "<option value='$reggrad->grado' selected>GRADO $reggrad->gradonom</option>";
				else
					echo "<option value='$reggrad->grado'>GRADO $reggrad->gradonom</option>";
			}
			?>																
			</select>
			<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
			&nbsp;
		</td>
		<td width="6" background="images/backsilver403.gif"></td>
	</tr>
</form>
</table>
<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="95%" align="center">
	<tr height="20">
		<th>Apellidos y Nombres</th>
		<th>Cargo</th>
		<th>Grados</th>
		<th width="50">Estado</th>
	</tr>
	<?php
	if ($numprof > 0)
	{
	foreach($datprof as $regprof)
	{
	$id_profesor = $regprof->id_profesor;
	$profesor = trim($regprof->apellidos)." ".trim($regprof->nombres);
	?>		
	<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
		<td><a href="javascript:;" onClick="devuelve('<?php echo $id_profesor;?>','<?php echo $profesor;?>');"><img src="images/ok.png" border="0" width="16" height="16" alt="Seleccionar" title="" hspace="0" align="absmiddle"><?php echo $regprof->apellidos." ".$regprof->nombres;?></a></td>
		<td><?php echo $regprof->cargo;?></td>
		<td><b><?php echo $regprof->grados;?></b></td>
		<td><?php echo $regprof->estado;?></td>
	</tr>
	<?php } } else { ?>
	<tr><td colspan="6" bgcolor="#FFFFFF" height="100" class="subtitulo" align="center">Asigne una condici�n de b�squeda...</td></tr>
	<?php } ?>
	<tr height="36">
		<td colspan="6" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
</body>
</html>
<?php disconnect();?>
