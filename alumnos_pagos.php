<?php
/***********************************************************************************************
* @file        : alumnos_pagos.php                                                             *
* @brief       : Historial de Pagos del Alumno                                                 *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 20-jul-2015                                                    *
* @Modificado  : Nora Rodriguez 25-jul-2015   
 * ********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("database/datos_pagos.php");
include("libs/fechas.php");
$id_alumno = $_GET["id_alumno"];
$ano = isset($_GET['ano']) ? $_GET['ano'] : $_SESSION["anoaca"] ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
$origen = "ALUMNO";
$opcion = "pagos";
$fchpago = date('Y-m-d');
$mespago = substr($fchpago,5,2);
$diapago = substr($fchpago,8,2);
connect();
include("includes/menu_titulo.php");
$alumno = "";
$porcen = 0;
if ($tipo=="P")
	$sql="SELECT matriculas.ano, matriculas.grado, matriculas.curso, matriculas.interes, alumnos.apellidos, alumnos.nombres FROM matriculas, alumnos WHERE matriculas.id_alumno = '$id_alumno' AND matriculas.ano = '$ano' AND  matriculas.id_alumno = alumnos.id_alumno LIMIT 1";
else
	$sql="SELECT transporte.ano, transporte.grado, transporte.curso, alumnos.apellidos, alumnos.nombres FROM transporte, alumnos WHERE transporte.id_alumno = '$id_alumno' AND transporte.ano = '$ano' AND  transporte.id_alumno = alumnos.id_alumno LIMIT 1";
$datmat=viewsql($sql);
foreach($datmat as $regmat)
{
	$alumno = $regmat->apellidos." ".$regmat->nombres;
	$grado = $regmat->grado;
	$curso = $regmat->curso;
        if ($tipo=="P")
           $porcen = $regmat->interes; 
}
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function imprimir()
{
	var id_alumno = document.form1.id_alumno.value;
	var ano = document.form1.ano.value;
	var theURL = 'reports/rep_pagoscue.php?id_alumno='+id_alumno+'&ano='+ano;
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
function seleccionar()
{
	var ano = document.form0.ano.value;
	var id_alumno = document.form0.id_alumno.value;
	var theURL = 'alumnos_pagos.php?id_alumno='+id_alumno+'&ano='+ano;
	window.location.href = theURL;
}
function seleccionarTipo()
{
	var ano = document.form1.ano.value;
	var id_alumno = document.form1.id_alumno.value;
	var tipo = document.form1.tipo.value;
	var theURL = 'alumnos_pagos.php?id_alumno='+id_alumno+'&ano='+ano+'&tipo='+tipo;
	window.location.href = theURL;
}
</script>
</head>
<body>
<?php include("includes/menu_alumnos.php");?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
<form action="javascript:imprimir();" method="post" name="form1">
<input type="hidden" name="id_alumno" value="<?php echo $id_alumno;?>">
<input type="hidden" name="ano" value="<?php echo $ano;?>">
	<tr height="450" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<br><br>
			<table width="100%" border="0" cellpadding="0" cellspacing="0">
			<tr valign="top">
			<td width="100" class="subtitulo">
				<?php if ($tipo=="P") { ?>
				<input type="radio" name="tipo" value="P" checked onChange="javascript:seleccionarTipo();"> Pensiones
				<br><br>
				<input type="radio" name="tipo" value="T" onChange="javascript:seleccionarTipo();"> Transporte
				<?php } else { ?>
				<input type="radio" name="tipo" value="P" onChange="javascript:seleccionarTipo();"> Pensiones
				<br><br>
				<input type="radio" name="tipo" value="T" checked onChange="javascript:seleccionarTipo();"> Transporte
				<?php } ?>
			</td>
			<td align="center">
			<?php include("includes/pagos_histo.php");?>
			</td>
			<td width="30"></td>
			</tr>
			</table>
		</td>
	</tr>
	<tr height="35">
		<th align="right" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<a href="javascript:;" onClick="imprimir();"><img src="images/btn_imprimir.png" border="0" width="70" height="24" alt="Imprimir" hspace="5" align="absmiddle"></a>
			&nbsp;&nbsp;
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="10" align="absmiddle"></a>
		</th>
	</tr>
</form>
</table>
</BR></BR></BR>
</body>
</html>
<?php disconnect();?>
