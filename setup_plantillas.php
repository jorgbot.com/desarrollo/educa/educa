<?php
/***********************************************************************************************
* @file        : setup_plantillas.php                                                          *
* @brief       : Lista y edita plantillas de documentos tipo  
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 12-abr-2012                                                    *
* @Modificado  : Nora Rodriguez 12-abr-2012                                                    *
* @Notes       : TINYMCE, editor opciones por pagina principal documentation - reference       *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_setup.php");
include("libs/fechas.php");
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : null ;
$id_plantilla = isset($_GET['id_plantilla']) ? $_GET['id_plantilla'] : null ;
$categoria = isset($_GET['categoria']) ? $_GET['categoria'] : null ;
if ($opcion == "")
	$opcion = "listar";
connect();
//lista de categorias
$sql="select lisnom from listas where listip = 'PLANTILLAS' order by lisnom";
$datcate=viewsql($sql);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.plantilla.value == '')
	{alert('Falta asignar descripci�n del plantilla...');document.form1.plantilla.focus(); return false;}
	if(document.form1.texto.value == '')
	{alert('Falta texto de plantilla...');document.form1.texto.focus(); return false;}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar esta plantilla?");
	if(ok)
	{
		document.form1.op.value='borrarplantilla';
		document.form1.submit();
	}
}
function seleccionar()
{
	var categoria = document.form0.categoria.value;
	var theURL = 'setup_plantillas.php?categoria='+categoria;
	window.location.href = theURL;
}
function imprimir_alumno()
{
	var id_plantilla = document.form1.id_plantilla.value;
	var id_alumno = document.form1.id_alumno.value;
	if (id_alumno > 0)
	{
		var consecutivo = document.form1.consecutivo.value;
		var theURL = 'reports/certificados_alum.php?id_plantilla='+id_plantilla+'&id_alumno='+id_alumno+'&consecutivo='+consecutivo+'&implogo=N';
		var winName='';
		var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
		window.open(theURL,winName,features);
	}
	else
	{alert('Falta seleccionar Alumno...');}
}
function imprimir_profesor()
{
	var id_plantilla = document.form1.id_plantilla.value;
	var id_profesor = document.form1.id_profesor.value;
	if (id_profesor > 0)
	{
		var consecutivo = document.form1.consecutivo.value;
		var theURL = 'reports/certificados_prof.php?id_plantilla='+id_plantilla+'&id_profesor='+id_profesor+'&consecutivo='+consecutivo+'&implogo=N';
		var winName='';
		var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
		window.open(theURL,winName,features);
	}
	else
	{alert('Falta seleccionar Docente...');}
}
</script>
<!-- TinyMCE -->
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript"> 
	tinyMCE.init({
       mode : "textareas",
	   theme : "advanced",
	   plugins : "spellchecker,advhr,preview",
	   theme_advanced_buttons1 : "bold,italic,underline,|,justifyleft,justifycenter,justifyright,|,bullist,numlist,|,outdent,indent,|,forecolor,backcolor,fontsizeselect,spellchecker,advhr,|,charmap,|,undo,redo,|,removeformat,|,code,|,preview",
	   theme_advanced_buttons2 : "",
	   theme_advanced_buttons3 : "",
	   theme_advanced_toolbar_location : "bottom",
	   theme_advanced_toolbar_align : "left",
	   theme_advanced_statusbar_location : "",
	   theme_advanced_resizing : true
	});
</script>
<!-- /TinyMCE -->
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/plantillas.jpg' width="40" height="40" border="0" align="absmiddle"> Plantillas Documentos</td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<?php if ($opcion == "listar") { ?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="380" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
			<form action="" method="post" name="form0">
				<tr height="20">
					<th width="30">ID</th>
					<th width="100">CATEGORIA</th>
					<th align="left">PLANTILLA</th>
				</tr>
				<?php
				$sql="SELECT id_plantilla, categoria, plantilla FROM plantillas order by plantilla";
				$datplan=viewsql($sql);
				foreach($datplan as $regplan)
				{
				$id_plantilla = $regplan->id_plantilla;
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><a class="link_list" href="setup_plantillas.php?opcion=editar&id_plantilla=<?php echo $id_plantilla;?>" title="ID plantilla"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $id_plantilla;?></a></td>
					<td><?php echo $regplan->categoria;?></td>
					<td><?php echo $regplan->plantilla;?></td>
				</tr>
				<?php } ?>
			</form>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivpar"] >= 3) { ?>
			<a href="setup_plantillas.php?opcion=editar"><img src="images/btn_nuevo.png" border="0" width="75" height="24" alt="Nuevo Registro" hspace="10"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
<br><br>
<?php } if ($opcion == "editar") {
$titulo = "Nueva Plantilla";
$datplantilla = datos_plantilla($id_plantilla);
if ($id_plantilla > 0)
	$titulo = "Editar Plantilla";
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_setup.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarplantilla'>
<input type='hidden' name='id_plantilla' value='<?php echo $id_plantilla;?>'>
	<tr height="10">
		<td width="30"></td>
		<td></td>
		<td width="30"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr height="30">
					<td width="80">Categor�a</td>
					<td>
						<select name="categoria" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php
						foreach($datcate as $regcate)
						{
							if ($datplantilla["categoria"] == $regcate->lisnom)
								echo "<option value='$regcate->lisnom' selected>$regcate->lisnom</option>";
							else
								echo "<option value='$regcate->lisnom'>$regcate->lisnom</option>";
						}
						?>																
						</select>
					</td>
				</tr>
				<tr height="30">
					<td>T�tulo</td>
					<td>
						<input type="text" name="plantilla" size="120" value="<?php echo  $datplantilla["plantilla"];?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">						
					</td>
				</tr>
				<tr valign="top">
					<td>Contenido</td>
					<td>
					<textarea name="texto" rows="15" cols="120" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datplantilla["texto"];?></textarea>
					</td>
				</tr>
			</table>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<th colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($id_plantilla > 0) { if ($datplantilla["categoria"]=="ALUMNOS") { ?>
			Consecutivo N�
			<input type="text" name="consecutivo" value="" style="width: 80px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			<input type="hidden" name="id_alumno">
			<input type="text" name="alumno" value="Seleccionar Alumno..." style="width: 230px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			<a href="alumnos_select.php" onclick="newWindow(this.href, '', 600, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_lupa.png' border="0" width="24" height="24" alt="Buscar" align="absmiddle" title="Seleccionar Alumno..."></a>
			<a href="javascript:;" onClick="imprimir_alumno();" title="Imprimir"><img src="images/btn_imprimir.png" border="0" width="75" height="24" alt="Cerrar" hspace="2" align="absmiddle"></a>
			<?php } else { ?>
			Consecutivo N�
			<input type="text" name="consecutivo" value="" style="width: 80px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			<input type="hidden" name="id_profesor">
			<input type="text" name="profesor" value="Seleccionar Docente..." style="width: 230px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			<a href="profesores_select.php" onclick="newWindow(this.href, '', 800, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_lupa.png' border="0" width="24" height="24" alt="Buscar" align="absmiddle" title="Seleccionar Docente..."></a>
			<a href="javascript:;" onClick="imprimir_profesor();" title="Imprimir"><img src="images/btn_imprimir.png" border="0" width="75" height="24" alt="Cerrar" hspace="2" align="absmiddle"></a>
			<?php } }?>
			<?php if (($_SESSION["nivpar"] >= 2 and $datplantilla["setup"]==0) or $_SESSION["usulog"]=="ADMIN") { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="2" alt="Grabar" align="absmiddle">
			<?php } if ($id_plantilla!="" and $_SESSION["nivpar"] >= 4 and $datplantilla["setup"]==0) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="2" align="absmiddle"></a>
			<?php } ?>
			<a href="setup_plantillas.php"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="2" align="absmiddle"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="2" align="absmiddle"></a>
		</th>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
