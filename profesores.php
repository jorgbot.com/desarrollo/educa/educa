<?php
/***********************************************************************************************
* @file        : profesores.php                                                                *
* @brief       : Muestra la lista general de profesores para selecci�n o creaci�n              *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 28-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 28-feb-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivpro"] <= 0)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</script>";}
include("database/database.php");
include("libs/fechas.php");
connect();
$op = isset($_POST['op']) ? $_POST['op'] : null ;
$estado = isset($_GET['estado']) ? $_GET['estado'] : null ;
$buscar = "";
if ($op == "buscar")
	$buscar = $_POST['buscar'];
$id_profesor = 0;
if ($_SESSION["usutip"]=="DOCENTE")
	$id_profesor = $_SESSION["usunro"];
$chkact = "";
$chkret = "checked";
if ($estado != "RETIRADO")
{
	$estado = "ACTIVO";
	$chkact = "checked";
	$chkret = "";
}
?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>EDUCA</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="robots" content="noindex"/>
<meta name="googlebot" content="noindex"/>
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function seleccionar()
{
	if (document.form0.estado[0].checked==true)
		var theURL = 'profesores.php?estado=ACTIVO';
	else	
		var theURL = 'profesores.php?estado=RETIRADO';
	window.location.href = theURL;
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#FFFF66">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="150" bgcolor="#006600">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr height="40" bgcolor="#006600">
		<td background="fondos/top.jpg"></td>
		<td background="fondos/top.jpg" class="titulo_color">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_top.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/top.jpg"></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_top.jpg"></td></tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="150">
					<?php include("includes/menu.php");?>
				</td>
				<td width="10"></td>
				<td>
					<div class="div_marco">
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
						<tr height="40">
							<td width="50" align="center"><img src='images/profesor.png' width="40" height="40" border="0" align="absmiddle"></td>
							<td class="titulo">Docentes</td>
							<td align="right">
							</td>
						</tr>
					</table>
					</div>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<form action="" method="post" name="form0">
					<input type="hidden" name="op" value="buscar">
						<tr><td colspan="3" height="15"></td></tr>
						<tr height="32">
							<td width="6" background="images/backsilver401.gif"></td>
							<td class="titulo_color" background="images/backsilver402.gif"><font color="#006633"><input type="radio" name="estado" value="ACTIVO" <?php echo $chkact;?> onClick="javascript:seleccionar();"> Activos &nbsp;&nbsp; <input type="radio" name="estado" value="RETIRADO" <?php echo $chkret;?> onClick="javascript:seleccionar();"> Retirados</font></td>
							<td align="right" background="images/backsilver402.gif">
								BUSCAR...
								<input type="text" name="buscar" value="<?php echo $buscar;?>" size="20" style="text-transform:uppercase;"> 
								<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle"> 
								<?php if ($_SESSION["nivpro"] >= 3) { ?>
								&nbsp;
								<a href="profesores_edit.php" onclick="newWindow(this.href, '', 900, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
								<?php } ?>
							</td>
							<td width="6" background="images/backsilver403.gif"></td>
						</tr>
					  </form>
					</table>
					<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="100%">
					  <tr height="20"> 
						<th width="210">Apellidos y Nombres</th>
						<th width="36">ID</th>
						<th width="27">Tipo</th>
						<th>Cargo</th>
						<th width="27">Est</th>
						<th>Asignat</th>
						<th>Horario</th>
					  </tr>
						<?php
							$sql="SELECT id_profesor, apellidos, nombres, tipo, estado, docunum, cargo FROM profesores ";
							if ($id_profesor > 0)
								$sql.="WHERE id_profesor = '$id_profesor' ";
							else
							{
								if ($buscar != "")
								{
									if ($buscar > 0)
										$sql.="WHERE id_profesor = '$buscar' or docunum = '$buscar' ";
									else
										$sql.="WHERE apellidos like '%$buscar%' or nombres like '%$buscar%' ";
								}
								else
								{
									$sql.="WHERE estado = '$estado' ";
								}
							}
							$sql.= "ORDER BY apellidos, nombres";
							$datprof=viewsql($sql);
							foreach($datprof as $regprof)
							{
							$idprof = $regprof->id_profesor;
							$seccion = "seccion".$idprof;
						?>
					  <tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left"> 
						<td><?php echo substr($regprof->apellidos." ".$regprof->nombres,0,32);?></td>
						<td><a class="link_list" name="<?php echo $seccion;?>" href="profesores_edit.php?id_profesor=<?php echo $idprof;?>" onclick="newWindow(this.href, '', 1000, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="ID Docente"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $idprof;?></a></td>
						<td><?php echo substr($regprof->tipo,0,3);?></td>
						<td><?php echo $regprof->cargo;?></td>
						<td><?php echo substr($regprof->estado,0,3);?></td>
						<td align="center" title="Asignaturas"> 
						  <?php if ($regprof->tipo=="DOCENTE") { ?>
						  <a class="link_list" href="profesores_asignat.php?id_profesor=<?php echo $idprof;?>" title="Asignaturas" onclick="newWindow(this.href, '', 650, 500, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src="images/asignaturasm.png" width="20" height="20" border="0" hspace="0" align="absmiddle"></a> 
						  <?php }?>
						</td>
						<td align="center" title="Horario"> 
						  <?php if ($regprof->tipo=="DOCENTE") { ?>
						  <a class="link_list" href="horarios_prof.php?id_profesor=<?php echo $idprof;?>" title="Horario" onclick="newWindow(this.href, '', 1024, 550, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src="images/horariosm.png" width="20" height="20" border="0" hspace="0" align="absmiddle"></a> 
						  <?php }?>
						</td>
					  </tr>
					  <?php } ?>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_bot.jpg"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#D3A919"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FFDB4B"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FCF463"></td></tr>
	<tr>
		<td colspan="3" valign="top">
		<div id="fondo_pie">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</div>
		</td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>