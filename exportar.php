<?php
/***********************************************************************************************
* @file        : exportar.php                                                                  *
* @brief       : Opciones de exportaci�n de archivos a Excel                                   *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 28-Nov-2018                                                    *
* @Modificado  : Nora Rodriguez 28-Nov-2018                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");    
include("database/datos_alumnos.php");
include("libs/fechas.php");
$opcion = isset($_POST['opcion']) ? $_POST['opcion'] : null ;
if ($opcion == "")
	$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : null ;
if ($opcion=="exportar") {
 echo"<script>window.document.location.href='exportar.php';</script>";    
}
$ano = isset($_POST['ano']) ? $_POST['ano'] : null ;
if ($ano <= 0)
	$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
if ($opcion == "")
	$opcion = "menu";
connect();
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script src="js/jquery-2.1.4.js"></script>
<script language='JavaScript'>
function validar()
{
	var opcion = document.form1.opcion.value;
	return true;
}
function imprimir()
{
 
var opcion = document.form1.opcion.value;
var anio_lect = document.form1.ano.value;

var radio_btn = $('input:radio[name=rad_dian]:checked').val();
var grado_desd = $("#grado_desd").val();
var grado_hast = $("#grado_hast").val();
var nuev = $("#nuevo").val();
if (  nuev == 1 ){
    alert("con la opcion SI solo se mostraran los alumnos matriculados los ultimos 2 meses");
}

if( radio_btn   ==   'general'){
location.href = "excel/com_excel/listado_general.php" }
if( radio_btn   ==   'padres'){
location.href = "excel/com_excel/listado_general_padres.php" }
if( radio_btn   ==   'madres'){
location.href = "excel/com_excel/listado_general_madres.php" }
if( radio_btn   ==   'acudientes'){
location.href = "excel/com_excel/listado_general_acudientes.php" }
if( radio_btn   ==   'rutas'){
location.href = "excel/com_excel/listado_ruta.php" }
if( radio_btn   ==   'matriculados'){
location.href = "excel/com_excel/matriculados.php" }
if( radio_btn   ==   'carnets'){
location.href = "excel/com_excel/Carnes.php" }
if( radio_btn   ==   'Reptientes'){
location.href = "excel/com_excel/repitentes.php" }
if( radio_btn  ==  'hospital'){		    		 			
location.href = "excel/com_excel/hospital_sur.php" }
if( radio_btn  ==  'areas'){		    		 			
location.href = "excel/com_excel/areas_pendientes.php" }
if( radio_btn  ==  'editoriales'){		    		 														 		
location.href = "excel/com_excel/editoriales.php"
}

        
        
        if(opcion == 'dian'){
		var radio_btn = $('input:radio[name=rad_dian]:checked').val();
		var anio_lect = $("#anio_lect").val();
		var mes_lect = $("#mes_lect").val();
var fchini = $("#fchini").val();
var fchfin = $("#fchfin").val();
		if(radio_btn == "" || anio_lect == ""){
			alert("seleccione los parametros");
		}else {	

if ( radio_btn == 'nuevos_2'){  
				
location.href = "excel/com_excel/alumnos_nuevos_dian.php?anio="+anio_lect;
		    			
}else { 
           
if (anio_lect == "2019") {
alert("seleccione a�o correcto");
 }

else  {             


                                         
if(radio_btn == 'Consolidado'){
location.href = "excel/com_excel/pagos_dian_cons.php?anio="+anio_lect+"&fchini="+fchini+"&fchfin="+fchfin;
			                          }   		
			        
if(radio_btn == 'detallado'){
location.href = "excel/com_excel/pagos_dian_deta.php?anio="+anio_lect+"&fchini="+fchini+"&fchfin="+fchfin;
			                          }

if(radio_btn == '2015deuda') { 
location.href = "excel/com_excel/penstrans_deuda_dian.php?anio="+anio_lect;		
		                 }

 if(radio_btn == 'interes') {

location.href = "excel/com_excel/intereses_dian.php?anio="+anio_lect;   

}
					
if(radio_btn == 'interes_deuda') {

location.href = "excel/com_excel/interes_deuda_dian.php?anio="+anio_lect;   

}

if(radio_btn == 'nuevos_2'){
		    			
location.href = "excel/com_excel/alumnos_nuevos_dian.php?anio="+anio_lect;

}


if( radio_btn   ==   'efectivo'){
		    		
if ( mes_lect == "" ){  

alert("seleccione un mes");

}  else  {
			    								
location.href = "excel/com_excel/ingresos_efectivo_hoja.php?anio="+anio_lect+"&mess="+mes_lect;
    	     	
} 
}
} 
}
}


		return false;

	}
	var implogo = 'S';
	var	papel = 'LETTER';
	if (document.form1.papel[1].checked==true)
		papel = 'LEGAL';
	if (document.form1.chklogo.checked == false)
		implogo = '';

	
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
function seleccionar()
{
	var opcion = document.form1.opcion.value;
	var ano = document.form1.ano.value;
	var theURL = 'reportes.php?ano='+ano+'&opcion='+opcion;
	window.location.href = theURL;
}
</script>
</head>
<body>
<form action="javascript:imprimir();" method="post" name="form1" onSubmit="return validar();">
<input type="hidden" name="opcion" value="<?php echo $opcion;?>">
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
    <tr height="40">
    	<td colspan="3" valign="middle" bgcolor="#009900" class="txt_destacados" align="center">
            <img src="images/excel.png" border="0" width="40" height="40" align="absmiddle"> LISTADOS EXCEL
	</td>
    </tr>
    <tr><td colspan="3" height="10"></td></tr>
    <tr height="280" valign="top">
	<td width="20"></td>
	<td align="center">
            <table border="0" width="100%">
                <tr>
                    <td>
                        Seleccione a�o Lectivo
			<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
			<?php
			$sql="SELECT ano FROM anoslect ORDER BY ano desc";
			$datanol=viewsql($sql);
			foreach($datanol as $reganol)
			{
				$selano = "";
				if ($reganol->ano==$ano)
					$selano = "selected";
				echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
			}
			?>
			</select>
                    </td>
                </tr>
                <tr valign="top">
                    <td align="center">
			<fieldset>
			<table border="0" cellpadding="0" cellspacing="0">
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="general"> Listado General</td></tr>
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="padres"> Listado General Padres </td></tr>
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="madres"> Listado General Madres</td></tr>
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="acudientes"> Listado General Acudientes </td></tr>
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="rutas"> Listado General Rutas </td></tr>
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="matriculados"> Estudiantes Matriculados </td></tr>
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="carnets"> Listado Para Carnetizaci�n </td></tr>
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="Reptientes"> Listado de Alumnos Repitentes </td></tr>
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="hospital"> Reporte Para Hospital del Sur</td></tr>	
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="areas"> Listado de �reas Pendientes Por Alumno</td></tr>
			<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="editoriales"> Listado Para Editoriales </td></tr>
    			</table>
			</fieldset>
                    </td>
		</tr>
            </table>	
	</td>
	<td width="20"></td>
    </tr>
    <tr><td colspan="3" height="30"></td></tr>
    <tr height="35">
	<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
            <input type="image" class="botones_img" src="images/btn_imprimir.png" border="0" name="imprimir"  width="70" height="24" hspace="10" alt="Imprimir">
            <a href="reportes.php"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
            <a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
	</td>
    </tr>
</table>
</form>
</body>
</html>
<?php disconnect();?>