<?php
/***********************************************************************************************
* @file        : asignaturas_observa.php                                                       *
* @brief       : Lista y edita observaciones generales alumnos para boletin o periodo          *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 28-mar-2012                                                    *
* @Modificado  : Nora Rodriguez 28-mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : null ;
$id_observacion = isset($_GET['id_observacion']) ? $_GET['id_observacion'] : null ;
$nivel = isset($_GET['nivel']) ? $_GET['nivel'] : null ;
$buscar = isset($_POST['buscar']) ? $_POST['buscar'] : null ;
if ($opcion == "")
	$opcion = "listar";
connect();
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/validInput.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.observacion.value == '')
	{alert('Falta asignar descripci�n del observacion...');document.form1.observacion.focus(); return false;}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar esta observacion?");
	if(ok)
	{
		document.form1.op.value='borrarobservacion';
		document.form1.submit();
	}
}
function seleccionar()
{
	var nivel = document.form0.nivel.value;
	var theURL = 'asignaturas_observa.php?nivel='+nivel;
	window.location.href = theURL;
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/observa.jpg' width="40" height="40" border="0" align="absmiddle"> Observaciones Acad�micas</td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<?php if ($opcion == "listar") {
$sql="SELECT observaciones.*, categorias.categoria FROM observaciones, categorias where observaciones.id_categoria = categorias.id_categoria ";
if ($nivel != "")
	$sql.="and observaciones.niveles like '% $nivel %' ";
if ($buscar!="")
{
	$buscar = strtoupper($buscar);
	$sql.="and observaciones.observacion like = '%$buscar%' ";
}
$sql.="order by categorias.categoria, observaciones.observacion";
$datobservacion=viewsql($sql);
?>
<table border="0" cellpadding="0" cellspacing="0" width="95%" align="center">
<form action="" method="post" name="form0">
	<tr height="32">
		<td width="6" background="images/backsilver401.gif"></td>
		<td background="images/backsilver402.gif">
			<a class="link_black" href="setup_categorias.php?tip_categoria=OBSERVACIONES" onclick="newWindow(this.href, '', 400, 500, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Tabla de Categorias..."><img src="images/idicestrella.png" width="24" height="24" border="0" hspace="0" align="absmiddle">Categorias</a>
		</td>
		<td align="right" background="images/backsilver402.gif">
			<b>BUSCAR...</b>
			<input type="text" name="buscar" value="<?php echo $buscar;?>" size="30" style="text-transform:uppercase;">
			&nbsp;&nbsp;
			<select name="nivel" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
			<option value="" selected>Nivel...</option>
			<?php
			$sql= "select nivel, nombre from niveles order by id_nivel";
			$datnive=viewsql($sql);
			foreach($datnive as $regnive)
			{
				if ($nivel == $regnive->nivel)
					echo "<option value='$regnive->nivel' selected>$regnive->nombre</option>";
				else
					echo "<option value='$regnive->nivel'>$regnive->nombre</option>";
			}
			?>																
			</select>
			&nbsp;
			<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle" hspace="5">
			<?php if ($_SESSION["nivasi"] >= 4) { ?>
			<a href="asignaturas_observa.php?opcion=editar&nivel=<?php echo $nivel;?>"><img src="images/btn_nuevo.png" border="0" width="75" height="24" alt="Nuevo Registro" hspace="5" align="absmiddle"></a>
			<?php } ?>
		</td>
		<td width="6" background="images/backsilver403.gif"></td>
	</tr>
</form>
</table>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="420" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th align="left">
						Observaci�n
					</th>
					<th>Niveles</th>
					<th>Estado</th>
					<th width="40">C�digo</th>
				</tr>
				<?php
				$id_categoria = "";
				foreach($datobservacion as $regobservacion)
				{
				$id_observacion = $regobservacion->id_observacion;
				if ($regobservacion->id_categoria != $id_categoria)
				{
				$id_categoria = $regobservacion->id_categoria;
				?>
				<tr height="20" align="left">
					<td colspan="4" class="tabla_color">&nbsp;<b><?php echo $regobservacion->categoria;?></b></td>
				</tr>
				<?php
				}
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><?php echo $regobservacion->observacion;?></td>
					<td align="center"><?php echo $regobservacion->niveles;?></td>
					<td align="center"><?php if ($regobservacion->inactivar > 0) echo "INAC";?></td>
					<td><a class="link_list" href="asignaturas_observa.php?opcion=editar&id_observacion=<?php echo $id_observacion;?>&nivel=<?php echo $nivel;?>" title="C�digo"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $regobservacion->codigo;?></a></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
<br><br>
<?php } if ($opcion == "editar") {
$titulo = "Nueva Observacion";
$datobservacion = datos_observacion($id_observacion);
if ($id_observacion > 0)
	$titulo = "Editar Observacion";
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarobservacion'>
<input type='hidden' name='id_observacion' value='<?php echo $id_observacion;?>'>
<input type='hidden' name='nivel' value='<?php echo $nivel;?>'>
	<tr height="10">
		<td width="30"></td>
		<td></td>
		<td width="30"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos"><?php echo $titulo;?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr height="30"><td></td></tr>
				<tr height="30">
					<td>
						C�digo  <font color="red">*</font>
						<input type="text" name="codigo" style="width: 30px;" value="<?php echo $datobservacion["codigo"];?>" maxlength="4" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						&nbsp;
						<select name="id_categoria" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
						<?php
						$sql="select id_categoria, categoria from categorias where tip_categoria = 'OBSERVACIONES' order by categoria";
						$datcateg=viewsql($sql);
						foreach($datcateg as $regcateg)
						{
							if (intval($datobservacion["id_categoria"]) == intval($regcateg->id_categoria))
								echo "<option value='$regcateg->id_categoria' selected>$regcateg->categoria</option>";
							else
								echo "<option value='$regcateg->id_categoria'>$regcateg->categoria</option>";
						}
						?>																
						</select>
						&nbsp;&nbsp;
						<?php
						$sql= "select nivel, nombre from niveles order by id_nivel";
						$datnive=viewsql($sql);
						foreach($datnive as $regnive)
						{
							$niv = $regnive->nivel;
							$name = "nivel".$niv;
							echo "$regnive->nombre";
							if (strpos($datobservacion["niveles"],$niv) > 0) {
							?>
							<input type="checkbox" name="<?php echo $name;?>" checked> &nbsp;&nbsp;
							<?php } else { ?>
							<input type="checkbox" name="<?php echo $name;?>"> &nbsp;&nbsp;
							<?php }
						}
						?>																
					</td>
				</tr>
				<tr>
					<td>
					<textarea name="observacion" rows="4" cols="125" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datobservacion["observacion"];?></textarea>
					</td>
				</tr>
				<tr height="30">
					<td>
						<?php if ($datobservacion["director"] <= 0) { ?>
						Disponible Docentes <input type="radio" name="director" value="0" checked> &nbsp;&nbsp;
						Director de Grupo <input type="radio" name="director" value="1">
						<?php } else { ?>
						Disponible Docentes <input type="radio" name="director" value="0"> &nbsp;&nbsp;
						Director de Grupo <input type="radio" name="director" value="1" checked>
						<?php } ?>
						&nbsp;&nbsp;&nbsp;&nbsp;
						<?php if ($datobservacion["inactivar"] > 0) { ?>
						Estado Activa <input type="radio" name="inactivar" value="0"> &nbsp;&nbsp;
						Inactiva <input type="radio" name="inactivar" value="1" checked>
						<?php } else { ?>
						Activa <input type="radio" name="inactivar" value="0" checked> &nbsp;&nbsp;
						Inactiva <input type="radio" name="inactivar" value="1">
						<?php } ?>
					</td>
				</tr>
				<tr><td height="30"></td></tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 4) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_observacion!="" and $_SESSION["nivasi"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10"></a>
			<?php } ?>
			<a href="asignaturas_observa.php?nivel=<?php echo $nivel;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
