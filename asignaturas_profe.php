<?php
/***********************************************************************************************
* @file        : asignaturas_profe.php                                                         *
* @brief       : Lista y edita profesores de la asignatura                                     *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_asignaturas.php");
include("libs/fechas.php");
$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
$id_asignatura = isset($_GET['id_asignatura']) ? $_GET['id_asignatura'] : null ;
$id_profesor = isset($_GET['id_profesor']) ? $_GET['id_profesor'] : null ;
$id_profasig = isset($_GET['id_profasig']) ? $_GET['id_profasig'] : null ;
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : null ;
if ($opcion == "")
	$opcion = "listar";
connect();
//datos asignatura
$datasignatura = datos_asignatura($id_asignatura);
$ano = $datasignatura["ano"];
$grado = $datasignatura["grado"];
$asignatura = $datasignatura["asignatura"];
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
if ($grado == "")
	$grado = 11;
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.curso.value == '')
	{alert('Falta asignar Curso...');document.form1.curso.focus(); return false;}
	if(document.form1.tipoedu.value == '')
	{alert('Falta asignar tipo de educaci�n...');document.form1.tipoedu.focus(); return false;}
	if(document.form1.id_profesor.value == '')
	{alert('Falta asignar Docente...');document.form1.id_profesor.focus(); return false;}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar este Registro?");
	if(ok)
	{
		document.form1.op.value='borrarprofasig';
		document.form1.submit();
	}
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/profesor.jpg' width="40" height="40" border="0" align="absmiddle"> DOCENTES GRADO <?php echo $grado;?> - <?php echo $ano;?></td>
		<td class="titulo_campos" align="right"><?php echo $asignatura;?></td>
	</tr>
</table>
<?php if ($opcion == "listar") { ?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="250" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
				<tr height="20">
					<th width="50">Curso</th>
					<th width="80">Alumnos</th>
					<th>Docente</th>
				</tr>
				<?php
				$file = "y".$ano."profasig";
				$sql="SELECT * FROM $file WHERE id_asignatura = '$id_asignatura' order by curso";
				$datprof=viewsql($sql);
				foreach($datprof as $regprof)
				{
				$id_profasig = $regprof->id_profasig;
				$id_profesor = $regprof->id_profesor;
				$curso = $regprof->curso;
				$alumnos = 0;
				$sql="SELECT alumtot FROM cursos WHERE ano = '$ano' and grado = '$grado' and curso = '$curso' limit 1";
				$datcurs=viewsql($sql);
				foreach($datcurs as $regcurs)
				{$alumnos = $regcurs->alumtot;}
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><a class="link_list" href="asignaturas_profe.php?opcion=editar&id_asignatura=<?php echo $id_asignatura;?>&id_profasig=<?php echo $id_profasig;?>" title="Curso"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $regprof->curso;?></a></td>
					<td align="center"><?php echo $alumnos;?></td>
					<td><a class="link_list" href="profesores_edit.php?id_asignatura=<?php echo $id_asignatura;?>&id_profesor=<?php echo $id_profesor;?>" title="Ficha Profesor..."><img src="images/profesorsm.jpg" width="16" height="16" border="0" hspace="0" align="absmiddle"></a> &nbsp;<?php echo $regprof->profesor;?></td>
				</tr>
				<?php } ?>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 4 and $ano >= $_SESSION["anoaca"]) { ?>
			<a href="asignaturas_profe.php?opcion=editar&id_asignatura=<?php echo $id_asignatura;?>"><img src="images/btn_nuevo.png" border="0" width="75" height="24" alt="Nuevo Registro" hspace="10"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
<?php } if ($opcion == "editar") {
$titulo = "Nueva Asignatura Docente";
$datprofasig = datos_profasig($ano, $id_profasig);
if ($id_profasig > 0)
	$titulo = "Editar Asignatura Docente";
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabarprofasig'>
<input type='hidden' name='ano' value='<?php echo $ano;?>'>
<input type='hidden' name='grado' value='<?php echo $grado;?>'>
<input type='hidden' name='id_asignatura' value='<?php echo $id_asignatura;?>'>
<input type='hidden' name='id_profasig' value='<?php echo $id_profasig;?>'>
<input type='hidden' name='oldcurso' value='<?php echo $datprofasig["curso"];?>'>
	<tr height="10">
		<td width="30"></td>
		<td></td>
		<td width="30"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos"><?php echo $titulo;?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td height="5"></td></tr>
				<tr height="30">
					<td>Curso</td>
					<td>
					<select name="curso" style="width: 100px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					<option value="" selected></option>
					<?php
					$sql="SELECT * FROM cursos WHERE ano='$ano' and grado='$grado' order by curso";
					$datcurs=viewsql($sql);
					foreach($datcurs as $regcurs)
					{
						if ($datprofasig["curso"] == $regcurs->curso)
							echo "<option value='$regcurs->curso' selected>$regcurs->curso</option>";
						else
							echo "<option value='$regcurs->curso'>$regcurs->curso</option>";
					}
					?>																
					</select>
					</td>
				</tr>
				<tr height="30">
					<td>Docente</td>
					<td>
					<select name="id_profesor" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					<option value="" selected></option>
					<?php
					$datprof = lista_profesores($ano,$grado);
					foreach($datprof as $regprof)
					{
						$campo = $regprof->id_profesor."-".$regprof->apellidos." ".$regprof->nombres;
						if ($datprofasig["id_profesor"] == $regprof->id_profesor)
							echo "<option value='$campo' selected>$regprof->apellidos $regprof->nombres</option>";
						else
							echo "<option value='$campo'>$regprof->apellidos $regprof->nombres</option>";
					}
					?>																
					</select>
					</td>
				</tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 4) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_profasig!="" and $ano >= $_SESSION["anoaca"] and $_SESSION["nivasi"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10"></a>
			<?php } ?>
			<a href="asignaturas_profe.php?id_asignatura=<?php echo $id_asignatura;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
