<?php
/***********************************************************************************************
* @file        : reportes.php                                                                  *
* @brief       : Opciones de impresi�n generales                                               *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 26-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 15-02-2018                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");    
include("database/datos_alumnos.php");
include("libs/fechas.php");
$opcion = isset($_POST['opcion']) ? $_POST['opcion'] : null ;
if ($opcion == "")
	$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : null ;
if ($opcion=="exportar") {
 echo"<script>window.document.location.href='exportar.php';</script>";    
}
$ano = isset($_POST['ano']) ? $_POST['ano'] : null ;
if ($ano <= 0)
	$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
if ($opcion == "")
	$opcion = "menu";
connect();
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script src="js/jquery-2.1.4.js"></script>
<script language='JavaScript'>
function validar()
{
	var opcion = document.form1.opcion.value;
	if (opcion == 'notas')
	{
		if(document.form1.periodo.value == '' && document.form1.formato[1].checked!=true && document.form1.formato[2].checked!=true && document.form1.formato[3].checked!=true && document.form1.formato[4].checked!=true && document.form1.formato[5].checked!=true)
		{alert('Debe seleccionar periodo...'); document.form1.periodo.focus(); return false;}
		if((document.form1.formato[0].checked==true || document.form1.formato[3].checked==true) && document.form1.periodo.value.substr(0,1)=='E')		
		{alert('Periodo incorrecto para el formato, no puede ser un examen...'); document.form1.periodo.focus(); return false;}
		if(document.form1.gradocurso.value == '' && document.form1.formato[3].checked!=true && document.form1.formato[4].checked!=true && document.form1.chkanterior.checked!==true)
		{alert('Debe seleccionar grado o curso...'); document.form1.gradocurso.focus(); return false;}
	}
	if (opcion == 'fallas')
	{
		if (document.form1.formato[3].checked!=true)
		{
			if(document.form1.fchini.value == '')
			{alert('Debe seleccionar fecha Inicial...'); document.form1.fchini.focus(); return false;}
			if(document.form1.fchfin.value == '')
			{alert('Debe seleccionar fecha Final...'); document.form1.fchfin.focus(); return false;}
		}
	}
	if (opcion == 'alumnos')
	{
		if(document.form1.id_alumno.value <= 0)
		{
			if(document.form1.formato[4].checked==true || document.form1.formato[5].checked==true)		
			{alert('Debe seleccionar Alumno...'); return false;}
			if(document.form1.id_plantilla.value > 0)
			{alert('Debe seleccionar Alumno...'); return false;}
		}
		if(document.form1.concepto.value == '' && (document.form1.formato[5].checked==true || document.form1.formato[6].checked==true))
		{alert('Debe especificar el concepto...'); document.form1.concepto.focus(); return false;}
	}
	if (opcion == 'profesores')
	{
		if (document.form1.formato[4].checked==true && document.form1.id_profesor.value <= 0)
		{alert('Debe seleccionar Docente...'); return false;}
		if(document.form1.id_plantilla.value > 0 && document.form1.id_profesor.value <= 0)
		{alert('Debe seleccionar Docente...'); return false;}
	}
	if (opcion == 'pagos')
	{
		if(document.form1.fchini.value == '' && document.form1.id_entrada.value <= 0)
		{alert('Debe asignar fecha de corte Inicial...'); document.form1.fchini.focus(); return false;}
		if ((document.form1.formato[0].checked==true || document.form1.formato[1].checked==true) && document.form1.id_entrada.value <= 0)
		{
			if(document.form1.fchfin.value == '')
			{alert('Debe seleccionar fecha de corte Final...'); document.form1.fchfin.focus(); return false;}
		}
	}
	return true;
}
function imprimir()
{
 
	//2017 se debe CAMBIAR POR A�O ACTUAL ���������������������������
	var opcion = document.form1.opcion.value;

	if(opcion == 'listados'){
		var radio_btn = $('input:radio[name=rad_dian]:checked').val();
		var grado_desd = $("#grado_desd").val();
		var grado_hast = $("#grado_hast").val();
		var nuev = $("#nuevo").val();
		  if (  nuev == 1 ){
		alert("con la opcion SI solo se mostraran los alumnos matriculados los ultimos 2 meses");
		}

if( radio_btn   ==   'general'){
location.href = "excel/com_excel/listado_general.php" }
if( radio_btn   ==   'padres'){
location.href = "excel/com_excel/listado_general_padres.php" }
if( radio_btn   ==   'madres'){
location.href = "excel/com_excel/listado_general_madres.php" }
if( radio_btn   ==   'acudientes'){
location.href = "excel/com_excel/listado_general_acudientes.php" }
if( radio_btn   ==   'rutas'){
location.href = "excel/com_excel/listado_ruta.php" }
if( radio_btn   ==   'matriculados'){
location.href = "excel/com_excel/matriculados.php" }
if( radio_btn   ==   'carnets'){
location.href = "excel/com_excel/Carnes.php" }
if( radio_btn   ==   'Reptientes'){
location.href = "excel/com_excel/repitentes.php" }
if( radio_btn  ==  'hospital'){		    		 			
location.href = "excel/com_excel/hospital_sur.php" }
if( radio_btn  ==  'areas'){		    		 			
location.href = "excel/com_excel/areas_pendientes.php" }
if( radio_btn  ==  'editoriales'){		    		 														 		
location.href = "excel/com_excel/editoriales.php"
}
		}
	if(opcion == 'dian'){
		var radio_btn = $('input:radio[name=rad_dian]:checked').val();
		var anio_lect = $("#anio_lect").val();
		var mes_lect = $("#mes_lect").val();
var fchini = $("#fchini").val();
var fchfin = $("#fchfin").val();
		if(radio_btn == "" || anio_lect == ""){
			alert("seleccione los parametros");
		}else {	

if ( radio_btn == 'nuevos_2'){  
				
location.href = "excel/com_excel/alumnos_nuevos_dian.php?anio="+anio_lect;
		    			
}else { 
           
if (anio_lect == "2019") {
alert("seleccione a�o correcto");
 }

else  {             


                                         
if(radio_btn == 'Consolidado'){
location.href = "excel/com_excel/pagos_dian_cons.php?anio="+anio_lect+"&fchini="+fchini+"&fchfin="+fchfin;
			                          }   		
			        
if(radio_btn == 'detallado'){
location.href = "excel/com_excel/pagos_dian_deta.php?anio="+anio_lect+"&fchini="+fchini+"&fchfin="+fchfin;
			                          }

if(radio_btn == '2015deuda') { 
location.href = "excel/com_excel/penstrans_deuda_dian.php?anio="+anio_lect;		
		                 }

 if(radio_btn == 'interes') {

location.href = "excel/com_excel/intereses_dian.php?anio="+anio_lect;   

}
					
if(radio_btn == 'interes_deuda') {

location.href = "excel/com_excel/interes_deuda_dian.php?anio="+anio_lect;   

}

if(radio_btn == 'nuevos_2'){
		    			
location.href = "excel/com_excel/alumnos_nuevos_dian.php?anio="+anio_lect;

}


if( radio_btn   ==   'efectivo'){
		    		
if ( mes_lect == "" ){  

alert("seleccione un mes");

}  else  {
			    								
location.href = "excel/com_excel/ingresos_efectivo_hoja.php?anio="+anio_lect+"&mess="+mes_lect;
    	     	
} 
}
} 
}
}


		return false;

	}
	var ano = document.form1.ano.value;
	var implogo = 'S';
	var	papel = 'LETTER';
	if (document.form1.papel[1].checked==true)
		papel = 'LEGAL';
	if (document.form1.chklogo.checked == false)
		implogo = '';

	
	if (opcion == 'notas')
	{
		var nivel = document.form1.nivel.value;
		var pertipo = document.form1.periodo.value.substr(0,1);
		var periodo = document.form1.periodo.value.substr(1,1);
		var notanum = document.form1.periodo.value.substr(2,1);
		var pos = document.form1.gradocurso.value.indexOf('*');
		var grado = document.form1.gradocurso.value.substr(0,pos);
		var curso = document.form1.gradocurso.value.substr(pos+1);
		var id_alumno = document.form1.id_alumno.value;
		var tipoedu = 'F';
		var impdoble = 'S';
		if (document.form1.chkdoble.checked == false)
			impdoble = '';
		var theURL= 'reports/rep_boletin.php?id_alumno='+id_alumno+'&periodo='+periodo+'&impdoble='+impdoble+'&';
		if (periodo == 5)
			theURL= 'reports/rep_boletincon.php?id_alumno='+id_alumno+'&notanum='+notanum+'&formato=FINAL&';
		if (document.form1.formato[1].checked==true)
			theURL= 'reports/rep_boletincon.php?id_alumno='+id_alumno+'&notanum='+notanum+'&';
		if (document.form1.formato[2].checked==true)
			theURL= 'reports/rep_libro.php?id_alumno='+id_alumno+'&notanum='+notanum+'&';
		if (document.form1.formato[3].checked==true)
			theURL= 'reports/rep_cuadrohonor.php?nivel='+nivel+'&periodo='+periodo+'&';
		if (document.form1.formato[4].checked==true)
			theURL= 'reports/rep_perdida.php?notanum='+notanum+'&';    
		if (document.form1.formato[5].checked==true)
                {
                    if (document.form1.chkanterior.checked == false)
                        theURL= 'reports/rep_perdidaare.php?notanum='+notanum+'&';
                    else
                        theURL= 'reports/rep_areaspend.php?';
                }
		if (document.form1.formato[6].checked==true)
			theURL= 'reports/rep_perdidaasi.php?notanum='+notanum+'&';
		if (document.form1.formato[7].checked==true)
			theURL= 'reports/rep_logros.php?notanum='+notanum+'&';
		if (document.form1.formato[8].checked==true)
			theURL= 'reports/rep_logroscon.php?implogros=S'+'&pertipo='+pertipo+'&periodo='+periodo+'&';
		if (document.form1.formato[9].checked==true)
			theURL= 'reports/rep_logrospen.php?implogros=S'+'&pertipo='+pertipo+'&periodo='+periodo+'&';
		theURL = theURL+'ano='+ano+'&grado='+grado+'&curso='+curso+'&tipoedu='+tipoedu+'&papel='+papel+'&implogo='+implogo;
	}
	if (opcion == 'fallas')
	{
		var pos = document.form1.gradocurso.value.indexOf('*');
		var grado = document.form1.gradocurso.value.substr(0,pos);
		var curso = document.form1.gradocurso.value.substr(pos+1);
		var fchini = document.form1.fchini.value;
		var fchfin = document.form1.fchfin.value;
		var tipo = document.form1.tipo.value;
		var id_alumno = document.form1.id_alumno.value;
		var id_categoria = document.form1.id_categoria.value;
                var sinjust = 0;
                if (document.form1.chkjust.checked == true)
                    sinjust = 1;
		var theURL= 'reports/rep_fallas.php';
		if (document.form1.formato[1].checked==true)
			theURL= 'reports/rep_fallasnov.php';                                                         
		if (document.form1.formato[2].checked==true)
			theURL= 'reports/rep_fallasfec.php';                                                         
		if (document.form1.formato[3].checked==true)
			theURL= 'reports/rep_fallasalu.php';                                                         
		if (document.form1.formato[4].checked==true)
			theURL= 'reports/rep_fallascon.php';                  
		if (document.form1.formato[5].checked==true)     
			theURL= 'reports/rep_fallasper.php';
		theURL = theURL+'?ano='+ano+'&grado='+grado+'&curso='+curso+'&tipo='+tipo+'&id_categoria='+id_categoria+'&id_alumno='+id_alumno+'&fchini='+fchini+'&fchfin='+fchfin+'&sinjust='+sinjust+'&papel='+papel+'&implogo='+implogo;
	}
	if (opcion == 'asignaturas') 
	{
		var grado = document.form1.grado.value;
		var periodo = document.form1.periodo.value;
		var id_categoria = document.form1.id_categoria.value;
		var implogros = 'N';
		var impactiv = 'N';
		var impmetod = 'N';
		if (document.form1.formato[1].checked==true)
			implogros = 'S';
		if (document.form1.formato[2].checked==true)
		{
			implogros = 'S';
			impactiv = 'S';
		}
		if (document.form1.formato[3].checked==true)
		{
			implogros = 'S';
			impmetod = 'S';
		}
		var theURL= 'reports/rep_asignaturas.php?ano='+ano+'&periodo='+periodo+'&grado='+grado+'&implogros='+implogros+'&impactiv='+impactiv+'&impmetod='+impmetod+'&implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[4].checked==true)
			theURL= 'reports/rep_areas.php?ano='+ano+'&grado='+grado+'&implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[5].checked==true)
			theURL= 'reports/rep_observaciones.php?id_categoria='+id_categoria+'&implogo='+implogo+'&papel='+papel;
	}
	if (opcion == 'alumnos')
	{
		var ano1 = ano+1;
		var pos = document.form1.gradocurso.value.indexOf('*');
		var grado = document.form1.gradocurso.value.substr(0,pos);
		var curso = document.form1.gradocurso.value.substr(pos+1);
		var matricula = document.form1.matricula.value;
		var estado = document.form1.estado.value;
		var genero = document.form1.genero.value;
		var tipoedu = document.form1.tipoedu.value;
		var id_alumno = document.form1.id_alumno.value;
		var id_plantilla = document.form1.id_plantilla.value;
		var consecutivo = document.form1.consecutivo.value;
		var concepto = document.form1.concepto.value;
		var swnuevos = 0;
		var swsalto = 1;
		if (document.form1.chknuevos.checked == true)
			swnuevos = 1;
		if (document.form1.chksalto.checked == false)
			swsalto = 0;
		var theURL= 'reports/rep_alumnosdir.php';
		theURL = theURL+'?ano='+ano+'&grado='+grado+'&curso='+curso+'&estado='+estado+'&matricula='+matricula+'&genero='+genero+'&tipoedu='+tipoedu+'&swsalto='+swsalto+'&swnuevos='+swnuevos+'&papel='+papel+'&implogo='+implogo;
		if (document.form1.formato[1].checked==true)
			theURL= 'reports/rep_alumnos.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&estado='+estado+'&matricula='+matricula+'&genero='+genero+'&tipoedu='+tipoedu+'&swsalto='+swsalto+'&swnuevos='+swnuevos+'&papel='+papel+'&implogo='+implogo;
		if (document.form1.formato[2].checked==true)
			theURL= 'reports/rep_matriculas.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&estado='+estado+'&matricula='+matricula+'&genero='+genero+'&tipoedu='+tipoedu+'&swsalto='+swsalto+'&swnuevos='+swnuevos+'&papel='+papel+'&implogo='+implogo;
		if (document.form1.formato[3].checked==true)
			theURL= 'reports/rep_alumnoslib.php?id_alumno='+id_alumno+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&swsalto='+swsalto+'&papel='+papel+'&implogo='+implogo;
		if (document.form1.formato[4].checked==true)
			theURL= 'reports/certificados_notas.php?id_alumno='+id_alumno+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&consecutivo='+consecutivo+'&papel='+papel+'&implogo='+implogo;
		if (document.form1.formato[5].checked==true)
			theURL= 'reports/diploma.php?id_alumno='+id_alumno+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&concepto='+concepto+'&papel='+papel;
		if (document.form1.formato[6].checked==true)
			theURL= 'reports/diploma_men.php?id_alumno='+id_alumno+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&concepto='+concepto+'&papel='+papel;
		if (document.form1.formato[7].checked==true)
			theURL= 'reports/carnet.php?id_alumno='+id_alumno+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&papel='+papel;
		if (document.form1.formato[8].checked==true)
			theURL= 'reports/Hojas_actua.php?id_alumno='+id_alumno+'&ano1='+ano+'&grado='+grado+'&curso='+curso+'&papel='+papel;
		if (document.form1.formato[9].checked==true)
			theURL= 'reports/matricula.php?id_alumno='+id_alumno+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&swsalto='+swsalto+'&papel='+papel;
		if (document.form1.formato[10].checked==true)
			theURL= 'reports/encargadospagos_gen.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&estado='+estado+'&matricula='+matricula+'&genero='+genero+'&tipoedu='+tipoedu+'&swsalto='+swsalto+'&swnuevos='+swnuevos+'&papel='+papel+'&implogo='+implogo;
		if (document.form1.formato[11].checked==true)
			theURL= 'reports/encargadospagos.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&estado='+estado+'&matricula='+matricula+'&genero='+genero+'&tipoedu='+tipoedu+'&swsalto='+swsalto+'&swnuevos='+swnuevos+'&papel='+papel+'&implogo='+implogo;
		if (document.form1.formato[12].checked==true)
			theURL= 'reports/info_alumno.php?id_alumno='+id_alumno+'&ano='+ano+'&estado='+estado+'&matricula='+matricula+'&grado='+grado+'&curso='+curso+'&swsalto='+swsalto+'&papel='+papel;
		if (document.form1.formato[13].checked==true)
			theURL= 'reports/rep_pagospapref.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&id_alumno='+id_alumno;
		if (id_plantilla > 0)
			theURL= 'reports/certificados_alum.php?id_plantilla='+id_plantilla+'&id_alumno='+id_alumno+'&ano='+ano+'&consecutivo='+consecutivo+'&papel='+papel+'&implogo='+implogo;
	}
	if (opcion == 'profesores') 
	{
		var grado = document.form1.grado.value;  
		var id_profesor = document.form1.id_profesor.value;
		var id_plantilla = document.form1.id_plantilla.value;
		var consecutivo = document.form1.consecutivo.value;
                var exportar = '';
		if (document.form1.chkexp.checked == true)
			exportar = 'XLS';
		var theURL= 'reports/rep_profesorescar.php?ano='+ano+'&grado='+grado+'&implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[1].checked==true)
			theURL= 'reports/rep_profesorescon.php?ano='+ano+'&grado='+grado+'&implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[2].checked==true)
			theURL= 'reports/rep_profesores.php?implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[3].checked==true)
			theURL= 'reports/rep_profesoresanu.php?ano='+ano+'&implogo='+implogo+'&papel='+papel+'&exportar='+exportar;
			//location.href = "excel/com_excel/profesores_rp.php?ano="+ano+"&implogo="+implogo+"&papel="+papel;
		if (document.form1.formato[4].checked==true)
			theURL= 'reports/certificados_histlabo.php?ano='+ano+'&grado='+grado+'&id_profesor='+id_profesor+'&consecutivo='+consecutivo+'&implogo='+implogo+'&papel='+papel;
		if (id_plantilla > 0)
			theURL= 'reports/certificados_prof.php?id_plantilla='+id_plantilla+'&id_profesor='+id_profesor+'&ano='+ano+'&consecutivo='+consecutivo+'&implogo='+implogo;
	}
	if (opcion == 'pagos')
	{
		var pos = document.form1.gradocurso.value.indexOf('*');
		var grado = document.form1.gradocurso.value.substr(0,pos);
		var curso = document.form1.gradocurso.value.substr(pos+1);
		var fchtip = document.form1.fchtip.value;
		var fchini = document.form1.fchini.value;
		var fchfin = document.form1.fchfin.value;
		var fchlim = document.form1.fchlim.value;
		var id_ruta = document.form1.id_ruta.value;
		var id_alumno = document.form1.id_alumno.value;
		var id_entrada = document.form1.id_entrada.value;
		var tipo = document.form1.tipo.value;
		var swcampo = document.form1.swcampo.value;
		var swretirados = 0;
                var swcobro = 0;
		if (document.form1.chkretirados.checked == true)
			swretirados = 1;
		if (document.form1.chkcobro.checked == true)
			swcobro = 1;
		var swsalto = 1;
		if (document.form1.chksalto.checked == false)
			swsalto = 0;
		var swdeben = 1;
		if (document.form1.chkdeben.checked == false)
			swdeben = 0;
		var swinteres = 0;
		if (document.form1.chkinteres.checked == true)
			swinteres = 1;
		var swvalores = 1;
		if (document.form1.chkvalores.checked == false)
			swvalores = 0;
		var formato = '';
		var theURL= 'reports/rep_pagos.php'+'?fchtip='+fchtip+'&fchini='+fchini+'&fchfin='+fchfin+'&id_entrada='+id_entrada;
		if (document.form1.formato[1].checked==true)
			theURL= 'reports/rep_pagosdet.php'+'?fchtip='+fchtip+'&fchini='+fchini+'&fchfin='+fchfin+'&id_entrada='+id_entrada;
		if (document.form1.formato[2].checked==true)
			theURL= 'reports/rep_pagosdet_ordID.php'+'?fchtip='+fchtip+'&fchini='+fchini+'&fchfin='+fchfin+'&id_entrada='+id_entrada;
		if (document.form1.formato[3].checked==true)
		{
                    theURL= 'reports/rep_pagossal.php'+'?fchini='+fchini+'&swcampo='+swcampo+'&swsalto='+swsalto+'&swdeben='+swdeben+'&swcobro='+swcobro+'&swinteres='+swinteres+'&swretirados='+swretirados;                    
                    if (tipo=='M')
			theURL= 'reports/rep_pagosmat.php'+'?fchini='+fchini+'&swcampo='+swcampo+'&swsalto='+swsalto+'&swdeben='+swdeben+'&swinteres='+swinteres+'&swretirados='+swretirados;
                    if (tipo=='T')
			theURL= 'reports/rep_pagossaltra.php'+'?fchini='+fchini+'&swcampo='+swcampo+'&swsalto='+swsalto+'&swdeben='+swdeben+'&swcobro='+swcobro+'&id_ruta='+id_ruta+'&swretirados='+swretirados;
		}
		if (document.form1.formato[4].checked==true)
			theURL= 'reports/rep_pagoscon.php'+'?fchini='+fchini+'&swsalto='+swsalto+'&swdeben='+swdeben+'&swinteres='+swinteres+'&formato='+formato;
		if (document.form1.formato[5].checked==true)
		{
			formato = 'DET';
			theURL= 'reports/rep_pagoscon.php'+'?fchini='+fchini+'&swsalto='+swsalto+'&swdeben='+swdeben+'&swinteres='+swinteres+'&formato='+formato;
		}
		if (document.form1.formato[6].checked==true)
			theURL= 'reports/rep_pagosacu.php'+'?fchini='+fchini+'&swinteres='+swinteres;
		if (document.form1.formato[7].checked==true)
                {
			theURL= 'reports/rep_pagospap.php'+'?swvalores='+swvalores+'&fchini='+fchini+'&fchlim='+fchlim+'&id_alumno='+id_alumno;
                        if (document.form1.chkverif.checked == true)
        			theURL= 'reports/rep_pagospapver.php'+'?swvalores='+swvalores+'&fchini='+fchini+'&fchlim='+fchlim+'&id_alumno='+id_alumno;
                 }
		if (document.form1.formato[8].checked==true)
			theURL= 'reports/rep_pagossalant.php'+'?swsalto='+swsalto;
		if (document.form1.formato[9].checked==true)
			theURL= 'reports/rep_pagosing.php'+'?fchini='+fchini+'&fchfin='+fchfin;
		theURL = theURL+'&ano='+ano+'&grado='+grado+'&curso='+curso+'&tipo='+tipo+'&formato='+formato+'&papel='+papel+'&implogo='+implogo;
	}
	if (opcion == 'transporte')
	{
		var pos = document.form1.gradocurso.value.indexOf('*');
		var grado = document.form1.gradocurso.value.substr(0,pos);
		var curso = document.form1.gradocurso.value.substr(pos+1);
		var id_alumno = document.form1.id_alumno.value;
		var id_ruta = document.form1.id_ruta.value;
		var estado = document.form1.estado.value;
		var codigo = document.form1.codigo.value;
		var swsalto = 1;
		if (document.form1.chksalto.checked == false)
			swsalto = 0;
		var swruta = 1;
		if (document.form1.chkruta.checked == false)
			swruta = 0;
		var theURL= 'reports/rep_transp.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&id_ruta='+id_ruta+'&estado='+estado+'&swsalto='+swsalto+'&implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[1].checked==true)
			theURL= 'reports/rep_transpdeb.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&id_ruta='+id_ruta+'&estado='+estado+'&swruta='+swruta+'&implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[2].checked==true)
			theURL= 'reports/rep_transpalu.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&id_ruta='+id_ruta+'&estado='+estado+'&swruta='+swruta+'&implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[3].checked==true)
			theURL= 'reports/rep_transppap.php?ano='+ano+'&grado='+grado+'&curso='+curso+'&id_ruta='+id_ruta+'&codigo='+codigo+'&id_alumno='+id_alumno+'&implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[4].checked==true)
			theURL= 'reports/rep_transprut.php?ano='+ano+'&implogo='+implogo+'&papel='+papel;
		if (document.form1.formato[5].checked==true)
			theURL= 'reports/rep_monirut.php?ano='+ano+'&implogo='+implogo+'&papel='+papel;
	}
	if (opcion == 'estadisticas')
	{
		var nivel = document.form1.nivel.value;
		var grado = document.form1.grado.value;
		var theURL= 'reports/est_alumnos.php?';
		var exportar = '';
		if (document.form1.formato[1].checked == true)
			theURL= 'reports/est_alumnoscursos.php?';
		if (document.form1.formato[2].checked == true)
			theURL= 'reports/edades_general.php?';
		if (document.form1.formato[3].checked == true)
			theURL= 'reports/edades_rangos.php?';
		if (document.form1.formato[4].checked == true)
			theURL= 'reports/est_perdida.php?';
		if (document.form1.formato[5].checked == true)
			theURL= 'reports/est_dane.php?';
		if (document.form1.formato[6].checked == true)
			theURL= 'reports/profe_rangos.php?';
		if (document.form1.chkexp.checked == true)
			exportar = 'XLS';
		theURL = theURL+'ano='+ano+'&grado='+grado+'&nivel='+nivel+'&papel='+papel+'&implogo='+implogo+'&exportar='+exportar;
	}
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
function seleccionar()
{
	var opcion = document.form1.opcion.value;
	var ano = document.form1.ano.value;
	var theURL = 'reportes.php?ano='+ano+'&opcion='+opcion;
	window.location.href = theURL;
}
</script>
</head>
<body>
<?php if ($opcion == "menu") { ?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="" method="post" name="form1">
	<tr height="40"><td colspan="3" valign="middle" bgcolor="#009900" class="txt_destacados" align="center">MODULO DE REPORTES</td></tr>
	<tr height="10"><td colspan="3"></td></tr>
	<tr height="280" valign="top">
		<td width="50"></td>
		<td align="center">
			<fieldset>
			<table border="0" cellpadding="2" cellspacing="2">
				<tr height="5"><td></td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="notas" checked> <img src="images/notas.png" border="0" width="30" height="30" align="absmiddle"> INFORMES ACADEMICOS Y NOTAS</td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="fallas"> <img src="images/fallas.png" border="0" width="30" height="30" align="absmiddle"> INASISTENCIAS, RETARDOS, PERMISOS Y CITACIONES</td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="alumnos"> <img src="images/alumno.png" border="0" width="30" height="30" align="absmiddle"> ALUMNOS Y MATRICULAS</td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="profesores"> <img src="images/profesor.png" border="0" width="30" height="30" align="absmiddle"> DOCENTES</td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="asignaturas"> <img src="images/asignaturas.png" border="0" width="30" height="30" align="absmiddle"> ASIGNATURAS Y LOGROS</td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="pagos"> <img src="images/pagos.png" border="0" width="30" height="30" align="absmiddle"> PAGOS PENSIONES Y TRANSPORTE&nbsp;&nbsp;</td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="transporte"> <img src="images/rutas.png" border="0" width="30" height="30" align="absmiddle"> RUTAS, CONDUCTORES Y TRANSPORTE&nbsp;&nbsp;</td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="estadisticas"> <img src="images/chart.png" border="0" width="30" height="30" align="absmiddle"> ESTADISTICAS</td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="dian"> <img src="images/stat.png" border="0" width="30" height="30" align="absmiddle"> REPORTES DIAN</td></tr>
				<tr><td height="35">&nbsp;&nbsp;<input type="radio" name="opcion" value="exportar"> <img src="images/excel.png" border="0" width="30" height="30" align="absmiddle"> LISTADOS EXCEL</td></tr>
				<tr height="5"><td></td></tr>
			</table>
			</fieldset>
		</td>
		<td width="50"></td>
	</tr>
	<tr height="10"><td colspan="3"></td></tr>
	<tr height="35">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<input type="image" class="botones_img" src="images/btn_continuar.png" border="0" name="continuar"  width="70" height="24" hspace="10" alt="Imprimir">
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<?php } else { ?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="javascript:imprimir();" method="post" name="form1" onSubmit="return validar();">
<input type="hidden" name="opcion" value="<?php echo $opcion;?>">
	<tr height="40">
		<td colspan="3" valign="middle" bgcolor="#009900" class="txt_destacados" align="center">
		<?php if ($opcion == "notas") { ?>
		<img src="images/notas.png" border="0" width="40" height="40" align="absmiddle"> REPORTES DE NOTAS E INFORMES ACADEMICOS
		<?php } if ($opcion == "fallas") { ?>
		<img src="images/fallas.png" border="0" width="40" height="40" align="absmiddle"> REPORTES DE INASISTENCIAS, RETARDOS, PERMISOS Y CITACIONES
		<?php } if ($opcion == "asignaturas") { ?>
		<img src="images/asignaturas.png" border="0" width="40" height="40" align="absmiddle"> REPORTES DE ASIGNATURAS Y LOGROS
		<?php } if ($opcion == "alumnos") { ?>
		<img src="images/alumno.png" border="0" width="40" height="40" align="absmiddle"> REPORTES DE ALUMNOS Y MATRICULAS
		<?php } if ($opcion == "profesores") { ?>
		<img src="images/profesor.png" border="0" width="40" height="40" align="absmiddle"> REPORTES DE DOCENTES
		<?php } if ($opcion == "pagos") { ?>
		<img src="images/pagos.png" border="0" width="40" height="40" align="absmiddle"> REPORTES PAGOS PENSIONES Y TRANSPORTE
		<?php } if ($opcion == "transporte") { ?>
		<img src="images/rutas.png" border="0" width="40" height="40" align="absmiddle"> REPORTES RUTAS, CONDUCTORES Y TRANSPORTE
		<?php } if ($opcion == "estadisticas") { ?>
		<img src="images/chart.png" border="0" width="40" height="40" align="absmiddle"> ESTADISTICAS
		<?php } if ($opcion == "dian") {?>
		<img src="images/stat.png" border="0" width="40" height="40" align="absmiddle"> DIAN
                <?php } if ($opcion == "listados") {?>
		<img src="images/excel.png" border="0" width="40" height="40" align="absmiddle"> LISTADOS EXCEL
		<?php } ?>
		</td>
	</tr>
	<tr><td colspan="3" height="10"></td></tr>
	<tr height="280" valign="top">
		<td width="20"></td>
		<td align="center">
			<?php if ($opcion == "notas") { ?>
			<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr valign="top">
							<td>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Informe Acad�mico (Bolet�n)</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Consolidado de Notas</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Libro de Calificaciones</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Informe Cuadro de Honor</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Perdida de A�o</td></tr>
								</table>
							</td>
							<td width="20"></td>
							<td>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="5"> Perdida de Areas - A�o anterior <input type="checkbox" name="chkanterior"></td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="6"> Perdida de Asignaturas</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="7"> Logros Perdidos - Detallado</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="8"> Logros Perdidos - Consolidado</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="9"> Logros Pendientes</td></tr>
								</table>
							</td>
						</tr>
						</table>
						</fieldset>
					</td>
					<td></td>
					<td></td>
				</tr>
				<tr>
					<td colspan="3">
						<fieldset>
						<legend class="titulo_campos">Filros y Opciones</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
										<tr>
											<td width="65">A�o Lectivo</td>
											<td>
											<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<?php
											$sql="SELECT ano FROM anoslect ORDER BY ano desc";
											$datanol=viewsql($sql);
											foreach($datanol as $reganol)
											{
												$selano = "";
												if ($reganol->ano==$ano)
													$selano = "selected";
												echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
											}
											?>
											</select>
											<select name="periodo" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Periodo corte...</option>
											<?php
											$cierre = "";
											$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
											$datperi=viewsql($sql);
											foreach($datperi as $regperi)
											{
												if ($regperi->pertipo=="P" and $regperi->periodo=="4" and $regperi->estado=="CERRADO")
													$cierre="CERRADO";
												if ($regperi->estado!="")
													echo "<option value='$regperi->pertipo$regperi->periodo$regperi->notanum'>$regperi->pernomb</option>";
											}
											if ($cierre=="CERRADO")
											{
											?>
											<option value="P59">DEFINITIVAS</option>
											<?php } ?>
											</select>
											<select name="nivel" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="">Nivel...</option>
											<?php
											$sql= "select nivel, nombre from niveles order by id_nivel";
											$datnive=viewsql($sql);
											foreach($datnive as $regnive)
											{
												echo "<option value='$regnive->nivel$regnive->nombre'>$regnive->nombre</option>";
											}
											?>																
											</select>
											</td>
										</tr>
										<tr>
											<td>Grado/Curso</td>
											<td>
											<select name="gradocurso" style="width: 262px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar...</option>
											<?php
											$sql = "SELECT grado, gradonom FROM grados order by id_grado";
											$datgrad = viewsql($sql);
											foreach($datgrad as $reggrad)
											{
												$lgrado = $reggrad->grado;
												$lcurso = "";
												if ($lgrado==$grado and $lcurso==$curso)
													echo "<option value='$lgrado*$lcurso' selected>GRADO $reggrad->gradonom</option>";
												else
													echo "<option value='$lgrado*$lcurso'>GRADO $reggrad->gradonom</option>";
												$sql = "SELECT grado, curso FROM cursos WHERE ano = '$ano' AND grado = '$lgrado' order by curso";
												$datcurs = viewsql($sql);
												foreach($datcurs as $regcurs)
												{
													if ($regcurs->grado==$grado and $regcurs->curso==$curso)
													echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
													else
													echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
												}
											}
											?>																
											</select>
											</td>
										</tr>
										<tr>
											<td>Alumno</td>
											<td>
												<input type="hidden" name="id_alumno">
												<input type="text" name="alumno" value="Seleccionar..." style="width: 237px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<a href="alumnos_select.php" onclick="newWindow(this.href, '', 600, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_lupa.png' border="0" width="24" height="24" alt="Buscar" align="absmiddle" title="Seleccionar Alumno..." hspace="0"></a>
											</td>
										</tr>
									</table>
								</td>
								<td width="5"></td>
								<td>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="25">
											<td>
											<input type="radio" name="papel" value="LETTER" checked> Papel Carta &nbsp;&nbsp;
											<input type="radio" name="papel" value="LEGAL"> Oficio &nbsp;&nbsp;
											</td>
										</tr>
										<tr height="25"><td><input type="checkbox" name="chkdoble" checked> Imprimir Doble Costado (Boletines)</td></tr>
										<tr height="25"><td><input type="checkbox" name="chklogo"> Imprimir Logo IDIC</td></tr>
									</table>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
			</table>
			<?php } if ($opcion == "fallas") { ?>
			<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Novedades por Fecha</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Novedades por Alumno</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Inasistencias en Asignaturas por Fecha y Alumnos</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Inasistencias en Asignaturas por Alumno y Fechas</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Inasistencias en Asignaturas Consolidado por Alumno </td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="5"> Asignaturas Perdidas por Fallas</td></tr>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
						<legend class="titulo_campos">Filtros y Opciones</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
										<tr>
											<td width="60">A�o Lectivo</td>
											<td>
											<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
											<?php
											$sql="SELECT ano FROM anoslect ORDER BY ano desc";
											$datanol=viewsql($sql);
											foreach($datanol as $reganol)
											{
												$selano = "";
												if ($reganol->ano==$ano)
													$selano = "selected";
												echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
											}
											?>
											</select>
											Fechas
											<input type="text" name="fchini" id="fchini" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchini, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											A
											<input type="text" name="fchfin" id="fchfin" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchfin, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											</td>
										</tr>
										<tr>
											<td>Grado/Curso</td>
											<td>
											<select name="gradocurso" style="width: 240px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar...</option>
											<?php
											$sql = "SELECT grado, gradonom FROM grados order by id_grado";
											$datgrad = viewsql($sql);
											foreach($datgrad as $reggrad)
											{
												$lgrado = $reggrad->grado;
												$lcurso = "";
												if ($lgrado==$grado and $lcurso==$curso)
													echo "<option value='$lgrado*$lcurso' selected>GRADO $reggrad->gradonom</option>";
												else
													echo "<option value='$lgrado*$lcurso'>GRADO $reggrad->gradonom</option>";
												$sql = "SELECT grado, curso FROM cursos WHERE ano = '$ano' AND grado = '$lgrado' order by curso";
												$datcurs = viewsql($sql);
												foreach($datcurs as $regcurs)
												{
													if ($regcurs->grado==$grado and $regcurs->curso==$curso)
													echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
													else
													echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
												}
											}
											?>																
											</select>
											</td>
										</tr>
										<tr>
											<td>Novedad</td>
											<td>
												<select name="tipo" style="width: 240px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                                                                                <option value='TODAS LAS FALLAS' selected>TODAS LAS FALLAS</option>
                                                                                                <option value='INASISTENCIAS'>INASISTENCIAS</option>
                                                                                                <option value='RETARDOS'>RETARDOS</option>
                                                                                                <option value='PERMISOS'>PERMISOS</option>
                                                                                                <option value='AUTORIZACIONES'>AUTORIZACIONES</option>
                                                                                                <option value='CITACIONES'>CITACIONES</option>
												</select>
											</td>
										</tr>

                                                                                <tr>
											<td>Concepto</td>
											<td>
												<select name="id_categoria" style="width: 240px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<option value="" selected>Seleccionar...</option>
												<?php
												$sql="select id_categoria, tip_categoria, categoria from categorias where tip_categoria = 'INASISTENCIAS' OR tip_categoria = 'PERMISOS' OR tip_categoria = 'CITACIONES' order by tip_categoria, categoria";
												$datcateg=viewsql($sql);
												foreach($datcateg as $regcateg)
												{
													echo "<option value='$regcateg->id_categoria'>$regcateg->tip_categoria - $regcateg->categoria</option>";
												}
												?>																
												</select>
											</td>
										</tr>
										<tr>
											<td>Alumno</td>
											<td>
												<input type="hidden" name="id_alumno">
												<input type="text" name="alumno" value="Seleccionar..." style="width: 240px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												&nbsp;
												<a href="alumnos_select.php" onclick="newWindow(this.href, '', 800, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_lupa.png' border="0" width="24" height="24" alt="Buscar" align="absmiddle" title="Seleccionar Alumno..."></a>
											</td>
										</tr>
									</table>
								</td>
								<td width="10"></td>
								<td>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="30">
											<td>
											<input type="radio" name="papel" value="LETTER" checked> Papel Carta &nbsp;&nbsp;
											<input type="radio" name="papel" value="LEGAL"> Oficio &nbsp;&nbsp;
											</td>
										</tr>
										<tr height="30">
											<td>
												<input type="checkbox" name="chklogo"> Imprimir Logo IDIC
											</td>
										</tr>
										<tr height="30">
											<td>
												<input type="checkbox" name="chkjust"> Sin Justificar
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
			</table>
			<?php } if ($opcion == "alumnos") { ?>
			<table border="0" width="100%">
				<tr valign="top"> 
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr valign="top">
							<td>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Directorio de Alumnos</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Lista de Alumnos y Observaci�n</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Informe Alumnos Matriculados por A�o</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Informe Alumnos Libro de Calificaciones</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Certificado de Notas</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="6">Menci�n de Honor</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="5">Diploma de Honor </td></tr>
								</table>
							</td>
							<td width="20"></td>
							<td>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="7">Carnets </td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="8">Hojas de Actualizaci�n de Datos </td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="9">Hojas de Matr�cula a�o Siguiente </td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="10">Encargados de Pago General </td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="11">Encargados de Pago Detallado </td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="12">Informaci�n por alumno</td></tr>
									<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="13">Papeleta Referencia Pagos</td></tr>
								</table>
							</td>
						</tr>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
						<legend class="titulo_campos">Filtros y Opciones</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
										<tr>
											<td width="60">A�o Lectivo</td>
											<td>
											<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;" >
											<?php
											$sql="SELECT ano FROM anoslect ORDER BY ano desc";
											$datanol=viewsql($sql);
											foreach($datanol as $reganol)
											{
												$selano = "";
												if ($reganol->ano==$ano)
													$selano = "selected";
												echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
											}
											?>
											</select>
											Saltar p�gina por Curso <input type="checkbox" name="chksalto" checked>
											&nbsp;
											Nuevos <input type="checkbox" name="chknuevos">
											</td>
										</tr>
										<tr>
											<td>Grado/Curso</td>
											<td>
											<select name="gradocurso" style="width: 230px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar...</option>
											<?php
											$sql = "SELECT grado, gradonom FROM grados order by id_grado";
											$datgrad = viewsql($sql);
											foreach($datgrad as $reggrad)
											{
												$lgrado = $reggrad->grado;
												$lcurso = "";
												if ($lgrado==$grado and $lcurso==$curso)
													echo "<option value='$lgrado*$lcurso' selected>GRADO $reggrad->gradonom</option>";
												else
													echo "<option value='$lgrado*$lcurso'>GRADO $reggrad->gradonom</option>";
												$sql = "SELECT grado, curso FROM cursos WHERE ano = '$ano' AND grado = '$lgrado' order by curso";
												$datcurs = viewsql($sql);
												foreach($datcurs as $regcurs)
												{
													if ($regcurs->grado==$grado and $regcurs->curso==$curso)
													echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
													else
													echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
												}
											}
											?>																
											</select>
											</td>
										</tr>
										<tr>
											<td>Alumno</td>
											<td>
												<input type="hidden" name="id_alumno">
												<input type="text" name="alumno" value="Seleccionar..." style="width: 230px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												&nbsp;
												<a href="alumnos_select.php" onclick="newWindow(this.href, '', 800, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_lupa.png' border="0" width="24" height="24" alt="Buscar" align="absmiddle" title="Seleccionar Alumno..."></a>
											</td>
										</tr>
									</table>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="20">
											<td>
											<input type="radio" name="papel" value="LETTER" checked> Papel Carta &nbsp;&nbsp;
											<input type="radio" name="papel" value="LEGAL"> Oficio &nbsp;&nbsp;
											</td>
											<td width="20"></td>
											<td>
												<input type="checkbox" name="chklogo"> Imprimir Logo IDIC
											</td>
										</tr>
									</table>
								</td>
								<td width="10"></td>
								<td width="1" bgcolor="#B7C4DB"></td>
								<td width="10"></td>
								<td align="center">
									<table border="0" cellpadding="1" cellspacing="1">
										<tr>
											<td width="95">Estado Matr�cula</td>
											<td>
											<select name="matricula" style="width: 90px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Todas...</option>
											<?php
											$sql="SELECT * FROM listas WHERE listip = 'MATRICULAS' order by id";
											$lista=viewsql($sql);
											foreach($lista as $reglis)
											{
												echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
											}
											?>																
											</select>
											</td>
										</tr>
										<tr>
											<td>Estado Alumno</td>
											<td>
												<?php if ($ano==$_SESSION["anoaca"]) { ?>
												<select name="estado" style="width: 90px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<option value="" selected>Todos...</option>
												<?php
												$sql="SELECT * FROM listas WHERE listip = 'ALUMNOS' order by id";
												$lista=viewsql($sql);
												foreach($lista as $reglis)
												{
													$lisnom = $reglis->lisnom."S";
													echo "<option value='$reglis->lisnom'>$lisnom</option>";
												}
												?>																
												</select>
												<?php } else { ?>
												A�O <?php echo $_SESSION["anoaca"];?>
												<input type="hidden" name="estado" value="">
												<?php } ?>
											</td>
										</tr>
										<tr>
											<td>G�nero M/F</td>
											<td>
												<select name="genero" style="width: 90px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<option value="" selected>Todos...</option>
												<?php
												$sql="SELECT * FROM listas WHERE listip = 'GENERO' order by id";
												$lista=viewsql($sql);
												foreach($lista as $reglis)
												{
													echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
												}
												?>																
												</select>
											</td>
										</tr>
										<tr>
											<td>Educaci�n Tipo</td>
											<td>
												<select name="tipoedu" style="width: 90px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<option value="" selected>Todas...</option>
												<?php
												$sql="SELECT * FROM listas WHERE listip = 'EDUCACION' order by id";
												$lista=viewsql($sql);
												foreach($lista as $reglis)
												{
													echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
												}
												?>																
												</select>
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</fieldset>
						<fieldset>
						<legend class="titulo_campos">Concepto Diploma / Menci�n</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>Por: <input type="text" name="concepto" style="width: 500px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
						</table>
						</fieldset>
						<fieldset>
						<legend class="titulo_campos">Constancias</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td width="25" align="center"><img src="images/certificado.png" border="0" width="19" height="28" align="absmiddle"></td>
								<td>
									<select name="id_plantilla" style="width: 350px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<option value="" selected>Seleccionar Formato...</option>
									<?php
									$sql="select id_plantilla, plantilla from plantillas where categoria = 'ALUMNOS' order by plantilla";
									$datplan=viewsql($sql);
									foreach($datplan as $regplan)
									{
										echo "<option value='$regplan->id_plantilla'>$regplan->plantilla</option>";
									}
									?>																
									</select>
									Consecutivo
									<input type="text" name="consecutivo" value="" style="width: 80px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
			</table>
			<?php } if ($opcion == "profesores") { ?>
			<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Carga Acad�mica</td></tr>
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Informaci�n Contratos Docentes</td></tr>
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Informaci�n General de Docentes</td></tr>
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Relaci�n Anual Personal Docente &nbsp;&nbsp;&nbsp;<input type="checkbox" name="chkexp">Excel <img src="images/ico_excel.gif" border="0" width="20" height="20" align="absmiddle"></td></tr>
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Certificado de Historia Laboral</td></tr>
						</table>
						</fieldset>
						<fieldset>
						<legend class="titulo_campos">Constancias</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr height="30">
								<td width="25" align="center"><img src="images/certificado.png" border="0" width="19" height="28" align="absmiddle"></td>
								<td>
									<select name="id_plantilla" style="width: 350px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
									<option value="" selected>Seleccionar Formato...</option>
									<?php
									$sql="select id_plantilla, plantilla from plantillas where categoria = 'PROFESORES' order by plantilla";
									$datplan=viewsql($sql);
									foreach($datplan as $regplan)
									{
										echo "<option value='$regplan->id_plantilla'>$regplan->plantilla</option>";
									}
									?>																
									</select>
									Consecutivo
									<input type="text" name="consecutivo" value="" style="width: 80px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
						<legend class="titulo_campos">Filros y Opciones</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
										<tr>
											<td width="75">A�o Lectivo</td>
											<td>
											<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<?php
											$sql="SELECT ano FROM anoslect ORDER BY ano desc";
											$datanol=viewsql($sql);
											foreach($datanol as $reganol)
											{
												$selano = "";
												if ($reganol->ano==$ano)
													$selano = "selected";
												echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
											}
											?>
											</select>
											</td>
										</tr>
										<tr>
											<td>Grado</td>
											<td>
											<select name="grado" style="width: 230px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar...</option>
											<?php
											$sql = "SELECT grado, gradonom FROM grados order by id_grado";
											$datgrad = viewsql($sql);
											foreach($datgrad as $reggrad)
											{
												echo "<option value='$reggrad->grado'>GRADO $reggrad->gradonom</option>";
											}
											?>																
											</select>
											</td>
										</tr>
										<tr>
											<td>Docente</td>
											<td>
												<input type="hidden" name="id_profesor">
												<input type="text" name="profesor" value="Seleccionar..." style="width: 230px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												&nbsp;
												<a href="profesores_select.php" onclick="newWindow(this.href, '', 800, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_lupa.png' border="0" width="24" height="24" alt="Buscar" align="absmiddle" title="Seleccionar Docente..."></a>
											</td>
										</tr>
									</table>
								</td>
								<td width="20"></td>
								<td>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="30">
											<td>
											<input type="radio" name="papel" value="LETTER" checked> Papel Carta &nbsp;&nbsp;
											<input type="radio" name="papel" value="LEGAL"> Oficio &nbsp;&nbsp;
											</td>
										</tr>
										<tr height="30">
											<td>
											<input type="checkbox" name="chklogo"> Imprimir Logo IDIC
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
			</table>
			<?php } if ($opcion == "asignaturas") { ?>
			<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Lista Asignaturas</td></tr>
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Lista de Asignaturas y Logros</td></tr>
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Lista de Asignaturas, Logros, Evidencias y Actividades</td></tr>
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Lista de Asignaturas, Logros y Metodologia</td></tr>
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Lista de Areas</td></tr>
							<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="5"> Lista de Observaciones para Planilla</td></tr>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
						<legend class="titulo_campos">Filros y Opciones</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
										<tr>
											<td width="75">A�o Lectivo</td>
											<td>
											<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<?php
											$sql="SELECT ano FROM anoslect ORDER BY ano desc";
											$datanol=viewsql($sql);
											foreach($datanol as $reganol)
											{
												$selano = "";
												if ($reganol->ano==$ano)
													$selano = "selected";
												echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
											}
											?>
											</select>
											<select name="periodo" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="0">PERIODO...</option>
											<?php
											$sql="SELECT * FROM periodos WHERE ano = '$ano' and pertipo='P' order by notanum";
											$datperi=viewsql($sql);
											foreach($datperi as $regperi)
											{echo "<option value='$regperi->periodo'>$regperi->pernomb</option>";}
											?>
											</select>
											</td>
										</tr>
										<tr>
											<td width="75">Grado</td>
											<td>
											<select name="grado" style="width: 250px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar...</option>
											<?php
											$sql = "SELECT grado, gradonom FROM grados order by id_grado";
											$datgrad = viewsql($sql);
											foreach($datgrad as $reggrad)
											{
												echo "<option value='$reggrad->grado'>GRADO $reggrad->gradonom</option>";
											}
											?>																
											</select>
											</td>
										</tr>
										<tr>
											<td width="75">Observaciones</td>
											<td>
											<select name="id_categoria" style="width: 250px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar Categor�a...</option>
											<?php
											$sql="select id_categoria, categoria from categorias where tip_categoria = 'OBSERVACIONES' order by categoria";
											$datcateg=viewsql($sql);
											foreach($datcateg as $regcateg)
											{
												echo "<option value='$regcateg->id_categoria'>$regcateg->categoria</option>";
											}
											?>																
											</select>
											</td>
										</tr>
									</table>
								</td>
								<td width="20"></td>
								<td>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="30">
											<td>
											<input type="radio" name="papel" value="LETTER" checked> Papel Carta &nbsp;&nbsp;
											<input type="radio" name="papel" value="LEGAL"> Oficio &nbsp;&nbsp;
											</td>
										</tr>
										<tr height="30">
											<td>
											<input type="checkbox" name="chklogo"> Imprimir Logo IDIC
											</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
			</table>
			<?php } if ($opcion == "pagos") { ?>
			<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr height="20"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Relaci�n Pagos Totales por Periodo</td></tr>
							<tr><td height="1" bgcolor="#CCCCCC"></td></tr>
							<tr height="20"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Relaci�n Pagos Causaci�n Mensual Ordenado Alfab�ticamente</td></tr>
							<tr><td height="1" bgcolor="#CCCCCC"></td></tr>
							<tr height="20"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Relaci�n Pagos Causaci�n Mensual Ordenado Por ID</td></tr>
							<tr><td height="1" bgcolor="#CCCCCC"></td></tr>
							<tr height="20"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Meses con Deuda, Valores<input type="radio" name="swcampo" value="0" checked> &nbsp;IDPago<input type="radio" name="swcampo" value="1"> &nbsp;IDEntrada<input type="radio" name="swcampo" value="2"> &nbsp;Alumnos Retirados <input type="checkbox" name="chkretirados"> &nbsp;Datos Cobro <input type="checkbox" name="chkcobro"></td></tr>
							<tr><td height="1" bgcolor="#CCCCCC"></td></tr>
							<tr height="20"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Consolidado Pagos Totales por Alumno y Saldo Final</td></tr>
							<tr><td height="1" bgcolor="#CCCCCC"></td></tr>
							<tr height="20"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="5"> Consolidado Pagos Detallados por Alumno y Saldo Final</td></tr>
							<tr><td height="1" bgcolor="#CCCCCC"></td></tr>
							<tr height="20"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="6"> Acumulados Pagos Mensuales y Saldos Finales</td></tr>
							<tr><td height="1" bgcolor="#CCCCCC"></td></tr>
							<tr height="20">
                                                            <td>&nbsp;&nbsp;<input type="radio" name="formato" value="7"> Papeletas Paz y Salvo, 
                                                             Verificaci�n
                                                             <input type="checkbox" name="chkverif">
                                                             Incluir Valores 
                                                             <input type="checkbox" name="chkvalores" checked>
                                                             Fecha Limite
                                                             <input type="text" name="fchlim" id="fchlim" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchlim, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                                                            </td>
                                                        </tr>
							<tr><td height="1" bgcolor="#CCCCCC"></td></tr>
							<tr height="20"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="8"> Pagos Pendientes A�o Anterior</td></tr>
							<tr><td height="1" bgcolor="#CCCCCC"></td></tr>
							<tr height="20"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="9"> Ingresos Totalizados por Entrada</td></tr>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<fieldset>
									<legend class="titulo_campos">Filtros</legend>
									<table border="0" cellpadding="1" cellspacing="1">
										<tr height="22">
											<td width="60">A�o Lectivo</td>
											<td>
											<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<?php
											$sql="SELECT ano FROM anoslect ORDER BY ano desc";
											$datanol=viewsql($sql);
											foreach($datanol as $reganol)
											{
												$selano = "";
												if ($reganol->ano==$ano)
													$selano = "selected";
												echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
											}
											?>
											</select>
											&nbsp;Tipo
											<select name="tipo" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<option value="P" selected>PENSIONES</option>
												<option value="T">TRANSPORTE</option>
												<option value="M">MATRICULAS</option>
											</select>
											&nbsp;Entrada N�
											<input type="text" name="id_entrada" style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											</td>
										</tr>
										<tr height="22">
											<td width="60">Fecha Tipo</td>
											<td>
											<select name="fchtip" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<option value='ENTRADAS' selected>Hoja Entrada</option>
												<option value='CONSIGNACIONES'>Consignaci�n</option>
											</select>
											Corte
											<input type="text" name="fchini" id="fchini" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchini, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											A
											<input type="text" name="fchfin" id="fchfin" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchfin, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											</td>
										</tr>
										<tr height="22">
											<td>Grado/Curso</td>
											<td>
											<select name="gradocurso" style="width: 240px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar...</option>
											<?php
											$sql = "SELECT grado, gradonom FROM grados order by id_grado";
											$datgrad = viewsql($sql);
											foreach($datgrad as $reggrad)
											{
												$lgrado = $reggrad->grado;
												$lcurso = "";
												if ($lgrado==$grado and $lcurso==$curso)
													echo "<option value='$lgrado*$lcurso' selected>GRADO $reggrad->gradonom</option>";
												else
													echo "<option value='$lgrado*$lcurso'>GRADO $reggrad->gradonom</option>";
												$sql = "SELECT grado, curso FROM cursos WHERE ano = '$ano' AND grado = '$lgrado' order by curso";
												$datcurs = viewsql($sql);
												foreach($datcurs as $regcurs)
												{
													if ($regcurs->grado==$grado and $regcurs->curso==$curso)
													echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
													else
													echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
												}
											}
											?>																
											</select>
											</td>
										</tr>
										<tr height="22">
											<td>Ruta</td>
											<td>
												<select name="id_ruta" style="width: 240px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<option value="" selected>Seleccionar...</option>
												<?php
												$sql="SELECT id_ruta, ruta, recorrido FROM rutas WHERE ano = '$ano' ORDER BY ruta";
												$datrut=viewsql($sql);
												foreach($datrut as $regrut)
												{
													echo "<option value='$regrut->id_ruta'>$regrut->ruta - $regrut->recorrido</option>";
												}
												?>																
												</select>
											</td>
										</tr>
										<tr height="22">
											<td>Alumno</td>
											<td>
												<input type="hidden" name="id_alumno">
												<input type="text" name="alumno" value="Seleccionar..." style="width: 240px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												&nbsp;
												<a href="alumnos_select.php" onclick="newWindow(this.href, '', 800, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_lupa.png' border="0" width="24" height="24" alt="Buscar" align="absmiddle" title="Seleccionar Alumno..."></a>
											</td>
										</tr>
									</table>
									</fieldset>
								</td>
								<td width="10"></td>
								<td>
									<fieldset>
									<legend class="titulo_campos">Opciones</legend>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="20">
											<td><input type="checkbox" name="chksalto" checked> Saltar p�gina por Curso</td>
										</tr>
										<tr height="20">
											<td><input type="checkbox" name="chkdeben" checked> Solo Alumnos que Deben</td>
										</tr>
										<tr height="20">
											<td><input type="checkbox" name="chkinteres"> Saldos incluir Inter�s Mora</td>
										</tr>
										<tr height="20">
											<td><input type="checkbox" name="chklogo"> Imprimir Logo IDIC</td>
										</tr>
										<tr height="20">
											<td>
											<input type="radio" name="papel" value="LETTER" checked> Papel Carta &nbsp;&nbsp;
											<input type="radio" name="papel" value="LEGAL"> Oficio &nbsp;&nbsp;
											</td>
										</tr>
									</table>
									</fieldset>
								</td>
							</tr>
						</table>
					</td>
				</tr>
			</table>
			<?php } if ($opcion == "transporte") { ?>
			<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Alumnos con Meses Pendientes por Cursos <input type="checkbox" name="chksalto" checked> Saltar p�gina por Curso</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Alumnos con Meses Pendientes por Rutas </td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Alumnos por Rutas con Direcciones <input type="checkbox" name="chkruta" checked> Saltar p�gina por Ruta</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Alumnos Papeletas para Conductores</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Rutas y Conductores</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="5"> Rutas y Monitores</td></tr>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
						<legend class="titulo_campos">Filtros y Opciones</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
										<tr height="20">
											<td width="60">A�o Lectivo</td>
											<td>
											<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<?php
											$sql="SELECT ano FROM anoslect ORDER BY ano desc";
											$datanol=viewsql($sql);
											foreach($datanol as $reganol)
											{
												$selano = "";
												if ($reganol->ano==$ano)
													$selano = "selected";
												echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
											}
											?>
											</select>
											</td>
										</tr>
										<tr height="20">
											<td>Grado o Curso</td>
											<td>
											<select name="gradocurso" style="width: 250px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar...</option>
											<?php
											$sql = "SELECT grado, gradonom FROM grados order by id_grado";
											$datgrad = viewsql($sql);
											foreach($datgrad as $reggrad)
											{
												$lgrado = $reggrad->grado;
												$lcurso = "";
												if ($lgrado==$grado and $lcurso==$curso)
													echo "<option value='$lgrado*$lcurso' selected>GRADO $reggrad->gradonom</option>";
												else
													echo "<option value='$lgrado*$lcurso'>GRADO $reggrad->gradonom</option>";
												$sql = "SELECT grado, curso FROM cursos WHERE ano = '$ano' AND grado = '$lgrado' order by curso";
												$datcurs = viewsql($sql);
												foreach($datcurs as $regcurs)
												{
													if ($regcurs->grado==$grado and $regcurs->curso==$curso)
													echo "<option value='$regcurs->grado*$regcurs->curso' selected>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
													else
													echo "<option value='$regcurs->grado*$regcurs->curso'>GRADO $reggrad->gradonom - CURSO $regcurs->curso</option>";
												}
											}
											?>																
											</select>
											</td>
										</tr>
										<tr height="20">
											<td>Seleccionar Ruta</td>
											<td>
												<select name="id_ruta" style="width: 250px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<option value="" selected></option>
												<?php
												$sql="SELECT id_ruta, ruta, recorrido FROM rutas WHERE ano = '$ano' ORDER BY ruta";
												$datrut=viewsql($sql);
												foreach($datrut as $regrut)
												{
													echo "<option value='$regrut->id_ruta'>$regrut->ruta - $regrut->recorrido</option>";
												}
												?>																
												</select>
											</td>
										</tr>
										<tr>
											<td>Papeleta Alumno</td>
											<td>
												<input type="hidden" name="id_alumno">
												<input type="text" name="alumno" value="Seleccionar..." style="width: 237px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<a href="alumnos_select.php" onclick="newWindow(this.href, '', 600, 600, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_lupa.png' border="0" width="24" height="24" alt="Buscar" align="absmiddle" title="Seleccionar Alumno..." hspace="0"></a>
											</td>
										</tr>
										<tr>
											<td width="95">Transporte Estado</td>
											<td>
												<select name="estado" style="width: 90px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
												<?php
												$sql="SELECT * FROM listas WHERE listip = 'PROFESORES' order by id";
												$lista=viewsql($sql);
												foreach($lista as $reglis)
												{
													$estado = $reglis->lisnom."S";
													echo "<option value='$reglis->lisnom'>$estado</option>";
												}
												?>
												<option value=''>TODOS</option>																
												</select>
												C�digo de Transporte
												<input type="text" name="codigo" style="width: 25px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">												
											</td>
										</tr>
									</table>
								</td>
								<td width="10"></td>
								<td>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="30">
											<td>
											<input type="radio" name="papel" value="LETTER" checked> Papel Carta &nbsp;&nbsp;
											<input type="radio" name="papel" value="LEGAL"> Oficio &nbsp;&nbsp;
											</td>
										</tr>
										<tr height="30">
											<td><input type="checkbox" name="chklogo"> Imprimir Logo IDIC</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
			</table>
			<?php } if ($opcion == "estadisticas") { ?>
			<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Conteo de alumnos por grado</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Conteo de alumnos por curso</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Listado de alumnos por edades</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Listado de alumnos por rango de edad</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Perdida de A�o</td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="5"> Alumnos DANE &nbsp;&nbsp;&nbsp;<input type="checkbox" name="chkexp">Excel <img src="images/ico_excel.gif" border="0" width="20" height="20" align="absmiddle"></td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="6"> Profesores por rango de edad &nbsp;&nbsp;&nbsp;</td></tr>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
						<legend class="titulo_campos">Filtros y Opciones</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
										<tr>
											<td width="60">A�o Lectivo</td>
											<td>
											<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<?php
											$sql="SELECT ano FROM anoslect ORDER BY ano desc";
											$datanol=viewsql($sql);
											foreach($datanol as $reganol)
											{
												$selano = "";
												if ($reganol->ano==$ano)
													$selano = "selected";
												echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
											}
											?>
											</select>
											</td>
										</tr>
										<tr>
											<td>Nivel</td>
											<td>
											<select name="nivel" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="">seleccionar...</option>
											<?php
											$sql= "select nivel, nombre from niveles order by id_nivel";
											$datnive=viewsql($sql);
											foreach($datnive as $regnive)
											{
												echo "<option value='$regnive->nivel$regnive->nombre'>$regnive->nombre</option>";
											}
											?>																
											</select>
											</td>
										</tr>
										<tr>
											<td>Grado</td>
											<td>
											<select name="grado" style="width: 230px;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar...</option>
											<?php
											$sql = "SELECT grado, gradonom FROM grados order by id_grado";
											$datgrad = viewsql($sql);
											foreach($datgrad as $reggrad)
											{
												echo "<option value='$reggrad->grado'>GRADO $reggrad->gradonom</option>";
											}
											?>																
											</select>
											</td>
										</tr>
									</table>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="30">
											<td>
											<input type="radio" name="papel" value="LETTER" checked> Papel Carta &nbsp;&nbsp;
											<input type="radio" name="papel" value="LEGAL"> Oficio &nbsp;&nbsp;
											</td>
											<td width="20"></td>
											<td>
												<input type="checkbox" name="chklogo"> Imprimir Logo IDIC
											</td>
										</tr>
									</table>
								</td>
								<td width="10"></td>
								<td width="1" bgcolor="#B7C4DB"></td>
								<td width="10"></td>
								<td align="center">
								</td>
							</tr>
						</table>

						</fieldset>
					</td>
				</tr>
			</table>
			<?php } if ($opcion == "dian") { ?>
			<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">

							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="Consolidado"> Pagos Pensiones y Transporte Consolidado Responsables </td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="detallado"> Pagos Pensiones y Transporte Detallado Alumnos </td></tr>	
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="nuevos_2"> Alumnos Nuevos Y Antiguos Con Nuevos Encargados De Pago</td></tr>	
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="2015deuda"> Reporte Deuda Pensi�n Y Transporte A�o Pasado Pagada En El Actual </td></tr>					
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="interes"> Intereses En Efectivo y Consignaci�n </td></tr>
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="interes_deuda"> Deuda de Intereses A�o Pasado Pagados En El Actual </td></tr>

							
							<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="efectivo"> Ingresos En Efectivo Por Mes  &#160;  &#160;       
                             
							 
                              	<select id='mes_lect'>
			   		<option value=''>Seleccione Mes</option>
			   		<option value='1'>Enero</option>
			   		<option value='2'>Febrero</option>
			   		<option value='3'>Marzo</option>
			   		<option value='4'>Abril</option>
			   		<option value='5'>Mayo</option>
			   		<option value='6'>Junio</option>
			   	    <option value='7'>Julio</option>
			   	    <option value='8'>Agosto</option>
			   	    <option value='9'>Septiembre</option>
			   	    <option value='10'>Octubre</option>
			   	    <option value='11'>Noviembre</option>
			   	    <option value='12'>Diciembre</option>


			   	</select>
                          </td>   
                              </tr>

							</tr>
							
							       
							       

						</table>
</fieldset>
				</td>

				<tr>
					<td>
						<fieldset>
						<legend class="titulo_campos">Filtros y Opciones</legend>
						<center>
						<table border='0' cellpadding='0' cellspacing='0'>
			   <tr height="30">
			   <td>Seleccione el a�o lectivo  &#160;  &#160; </td>
			   <td>
				<select name="ano" id='anio_lect' onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					<?php
					$sql="SELECT ano FROM anoslect ORDER BY ano desc";
					$datanol=viewsql($sql);
					foreach($datanol as $reganol)
					{
						$selano = "";
						if ($reganol->ano==$ano)
							$selano = "selected";
						echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
					}
					?>
				</select>
			   	</td>
			   </tr>
                           <tr height="30"><td>Rango de fechas desde </td>
                               <td>
				<input type="text" name="fchini" id="fchini" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchini, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
				hasta
				<input type="text" name="fchfin" id="fchfin" style="width: 65px;" onBlur="this.value=ValidDate(this,this.value);" onClick="popUpCalendar(this, form1.fchfin, 'dd-mm-yyyy');" title="dd-mm-aaaa" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
                               
                               </td></tr>
			   </table>	
			   </center>
			   <br>	
			</fieldset>
					</td>
				</tr>
			</table>
						


				<?php } if ($opcion == "listados") { ?>
				<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="general"> Listado General</td></tr>
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="padres"> Listado General Padres </td></tr>
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="madres"> Listado General Madres</td></tr>
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="acudientes"> Listado General Acudientes </td></tr>
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="rutas"> Listado General Rutas </td></tr>
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="matriculados"> Estudiantes Matriculados </td></tr>
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="carnets"> Listado Para Carnetizaci�n </td></tr>
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="Reptientes"> Listado de Alumnos Repitentes </td></tr>
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="hospital"> Reporte Para Hospital del Sur</td></tr>	
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="areas"> Listado de �reas Pendientes Por Alumno</td></tr>
						<tr height="25"><td>&nbsp;&nbsp;<input type="radio" name="rad_dian" id="rad_dian" value="editoriales"> Listado Para Editoriales </td></tr>
						
											
								
						</table>
						</fieldset>
                        <tr>
						<td>
						</fieldset>
					</td>
				</tr>
				</table>	
						
			
			<?php } ?>
		</td>
		<td width="20"></td>
	</tr>
	<tr><td colspan="3" height="30"></td></tr>
	<tr height="35">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<input type="image" class="botones_img" src="images/btn_imprimir.png" border="0" name="imprimir"  width="70" height="24" hspace="10" alt="Imprimir">
			<a href="reportes.php"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>