<?php
/***********************************************************************************************
* @file        : pagos_ajuste.php                                                              *
* @brief       : Formulario captura ajustes en intereses de pension                            *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 17-Jul-2015                                                    *
* @Modificado  : Nora Rodriguez 02-Nov-2018                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_pagos.php");
include("libs/fechas.php");
$id_ajuste = isset($_GET['id_ajuste']) ? $_GET['id_ajuste'] : 0 ;
$id_pago = isset($_GET['id_pago']) ? $_GET['id_pago'] : 0 ;
$id_alumno = isset($_GET['id_alumno']) ? $_GET['id_alumno'] : 0 ;
$id_entrada = isset($_GET['id_entrada']) ? $_GET['id_entrada'] : null ;
$fchentrada = isset($_GET['fchentrada']) ? $_GET['fchentrada'] : date('Y-m-d') ;
$ano = isset($_GET['ano']) ? $_GET['ano'] :  $_SESSION["anoaca"] ;
$tipo = isset($_GET['tipo']) ? $_GET['tipo'] : "P" ;
connect();
//traer ajuste
$opcion = "ajustespen";
$fchpago = date('Y-m-d');
$anopago = substr($fchpago,0,4);
$mespago = substr($fchpago,5,2);
$datajuste = datos_ajuste($id_ajuste,$ano);
//traer nombre del alumno y a�os de matriculas
$alumno = "";
$grado = "";
$curso = "";
$sql="SELECT matriculas.ano, matriculas.grado, matriculas.curso, alumnos.apellidos, alumnos.nombres FROM matriculas, alumnos WHERE matriculas.id_alumno = '$id_alumno' AND matriculas.ano = '$ano' AND  matriculas.id_alumno = alumnos.id_alumno LIMIT 1";
$datmat=viewsql($sql);
foreach($datmat as $regmat)
{
	$alumno = $regmat->apellidos." ".$regmat->nombres;
	$grado = $regmat->grado;
	$curso = $regmat->curso;
}
if ($id_alumno <= 0 or $alumno=="")
	echo"<script>window.location.href='pagos_edit.php?tipo=I';</script>";
//cargar porcentaje de interes por mora pension
$valortot = 0;
$intertot = 0;
$saldotot = 0;
$porcen = 0;
//cargar porcentaje de interes del alumno
$sql="SELECT interes FROM matriculas WHERE id_alumno = '$id_alumno' AND ano = '$ano' LIMIT 1";
$datanol=viewsql($sql);
foreach($datanol as $reganol)
{$porcen = $reganol->interes;}
//cargar pagos mensuales con ajustes de intereses para impedir que se modifique el ajuste
$filedet = "y".$ano."pagosdet";
$pagos = array();
for($i=1; $i<=11 ;$i++)
{
	$pagos[$i] = "";
}
if ($tipo=="I") {
    $opcion = "ajustesint";
    $sql="SELECT id_pago, mes FROM $filedet WHERE id_alumno = '$id_alumno' AND interesaju > '0' ";
    $datpag = viewsql($sql);
    foreach($datpag as $regpag)
    {
        $mes = $regpag->mes;
        $pagos[$mes] = $regpag->id_pago;
    }
}
$swgrabar = 1;
/*
$mes = $datajuste["mes"];
if ($id_ajuste > 0 and $pagos[$mes] > 0)
    $swgrabar = 0;
*/
$pagosaju = "y".$ano."pagosaju";
$sql="SELECT * FROM $pagosaju WHERE id_alumno = '$id_alumno' AND tipo = '$tipo' ";
$datite = viewsql($sql);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language="javascript" type='text/JavaScript'>
function validarAjuste()
{
	if(document.form1.mes.value <= 0 || document.form1.mes.value >= 12)
	{alert('Mes no v�lido ...'); document.form1.mes.focus(); return false;}
	if(document.form1.valor.value < 0)
	{alert('Valor incorrecto...'); document.form1.valor.focus(); return false;}
	if(document.form1.valor.value >= 0 && document.form1.detalles.value == '')
	{alert('Falta concepto del ajuste...'); document.form1.detalles.focus(); return false;}
	return true;
}
//Seleccionar A�o Lectivo
function SeleccionarAno()
{
	var id_alumno = document.form1.id_alumno.value;
	var ano = document.form1.ano.value;
	var theURL = 'pagos_ajuste.php?id_alumno='+id_alumno+'&ano='+ano;
	window.location.href = theURL;
}
//Funcion que confirma borrado de un ajuste
function borrarAjuste()
{
	var id_ajuste = document.form1.id_ajuste.value;
	ok=confirm('Esta seguro de eliminar AJUSTE ID '+id_ajuste+' ?');
	if(ok)
	{
		document.form1.op.value='borrar_ajuste';
		document.form1.submit();
	}
}
function seleccionarTipo()
{
	var id_alumno = document.form1.id_alumno.value;
	var id_pago = document.form1.id_pago.value;
	var id_entrada = document.form1.id_entrada.value;
	var fchentrada = document.form1.fchentrada.value;
	var tipo = document.form1.tipo.value;
	var theURL = 'pagos_ajuste.php?id_alumno='+id_alumno+'&id_pago='+id_pago+'&id_entrada='+id_entrada+'&fchentrada='+fchentrada+'&tipo='+tipo;
	window.location.href = theURL;
}

</script>
</head>
<body>
<?php include("includes/menu_pago.php"); ?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<?php if ($swgrabar > 0) { ?>
<form action="database/grabar_pagos.php" method="post" name="form1" onSubmit="return validarAjuste();">
<?php } else { ?>
<form action="" method="post" name="form1" onSubmit="return validarAjuste();">
<?php } ?>
<input type='hidden' name='op' value='grabar_ajuste'>
<input type='hidden' name='id_ajuste' value='<?php echo $id_ajuste;?>'>
<input type='hidden' name='id_alumno' value='<?php echo $id_alumno;?>'>
<input type='hidden' name='id_pago' value='<?php echo $id_pago;?>'>
<input type='hidden' name='id_entrada' value='<?php echo $id_entrada;?>'>
<input type='hidden' name='fchentrada' value='<?php echo $fchentrada;?>'>
<input type='hidden' name='tipo' value='<?php echo $tipo;?>'>
	<tr height="25">
		<th width="15"></th>
		<th colspan="3" class="titulo" align="left"><?php echo "ID ".$id_alumno." ".$alumno.", ".$grado."&deg; CURSO ".$curso;?></th>
		<th width="15"></th>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr>
		<td></td>
		<td colspan="3">
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr>
					<td valign='top'>
						<table border="0" cellpadding="1" cellspacing="1">
							<tr height="40">
                                                            <td class="titulo_error">
                                                                <?php if ($tipo=="P") { ?>
                                                                AJUSTES EN PENSIONES
                                                                <?php } else { ?>
                                                                AJUSTES INTERESES POR MORA
                                                                <?php } ?>
                                                            </td>
							</tr>
						</table>
						<fieldset>
						<?php if ($id_ajuste > 0) { ?>
						<legend class="titulo_campos">Ajuste ID N� <?php echo $id_ajuste;?></legend>
						<?php } else { ?>
						<legend class="titulo_campos">Nuevo Ajuste</legend>
						<?php } ?>
						<table border="0" cellpadding="1" cellspacing="1">
							<tr height="25">
								<td>A�o&nbsp;</td>
								<td>
									<input type="text" name="ano" value="<?php echo $datajuste["ano"];?>" style="width: 50px;" readonly onKeyDown="if(event.keyCode==13) event.keyCode=9;">
								</td>
							</tr>
							<tr height="25">
								<td>Mes</td>
								<td><input type="text" name="mes" value="<?php echo $datajuste["mes"];?>" style="width: 25px;" maxlength="2" title="mes" onKeyDown="if(event.keyCode==13) event.keyCode=9;" autofocus></td>
							</tr>
							<tr height="25">
								<td>Valor&nbsp; </td>
								<td><input type="text" name="valor" style="width: 55px; text-align: right; font-size: 12px;" value="<?php echo $datajuste["valor"];?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;"></td>
							</tr>
						</table>
						</fieldset>
						<fieldset>
						<legend class="titulo_campos">Concepto Ajuste</legend>
						<table border="0" cellpadding="1" cellspacing="1" width="100%">
							<tr>
								<td>
								<textarea name='detalles' style="width: 240px; height: 60px; text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;"><?php echo $datajuste["detalles"];?></textarea>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
					<td width="15"></td>
					<td>
						<table border="0" cellpadding="2" cellspacing="1" bgcolor="#C0C0C0" align="center">
							<tr>
								<th width="50" class="titulo_campos">Mes</th>
								<th width="50" class="titulo_campos">Ajuste $</th>
								<th width="300" class="titulo_campos">Concepto Ajuste</th>
                                                                <?php if ($tipo=="P") { ?>
								<th width="50">Usuario</th>
                                                                <?php } else { ?>
								<th width="50">IDPago</th>
                                                                <?php } ?>
								<th width="30">Editar</th>
							</tr>
							<?php
							$valortot = 0;
							for($i=2; $i<=11 ;$i++)
							{
								if ($i <= 9)
									$mes = "0".$i;
								else
									$mes = $i;
								$idaju = 0;
								$valor = "";
								$detalles = "";
                                                                $usucrea = "";
								$bgcolor = "#FFFFFF";
								foreach($datite as $regite)
								{
									if (intval($regite->mes)==$i)
									{
										$idaju = $regite->id_ajuste;
										$valor = $regite->valor;
										$detalles = $regite->detalles;
                                                                                $usucrea = $regite->usucrea;
									}
								}
								if ($id_ajuste > 0 and $id_ajuste==$idaju)
									$bgcolor = "#FFFFDF";
								?>
								<tr bgcolor="<?php echo $bgcolor;?>" align="center" height="22">
									<th align="left"><?php echo $mes." - ".strtoupper(substr(nombre_mes($mes),0,3));?></th>
									<td align="right"><?php echo $valor;?></td>
									<td align="left"><?php echo $detalles;?></td>
                                                                        <?php if ($tipo=="P") { ?>
                                                                        <td><?php echo $usucrea;?></td>
                                                                        <?php } else { ?>
                                                                        <td><?php echo $pagos[$i];?></td>
                                                                        <?php } ?>
									<td><?php if ($idaju > 0) {?><a class="link_list" href="pagos_ajuste.php?id_ajuste=<?php echo $idaju;?>&id_alumno=<?php echo $id_alumno;?>&id_pago=<?php echo $id_pago;?>&id_entrada=<?php echo $id_entrada;?>&fchentrada=<?php echo $fchentrada;?>&tipo=<?php echo $tipo;?>"><img src="images/actividadsm.png" width="16" height="16" border="0" hspace="0" align="absmiddle"></a><?php } ?></td>
								</tr>
								<?php
							}
							?>
						</table>
					</td>
				</tr>
			</table>			
		</td>
		<td></td>
	</tr>
	<tr><td colspan="5" height="20"></td></tr>
	<tr height="36" bgcolor="#CCCCCC" background="images/backsilver.gif">
		<td></td>
		<td align="left">
			<?php if ($id_ajuste > 0) { ?>
			ACTUALIZADO <?php echo substr($datajuste["fchactual"],0,16);?> <?php echo $datajuste["usuactual"];?>
			<?php } ?>
		</td>
		<td colspan="2" align="right" valign="middle">
			<?php if ($swgrabar > 0 and $_SESSION["nivpag"] >= 3 and $_SESSION["nivmat"] >= 1) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="5" alt="Grabar" align="absmiddle">
			<?php if ($id_ajuste > 0) { ?>
			<a href="javascript:;" onClick="borrarAjuste();"><img src="images/btn_eliminar.png" border="0" width="70" height="24" alt="Eliminar" hspace="5" align="absmiddle"></a>			
			<?php } } ?>
			<a href="javascript:;" onClick="window.opener.document.location.reload(); window.close();"><img src="images/btn_cerrar.png" border="0" width="70" height="24" alt="Cerrar" hspace="5" align="absmiddle"></a>
		</td>
		<td></td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
