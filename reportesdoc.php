<?php
/***********************************************************************************************
* @file        : reportes.php                                                                  *
* @brief       : Opciones de impresi�n generales                                               *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 26-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 26-Mar-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("database/datos_alumnos.php");
include("libs/fechas.php");
$id_profesor = $_SESSION["usunro"];
$ano = isset($_POST['ano']) ? $_POST['ano'] : null ;
if ($ano <= 0)
	$ano = isset($_GET['ano']) ? $_GET['ano'] : null ;
if ($ano <= 0)
	$ano = $_SESSION["anoaca"];
$profasig = "y".$ano."profasig";
connect();
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/fechas.js'></script>
<script language='JavaScript' type='text/JavaScript' src='js/calendario/popcalendar.js'></script>
<script language='JavaScript'>
function validar()
{
	if(document.form1.periodo.value == '' && document.form1.formato[0].checked!=true && document.form1.formato[4].checked!=true && document.form1.formato[5].checked!=true && document.form1.formato[6].checked!=true)
	{alert('Debe seleccionar periodo...'); document.form1.periodo.focus(); return false;}
	if(document.form1.gradocurso.value == '')
	{alert('Debe seleccionar grado y curso...'); document.form1.gradocurso.focus(); return false;}
	return true;
}
function imprimir()
{
	var ano = document.form1.ano.value;
	var pertipo = document.form1.periodo.value.substr(0,1);
	var periodo = document.form1.periodo.value.substr(1,1);
	var notanum = document.form1.periodo.value.substr(2,1);
	var pos = document.form1.gradocurso.value.indexOf('*');
	var grado = document.form1.gradocurso.value.substr(0,pos);
	var curso = document.form1.gradocurso.value.substr(pos+1);
	var tipoedu = 'F';
	var implogo = 'S';
	var papel = 'LEGAL';
	var implogros = 'N';
	var impactiv = 'N';
	var impmetod = 'N';
	if (document.form1.papel[1].checked==true)
		papel = 'LETTER';
	if (document.form1.chklogo.checked==false)
		implogo = '';
	if (document.form1.formato[1].checked==true)
		implogros = 'S';
	if (document.form1.formato[2].checked==true)
	{
		implogros = 'S';
		impactiv = 'S';
	}
	if (document.form1.formato[3].checked==true)
	{
		implogros = 'S';
		impmetod = 'S';
	}
	var theURL= 'reports/rep_asignaturas.php?'+'&implogros='+implogros+'&impactiv='+impactiv+'&impmetod='+impmetod+'&';
	if (document.form1.formato[4].checked==true)
		theURL= 'reports/rep_alumnos.php?';
	if (document.form1.formato[5].checked==true)
		theURL= 'reports/rep_perdida.php?notanum='+notanum+'&';
	if (document.form1.formato[6].checked==true)
		theURL= 'reports/rep_perdidaare.php?notanum='+notanum+'&';
	if (document.form1.formato[7].checked==true)
		theURL= 'reports/rep_perdidaasi.php?notanum='+notanum+'&';
	if (document.form1.formato[8].checked==true)
		theURL= 'reports/rep_logros.php?notanum='+notanum+'&';
	if (document.form1.formato[9].checked==true)
		theURL= 'reports/rep_logroscon.php?implogros=S'+'&pertipo='+pertipo+'&periodo='+periodo+'&';
	if (document.form1.formato[10].checked==true)
		theURL= 'reports/rep_logrospen.php?implogros=S'+'&pertipo='+pertipo+'&periodo='+periodo+'&';
	theURL = theURL+'ano='+ano+'&grado='+grado+'&curso='+curso+'&tipoedu='+tipoedu+'&papel='+papel+'&implogo='+implogo;
	var winName='';
	var features='top=0,left=0,scrollbars=yes,menubar=yes,width=900,height=600,resizable=yes';
	window.open(theURL,winName,features);
}
function seleccionar()
{
	var ano = document.form1.ano.value;
	var theURL = 'reportesdoc.php?ano='+ano;
	window.location.href = theURL;
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="40"><td class="titulo"><img src='images/printer.jpg' width="40" height="40" border="0" align="absmiddle"> REPORTES</td></tr>
</table>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="javascript:imprimir();" method="post" name="form1" onSubmit="return validar();">
	<tr height="30">
		<th colspan="3" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
		<img src="images/profesorsm.png" border="0" width="20" height="20" align="absmiddle"> OPCIONES DOCENTE
		</th>
	</tr>
	<tr><td colspan="3" height="10"></td></tr>
	<tr height="280" valign="top">
		<td width="20"></td>
		<td align="center">
			<table border="0" width="100%">
				<tr valign="top">
					<td align="center">
						<fieldset>
						<table border="0" cellpadding="0" cellspacing="0">
						<tr valign="top">
							<td>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="0" checked> Lista Asignaturas</td></tr>
									<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="1"> Lista de Asignaturas y Logros</td></tr>
									<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="2"> Lista de Asignaturas, Logros y Actividades</td></tr>
									<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="3"> Lista de Asignaturas, Logros y Metodologia</td></tr>
									<tr><td height="25">&nbsp;&nbsp;<input type="radio" name="formato" value="4"> Lista de Alumnos</td></tr>
								</table>
							</td>
							<td width="20"></td>
							<td>
								<table border="0" cellpadding="0" cellspacing="0">
									<tr height="21"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="5"> Perdida de A�o</td></tr>
									<tr height="21"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="6"> Perdida de Areas</td></tr>
									<tr height="21"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="7"> Perdida de Asignaturas</td></tr>
									<tr height="21"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="8"> Logros Perdidos - Detallado</td></tr>
									<tr height="21"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="9"> Logros Perdidos - Consolidado</td></tr>
									<tr height="21"><td>&nbsp;&nbsp;<input type="radio" name="formato" value="10"> Logros Pendientes</td></tr>
								</table>
							</td>
						</tr>
						</table>
						</fieldset>
					</td>
				</tr>
				<tr>
					<td>
						<fieldset>
						<legend class="titulo_campos">Filros y Opciones</legend>
						<table border="0" cellpadding="0" cellspacing="0">
							<tr valign="top">
								<td>
									<table border="0" cellpadding="1" cellspacing="1">
										<tr>
											<td width="65">A�o Lectivo</td>
											<td>
											<select name="ano" onKeyDown="if(event.keyCode==13) event.keyCode=9;" onChange="javascript:seleccionar();">
											<?php
											$sql="SELECT ano FROM anoslect ORDER BY ano desc";
											$datanol=viewsql($sql);
											foreach($datanol as $reganol)
											{
												$selano = "";
												if ($reganol->ano==$ano)
													$selano = "selected";
												echo "<option value='$reganol->ano' $selano>$reganol->ano</option>";
											}
											?>
											</select>
											<select name="periodo" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Periodo corte...</option>
											<?php
											$cierre = "";
											$sql="SELECT * FROM periodos WHERE ano = '$ano' order by notanum";
											$datperi=viewsql($sql);
											foreach($datperi as $regperi)
											{
												if ($regperi->pertipo=="P" and $regperi->periodo=="4" and $regperi->estado=="CERRADO")
													$cierre="CERRADO";
												if ($regperi->estado!="")
													echo "<option value='$regperi->pertipo$regperi->periodo$regperi->notanum'>$regperi->pernomb</option>";
											}
											if ($cierre=="CERRADO")
											{
											?>
											<option value="P59">DEFINITIVAS</option>
											<?php } ?>
											</select>
											</td>
										</tr>
										<tr>
											<td>Grado y Curso</td>
											<td>
											<select name="gradocurso" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
											<option value="" selected>Seleccionar...</option>
											<?php
											//cursos docente
											$sql = "SELECT $profasig.grado, $profasig.curso, grados.gradonom FROM $profasig, grados WHERE $profasig.grado = grados.grado AND $profasig.id_profesor = '$id_profesor' GROUP BY $profasig.grado, $profasig.curso ORDER BY sign($profasig.grado), cast(SUBSTRING_INDEX($profasig.grado, '-', 1) as decimal), $profasig.curso";
											if(strtoupper($_SESSION["usulog"])=="ADMIN")
												$sql = "SELECT cursos.grado, cursos.curso, grados.gradonom FROM cursos, grados where cursos.ano = '$ano' and cursos.grado = grados.grado order by grados.id_grado, cursos.curso";
											$datgrad = viewsql($sql);
											foreach($datgrad as $reggrad)
											{
												echo "<option value='$reggrad->grado*$reggrad->curso'>GRADO $reggrad->gradonom - CURSO $reggrad->curso</option>";
											}
											?>																
											</select>
											</td>
										</tr>
									</table>
								</td>
								<td width="15"></td>
								<td>
									<table border="0" cellpadding="0" cellspacing="0">
										<tr height="25">
											<td>
											<input type="radio" name="papel" value="LETTER" checked> Papel Carta &nbsp;&nbsp;
											<input type="radio" name="papel" value="LEGAL"> Oficio &nbsp;&nbsp;
											</td>
										</tr>
										<tr height="25"><td><input type="checkbox" name="chklogo"> Imprimir Logo IDIC</td></tr>
									</table>
								</td>
							</tr>
						</table>
						</fieldset>
					</td>
				</tr>
			</table>
		</td>
		<td width="20"></td>
	</tr>
	<tr><td colspan="3" height="10"></td></tr>
	<tr height="35">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<input type="image" class="botones_img" src="images/btn_imprimir.png" border="0" name="imprimir"  width="70" height="24" hspace="10" alt="Imprimir">
			<a href="reportes.php"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
</body>
</html>
<?php disconnect();?>
