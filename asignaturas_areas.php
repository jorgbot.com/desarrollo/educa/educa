<?php
/***********************************************************************************************
* @file        : asignaturas_areas.php                                                         *
* @brief       : Lista y edita areas acad�micas                                                *
* @version     : 1.0                                                                           *
* @company     : IDIC                                                                          *
* @author      : Nora Rodriguez 21-feb-2012                                                    *
* @Modificado  : Nora Rodriguez 21-feb-2012                                                    *
***********************************************************************************************/

session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.close();window.opener.document.location.href='libs/logout.php';</script>";}
include("webparam.php");
include("database/database.php");
include("libs/fechas.php");
$opcion = isset($_GET['opcion']) ? $_GET['opcion'] : null ;
$id_area = isset($_GET['id_area']) ? $_GET['id_area'] : null ;
$grado = isset($_GET['grado']) ? $_GET['grado'] : null ;
if ($opcion == "")
	$opcion = "listar";
connect();
//lista de grados
$sql="select id_grado, grado, gradonom from grados order by id_grado";
$datgrad=viewsql($sql);
?>
<html>
<head>
<title>IDIC</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link rel="stylesheet" type="text/css" href="estilos.css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language="javascript" type='text/JavaScript'>
function validar()
{
	if(document.form1.area.value == '')
	{alert('Falta asignar descripci�n del Area...');document.form1.area.focus(); return false;}
	return true;
}
function borrarconf()
{
	ok=confirm("Esta seguro de eliminar esta Area?");
	if(ok)
	{
		document.form1.op.value='borrararea';
		document.form1.submit();
	}
}
function seleccionar()
{
	var grado = document.form0.grado.value;
	var theURL = 'asignaturas_areas.php?grado='+grado;
	window.location.href = theURL;
}
</script>
</head>
<body>
<table width="95%" cellpadding="0" cellspacing="0" border="0" align="center">
	<tr height="50">
		<td class="titulo"><img src='images/areas.jpg' width="40" height="40" border="0" align="absmiddle"> Areas Acad�micas</td>
		<td class="subtitulo" align="right"></td>
	</tr>
</table>
<?php if ($opcion == "listar") { ?>
<table width="95%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
	<tr height="420" bgcolor="#FFFFFF">
		<td align="center" valign="top">
			<table width="100%" border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" align="center">
			<form action="" method="post" name="form0">
				<tr height="20">
					<th align="left">
						&nbsp;
						<select name="grado" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
						<?php
						foreach($datgrad as $reggrad)
						{
							if ($grado == $reggrad->grado)
								echo "<option value='$reggrad->grado' selected>GRADO $reggrad->gradonom</option>";
							else
								echo "<option value='$reggrad->grado'>GRADO $reggrad->gradonom</option>";
						}
						?>																
						</select>
					</th>
					<th width="30">COD</th>
					<th width="40">ID</th>
				</tr>
				<?php
				$sql="SELECT id_area, area, areacod FROM areas where grado = '$grado' order by area";
				$datarea=viewsql($sql);
				foreach($datarea as $regarea)
				{
				$id_area = $regarea->id_area;
				$area = $regarea->area;
				?>		
				<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
					<td><?php echo $area;?></td>
					<td><?php echo $regarea->areacod;?></td>
					<td><a class="link_list" href="asignaturas_areas.php?opcion=editar&id_area=<?php echo $id_area;?>&grado=<?php echo $grado;?>" title="ID area"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $id_area;?></a></td>
				</tr>
				<?php } ?>
			</form>
			</table>
		</td>
	</tr>
	<tr height="36">
		<td align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 4) { ?>
			<a href="asignaturas_areas.php?opcion=editar&grado=<?php echo $grado;?>"><img src="images/btn_nuevo.png" border="0" width="75" height="24" alt="Nuevo Registro" hspace="10"></a>
			<?php } ?>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</table>
<br><br>
<?php } if ($opcion == "editar") {
$titulo = "Nueva Area";
$chksi = "checked";
$chkno = "";
$area = "";
$areacod = "";
if ($id_area > 0)
{
	$sql="select * from areas where id_area = '$id_area' limit 1";
	$datarea = viewsql($sql);
	foreach($datarea as $regarea)
	{
		$area = $regarea->area;
		$areacod = $regarea->areacod;
		if ($regarea->inactivar == 1)
		{
			$chksi = "";
			$chkno = "checked";
		}
	}
	$titulo = "Editar Area";
}
$titulo.= " Grado ".$grado;
?>
<table width="95%" border="0" class="tabla_marco" cellpadding="0" cellspacing="0" align="center">
<form action="database/grabar_asignaturas.php" method="post" name="form1" onSubmit="return validar();">
<input type='hidden' name='op' value='grabararea'>
<input type='hidden' name='id_area' value='<?php echo $id_area;?>'>
<input type='hidden' name='grado' value='<?php echo $grado;?>'>
	<tr height="10">
		<td width="30"></td>
		<td></td>
		<td width="30"></td>
	</tr>
	<tr valign="top" align="left">
		<td></td>
		<td>
			<fieldset>
			<legend class="titulo_campos"><?php echo $titulo;?></legend>
			<table border="0" cellpadding="0" cellspacing="0" width="100%">
				<tr><td height="30"></td></tr>
				<tr height="30">
					<td>
					<input type="text" name="area" size="90" value="<?php echo $area;?>" style="text-transform:uppercase;" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					</td>
				</tr>
				<tr height="30">
					<td>
					CODIGO &nbsp;
					<input type="text" name="areacod" size="6" value="<?php echo $areacod;?>" onKeyDown="if(event.keyCode==13) event.keyCode=9;">
					&nbsp;(El mismo para todos los grados en la misma area)
					</td>
				</tr>
				<tr height="30">
					<td>
						Activa <input type="radio" name="inactivar" value="0" <?php echo $chksi;?>>
						&nbsp;&nbsp;&nbsp;
						Inactiva <input type="radio" name="inactivar" value="1" <?php echo $chkno;?>>
					</td>
				</tr>
				<tr><td height="30"></td></tr>
			</table>
			</fieldset>
		</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20"></td></tr>
	<tr height="36">
		<td colspan="3" align="right" valign="middle" bgcolor="#CCCCCC" background="images/backsilver.gif">
			<?php if ($_SESSION["nivasi"] >= 4) { ?>
			<input type="image" class="botones_img" src="images/btn_grabar.png" border="0" name="grabar"  width="70" height="24" hspace="10" alt="Grabar">
			<?php } if ($id_area!="" and $_SESSION["nivasi"] >= 4) { ?>
			<a href="javascript:;" onClick="borrarconf();"><img src="images/btn_eliminar.png" border="0" width="75" height="24" alt="Eliminar" hspace="10"></a>
			<?php } ?>
			<a href="asignaturas_areas.php?grado=<?php echo $grado;?>"><img src="images/btn_regresar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
			<a href="javascript:;" onClick="window.close();"><img src="images/btn_cerrar.png" border="0" width="75" height="24" alt="Cerrar" hspace="10"></a>
		</td>
	</tr>
</form>
</table>
<?php } ?>
</body>
</html>
<?php disconnect();?>
