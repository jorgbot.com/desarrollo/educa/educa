<?php
/***********************************************************************************************
* @file        : setup_usuarios.php                                                            *
* @brief       : Lista usuarios del aplicativo                                                 *
* @version     : 1.0                                                                           *
* @author      : Nora Rodriguez 20-Mar-2012                                                    *
* @Modificado  : Nora Rodriguez 20-Mar-2012                                                    *
***********************************************************************************************/
session_start();
if($_SESSION["usulog"]=="")
{echo"<script>alert('La sesi�n de usuario ha expirado.');window.location.href='libs/logout.php';</script>";}
if($_SESSION["nivusu"] <= 0)
{echo"<script>alert('Opci�n no autorizada.');window.location.href='inicio.php';</script>";}
include("database/database.php");
include("libs/fechas.php");
$op = isset($_POST['op']) ? $_POST['op'] : null ;
$perfil = isset($_GET['perfil']) ? $_GET['perfil'] : null ;
$buscar = "";
if ($op == "buscar")
	$buscar = $_POST['buscar'];
connect();
?>
<html><!-- InstanceBegin template="/Templates/plantilla.dwt.php" codeOutsideHTMLIsLocked="false" -->
<head>
<!-- InstanceBeginEditable name="doctitle" -->
<title>IDIC</title>
<!-- InstanceEndEditable -->
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<meta name="robots" content="noindex"/>
<meta name="googlebot" content="noindex"/>
<!-- InstanceBeginEditable name="head" -->
<link rel="stylesheet" type="text/css" href="estilos.css">
<link href="estilosfot.css" rel="stylesheet" type="text/css">
<link rel="shortcut icon" type="image/icon" href="images/idic.ico">
<script language='JavaScript' type='text/JavaScript' src='js/ventanas.js'></script>
<script language='JavaScript' type='text/JavaScript'>
function seleccionar()
{
	var ano = document.form0.perfil.value;
	var theURL = 'setup_usuarios.php?perfil='+perfil;
	window.location.href = theURL;
}
</script>
<!-- InstanceEndEditable -->
</head>
<body>
<table width="100%" border="0" cellpadding="0" cellspacing="0">
	<tr height="1" bgcolor="#FFFF66">
		<td></td>
		<td width="1000"></td>
		<td></td>
	</tr>
	<tr height="150" bgcolor="#006600">
		<td background="fondos/fondolat1.jpg">&nbsp;</td>
		<td width="1000" align="center">
		<!-- InstanceBeginEditable name="encabezado" --><?php include("includes/encabezado.php");?><!-- InstanceEndEditable -->	
		</td>
		<td background="fondos/fondolat2.jpg">&nbsp;</td>
	</tr>
	<tr height="40" bgcolor="#006600">
		<td background="fondos/top_verde.jpg"></td>
		<td background="fondos/top_verde.jpg" class="titulo_color">
		<!-- InstanceBeginEditable name="menu" --><?php include("includes/menu_top.php");?><!-- InstanceEndEditable -->
		</td>
		<td background="fondos/top_verde.jpg"></td>
	</tr>
	<tr height="3" bgcolor="#FFFF66">
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
		<td background="fondos/back_amarillo.jpg"></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_top.jpg"></td></tr>
	<tr height="535">
		<td></td>
		<td valign="top">
		<!-- InstanceBeginEditable name="contenido" -->
		<table border="0" cellpadding="0" cellspacing="0" width="1000" align="center">
			<tr><td colspan="3" height="5"></td></tr>
			<tr valign="top">
				<td width="200">
				<?php include("includes/menu.php");?>
				</td>
				<td width="10"></td>
				<td>
					<?php include("includes/menu_setup.php");?>
					<br/>
					<table border="0" cellpadding="0" cellspacing="0" width="100%">
					<form action="" method="post" name="form0">
					<input type="hidden" name="op" value="buscar">
						<tr><td colspan="3" height="15"></td></tr>
						<tr height="32">
							<td width="6" background="images/backsilver401.gif"></td>
							<td background="images/backsilver402.gif" class="titulo"><img src="images/key.png" width="24" height="22" border="0" align="absmiddle"> Usuarios</td>
							<td align="right" background="images/backsilver402.gif">
								BUSCAR...
								<input type="text" name="buscar" value="<?php echo $buscar;?>" size="25" style="text-transform:uppercase;">
								&nbsp;&nbsp;
								<select name="perfil" style="font-size: 12px; font-weight: bold;" onChange="javascript:seleccionar();">
								<option value="" selected>Perfil...</option>
								<?php
								$sql="SELECT * FROM listas WHERE listip = 'USUARIOS' order by id";
								$lista=viewsql($sql);
								foreach($lista as $reglis)
								{
									if ($perfil == $reglis->lisnom)
										echo "<option value='$reglis->lisnom' selected>$reglis->lisnom</option>";
									else
										echo "<option value='$reglis->lisnom'>$reglis->lisnom</option>";
								}
								?>																
								</select>
								&nbsp;
								&nbsp;
								<input type="image" class="botones_img" name="btnbuscar" src="images/btn_lupa.png" width="24" height="24" border="0" alt="buscar" title="buscar..." align="absmiddle">
								<?php if ($_SESSION["nivusu"] >= 3) { ?>
								&nbsp;
								<a href="setup_usuariosedit.php?opcion=SELECCIONAR" onclick="newWindow(this.href, '', 800, 450, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank"><img src='images/btn_nuevo.png' border="0" width="70" height="24" alt="Nuevo" align="absmiddle" title="Crear registro"></a>
								<?php } ?>
							</td>
							<td width="6" background="images/backsilver403.gif"></td>
						</tr>
					</form>
					</table>
					<table border="0" cellpadding="1" cellspacing="1" bgcolor="#C0C0C0" width="100%">
						<tr height="20">
							<th>Apellidos y Nombres</th>
							<th>Perfil</th>
							<th>Login</th>
							<th width="80">Inactivo</th>
						</tr>
						<?php
						$sql="SELECT id_usuario, login, apellidos, nombres, tipo, inactivar, docunum FROM usuarios WHERE login != 'ADMIN' ";
						if ($buscar != "")
						{
							if ($buscar > 0)
								$sql.="AND id_usuario = '$buscar' OR docunum = '$buscar' ";
							else
								$sql.="AND apellidos like '%$buscar%' OR nombres LIKE '%$buscar%' ";
						}
						$sql.= "ORDER BY inactivar, apellidos, nombres";
						$datusua=viewsql($sql);
						foreach($datusua as $regusua)
						{
						$id_usuario = $regusua->id_usuario;
						$seccion = "seccion".$id_usuario;
						$inactivo = "";
						if ($regusua->inactivar==1)
							$inactivo = "Inactivo";
						?>		
						<tr height="20" bgcolor="#FFFFFF" ONMOUSEOVER="this.bgColor='#F4F4F4'" ONMOUSEOUT="this.bgColor='#FFFFFF'" align="left">
							<td><?php echo $regusua->apellidos." ".$regusua->nombres;?></td>
							<td><?php echo $regusua->tipo;?></td>
							<td><a name="<?php echo $seccion;?>" class="link_list" href="setup_usuariosedit.php?id_usuario=<?php echo $id_usuario;?>" onclick="newWindow(this.href, '', 800, 450, 1, 1, 0, 0, 0, 1, 0); return false;" target="_blank" title="Editar"><img src="images/ico_lupa.gif" width="11" height="11" border="0" hspace="0" align="absmiddle"> <?php echo $regusua->login;?></a></td>
							<td><?php echo $inactivo;?></td>
						</tr>
						<?php } ?>
					</table>
				</td>
			</tr>
			<tr><td colspan="3" height="5"></td></tr>			
		</table>
		<!-- InstanceEndEditable -->	
    	</td>
		<td></td>
	</tr>
	<tr><td colspan="3" height="20" background="fondos/back_bot.jpg"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#D3A919"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FFDB4B"></td></tr>
	<tr height="1"><td colspan="3" bgcolor="#FCF463"></td></tr>
	<tr>
		<td colspan="3" valign="top">
		<div id="fondo_pie">
		<!-- InstanceBeginEditable name="piedepagina" --><?php include("includes/menu_bottom.php");?><!-- InstanceEndEditable -->
		</div>
		</td>
	</tr>
</table>
</body>
<!-- InstanceEnd --></html>
<?php disconnect();?>